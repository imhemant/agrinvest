package com.agrinvestapp;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.agrinvestapp.data.AppDataManager;

/**
 * Created by hemant.
 * Date: 30/8/18
 * Time: 2:59 PM
 */

public class Agrinvest extends Application {

    public static double LATITUDE;
    public static double LONGITUDE;
    private static AppDataManager appInstance;
    private static Agrinvest instance;

    public static synchronized Agrinvest getInstance() {
        if (instance != null) {
            return instance;
        }
        return new Agrinvest();
    }

    public static AppDataManager getDataManager() {
        return appInstance;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        appInstance = AppDataManager.getInstance(this);
    }

}
