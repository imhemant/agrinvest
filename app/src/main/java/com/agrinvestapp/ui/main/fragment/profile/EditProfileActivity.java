package com.agrinvestapp.ui.main.fragment.profile;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.BuildConfig;
import com.agrinvestapp.R;
import com.agrinvestapp.data.model.db.UserInfoBean;
import com.agrinvestapp.ui.base.BaseActivity;
import com.agrinvestapp.ui.base.dialog.ImagePickCallback;
import com.agrinvestapp.ui.base.dialog.ImagePickDialog;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.CommonUtils;
import com.agrinvestapp.utils.Permission;
import com.agrinvestapp.volley.AppHelper;
import com.agrinvestapp.volley.VolleyMultipartRequest;
import com.agrinvestapp.volley.VolleyRequest;
import com.agrinvestapp.volley.VolleyResponseListener;
import com.android.volley.VolleyError;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class EditProfileActivity extends BaseActivity implements View.OnClickListener {

    private Bitmap productBitmap;
    private ImageView imgUser;
    private EditText etName, etNum;
    private TextView tvAddress;

    private String address = "";
    private Double latitude = 0.0, longitude = 0.0;
    private String mCurrentPhotoPath = "";
    private UserInfoBean userInfoBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        userInfoBean = getDataManager().getUserInfo();
        initView();
    }

    private void initView() {
          /* toolbar view start */
        ImageView imgBack = findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);
        imgBack.setVisibility(View.VISIBLE);

        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.edit_profile);

        etName = findViewById(R.id.etName);
        TextView tvEmail = findViewById(R.id.tvEmail);
        etNum = findViewById(R.id.etNum);
        imgUser = findViewById(R.id.imgUser);
        tvAddress = findViewById(R.id.tvAddress);

        if (!userInfoBean.getProfileImage().isEmpty())
            Picasso.get().load(userInfoBean.getProfileImage()).placeholder(R.drawable.user_placeholder).into(imgUser);

        etName.setText(userInfoBean.getName());
        tvEmail.setText(userInfoBean.getEmail());
        etNum.setText(userInfoBean.getContactNumber());

        latitude = userInfoBean.getLatitude().isEmpty() ? 0.0 : Double.parseDouble(userInfoBean.getLatitude());
        longitude = userInfoBean.getLongitude().isEmpty() ? 0.0 : Double.parseDouble(userInfoBean.getLongitude());
        if (latitude == 0.0) {
            tvAddress.setText("");
        } else {
            address = userInfoBean.getLocation();
            tvAddress.setText(address);
        }

        findViewById(R.id.btnUpdate).setOnClickListener(this);
        imgUser.setOnClickListener(this);
        tvAddress.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.imgBack:
                getActivity().onBackPressed();
                break;

            case R.id.btnUpdate:
                if (isNetworkConnected(false)) {
                    doUpdateProfile();
                } else
                    CommonUtils.showToast(getActivity(), getString(R.string.alert_internet), Toast.LENGTH_SHORT);
                break;

            case R.id.imgUser:
                if (Permission.RequestMultiplePermissionCamera(getActivity())) {
                    ImagePickDialog.newInstance(new ImagePickCallback() {
                        @Override
                        public void onCameraClick(ImagePickDialog imagePickDialog) {
                            dispatchTakePictureIntent();

                            imagePickDialog.dismiss();
                        }

                        @Override
                        public void onGalleryClick(ImagePickDialog imagePickDialog) {
                            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(photoPickerIntent, AppConstants.REQUEST_GALLERY);

                            imagePickDialog.dismiss();
                        }
                    }).show(getSupportFragmentManager());
                }
                break;

            case R.id.tvAddress:
                tvAddress.setClickable(false);
                try {
                    Intent intent =
                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                                    .build(getActivity());
                    startActivityForResult(intent, AppConstants.PLACE_AUTOCOMPLETE_REQUEST_CODE);
                } catch (GooglePlayServicesRepairableException e) {
                    CommonUtils.showToast(getActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                } catch (GooglePlayServicesNotAvailableException e) {
                    CommonUtils.showToast(getActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                }
                new Handler().postDelayed(() -> tvAddress.setClickable(true), 2000);
                break;
        }
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getActivity(),
                        BuildConfig.APPLICATION_ID + ".provider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, AppConstants.REQUEST_TAKE_PHOTO);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        @SuppressLint("SimpleDateFormat")
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private boolean verifyInputs(String nameTemp, String contNumTemp, String address) {
        if (nameTemp.isEmpty()) {
            CommonUtils.showToast(getActivity(), getString(R.string.alert_enter_name), Toast.LENGTH_SHORT);
            return false;
        } else if (!(nameTemp.length() >= 3)) {
            CommonUtils.showToast(getActivity(), getString(R.string.alert_enter_name_length), Toast.LENGTH_SHORT);
            return false;
        } else if (contNumTemp.isEmpty()) {
            CommonUtils.showToast(getActivity(), getString(R.string.alert_enter_number), Toast.LENGTH_SHORT);
            return false;
        } else if (!(contNumTemp.length() >= 7 && contNumTemp.length() <= 12)) {
            CommonUtils.showToast(getActivity(), getString(R.string.alert_enter_cont_length), Toast.LENGTH_SHORT);
            return false;
        } else if (address.isEmpty()) {
            CommonUtils.showToast(getActivity(), getString(R.string.alert_select_address), Toast.LENGTH_SHORT);
            return false;
        } else return true;
    }

    private void doUpdateProfile() {
        String nameTemp = etName.getText().toString().trim();
        String contNumTemp = etNum.getText().toString().trim();

        if (verifyInputs(nameTemp, contNumTemp, address)) {
            setLoading(true);
            HashMap<String, String> mParameterMap = new HashMap<>();
            mParameterMap.put("name", nameTemp);
            mParameterMap.put("email", userInfoBean.getEmail());
            mParameterMap.put("contactNumber", contNumTemp);
            mParameterMap.put("location", address);
            mParameterMap.put("longitude", latitude.toString());
            mParameterMap.put("latitude", longitude.toString());
            mParameterMap.put("language", userInfoBean.getLanguage());

            HashMap<String, VolleyMultipartRequest.DataPart> mDataParameterMap = new HashMap<>();

            if (productBitmap != null) {
                byte[] productImageInFile = AppHelper.getFileDataFromDrawable(productBitmap);
                mDataParameterMap.put("profileImage", new VolleyMultipartRequest.DataPart("image.jpg", productImageInFile, "*/*"));
            }

            getDataManager().doServerProfileUpdateApiCall(getDataManager().getHeader(), mParameterMap, mDataParameterMap)
                    .getAsResponse(new VolleyResponseListener() {
                        @Override
                        public void onVolleyResponse(String response) {
                            setLoading(false);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String status = jsonObject.getString("status");
                                String message = jsonObject.getString("message");
                                if (status.equals("success")) {

                                    JSONObject userDetail = jsonObject.getJSONObject("userDetails");
                                    userInfoBean.setUserId(userDetail.getString("userId"));
                                    userInfoBean.setName(userDetail.getString("name"));
                                    userInfoBean.setEmail(userDetail.getString("email"));
                                    userInfoBean.setProfileImage(userDetail.getString("profileImage"));
                                    userInfoBean.setContactNumber(userDetail.getString("contactNumber"));
                                    userInfoBean.setLocation(userDetail.getString("location"));
                                    userInfoBean.setLatitude(userDetail.getString("latitude"));
                                    userInfoBean.setLongitude(userDetail.getString("longitude"));
                                    userInfoBean.setLanguage(userDetail.getString("language"));
                                    userInfoBean.setDeviceType(userDetail.getString("deviceType"));
                                    userInfoBean.setDeviceToken(userDetail.getString("deviceToken"));
                                    userInfoBean.setSocialId(userDetail.getString("socialId"));
                                    userInfoBean.setSocialType(userDetail.getString("socialType"));
                                    userInfoBean.setAuthToken(userDetail.getString("authToken"));
                                    userInfoBean.setStatus(userDetail.getString("status"));
                                    userInfoBean.setCrd(userDetail.getString("crd"));
                                    userInfoBean.setUpd(userDetail.getString("upd"));

                                    getDataManager().setUserInfo(userInfoBean);
                                    CommonUtils.showToast(getActivity(), message, Toast.LENGTH_SHORT);
                                    getActivity().onBackPressed();
                                } else {
                                    CommonUtils.showToast(getActivity(), message, Toast.LENGTH_SHORT);
                                }
                            } catch (Exception e) {
                                CommonUtils.showToast(getActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                            }
                        }

                        @Override
                        public void onVolleyError(VolleyError error) {
                            setLoading(false);
                            VolleyRequest.volleyErrorHandle(getActivity(), error);
                        }
                    });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case AppConstants.PLACE_AUTOCOMPLETE_REQUEST_CODE:
                    Place place = PlaceAutocomplete.getPlace(getActivity(), data);

                    latitude = place.getLatLng().latitude;
                    longitude = place.getLatLng().longitude;
                    address = String.valueOf(place.getAddress());
                    tvAddress.setText(address);
                    break;

                case AppConstants.REQUEST_GALLERY:
                    try {
                        Uri uri = data.getData();
                        if (uri != null) {
                            CropImage.activity(uri).setCropShape(CropImageView.CropShape.RECTANGLE).setMinCropResultSize(300, 300).setMaxCropResultSize(4000, 4000).setAspectRatio(400, 400).start(getActivity());
                        } else {
                            Toast.makeText(getActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();
                        }
                    } catch (OutOfMemoryError e) {
                        CommonUtils.showToast(getActivity(), getString(R.string.alert_out_of_memory), Toast.LENGTH_SHORT);
                        e.printStackTrace();
                    } catch (Exception e) {
                        Toast.makeText(getActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                    break;

                case AppConstants.REQUEST_TAKE_PHOTO:
                    try {
                        Uri uri = Uri.fromFile(new File(mCurrentPhotoPath));
                        if (uri != null) {
                            // Calling Image Cropper
                            CropImage.activity(uri).setCropShape(CropImageView.CropShape.RECTANGLE).setMinCropResultSize(300, 300).setMaxCropResultSize(4000, 4000).setAspectRatio(400, 400).start(getActivity());
                        } else {
                            Toast.makeText(getActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();
                        }

                    } catch (OutOfMemoryError e) {
                        CommonUtils.showToast(getActivity(), getString(R.string.alert_out_of_memory), Toast.LENGTH_SHORT);
                        e.printStackTrace();
                    } catch (Exception e) {
                        Toast.makeText(getActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                    break;

                case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    if (result != null) {
                        Uri uri = result.getUri();
                        try {
                            productBitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                            imgUser.setImageBitmap(productBitmap);
                            //updateImage("");
                        } catch (Exception e) {
                            Toast.makeText(getActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();
                        } catch (OutOfMemoryError e) {
                            Toast.makeText(getActivity(), getString(R.string.alert_out_of_memory), Toast.LENGTH_SHORT).show();
                        }
                    }
                    break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case AppConstants.REQUEST_MULTIPLE_CAMERA_PERMISSIONS:
                if (grantResults.length > 0) {
                    boolean cameraPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean externalStoragePermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                    if (cameraPermission && externalStoragePermission) {
                        imgUser.callOnClick();
                    } else {
                        CommonUtils.showToast(getActivity(), getString(R.string.permission_camera_deny), Toast.LENGTH_SHORT);
                    }
                }

                break;
        }
    }
}
