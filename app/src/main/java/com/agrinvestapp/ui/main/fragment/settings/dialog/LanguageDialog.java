package com.agrinvestapp.ui.main.fragment.settings.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.db.UserInfoBean;
import com.agrinvestapp.data.model.other.Language;
import com.agrinvestapp.ui.base.BaseDialog;
import com.agrinvestapp.ui.base.ClickListener;
import com.agrinvestapp.ui.main.fragment.settings.adapter.LanguageAdapter;
import com.agrinvestapp.utils.CommonUtils;
import com.agrinvestapp.volley.VolleyJsonObjectListener;
import com.agrinvestapp.volley.VolleyRequest;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LanguageDialog extends BaseDialog implements View.OnClickListener {

    private static final String TAG = LanguageDialog.class.getSimpleName();

    private String selectedLang = "";
    private List<Language> langList;
    private LanguageCallback callback;

    public static LanguageDialog newInstance(LanguageCallback callback) {
        Bundle args = new Bundle();

        LanguageDialog fragment = new LanguageDialog();
        fragment.setOnClick(callback);
        fragment.setArguments(args);
        return fragment;
    }

    private List<Language> getList() {
        langList = new ArrayList<>();

        Language language = new Language();
        language.langName = "English";
        language.isSelected = false;
        language.localisationTitle = getString(R.string.english);
        langList.add(language);

        language = new Language();
        language.langName = "Spanish";
        language.isSelected = false;
        language.localisationTitle = getString(R.string.spanish);
        langList.add(language);

        language = new Language();
        language.langName = "Portuguese";
        language.isSelected = false;
        language.localisationTitle = getString(R.string.portuguese);
        langList.add(language);

        language = new Language();
        language.langName = "French";
        language.isSelected = false;
        language.localisationTitle = getString(R.string.french);
        langList.add(language);

        language = new Language();
        language.langName = "German";
        language.isSelected = false;
        language.localisationTitle = getString(R.string.german);
        langList.add(language);

        return langList;
    }

    private void setOnClick(LanguageCallback callback) {
        this.callback = callback;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_language, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.btnCancel).setOnClickListener(this);
        view.findViewById(R.id.btnDone).setOnClickListener(this);
        initLangRecycler(view);
    }

    private void initLangRecycler(View view) {
        RecyclerView rvChangeLang = view.findViewById(R.id.rvChangeLang);
        LanguageAdapter adapter = new LanguageAdapter(getList(), new ClickListener() {
            @Override
            public void onItemClick(int pos) {
                selectedLang = langList.get(pos).langName.toLowerCase();
            }

            @Override
            public void onEditClick(int pos) {
//not usable
            }

            @Override
            public void onDeleteClick(int pos) {
//not usable
            }
        });

        selectedLang = getDataManager().getUserInfo().getLanguage().toLowerCase();
        switch (selectedLang) {
            case "english":
                adapter.lastPos = 0;
                langList.get(0).isSelected = true;
                break;

            case "spanish":
                adapter.lastPos = 1;
                langList.get(1).isSelected = true;
                break;

            case "portuguese":
                adapter.lastPos = 2;
                langList.get(2).isSelected = true;
                break;

            case "french":
                adapter.lastPos = 3;
                langList.get(3).isSelected = true;
                break;

            case "german":
                adapter.lastPos = 4;
                langList.get(4).isSelected = true;
                break;
        }
        rvChangeLang.setAdapter(adapter);
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    public void onClick(View v) {
        hideKeyboard();
        switch (v.getId()) {
            case R.id.btnDone:
                if (isNetworkConnected(false)) {
                    if (verifyInput()) {
                        doChangeLangApiCall();
                    } else dismissDialog(TAG);
                } else
                    CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_internet), Toast.LENGTH_SHORT);
                break;

            case R.id.btnCancel:
                dismissDialog(TAG);
                break;

        }
    }

    private Boolean verifyInput() {
        return !selectedLang.equals(getDataManager().getUserInfo().getLanguage().toLowerCase());
    }

    private void doChangeLangApiCall() {
        setLoading(true);

        HashMap<String, String> mParameterMap = new HashMap<>();
        mParameterMap.put("language", selectedLang);

        getDataManager().doServerChangeLangApiCall(getDataManager().getHeader(), mParameterMap)
                .getAsJsonObject(new VolleyJsonObjectListener() {
                    @Override
                    public void onVolleyJsonObject(JSONObject jsonObject) {
                        setLoading(false);

                        try {
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            if (status.equals("success")) {
                                UserInfoBean userInfoBean = getDataManager().getUserInfo();
                                userInfoBean.setLanguage(selectedLang);
                                getDataManager().setUserInfo(userInfoBean);
                                getBaseActivity().updateLanguage(getDataManager().getUserInfo().getLanguage());
                                CommonUtils.showToast(getBaseActivity(), getString(R.string.langChangeMsg), Toast.LENGTH_SHORT);
                                callback.onDoneClick();
                            } else {
                                CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                            }
                        } catch (Exception e) {
                            CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                        dismissDialog(TAG);
                    }

                    @Override
                    public void onVolleyJsonException() {
                        setLoading(false);
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        dismissDialog(TAG);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                        dismissDialog(TAG);
                    }
                });

    }

}
