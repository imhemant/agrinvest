package com.agrinvestapp.ui.main.fragment.settings;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.agrinvestapp.R;
import com.agrinvestapp.ui.base.BaseFragment;
import com.agrinvestapp.ui.main.MainActivity;
import com.agrinvestapp.ui.main.fragment.settings.dialog.LanguageDialog;
import com.agrinvestapp.utils.AppConstants;

public class SettingsFragment extends BaseFragment implements View.OnClickListener {

    public static SettingsFragment newInstance() {

        Bundle args = new Bundle();

        SettingsFragment fragment = new SettingsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        view.findViewById(R.id.rlTNC).setOnClickListener(this);
        view.findViewById(R.id.rlPrivacyPolicy).setOnClickListener(this);
        view.findViewById(R.id.rlChangeLangauge).setOnClickListener(this);
        view.findViewById(R.id.rlAbout).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.rlTNC:
                Intent intent = new Intent(getBaseActivity(), ContentActivity.class);
                intent.putExtra(AppConstants.KEY_FROM_TNC, "");
                startActivity(intent);
                break;

            case R.id.rlPrivacyPolicy:
                intent = new Intent(getBaseActivity(), ContentActivity.class);
                intent.putExtra(AppConstants.KEY_FROM_PP, "");
                startActivity(intent);
                break;

            case R.id.rlChangeLangauge:
                LanguageDialog.newInstance(this::reloadFragment).show(getChildFragmentManager());
                break;

            case R.id.rlAbout:
                intent = new Intent(getBaseActivity(), ContentActivity.class);
                intent.putExtra(AppConstants.KEY_FROM_ABOUT_US, "");
                startActivity(intent);
                break;
        }
    }

    private void reloadFragment() {
        assert getFragmentManager() != null;
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.detach(this).attach(this).commit();

        if (getBaseActivity() instanceof MainActivity) {
            MainActivity activity = (MainActivity) getBaseActivity();
            activity.tvTitle.setText(R.string.settings);
        }
    }
}
