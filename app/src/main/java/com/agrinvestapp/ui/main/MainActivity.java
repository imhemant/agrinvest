package com.agrinvestapp.ui.main;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.GetMasterSyncResponse;
import com.agrinvestapp.data.model.db.Animal;
import com.agrinvestapp.data.model.db.Calving;
import com.agrinvestapp.data.model.db.Crop;
import com.agrinvestapp.data.model.db.CropTreatment;
import com.agrinvestapp.data.model.db.CropTreatmentMapping;
import com.agrinvestapp.data.model.db.Expense;
import com.agrinvestapp.data.model.db.ExpenseMapping;
import com.agrinvestapp.data.model.db.ExpenseProductMapping;
import com.agrinvestapp.data.model.db.Insemination;
import com.agrinvestapp.data.model.db.Parcel;
import com.agrinvestapp.data.model.db.ParcelMovement;
import com.agrinvestapp.data.model.db.PregnancyTest;
import com.agrinvestapp.data.model.db.Product;
import com.agrinvestapp.data.model.db.Property;
import com.agrinvestapp.data.model.db.RainData;
import com.agrinvestapp.data.model.db.Treatment;
import com.agrinvestapp.data.model.db.TreatmentMapping;
import com.agrinvestapp.data.model.db.UserInfoBean;
import com.agrinvestapp.data.model.db.Weight;
import com.agrinvestapp.ui.base.BaseActivity;
import com.agrinvestapp.ui.main.dialog.LogoutCallback;
import com.agrinvestapp.ui.main.dialog.LogoutDialog;
import com.agrinvestapp.ui.main.fragment.DashboardFragment;
import com.agrinvestapp.ui.main.fragment.profile.MyProfileFragment;
import com.agrinvestapp.ui.main.fragment.settings.SettingsFragment;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.AppLogger;
import com.agrinvestapp.utils.AppUtils;
import com.agrinvestapp.utils.CalenderUtils;
import com.agrinvestapp.utils.CommonUtils;
import com.agrinvestapp.volley.VolleyRequest;
import com.agrinvestapp.volley.VolleyResponseListener;
import com.android.volley.VolleyError;
import com.google.android.gms.ads.MobileAds;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity implements TabLayout.BaseOnTabSelectedListener {

    public TextView tvTitle;
    private TabLayout tabLayout;
    private byte lastPos = 0;

    private UserInfoBean userInfoBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        userInfoBean = getDataManager().getUserInfo();
        updateLanguage(userInfoBean.getLanguage());
        AppUtils.showDebugDBAddressLogToast();
        initView();
        MobileAds.initialize(this, getString(R.string.app_ad_id));
        if (getIntent().getExtras() != null) {
            Boolean isTrue = getIntent().getBooleanExtra(AppConstants.FROM_SYNC_DIALOG, false);

            //Close all activity and sync data
            if (!userInfoBean.getUserType().equals(AppConstants.TYPE_BUSINESS)) {
                if (isTrue) {
                    doSyncAppDB(true, flag -> {
                        if (flag.equals(AppConstants.ONLINE)) {
                            getMasterServerSync();
                        }
                    });
                }
            }
        } else syncData();
    }

    private void syncData() {
        if (isNetworkConnected(true)) {
            if (!userInfoBean.getUserType().equals(AppConstants.TYPE_BUSINESS)) {
                doSyncAppDB(false, flag -> {
                    if (flag.equals(AppConstants.ONLINE)) {
                        getMasterServerSync();
                    }
                });
            }
        }
    }

    private void initView() {
        tabLayout = findViewById(R.id.tabs);
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_home));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_user));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_settings));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_logout));

        tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.dashboard);
        tabLayout.getTabAt(0).getIcon().setColorFilter(ContextCompat.getColor(this, R.color.colorPrimaryDark), android.graphics.PorterDuff.Mode.SRC_IN);
        replaceFragment(new DashboardFragment(), R.id.mainFrame);

        tabLayout.addOnTabSelectedListener(this);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        switch (tab.getPosition()) {
            case 0:
                tvTitle.setText(R.string.dashboard);
                tabLayout.getTabAt(0).getIcon().setColorFilter(ContextCompat.getColor(this, R.color.colorPrimaryDark), android.graphics.PorterDuff.Mode.SRC_IN);
                replaceFragment(DashboardFragment.newInstance(), R.id.mainFrame);
                lastPos = 0;
                break;

            case 1:
                tvTitle.setText(R.string.profile);
                tabLayout.getTabAt(1).getIcon().setColorFilter(ContextCompat.getColor(this, R.color.colorPrimaryDark), android.graphics.PorterDuff.Mode.SRC_IN);
                replaceFragment(MyProfileFragment.newInstance(), R.id.mainFrame);
                lastPos = 1;
                break;

            case 2:
                tvTitle.setText(R.string.settings);
                tabLayout.getTabAt(2).getIcon().setColorFilter(ContextCompat.getColor(this, R.color.colorPrimaryDark), android.graphics.PorterDuff.Mode.SRC_IN);
                replaceFragment(SettingsFragment.newInstance(), R.id.mainFrame);
                lastPos = 2;
                break;

            case 3:
                //logout
                tabLayout.getTabAt(3).getIcon().setColorFilter(ContextCompat.getColor(this, R.color.colorPrimaryDark), android.graphics.PorterDuff.Mode.SRC_IN);
                LogoutDialog.newInstance(new LogoutCallback() {
                    @Override
                    public void onLogout() {
                        setLoading(true);
                        if (isNetworkConnected(false)) {
                            doLogout();
                        } else {
                            setLoading(false);
                            tabLayout.getTabAt(lastPos).select();
                            CommonUtils.showToast(getActivity(), getString(R.string.alert_internet), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onSync() {
                        if (isNetworkConnected(false)) {
                            tabLayout.getTabAt(0).select();  //redirect to dashboard
                            setLoading(true);
                            if (!userInfoBean.getUserType().equals(AppConstants.TYPE_BUSINESS)) {
                                doSyncAppDB(true, flag -> {
                                    if (flag.equals(AppConstants.ONLINE)) {
                                        getMasterServerSync();
                                    } else setLoading(false);
                                });
                            }

                        } else {
                            tabLayout.getTabAt(lastPos).select();
                            CommonUtils.showToast(getActivity(), getString(R.string.alert_internet), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onCancel() {
                        tabLayout.getTabAt(lastPos).select();
                    }
                }).show(getSupportFragmentManager());
                break;
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        switch (tab.getPosition()) {
            case 0:
                tabLayout.getTabAt(0).getIcon().setColorFilter(ContextCompat.getColor(this, R.color.black), android.graphics.PorterDuff.Mode.SRC_IN);
                break;

            case 1:
                tabLayout.getTabAt(1).getIcon().setColorFilter(ContextCompat.getColor(this, R.color.black), android.graphics.PorterDuff.Mode.SRC_IN);
                break;

            case 2:
                tabLayout.getTabAt(2).getIcon().setColorFilter(ContextCompat.getColor(this, R.color.black), android.graphics.PorterDuff.Mode.SRC_IN);
                break;

            case 3:
                tabLayout.getTabAt(3).getIcon().setColorFilter(ContextCompat.getColor(this, R.color.black), android.graphics.PorterDuff.Mode.SRC_IN);
                break;
        }
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        Fragment fragment = getCurrentFragment();
        if (fragment != null && fragment instanceof MyProfileFragment) {
            MyProfileFragment profileFragment = (MyProfileFragment) fragment;
            profileFragment.updateUI();
        }
    }

    private void getMasterServerSync() {
        setSyncLoading();
        getDataManager().doServerGetMasterSyncApiCall(getDataManager().getHeader())
                .getAsResponse(new VolleyResponseListener() {
                    @Override
                    public void onVolleyResponse(String response) {
                        setLoading(false);
                        GetMasterSyncResponse getMasterSyncResponse = getDataManager().mGson.fromJson(response, GetMasterSyncResponse.class);

                        if (getMasterSyncResponse.getStatus().equals("success")) {
                            updateDB(getMasterSyncResponse);
                        }
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getActivity(), error);
                    }
                });
    }

    private void updateDB(GetMasterSyncResponse getMasterSyncResponse) {
        new Thread(() -> {
            getDataManager().deleteAllProperty();
            getDataManager().deleteAllProduct();
            getDataManager().deleteAllExpense();

            AppLogger.d("database", "property, product and expense table clear success");

            GetMasterSyncResponse.MasterDataBean masterDataBeans = getMasterSyncResponse.getMasterData();

            List<Property> propertyList = new ArrayList<>();
            List<RainData> rainDataList = new ArrayList<>();
            List<Parcel> parcelList = new ArrayList<>();

            List<Animal> animalList = new ArrayList<>();
            List<Treatment> treatmentList = new ArrayList<>();
            List<TreatmentMapping> treatmentMappingList = new ArrayList<>();
            List<Insemination> inseminationList = new ArrayList<>();
            List<PregnancyTest> pregnancyTestList = new ArrayList<>();
            List<ParcelMovement> parcelMovementList = new ArrayList<>();
            List<Calving> calvingList = new ArrayList<>();
            List<Weight> weightList = new ArrayList<>();
            List<Crop> cropList = new ArrayList<>();
            List<CropTreatment> cropTreatmentList = new ArrayList<>();
            List<CropTreatmentMapping> cropTreatmentMappingList = new ArrayList<>();
            List<Product> ProductList = new ArrayList<>();
            List<Expense> expenseList = new ArrayList<>();
            List<ExpenseMapping> expenseMappingList = new ArrayList<>();
            List<ExpenseProductMapping> productMappingList = new ArrayList<>();

            if (masterDataBeans.getPropertyList() != null) {
                for (GetMasterSyncResponse.MasterDataBean.PropertyListBean property : masterDataBeans.getPropertyList()) {

                    Property propertyBean = new Property();
                    propertyBean.setPropertyId(property.getPropertyId());
                    propertyBean.setPropertyName(property.getPropertyName());
                    propertyBean.setUser_id(property.getUser_id());
                    propertyBean.setLocation(property.getLocation());
                    propertyBean.setLongitude(property.getLongitude());
                    propertyBean.setLatitude(property.getLatitude());
                    propertyBean.setSize(property.getSize());
                    propertyBean.setSizeUnite(property.getSizeUnite());
                    propertyBean.setDescription(property.getDescription());
                    propertyBean.setCrd(property.getCrd());
                    propertyBean.setUpd(property.getUpd());
                    propertyBean.setStatus(property.getStatus());
                    propertyBean.setEvent(AppConstants.DB_EVENT_ADD);
                    propertyBean.setDataSync(AppConstants.DB_SYNC_TRUE);
                    propertyList.add(propertyBean);

                    if (property.getRaindataList() != null) {
                        for (GetMasterSyncResponse.MasterDataBean.PropertyListBean.RaindataListBean raindataListBean : property.getRaindataList()) {
                            RainData rainBean = new RainData();
                            rainBean.setRaindataId(raindataListBean.getRaindataId());
                            rainBean.setProperty_id(raindataListBean.getProperty_id());
                            rainBean.setFromDate(raindataListBean.getFromDate());
                            rainBean.setToDate(raindataListBean.getToDate());
                            rainBean.setQuantity(raindataListBean.getQuantity());
                            rainBean.setQuantityUnite(raindataListBean.getQuantityUnite());
                            rainBean.setCrd(raindataListBean.getCrd());
                            rainBean.setUpd(raindataListBean.getUpd());
                            rainBean.setStatus(raindataListBean.getStatus());
                            rainBean.setEvent(AppConstants.DB_EVENT_ADD);
                            rainBean.setDataSync(AppConstants.DB_SYNC_TRUE);
                            rainDataList.add(rainBean);
                        }
                    }

                    if (property.getParcelList() != null) {
                        for (GetMasterSyncResponse.MasterDataBean.PropertyListBean.ParcelListBean parcelListBean : property.getParcelList()) {
                            Parcel parcel = new Parcel();
                            parcel.setProperty_id(parcelListBean.getProperty_id());
                            parcel.setParcelId(parcelListBean.getParcelId());
                            parcel.setParcelName(parcelListBean.getParcelName());
                            parcel.setRecordKey(parcelListBean.getRecordKey());
                            parcel.setCrd(parcelListBean.getCrd());
                            parcel.setUpd(parcelListBean.getUpd());
                            parcel.setStatus(parcelListBean.getStatus());
                            parcel.setEvent(AppConstants.DB_EVENT_ADD);
                            parcel.setDataSync(AppConstants.DB_SYNC_TRUE);
                            parcelList.add(parcel);

                            if (parcelListBean.getAnimalList() != null) {
                                for (GetMasterSyncResponse.MasterDataBean.PropertyListBean.ParcelListBean.AnimalListBean animalListBean : parcelListBean.getAnimalList()) {

                                    Animal animal = new Animal();
                                    animal.setAnimalId(animalListBean.getAnimalId());
                                    animal.setParcel_id(animalListBean.getParcel_id());
                                    animal.setLotId(animalListBean.getLotId());
                                    animal.setTagNumber(animalListBean.getTagNumber());
                                    animal.setBornOrBuyDate(animalListBean.getBornOrBuyDate());
                                    animal.setMother(animalListBean.getMother());
                                    animal.setBoughtFrom(animalListBean.getBoughtFrom());
                                    animal.setDescription(animalListBean.getDescription());
                                    animal.setDismant(animalListBean.getDismant());
                                    animal.setSaleDate(animalListBean.getSaleDate().isEmpty() ? null : CalenderUtils.getDateFormat(animalListBean.getSaleDate().contains("/") ? CalenderUtils.formatDate(animalListBean.getSaleDate(), AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT) : animalListBean.getSaleDate(), AppConstants.SERVER_TIMESTAMP_FORMAT));
                                    animal.setMarkingOrTagging(animalListBean.getMarkingOrTagging());
                                    animal.setSalePrice(animalListBean.getSalePrice());
                                    animal.setSaleQuantity(animalListBean.getSaleQuantity());
                                    animal.setSaleTotalPrice(animalListBean.getSaleTotalPrice());
                                    animal.setCostFrom(animalListBean.getCostFrom());
                                    animal.setCostTo(animalListBean.getCostTo());
                                    animal.setCostTotal(animalListBean.getCostTotal());
                                    animal.setStatus(animalListBean.getStatus());
                                    animal.setRecordKey(animalListBean.getRecordKey());
                                    animal.setCrd(animalListBean.getCrd());
                                    animal.setUpd(animalListBean.getUpd());
                                    animal.setEvent(AppConstants.DB_EVENT_ADD);
                                    animal.setDataSync(AppConstants.DB_SYNC_TRUE);
                                    animalList.add(animal);

                                    if (animalListBean.getTreatmentList() != null) {
                                        for (GetMasterSyncResponse.MasterDataBean.PropertyListBean.ParcelListBean.AnimalListBean.TreatmentListBean treatmentBean : animalListBean.getTreatmentList()) {
                                            Treatment treatment = new Treatment();

                                            treatment.setTreatmentId(treatmentBean.getTreatmentId());
                                            treatment.setTreatmentName(treatmentBean.getTreatmentName());
                                            treatment.setTreatmentForId(treatmentBean.getTreatmentForId());
                                            treatment.setTreatmentFor(treatmentBean.getTreatmentFor());
                                            treatment.setRecordKey(treatmentBean.getRecordKey());
                                            treatment.setCrd(treatmentBean.getCrd());
                                            treatment.setUpd(treatmentBean.getUpd());
                                            treatment.setDataSync(AppConstants.DB_SYNC_TRUE);
                                            treatment.setEvent(AppConstants.DB_EVENT_ADD);

                                            treatmentList.add(treatment);

                                            if (treatmentBean.getMappingList() != null) {
                                                for (GetMasterSyncResponse.MasterDataBean.PropertyListBean.ParcelListBean.AnimalListBean.TreatmentListBean.MappingListBean mappingListBean : treatmentBean.getMappingList()) {
                                                    TreatmentMapping treatmentMapping = new TreatmentMapping();

                                                    treatmentMapping.setTreatmentMappingId(mappingListBean.getTreatmentMappingId());
                                                    treatmentMapping.setTreatment_id(mappingListBean.getTreatment_id());
                                                    treatmentMapping.setTreatmentDate(mappingListBean.getTreatmentDate());
                                                    treatmentMapping.setTreatmentFor(treatmentBean.getTreatmentFor());
                                                    treatmentMapping.setTreatmentDescription(mappingListBean.getTreatmentDescription());
                                                    treatmentMapping.setCrd(mappingListBean.getCrd());
                                                    treatmentMapping.setUpd(mappingListBean.getUpd());
                                                    treatmentMapping.setDataSync(AppConstants.DB_SYNC_TRUE);
                                                    treatmentMapping.setEvent(AppConstants.DB_EVENT_ADD);

                                                    treatmentMappingList.add(treatmentMapping);
                                                }
                                            }
                                        }
                                    }

                                    if (animalListBean.getInseminationList() != null) {
                                        for (GetMasterSyncResponse.MasterDataBean.PropertyListBean.ParcelListBean.AnimalListBean.InseminationListBean inseminationBean : animalListBean.getInseminationList()) {
                                            Insemination insemination = new Insemination();
                                            insemination.setInseminationId(inseminationBean.getInseminationId());
                                            insemination.setAnimal_id(inseminationBean.getAnimal_id());
                                            insemination.setInseminationName(inseminationBean.getInseminationName());
                                            insemination.setInseminationDate(inseminationBean.getInseminationDate());
                                            insemination.setInseminationDescription(inseminationBean.getInseminationDescription());
                                            insemination.setRecordKey(inseminationBean.getRecordKey());
                                            insemination.setCrd(inseminationBean.getCrd());
                                            insemination.setUpd(inseminationBean.getUpd());
                                            insemination.setDataSync(AppConstants.DB_SYNC_TRUE);
                                            insemination.setEvent(AppConstants.DB_EVENT_ADD);

                                            inseminationList.add(insemination);
                                        }
                                    }

                                    if (animalListBean.getPregnancyTestList() != null) {
                                        for (GetMasterSyncResponse.MasterDataBean.PropertyListBean.ParcelListBean.AnimalListBean.PregnancyTestListBean pregnancyBean : animalListBean.getPregnancyTestList()) {
                                            PregnancyTest pregnancyTest = new PregnancyTest();

                                            pregnancyTest.setPregnancyId(pregnancyBean.getPregnancyId());
                                            pregnancyTest.setAnimal_id(pregnancyBean.getAnimal_id());
                                            pregnancyTest.setPregnancyName(pregnancyBean.getPregnancyName());
                                            pregnancyTest.setPregnancyDate(pregnancyBean.getPregnancyDate());
                                            pregnancyTest.setPregnancyDescription(pregnancyBean.getPregnancyDescription());
                                            pregnancyTest.setRecordKey(pregnancyBean.getRecordKey());
                                            pregnancyTest.setCrd(pregnancyBean.getCrd());
                                            pregnancyTest.setUpd(pregnancyBean.getUpd());
                                            pregnancyTest.setDataSync(AppConstants.DB_SYNC_TRUE);
                                            pregnancyTest.setEvent(AppConstants.DB_EVENT_ADD);

                                            pregnancyTestList.add(pregnancyTest);
                                        }
                                    }

                                    if (animalListBean.getParcelMovementList() != null) {
                                        for (GetMasterSyncResponse.MasterDataBean.PropertyListBean.ParcelListBean.AnimalListBean.ParcelMovementListBean parcelMovementBean : animalListBean.getParcelMovementList()) {
                                            ParcelMovement parcelMovement = new ParcelMovement();

                                            parcelMovement.setParcelMovementId(parcelMovementBean.getParcelMovementId());
                                            parcelMovement.setAnimal_id(parcelMovementBean.getAnimal_id());
                                            parcelMovement.setMovementFrom(parcelMovementBean.getMovementFrom());
                                            parcelMovement.setMovementTo(parcelMovementBean.getMovementTo());
                                            parcelMovement.setMovementDate(parcelMovementBean.getMovementDate());
                                            parcelMovement.setRecordKey(parcelMovementBean.getRecordKey());
                                            parcelMovement.setCrd(parcelMovementBean.getCrd());
                                            parcelMovement.setUpd(parcelMovementBean.getUpd());
                                            parcelMovement.setDataSync(AppConstants.DB_SYNC_TRUE);
                                            parcelMovement.setEvent(AppConstants.DB_EVENT_ADD);

                                            parcelMovementList.add(parcelMovement);
                                        }
                                    }

                                    if (animalListBean.getCalvingList() != null) {
                                        for (GetMasterSyncResponse.MasterDataBean.PropertyListBean.ParcelListBean.AnimalListBean.CalvingListBean calvingBean : animalListBean.getCalvingList()) {
                                            Calving calving = new Calving();

                                            calving.setCalvingId(calvingBean.getCalvingId());
                                            calving.setAnimal_id(calvingBean.getAnimal_id());
                                            calving.setCalvingNumber(calvingBean.getCalvingNumber());
                                            calving.setCalvingDate(calvingBean.getCalvingDate());
                                            calving.setRecordKey(calvingBean.getRecordKey());
                                            calving.setCrd(calvingBean.getCrd());
                                            calving.setUpd(calvingBean.getUpd());
                                            calving.setDataSync(AppConstants.DB_SYNC_TRUE);
                                            calving.setEvent(AppConstants.DB_EVENT_ADD);

                                            calvingList.add(calving);
                                        }
                                    }

                                    if (animalListBean.getWeightList() != null) {
                                        for (GetMasterSyncResponse.MasterDataBean.PropertyListBean.ParcelListBean.AnimalListBean.WeightListBean weightBean : animalListBean.getWeightList()) {
                                            Weight weight = new Weight();

                                            weight.setWeightId(weightBean.getWeightId());
                                            weight.setAnimal_id(weightBean.getAnimal_id());
                                            weight.setWeight(weightBean.getWeight());
                                            weight.setWeightUnit(weightBean.getWeightUnit());
                                            weight.setWeightDate(weightBean.getWeightDate());
                                            weight.setRecordKey(weightBean.getRecordKey());
                                            weight.setCrd(weightBean.getCrd());
                                            weight.setUpd(weightBean.getUpd());
                                            weight.setDataSync(AppConstants.DB_SYNC_TRUE);
                                            weight.setEvent(AppConstants.DB_EVENT_ADD);

                                            weightList.add(weight);
                                        }
                                    }
                                }
                            }

                            if (parcelListBean.getCropList() != null) {
                                for (GetMasterSyncResponse.MasterDataBean.PropertyListBean.ParcelListBean.CropListBean cropListBean : parcelListBean.getCropList()) {
                                    Crop crop = new Crop();

                                    crop.setCropId(cropListBean.getCropId());
                                    crop.setParcel_id(cropListBean.getParcel_id());
                                    crop.setCropName(cropListBean.getCropName());
                                    crop.setLotId(cropListBean.getLotId());
                                    crop.setDescription(cropListBean.getDescription());
                                    crop.setPreparationDate(cropListBean.getPreparationDate());
                                    crop.setPlantingDate(cropListBean.getPlantingDate());
                                    crop.setHarvestDate(cropListBean.getHarvestDate());
                                    crop.setSaleDate(cropListBean.getSaleDate().isEmpty() ? null : CalenderUtils.getDateFormat(cropListBean.getSaleDate().contains("/") ? CalenderUtils.formatDate(cropListBean.getSaleDate(), AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT) : cropListBean.getSaleDate(), AppConstants.SERVER_TIMESTAMP_FORMAT));
                                    crop.setSalePrice(cropListBean.getSalePrice());
                                    crop.setSaleQuantity(cropListBean.getSaleQuantity());
                                    crop.setSaleTotalPrice(cropListBean.getSaleTotalPrice());
                                    crop.setCostFrom(cropListBean.getCostFrom());
                                    crop.setCostTo(cropListBean.getCostTo());
                                    crop.setCostTotal(cropListBean.getCostTotal());
                                    crop.setStatus(cropListBean.getStatus());
                                    crop.setRecordKey(cropListBean.getRecordKey());
                                    crop.setCrd(cropListBean.getCrd());
                                    crop.setUpd(cropListBean.getUpd());
                                    crop.setEvent(AppConstants.DB_EVENT_ADD);
                                    crop.setDataSync(AppConstants.DB_SYNC_TRUE);
                                    cropList.add(crop);

                                    if (cropListBean.getTreatmentList() != null) {
                                        for (GetMasterSyncResponse.MasterDataBean.PropertyListBean.ParcelListBean.CropListBean.TreatmentListBean cropTreatment : cropListBean.getTreatmentList()) {
                                            CropTreatment cropTreatmentBean = new CropTreatment();

                                            cropTreatmentBean.setTreatmentId(cropTreatment.getTreatmentId());
                                            cropTreatmentBean.setCrop_id(cropTreatment.getCrop_id());
                                            cropTreatmentBean.setTreatmentName(cropTreatment.getTreatmentName());
                                            cropTreatmentBean.setRecordKey(cropTreatment.getRecordKey());
                                            cropTreatmentBean.setCrd(cropTreatment.getCrd());
                                            cropTreatmentBean.setUpd(cropTreatment.getUpd());
                                            cropTreatmentBean.setEvent(AppConstants.DB_EVENT_ADD);
                                            cropTreatmentBean.setDataSync(AppConstants.DB_SYNC_TRUE);
                                            cropTreatmentList.add(cropTreatmentBean);

                                            if (cropTreatment.getMappingList() != null) {
                                                for (GetMasterSyncResponse.MasterDataBean.PropertyListBean.ParcelListBean.CropListBean.TreatmentListBean.MappingListBean cropMappingBean : cropTreatment.getMappingList()) {
                                                    CropTreatmentMapping cropTreatmentMapping = new CropTreatmentMapping();

                                                    cropTreatmentMapping.setTreatmentMappingId(cropMappingBean.getTreatmentMappingId());
                                                    cropTreatmentMapping.setTreatment_id(cropMappingBean.getTreatment_id());
                                                    cropTreatmentMapping.setTreatmentDate(cropMappingBean.getTreatmentDate());
                                                    cropTreatmentMapping.setTreatmentDescription(cropMappingBean.getTreatmentDescription());
                                                    cropTreatmentMapping.setRecordKey(cropMappingBean.getRecordKey());
                                                    cropTreatmentMapping.setCrd(cropMappingBean.getCrd());
                                                    cropTreatmentMapping.setUpd(cropMappingBean.getUpd());
                                                    cropTreatmentMapping.setEvent(AppConstants.DB_EVENT_ADD);
                                                    cropTreatmentMapping.setDataSync(AppConstants.DB_SYNC_TRUE);

                                                    cropTreatmentMappingList.add(cropTreatmentMapping);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (masterDataBeans.getProductList() != null) {
                for (GetMasterSyncResponse.MasterDataBean.ProductListBean productList : masterDataBeans.getProductList()) {
                    Product product = new Product();

                    product.setProductId(productList.getProductId());
                    product.setProductName(productList.getProductName());
                    product.setProductImage(productList.getProductImage());
                    product.setPrice(Float.parseFloat(productList.getPrice()));
                    product.setUnit(productList.getUnit());
                    product.setQuantity(productList.getQuantity());
                    product.setDescription(productList.getDescription());
                    product.setRecordKey(productList.getRecordKey());
                    product.setUser_id(productList.getUser_id());
                    product.setUserType(productList.getUserType());
                    product.setStatus(productList.getStatus());
                    product.setCrd(productList.getCrd());
                    product.setUpd(productList.getUpd());
                    product.setProductImageThumb(productList.getProductImageThumb());
                    product.setEvent(AppConstants.DB_EVENT_ADD);
                    product.setDataSync(AppConstants.DB_SYNC_TRUE);
                    ProductList.add(product);
                }
            }

            if (masterDataBeans.getExpenseList() != null) {
                for (GetMasterSyncResponse.MasterDataBean.ExpenseListBean expenseListBean : masterDataBeans.getExpenseList()) {
                    Expense expense = new Expense();

                    expense.setExpenseId(expenseListBean.getExpenseId());
                    expense.setUser_id(expenseListBean.getUser_id());
                    expense.setDateBought(expenseListBean.getDateBought());
                    expense.setCost(expenseListBean.getCost());
                    expense.setDescription(expenseListBean.getDescription());
                    expense.setDateApplied(expenseListBean.getDateApplied().isEmpty() ? null : CalenderUtils.getDateFormat(expenseListBean.getDateApplied().contains("/") ? CalenderUtils.formatDate(expenseListBean.getDateApplied(), AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT) : expenseListBean.getDateApplied(), AppConstants.SERVER_TIMESTAMP_FORMAT));
                    expense.setExpenseFor(expenseListBean.getExpenseFor());
                    expense.setStatus(expenseListBean.getStatus());
                    expense.setRecordKey(expenseListBean.getRecordKey());
                    expense.setCrd(expenseListBean.getCrd());
                    expense.setUpd(expenseListBean.getUpd());
                    expense.setEvent(AppConstants.DB_EVENT_ADD);
                    expense.setDataSync(AppConstants.DB_SYNC_TRUE);
                    expenseList.add(expense);

                    if (expenseListBean.getAppliedOn() != null) {
                        for (GetMasterSyncResponse.MasterDataBean.ExpenseListBean.AppliedOnBean applyOnBean : expenseListBean.getAppliedOn()) {
                            ExpenseMapping expenseMapping = new ExpenseMapping();

                            expenseMapping.setExpenseMappingId(applyOnBean.getExpenseMappingId());
                            expenseMapping.setExpense_id(applyOnBean.getExpense_id());
                            expenseMapping.setAppliedOn(applyOnBean.getAppliedOn());
                            expenseMapping.setAppliedOnId(applyOnBean.getAppliedOnRecordKey());
                            expenseMapping.setStatus(applyOnBean.getStatus());
                            expenseMapping.setCrd(applyOnBean.getCrd());
                            expenseMapping.setUpd(applyOnBean.getUpd());
                            expenseMapping.setEvent(AppConstants.DB_EVENT_ADD);
                            expenseMapping.setDataSync(AppConstants.DB_SYNC_TRUE);
                            expenseMappingList.add(expenseMapping);
                        }
                    }

                    if (expenseListBean.getProductDetails() != null) {
                        for (GetMasterSyncResponse.MasterDataBean.ExpenseListBean.ProductDetailsBean productDetailsBean : expenseListBean.getProductDetails()) {
                            ExpenseProductMapping expenseProductMapping = new ExpenseProductMapping();

                            expenseProductMapping.setExpenseProductMappingId(productDetailsBean.getExpenseProductMappingId());
                            expenseProductMapping.setExpense_id(productDetailsBean.getExpense_id());
                            expenseProductMapping.setProduct_id(productDetailsBean.getProduct_id());
                            expenseProductMapping.setQuantity(productDetailsBean.getQuantity());
                            expenseProductMapping.setPrice(productDetailsBean.getPrice());
                            expenseProductMapping.setStatus(productDetailsBean.getStatus());
                            expenseProductMapping.setCrd(productDetailsBean.getCrd());
                            expenseProductMapping.setUpd(productDetailsBean.getUpd());
                            expenseProductMapping.setEvent(AppConstants.DB_EVENT_ADD);
                            expenseProductMapping.setDataSync(AppConstants.DB_SYNC_TRUE);

                            productMappingList.add(expenseProductMapping);
                        }
                    }
                }
            }

            getDataManager().insertAllProperty(propertyList);
            getDataManager().insertAllRainData(rainDataList);
            getDataManager().insertAllParcel(parcelList);
            getDataManager().insertAllAnimal(animalList);
            getDataManager().insertAllTreatment(treatmentList);
            getDataManager().insertAllTreatmentDetail(treatmentMappingList);
            getDataManager().insertAllInsemination(inseminationList);
            getDataManager().insertAllPregnancyTest(pregnancyTestList);
            getDataManager().insertAllParcelMovement(parcelMovementList);
            getDataManager().insertAllCalving(calvingList);
            getDataManager().insertAllWeight(weightList);
            getDataManager().insertAllCrop(cropList);
            getDataManager().insertAllCropTreatment(cropTreatmentList);
            getDataManager().insertAllCropTreatmentDetail(cropTreatmentMappingList);
            getDataManager().insertAllProduct(ProductList);
            getDataManager().insertAllExpense(expenseList);
            getDataManager().insertAllExpenseMapping(expenseMappingList);
            getDataManager().insertAllExpenseProdMapping(productMappingList);
            AppLogger.d("database", "Server Db Add done");
        }).start();
    }

    private void doLogout() {

        getDataManager().doServerLogoutApiCall(getDataManager().getHeader())
                .getAsResponse(new VolleyResponseListener() {
                    @Override
                    public void onVolleyResponse(String response) {
                        setLoading(false);
                        new Thread(() -> getDataManager().clearAllTable()).start();
                        getDataManager().logout(getActivity());
                        setLoading(false);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getActivity(), error);
                    }
                });
    }
}
