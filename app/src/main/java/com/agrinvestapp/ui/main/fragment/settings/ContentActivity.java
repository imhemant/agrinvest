package com.agrinvestapp.ui.main.fragment.settings;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.ui.base.BaseActivity;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.CommonUtils;
import com.agrinvestapp.volley.VolleyJsonObjectListener;
import com.agrinvestapp.volley.VolleyRequest;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import static com.agrinvestapp.utils.AppConstants.KEY_FROM_ABOUT_US;
import static com.agrinvestapp.utils.AppConstants.KEY_FROM_PP;
import static com.agrinvestapp.utils.AppConstants.KEY_FROM_TNC;

public class ContentActivity extends BaseActivity implements View.OnClickListener {

    private WebView webView;
    private String flag = "", aboutContent = "";
    private LinearLayout llProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);
 /* toolbar view start */
        ImageView imgBack = findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);
        imgBack.setVisibility(View.VISIBLE);

        TextView tvTitle = findViewById(R.id.tvTitle);
        /* toolbar view end */

        webView = findViewById(R.id.webview);

        Intent intent = getIntent();
        assert intent != null;
        if (intent.hasExtra(KEY_FROM_TNC)) {
            flag = "TNC";
            tvTitle.setText(R.string.terms_and_conditions);
        } else if (intent.hasExtra(KEY_FROM_PP)) {
            flag = "PP";
            tvTitle.setText(R.string.privacy_policy);
        } else if (intent.hasExtra(KEY_FROM_ABOUT_US)) {
            flag = "ABOUT_US";
            tvTitle.setText(R.string.about_us);
        }

        initializeView();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initializeView() {
        llProgress = findViewById(R.id.llProgress);
        webView.setWebViewClient(new AppWebViewClients());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.setInitialScale(50);
        webView.getSettings().setDisplayZoomControls(true);

        if (isNetworkConnected(false)) {
            doGetSettingContent();
            /*if (AppConstants.TNC.isEmpty()) {
                doGetSettingContent();
            } else {
                setContentLoading(true);
                updateUI();
            }*/
        } else
            CommonUtils.showToast(getActivity(), getString(R.string.alert_internet), Toast.LENGTH_SHORT);
    }

    private void updateUI() {
        switch (flag) {
            case "TNC":
                //webView.loadUrl("http://docs.google.com/gview?embedded=true&url=" + AppConstants.TNC);
                webView.loadUrl(AppConstants.TNC + getDataManager().getUserInfo().getLanguage().toLowerCase());
                break;

            case "PP":
                // webView.loadUrl("http://docs.google.com/gview?embedded=true&url=" + AppConstants.PRIVACY_POLICY);
                webView.loadUrl(AppConstants.PRIVACY_POLICY + getDataManager().getUserInfo().getLanguage().toLowerCase());
                break;

            case "ABOUT_US":
                setContentLoading(false);
                webView.setVisibility(View.GONE);
                TextView tvAboutContent = findViewById(R.id.tvAboutContent);
                tvAboutContent.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    tvAboutContent.setText(Html.fromHtml(aboutContent, Html.FROM_HTML_MODE_COMPACT));
                } else {
                    tvAboutContent.setText(Html.fromHtml(aboutContent));
                }
                //webView.loadDataWithBaseURL("", aboutContent, "text/html", "UTF-8", "");
                //webView.loadUrl("http://docs.google.com/gview?embedded=true&url=" + AppConstants.ABOUT_US);
                break;

            default:
                setContentLoading(false);
                break;
        }
    }

    private void doGetSettingContent() {
        setContentLoading(true);

        getDataManager().doServerSettingContentApiCall(getDataManager().getHeader())
                .getAsJsonObject(new VolleyJsonObjectListener() {
                    @Override
                    public void onVolleyJsonObject(JSONObject jsonObject) {
                        try {
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            if (status.equals("success")) {
                                JSONObject obj = jsonObject.getJSONObject("Content");

                                //AppConstants.TNC = obj.getString("term_condition");
                                //AppConstants.PRIVACY_POLICY = obj.getString("policy");

                                AppConstants.TNC = obj.getString("terms_url");
                                AppConstants.PRIVACY_POLICY = obj.getString("policy_url");
                                AppConstants.ABOUT_US = obj.getString("aboutus");

                                if (flag.equals("ABOUT_US")) {
                                    switch (getDataManager().getUserInfo().getLanguage().toLowerCase()) {
                                        //English
                                        case "english":
                                            aboutContent = obj.getString("about_us_english");
                                            break;

                                        //Spanish
                                        case "spanish":
                                            aboutContent = obj.getString("about_us_spanish");
                                            break;

                                        //Portuguese
                                        case "portuguese":
                                            aboutContent = obj.getString("about_us_portuguese");
                                            break;

                                        //French
                                        case "french":
                                            aboutContent = obj.getString("about_us_french");
                                            break;

                                        //German
                                        case "german":
                                            aboutContent = obj.getString("about_us_german");
                                            break;
                                    }
                                }

                                updateUI();
                            } else {
                                setContentLoading(false);
                                CommonUtils.showToast(getActivity(), message, Toast.LENGTH_SHORT);
                            }
                        } catch (Exception e) {
                            setContentLoading(false);
                            CommonUtils.showToast(getActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyJsonException() {
                        setContentLoading(false);
                        CommonUtils.showToast(getActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setContentLoading(false);
                        VolleyRequest.volleyErrorHandle(getActivity(), error);
                    }
                });
    }

    private void setContentLoading(Boolean isLoading) {
        llProgress.setVisibility(isLoading ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
        }
    }

    private class AppWebViewClients extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageCommitVisible(WebView view, String url) {
            super.onPageCommitVisible(view, url);
            setContentLoading(false);
        }
    }
}
