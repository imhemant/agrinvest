package com.agrinvestapp.ui.main.fragment.profile.dialog;

/**
 * Created by mindiii on 27/12/18.
 */

public interface SubscriptionCancelCallback {
    void onCancelSubscription();
}
