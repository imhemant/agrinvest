package com.agrinvestapp.ui.main.fragment.settings.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.other.Language;
import com.agrinvestapp.ui.base.ClickListener;

import java.util.List;


/**
 * Created by hemant
 * Date: 17/4/18
 * Time: 4:03 PM
 */

public class LanguageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public int lastPos = -1;
    private List<Language> list;
    private ClickListener clickListener;

    public LanguageAdapter(List<Language> list, ClickListener clickListener) {
        this.list = list;
        this.clickListener = clickListener;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder rvHolder, int position) {
        LanguageAdapter.ViewHolder holder = ((LanguageAdapter.ViewHolder) rvHolder);

        Language bean = list.get(position);

        holder.frameCheck.setVisibility(bean.isSelected ? View.VISIBLE : View.GONE);
        holder.tvName.setText(bean.localisationTitle);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lang, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvName;
        private FrameLayout frameCheck;

        ViewHolder(@NonNull View v) {
            super(v);
            tvName = v.findViewById(R.id.tvName);
            frameCheck = v.findViewById(R.id.frameCheck);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(@NonNull final View view) {
            int tempPos = getAdapterPosition();
            if (tempPos != -1 && clickListener != null) {
                if (lastPos != -1) {
                    list.get(lastPos).isSelected = false;
                    notifyItemChanged(lastPos);
                }

                clickListener.onItemClick(getAdapterPosition());
                list.get(tempPos).isSelected = true;
                notifyItemChanged(tempPos);

                lastPos = tempPos;
            }

        }
    }
}

