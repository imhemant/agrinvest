package com.agrinvestapp.ui.main.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.agrinvestapp.R;
import com.agrinvestapp.ui.base.BaseDialog;


public class LogoutDialog extends BaseDialog implements View.OnClickListener {

    private static final String TAG = LogoutDialog.class.getSimpleName();

    private LogoutCallback callback;

    public static LogoutDialog newInstance(LogoutCallback callback) {

        Bundle args = new Bundle();

        LogoutDialog fragment = new LogoutDialog();
        fragment.setArguments(args);
        fragment.setOnLogoutListener(callback);
        return fragment;
    }

    private void setOnLogoutListener(LogoutCallback callback) {
        this.callback = callback;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_logout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Button btnCancel = view.findViewById(R.id.btnCancel);
        FrameLayout frameCancel = view.findViewById(R.id.frameCancel);
        if (getDataManager().getLocalDataExistForSync()) {
            btnCancel.setVisibility(View.INVISIBLE);
            frameCancel.setVisibility(View.VISIBLE);

            Button btnSync = view.findViewById(R.id.btnSync);
            btnSync.setOnClickListener(this);
            btnSync.setVisibility(View.VISIBLE);
            TextView tvMsg = view.findViewById(R.id.tvMsg);
            tvMsg.setText(R.string.alert_logout_with_sync);
        }

        btnCancel.setOnClickListener(this);
        frameCancel.setOnClickListener(this);
        view.findViewById(R.id.btnLogout).setOnClickListener(this);
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.frameCancel:
            case R.id.btnCancel:
                dismissDialog(TAG);
                callback.onCancel();
                break;

            case R.id.btnSync:
                dismissDialog(TAG);
                callback.onSync();
                break;

            case R.id.btnLogout:
                dismissDialog(TAG);
                callback.onLogout();
                break;
        }
    }

}
