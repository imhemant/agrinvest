package com.agrinvestapp.ui.main.dialog;

public interface LogoutCallback {
    void onLogout();

    void onSync();

    void onCancel();
}
