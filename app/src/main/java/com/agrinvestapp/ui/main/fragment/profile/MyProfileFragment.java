package com.agrinvestapp.ui.main.fragment.profile;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.db.UserInfoBean;
import com.agrinvestapp.ui.base.BaseFragment;
import com.agrinvestapp.ui.main.fragment.profile.dialog.ChangePwdDialog;
import com.agrinvestapp.ui.main.fragment.profile.dialog.PremiumPlanDialog;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.CommonUtils;
import com.agrinvestapp.volley.VolleyRequest;
import com.agrinvestapp.volley.VolleyResponseListener;
import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;


public class MyProfileFragment extends BaseFragment implements View.OnClickListener {

    private TextView tvUserName, tvEmail, tvNum, tvAddress, tvAnimalCount, tvFarmCount, tvPlanType;
    private ImageView imgUser;
    private UserInfoBean userInfoBean;
    private LinearLayout llCountStatus;

    public static MyProfileFragment newInstance() {

        Bundle args = new Bundle();

        MyProfileFragment fragment = new MyProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    public void initView(View view) {
        tvUserName = view.findViewById(R.id.tvUserName);
        tvEmail = view.findViewById(R.id.tvEmail);
        tvNum = view.findViewById(R.id.tvNum);
        imgUser = view.findViewById(R.id.imgUser);
        tvAddress = view.findViewById(R.id.tvAddress);
        tvAnimalCount = view.findViewById(R.id.tvAnimalCount);
        tvFarmCount = view.findViewById(R.id.tvFarmCount);
        tvPlanType = view.findViewById(R.id.tvPlanType);
        llCountStatus = view.findViewById(R.id.llCountStatus);
        view.findViewById(R.id.rlPlan).setOnClickListener(this);
        view.findViewById(R.id.rlChangePassword).setOnClickListener(this);
        view.findViewById(R.id.btnEdit).setOnClickListener(this);

        updateUI();
    }

    public void updateUI() {
        userInfoBean = getDataManager().getUserInfo();
        if (!userInfoBean.getUserType().equals(AppConstants.TYPE_BUSINESS)) {
            tvPlanType.setText(userInfoBean.getSubscriptionPlan().equalsIgnoreCase(AppConstants.PLAN_FREE) ? getString(R.string.free_plan) : getString(R.string.premium_plan));
            llCountStatus.setVisibility(View.VISIBLE);
            updateProfileCount();
        } else {
            llCountStatus.setVisibility(View.GONE);
            tvPlanType.setText(userInfoBean.getSubscriptionPlan().equalsIgnoreCase(AppConstants.PLAN_FREE) ? getString(R.string.free_plan) : getString(R.string.business_plan));
        }

        tvUserName.setText(userInfoBean.getName());
        tvEmail.setText(userInfoBean.getEmail());
        tvNum.setText(userInfoBean.getContactNumber());
        if (!userInfoBean.getProfileImage().isEmpty())
            Picasso.get().load(userInfoBean.getProfileImage()).placeholder(R.drawable.user_placeholder).into(imgUser);
        tvAddress.setText(userInfoBean.getLocation().isEmpty() ? getString(R.string.na) : userInfoBean.getLocation());
    }

    private void updateProfileCount() {
        new Thread(() -> {
            String propCount = getDataManager().getPropertyCount();
            String animalCount = getDataManager().getAnimalCount();

            new Handler(Looper.getMainLooper()).post(() -> {
                tvAnimalCount.setText(animalCount);
                tvFarmCount.setText(propCount);
            });
        }).start();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rlChangePassword:
                ChangePwdDialog.newInstance().show(getChildFragmentManager());
                break;

            case R.id.rlPlan:
                if (userInfoBean.getSubscriptionPlan().equalsIgnoreCase(AppConstants.PLAN_FREE))
                    startActivity(new Intent(getActivity(), PlanActivity.class));
                else {
                    if (isNetworkConnected(false)) {
                        doGetPlanDetailInfo();
                    } else
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_internet), Toast.LENGTH_SHORT);
                }
                break;

            case R.id.btnEdit:
                startActivity(new Intent(getBaseActivity(), EditProfileActivity.class));
                break;
        }
    }

    private void doGetPlanDetailInfo() {
        setLoading(true);

        getDataManager().doServerCheckPlanApiCall(getDataManager().getHeader())
                .getAsResponse(new VolleyResponseListener() {
                    @Override
                    public void onVolleyResponse(String response) {
                        setLoading(false);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            if (status.equalsIgnoreCase("success")) {
                                JSONObject subObj = jsonObject.getJSONObject("planDetails");
                                String plan = subObj.getString("subscriptionPlan");
                                String planId = subObj.getString("subscriptionSaleId");

                                if (plan.equalsIgnoreCase(AppConstants.PLAN_FREE)) {
                                    userInfoBean.setSubscriptionPlan(plan);
                                    userInfoBean.setSubscriptionSaleId(planId);
                                    getDataManager().setUserInfo(userInfoBean);
                                    tvPlanType.setText(getString(R.string.free_plan));
                                } else {
                                    String planStartDate = subObj.getString("plan_start_date");
                                    String planAmount = subObj.getString("plan_amount");
                                    String nextDate = subObj.getString("next_date");
                                    String recurrence = subObj.getString("recurrence");
                                    String dayLeft = subObj.getString("day_left");

                                    Bundle args = new Bundle();
                                    args.putString("plan", plan);
                                    args.putString("planId", planId);
                                    args.putString("planStartDate", planStartDate);
                                    args.putString("planAmount", planAmount);
                                    args.putString("nextDate", nextDate);
                                    args.putString("recurrence", recurrence);
                                    args.putString("dayLeft", dayLeft);
                                    PremiumPlanDialog.newInstance(args, () -> updateUI()).show(getChildFragmentManager());
                                }

                            } else {
                                CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                            }
                        } catch (Exception e) {
                            CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }

                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }
}
