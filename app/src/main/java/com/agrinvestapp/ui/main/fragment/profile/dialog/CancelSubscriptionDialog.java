package com.agrinvestapp.ui.main.fragment.profile.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.agrinvestapp.R;
import com.agrinvestapp.ui.base.BaseDialog;


public class CancelSubscriptionDialog extends BaseDialog implements View.OnClickListener {

    private static final String TAG = CancelSubscriptionDialog.class.getSimpleName();
    private SubscriptionCancelCallback callback;

    public static CancelSubscriptionDialog newInstance(SubscriptionCancelCallback callback) {

        Bundle args = new Bundle();

        CancelSubscriptionDialog fragment = new CancelSubscriptionDialog();
        fragment.setCallback(callback);
        fragment.setArguments(args);
        return fragment;
    }

    private void setCallback(SubscriptionCancelCallback callback) {
        this.callback = callback;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_common, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.subsCancel));

        TextView tvMsg = view.findViewById(R.id.tvMsg);
        tvMsg.setText(getString(R.string.subsCancelMsg));

        Button btnCancel = view.findViewById(R.id.btnCancel);
        btnCancel.setText(getString(R.string.goBack));
        btnCancel.setOnClickListener(this);

        Button btnOk = view.findViewById(R.id.btnOk);
        btnOk.setText(getString(R.string.ok));
        btnOk.setOnClickListener(this);
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnOk:
                dismissDialog(TAG);
                callback.onCancelSubscription();
                break;

            case R.id.btnCancel:
                dismissDialog(TAG);
                break;

        }
    }
}
