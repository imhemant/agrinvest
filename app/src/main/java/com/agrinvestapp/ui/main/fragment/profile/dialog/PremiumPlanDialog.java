package com.agrinvestapp.ui.main.fragment.profile.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.db.UserInfoBean;
import com.agrinvestapp.ui.base.BaseDialog;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.CalenderUtils;
import com.agrinvestapp.utils.CommonUtils;
import com.agrinvestapp.volley.VolleyJsonObjectListener;
import com.agrinvestapp.volley.VolleyRequest;
import com.android.volley.VolleyError;

import org.json.JSONObject;


public class PremiumPlanDialog extends BaseDialog implements View.OnClickListener {

    private static final String TAG = PremiumPlanDialog.class.getSimpleName();

    private String plan, planId, planStartDate, planAmount, nextDate, recurrence, dayLeft;
    private SubscriptionCancelCallback callback;

    public static PremiumPlanDialog newInstance(Bundle args, SubscriptionCancelCallback callback) {

        PremiumPlanDialog fragment = new PremiumPlanDialog();
        fragment.setArguments(args);
        fragment.setCallback(callback);
        return fragment;
    }

    private void setCallback(SubscriptionCancelCallback callback) {
        this.callback = callback;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            plan = bundle.getString("plan");
            planId = bundle.getString("planId");
            planStartDate = bundle.getString("planStartDate");
            planAmount = bundle.getString("planAmount");
            nextDate = bundle.getString("nextDate");
            recurrence = bundle.getString("recurrence");
            dayLeft = bundle.getString("dayLeft");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_premium_plan, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.frameCancel).setOnClickListener(this);
        view.findViewById(R.id.btnCancelSubs).setOnClickListener(this);
        updateUI(view);
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCancelSubs:
                CancelSubscriptionDialog.newInstance(this::doCancelSubscription).show(getChildFragmentManager());
                break;

            case R.id.frameCancel:
                dismissDialog(TAG);
                break;
        }
    }

    private void updateUI(View view) {
        TextView tvPlanAmt = view.findViewById(R.id.tvPlanAmt);
        TextView tvPlanType = view.findViewById(R.id.tvPlanType);
        TextView tvPurDate = view.findViewById(R.id.tvPurDate);
        TextView tvPlanEndsIn = view.findViewById(R.id.tvPlanEndsIn);
        TextView tvNextBillDate = view.findViewById(R.id.tvNextBillDate);

        if (getDataManager().getUserInfo().getUserType().equals(AppConstants.TYPE_BUSINESS)) {
            ((TextView) view.findViewById(R.id.tvHeader)).setText(getString(R.string.business_plan));
        }

        /*String[] planSplit = recurrence.split("\\s+");
        planSplit.length <= 2 ? planSplit[1] : planSplit[0]*/
        planAmount = "$".concat(planAmount).concat(" / ").concat(getString(R.string.month));
        tvPlanAmt.setText(planAmount);
        String[] planTypeSplit = recurrence.split("\\s+");
        tvPlanType.setText(planTypeSplit.length <= 2 ? planTypeSplit[0] + " " + getString(R.string.month) : planTypeSplit[0]);
        tvPurDate.setText(planStartDate.contains("-") ? CalenderUtils.formatDate(planStartDate, AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT) : planStartDate);
        if (!dayLeft.isEmpty() && !dayLeft.contains("NA"))
            dayLeft = dayLeft.concat(" ").concat(getString(R.string.days));
        tvPlanEndsIn.setText(dayLeft);
        String nbDate = nextDate.contains("-") ? CalenderUtils.formatDate(nextDate, AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT) : nextDate;
        tvNextBillDate.setText(nbDate.isEmpty() ? getString(R.string.na) : nbDate);
    }

    void doCancelSubscription() {
        setLoading(true);

        getDataManager().doServerCancelSubscriptionApiCall(getDataManager().getHeader()).getAsJsonObject(new VolleyJsonObjectListener() {
            @Override
            public void onVolleyJsonObject(JSONObject jsonObject) {
                setLoading(false);

                try {
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");

                    if (status.equalsIgnoreCase("success")) {
                        UserInfoBean userInfoBean = getDataManager().getUserInfo();
                        userInfoBean.setSubscriptionSaleId("");
                        userInfoBean.setSubscriptionPlan(AppConstants.PLAN_FREE);
                        getDataManager().setUserInfo(userInfoBean);

                        CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                        dismissDialog(TAG);
                        callback.onCancelSubscription();
                    } else {
                        CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                    }
                } catch (Exception e) {
                    CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                }
            }

            @Override
            public void onVolleyJsonException() {
                setLoading(false);
                CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
            }

            @Override
            public void onVolleyError(VolleyError error) {
                setLoading(false);
                VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
            }
        });
    }
}
