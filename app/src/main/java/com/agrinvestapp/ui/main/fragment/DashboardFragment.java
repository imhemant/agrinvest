package com.agrinvestapp.ui.main.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.agrinvestapp.R;
import com.agrinvestapp.ui.animal.AnimalActivity;
import com.agrinvestapp.ui.base.BaseFragment;
import com.agrinvestapp.ui.base.dialog.PlanUpgradeDialog;
import com.agrinvestapp.ui.crop.CropActivity;
import com.agrinvestapp.ui.expense.ExpenseActivity;
import com.agrinvestapp.ui.incomestatement.IncomeStateActivity;
import com.agrinvestapp.ui.main.fragment.profile.PlanActivity;
import com.agrinvestapp.ui.products.ProductActivity;
import com.agrinvestapp.ui.property.PropertyActivity;
import com.agrinvestapp.utils.AppConstants;

public class DashboardFragment extends BaseFragment implements View.OnClickListener {

    public static DashboardFragment newInstance() {
        Bundle args = new Bundle();

        DashboardFragment fragment = new DashboardFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getBaseActivity().updateLanguage(getDataManager().getUserInfo().getLanguage());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dashboard, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getDataManager().getUserInfo().getUserType().equals(AppConstants.TYPE_BUSINESS)) {
            int mGray = ContextCompat.getColor(getContext(), R.color.mid_grey);
            ((CardView) view.findViewById(R.id.cvAnimal)).setCardBackgroundColor(mGray);
            ((CardView) view.findViewById(R.id.cvCrop)).setCardBackgroundColor(mGray);
            ((CardView) view.findViewById(R.id.cvProperty)).setCardBackgroundColor(mGray);
            ((CardView) view.findViewById(R.id.cvExpense)).setCardBackgroundColor(mGray);
            ((CardView) view.findViewById(R.id.cvIncome)).setCardBackgroundColor(mGray);
            onClickListener(view.findViewById(R.id.cvProducts));
        } else {
            onClickListener(view.findViewById(R.id.cvProperty), view.findViewById(R.id.cvAnimal),
                    view.findViewById(R.id.cvExpense), view.findViewById(R.id.cvCrop), view.findViewById(R.id.cvProducts),
                    view.findViewById(R.id.cvIncome));
        }
    }

    private void onClickListener(View... views){
        for (View view: views){
            view.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cvProperty:
                startActivity(new Intent(getBaseActivity(), PropertyActivity.class));
                break;

            case R.id.cvAnimal:
                startActivity(new Intent(getBaseActivity(), AnimalActivity.class));
                break;

            case R.id.cvExpense:
                startActivity(new Intent(getBaseActivity(), ExpenseActivity.class));
                break;

            case R.id.cvCrop:
                startActivity(new Intent(getBaseActivity(), CropActivity.class));
                break;

            case R.id.cvProducts:
                if (getDataManager().getUserInfo().getUserType().equals(AppConstants.TYPE_BUSINESS)) {
                    if (!getDataManager().getUserInfo().getSubscriptionPlan().equalsIgnoreCase(AppConstants.PLAN_FREE)) {
                        startActivity(new Intent(getBaseActivity(), ProductActivity.class));
                    } else {
                        PlanUpgradeDialog.newInstance(getString(R.string.alert_free_product), getString(R.string.upgrade_to_business_plans), () -> startActivity(new Intent(getBaseActivity(), PlanActivity.class))).show(getChildFragmentManager());
                    }
                } else {
                    startActivity(new Intent(getBaseActivity(), ProductActivity.class));
                }
                break;

            case R.id.cvIncome:
                startActivity(new Intent(getBaseActivity(), IncomeStateActivity.class));
                break;
        }
    }
}
