package com.agrinvestapp.ui.main.fragment.profile;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.db.UserInfoBean;
import com.agrinvestapp.ui.base.BaseActivity;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.CommonUtils;
import com.agrinvestapp.volley.VolleyRequest;
import com.agrinvestapp.volley.VolleyResponseListener;
import com.android.volley.VolleyError;

import org.json.JSONObject;

public class PlanActivity extends BaseActivity implements View.OnClickListener {

    private Button btnGetPlan;
    private Boolean isTypeBusiness = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan);
        isTypeBusiness = getDataManager().getUserInfo().getUserType().equals(AppConstants.TYPE_BUSINESS);
        initView();
    }

    private void initView() {
            /* toolbar view start */
        ImageView imgBack = findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);
        imgBack.setVisibility(View.VISIBLE);

        TextView tvTitle = findViewById(R.id.tvTitle);
        TextView tvHeader = findViewById(R.id.headingTxt);
        if (isTypeBusiness) {
            tvTitle.setText(R.string.business_plan);
            tvHeader.setText(R.string.payment_title_business);
        } else {
            tvTitle.setText(R.string.premium_plan);
        }
        /* toolbar view end */

        btnGetPlan = findViewById(R.id.btnGetPlan);
        btnGetPlan.setOnClickListener(this);

        if (isNetworkConnected(false)) {
            doGetPlanInfo();
        } else
            CommonUtils.showToast(getActivity(), getString(R.string.alert_internet), Toast.LENGTH_SHORT);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                getActivity().onBackPressed();
                break;

            case R.id.btnGetPlan:
                if (isNetworkConnected(false)) {
                    doCheckPlanInfo(false);
                } else
                    CommonUtils.showToast(getActivity(), getString(R.string.alert_internet), Toast.LENGTH_SHORT);
                break;
        }
    }

    private void doGetPlanInfo() {
        setLoading(true);

        getDataManager().doServerGetPlanApiCall(getDataManager().getHeader())
                .getAsResponse(new VolleyResponseListener() {
                    @Override
                    public void onVolleyResponse(String response) {
                        setLoading(false);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            if (status.equalsIgnoreCase("success")) {
                                JSONObject subObj = jsonObject.getJSONObject("planPrice");
                                String plan = subObj.getString("planPrice");
                                String planType = subObj.getString("plan_type");
                                String planContent = subObj.getString("plan_content");
                                String subscriptionPlan = subObj.getString("subscriptionPlan");
                                AppConstants.WEB_PAYMENT = subObj.getString("paymentUrl");

                                updateUI(plan, planType, planContent, subscriptionPlan);
                            } else {
                                CommonUtils.showToast(getActivity(), message, Toast.LENGTH_SHORT);
                            }
                        } catch (Exception e) {
                            CommonUtils.showToast(getActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getActivity(), error);
                    }
                });
    }

    private void updateUI(String plan, String planType, String planContent, String subscriptionPlan) {
        findViewById(R.id.rlMain).setVisibility(View.VISIBLE);

        if (!subscriptionPlan.equalsIgnoreCase(AppConstants.PLAN_FREE)) {
            UserInfoBean userInfoBean = getDataManager().getUserInfo();
            userInfoBean.setSubscriptionPlan(plan);
            userInfoBean.setSubscriptionSaleId("");
            btnGetPlan.setVisibility(View.INVISIBLE);
            getDataManager().setUserInfo(userInfoBean);
        }

        /*String[] planSplit = planType.split("\\s+");
        planSplit.length <= 2 ? planSplit[1] : planSplit[0]*/
        String planPrice = "$".concat(plan).concat(" / ").concat(getString(R.string.month));
        TextView tvPlan = findViewById(R.id.tvPlan);
        tvPlan.setText(planPrice);

        TextView tvPlanTxt = findViewById(R.id.tvPlanTxt);
        tvPlanTxt.setText(planContent);
    }

    private void doCheckPlanInfo(Boolean checkCancelPaymentStatus) {
        setLoading(true);

        getDataManager().doServerCheckPlanApiCall(getDataManager().getHeader())
                .getAsResponse(new VolleyResponseListener() {
                    @Override
                    public void onVolleyResponse(String response) {
                        setLoading(false);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            if (status.equalsIgnoreCase("success")) {
                                JSONObject subObj = jsonObject.getJSONObject("planDetails");
                                String plan = subObj.getString("subscriptionPlan");
                                String planId = subObj.getString("subscriptionSaleId");

                                if (planId.isEmpty()) {
                                    //do subscription
                                    if (!checkCancelPaymentStatus)
                                        startActivityForResult(new Intent(getActivity(), PaymentActivity.class), AppConstants.PAYMENT_CALLBACK);
                                } else {
                                    //don't do subscription
                                    UserInfoBean userInfoBean = getDataManager().getUserInfo();
                                    userInfoBean.setSubscriptionPlan(plan);
                                    userInfoBean.setSubscriptionSaleId(planId);
                                    getDataManager().setUserInfo(userInfoBean);
                                    btnGetPlan.setVisibility(View.INVISIBLE);
                                    if (!checkCancelPaymentStatus)
                                        CommonUtils.showToast(getActivity(), getString(R.string.alert_subs_user), Toast.LENGTH_SHORT);
                                    finish();
                                }
                            } else {
                                CommonUtils.showToast(getActivity(), message, Toast.LENGTH_SHORT);
                            }
                        } catch (Exception e) {
                            CommonUtils.showToast(getActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }

                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getActivity(), error);
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == AppConstants.PAYMENT_CALLBACK) {
            if (resultCode == Activity.RESULT_OK) {
                //navigate to calling activity or fragment
                finish();
            } else if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                doCheckPlanInfo(true);
            }
        }
    }
}
