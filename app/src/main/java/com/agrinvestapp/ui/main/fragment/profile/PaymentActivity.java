package com.agrinvestapp.ui.main.fragment.profile;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.db.UserInfoBean;
import com.agrinvestapp.ui.base.BaseActivity;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.CommonUtils;

public class PaymentActivity extends BaseActivity implements View.OnClickListener {

    private LinearLayout llProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        /* toolbar view start */
        ImageView imgBack = findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);
        imgBack.setVisibility(View.VISIBLE);

        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.payment));
        /* toolbar view end */

        initializeView();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initializeView() {
        llProgress = findViewById(R.id.llProgress);
        WebView webView = findViewById(R.id.webview);
        webView.setWebViewClient(new AppWebViewClients());
        final MyJavaScriptInterface myJavaScriptInterface = new MyJavaScriptInterface();
        webView.addJavascriptInterface(myJavaScriptInterface,
                "AppPaymentCallback");
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.setInitialScale(50);
        webView.getSettings().setDisplayZoomControls(true);

        if (isNetworkConnected(false)) {
            setContentLoading(true);
            webView.loadUrl(getDataManager().getPaymentUrl(getDataManager().getUserInfo().getAuthToken()));
        } else
            CommonUtils.showToast(getActivity(), getString(R.string.alert_internet), Toast.LENGTH_SHORT);
    }

    private void setContentLoading(Boolean isLoading) {
        llProgress.setVisibility(isLoading ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
        }
    }

    private void updateSession(boolean isPaymentDone) {
        UserInfoBean userInfoBean = getDataManager().getUserInfo();
        userInfoBean.setSubscriptionPlan(isPaymentDone ? AppConstants.PLAN_PREMIUM : AppConstants.PLAN_FREE);
        userInfoBean.setSubscriptionSaleId("");
        getDataManager().setUserInfo(userInfoBean);

        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }

    private class AppWebViewClients extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageCommitVisible(WebView view, String url) {
            super.onPageCommitVisible(view, url);
            setContentLoading(false);
        }
    }

    public class MyJavaScriptInterface {

        @JavascriptInterface
        public void paymentCallback(String msg) {
            if (msg.equalsIgnoreCase("success")) {
                //success
                updateSession(true);
            } else {
                //fail
                updateSession(false);
            }
        }

    }
}
