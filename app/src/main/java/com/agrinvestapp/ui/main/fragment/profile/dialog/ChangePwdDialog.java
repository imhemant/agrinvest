package com.agrinvestapp.ui.main.fragment.profile.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.ui.base.BaseDialog;
import com.agrinvestapp.utils.CommonUtils;
import com.agrinvestapp.volley.VolleyRequest;
import com.agrinvestapp.volley.VolleyResponseListener;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.HashMap;


public class ChangePwdDialog extends BaseDialog implements View.OnClickListener {

    private static final String TAG = ChangePwdDialog.class.getSimpleName();

    private EditText etCurPwd, etNPwd, etConfPwd;

    public static ChangePwdDialog newInstance() {

        Bundle args = new Bundle();

        ChangePwdDialog fragment = new ChangePwdDialog();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_cpwd, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.btnCancel).setOnClickListener(this);
        view.findViewById(R.id.btnUpdate).setOnClickListener(this);

        etCurPwd = view.findViewById(R.id.etCurPwd);
        etNPwd = view.findViewById(R.id.etNPwd);
        etConfPwd = view.findViewById(R.id.etConfPwd);
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnUpdate:
                if (isNetworkConnected(false)) {
                    String tempCPwd = etCurPwd.getText().toString().trim();
                    String tempNPwd = etNPwd.getText().toString().trim();
                    String tempConfPwd = etConfPwd.getText().toString().trim();

                    if (verifyInput(tempCPwd, tempNPwd, tempConfPwd)) {
                        doChangePwd(tempCPwd, tempNPwd, tempConfPwd);
                    }
                } else {
                    CommonUtils.showToast(getActivity(), getString(R.string.alert_internet), Toast.LENGTH_SHORT);
                }
                break;

            case R.id.btnCancel:
                dismissDialog(TAG);
                break;

        }
    }

    private Boolean verifyInput(String oldPassword, String newPassword, String confirmPassword) {
        if (TextUtils.isEmpty(oldPassword)) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_old_password), Toast.LENGTH_SHORT);
            return false;
        } else if (TextUtils.isEmpty(newPassword)) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_new_password), Toast.LENGTH_SHORT);
            return false;
        } else if (!(newPassword.length() >= 6 && newPassword.length() <= 16)) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_length_pwd), Toast.LENGTH_SHORT);
            return false;
        } else if (TextUtils.isEmpty(confirmPassword)) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_confirm_password), Toast.LENGTH_SHORT);
            return false;
        } else if (!newPassword.equals(confirmPassword)) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_new_and_confirm_password), Toast.LENGTH_SHORT);
            return false;
        } else return true;
    }

    private void doChangePwd(String oldPassword, String newPassword, String confirmPassword) {
        setLoading(true);
        HashMap<String, String> mParameterMap = new HashMap<>();
        mParameterMap.put("oldPassword", oldPassword);
        mParameterMap.put("newPassword", newPassword);
        mParameterMap.put("confirmPassword", confirmPassword);


        getDataManager().doServerChangePwdApiCall(getDataManager().getHeader(), mParameterMap)
                .getAsResponse(new VolleyResponseListener() {
                    @Override
                    public void onVolleyResponse(String response) {
                        setLoading(false);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");
                            if (status.equals("success")) {
                                dismissDialog(TAG);
                                CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                            } else {
                                CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                            }
                        } catch (Exception e) {
                            dismissDialog(TAG);
                            CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        dismissDialog(TAG);
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

}
