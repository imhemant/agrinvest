package com.agrinvestapp.ui.crop.fragment.treatments.dialog;

import com.agrinvestapp.data.model.api.TreatmentDetailResponse;

public interface CropTreatmentDetailCallback {
    void onAdd(TreatmentDetailResponse.TreatmentDetailBean treatmentDetailBean);

    void onEdit(int pos, TreatmentDetailResponse.TreatmentDetailBean treatmentDetailBean);
}
