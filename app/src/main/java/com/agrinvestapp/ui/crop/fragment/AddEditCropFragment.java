package com.agrinvestapp.ui.crop.fragment;


import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.CropInfoResponse;
import com.agrinvestapp.data.model.db.Crop;
import com.agrinvestapp.data.model.db.Parcel;
import com.agrinvestapp.ui.animal.adapter.SpParcelNameAdapter;
import com.agrinvestapp.ui.base.BaseFragment;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.AppLogger;
import com.agrinvestapp.utils.CalenderUtils;
import com.agrinvestapp.utils.CommonUtils;
import com.agrinvestapp.volley.VolleyJsonObjectListener;
import com.agrinvestapp.volley.VolleyRequest;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AddEditCropFragment extends BaseFragment implements View.OnClickListener {

    private EditText etCropName, etLotId, etDescription;

    private CropInfoFragment cropInfoFragment;
    private Boolean isAdd = true;

    private List<Parcel> parcelList;
    private Spinner spParcel;
    private SpParcelNameAdapter spParcelNameAdapter;
    private CropInfoResponse.CroplListBean cropInfoBean;
    private Boolean isClick = false;

    private Handler handler = new Handler(Looper.getMainLooper());

    public static AddEditCropFragment newInstance(CropInfoFragment cropInfoFragment, boolean isAdd, CropInfoResponse.CroplListBean cropInfoBean) {

        Bundle args = new Bundle();

        args.putParcelable(AppConstants.CROP_MODEL, cropInfoBean);
        AddEditCropFragment fragment = new AddEditCropFragment();
        fragment.setInstance(cropInfoFragment, isAdd);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments()!=null){
            cropInfoBean = getArguments().getParcelable(AppConstants.CROP_MODEL);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_edit_crop, container, false);
        initView(view);
        return view;
    }

    private void setInstance(CropInfoFragment cropInfoFragment, Boolean isAdd) {
        this.cropInfoFragment = cropInfoFragment;
        this.isAdd = isAdd;
    }

    private void initView(View view) {
          /* toolbar view start */
        ImageView imgBack = view.findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);
        imgBack.setVisibility(View.VISIBLE);

        TextView tvTitle = view.findViewById(R.id.tvTitle);

        Button tvAdd = view.findViewById(R.id.btnAddCrop);
        if (isAdd) {
            tvTitle.setText(R.string.add_crop_info);
            tvAdd.setText(getString(R.string.add));
        } else {
            tvTitle.setText(R.string.edit_crop_info);
            tvAdd.setText(getString(R.string.update));
        }
        tvAdd.setOnClickListener(this);
        /* toolbar view end */
        view.findViewById(R.id.llMain).setOnClickListener(this); //avoid bg click

        etCropName = view.findViewById(R.id.etCropName);
        etLotId = view.findViewById(R.id.etLotId);
        etDescription = view.findViewById(R.id.etDescription);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initSpinner(view);
    }

    private void initSpinner(View view) {
        spParcel = view.findViewById(R.id.spParcel);

        parcelList = new ArrayList<>();
        Parcel bean = new Parcel();
        bean.setParcelId("");
        bean.setParcelName(getString(R.string.select_parcel));
        bean.setProperty_id("");
        bean.setRecordKey("");
        parcelList.add(bean);

        spParcelNameAdapter = new SpParcelNameAdapter(getBaseActivity(), parcelList);
        spParcel.setAdapter(spParcelNameAdapter);

        if (isAdd) {
            if (isNetworkConnected(true)) {
                if (getDataManager().getLocalDataExistForSync()) {
                    getSpLocalDBData();
                } else {
                    getSpParcelApi();
                }
            } else {
                getSpLocalDBData();
            }
        } else {
            if (cropInfoBean != null) updateCropUi();
            if (isNetworkConnected(true)) {
                if (getDataManager().getLocalDataExistForSync()) {
                    getSpLocalDBData();
                } else {
                    getSpParcelApi();
                }
            } else getSpLocalDBData();
        }
    }

    private void getSpParcelApi() {
        setLoading(true);
        getDataManager().doServerGetParcelListApiCall(getDataManager().getHeader())
                .getAsJsonObject(new VolleyJsonObjectListener() {

                    @Override
                    public void onVolleyJsonObject(JSONObject jsonObject) {
                        setLoading(false);

                        try {
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            if (status.equals("success")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("parcelList");

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject obj = jsonArray.getJSONObject(i);
                                    Parcel parcel = new Parcel();

                                    parcel.setParcelId(obj.getString("parcelId"));
                                    parcel.setParcelName(obj.getString("parcelName"));
                                    parcel.setProperty_id(obj.getString("property_id"));
                                    parcel.setRecordKey(obj.getString("recordKey"));
                                    parcelList.add(parcel);
                                }

                                spParcelNameAdapter.notifyDataSetChanged();

                                updateSp();
                            } else {
                                if (!message.equalsIgnoreCase(getString(R.string.no_record_found2)))
                                    CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                            }
                        } catch (Exception e) {
                            CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyJsonException() {
                        setLoading(false);
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    private void updateSp() {
        if (!isAdd && cropInfoBean!=null) {
            for (int i = 0; i < parcelList.size(); i++) {
                if (cropInfoBean.getParcelName().equalsIgnoreCase(parcelList.get(i).getParcelName())) {
                    spParcel.setSelection(i);
                    break;
                }
            }
        }
    }

    private void updateCropUi() {
        etCropName.setText(cropInfoBean.getCropName());
        etLotId.setText(cropInfoBean.getLotId());
        etDescription.setText(cropInfoBean.getDescription());
    }

    private void doAddCrop() {
        String tempCropName = etCropName.getText().toString().trim();
        String tempParcelRk = parcelList.get(spParcel.getSelectedItemPosition()).getRecordKey().trim();
        String tempLotId = etLotId.getText().toString().trim();
        String tempDes = etDescription.getText().toString().trim();

        if (verifyInputs(tempCropName, tempParcelRk, tempLotId, tempDes)) {
            setLoading(true);

            HashMap<String, String> mParameterMap = new HashMap<>();
            mParameterMap.put("cropName", tempCropName);
            mParameterMap.put("lotId", tempLotId);
            mParameterMap.put("parcelRecordKey", tempParcelRk);
            mParameterMap.put("description", tempDes);
            mParameterMap.put("recordKey", CalenderUtils.getTimestamp());

            getDataManager().doServerAddCropApiCall(getDataManager().getHeader(), mParameterMap)
                    .getAsJsonObject(new VolleyJsonObjectListener() {
                        @Override
                        public void onVolleyJsonObject(JSONObject jsonObject) {
                            setLoading(false);

                            try {
                                String status = jsonObject.getString("status");
                                String message = jsonObject.getString("message");

                                if (status.equals("success")) {
                                    //Todo local db
                                    doAddEditCropToLocalDB("add", jsonObject);

                                    CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                                    getBaseActivity().onBackPressed();
                                    if (cropInfoFragment != null)
                                        cropInfoFragment.hitApi();
                                } else {
                                    CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                                }
                            } catch (Exception e) {
                                CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                            }
                        }

                        @Override
                        public void onVolleyJsonException() {
                            setLoading(false);
                            CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }

                        @Override
                        public void onVolleyError(VolleyError error) {
                            setLoading(false);
                            VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                        }
                    });
        }

    }

    private void doUpdateCrop() {
        String tempCropName = etCropName.getText().toString().trim();
        String tempParcelRk = parcelList.get(spParcel.getSelectedItemPosition()).getRecordKey().trim();
        String tempLotId = etLotId.getText().toString().trim();
        String tempDes = etDescription.getText().toString().trim();

        if (verifyInputs(tempCropName, tempParcelRk, tempLotId, tempDes)) {
            setLoading(true);

            HashMap<String, String> mParameterMap = new HashMap<>();
            mParameterMap.put("recordKey", cropInfoBean.getRecordKey()); //crop record key
            mParameterMap.put("cropName", tempCropName);
            mParameterMap.put("lotId", tempLotId);
            mParameterMap.put("parcelRecordKey", tempParcelRk);
            mParameterMap.put("description", tempDes);

            getDataManager().doServerUpdateCropApiCall(getDataManager().getHeader(), mParameterMap)
                    .getAsJsonObject(new VolleyJsonObjectListener() {

                        @Override
                        public void onVolleyJsonObject(JSONObject jsonObject) {
                            setLoading(false);

                            try {
                                String status = jsonObject.getString("status");
                                String message = jsonObject.getString("message");

                                if (status.equals("success")) {
                                    //Todo manage local db update
                                    doAddEditCropToLocalDB("edit", jsonObject);
                                    CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                                    getBaseActivity().onBackPressed();
                                    if (cropInfoFragment != null)
                                        cropInfoFragment.hitApi();
                                } else {
                                    CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                                }
                            } catch (Exception e) {
                                CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                            }
                        }

                        @Override
                        public void onVolleyJsonException() {
                            setLoading(false);
                            CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }

                        @Override
                        public void onVolleyError(VolleyError error) {
                            setLoading(false);
                            VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                        }
                    });
        }
    }

    private boolean verifyInputs(String tempCropName, String tempParcel, String tempLotId, String tempDes) {
        if (tempCropName.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_cropName), Toast.LENGTH_SHORT);
            return false;
        } else if (!(tempCropName.length() >= 3 && tempCropName.length() <= 30)) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_crop_length), Toast.LENGTH_SHORT);
            return false;
        } else if (tempParcel.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_select_parcel), Toast.LENGTH_SHORT);
            return false;
        } else if (tempLotId.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_lotId), Toast.LENGTH_SHORT);
            return false;
        } else if (!(tempLotId.length() >= 3 && tempLotId.length() <= 10)) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_lot_length), Toast.LENGTH_SHORT);
            return false;
        } else if (tempDes.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_des), Toast.LENGTH_SHORT);
            return false;
        } else return true;

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.imgBack:
                getBaseActivity().onBackPressed();
                break;

            case R.id.btnAddCrop:
                if (!isClick) {
                    isClick = true;

                    if (isNetworkConnected(true)) getBaseActivity().doSyncAppDB(false, flag1 -> {
                        switch (flag1) {
                            case AppConstants.ONLINE:
                                if (isAdd) doAddCrop();
                                else if (cropInfoBean != null && !cropInfoBean.getCropId().equalsIgnoreCase(cropInfoBean.getRecordKey()))
                                    doUpdateCrop();
                                else doAddCrop();
                                break;

                            case AppConstants.OFFLINE:
                                String tempCropName = etCropName.getText().toString().trim();
                                String tempParcelRk = parcelList.get(spParcel.getSelectedItemPosition()).getRecordKey().trim();
                                String tempLotId = etLotId.getText().toString().trim();
                                String tempDes = etDescription.getText().toString().trim();

                                if (verifyInputs(tempCropName, tempParcelRk, tempLotId, tempDes)) {
                                    if (isAdd)
                                        doAddEditCropToLocalDB("add", tempCropName, tempLotId, tempDes);
                                    else if (cropInfoBean != null && !cropInfoBean.getCropId().equalsIgnoreCase(cropInfoBean.getRecordKey()))
                                        doAddEditCropToLocalDB("edit", tempCropName, tempLotId, tempDes);
                                    else if (!isAdd)
                                        doAddEditCropToLocalDB("edit", tempCropName, tempLotId, tempDes);
                                }
                                break;
                        }
                    });
                    else {
                        String tempCropName = etCropName.getText().toString().trim();
                        String tempParcelRk = parcelList.get(spParcel.getSelectedItemPosition()).getRecordKey().trim();
                        String tempLotId = etLotId.getText().toString().trim();
                        String tempDes = etDescription.getText().toString().trim();

                        if (verifyInputs(tempCropName, tempParcelRk, tempLotId, tempDes)) {
                            if (isAdd)
                                doAddEditCropToLocalDB("add", tempCropName, tempLotId, tempDes);
                            else if (cropInfoBean != null && !cropInfoBean.getCropId().equalsIgnoreCase(cropInfoBean.getRecordKey()))
                                doAddEditCropToLocalDB("edit", tempCropName, tempLotId, tempDes);
                            else if (!isAdd)
                                doAddEditCropToLocalDB("edit", tempCropName, tempLotId, tempDes);
                        }
                    }
                }

                new Handler().postDelayed(() -> isClick = false, 3000);

                break;

        }
    }

    /*Local DB Start here*/
    private void getSpLocalDBData() {
        new Thread(() -> {
            List<Parcel> loadAllParcel = getDataManager().loadAllParcel();
            if (parcelList.size() < 1) parcelList.clear();
            parcelList.addAll(loadAllParcel);

            handler.post(() -> {
                updateSp();
                spParcelNameAdapter.notifyDataSetChanged();
            });
        }).start();
    }

    private void doAddEditCropToLocalDB(String flag, JSONObject jsonObject) {
        new Thread(() -> {
            try {
                JSONObject obj = jsonObject.getJSONObject("cropDetail");

                Crop crop = new Crop();

                crop.setCropId(obj.getString("cropId"));
                crop.setParcel_id(obj.getString("parcel_id"));
                crop.setCropName(obj.getString("cropName"));
                crop.setLotId(obj.getString("lotId"));
                crop.setDescription(obj.getString("description"));
                crop.setRecordKey(obj.getString("recordKey"));
                crop.setPreparationDate(obj.getString("preparationDate"));
                crop.setPlantingDate(obj.getString("plantingDate"));
                crop.setHarvestDate(obj.getString("harvestDate"));
                String saleDate = obj.getString("saleDate");
                crop.setSaleDate(saleDate.isEmpty() ? null : CalenderUtils.getDateFormat(saleDate.contains("/") ? CalenderUtils.formatDate(saleDate, AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT) : saleDate, AppConstants.SERVER_TIMESTAMP_FORMAT));
                crop.setSalePrice(obj.getString("salePrice"));
                crop.setSaleQuantity(obj.getString("saleQuantity"));
                crop.setSaleTotalPrice(obj.getString("saleTotalPrice"));
                crop.setCostFrom(obj.getString("costFrom"));
                crop.setCostTo(obj.getString("costTo"));
                crop.setCostTotal(obj.getString("costTotal"));
                crop.setStatus(obj.getString("status"));
                crop.setCrd(obj.getString("crd"));
                crop.setUpd(obj.getString("upd"));

                switch (flag) {
                    case "add":
                        crop.setDataSync(AppConstants.DB_SYNC_TRUE);
                        crop.setEvent(AppConstants.DB_EVENT_ADD);
                        getDataManager().insertCrop(crop);
                        break;

                    case "edit":
                        crop.setDataSync(AppConstants.DB_SYNC_TRUE);
                        crop.setEvent(AppConstants.DB_EVENT_EDIT);
                        getDataManager().updateCrop(crop);
                        break;
                }
            } catch (Exception e) {
                AppLogger.d("database", "Crop add or edit Exception");
            }
        }).start();
    }

    private void doAddEditCropToLocalDB(String flag, String tempCropName, String tempLotId, String tempDes) {
        new Thread(() -> {
            String tempParcelId = parcelList.get(spParcel.getSelectedItemPosition()).getParcelId().trim();


            Crop crop = new Crop();

            crop.setParcel_id(tempParcelId);
            crop.setCropName(tempCropName);
            crop.setLotId(tempLotId);
            crop.setDescription(tempDes);
            crop.setUpd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));

            switch (flag) {

                case "add":
                    String rKey = CalenderUtils.getTimestamp();
                    crop.setCropId(rKey);
                    crop.setRecordKey(rKey);
                    crop.setPreparationDate("0000-00-00");
                    crop.setPlantingDate("0000-00-00");
                    crop.setHarvestDate("0000-00-00");
                    crop.setSaleDate(CalenderUtils.getDateFormat("0000-00-00", AppConstants.SERVER_TIMESTAMP_FORMAT));
                    crop.setSalePrice("0");
                    crop.setSaleQuantity("0");
                    crop.setSaleTotalPrice("0");
                    crop.setCostFrom("0000-00-00");
                    crop.setCostTo("0000-00-00");
                    crop.setCostTotal("");
                    crop.setStatus("0");
                    crop.setCrd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    crop.setDataSync(AppConstants.DB_SYNC_FALSE);
                    crop.setEvent(AppConstants.DB_EVENT_ADD);
                    getDataManager().insertCrop(crop);

                    handler.post(() -> {
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.cropAddSuccess), Toast.LENGTH_SHORT);
                        getBaseActivity().onBackPressed();
                        if (cropInfoFragment != null) cropInfoFragment.hitApi();
                    });
                    break;

                case "edit":
                    crop.setCropId(cropInfoBean.getCropId());
                    crop.setRecordKey(cropInfoBean.getRecordKey());
                    crop.setPreparationDate(cropInfoBean.getPreparationDate());
                    crop.setPlantingDate(cropInfoBean.getPlantingDate());
                    crop.setHarvestDate(cropInfoBean.getHarvestDate());
                    crop.setSaleDate(cropInfoBean.getSaleDate().isEmpty() ? null : CalenderUtils.getDateFormat(cropInfoBean.getSaleDate().contains("/") ? CalenderUtils.formatDate(cropInfoBean.getSaleDate(), AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT) : cropInfoBean.getSaleDate(), AppConstants.SERVER_TIMESTAMP_FORMAT));
                    crop.setSalePrice(cropInfoBean.getSalePrice());
                    crop.setSaleQuantity(cropInfoBean.getSaleQuantity());
                    crop.setSaleTotalPrice(cropInfoBean.getSaleTotalPrice());
                    crop.setCostFrom(cropInfoBean.getCostFrom());
                    crop.setCostTo(cropInfoBean.getCostTo());
                    crop.setCostTotal(cropInfoBean.getCostTotal());
                    crop.setStatus(cropInfoBean.getStatus());
                    crop.setCrd(cropInfoBean.getCrd());
                    crop.setDataSync(AppConstants.DB_SYNC_FALSE);
                    crop.setEvent(cropInfoBean.getCropId().equals(cropInfoBean.getRecordKey()) ? AppConstants.DB_EVENT_ADD : AppConstants.DB_EVENT_EDIT);
                    getDataManager().updateCrop(crop);

                    handler.post(() -> {
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.cropUpdateSuccess), Toast.LENGTH_SHORT);
                        getBaseActivity().onBackPressed();
                        if (cropInfoFragment != null) cropInfoFragment.hitApi();
                    });
                    break;
            }

            //set sync status true
            getDataManager().setLocalDataExistForSync(true);
        }).start();
    }
}
