package com.agrinvestapp.ui.crop.fragment;

public interface CheckSyncCropCallback {
    void onSync(String data);

    void onCallApi();
}
