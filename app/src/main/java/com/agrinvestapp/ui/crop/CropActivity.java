package com.agrinvestapp.ui.crop;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.agrinvestapp.R;
import com.agrinvestapp.ui.base.BaseActivity;
import com.agrinvestapp.ui.crop.fragment.CropInfoFragment;
import com.agrinvestapp.utils.AppConstants;

public class CropActivity extends BaseActivity {

    public static String fromPropertyId ="";
    private Boolean isReplace = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop);
        fromPropertyId = "";
        if (getIntent()!=null && getIntent().hasExtra(AppConstants.KEY_FROM_PROPERTY)){
            fromPropertyId = getIntent().getStringExtra(AppConstants.KEY_FROM_PROPERTY);
        }

        replaceFragment(new CropInfoFragment(), R.id.cropFrame);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isReplace){
            Fragment fragment = getCurrentFragment();
            assert fragment != null;
            if (fragment instanceof CropInfoFragment) {
                CropInfoFragment infoFragment = (CropInfoFragment) fragment;
                infoFragment.hitApi();
            }
        }else isReplace = false;
    }
}
