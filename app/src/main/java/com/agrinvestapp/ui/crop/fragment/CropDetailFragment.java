package com.agrinvestapp.ui.crop.fragment;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.CropInfoResponse;
import com.agrinvestapp.data.model.db.Crop;
import com.agrinvestapp.ui.animal.dialog.CostToDateDialog;
import com.agrinvestapp.ui.base.BaseFragment;
import com.agrinvestapp.ui.crop.fragment.treatments.CropTreatmentFragment;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.AppLogger;
import com.agrinvestapp.utils.CalenderUtils;
import com.agrinvestapp.utils.CommonUtils;
import com.agrinvestapp.volley.VolleyJsonObjectListener;
import com.agrinvestapp.volley.VolleyRequest;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class CropDetailFragment extends BaseFragment implements View.OnClickListener, TextWatcher, BaseFragment.FragmentCallback{

    private DecimalFormat decimalFormat = new DecimalFormat(".##");

    private ImageView imgPrepDate, imgPlantingDate, imgHarvestDate, imgSaleDate, imgCtoDate;
    private CheckBox cbPrepDate, cbPlantingDate, cbHarvestDate, cbSale, cbCToDate;
    private TextView tvPrepDate, tvPlantingDate, tvHarvestDate, tvSaleDate, tvSaleAmt, tvCtoDate;
    private EditText etSalePrice, etSaleQty;
    private LinearLayout llSale;

    private String costFrom = "", costTo = "", costTotal = "";
    private StringBuilder builder;

    private CropInfoResponse.CroplListBean cropInfoBean;
    private CropInfoFragment cropInfoFragment;

    public static CropDetailFragment newInstance(CropInfoFragment cropInfoFragment, CropInfoResponse.CroplListBean cropInfoBean) {
        Bundle args = new Bundle();
        args.putParcelable(AppConstants.CROP_MODEL, cropInfoBean);
        CropDetailFragment fragment = new CropDetailFragment();
        fragment.setInstance(cropInfoFragment);
        fragment.setArguments(args);
        return fragment;
    }

    private void setInstance(CropInfoFragment cropInfoFragment) {
        this.cropInfoFragment = cropInfoFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments()!=null){
            cropInfoBean = getArguments().getParcelable(AppConstants.CROP_MODEL);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_crop_detail, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
           /* toolbar view start */
        ImageView imgBack = view.findViewById(R.id.imgBack);
        imgBack.setVisibility(View.VISIBLE);
        TextView tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.crop_details);
        /* toolbar view end */

        TextView tvCropName = view.findViewById(R.id.tvCropName);
        TextView tvLotId = view.findViewById(R.id.tvLotId);
        TextView tvParcelTitle = view.findViewById(R.id.tvParcelTitle);
        TextView tvDescription = view.findViewById(R.id.tvDescription);

        tvPrepDate = view.findViewById(R.id.tvPrepDate);
        tvPlantingDate = view.findViewById(R.id.tvPlantingDate);
        tvHarvestDate = view.findViewById(R.id.tvHarvestDate);
        tvSaleDate = view.findViewById(R.id.tvSaleDate);
        tvCtoDate = view.findViewById(R.id.tvCtoDate);

        cbPrepDate = view.findViewById(R.id.cbPrepDate);
        cbPlantingDate = view.findViewById(R.id.cbPlantingDate);
        cbHarvestDate = view.findViewById(R.id.cbHarvestDate);
        cbSale = view.findViewById(R.id.cbSale);
        cbCToDate = view.findViewById(R.id.cbCToDate);

        onClickListener(imgBack, view.findViewById(R.id.llMain), view.findViewById(R.id.rlTreatment), tvPrepDate,
                tvPlantingDate, tvHarvestDate, tvSaleDate, tvCtoDate, cbPrepDate, cbPlantingDate,
                cbHarvestDate, cbSale, cbCToDate);

        imgPrepDate = view.findViewById(R.id.imgPrepDate);
        imgPlantingDate = view.findViewById(R.id.imgPlantingDate);
        imgHarvestDate = view.findViewById(R.id.imgHarvestDate);
        imgSaleDate = view.findViewById(R.id.imgSaleDate);
        imgCtoDate = view.findViewById(R.id.imgCtoDate);
        llSale = view.findViewById(R.id.llSale);
        etSalePrice = view.findViewById(R.id.etSalePrice);
        etSaleQty = view.findViewById(R.id.etSaleQty);
        tvSaleAmt = view.findViewById(R.id.tvSaleAmt);

        etSalePrice.addTextChangedListener(this);
        etSaleQty.addTextChangedListener(this);

        if (cropInfoBean != null) {
            tvCropName.setText(cropInfoBean.getCropName());
            StringBuilder ltBuilder = new StringBuilder();
            ltBuilder.append("#").append(cropInfoBean.getLotId());
            tvLotId.setText(ltBuilder);
            tvParcelTitle.setText(cropInfoBean.getParcelName());
            tvDescription.setText(cropInfoBean.getDescription());
        }

        updateView();
    }

    private void onClickListener(View... views){
        for (View view: views){
            view.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        hideKeyboard();
        switch (v.getId()) {
            case R.id.imgBack:
                getBaseActivity().onBackPressed();
                break;

            case R.id.rlTreatment:
                getBaseActivity().addFragment(CropTreatmentFragment.newInstance(cropInfoBean), R.id.cropFrame, true);
                break;

            case R.id.cbPrepDate:
                imgPrepDate.setVisibility(cbPrepDate.isChecked() ? View.VISIBLE : View.GONE);
                break;

            case R.id.cbPlantingDate:
                imgPlantingDate.setVisibility(cbPlantingDate.isChecked() ? View.VISIBLE : View.GONE);
                break;

            case R.id.cbHarvestDate:
                imgHarvestDate.setVisibility(cbHarvestDate.isChecked() ? View.VISIBLE : View.GONE);
                break;

            case R.id.cbSale:
                imgSaleDate.setVisibility(cbSale.isChecked() ? View.VISIBLE : View.GONE);
                llSale.setVisibility(cbSale.isChecked() ? View.VISIBLE : View.GONE);
                break;

            case R.id.cbCToDate:
                imgCtoDate.setVisibility(cbCToDate.isChecked() ? View.VISIBLE : View.GONE);
                break;

            case R.id.tvPrepDate:
                if (cbPrepDate.isChecked()) getDate("prepDate");
                break;

            case R.id.tvPlantingDate:
                if (cbPlantingDate.isChecked()) getDate("plantingDate");
                break;

            case R.id.tvHarvestDate:
                if (cbHarvestDate.isChecked()) getDate("harvestDate");
                break;

            case R.id.tvSaleDate:
                if (cbSale.isChecked()) {
                    String price = etSalePrice.getText().toString().trim();
                    String qty = etSaleQty.getText().toString().trim();
                    if (verifySale(price, qty)) {
                        getDate("saleDate");
                    }
                }
                break;

            case R.id.tvCtoDate:
                if (cbCToDate.isChecked()) {

                    CostToDateDialog.newInstance(cropInfoBean.getCostFrom(), cropInfoBean.getCostTo(),
                            cropInfoBean.getCostTotal(), (fromDate, toDate, ttlCost) -> {
                                imgCtoDate.setVisibility(View.GONE);
                                cbCToDate.setClickable(false);

                                costFrom = fromDate;
                                costTo = toDate;
                                costTotal = ttlCost;

                                if (isNetworkConnected(true))
                                    getBaseActivity().doSyncAppDB(false, flag -> {
                                        switch (flag) {
                                            case AppConstants.ONLINE:
                                                updateCropDetail();
                                                break;

                                            case AppConstants.OFFLINE:
                                                CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_internet), Toast.LENGTH_SHORT);
                                                builder = new StringBuilder("");
                                                builder.append(costFrom).append(" ")
                                                        .append(getString(R.string.to)).append(" ")
                                                        .append(costTo);
                                                tvCtoDate.setText(builder);
                                                cropInfoBean.setCostFrom(fromDate);
                                                cropInfoBean.setCostTo(toDate);
                                                cropInfoBean.setCostTotal(ttlCost);
                                                updateCropHistoryDB(false, cropInfoBean);
                                                break;
                                        }
                                    });
                                else {
                                    CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_internet),Toast.LENGTH_SHORT);
                                    builder = new StringBuilder("");
                                    builder.append(costFrom).append(" ")
                                            .append(getString(R.string.to)).append(" ")
                                            .append(costTo);
                                    tvCtoDate.setText(builder);
                                    cropInfoBean.setCostFrom(fromDate);
                                    cropInfoBean.setCostTo(toDate);
                                    cropInfoBean.setCostTotal(ttlCost);
                                    updateCropHistoryDB(false, cropInfoBean);
                                }
                            }).show(getChildFragmentManager());
                }
                break;

        }
    }

    private void updateCropDetail() {
        setLoading(true);

        HashMap<String, String> mParams = new HashMap<>();
        mParams.put("recordKey", cropInfoBean.getRecordKey());
        mParams.put("preparationDate", tvPrepDate.getText().toString().trim());
        mParams.put("plantingDate", tvPlantingDate.getText().toString().trim());
        mParams.put("harvestDate", tvHarvestDate.getText().toString().trim());
        mParams.put("saleDate", tvSaleDate.getText().toString().trim());
        mParams.put("salePrice", etSalePrice.getText().toString().trim());
        mParams.put("saleQuantity", etSaleQty.getText().toString().trim());
        mParams.put("saleTotalPrice", tvSaleAmt.getText().toString());
        mParams.put("costFrom", costFrom);
        mParams.put("costTo", costTo);
        mParams.put("costTotal", costTotal);

        getDataManager().doServerUpdateCropHistoryApiCall(getDataManager().getHeader(), mParams)
                .getAsJsonObject(new VolleyJsonObjectListener() {

                    @Override
                    public void onVolleyJsonObject(JSONObject jsonObject) {
                        setLoading(false);

                        try {
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            if (status.equals("success")) {

                                JSONObject subObj = jsonObject.getJSONObject("cropDetail");
                                cropInfoBean.setPropertyId(subObj.getString("propertyId"));
                                cropInfoBean.setParcelName(subObj.getString("parcelName"));
                                cropInfoBean.setParcelRecordKey(subObj.getString("parcelRecordKey"));
                                cropInfoBean.setCropId(subObj.getString("cropId"));
                                cropInfoBean.setParcel_id(subObj.getString("parcel_id"));
                                cropInfoBean.setCropName(subObj.getString("cropName"));
                                cropInfoBean.setLotId(subObj.getString("lotId"));
                                cropInfoBean.setDescription(subObj.getString("description"));
                                cropInfoBean.setRecordKey(subObj.getString("recordKey"));
                                cropInfoBean.setPreparationDate(subObj.getString("preparationDate"));
                                cropInfoBean.setPlantingDate(subObj.getString("plantingDate"));
                                cropInfoBean.setHarvestDate(subObj.getString("harvestDate"));
                                cropInfoBean.setSaleDate(subObj.getString("saleDate"));
                                cropInfoBean.setSalePrice(subObj.getString("salePrice"));
                                cropInfoBean.setSaleQuantity(subObj.getString("saleQuantity"));
                                cropInfoBean.setSaleTotalPrice(subObj.getString("saleTotalPrice"));
                                cropInfoBean.setCostFrom(subObj.getString("costFrom"));
                                cropInfoBean.setCostTo(subObj.getString("costTo"));
                                cropInfoBean.setCostTotal(subObj.getString("costTotal"));
                                cropInfoBean.setStatus(subObj.getString("status"));
                                cropInfoBean.setCrd(subObj.getString("crd"));
                                cropInfoBean.setUpd(subObj.getString("upd"));

                                updateView();
                                //Todo manage local db update all data
                                updateCropHistoryDB(true, cropInfoBean);
                                CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                            } else {
                                if (!message.equals(getString(R.string.no_record_found)))
                                    CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                            }
                        } catch (Exception e) {
                            CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyJsonException() {
                        setLoading(false);
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    private void updateView() {
        if (cropInfoBean != null) {

            if (!cropInfoBean.getPreparationDate().equals("0000-00-00")) {
                cbPrepDate.setClickable(false);
                cbPrepDate.setChecked(true);
                imgPrepDate.setVisibility(View.GONE);
                tvPrepDate.setText(CalenderUtils.formatDate(cropInfoBean.getPreparationDate(), AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT));
            } else {
                cbPrepDate.setClickable(true);
                cbPrepDate.setChecked(false);
            }

            if (!cropInfoBean.getPlantingDate().equals("0000-00-00")) {
                cbPlantingDate.setClickable(false);
                cbPlantingDate.setChecked(true);
                imgPlantingDate.setVisibility(View.GONE);
                tvPlantingDate.setText(CalenderUtils.formatDate(cropInfoBean.getPlantingDate(), AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT));
            } else {
                cbPlantingDate.setClickable(true);
                cbPlantingDate.setChecked(false);
            }

            if (!cropInfoBean.getHarvestDate().equals("0000-00-00")) {
                cbHarvestDate.setClickable(false);
                cbHarvestDate.setChecked(true);
                imgHarvestDate.setVisibility(View.GONE);
                tvHarvestDate.setText(CalenderUtils.formatDate(cropInfoBean.getHarvestDate(), AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT));
            } else {
                cbHarvestDate.setClickable(true);
                cbHarvestDate.setChecked(false);
            }

            if (!cropInfoBean.getSaleDate().equals("0000-00-00") && !cropInfoBean.getSaleDate().equals("0002-11-30")) {
                cbSale.setClickable(false);
                cbSale.setChecked(true);
                imgSaleDate.setVisibility(View.GONE);
                tvSaleDate.setText(CalenderUtils.formatDate(cropInfoBean.getSaleDate(), AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT));
                llSale.setVisibility(View.VISIBLE);
                etSalePrice.setText(cropInfoBean.getSalePrice());
                etSaleQty.setText(cropInfoBean.getSaleQuantity());
                tvSaleAmt.setText(cropInfoBean.getSaleTotalPrice());
            } else {
                cbSale.setClickable(true);
                cbSale.setChecked(false);
                tvSaleDate.setText("");
                etSalePrice.setText("");
                etSaleQty.setText("");
                tvSaleAmt.setText("");
            }

            if (!cropInfoBean.getCostFrom().equals("0000-00-00")) {
                costFrom = CalenderUtils.formatDate(cropInfoBean.getCostFrom(), AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT);
                costTo = CalenderUtils.formatDate(cropInfoBean.getCostTo(), AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT);
                costTotal = cropInfoBean.getCostTotal();

                cbCToDate.setClickable(false);
                cbCToDate.setChecked(true);
                imgCtoDate.setVisibility(View.GONE);

                builder = new StringBuilder("");
                builder.append(costFrom).append(" ")
                        .append(getString(R.string.to)).append(" ")
                        .append(costTo);
                tvCtoDate.setText(builder);
            } else {
                cbCToDate.setClickable(true);
                cbCToDate.setChecked(false);
                costFrom = "";
                costTo = "";
                costTotal = "";
            }
        }
    }

    private boolean verifySale(String price, String qty) {
        if (price.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_sprice), Toast.LENGTH_SHORT);
            return false;
        } else if (price.startsWith(".")) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_valid_sprice), Toast.LENGTH_SHORT);
            return false;
        } else if (Double.parseDouble(price) <= 0) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_sprice_zero), Toast.LENGTH_SHORT);
            return false;
        } else if (qty.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_qty), Toast.LENGTH_SHORT);
            return false;
        } else if (qty.startsWith(".")) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_valid_qty), Toast.LENGTH_SHORT);
            return false;
        } else if (Double.parseDouble(qty) <= 0) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_qty_zero), Toast.LENGTH_SHORT);
            return false;
        } else return true;
    }

    private void getDate(String flag) {
        String selectedDate = "";
        switch (flag) {
            case "prepDate":
                selectedDate = tvPrepDate.getText().toString().trim();
                break;
            case "plantingDate":
                selectedDate = tvPlantingDate.getText().toString().trim();
                break;
            case "harvestDate":
                selectedDate = tvHarvestDate.getText().toString().trim();
                break;
            case "saleDate":
                selectedDate = tvSaleDate.getText().toString().trim();
                break;
        }

        Calendar newCalender = Calendar.getInstance();

        int day, month, year;

        if (selectedDate.isEmpty()) {
            day = newCalender.get(Calendar.DAY_OF_MONTH);
            month = newCalender.get(Calendar.MONTH);
            year = newCalender.get(Calendar.YEAR);
        } else {
            String[] date = selectedDate.split("/");

            day = Integer.parseInt(date[0]);
            month = Integer.parseInt(date[1]) - 1;
            year = Integer.parseInt(date[2]);
        }

        DatePickerDialog datePickerDialog = new DatePickerDialog(getBaseActivity(), (view, selectedYear, selectedMonth, selectedDay) -> {
            selectedMonth += 1;

            StringBuilder date = new StringBuilder("");
            date.append(selectedDay).append("/").append(selectedMonth).append("/").append(selectedYear);

            switch (flag) {
                case "prepDate":
                    imgPrepDate.setVisibility(View.GONE);
                    tvPrepDate.setText(CalenderUtils.formatDate(String.valueOf(date), AppConstants.TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT));
                    cbPrepDate.setClickable(false);
                    if (isNetworkConnected(true)) getBaseActivity().doSyncAppDB(false, flag1 -> {
                        switch (flag1) {
                            case AppConstants.ONLINE:
                                updateCropDetail();
                                break;

                            case AppConstants.OFFLINE:
                                cropInfoBean.setPreparationDate(tvPrepDate.getText().toString().trim());
                                updateCropHistoryDB(false, cropInfoBean);
                                break;
                        }
                    });
                    else {
                        cropInfoBean.setPreparationDate(tvPrepDate.getText().toString().trim());
                        updateCropHistoryDB(false, cropInfoBean);
                    }
                    break;

                case "plantingDate":
                    imgPlantingDate.setVisibility(View.GONE);
                    tvPlantingDate.setText(CalenderUtils.formatDate(String.valueOf(date), AppConstants.TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT));
                    cbPlantingDate.setClickable(false);
                    if (isNetworkConnected(true)) getBaseActivity().doSyncAppDB(false, flag1 -> {
                        switch (flag1) {
                            case AppConstants.ONLINE:
                                updateCropDetail();
                                break;

                            case AppConstants.OFFLINE:
                                cropInfoBean.setPlantingDate(tvPlantingDate.getText().toString().trim());
                                updateCropHistoryDB(false, cropInfoBean);
                                break;
                        }
                    });
                    else {
                        cropInfoBean.setPlantingDate(tvPlantingDate.getText().toString().trim());
                        updateCropHistoryDB(false, cropInfoBean);
                    }
                    break;

                case "harvestDate":
                    imgHarvestDate.setVisibility(View.GONE);
                    tvHarvestDate.setText(CalenderUtils.formatDate(String.valueOf(date), AppConstants.TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT));
                    cbHarvestDate.setClickable(false);
                    if (isNetworkConnected(true)) getBaseActivity().doSyncAppDB(false, flag1 -> {
                        switch (flag1) {
                            case AppConstants.ONLINE:
                                updateCropDetail();
                                break;

                            case AppConstants.OFFLINE:
                                cropInfoBean.setHarvestDate(tvHarvestDate.getText().toString().trim());
                                updateCropHistoryDB(false, cropInfoBean);
                                break;
                        }
                    });
                    else {
                        cropInfoBean.setHarvestDate(tvHarvestDate.getText().toString().trim());
                        updateCropHistoryDB(false, cropInfoBean);
                    }
                    break;

                case "saleDate":
                    imgSaleDate.setVisibility(View.GONE);
                    tvSaleDate.setText(CalenderUtils.formatDate(String.valueOf(date), AppConstants.TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT));
                    cbSale.setClickable(false);
                    if (isNetworkConnected(true)) getBaseActivity().doSyncAppDB(false, flag1 -> {
                        switch (flag1) {
                            case AppConstants.ONLINE:
                                updateCropDetail();
                                break;

                            case AppConstants.OFFLINE:
                                cropInfoBean.setSaleTotalPrice(tvSaleAmt.getText().toString());
                                cropInfoBean.setSaleQuantity(etSaleQty.getText().toString().trim());
                                cropInfoBean.setSalePrice(etSalePrice.getText().toString().trim());
                                cropInfoBean.setSaleDate(tvSaleDate.getText().toString().trim());
                                updateCropHistoryDB(false, cropInfoBean);
                                break;
                        }
                    });
                    else {
                        cropInfoBean.setSaleTotalPrice(tvSaleAmt.getText().toString());
                        cropInfoBean.setSaleQuantity(etSaleQty.getText().toString().trim());
                        cropInfoBean.setSalePrice(etSalePrice.getText().toString().trim());
                        cropInfoBean.setSaleDate(tvSaleDate.getText().toString().trim());
                        updateCropHistoryDB(false, cropInfoBean);
                    }
                    break;
            }

        }, year, month, day);

        Date maxDate = CalenderUtils.getDateFormat(CalenderUtils.getCurrentDate(), AppConstants.TIMESTAMP_FORMAT);
        assert maxDate != null;
        datePickerDialog.getDatePicker().setMaxDate(maxDate.getTime());

        datePickerDialog.show();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        try {
            if (!s.toString().startsWith(".")) {
                long price = etSalePrice.getText().toString().isEmpty() ? 0 : Double.valueOf(etSalePrice.getText().toString()).longValue();
                long qty = etSaleQty.getText().toString().isEmpty() ? 0 : Double.valueOf(etSaleQty.getText().toString()).longValue();

                tvSaleAmt.setText(String.valueOf(decimalFormat.format(price * qty)));
            }
        } catch (Exception ignored) {}
    }

    @Override
    public void onBackPress() {
        if (cropInfoBean != null) cropInfoFragment.hitApi();
    }

    private void updateCropHistoryDB(Boolean isSync, CropInfoResponse.CroplListBean cropInfoBean) {
        //Todo manage local db update all data
        new Thread(() -> {
            Crop crop = new Crop();

            crop.setCropId(cropInfoBean.getCropId());
            crop.setParcel_id(cropInfoBean.getParcel_id());
            crop.setCropName(cropInfoBean.getCropName());
            crop.setLotId(cropInfoBean.getLotId());
            crop.setDescription(cropInfoBean.getDescription());
            crop.setPreparationDate(cropInfoBean.getPreparationDate().contains("-") ? cropInfoBean.getPreparationDate() : CalenderUtils.formatDate(cropInfoBean.getPreparationDate(), AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT));
            crop.setPlantingDate(cropInfoBean.getPlantingDate().contains("-") ? cropInfoBean.getPlantingDate() : CalenderUtils.formatDate(cropInfoBean.getPlantingDate(), AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT));
            crop.setHarvestDate(cropInfoBean.getHarvestDate().contains("-") ? cropInfoBean.getHarvestDate() : CalenderUtils.formatDate(cropInfoBean.getHarvestDate(), AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT));
            crop.setSaleDate(cropInfoBean.getSaleDate().isEmpty() ? null : CalenderUtils.getDateFormat(cropInfoBean.getSaleDate().contains("/") ? CalenderUtils.formatDate(cropInfoBean.getSaleDate(), AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT) : cropInfoBean.getSaleDate(), AppConstants.SERVER_TIMESTAMP_FORMAT));
            crop.setSalePrice(cropInfoBean.getSalePrice());
            crop.setSaleQuantity(cropInfoBean.getSaleQuantity());
            crop.setSaleTotalPrice(cropInfoBean.getSaleTotalPrice());
            crop.setCostFrom(cropInfoBean.getCostFrom().contains("-")?cropInfoBean.getCostFrom():CalenderUtils.formatDate(cropInfoBean.getCostFrom(), AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT));
            crop.setCostTo(cropInfoBean.getCostTo().contains("-")?cropInfoBean.getCostTo():CalenderUtils.formatDate(cropInfoBean.getCostTo(), AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT));
            crop.setCostTotal(cropInfoBean.getCostTotal());
            crop.setStatus(cropInfoBean.getStatus());
            crop.setRecordKey(cropInfoBean.getRecordKey());
            crop.setCrd(cropInfoBean.getCrd());
            crop.setUpd(cropInfoBean.getUpd());
            crop.setEvent(cropInfoBean.getCropId().equals(cropInfoBean.getRecordKey()) ? AppConstants.DB_EVENT_ADD : AppConstants.DB_EVENT_EDIT);
            crop.setDataSync(isSync ? AppConstants.DB_SYNC_TRUE : AppConstants.DB_SYNC_FALSE);

            getDataManager().updateCrop(crop);

            //set sync status true
            getDataManager().setLocalDataExistForSync(true);
            AppLogger.d("database", "crop detail update success");

            new Handler(Looper.getMainLooper()).post(() -> {
                if (!isSync)
                    CommonUtils.showToast(getBaseActivity(), getString(R.string.cropHistSuccessMsg), Toast.LENGTH_SHORT);
            });
        }).start();
    }
}
