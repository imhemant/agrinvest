package com.agrinvestapp.ui.crop.fragment.treatments;


import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.CropInfoResponse;
import com.agrinvestapp.data.model.api.CropTreatmentInfoResponse;
import com.agrinvestapp.data.model.db.CropTreatment;
import com.agrinvestapp.data.model.db.CropTreatmentMapping;
import com.agrinvestapp.ui.base.BaseFragment;
import com.agrinvestapp.ui.base.dialog.DeleteDialog;
import com.agrinvestapp.ui.crop.fragment.treatments.adapter.CropTreatmentAdapter;
import com.agrinvestapp.ui.crop.fragment.treatments.dialog.CropTreatmentCallback;
import com.agrinvestapp.ui.crop.fragment.treatments.dialog.CropTreatmentNameDialog;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.AppLogger;
import com.agrinvestapp.utils.CalenderUtils;
import com.agrinvestapp.utils.CommonUtils;
import com.agrinvestapp.volley.VolleyJsonObjectListener;
import com.agrinvestapp.volley.VolleyRequest;
import com.agrinvestapp.volley.VolleyResponseListener;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CropTreatmentFragment extends BaseFragment implements View.OnClickListener, CropTreatmentCallback {

    private TextView tvNoRecord;
    private CropInfoResponse.CroplListBean cropInfoBean;
    private List<CropTreatmentInfoResponse.TreatmentDetailListBean> list;
    private CropTreatmentAdapter treatmentAdapter;
    private RecyclerView rvTreatments;
    private Handler handler = new Handler(Looper.getMainLooper());

    public static CropTreatmentFragment newInstance(CropInfoResponse.CroplListBean cropInfoBean) {
        Bundle args = new Bundle();
        args.putParcelable(AppConstants.CROP_MODEL, cropInfoBean);
        CropTreatmentFragment fragment = new CropTreatmentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments()!=null){
            cropInfoBean = getArguments().getParcelable(AppConstants.CROP_MODEL);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_crop_treatment, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        /* toolbar view start */
        ImageView imgBack = view.findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);
        imgBack.setVisibility(View.VISIBLE);

        TextView tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.treatments);

        FrameLayout frameAdd = view.findViewById(R.id.frameAddT);
        frameAdd.setVisibility(View.VISIBLE);
        frameAdd.setOnClickListener(this);
        /* toolbar view end */
        view.findViewById(R.id.rlMain).setOnClickListener(this); //avoid bg click
        tvNoRecord = view.findViewById(R.id.tvNoRecord);

        list = new ArrayList<>();

        rvTreatments = view.findViewById(R.id.rvTreatments);
        treatmentAdapter = new CropTreatmentAdapter(list, new CropTreatmentAdapter.ItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                getBaseActivity().addFragment(CropTreatmentDetailFragment.newInstance(CropTreatmentFragment.this, list.get(pos)), R.id.cropFrame, true);
            }

            @Override
            public void onEditClick(int pos) {
                CropTreatmentNameDialog.newInstance(pos, list.get(pos).getTreatmentName(), CropTreatmentFragment.this)
                        .show(getChildFragmentManager());
            }

            @Override
            public void onDeleteClick(int pos) {
                DeleteDialog.newInstance(false, "", () -> {
                    if (isNetworkConnected(true)) {
                        getBaseActivity().doSyncAppDB(false, flag -> {
                            switch (flag) {
                                case AppConstants.ONLINE:
                                    doAddEditDelTreatmentApi(pos, AppConstants.REQUEST_TYPE_DEL, list.get(pos).getTreatmentName(), list.get(pos).getRecordKey());
                                    break;

                                case AppConstants.OFFLINE:
                                    doAddEditDelCropTreatmentLocalDB(pos, AppConstants.DB_EVENT_DEL, "");
                                    break;
                            }
                        });
                    } else {
                        doAddEditDelCropTreatmentLocalDB(pos, AppConstants.DB_EVENT_DEL, "");
                    }
                }).show(getChildFragmentManager());
            }
        });

        rvTreatments.setAdapter(treatmentAdapter);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hitCropTreatmentApi();
    }

    public void hitCropTreatmentApi() {
        if (isNetworkConnected(true)) {
            getBaseActivity().doSyncAppDB(false, flag -> {
                switch (flag) {
                    case AppConstants.ONLINE:
                        getCropTreatmentInfo();
                        break;

                    case AppConstants.OFFLINE:
                        getCropTreatmentInfoFromDB();
                        break;
                }
            });
        } else {
            getCropTreatmentInfoFromDB();
        }
    }

    private void getCropTreatmentInfo() {
        setLoading(true);

        HashMap<String, String> mParameterMap = new HashMap<>();
        mParameterMap.put("recordKey", cropInfoBean.getRecordKey());

        getDataManager().doServerCropTreatmentInfoApiCall(getDataManager().getHeader(), mParameterMap)
                .getAsResponse(new VolleyResponseListener() {
                    @Override
                    public void onVolleyResponse(String response) {
                        setLoading(false);
                        CropTreatmentInfoResponse infoResponse = getDataManager().mGson.fromJson(response, CropTreatmentInfoResponse.class);

                        if (infoResponse.getStatus().equals("success")) {
                            list.clear();
                            list.addAll(infoResponse.getTreatmentDetailList());

                            updateUi();
                            if (!list.isEmpty()) {
                                //Todo manage local db add all data
                                doAddCropTreatmentsLocalDB(infoResponse);
                            }

                            treatmentAdapter.notifyDataSetChanged();
                        } else {
                            updateUi();
                            if (infoResponse.getMessage().equals(getString(R.string.no_record_found))) {
                                list.clear();
                                treatmentAdapter.notifyDataSetChanged();
                            }
                            if (!infoResponse.getMessage().equals(getString(R.string.no_record_found)))
                                CommonUtils.showToast(getBaseActivity(), infoResponse.getMessage(), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        updateUi();
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    private void doAddEditDelTreatmentApi(int pos, String requestType, String name, String treatRk) {
        setLoading(true);

        HashMap<String, String> mParameterMap = new HashMap<>();
        mParameterMap.put("requestType", requestType);
        mParameterMap.put("recordKey", cropInfoBean.getRecordKey());
        mParameterMap.put("treatmentName", name);
        mParameterMap.put("treatmentRecordKey", treatRk);

        getDataManager().doServerCropTreatmentAddEditDelApiCall(getDataManager().getHeader(), mParameterMap)
                .getAsJsonObject(new VolleyJsonObjectListener() {

                    @Override
                    public void onVolleyJsonObject(JSONObject jsonObject) {
                        setLoading(false);
                        try {
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            if (status.equals("success")) {
                                switch (requestType) {
                                    case AppConstants.DB_EVENT_ADD:
                                        JSONObject obj = jsonObject.getJSONObject("treatmentDetail");
                                        CropTreatmentInfoResponse.TreatmentDetailListBean bean = new CropTreatmentInfoResponse.TreatmentDetailListBean();

                                        bean.setTreatmentId(obj.getString("treatmentId"));
                                        bean.setCrop_id(obj.getString("crop_id"));
                                        bean.setTreatmentName(obj.getString("treatmentName"));
                                        bean.setRecordKey(obj.getString("recordKey"));
                                        bean.setCrd(obj.getString("crd"));
                                        bean.setUpd(obj.getString("upd"));
                                        list.add(pos, bean);
                                        treatmentAdapter.notifyItemInserted(pos);
                                        rvTreatments.smoothScrollToPosition(0);
                                        //Todo add to local db
                                        doAddEditDelCropTreatmentLocalDB(requestType, list.get(pos));

                                        CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                                        break;

                                    case AppConstants.DB_EVENT_EDIT:
                                        JSONObject obj2 = jsonObject.getJSONObject("treatmentDetail");
                                        CropTreatmentInfoResponse.TreatmentDetailListBean bean2 = new CropTreatmentInfoResponse.TreatmentDetailListBean();
                                        bean2.setTreatmentId(obj2.getString("treatmentId"));
                                        bean2.setCrop_id(obj2.getString("crop_id"));
                                        bean2.setTreatmentName(obj2.getString("treatmentName"));
                                        bean2.setRecordKey(obj2.getString("recordKey"));
                                        bean2.setCrd(obj2.getString("crd"));
                                        bean2.setUpd(obj2.getString("upd"));

                                        bean2.setTreatmentList(list.get(pos).getTreatmentList());
                                        list.set(pos, bean2);
                                        treatmentAdapter.notifyItemChanged(pos);
                                        //Todo update to local db
                                        doAddEditDelCropTreatmentLocalDB(requestType, list.get(pos));

                                        CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                                        break;

                                    case AppConstants.DB_EVENT_DEL:
                                        //Todo del to local db
                                        doAddEditDelCropTreatmentLocalDB(requestType, list.get(pos));

                                        list.remove(pos);
                                        treatmentAdapter.notifyItemRemoved(pos);

                                        CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                                        break;
                                }
                                updateUi();
                            } else {
                                CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                            }
                        } catch (Exception e) {
                            CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyJsonException() {
                        setLoading(false);
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    private void updateUi() {
        tvNoRecord.setVisibility(list.isEmpty() ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                getBaseActivity().onBackPressed();
                break;

            case R.id.frameAddT:
                if (list.size() < 10) {
                    CropTreatmentNameDialog.newInstance(0, "", CropTreatmentFragment.this).show(getChildFragmentManager());
                } else
                    CommonUtils.showToast(getBaseActivity(), getString(R.string.cropTreatmentLimit), Toast.LENGTH_SHORT);
                break;
        }
    }

    @Override
    public void onAdd(String name) {
        if (isNetworkConnected(true)) {
            getBaseActivity().doSyncAppDB(false, flag -> {
                switch (flag) {
                    case AppConstants.ONLINE:
                        doAddEditDelTreatmentApi(0, AppConstants.REQUEST_TYPE_ADD, name, CalenderUtils.getTimestamp());
                        break;

                    case AppConstants.OFFLINE:
                        doAddEditDelCropTreatmentLocalDB(0, AppConstants.DB_EVENT_ADD, name);
                        break;
                }
            });
        } else {
            doAddEditDelCropTreatmentLocalDB(0, AppConstants.DB_EVENT_ADD, name);
        }
    }

    @Override
    public void onEdit(int pos, String name) {
        if (isNetworkConnected(true)) {
            getBaseActivity().doSyncAppDB(false, flag -> {
                switch (flag) {
                    case AppConstants.ONLINE:
                        doAddEditDelTreatmentApi(pos, AppConstants.REQUEST_TYPE_EDIT, name, list.get(pos).getRecordKey());
                        break;

                    case AppConstants.OFFLINE:
                        doAddEditDelCropTreatmentLocalDB(pos, AppConstants.DB_EVENT_EDIT, name);
                        break;
                }
            });
        } else {
            doAddEditDelCropTreatmentLocalDB(pos, AppConstants.DB_EVENT_EDIT, name);
        }
    }

    /*Local DB Start here*/
    private void getCropTreatmentInfoFromDB() {
        setLoading(true);

        new Thread(() -> {
            list.clear();
            List<CropTreatmentInfoResponse.TreatmentDetailListBean> treatmentList = getDataManager().getCropTreatmentList(cropInfoBean.getCropId());

            List<CropTreatmentInfoResponse.TreatmentDetailListBean> tempList = new ArrayList<>();
            for (CropTreatmentInfoResponse.TreatmentDetailListBean tempBean : treatmentList) {
                List<CropTreatmentInfoResponse.TreatmentDetailListBean.TreatmentListBean> treatmentMappingList = getDataManager().getCropTreatmentDetailList(tempBean.getTreatmentId());
                tempBean.setTreatmentList(treatmentMappingList);
                tempList.add(tempBean);
            }
            list.addAll(tempList);

            handler.post(() -> {
                setLoading(false);
                treatmentAdapter.notifyDataSetChanged();
                updateUi();
            });
        }).start();
    }

    private void doAddCropTreatmentsLocalDB(CropTreatmentInfoResponse infoResponse) {
        new Thread(() -> {
            getDataManager().deleteAllCropTreatment();
            getDataManager().deleteAllCropTreatmentDetail();

            List<CropTreatment> treatmentList = new ArrayList<>();
            List<CropTreatmentMapping> treatmentMappingList = new ArrayList<>();
            for (CropTreatmentInfoResponse.TreatmentDetailListBean mainBean : infoResponse.getTreatmentDetailList()) {
                CropTreatment treatment = new CropTreatment();

                treatment.setTreatmentId(mainBean.getTreatmentId());
                treatment.setCrop_id(mainBean.getCrop_id());
                treatment.setTreatmentName(mainBean.getTreatmentName());
                treatment.setRecordKey(mainBean.getRecordKey());
                treatment.setCrd(mainBean.getCrd());
                treatment.setUpd(mainBean.getUpd());
                treatment.setDataSync(AppConstants.DB_SYNC_TRUE);
                treatment.setEvent(AppConstants.DB_EVENT_ADD);

                treatmentList.add(treatment);

                for (CropTreatmentInfoResponse.TreatmentDetailListBean.TreatmentListBean subBean : mainBean.getTreatmentList()) {
                    CropTreatmentMapping treatmentMapping = new CropTreatmentMapping();

                    treatmentMapping.setTreatmentMappingId(subBean.getTreatmentMappingId());
                    treatmentMapping.setTreatment_id(subBean.getTreatment_id());
                    treatmentMapping.setTreatmentDate(subBean.getTreatmentDate());
                    treatmentMapping.setTreatmentDescription(subBean.getTreatmentDescription());
                    treatmentMapping.setRecordKey(subBean.getRecordKey());
                    treatmentMapping.setCrd(subBean.getCrd());
                    treatmentMapping.setUpd(subBean.getUpd());
                    treatmentMapping.setDataSync(AppConstants.DB_SYNC_TRUE);
                    treatmentMapping.setEvent(AppConstants.DB_EVENT_ADD);

                    treatmentMappingList.add(treatmentMapping);
                }
            }

            getDataManager().insertAllCropTreatment(treatmentList);
            getDataManager().insertAllCropTreatmentDetail(treatmentMappingList);
            AppLogger.d("database", "treatment and treatment detail success");
        }).start();
    }

    private void doAddEditDelCropTreatmentLocalDB(String flag, CropTreatmentInfoResponse.TreatmentDetailListBean mainBean) {
        new Thread(() -> {

            switch (flag) {
                case AppConstants.REQUEST_TYPE_ADD:
                    CropTreatment treatment = new CropTreatment();
                    treatment.setTreatmentId(mainBean.getTreatmentId());
                    treatment.setCrop_id(mainBean.getCrop_id());
                    treatment.setTreatmentName(mainBean.getTreatmentName());
                    treatment.setRecordKey(mainBean.getRecordKey());
                    treatment.setCrd(mainBean.getCrd());
                    treatment.setUpd(mainBean.getUpd());
                    treatment.setDataSync(AppConstants.DB_SYNC_TRUE);
                    treatment.setEvent(AppConstants.DB_EVENT_ADD);

                    getDataManager().insertCropTreatment(treatment);
                    AppLogger.d("database", "Treament add success");
                    break;

                case AppConstants.REQUEST_TYPE_EDIT:
                    treatment = new CropTreatment();
                    treatment.setTreatmentId(mainBean.getTreatmentId());
                    treatment.setCrop_id(mainBean.getCrop_id());
                    treatment.setTreatmentName(mainBean.getTreatmentName());
                    treatment.setRecordKey(mainBean.getRecordKey());
                    treatment.setCrd(mainBean.getCrd());
                    treatment.setUpd(mainBean.getUpd());
                    treatment.setDataSync(AppConstants.DB_SYNC_TRUE);
                    treatment.setEvent(AppConstants.DB_EVENT_EDIT);

                    getDataManager().updateCropTreatment(treatment);
                    AppLogger.d("database", "Treament update success");
                    break;

                case AppConstants.REQUEST_TYPE_DEL:
                    getDataManager().deleteUsingCropTreatmentId(mainBean.getTreatmentId());
                    AppLogger.d("database", "Treament delete success");
                    break;
            }
        }).start();
    }

    private void doAddEditDelCropTreatmentLocalDB(int pos, String flag, String name) {
        new Thread(() -> {

            switch (flag) {
                case AppConstants.REQUEST_TYPE_ADD:
                    String timestamp = CalenderUtils.getTimestamp();
                    CropTreatment treatment = new CropTreatment();
                    treatment.setTreatmentId(timestamp);
                    treatment.setCrop_id(cropInfoBean.getCropId());
                    treatment.setTreatmentName(name);
                    treatment.setRecordKey(timestamp);
                    treatment.setCrd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    treatment.setUpd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    treatment.setDataSync(AppConstants.DB_SYNC_FALSE);
                    treatment.setEvent(AppConstants.DB_EVENT_ADD);

                    getDataManager().insertCropTreatment(treatment);

                    //set sync status true
                    getDataManager().setLocalDataExistForSync(true);
                    AppLogger.d("database", "Treament add success");

                    CropTreatmentInfoResponse.TreatmentDetailListBean bean = new CropTreatmentInfoResponse.TreatmentDetailListBean();

                    bean.setTreatmentId(timestamp);
                    bean.setCrop_id(cropInfoBean.getCropId());
                    bean.setTreatmentName(name);
                    bean.setRecordKey(timestamp);
                    bean.setCrd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    bean.setUpd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    list.add(pos, bean);
                    handler.post(() -> {
                        treatmentAdapter.notifyItemInserted(pos);
                        rvTreatments.smoothScrollToPosition(0);
                        updateUi();
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.cropTreatmentAddSuccessMsg), Toast.LENGTH_SHORT);
                    });
                    break;

                case AppConstants.REQUEST_TYPE_EDIT:
                    treatment = new CropTreatment();
                    CropTreatmentInfoResponse.TreatmentDetailListBean tempBean = list.get(pos);
                    treatment.setTreatmentId(tempBean.getTreatmentId());
                    treatment.setCrop_id(cropInfoBean.getCropId());
                    treatment.setTreatmentName(name);
                    treatment.setRecordKey(tempBean.getRecordKey());
                    treatment.setCrd(tempBean.getCrd());
                    treatment.setUpd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    treatment.setDataSync(AppConstants.DB_SYNC_FALSE);
                    treatment.setEvent(tempBean.getTreatmentId().equals(tempBean.getRecordKey()) ? AppConstants.DB_EVENT_ADD : AppConstants.DB_EVENT_EDIT);

                    getDataManager().updateCropTreatment(treatment);

                    //set sync status true
                    getDataManager().setLocalDataExistForSync(true);
                    AppLogger.d("database", "Treament update success");


                    bean = new CropTreatmentInfoResponse.TreatmentDetailListBean();
                    bean.setTreatmentId(tempBean.getTreatmentId());
                    bean.setCrop_id(cropInfoBean.getCropId());
                    bean.setTreatmentName(name);
                    bean.setRecordKey(tempBean.getRecordKey());
                    bean.setCrd(tempBean.getCrd());
                    bean.setUpd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    list.set(pos, bean);
                    handler.post(() -> {
                        treatmentAdapter.notifyItemChanged(pos);
                        updateUi();
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.cropTreatmentUpdateSuccessMsg), Toast.LENGTH_SHORT);
                    });
                    break;

                case AppConstants.REQUEST_TYPE_DEL:
                    tempBean = list.get(pos);
                    if (tempBean.getTreatmentId().equals(tempBean.getRecordKey())) {
                        getDataManager().deleteUsingCropTreatmentId(tempBean.getTreatmentId());
                    } else {
                        treatment = new CropTreatment();
                        treatment.setTreatmentId(tempBean.getTreatmentId());
                        treatment.setCrop_id(tempBean.getCrop_id());
                        treatment.setTreatmentName(name);
                        treatment.setRecordKey(tempBean.getRecordKey());
                        treatment.setCrd(tempBean.getCrd());
                        treatment.setUpd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                        treatment.setDataSync(AppConstants.DB_SYNC_FALSE);
                        treatment.setEvent(AppConstants.DB_EVENT_DEL);

                        getDataManager().updateCropTreatment(treatment);

                        //set sync status true
                        getDataManager().setLocalDataExistForSync(true);
                    }
                    AppLogger.d("database", "Treament delete success");

                    list.remove(pos);
                    handler.post(() -> {
                        treatmentAdapter.notifyItemRemoved(pos);
                        updateUi();
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.cropTreatmentDelSuccessMsg), Toast.LENGTH_SHORT);
                    });
                    break;
            }
        }).start();
    }
    /*Local DB End here*/
}
