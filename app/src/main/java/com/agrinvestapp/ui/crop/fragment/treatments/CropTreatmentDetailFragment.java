package com.agrinvestapp.ui.crop.fragment.treatments;


import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.CropTreatmentDetailResponse;
import com.agrinvestapp.data.model.api.CropTreatmentInfoResponse;
import com.agrinvestapp.data.model.api.TreatmentDetailResponse;
import com.agrinvestapp.data.model.db.CropTreatment;
import com.agrinvestapp.data.model.db.CropTreatmentMapping;
import com.agrinvestapp.ui.base.BaseFragment;
import com.agrinvestapp.ui.base.dialog.DeleteDialog;
import com.agrinvestapp.ui.crop.fragment.treatments.adapter.CropTreatmentDetailAdapter;
import com.agrinvestapp.ui.crop.fragment.treatments.dialog.CropTreatmentDetailCallback;
import com.agrinvestapp.ui.crop.fragment.treatments.dialog.CropTreatmentsDialog;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.AppLogger;
import com.agrinvestapp.utils.CalenderUtils;
import com.agrinvestapp.utils.CommonUtils;
import com.agrinvestapp.volley.VolleyRequest;
import com.agrinvestapp.volley.VolleyResponseListener;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CropTreatmentDetailFragment extends BaseFragment implements View.OnClickListener, BaseFragment.FragmentCallback,CropTreatmentDetailCallback {

    private static final String MODEL = "model";

    private TextView tvNoRecord;

    private CropTreatmentInfoResponse.TreatmentDetailListBean treatmentsBean;
    private List<CropTreatmentDetailResponse.TreatmentDetailBean> list;
    private CropTreatmentDetailAdapter treatmentAdapter;
    private RecyclerView rvTreatmentDetail;
    private CropTreatmentFragment cropTreatmentFragment;

    private Handler handler = new Handler(Looper.getMainLooper());

    public static CropTreatmentDetailFragment newInstance(CropTreatmentFragment cropTreatmentFragment, CropTreatmentInfoResponse.TreatmentDetailListBean treatmentDetailBean) {

        Bundle args = new Bundle();
        args.putParcelable(MODEL, treatmentDetailBean);
        CropTreatmentDetailFragment fragment = new CropTreatmentDetailFragment();
        fragment.setInstance(cropTreatmentFragment);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            treatmentsBean = getArguments().getParcelable(MODEL);
        }
    }

    private void setInstance(CropTreatmentFragment cropTreatmentFragment) {
        this.cropTreatmentFragment = cropTreatmentFragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_crop_treatment_detail, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        /* toolbar view start */
        ImageView imgBack = view.findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);
        imgBack.setVisibility(View.VISIBLE);

        TextView tvTitle = view.findViewById(R.id.tvTitle);
        assert treatmentsBean != null;
        tvTitle.setText(treatmentsBean.getTreatmentName());

        FrameLayout frameAdd = view.findViewById(R.id.frameAddT);
        frameAdd.setVisibility(View.VISIBLE);
        frameAdd.setOnClickListener(this);
        /* toolbar view end */
        view.findViewById(R.id.rlMain).setOnClickListener(this); //avoid bg click
        tvNoRecord = view.findViewById(R.id.tvNoRecord);

        list = new ArrayList<>();

        rvTreatmentDetail = view.findViewById(R.id.rvTreatmentDetail);
        treatmentAdapter = new CropTreatmentDetailAdapter(list, new CropTreatmentDetailAdapter.ItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                //Not require
            }

            @Override
            public void onEditClick(int pos) {
                CropTreatmentsDialog.newInstance(pos, treatmentsBean, list.get(pos), CropTreatmentDetailFragment.this)
                        .show(getChildFragmentManager());
            }

            @Override
            public void onDeleteClick(int pos) {
                DeleteDialog.newInstance(false, "", () -> {
                    if (isNetworkConnected(true)) {
                        getBaseActivity().doSyncAppDB(false, flag -> {
                            switch (flag) {
                                case AppConstants.ONLINE:
                                    doAddTreatmentDetails(pos, AppConstants.REQUEST_TYPE_DEL, null);
                                    break;

                                case AppConstants.OFFLINE:
                                    TreatmentDetailResponse.TreatmentDetailBean treatmentDetailBean = null;
                                    doAddEditDelTreatmentLocalDB(pos, AppConstants.REQUEST_TYPE_DEL, treatmentDetailBean);
                                    break;
                            }
                        });
                    } else {
                        TreatmentDetailResponse.TreatmentDetailBean treatmentDetailBean = null;
                        doAddEditDelTreatmentLocalDB(pos, AppConstants.REQUEST_TYPE_DEL, treatmentDetailBean);
                    }
                }).show(getChildFragmentManager());
            }
        });

        rvTreatmentDetail.setAdapter(treatmentAdapter);

        updateTreatmentDetailList();
    }

    private void updateTreatmentDetailList() {
        new Thread(() -> {
            if (treatmentsBean != null && treatmentsBean.getTreatmentList() != null) {
                List<CropTreatmentDetailResponse.TreatmentDetailBean> tempList = new ArrayList<>();
                for (CropTreatmentInfoResponse.TreatmentDetailListBean.TreatmentListBean tempBean : treatmentsBean.getTreatmentList()) {
                    CropTreatmentDetailResponse.TreatmentDetailBean beanList = new CropTreatmentDetailResponse.TreatmentDetailBean();

                    beanList.setTreatmentMappingId(tempBean.getTreatmentMappingId());
                    beanList.setTreatment_id(tempBean.getTreatment_id());
                    beanList.setTreatmentDate(tempBean.getTreatmentDate());
                    beanList.setTreatmentDescription(tempBean.getTreatmentDescription());
                    beanList.setRecordKey(tempBean.getRecordKey());
                    beanList.setCrd(tempBean.getCrd());
                    beanList.setUpd(tempBean.getUpd());

                    tempList.add(beanList);
                }

                if (!tempList.isEmpty()) {
                    list.clear();
                    list.addAll(tempList);
                }
            }

            handler.post(() -> {
                treatmentAdapter.notifyDataSetChanged();
                updateUi();
            });
        }).start();

    }

    private void doAddTreatmentDetails(int pos, String flag, TreatmentDetailResponse.TreatmentDetailBean treatmentDetailBean) {
        setLoading(true);
        HashMap<String, String> mParams = new HashMap<>();
        mParams.put("requestType", flag);
        mParams.put("recordKey", treatmentsBean.getRecordKey());

        switch (flag) {
            case AppConstants.REQUEST_TYPE_ADD:
                mParams.put("treatmentDate", treatmentDetailBean.getTreatmentDate());
                mParams.put("treatmentDescription", treatmentDetailBean.getTreatmentDescription());
                mParams.put("mappingRecordKey", CalenderUtils.getTimestamp());
                break;

            case AppConstants.REQUEST_TYPE_EDIT:
                mParams.put("mappingRecordKey", list.get(pos).getRecordKey());
                mParams.put("treatmentDate", treatmentDetailBean.getTreatmentDate());
                mParams.put("treatmentDescription", treatmentDetailBean.getTreatmentDescription());
                break;

            case AppConstants.REQUEST_TYPE_DEL:
                mParams.put("mappingRecordKey", list.get(pos).getRecordKey());
                mParams.put("treatmentDate", list.get(pos).getTreatmentDate());
                mParams.put("treatmentDescription", list.get(pos).getTreatmentDescription());
                break;
        }

        getDataManager().doServerCropTreatmentDetailAddEditDelApiCall(getDataManager().getHeader(), mParams)
                .getAsResponse(new VolleyResponseListener() {
                    @Override
                    public void onVolleyResponse(String response) {
                        setLoading(false);

                        CropTreatmentDetailResponse infoResponse = getDataManager().mGson.fromJson(response, CropTreatmentDetailResponse.class);

                        if (infoResponse.getStatus().equals("success")) {
                            switch (flag) {
                                case AppConstants.REQUEST_TYPE_ADD:
                                    list.add(pos, infoResponse.getTreatmentDetail());
                                    treatmentAdapter.notifyItemInserted(pos);
                                    rvTreatmentDetail.smoothScrollToPosition(0);
                                    CommonUtils.showToast(getBaseActivity(), infoResponse.getMessage(), Toast.LENGTH_SHORT);
                                    //Todo add to local db
                                    doAddEditDelTreatmentLocalDB(pos, flag, list);
                                    break;

                                case AppConstants.REQUEST_TYPE_EDIT:
                                    list.set(pos, infoResponse.getTreatmentDetail());
                                    treatmentAdapter.notifyItemChanged(pos);
                                    CommonUtils.showToast(getBaseActivity(), infoResponse.getMessage(), Toast.LENGTH_SHORT);
                                    //Todo update to local db
                                    doAddEditDelTreatmentLocalDB(pos, flag, list);
                                    break;

                                case AppConstants.REQUEST_TYPE_DEL:
                                    //Todo del to local db
                                    doAddEditDelTreatmentLocalDB(pos, flag, list);

                                    list.remove(pos);
                                    treatmentAdapter.notifyItemRemoved(pos);
                                    CommonUtils.showToast(getBaseActivity(), infoResponse.getMessage(), Toast.LENGTH_SHORT);
                                    break;
                            }
                            updateUi();

                        } else {
                            updateUi();
                            if (infoResponse.getMessage().equals(getString(R.string.no_record_found))) {
                                list.clear();
                                treatmentAdapter.notifyDataSetChanged();
                            }
                            if (!infoResponse.getMessage().equals(getString(R.string.no_record_found)))
                                CommonUtils.showToast(getBaseActivity(), infoResponse.getMessage(), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    private void updateUi() {
        tvNoRecord.setVisibility(list.isEmpty() ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                getBaseActivity().onBackPressed();
                break;

            case R.id.frameAddT:
                if (list.size() < 20) {
                    CropTreatmentsDialog.newInstance(0, treatmentsBean, null, this)
                            .show(getChildFragmentManager());
                } else
                    CommonUtils.showToast(getBaseActivity(), getString(R.string.cropTreatmentDetailLimit), Toast.LENGTH_SHORT);
                break;
        }
    }

    @Override
    public void onBackPress() {
        if (cropTreatmentFragment != null) cropTreatmentFragment.hitCropTreatmentApi();
    }

    @Override
    public void onAdd(TreatmentDetailResponse.TreatmentDetailBean treatmentDetailBean) {
        if (isNetworkConnected(true)) {
            getBaseActivity().doSyncAppDB(false, flag -> {
                switch (flag) {
                    case AppConstants.ONLINE:
                        doAddTreatmentDetails(0, AppConstants.REQUEST_TYPE_ADD, treatmentDetailBean);
                        break;

                    case AppConstants.OFFLINE:
                        doAddEditDelTreatmentLocalDB(0, AppConstants.REQUEST_TYPE_ADD, treatmentDetailBean);
                        break;
                }
            });
        } else {
            doAddEditDelTreatmentLocalDB(0, AppConstants.REQUEST_TYPE_ADD, treatmentDetailBean);
        }
    }

    @Override
    public void onEdit(int pos, TreatmentDetailResponse.TreatmentDetailBean treatmentDetailBean) {
        if (isNetworkConnected(true)) {
            getBaseActivity().doSyncAppDB(false, flag -> {
                switch (flag) {
                    case AppConstants.ONLINE:
                        doAddTreatmentDetails(pos, AppConstants.REQUEST_TYPE_EDIT, treatmentDetailBean);
                        break;

                    case AppConstants.OFFLINE:
                        doAddEditDelTreatmentLocalDB(pos, AppConstants.REQUEST_TYPE_EDIT, treatmentDetailBean);
                        break;
                }
            });
        } else {
            doAddEditDelTreatmentLocalDB(pos, AppConstants.REQUEST_TYPE_EDIT, treatmentDetailBean);
        }
    }

    /*Local DB Start here*/
    private void doAddEditDelTreatmentLocalDB(int pos, String flag, List<CropTreatmentDetailResponse.TreatmentDetailBean> list) {
        new Thread(() -> {
            CropTreatmentMapping treatmentMapping = new CropTreatmentMapping();
            switch (flag) {
                case AppConstants.REQUEST_TYPE_ADD:
                    CropTreatmentDetailResponse.TreatmentDetailBean tempBean = list.get(pos);

                    treatmentMapping.setTreatmentMappingId(tempBean.getTreatmentMappingId());
                    treatmentMapping.setTreatment_id(tempBean.getTreatment_id());
                    treatmentMapping.setTreatmentDate(tempBean.getTreatmentDate());
                    treatmentMapping.setTreatmentDescription(tempBean.getTreatmentDescription());
                    treatmentMapping.setCrd(tempBean.getCrd());
                    treatmentMapping.setUpd(tempBean.getUpd());
                    treatmentMapping.setDataSync(AppConstants.DB_SYNC_TRUE);
                    treatmentMapping.setEvent(AppConstants.DB_EVENT_ADD);
                    getDataManager().insertCropTreatmentDetail(treatmentMapping);
                    AppLogger.d("database", "treatment detail add success");
                    break;

                case AppConstants.REQUEST_TYPE_EDIT:
                    tempBean = list.get(pos);

                    treatmentMapping.setTreatmentMappingId(tempBean.getTreatmentMappingId());
                    treatmentMapping.setTreatment_id(tempBean.getTreatment_id());
                    treatmentMapping.setTreatmentDate(tempBean.getTreatmentDate());
                    treatmentMapping.setTreatmentDescription(tempBean.getTreatmentDescription());
                    treatmentMapping.setCrd(tempBean.getCrd());
                    treatmentMapping.setUpd(tempBean.getUpd());
                    treatmentMapping.setDataSync(AppConstants.DB_SYNC_TRUE);
                    treatmentMapping.setEvent(AppConstants.DB_EVENT_EDIT);

                    getDataManager().updateCropTreatmentDetail(treatmentMapping);
                    AppLogger.d("database", "treatment detail update success");
                    break;

                case AppConstants.REQUEST_TYPE_DEL:
                    tempBean = list.get(pos);
                    getDataManager().deleteUsingCropTreatmentDetailMappingId(tempBean.getTreatmentMappingId());

                    handler.post(() -> {
                        list.remove(pos);
                        treatmentAdapter.notifyItemRemoved(pos);
                        updateUi();
                    });
                    AppLogger.d("database", "treatment detail delete success");
                    break;
            }

        }).start();
    }

    private void doAddEditDelTreatmentLocalDB(int pos, String flag, TreatmentDetailResponse.TreatmentDetailBean mainBean) {
        new Thread(() -> {
            CropTreatmentMapping treatmentMapping = new CropTreatmentMapping();

            switch (flag){
                case AppConstants.REQUEST_TYPE_ADD:

                    //for ui update
                    CropTreatmentDetailResponse.TreatmentDetailBean tdlBean = new CropTreatmentDetailResponse.TreatmentDetailBean();
                    tdlBean.setTreatmentMappingId(mainBean.getRecordKey());
                    tdlBean.setTreatment_id(treatmentsBean.getTreatmentId());
                    tdlBean.setTreatmentDate(mainBean.getTreatmentDate());
                    tdlBean.setTreatmentDescription(mainBean.getTreatmentDescription());
                    tdlBean.setRecordKey(mainBean.getRecordKey());
                    tdlBean.setCrd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    tdlBean.setUpd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));

                    list.add(pos, tdlBean);
                    handler.post(() -> {
                        treatmentAdapter.notifyItemInserted(pos);
                        rvTreatmentDetail.smoothScrollToPosition(0);
                        updateUi();
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.cropTreatmentAddSuccessMsg), Toast.LENGTH_SHORT);
                    });

                    treatmentMapping.setTreatmentMappingId(mainBean.getRecordKey());
                    treatmentMapping.setTreatment_id(treatmentsBean.getTreatmentId());
                    treatmentMapping.setTreatmentDate(mainBean.getTreatmentDate());
                    treatmentMapping.setTreatmentDescription(mainBean.getTreatmentDescription());
                    treatmentMapping.setRecordKey(mainBean.getRecordKey());
                    treatmentMapping.setCrd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    treatmentMapping.setUpd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    treatmentMapping.setDataSync(AppConstants.DB_SYNC_FALSE);
                    treatmentMapping.setEvent(AppConstants.DB_EVENT_ADD);
                    getDataManager().insertCropTreatmentDetail(treatmentMapping);

                    //update crop treatment flag
                    CropTreatment cTreatment = getDataManager().getCropTreatmentInfo(treatmentsBean.getTreatmentId());
                    cTreatment.setDataSync(AppConstants.DB_SYNC_FALSE);
                    cTreatment.setEvent((treatmentsBean.getTreatmentId().equals(cTreatment.getRecordKey())) ? AppConstants.DB_EVENT_ADD : AppConstants.DB_EVENT_EDIT);
                    getDataManager().updateCropTreatment(cTreatment);

                    //set sync status true
                    getDataManager().setLocalDataExistForSync(true);
                    AppLogger.d("database", "treatment detail add success");
                    break;

                case AppConstants.REQUEST_TYPE_EDIT:
                    CropTreatmentDetailResponse.TreatmentDetailBean myBean = list.get(pos);

                    //for ui update
                    tdlBean = new CropTreatmentDetailResponse.TreatmentDetailBean();
                    tdlBean.setTreatmentMappingId(myBean.getTreatmentMappingId());
                    tdlBean.setTreatment_id(myBean.getTreatment_id());
                    tdlBean.setTreatmentDate(mainBean.getTreatmentDate());
                    tdlBean.setTreatmentDescription(mainBean.getTreatmentDescription());
                    tdlBean.setRecordKey(myBean.getRecordKey());
                    tdlBean.setCrd(myBean.getCrd());
                    tdlBean.setUpd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));

                    list.set(pos, tdlBean);
                    handler.post(() -> {
                        treatmentAdapter.notifyItemChanged(pos);
                        updateUi();
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.cropTreatmentUpdateSuccessMsg), Toast.LENGTH_SHORT);
                    });

                    treatmentMapping.setTreatmentMappingId(myBean.getTreatmentMappingId());
                    treatmentMapping.setTreatment_id(myBean.getTreatment_id());
                    treatmentMapping.setTreatmentDate(mainBean.getTreatmentDate());
                    treatmentMapping.setTreatmentDescription(mainBean.getTreatmentDescription());
                    treatmentMapping.setRecordKey(myBean.getRecordKey());
                    treatmentMapping.setCrd(myBean.getCrd());
                    treatmentMapping.setUpd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    treatmentMapping.setDataSync(AppConstants.DB_SYNC_FALSE);
                    treatmentMapping.setEvent(myBean.getTreatmentMappingId().equals(myBean.getRecordKey()) ? AppConstants.DB_EVENT_ADD : AppConstants.DB_EVENT_EDIT);
                    getDataManager().updateCropTreatmentDetail(treatmentMapping);

                    //update crop treatment flag
                    cTreatment = getDataManager().getCropTreatmentInfo(myBean.getTreatment_id());
                    cTreatment.setDataSync(AppConstants.DB_SYNC_FALSE);
                    cTreatment.setEvent((myBean.getTreatment_id().equals(cTreatment.getRecordKey())) ? AppConstants.DB_EVENT_ADD : AppConstants.DB_EVENT_EDIT);
                    getDataManager().updateCropTreatment(cTreatment);

                    //set sync status true
                    getDataManager().setLocalDataExistForSync(true);
                    AppLogger.d("database", "treatment detail update success");
                    break;

                case AppConstants.REQUEST_TYPE_DEL:
                    myBean = list.get(pos);

                    if (myBean.getTreatmentMappingId().equals(myBean.getRecordKey())) {
                        getDataManager().deleteUsingCropTreatmentDetailMappingId(myBean.getTreatmentMappingId());
                    } else {

                        treatmentMapping.setTreatmentMappingId(myBean.getTreatmentMappingId());
                        treatmentMapping.setTreatment_id(myBean.getTreatment_id());
                        treatmentMapping.setTreatmentDate(myBean.getTreatmentDate());
                        treatmentMapping.setTreatmentDescription(myBean.getTreatmentDescription());
                        treatmentMapping.setRecordKey(myBean.getRecordKey());
                        treatmentMapping.setCrd(myBean.getCrd());
                        treatmentMapping.setUpd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                        treatmentMapping.setDataSync(AppConstants.DB_SYNC_FALSE);
                        treatmentMapping.setEvent(AppConstants.DB_EVENT_DEL);
                        getDataManager().updateCropTreatmentDetail(treatmentMapping);

                        //update crop treatment flag
                        cTreatment = getDataManager().getCropTreatmentInfo(myBean.getTreatment_id());
                        cTreatment.setDataSync(AppConstants.DB_SYNC_FALSE);
                        cTreatment.setEvent((myBean.getTreatment_id().equals(cTreatment.getRecordKey())) ? AppConstants.DB_EVENT_ADD : AppConstants.DB_EVENT_EDIT);
                        getDataManager().updateCropTreatment(cTreatment);

                        //set sync status true
                        getDataManager().setLocalDataExistForSync(true);
                    }
                    AppLogger.d("database", "treatment detail delete success");

                    handler.post(() -> {
                        list.remove(pos);
                        treatmentAdapter.notifyItemRemoved(pos);
                        updateUi();
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.cropTreatmentDelSuccessMsg), Toast.LENGTH_SHORT);
                    });
                    break;
            }


        }).start();
    }
    /*Local DB End here*/
}
