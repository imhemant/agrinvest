package com.agrinvestapp.ui.crop.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.CropInfoResponse;
import com.agrinvestapp.data.model.db.Crop;
import com.agrinvestapp.ui.base.BaseFragment;
import com.agrinvestapp.ui.base.ClickListener;
import com.agrinvestapp.ui.base.dialog.DeleteDialog;
import com.agrinvestapp.ui.base.dialog.PlanUpgradeDialog;
import com.agrinvestapp.ui.crop.CropActivity;
import com.agrinvestapp.ui.crop.adapter.CropInfoAdapter;
import com.agrinvestapp.ui.main.fragment.profile.PlanActivity;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.AppLogger;
import com.agrinvestapp.utils.AppUtils;
import com.agrinvestapp.utils.CalenderUtils;
import com.agrinvestapp.utils.CommonUtils;
import com.agrinvestapp.volley.VolleyJsonObjectListener;
import com.agrinvestapp.volley.VolleyRequest;
import com.agrinvestapp.volley.VolleyResponseListener;
import com.android.volley.VolleyError;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CropInfoFragment extends BaseFragment implements View.OnClickListener {

    private TextView tvNoRecord;
    private Button btnAddCrop;
    private FrameLayout frameAdd;

    private CropInfoAdapter adapter;

    private List<CropInfoResponse.CroplListBean> list;

    private Handler handler = new Handler(Looper.getMainLooper());

    public static CropInfoFragment newInstance() {

        Bundle args = new Bundle();

        CropInfoFragment fragment = new CropInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_crop_info, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        /* toolbar view start */
        ImageView imgBack = view.findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);
        imgBack.setVisibility(View.VISIBLE);

        TextView tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.crop); //crop_info
        /* toolbar view end */
        view.findViewById(R.id.rlMain).setOnClickListener(this); //avoid bg click

        RecyclerView rvCrop = view.findViewById(R.id.rvCrop);
        list = new ArrayList<>();

        adapter = new CropInfoAdapter(list, new ClickListener() {
            @Override
            public void onItemClick(int pos) {
                getBaseActivity().addFragment(CropDetailFragment.newInstance(CropInfoFragment.this,list.get(pos)),  R.id.cropFrame, true);
            }

            @Override
            public void onEditClick(int pos) {
                getBaseActivity().addFragment(AddEditCropFragment.newInstance(CropInfoFragment.this, false, list.get(pos)), R.id.cropFrame, true);
            }

            @Override
            public void onDeleteClick(int pos) {
                new Thread(() -> {
                    String cropExpenseStatus = getDataManager().getExpenseApplyStatus(list.get(pos).getCropId(), list.get(pos).getRecordKey());
                    String msg = (cropExpenseStatus == null) ? "" : getString(R.string.deleteExpenseMsg);

                    handler.post(() -> DeleteDialog.newInstance(cropExpenseStatus != null, msg, () -> {
                        if (isNetworkConnected(true)) {
                            getBaseActivity().doSyncAppDB(false, flag -> {
                                switch (flag) {
                                    case AppConstants.ONLINE:
                                        if (!list.get(pos).getCropId().equals(list.get(pos).getRecordKey()))
                                            doDeleteCropInfo(pos);
                                        else
                                            doDeleteCropFromLocalDB(AppConstants.OFFLINE, pos, list.get(pos));
                                        break;

                                    case AppConstants.OFFLINE:
                                        doDeleteCropFromLocalDB(AppConstants.OFFLINE, pos, list.get(pos));
                                        break;
                                }
                            });

                        } else {
                            doDeleteCropFromLocalDB(AppConstants.OFFLINE, pos, list.get(pos));
                        }
                    }).show(getChildFragmentManager()));
                }).start();
            }
        });

        rvCrop.setAdapter(adapter);

        tvNoRecord = view.findViewById(R.id.tvNoRecord);
        frameAdd = view.findViewById(R.id.frameAdd);
        frameAdd.setOnClickListener(this);
        btnAddCrop = view.findViewById(R.id.btnAddCrop);
        btnAddCrop.setOnClickListener(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hitApi();
        initAd(view);
    }

    private void initAd(View view) {
        AdView mAdView = view.findViewById(R.id.adView);
        mAdView.setVisibility(View.VISIBLE);
        AppUtils.setAdBanner(mAdView, frameAdd);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                AppUtils.setMargins(frameAdd, 0, 0, 15, 15);
                mAdView.setVisibility(View.GONE);
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when when the user is about to return
                // to the app after tapping on an ad.
            }
        });
    }

    public void hitApi() {
        if (isNetworkConnected(true)) {
            getBaseActivity().doSyncAppDB(false, flag -> {
                switch (flag) {
                    case AppConstants.ONLINE:
                        if (isDashBoardController()) {
                            getCropInfoFromDB(true);
                            //getCropInfo();
                        }
                        else getCropInfoFromDB(false);
                        break;

                    case AppConstants.OFFLINE:
                        if (isDashBoardController()) {
                            getCropInfoFromDB(true);
                        } else getCropInfoFromDB(false);
                        break;
                }
            });
        } else {
            if (isDashBoardController()) {
                getCropInfoFromDB(true);
            } else getCropInfoFromDB(false);
        }
    }

    private Boolean isDashBoardController(){
        return CropActivity.fromPropertyId.isEmpty();
    }

    private void getCropInfo() {
        setLoading(true);
        getDataManager().doServerCropInfoApiCall(getDataManager().getHeader())
                .getAsResponse(new VolleyResponseListener() {
                    @Override
                    public void onVolleyResponse(String response) {
                        setLoading(false);
                        CropInfoResponse cropInfoResponse = getDataManager().mGson.fromJson(response, CropInfoResponse.class);

                        if (cropInfoResponse.getStatus().equals("success")) {
                            list.clear();
                            list.addAll(cropInfoResponse.getCroplList());

                            updateView();
                            if (!list.isEmpty()) {
                                //Todo check local db
                                updateCropDataDB(cropInfoResponse);
                            }

                            adapter.notifyDataSetChanged();
                        } else {
                            updateView();
                            if (cropInfoResponse.getMessage().equals(getString(R.string.no_record_found))) {
                                list.clear();
                                adapter.notifyDataSetChanged();
                            } else
                                CommonUtils.showToast(getBaseActivity(), cropInfoResponse.getMessage(), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        updateView();
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    private void doDeleteCropInfo(int pos) {
        setLoading(true);
        HashMap<String, String> mParams = new HashMap<>();
        mParams.put("recordKey", list.get(pos).getRecordKey());

        getDataManager().doServerDeleteCropInfoApiCall(getDataManager().getHeader(), mParams)
                .getAsJsonObject(new VolleyJsonObjectListener() {

                    @Override
                    public void onVolleyJsonObject(JSONObject jsonObject) {
                        setLoading(false);
                        try {
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            if (status.equals("success")) {
                                //Todo delete property from local db
                                doDeleteCropFromLocalDB(AppConstants.ONLINE, pos, list.get(pos));
                            } else
                                CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                        } catch (Exception e) {
                            CommonUtils.showToast(getBaseActivity(), getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyJsonException() {
                        setLoading(false);
                        CommonUtils.showToast(getBaseActivity(), getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    private void updateView() {
        if (CropActivity.fromPropertyId.isEmpty()){
            if (list.isEmpty()) {
                frameAdd.setVisibility(View.GONE);
                tvNoRecord.setVisibility(View.VISIBLE);
                btnAddCrop.setVisibility(View.VISIBLE);
            }else{
                frameAdd.setVisibility(View.VISIBLE);
                tvNoRecord.setVisibility(View.GONE);
                btnAddCrop.setVisibility(View.GONE);
            }
        }else{
            frameAdd.setVisibility(View.GONE);
            btnAddCrop.setVisibility(View.GONE);
            if (list.isEmpty())tvNoRecord.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                getBaseActivity().onBackPressed();
                break;

            case R.id.btnAddCrop:
            case R.id.frameAdd:
                String planStatus = getDataManager().getUserInfo().getSubscriptionPlan();
                if (planStatus.equalsIgnoreCase(AppConstants.PLAN_FREE) && list.size() < AppConstants.FREE_CROP_LIMIT) {
                    addCrop();
                } else if (!planStatus.equalsIgnoreCase(AppConstants.PLAN_FREE)) {
                    addCrop();
                } else {
                    PlanUpgradeDialog.newInstance(getString(R.string.alert_free_crop), getString(R.string.upgrade_to_premium_plans), () -> startActivity(new Intent(getBaseActivity(), PlanActivity.class))).show(getChildFragmentManager());
                }
                break;
        }
    }

    private void addCrop() {
        btnAddCrop.setClickable(false);
        frameAdd.setClickable(false);
        getBaseActivity().addFragment(AddEditCropFragment.newInstance(this, true, null), R.id.cropFrame, true);

        new Handler().postDelayed(() -> {
            btnAddCrop.setClickable(true);
            frameAdd.setClickable(true);
        }, 2000);
    }

    /*Local DB start here*/
    private void getCropInfoFromDB(Boolean isAllCrop) {
        setLoading(true);
        //property crop list
        new Thread(() -> {

            List<CropInfoResponse.CroplListBean> localList;

            if (isAllCrop) localList = getDataManager().getPropertyCropList();
            else localList = getDataManager().getPropertyCropList(CropActivity.fromPropertyId);

            //update on ui thread
            if (!localList.isEmpty()) {
                list.clear();
                list.addAll(localList);
            } else list.clear();

            handler.post(() -> {
                setLoading(false);

                adapter.notifyDataSetChanged();
                updateView();
            });
        }).start();
    }

    private void updateCropDataDB(CropInfoResponse cropInfoResponse) {
        new Thread(() -> {
            getDataManager().deleteAllCrop();
            List<Crop> cropList = new ArrayList<>();

            for (CropInfoResponse.CroplListBean croplListBean : cropInfoResponse.getCroplList()) {
                Crop crop = new Crop();

                crop.setCropId(croplListBean.getCropId());
                crop.setParcel_id(croplListBean.getParcel_id());
                crop.setCropName(croplListBean.getCropName());
                crop.setLotId(croplListBean.getLotId());
                crop.setDescription(croplListBean.getDescription());
                crop.setPreparationDate(croplListBean.getPreparationDate());
                crop.setPlantingDate(croplListBean.getPlantingDate());
                crop.setHarvestDate(croplListBean.getHarvestDate());
                crop.setSaleDate(croplListBean.getSaleDate().isEmpty() ? null : CalenderUtils.getDateFormat(croplListBean.getSaleDate().contains("/") ? CalenderUtils.formatDate(croplListBean.getSaleDate(), AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT) : croplListBean.getSaleDate(), AppConstants.SERVER_TIMESTAMP_FORMAT));
                crop.setSalePrice(croplListBean.getSalePrice());
                crop.setSaleQuantity(croplListBean.getSaleQuantity());
                crop.setSaleTotalPrice(croplListBean.getSaleTotalPrice());
                crop.setCostFrom(croplListBean.getCostFrom());
                crop.setCostTo(croplListBean.getCostTo());
                crop.setCostTotal(croplListBean.getCostTotal());
                crop.setStatus(croplListBean.getStatus());
                crop.setRecordKey(croplListBean.getRecordKey());
                crop.setCrd(croplListBean.getCrd());
                crop.setUpd(croplListBean.getUpd());
                crop.setEvent(AppConstants.DB_EVENT_ADD);
                crop.setDataSync(AppConstants.DB_SYNC_TRUE);
                cropList.add(crop);
            }
            getDataManager().insertAllCrop(cropList);
            AppLogger.d("database", "Db Add Crop done");
        }).start();
    }

    private void doDeleteCropFromLocalDB(String flag, int pos, CropInfoResponse.CroplListBean cropBean) {
        new Thread(() -> {
            switch (flag) {
                case AppConstants.ONLINE:
                    //delete here
                    getDataManager().deleteUsingCropId(cropBean.getCropId());
                    AppLogger.d("database", "crop delete success");

                    handler.post(() -> {
                        list.remove(pos);
                        adapter.notifyItemRemoved(pos);
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.cropDelSuccess), Toast.LENGTH_SHORT);
                        updateView();
                    });
                    break;

                case AppConstants.OFFLINE:
                    if (cropBean.getCropId().equals(cropBean.getRecordKey())) {
                        //delete here
                        getDataManager().deleteUsingCropId(cropBean.getCropId());
                        AppLogger.d("database", "crop delete success");

                        handler.post(() -> {
                            list.remove(pos);
                            adapter.notifyItemRemoved(pos);
                            CommonUtils.showToast(getBaseActivity(), getString(R.string.cropDelSuccess), Toast.LENGTH_SHORT);
                            updateView();
                        });
                    } else {
                        //update event
                        Crop crop = new Crop();

                        crop.setCropId(cropBean.getCropId());
                        crop.setParcel_id(cropBean.getParcel_id());
                        crop.setCropName(cropBean.getCropName());
                        crop.setLotId(cropBean.getLotId());
                        crop.setDescription(cropBean.getDescription());
                        crop.setPreparationDate(cropBean.getPreparationDate());
                        crop.setPlantingDate(cropBean.getPlantingDate());
                        crop.setHarvestDate(cropBean.getHarvestDate());
                        crop.setSaleDate(cropBean.getSaleDate().isEmpty() ? null : CalenderUtils.getDateFormat(cropBean.getSaleDate().contains("/") ? CalenderUtils.formatDate(cropBean.getSaleDate(), AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT) : cropBean.getSaleDate(), AppConstants.SERVER_TIMESTAMP_FORMAT));
                        crop.setSalePrice(cropBean.getSalePrice());
                        crop.setSaleQuantity(cropBean.getSaleQuantity());
                        crop.setSaleTotalPrice(cropBean.getSaleTotalPrice());
                        crop.setCostFrom(cropBean.getCostFrom());
                        crop.setCostTo(cropBean.getCostTo());
                        crop.setCostTotal(cropBean.getCostTotal());
                        crop.setStatus(cropBean.getStatus());
                        crop.setRecordKey(cropBean.getRecordKey());
                        crop.setCrd(cropBean.getCrd());
                        crop.setUpd(cropBean.getUpd());
                        crop.setEvent(AppConstants.DB_EVENT_DEL);
                        crop.setDataSync(AppConstants.DB_SYNC_FALSE);

                        getDataManager().updateCrop(crop);
                        AppLogger.d("database", "crop delete success");

                        //set sync status true
                        getDataManager().setLocalDataExistForSync(true);

                        handler.post(() -> {
                            list.remove(pos);
                            adapter.notifyItemRemoved(pos);
                            CommonUtils.showToast(getBaseActivity(), getString(R.string.cropDelSuccess), Toast.LENGTH_SHORT);
                            updateView();
                        });
                    }
                    break;
            }
        }).start();
    }
    /*Local DB end here*/
}
