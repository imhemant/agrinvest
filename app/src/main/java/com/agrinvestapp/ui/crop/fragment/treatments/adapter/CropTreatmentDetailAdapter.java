package com.agrinvestapp.ui.crop.fragment.treatments.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.CropTreatmentDetailResponse;
import com.agrinvestapp.data.model.api.CropTreatmentInfoResponse;
import com.agrinvestapp.data.model.api.TreatmentDetailResponse;
import com.agrinvestapp.utils.pagination.FooterLoader;
import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chauthai.swipereveallayout.ViewBinderHelper;

import java.util.List;


/**
 * Created by hemant
 * Date: 17/4/18
 * Time: 4:03 PM
 */

public class CropTreatmentDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEWTYPE_ITEM = 1;
    private final int VIEWTYPE_LOADER = 2;
    // This object helps you save/restore the open/close state of each view
    private final ViewBinderHelper viewBinderHelper = new ViewBinderHelper();
    private List<CropTreatmentDetailResponse.TreatmentDetailBean> list;
    private boolean showLoader;
    private ItemClickListener itemClickListener;

    public CropTreatmentDetailAdapter(List<CropTreatmentDetailResponse.TreatmentDetailBean> list, ItemClickListener itemClickListener) {
        this.list = list;
        this.itemClickListener = itemClickListener;
        viewBinderHelper.setOpenOnlyOne(true);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder rvHolder, int position) {
        if (rvHolder instanceof FooterLoader) {
            FooterLoader loaderViewHolder = (FooterLoader) rvHolder;
            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }
            return;
        }
        CropTreatmentDetailAdapter.ViewHolder holder = ((CropTreatmentDetailAdapter.ViewHolder) rvHolder);
        CropTreatmentDetailResponse.TreatmentDetailBean bean = list.get(position);

        viewBinderHelper.bind(holder.swipe, bean.getRecordKey() == null ? bean.getTreatmentMappingId() : bean.getRecordKey());
        holder.tvDate.setText(bean.getTreatmentDate());
        holder.tvDescription.setText(bean.getTreatmentDescription());
    }

    public void showLoading(boolean status) {
        showLoader = status;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case VIEWTYPE_ITEM:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_treatment_detail, parent, false);
                return new ViewHolder(view);

            case VIEWTYPE_LOADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pagination_item_loader, parent, false);
                return new FooterLoader(view);
        }
        return null;

    }

    @Override
    public int getItemViewType(int position) {
        if (position == list.size() - 1) {
            return showLoader ? VIEWTYPE_LOADER : VIEWTYPE_ITEM;
        }
        return VIEWTYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface ItemClickListener {
        void onItemClick(int pos);

        void onEditClick(int pos);

        void onDeleteClick(int pos);
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvDate, tvDescription;
        private LinearLayout llEdit, llDelete;
        private SwipeRevealLayout swipe;

        ViewHolder(@NonNull View v) {
            super(v);
            tvDate = v.findViewById(R.id.tvDate);
            tvDescription = v.findViewById(R.id.tvDescription);
            swipe = v.findViewById(R.id.swipe);
            llEdit = v.findViewById(R.id.llEdit);
            llDelete = v.findViewById(R.id.llDelete);

            llEdit.setOnClickListener(this);
            llDelete.setOnClickListener(this);

        }

        @Override
        public void onClick(@NonNull final View view) {

            switch (view.getId()) {
                case R.id.llEdit:
                    if (getAdapterPosition() != -1) {
                        swipe.close(true);
                        itemClickListener.onEditClick(getAdapterPosition());
                    }
                    break;

                case R.id.llDelete:
                    if (getAdapterPosition() != -1) {
                        swipe.close(true);
                        itemClickListener.onDeleteClick(getAdapterPosition());
                    }
                    break;

                case R.id.llMain:
                    if (getAdapterPosition() != -1) {
                        itemClickListener.onItemClick(getAdapterPosition());
                    }
                    break;
            }
        }
    }

}

