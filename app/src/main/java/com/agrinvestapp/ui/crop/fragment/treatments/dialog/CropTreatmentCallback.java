package com.agrinvestapp.ui.crop.fragment.treatments.dialog;

public interface CropTreatmentCallback {
    void onAdd(String name);

    void onEdit(int pos, String name);
}
