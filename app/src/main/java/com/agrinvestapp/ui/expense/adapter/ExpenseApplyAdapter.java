package com.agrinvestapp.ui.expense.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.other.IncomeOrExpenseApplyBean;
import com.agrinvestapp.ui.base.ClickListener;

import java.util.List;


/**
 * Created by hemant
 * Date: 17/4/18
 * Time: 4:03 PM
 */

public class ExpenseApplyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public int lastPos = 0;
    private List<IncomeOrExpenseApplyBean> list;
    private ClickListener clickListener;

    public ExpenseApplyAdapter(List<IncomeOrExpenseApplyBean> list, ClickListener clickListener) {
        this.list = list;
        this.clickListener = clickListener;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder rvHolder, int position) {
        ExpenseApplyAdapter.ViewHolder holder = ((ExpenseApplyAdapter.ViewHolder) rvHolder);

        IncomeOrExpenseApplyBean bean = list.get(position);

        if (bean.isSelect){
            holder.tvTitle.setTextColor(holder.frameMain.getContext().getResources().getColor(R.color.colorWhite));
            holder.frameMain.setBackground(holder.frameMain.getContext().getResources().getDrawable(R.drawable.rounded_shape));
        }else{
            holder.tvTitle.setTextColor(holder.frameMain.getContext().getResources().getColor(R.color.colorPrimaryDark));
            holder.frameMain.setBackground(holder.frameMain.getContext().getResources().getDrawable(R.drawable.grey_rounded_stroke));
        }
        holder.tvTitle.setText(bean.localisationTitle);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_expense_apply, parent, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvTitle;
        private FrameLayout frameMain;

        ViewHolder(@NonNull View v) {
            super(v);
            tvTitle = v.findViewById(R.id.tvTitle);
            frameMain = v.findViewById(R.id.frameMain);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(@NonNull final View view) {
            int tempPos = getAdapterPosition();
            if (tempPos != -1 && clickListener != null)
            {
                list.get(lastPos).isSelect = false;
                notifyItemChanged(lastPos);

                clickListener.onItemClick(getAdapterPosition());
                list.get(tempPos).isSelect = true;
                notifyItemChanged(tempPos);

                lastPos = tempPos;
            }
        }
    }
}

