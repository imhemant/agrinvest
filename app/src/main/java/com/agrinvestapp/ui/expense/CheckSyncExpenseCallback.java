package com.agrinvestapp.ui.expense;

public interface CheckSyncExpenseCallback {
    void onSync(String data);

    void onCallApi();
}
