package com.agrinvestapp.ui.expense.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.other.ExpenseProductBean;

import java.util.List;

/**
 * Created by hemant
 * Date: 15/11/18
 * Time: 4:00 PM
 */

public class AddProductExpenseAdapter extends RecyclerView.Adapter<AddProductExpenseAdapter.ViewHolder> {

    private DeleteClickListener listener;
    private List<ExpenseProductBean> list;

    public AddProductExpenseAdapter(List<ExpenseProductBean> list, DeleteClickListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public AddProductExpenseAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_add_product_expense, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AddProductExpenseAdapter.ViewHolder holder, final int position) {
        ExpenseProductBean bean = list.get(position);
        holder.tvName.setText(bean.getProductName());
        holder.tvQty.setText(bean.getQuantity());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface DeleteClickListener {
        void onDeleteClick(int pos);
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvName, tvQty;
        private ImageView imgDel;

        ViewHolder(@NonNull View v) {
            super(v);
            tvName = v.findViewById(R.id.tvName);
            tvQty = v.findViewById(R.id.tvQty);
            imgDel = v.findViewById(R.id.imgDel);

            imgDel.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position=getAdapterPosition();
            if (position!= -1){
                listener.onDeleteClick(position);
            }
        }
    }
}

