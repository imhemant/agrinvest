package com.agrinvestapp.ui.expense.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.ExpenseInfoResponse;
import com.agrinvestapp.ui.base.BaseFragment;
import com.agrinvestapp.ui.expense.adapter.ProductExpenseDetailAdapter;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.CalenderUtils;


public class ExpenseDetailFragment extends BaseFragment implements View.OnClickListener {

    private ExpenseInfoResponse.ExpenseListBean expenseListBean;

    public static ExpenseDetailFragment newInstance(ExpenseInfoResponse.ExpenseListBean expenseListBean) {

        Bundle args = new Bundle();
        args.putParcelable(AppConstants.MODEL, expenseListBean);
        ExpenseDetailFragment fragment = new ExpenseDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments()!=null){
            expenseListBean = getArguments().getParcelable(AppConstants.MODEL);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_expense_detail, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
             /* toolbar view start */
        ImageView imgBack = view.findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);
        imgBack.setVisibility(View.VISIBLE);

        TextView tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.expensesDetail);
        /* toolbar view end */
        view.findViewById(R.id.llMain).setOnClickListener(this);  //avoid bg click
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        updateUI(view);
    }

    private void updateUI(View view) {
        TextView tvCount = view.findViewById(R.id.tvCount);
        TextView tvCost = view.findViewById(R.id.tvCost);
        TextView tvDateBought = view.findViewById(R.id.tvDateBought);
        TextView tvApplyDate = view.findViewById(R.id.tvApplyDate);
        TextView tvApplyOnTitl = view.findViewById(R.id.tvApplyOnTitl);
        TextView tvApplyingOn = view.findViewById(R.id.tvApplyingOn);
        TextView tvDescription = view.findViewById(R.id.tvDescription);
        RecyclerView rvProductExpenses = view.findViewById(R.id.rvProductExpenses);

        if (expenseListBean!=null){
            ProductExpenseDetailAdapter adapter = new ProductExpenseDetailAdapter(expenseListBean.getProductDetails());
            rvProductExpenses.setAdapter(adapter);

            tvCount.setText(String.valueOf(expenseListBean.getProductDetails().size()));
            StringBuilder costBuilder = new StringBuilder("");
            costBuilder.append("$").append(expenseListBean.getCost());
            tvCost.setText(costBuilder);
            tvDateBought.setText(expenseListBean.getDateBought().contains("-") ? CalenderUtils.formatDate(expenseListBean.getDateBought(), AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT) : expenseListBean.getDateBought());
            tvApplyDate.setText(expenseListBean.getDateApplied().contains("-") ? CalenderUtils.formatDate(expenseListBean.getDateApplied(), AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT) : expenseListBean.getDateApplied());
            tvDescription.setText(expenseListBean.getDescription().isEmpty() ? getString(R.string.na) : expenseListBean.getDescription());
            switch (expenseListBean.getAppliedOn().isEmpty()?"animal":expenseListBean.getAppliedOn().get(0).getAppliedOn()){
                case "animal":
                    tvApplyOnTitl.setText(R.string.what_animal_you_are_applying_it);
                    break;

                case "crop":
                    tvApplyOnTitl.setText(R.string.what_crop_you_are_applying_it);
                    break;

                case "lot":
                    tvApplyOnTitl.setText(R.string.what_lot_you_are_applying_it);
                    break;

                case "parcel":
                    tvApplyOnTitl.setText(R.string.what_parcel_you_are_applying_it);
                    break;

                case "property":
                    tvApplyOnTitl.setText(R.string.what_property_you_are_applying_it);
                    break;
            }

            StringBuilder applyOnShowBuilder = new StringBuilder("");
            for (ExpenseInfoResponse.ExpenseListBean.AppliedOnBean applyOnBean:expenseListBean.getAppliedOn()){
                applyOnShowBuilder.append(applyOnBean.getAppliedName()).append(", ");
            }

            String tempApplyOnShow = applyOnShowBuilder.toString().trim();
            tempApplyOnShow = tempApplyOnShow.isEmpty() ? "" : tempApplyOnShow.substring(0, tempApplyOnShow.length() - 1);
            tvApplyingOn.setText(tempApplyOnShow);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                getBaseActivity().onBackPressed();
                break;
        }
    }
}
