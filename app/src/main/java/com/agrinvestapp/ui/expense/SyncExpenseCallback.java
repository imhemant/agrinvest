package com.agrinvestapp.ui.expense;

public interface SyncExpenseCallback {
    void onSuccess();
}
