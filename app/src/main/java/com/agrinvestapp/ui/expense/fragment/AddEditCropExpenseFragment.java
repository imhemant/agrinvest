package com.agrinvestapp.ui.expense.fragment;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.ExpenseInfoResponse;
import com.agrinvestapp.data.model.db.Expense;
import com.agrinvestapp.data.model.db.ExpenseMapping;
import com.agrinvestapp.data.model.db.ExpenseProductMapping;
import com.agrinvestapp.data.model.other.ExpenseProductBean;
import com.agrinvestapp.data.model.other.IncomeOrExpenseApplyBean;
import com.agrinvestapp.data.model.other.IncomeOrExpenseApplyOnBean;
import com.agrinvestapp.ui.base.BaseFragment;
import com.agrinvestapp.ui.base.ClickListener;
import com.agrinvestapp.ui.expense.adapter.AddProductExpenseAdapter;
import com.agrinvestapp.ui.expense.adapter.ExpenseApplyAdapter;
import com.agrinvestapp.ui.expense.adapter.SpProductAdapter;
import com.agrinvestapp.ui.expense.dialog.ExpenseApplyOnDialog;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.AppLogger;
import com.agrinvestapp.utils.CalenderUtils;
import com.agrinvestapp.utils.CommonUtils;
import com.agrinvestapp.volley.VolleyJsonObjectListener;
import com.agrinvestapp.volley.VolleyRequest;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class AddEditCropExpenseFragment extends BaseFragment implements View.OnClickListener {

    private EditText etQty, etDescription;
    private TextView tvCost, tvDateBought, tvDateApply, tvDesCount, tvApplyOn;
    private Spinner spProduct;
    private Boolean isDateBought = false;
    private StringBuilder dateBought, dateApply;

    private List<IncomeOrExpenseApplyBean> applyList;
    private List<ExpenseProductBean> spProductList;
    private List<ExpenseProductBean> selectedProductList;
    private List<IncomeOrExpenseApplyOnBean> applyOnList;

    private AddProductExpenseAdapter productExpenseAdapter;
    private ExpenseApplyAdapter applyAdapter;
    private StringBuilder builder;
    private String applyOn = "", tempApplyOn = "";
    private Integer ttlCost = 0;

    private Handler handler = new Handler(Looper.getMainLooper());
    private ExpenseFragment expenseFragment;

    //Edit case
    private Boolean isAdd;
    private ExpenseInfoResponse.ExpenseListBean expenseListBean;
    Runnable applyOnListRunnable = new Runnable() {
        @Override
        public void run() {
            tempApplyOn = "";
            StringBuilder applyOnBuilder = new StringBuilder("");
            StringBuilder applyOnShowBuilder = new StringBuilder("");

            for (int i = 0; i < applyOnList.size(); i++) {
                for (ExpenseInfoResponse.ExpenseListBean.AppliedOnBean applyOnBean : expenseListBean.getAppliedOn()) {
                    if (applyOnBean.getAppliedOnRecordKey().equals(applyOnList.get(i).getRecordKey())) {
                        applyOnList.get(i).setSelect(true);
                        applyOnBuilder.append(applyOnBean.getAppliedOnRecordKey()).append(",");
                        applyOnShowBuilder.append(applyOnBean.getAppliedName()).append(", ");
                    }
                }
            }
            String applyOntemp = applyOnBuilder.toString().trim();
            String tempApplyOnShow = applyOnShowBuilder.toString().trim();
            tempApplyOn = applyOntemp.isEmpty() ? "" : applyOntemp.substring(0, applyOntemp.length() - 1);
            tempApplyOnShow = tempApplyOnShow.isEmpty() ? "" : tempApplyOnShow.substring(0, tempApplyOnShow.length() - 1);

            String finalTempApplyOnShow = tempApplyOnShow;
            handler.post(() -> tvApplyOn.setText(finalTempApplyOnShow));
        }
    };
    private Boolean isClick = false;
    private Boolean isClickAdd = false;
    // the callback received when the user "sets" the Date in the
    // DatePickerDialog
    @NonNull
    private DatePickerDialog.OnDateSetListener datePicker = (view, selectedYear, selectedMonth, selectedDay) -> {
        selectedMonth += 1;

        if (isDateBought) {
            dateBought = new StringBuilder("");
            dateBought.append(selectedDay).append("/").append(selectedMonth).append("/").append(selectedYear);

            tvDateBought.setText(CalenderUtils.formatDate(String.valueOf(dateBought), AppConstants.TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT));
        } else {
            dateApply = new StringBuilder("");
            dateApply.append(selectedDay).append("/").append(selectedMonth).append("/").append(selectedYear);

            tvDateApply.setText(CalenderUtils.formatDate(String.valueOf(dateApply), AppConstants.TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT));
        }
    };

    public static AddEditCropExpenseFragment newInstance(Boolean isAdd, ExpenseFragment expenseFragment, ExpenseInfoResponse.ExpenseListBean expenseListBean) {

        Bundle args = new Bundle();
        args.putBoolean(AppConstants.REQUEST_TYPE_ADD, isAdd);
        args.putParcelable(AppConstants.MODEL,expenseListBean);
        AddEditCropExpenseFragment fragment = new AddEditCropExpenseFragment();
        fragment.setInstance(expenseFragment);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments()!=null){
            isAdd = getArguments().getBoolean(AppConstants.REQUEST_TYPE_ADD);
            expenseListBean = getArguments().getParcelable(AppConstants.MODEL);
        }
    }

    private void setInstance(ExpenseFragment expenseFragment) {
        this.expenseFragment = expenseFragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_edit_expense, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        /* toolbar view start */
        ImageView imgBack = view.findViewById(R.id.imgBack);
        imgBack.setVisibility(View.VISIBLE);

        TextView tvTitle = view.findViewById(R.id.tvTitle);
        Button btnAdd = view.findViewById(R.id.btnAdd);
        if (isAdd){
            tvTitle.setText(R.string.addCropExpense);
        }
        else{
            tvTitle.setText(R.string.editCropExpense);
            btnAdd.setText(R.string.edit_expenses);
        }
        /* toolbar view end */

        tvApplyOn = view.findViewById(R.id.tvApplyOn);
        tvCost = view.findViewById(R.id.tvCost);
        etQty = view.findViewById(R.id.etQty);
        etDescription = view.findViewById(R.id.etDescription);
        tvDateBought = view.findViewById(R.id.tvDateBought);
        tvDateApply = view.findViewById(R.id.tvDateApply);
        tvDesCount = view.findViewById(R.id.tvDesCount);
        //avoid bg click
        onClickListener(imgBack, view.findViewById(R.id.llMain), view.findViewById(R.id.rlApplyOn), btnAdd, view.findViewById(R.id.btnAddProduct), tvDateBought, tvDateApply);

        initProductSpinners(view);
        initApplyRecyclerView(view);
        initExpenseProductRecyclerView(view);

        etDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                builder = new StringBuilder("");
                builder.append(s.length()).append("/").append("250");
                tvDesCount.setText(builder);
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //by default animal is selected if add expense
        if (isAdd){
            applyOn = "Crop";
            updateAppliedOnList(false, applyOn);
        }else{
            updateEditUI();
        }
    }

    private void updateEditUI() {
        if (expenseListBean!=null){
            new Thread(() -> {
                selectedProductList.clear();
                for (ExpenseInfoResponse.ExpenseListBean.ProductDetailsBean productBean : expenseListBean.getProductDetails()){
                    ExpenseProductBean bean = new ExpenseProductBean();
                    bean.setProductId(productBean.getProductRecordKey());  //used as record key
                    bean.setProductName(productBean.getProductName());
                    bean.setQuantity(productBean.getQuantity());
                    bean.setPrice(productBean.getPrice());
                    selectedProductList.add(bean);
                }

                handler.post(() -> productExpenseAdapter.notifyDataSetChanged());

            }).start();

            applyOn = expenseListBean.getAppliedOn().size()==0?"crop":expenseListBean.getAppliedOn().get(0).getAppliedOn();

            switch (applyOn){
                case "crop":
                    applyAdapter.lastPos=0;
                    applyList.get(0).isSelect =true;
                    break;

                case "lot":
                    applyAdapter.lastPos=1;
                    applyList.get(1).isSelect =true;
                    break;

                case "parcel":
                    applyAdapter.lastPos=2;
                    applyList.get(2).isSelect =true;
                    break;

                case "property":
                    applyAdapter.lastPos=3;
                    applyList.get(3).isSelect =true;
                    break;
            }
            applyAdapter.notifyDataSetChanged();

            updateAppliedOnList(true, applyOn);

            tvDateBought.setText(expenseListBean.getDateBought().contains("-")?CalenderUtils.formatDate(expenseListBean.getDateBought(), AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT):expenseListBean.getDateBought());
            tvCost.setText(expenseListBean.getCost());
            ttlCost = Integer.parseInt(expenseListBean.getCost());
            tvDateApply.setText(expenseListBean.getDateApplied().contains("-")?CalenderUtils.formatDate(expenseListBean.getDateApplied(), AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT):expenseListBean.getDateApplied());
            etDescription.setText(expenseListBean.getDescription());
            builder = new StringBuilder("");
            builder.append(expenseListBean.getDescription().length()).append("/").append("250");
            tvDesCount.setText(builder);
        }
    }

    private void initProductSpinners(View view) {
        spProduct = view.findViewById(R.id.spProduct);
        spProductList = new ArrayList<>();
        ExpenseProductBean expenseBean = new ExpenseProductBean();
        expenseBean.setProductName(getString(R.string.select_product));
        expenseBean.setProductId("");
        spProductList.add(expenseBean);

        SpProductAdapter adapter = new SpProductAdapter(getBaseActivity(), spProductList);
        spProduct.setAdapter(adapter);

        new Thread(() -> {
            List<ExpenseProductBean> list = getDataManager().getProductList();
            spProductList.addAll(list);
            handler.post(adapter::notifyDataSetChanged);
        }).start();
    }

    private void initExpenseProductRecyclerView(View view) {
        RecyclerView rvExpenses = view.findViewById(R.id.rvExpenses);
        selectedProductList = new ArrayList<>();
        productExpenseAdapter = new AddProductExpenseAdapter(selectedProductList, pos -> {
            updateCost(false, selectedProductList.get(pos));
            selectedProductList.remove(pos);
            productExpenseAdapter.notifyItemRemoved(pos);
        });
        rvExpenses.setAdapter(productExpenseAdapter);
    }

    private void initApplyRecyclerView(View view) {
        RecyclerView rvApplyOn = view.findViewById(R.id.rvApplyOn);
        RecyclerView.LayoutManager manager = new GridLayoutManager(getBaseActivity(), 3);
        rvApplyOn.setLayoutManager(manager);

        applyList = new ArrayList<>();
        applyOnList = new ArrayList<>();

        IncomeOrExpenseApplyBean bean = new IncomeOrExpenseApplyBean();
        bean.isSelect = isAdd;
        bean.title = "Crop";
        bean.localisationTitle = getString(R.string.crop);
        applyList.add(bean);

        bean = new IncomeOrExpenseApplyBean();
        bean.isSelect = false;
        bean.title = "Lot";
        bean.localisationTitle = getString(R.string.lot);
        applyList.add(bean);

        bean = new IncomeOrExpenseApplyBean();
        bean.isSelect = false;
        bean.title = "Parcel";
        bean.localisationTitle = getString(R.string.parcel);
        applyList.add(bean);

        bean = new IncomeOrExpenseApplyBean();
        bean.isSelect = false;
        bean.title = "Property";
        bean.localisationTitle = getString(R.string.property);
        applyList.add(bean);

        applyAdapter = new ExpenseApplyAdapter(applyList, new ClickListener() {
            @Override
            public void onItemClick(int pos) {
                applyOn = applyList.get(pos).title;
                tempApplyOn = "";
                tvApplyOn.setText("");

                updateAppliedOnList(false, applyOn);
            }

            @Override
            public void onEditClick(int pos) {
//not usable
            }

            @Override
            public void onDeleteClick(int pos) {
//not usable
            }
        });
        rvApplyOn.setAdapter(applyAdapter);
    }

    private void updateAppliedOnList(Boolean isEdit, String applyOn) {
        new Thread(() -> {
            applyOnList.clear();

            List<IncomeOrExpenseApplyOnBean> localList = new ArrayList<>();

            switch (applyOn.toLowerCase()) {
                case "crop":
                    localList = getDataManager().getCropApplyOnInfo();
                    break;

                case "lot":
                    localList = getDataManager().getCropLotApplyOnInfo();
                    break;

                case "parcel":
                    localList = getDataManager().getParcelApplyOnInfo();
                    break;

                case "property":
                    localList = getDataManager().getPropertyApplyOnInfo();
                    break;
            }

            applyOnList.addAll(localList);

            if (isEdit)new Thread(applyOnListRunnable).start();
        }).start();
    }

    private void onClickListener(View... views) {
        for (View view:views){
            view.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                getBaseActivity().onBackPressed();
                break;

            case R.id.btnAdd:
                if (!isClickAdd) {
                    isClickAdd = true;
                    String tempDB = tvDateBought.getText().toString().trim();
                    String tempCost = tvCost.getText().toString().trim();
                    String tempDA = tvDateApply.getText().toString().trim();


                    if (isVerifyInput(tempDB, tempCost, tempDA, applyOn, tempApplyOn)) {
                        if (isNetworkConnected(true)) {
                            String productJson = selectedProductList.isEmpty() ? "" : getDataManager().mGson.toJson(selectedProductList);

                            if (isAdd)
                                doAddCropExpense(productJson, tempDB, tempCost, tempDA, applyOn.toLowerCase(), tempApplyOn);
                            else if (expenseListBean != null && !expenseListBean.getExpenseId().equalsIgnoreCase(expenseListBean.getRecordKey()))
                                doUpdateCropExpense(productJson, tempDB, tempCost, tempDA, applyOn.toLowerCase(), tempApplyOn);
                            else
                                doAddCropExpense(productJson, tempDB, tempCost, tempDA, applyOn.toLowerCase(), tempApplyOn);
                        } else {
                            if (isAdd)
                                doAddEditCropExpenseToLocalDB("add", selectedProductList, tempDB, tempCost, tempDA, applyOn.toLowerCase(), applyOnList);
                            else if (expenseListBean != null && !expenseListBean.getExpenseId().equalsIgnoreCase(expenseListBean.getRecordKey()))
                                doAddEditCropExpenseToLocalDB("edit", selectedProductList, tempDB, tempCost, tempDA, applyOn.toLowerCase(), applyOnList);
                            else if (!isAdd)
                                doAddEditCropExpenseToLocalDB("edit", selectedProductList, tempDB, tempCost, tempDA, applyOn.toLowerCase(), applyOnList);
                        }
                    }
                }

                new Handler().postDelayed(() -> isClickAdd = false, 3000);


                break;

            case R.id.tvDateBought:
                isDateBought = true;
                getDate(tvDateBought.getText().toString());
                break;

            case R.id.tvDateApply:
                isDateBought = false;
                getDate(tvDateApply.getText().toString());
                break;

            case R.id.btnAddProduct:
                ExpenseProductBean tempBean = spProductList.get(spProduct.getSelectedItemPosition());
                String tempPName = tempBean.getProductName();
                String tempQty = etQty.getText().toString().trim();
                if (verifyProductExpense(tempPName, tempQty)){
                    ExpenseProductBean bean = new ExpenseProductBean();
                    bean.setProductName(tempBean.getProductName());
                    bean.setProductId(tempBean.getProductId());
                    bean.setQuantity(String.valueOf(Integer.parseInt(tempQty)));
                    bean.setPrice(tempBean.getPrice());
                    selectedProductList.add(0, bean);
                    productExpenseAdapter.notifyItemInserted(0);

                    updateCost(true, bean);
                    resetProductData();
                }
                break;

            //open dialog
            case R.id.rlApplyOn:
                if (!isClick) {
                    isClick = true;

                    ArrayList<IncomeOrExpenseApplyOnBean> expenseApplyOnBeans = new ArrayList<>();
                    for (IncomeOrExpenseApplyOnBean a : applyOnList) {
                        IncomeOrExpenseApplyOnBean applyOnBean = new IncomeOrExpenseApplyOnBean();
                        applyOnBean.setSelect(a.getSelect());
                        applyOnBean.setRecordKey(a.getRecordKey());
                        applyOnBean.setName(a.getName());
                        expenseApplyOnBeans.add(applyOnBean);
                    }
                    ExpenseApplyOnDialog.newInstance(expenseApplyOnBeans, (tempApplyOnList, data, showData) -> {
                        applyOnList.clear();
                        applyOnList.addAll(tempApplyOnList);
                        tempApplyOn = data;
                        tvApplyOn.setText(showData);
                    }).show(getChildFragmentManager());
                }

                new Handler().postDelayed(() -> isClick = false, 3000);
                break;
        }
    }

    private synchronized void updateCost(Boolean  isAdd, ExpenseProductBean bean) {
        try {
          /* BigDecimal bd = new BigDecimal(bean.getPrice());
           bd = bd.round(new MathContext(2, RoundingMode.HALF_UP));*/

            if (isAdd) ttlCost += Integer.parseInt(bean.getPrice()) * Integer.parseInt(bean.getQuantity());
            else ttlCost -= Integer.parseInt(bean.getPrice()) * Integer.parseInt(bean.getQuantity());

            tvCost.setText(String.valueOf(ttlCost));
        } catch (Exception ignored) {
        }
    }

    private void resetProductData() {
        hideKeyboard();
        spProduct.setSelection(0);
        etQty.setText("");
    }

    private boolean verifyProductExpense(String tempPName, String tempQty) {
        if (tempPName.equalsIgnoreCase(getString(R.string.select_product))) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_select_prod), Toast.LENGTH_SHORT);
            return false;
        }else if (tempQty.isEmpty()){
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_qty), Toast.LENGTH_SHORT);
            return false;
        } else if (Double.parseDouble(tempQty) <= 0) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_qty_zero), Toast.LENGTH_SHORT);
            return false;
        } else return true;
    }

    private boolean isVerifyInput(String tempDB, String tempCost, String tempDA, String applyOn, String applyOnBuilder) {
        if (selectedProductList.isEmpty()){
            String tempPName = spProductList.get(spProduct.getSelectedItemPosition()).getProductName();
            String tempQty = etQty.getText().toString().trim();

            if (tempPName.equalsIgnoreCase(getString(R.string.select_product))) {
                CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_select_prod), Toast.LENGTH_SHORT);
                return false;
            } else if (tempQty.isEmpty()) {
                CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_qty), Toast.LENGTH_SHORT);
                return false;
            } else if (Double.parseDouble(tempQty) <= 0) {
                CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_qty_zero), Toast.LENGTH_SHORT);
                return false;
            } else {
                CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_press_add), Toast.LENGTH_SHORT);
                return false;
            }
        } else if (tempDB.isEmpty()){
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_select_db), Toast.LENGTH_SHORT);
            return false;
        }else if (tempCost.isEmpty()){
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_cost), Toast.LENGTH_SHORT);
            return false;
        }else if (tempDA.isEmpty()){
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_select_da), Toast.LENGTH_SHORT);
            return false;
        }else if (applyOn.isEmpty()){
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_select_apply_on), Toast.LENGTH_SHORT);
            return false;
        } else if (applyOnBuilder.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_select_expense_option), Toast.LENGTH_SHORT);
            return false;
        }else return true;
    }

    private void doAddCropExpense(String productJson, String tempDB, String tempCost, String tempDA, String applyOn, String tempApplyOn) {
        setLoading(true);

        HashMap<String, String> mParameterMap = new HashMap<>();
        mParameterMap.put("productDetail", productJson); // [{"productId": "1541164719.55715","quantity": "16","price": "1235"},{"productId": "1541164719.55715","quantity": "16","price": "1235"}]
        mParameterMap.put("dateBought", tempDB);  //bought date
        mParameterMap.put("cost", tempCost);  //expense total cost
        mParameterMap.put("dateApplied", tempDA);  //applied date not required
        mParameterMap.put("description", etDescription.getText().toString().trim());  //not required
        mParameterMap.put("recordKey", CalenderUtils.getTimestamp());  //expense record key
        mParameterMap.put("expenseFor", AppConstants.EXPENSE_FOR_CROP);  //animal,crop
        mParameterMap.put("appliedOn", applyOn);  //animal,crop,lot,parcel,property
        mParameterMap.put("appliedOnId", tempApplyOn); //comma seprated (animal recordkey,crop recordkey,lot id,parcel recordkey,property id)
        getDataManager().doServerAddExpenseApiCall(getDataManager().getHeader(),mParameterMap)
                .getAsJsonObject(new VolleyJsonObjectListener() {

                    @Override
                    public void onVolleyJsonObject(JSONObject jsonObject) {
                        setLoading(false);

                        try {
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");
                            if (status.equals("success")) {
                                //Todo check local db
                                doAddEditExpenseToLocalDB("add", jsonObject, message);
                            } else {
                                CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                            }
                        }catch (Exception e){
                            CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyJsonException() {
                        setLoading(false);
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    private void doUpdateCropExpense(String productJson, String tempDB, String tempCost, String tempDA, String applyOn, String tempApplyOn) {
        setLoading(true);

        HashMap<String, String> mParameterMap = new HashMap<>();
        mParameterMap.put("productDetail", productJson); // [{"productId": "1541164719.55715","quantity": "16","price": "1235"},{"productId": "1541164719.55715","quantity": "16","price": "1235"}]
        mParameterMap.put("dateBought", tempDB);  //bought date
        mParameterMap.put("cost", tempCost);  //expense total cost
        mParameterMap.put("dateApplied", tempDA);  //applied date not required
        mParameterMap.put("description", etDescription.getText().toString().trim());  //not required
        mParameterMap.put("recordKey", expenseListBean.getRecordKey());  //expense record key
        mParameterMap.put("expenseFor", AppConstants.EXPENSE_FOR_CROP);  //animal,crop
        mParameterMap.put("appliedOn", applyOn);  //animal,crop,lot,parcel,property
        mParameterMap.put("appliedOnId", tempApplyOn); //comma seprated (animal recordkey,crop recordkey,lot id,parcel recordkey,property id)
        getDataManager().doServerUpdateExpenseApiCall(getDataManager().getHeader(),mParameterMap)
                .getAsJsonObject(new VolleyJsonObjectListener() {

                    @Override
                    public void onVolleyJsonObject(JSONObject jsonObject) {
                        setLoading(false);

                        try {
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");
                            if (status.equals("success")) {
                                //Todo check local db
                                doAddEditExpenseToLocalDB("edit", jsonObject, message);
                            } else {
                                CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                            }
                        }catch (Exception e){
                            CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyJsonException() {
                        setLoading(false);
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    private void getDate(String selectedDate) {
        //date 26/06/2018

        Calendar newCalender = Calendar.getInstance();

        int day, month, year;

        if (selectedDate.isEmpty()) {
            day = newCalender.get(Calendar.DAY_OF_MONTH);
            month = newCalender.get(Calendar.MONTH);
            year = newCalender.get(Calendar.YEAR);
        } else {
            String[] date = selectedDate.split("/");

            day = Integer.parseInt(date[0]);
            month = Integer.parseInt(date[1]) - 1;
            year = Integer.parseInt(date[2]);
        }

        DatePickerDialog datePickerDialog = new DatePickerDialog(getBaseActivity(), datePicker, year, month, day);

        Boolean isMax = true;
        if (!isDateBought && !tvDateBought.getText().toString().isEmpty() && tvDateApply.getText().toString().isEmpty()) {
            Date cDate = CalenderUtils.getDateFormat(tvDateBought.getText().toString(), AppConstants.TIMESTAMP_FORMAT);
            assert cDate != null;
            datePickerDialog.getDatePicker().setMinDate(cDate.getTime());
        } else if (isDateBought && tvDateBought.getText().toString().isEmpty() && !tvDateApply.getText().toString().isEmpty()) {
            Date cDate = CalenderUtils.getDateFormat(tvDateApply.getText().toString(), AppConstants.TIMESTAMP_FORMAT);
            assert cDate != null;
            datePickerDialog.getDatePicker().setMaxDate(cDate.getTime());

            isMax = false;
        } else if (isDateBought && !tvDateBought.getText().toString().isEmpty() && !tvDateApply.getText().toString().isEmpty()) {
            Date cDateMax = CalenderUtils.getDateFormat(tvDateApply.getText().toString(), AppConstants.TIMESTAMP_FORMAT);
            assert cDateMax != null;
            datePickerDialog.getDatePicker().setMaxDate(cDateMax.getTime());

            isMax = false;
        } else if (!isDateBought && !tvDateBought.getText().toString().isEmpty() && !tvDateApply.getText().toString().isEmpty()) {
            Date cDateMin = CalenderUtils.getDateFormat(tvDateBought.getText().toString(), AppConstants.TIMESTAMP_FORMAT);
            assert cDateMin != null;
            datePickerDialog.getDatePicker().setMinDate(cDateMin.getTime());
        }

        if (isMax){
            Date maxDate = CalenderUtils.getDateFormat(CalenderUtils.getCurrentDate(), AppConstants.TIMESTAMP_FORMAT);
            assert maxDate != null;
            datePickerDialog.getDatePicker().setMaxDate(maxDate.getTime());
        }

        datePickerDialog.show();
    }

    /*Local DB Start here*/
    private void doAddEditExpenseToLocalDB(String flag, JSONObject jsonObject, String message) {
        new Thread(() -> {
            try {
                JSONObject expenseDetailObj = jsonObject.getJSONObject("expenseDetail");
                List<ExpenseMapping> expenseMappingList = new ArrayList<>();
                List<ExpenseProductMapping> productMappingList = new ArrayList<>();

                Expense expense = new Expense();

                expense.setExpenseId(expenseDetailObj.getString("expenseId"));
                expense.setUser_id(expenseDetailObj.getString("user_id"));
                expense.setDateBought(expenseDetailObj.getString("dateBought"));
                expense.setCost(expenseDetailObj.getString("cost"));
                expense.setDescription(expenseDetailObj.getString("description"));
                String dateApplied = expenseDetailObj.getString("dateApplied");
                expense.setDateApplied(dateApplied.isEmpty() ? null : CalenderUtils.getDateFormat(dateApplied.contains("/") ? CalenderUtils.formatDate(dateApplied, AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT) : dateApplied, AppConstants.SERVER_TIMESTAMP_FORMAT));
                expense.setExpenseFor(expenseDetailObj.getString("expenseFor"));
                expense.setStatus(expenseDetailObj.getString("status"));
                expense.setRecordKey(expenseDetailObj.getString("recordKey"));
                expense.setCrd(expenseDetailObj.getString("crd"));
                expense.setUpd(expenseDetailObj.getString("upd"));
                expense.setEvent(flag.equals("add") ? AppConstants.DB_EVENT_ADD : AppConstants.DB_EVENT_EDIT);
                expense.setDataSync(AppConstants.DB_SYNC_TRUE);

                JSONArray appliedOnArray = expenseDetailObj.getJSONArray("appliedOn");

                for (int i = 0; i < appliedOnArray.length(); i++) {
                    JSONObject applyOnObj = appliedOnArray.getJSONObject(i);
                    ExpenseMapping expenseMapping = new ExpenseMapping();

                    expenseMapping.setExpenseMappingId(applyOnObj.getString("expenseMappingId"));
                    expenseMapping.setExpense_id(applyOnObj.getString("expense_id"));
                    expenseMapping.setAppliedOn(applyOnObj.getString("appliedOn"));
                    expenseMapping.setAppliedOnId(applyOnObj.getString("appliedOnRecordKey"));
                    expenseMapping.setStatus(applyOnObj.getString("status"));
                    expenseMapping.setCrd(applyOnObj.getString("crd"));
                    expenseMapping.setUpd(applyOnObj.getString("upd"));
                    expenseMapping.setEvent(flag.equals("add") ? AppConstants.DB_EVENT_ADD : AppConstants.DB_EVENT_EDIT);
                    expenseMapping.setDataSync(AppConstants.DB_SYNC_TRUE);
                    expenseMappingList.add(expenseMapping);
                }

                JSONArray productDetailArray = expenseDetailObj.getJSONArray("productDetails");
                for (int i = 0; i < productDetailArray.length(); i++) {
                    JSONObject productDetailObj = productDetailArray.getJSONObject(i);
                    ExpenseProductMapping expenseProductMapping = new ExpenseProductMapping();

                    expenseProductMapping.setExpenseProductMappingId(productDetailObj.getString("expenseProductMappingId"));
                    expenseProductMapping.setExpense_id(productDetailObj.getString("expense_id"));
                    expenseProductMapping.setProduct_id(productDetailObj.getString("product_id"));
                    expenseProductMapping.setQuantity(productDetailObj.getString("quantity"));
                    expenseProductMapping.setPrice(productDetailObj.getString("price"));
                    expenseProductMapping.setStatus(productDetailObj.getString("status"));
                    expenseProductMapping.setCrd(productDetailObj.getString("crd"));
                    expenseProductMapping.setUpd(productDetailObj.getString("upd"));
                    expenseProductMapping.setEvent(flag.equals("add") ? AppConstants.DB_EVENT_ADD : AppConstants.DB_EVENT_EDIT);
                    expenseProductMapping.setDataSync(AppConstants.DB_SYNC_TRUE);
                    productMappingList.add(expenseProductMapping);
                }

                switch (flag) {
                    case "add":
                        getDataManager().insertExpense(expense);
                        getDataManager().insertAllExpenseMapping(expenseMappingList);
                        getDataManager().insertAllExpenseProdMapping(productMappingList);
                        break;

                    case "edit":
                        //delete all expense childs
                        getDataManager().deleteExpenseMappingUsingExpenseId(expense.getExpenseId());
                        getDataManager().deleteExpenseProdMappingUsingExpenseId(expense.getExpenseId());

                        getDataManager().updateExpense(expense);
                        getDataManager().insertAllExpenseMapping(expenseMappingList);
                        getDataManager().insertAllExpenseProdMapping(productMappingList);
                        break;
                }

                handler.post(() -> {
                    CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                    getBaseActivity().onBackPressed();
                    if (expenseFragment != null) expenseFragment.hitApi(false);
                });


                AppLogger.d("database", "Expense add or edit success");
            } catch (Exception e) {
                AppLogger.d("database", "Expense add or edit exception");
            }
        }).start();
    }

    private void doAddEditCropExpenseToLocalDB(String flag, List<ExpenseProductBean> productList, String tempDB, String tempCost, String tempDA, String applyOn, List<IncomeOrExpenseApplyOnBean> tempApplyOn) {
        new Thread(() -> {
            List<ExpenseMapping> expenseMappingList = new ArrayList<>();

            Expense expense = new Expense();

            expense.setDateBought((tempDB.contains("/") ? CalenderUtils.formatDate(tempDB, AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT) : tempDB));
            expense.setCost(tempCost);
            expense.setDescription(etDescription.getText().toString().trim());
            expense.setDateApplied(CalenderUtils.getDateFormat(tempDA.contains("/") ? CalenderUtils.formatDate(tempDA, AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT) : tempDA, AppConstants.SERVER_TIMESTAMP_FORMAT));
            expense.setExpenseFor(AppConstants.EXPENSE_FOR_CROP);
            expense.setStatus("1");
            expense.setUpd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
            expense.setDataSync(AppConstants.DB_SYNC_FALSE);
            switch (flag) {
                case "add":
                    String rKey = CalenderUtils.getTimestamp();
                    expense.setExpenseId(rKey);
                    expense.setRecordKey(rKey);
                    expense.setUser_id(getDataManager().getUserInfo().getUserId());
                    expense.setCrd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    expense.setEvent(AppConstants.DB_EVENT_ADD);

                    getDataManager().insertExpense(expense);
                    break;

                case "edit":
                    expense.setExpenseId(expenseListBean.getExpenseId());
                    expense.setRecordKey(expenseListBean.getRecordKey());
                    expense.setUser_id(expenseListBean.getUser_id());
                    expense.setCrd(expenseListBean.getCrd());
                    expense.setEvent(AppConstants.DB_EVENT_EDIT);

                    //delete all expense childs
                    getDataManager().deleteExpenseMappingUsingExpenseId(expense.getExpenseId());
                    getDataManager().deleteExpenseProdMappingUsingExpenseId(expense.getExpenseId());

                    getDataManager().updateExpense(expense);
                    break;
            }

            for (IncomeOrExpenseApplyOnBean applyOnObj : tempApplyOn) {
                ExpenseMapping expenseMapping = new ExpenseMapping();

                if (applyOnObj.getSelect()) {
                    expenseMapping.setExpenseMappingId(CalenderUtils.getTimestamp());
                    expenseMapping.setExpense_id(expense.getExpenseId());
                    expenseMapping.setAppliedOn(applyOn);
                    expenseMapping.setAppliedOnId(applyOnObj.getRecordKey());
                    expenseMapping.setStatus("1");
                    expenseMapping.setCrd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    expenseMapping.setUpd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    expenseMapping.setEvent(flag.equals("add") ? AppConstants.DB_EVENT_ADD : AppConstants.DB_EVENT_EDIT);
                    expenseMapping.setDataSync(AppConstants.DB_SYNC_FALSE);
                    expenseMappingList.add(expenseMapping);
                }
            }
            getDataManager().insertAllExpenseMapping(expenseMappingList);

            for (ExpenseProductBean productDetailObj : productList) {

                ExpenseProductMapping expenseProductMapping = new ExpenseProductMapping();

                expenseProductMapping.setExpenseProductMappingId(CalenderUtils.getTimestamp());
                expenseProductMapping.setExpense_id(expense.getExpenseId());
                expenseProductMapping.setProduct_id(productDetailObj.getProductId());
                expenseProductMapping.setQuantity(productDetailObj.getQuantity());
                expenseProductMapping.setPrice(productDetailObj.getPrice());
                expenseProductMapping.setStatus("1");
                expenseProductMapping.setCrd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                expenseProductMapping.setUpd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                expenseProductMapping.setEvent(flag.equals("add") ? AppConstants.DB_EVENT_ADD : AppConstants.DB_EVENT_EDIT);
                expenseProductMapping.setDataSync(AppConstants.DB_SYNC_FALSE);

                //same product qty merge logic here
                //always data insert because productMapping table clear
                ExpenseProductMapping productBean = getDataManager().getExpenseProductDetail(expenseProductMapping.getExpense_id(), expenseProductMapping.getProduct_id());

                if (productBean == null) {
                    getDataManager().insertExpenseProdMapping(expenseProductMapping);
                } else {
                    int qty = Integer.parseInt(productBean.getQuantity()) + Integer.parseInt(productDetailObj.getQuantity());
                    expenseProductMapping.setExpenseProductMappingId(productBean.getExpenseProductMappingId());
                    expenseProductMapping.setQuantity(String.valueOf(qty));
                    getDataManager().updateExpenseProdMapping(expenseProductMapping);
                }
            }

            switch (flag) {
                case "add":
                    handler.post(() -> {
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.expenseAddSuccessMsg), Toast.LENGTH_SHORT);
                        getBaseActivity().onBackPressed();
                        if (expenseFragment != null) expenseFragment.hitApi(false);
                    });
                    break;

                case "edit":
                    handler.post(() -> {
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.expenseUpdateSuccessMsg), Toast.LENGTH_SHORT);
                        getBaseActivity().onBackPressed();
                        if (expenseFragment != null) expenseFragment.hitApi(false);
                    });
                    break;
            }
            getDataManager().setLocalDataExistForSync(true);
            AppLogger.d("database", "Expense add or edit success");
        }).start();
    }
    /*Local DB End here*/
}
