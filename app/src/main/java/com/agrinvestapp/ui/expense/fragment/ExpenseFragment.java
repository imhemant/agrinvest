package com.agrinvestapp.ui.expense.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.ExpenseInfoResponse;
import com.agrinvestapp.data.model.db.Expense;
import com.agrinvestapp.data.model.db.ExpenseMapping;
import com.agrinvestapp.data.model.db.ExpenseProductMapping;
import com.agrinvestapp.ui.base.BaseFragment;
import com.agrinvestapp.ui.base.ClickListener;
import com.agrinvestapp.ui.base.dialog.DeleteDialog;
import com.agrinvestapp.ui.expense.adapter.ExpenseInfoAdapter;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.AppLogger;
import com.agrinvestapp.utils.AppUtils;
import com.agrinvestapp.utils.CalenderUtils;
import com.agrinvestapp.utils.CommonUtils;
import com.agrinvestapp.volley.VolleyJsonObjectListener;
import com.agrinvestapp.volley.VolleyRequest;
import com.agrinvestapp.volley.VolleyResponseListener;
import com.android.volley.VolleyError;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExpenseFragment extends BaseFragment implements View.OnClickListener {

    public static String expenseType;
    private TextView tvNoRecord, tvAnimal, tvCrop;
    private Button btnAdd;
    private FrameLayout frameAdd;

    private List<ExpenseInfoResponse.ExpenseListBean> list;
    private ExpenseInfoAdapter adapter;

    private Handler handler = new Handler(Looper.getMainLooper());

    public static ExpenseFragment newInstance() {
        expenseType = "animal";
        Bundle args = new Bundle();

        ExpenseFragment fragment = new ExpenseFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_expense, container, false);
        initView(view);
        return  view;
    }

    private void initView(View view) {
        /* toolbar view start */
        ImageView imgBack = view.findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);
        imgBack.setVisibility(View.VISIBLE);

        TextView tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.expenses);
        /* toolbar view end */
        view.findViewById(R.id.rlMain).setOnClickListener(this); //avoid bg click

        RecyclerView rvExpense = view.findViewById(R.id.rvExpense);
        list = new ArrayList<>();

        adapter = new ExpenseInfoAdapter(list, new ClickListener() {
            @Override
            public void onItemClick(int pos) {
                getBaseActivity().addFragment(ExpenseDetailFragment.newInstance(list.get(pos)), R.id.expenseFrame, true);
            }

            @Override
            public void onEditClick(int pos) {
                if (expenseType.equalsIgnoreCase("animal"))
                    getBaseActivity().addFragment(AddEditAnimalExpenseFragment.newInstance(false, ExpenseFragment.this, list.get(pos)), R.id.expenseFrame, true);
                else getBaseActivity().addFragment(AddEditCropExpenseFragment.newInstance(false, ExpenseFragment.this, list.get(pos)), R.id.expenseFrame, true);
            }

            @Override
            public void onDeleteClick(int pos) {
                DeleteDialog.newInstance(false, "", () -> {
                    if (isNetworkConnected(true)) {
                        getBaseActivity().doSyncAppDB(false, flag -> {
                            switch (flag) {
                                case AppConstants.ONLINE:
                                    if (!list.get(pos).getExpenseId().equals(list.get(pos).getRecordKey()))
                                        doDeleteExpense(pos);
                                    else
                                        deleteExpenseDataFromDB(AppConstants.OFFLINE, pos, list.get(pos));
                                    break;

                                case AppConstants.OFFLINE:
                                    deleteExpenseDataFromDB(AppConstants.OFFLINE, pos, list.get(pos));
                                    break;
                            }
                        });
                    } else {
                        deleteExpenseDataFromDB(AppConstants.OFFLINE, pos, list.get(pos));
                    }

                }).show(getChildFragmentManager());
            }
        });
        rvExpense.setAdapter(adapter);

        tvNoRecord = view.findViewById(R.id.tvNoRecord);
        frameAdd = view.findViewById(R.id.frameAdd);
        frameAdd.setOnClickListener(this);
        btnAdd = view.findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);

        tvAnimal = view.findViewById(R.id.tvAnimal);
        tvAnimal.setOnClickListener(this);
        tvCrop = view.findViewById(R.id.tvCrop);
        tvCrop.setOnClickListener(this);
    }

    private void doDeleteExpense(int pos) {
        setLoading(true);
        HashMap<String, String> mParameterMap = new HashMap<>();
        mParameterMap.put("recordKey", list.get(pos).getRecordKey());

        getDataManager().doServerDeleteExpenseApiCall(getDataManager().getHeader(), mParameterMap)
                .getAsJsonObject(new VolleyJsonObjectListener() {

                    @Override
                    public void onVolleyJsonObject(JSONObject jsonObject) {
                        setLoading(false);

                        try {
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            if (status.equals("success")) {
                                CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                                //Todo check local db
                                deleteExpenseDataFromDB(AppConstants.ONLINE, pos, list.get(pos));
                                list.remove(pos);
                                adapter.notifyItemRemoved(pos);

                                updateView();
                            } else {
                                updateView();
                                CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                            }
                        }catch (Exception e){
                            updateView();
                            CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyJsonException() {
                        setLoading(false);
                        updateView();
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        updateView();
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hitApi(true);
        initAd(view);
    }

    private void initAd(View view) {
        AdView mAdView = view.findViewById(R.id.adView);
        mAdView.setVisibility(View.VISIBLE);
        AppUtils.setAdBanner(mAdView, frameAdd);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                AppUtils.setMargins(frameAdd, 0, 0, 15, 15);
                mAdView.setVisibility(View.GONE);
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when when the user is about to return
                // to the app after tapping on an ad.
            }
        });
    }

    public void hitApi(Boolean isAnimalExpense) {
        if (isNetworkConnected(true)) {
            getBaseActivity().doSyncAppDB(false, flag -> {
                switch (flag) {
                    case AppConstants.ONLINE:
                        //getExpenseInfo(isAnimalExpense);
                        getExpenseInfoFromDB(isAnimalExpense);
                        break;

                    case AppConstants.OFFLINE:
                        getExpenseInfoFromDB(isAnimalExpense);
                        break;
                }
            });
        } else {
            getExpenseInfoFromDB(isAnimalExpense);
        }

    }

    private void updateView() {
        if (list.isEmpty()) {
            frameAdd.setVisibility(View.GONE);
            tvNoRecord.setVisibility(View.VISIBLE);
            btnAdd.setVisibility(View.VISIBLE);
        }
    }

    private void getExpenseInfo(Boolean isAnimalExpense) {
        setLoading(true);
        getDataManager().doServerExpenseInfoApiCall(isAnimalExpense, getDataManager().getHeader())
                .getAsResponse(new VolleyResponseListener() {
                    @Override
                    public void onVolleyResponse(String response) {
                        setLoading(false);
                        ExpenseInfoResponse expenseInfoResponse = getDataManager().mGson.fromJson(response, ExpenseInfoResponse.class);

                        if (expenseInfoResponse.getStatus().equals("success")) {
                            list.clear();
                            for (ExpenseInfoResponse.ExpenseListBean bean : expenseInfoResponse.getExpenseList()) {
                                StringBuilder builder = new StringBuilder("");
                                for (int i = 0; i < bean.getAppliedOn().size(); i++) {
                                    if (i<=1){
                                        builder.append(bean.getAppliedOn().get(i).getAppliedName()).append(", ");
                                    }else break;
                                }

                                bean.setApplyOnNameCount(bean.getAppliedOn().size()-2<1?"":String.valueOf(bean.getAppliedOn().size()-2));
                                bean.setApplyOnName(builder.toString().isEmpty() ? "" : builder.toString().substring(0, builder.toString().trim().length() - 1));
                                list.add(bean);
                            }

                            updateView();
                            if (!list.isEmpty()) {
                                frameAdd.setVisibility(View.VISIBLE);
                                tvNoRecord.setVisibility(View.GONE);
                                btnAdd.setVisibility(View.GONE);

                                //Todo check local db
                                updateExpenseDataDB(expenseInfoResponse);
                            }

                            adapter.notifyDataSetChanged();
                        } else {
                            if (expenseInfoResponse.getMessage().equals(getString(R.string.no_record_found))) {
                                list.clear();
                                adapter.notifyDataSetChanged();
                            } else CommonUtils.showToast(getBaseActivity(), expenseInfoResponse.getMessage(), Toast.LENGTH_SHORT);

                            updateView();
                        }
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        updateView();
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                getBaseActivity().onBackPressed();
                break;

            case R.id.btnAdd:
            case R.id.frameAdd:
                btnAdd.setClickable(false);
                frameAdd.setClickable(false);
                if (expenseType.equalsIgnoreCase("animal"))
                    getBaseActivity().addFragment(AddEditAnimalExpenseFragment.newInstance(true, ExpenseFragment.this, null), R.id.expenseFrame, true);
                else  getBaseActivity().addFragment(AddEditCropExpenseFragment.newInstance(true, ExpenseFragment.this, null), R.id.expenseFrame, true);

                new Handler().postDelayed(() -> {
                    btnAdd.setClickable(true);
                    frameAdd.setClickable(true);
                }, 2000);
                break;

            case R.id.tvAnimal:
                if (!expenseType.equalsIgnoreCase("animal")){
                    expenseType = "animal";
                    tvAnimal.setTextColor(getResources().getColor(R.color.colorWhite));
                    tvCrop.setTextColor(getResources().getColor(R.color.grey));
                    tvAnimal.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                    tvCrop.setBackgroundColor(getResources().getColor(R.color.colorWhite));

                    if (isNetworkConnected(false)) {
                        //getExpenseInfo( true);
                        getExpenseInfoFromDB(true);
                    } else {
                        getExpenseInfoFromDB(true);
                    }
                }
                break;

            case R.id.tvCrop:
                if (!expenseType.equalsIgnoreCase("crop")) {
                    expenseType = "crop";
                    tvCrop.setTextColor(getResources().getColor(R.color.colorWhite));
                    tvAnimal.setTextColor(getResources().getColor(R.color.grey));
                    tvCrop.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                    tvAnimal.setBackgroundColor(getResources().getColor(R.color.colorWhite));

                    if (isNetworkConnected(false)) {
                        //getExpenseInfo(false);
                        getExpenseInfoFromDB(false);
                    } else {
                        getExpenseInfoFromDB(false);
                    }
                }
                break;
        }
    }

    /*Local DB start here*/
    private void getExpenseInfoFromDB(Boolean isAnimalExpense) {
        new Thread(() -> {
            list.clear();
            List<ExpenseInfoResponse.ExpenseListBean> expenseList = getDataManager().getExpenseList(isAnimalExpense ? "animal" : "crop");

            for (ExpenseInfoResponse.ExpenseListBean mainBean : expenseList) {
                List<ExpenseInfoResponse.ExpenseListBean.AppliedOnBean> appliedOnList = new ArrayList<>();

                switch (mainBean.getApplyOnName() == null ? "" : mainBean.getApplyOnName()) {
                    case "animal":
                        appliedOnList = getDataManager().getAnimalExpenseAppliedOnList(mainBean.getExpenseId());
                        break;

                    case "crop":
                        appliedOnList = getDataManager().getCropExpenseAppliedOnList(mainBean.getExpenseId());
                        break;

                    case "lot":
                        if (mainBean.getExpenseFor().equals("animal"))
                            appliedOnList = getDataManager().getAnimalLotExpenseAppliedOnList(mainBean.getExpenseId());
                        else
                            appliedOnList = getDataManager().getCropLotExpenseAppliedOnList(mainBean.getExpenseId());
                        break;

                    case "parcel":
                        appliedOnList = getDataManager().getParcelExpenseAppliedOnList(mainBean.getExpenseId());
                        break;

                    case "property":
                        appliedOnList = getDataManager().getPropertyExpenseAppliedOnList(mainBean.getExpenseId());
                        break;
                }

                List<ExpenseInfoResponse.ExpenseListBean.ProductDetailsBean> productList = getDataManager().getExpenseProductList(mainBean.getExpenseId());
                mainBean.setAppliedOn(appliedOnList);
                mainBean.setProductDetails(productList);
                mainBean.setProductName(productList.isEmpty() ? "" : productList.get(0).getProductName());
                StringBuilder builder = new StringBuilder("");
                for (int i = 0; i < mainBean.getAppliedOn().size(); i++) {
                    if (i <= 1) {
                        builder.append(mainBean.getAppliedOn().get(i).getAppliedName()).append(", ");
                    } else break;
                }

                mainBean.setApplyOnNameCount(mainBean.getAppliedOn().size() - 2 < 1 ? "" : String.valueOf(mainBean.getAppliedOn().size() - 2));
                mainBean.setApplyOnName(builder.toString().isEmpty() ? "" : builder.toString().substring(0, builder.toString().trim().length() - 1));

                list.add(mainBean);
            }

            handler.post(() -> {
                updateView();
                if (!list.isEmpty()) {
                    frameAdd.setVisibility(View.VISIBLE);
                    tvNoRecord.setVisibility(View.GONE);
                    btnAdd.setVisibility(View.GONE);
                }

                adapter.notifyDataSetChanged();
            });

        }).start();
    }

    private void updateExpenseDataDB(ExpenseInfoResponse expenseInfoResponse) {
        new Thread(() -> {
            AppLogger.d("database", "Expense table clear success");
            getDataManager().deleteAllExpense();
            List<Expense> expenseList = new ArrayList<>();
            List<ExpenseMapping> expenseMappingList = new ArrayList<>();
            List<ExpenseProductMapping> expenseProductMappingList = new ArrayList<>();

            for (ExpenseInfoResponse.ExpenseListBean expenseListBean : expenseInfoResponse.getExpenseList()) {

                Expense expense = new Expense();

                expense.setExpenseId(expenseListBean.getExpenseId());
                expense.setUser_id(expenseListBean.getUser_id());
                expense.setDateBought(expenseListBean.getDateBought());
                expense.setCost(expenseListBean.getCost());
                expense.setDescription(expenseListBean.getDescription());
                expense.setDateApplied(expenseListBean.getDateApplied().isEmpty() ? null : CalenderUtils.getDateFormat(expenseListBean.getDateApplied().contains("/") ? CalenderUtils.formatDate(expenseListBean.getDateApplied(), AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT) : expenseListBean.getDateApplied(), AppConstants.SERVER_TIMESTAMP_FORMAT));
                expense.setExpenseFor(expenseListBean.getExpenseFor());
                expense.setStatus(expenseListBean.getStatus());
                expense.setRecordKey(expenseListBean.getRecordKey());
                expense.setCrd(expenseListBean.getCrd());
                expense.setUpd(expenseListBean.getUpd());
                expense.setEvent(AppConstants.DB_EVENT_ADD);
                expense.setDataSync(AppConstants.DB_SYNC_TRUE);
                expenseList.add(expense);

                for (ExpenseInfoResponse.ExpenseListBean.AppliedOnBean applyOnBean : expenseListBean.getAppliedOn()) {
                    ExpenseMapping expenseMapping = new ExpenseMapping();

                    expenseMapping.setExpenseMappingId(applyOnBean.getExpenseMappingId());
                    expenseMapping.setExpense_id(applyOnBean.getExpense_id());
                    expenseMapping.setAppliedOn(applyOnBean.getAppliedOn());
                    expenseMapping.setAppliedOnId(applyOnBean.getAppliedOnRecordKey()); //id treat as record key
                    expenseMapping.setStatus(applyOnBean.getStatus());
                    expenseMapping.setCrd(applyOnBean.getCrd());
                    expenseMapping.setUpd(applyOnBean.getUpd());
                    expenseMapping.setEvent(AppConstants.DB_EVENT_ADD);
                    expenseMapping.setDataSync(AppConstants.DB_SYNC_TRUE);
                    expenseMappingList.add(expenseMapping);
                }

                for (ExpenseInfoResponse.ExpenseListBean.ProductDetailsBean productDetailsBean : expenseListBean.getProductDetails()) {
                    ExpenseProductMapping expenseProductMapping = new ExpenseProductMapping();

                    expenseProductMapping.setExpenseProductMappingId(productDetailsBean.getExpenseProductMappingId());
                    expenseProductMapping.setExpense_id(productDetailsBean.getExpense_id());
                    expenseProductMapping.setProduct_id(productDetailsBean.getProduct_id());
                    expenseProductMapping.setQuantity(productDetailsBean.getQuantity());
                    expenseProductMapping.setPrice(productDetailsBean.getPrice());
                    expenseProductMapping.setStatus(productDetailsBean.getStatus());
                    expenseProductMapping.setCrd(productDetailsBean.getCrd());
                    expenseProductMapping.setUpd(productDetailsBean.getUpd());
                    expenseProductMapping.setEvent(AppConstants.DB_EVENT_ADD);
                    expenseProductMapping.setDataSync(AppConstants.DB_SYNC_TRUE);

                    expenseProductMappingList.add(expenseProductMapping);
                }

            }
            getDataManager().insertAllExpense(expenseList);
            getDataManager().insertAllExpenseMapping(expenseMappingList);
            getDataManager().insertAllExpenseProdMapping(expenseProductMappingList);
            AppLogger.d("database", "Db Add Expense, ExpenseMapping, ExpenseProdMapping done");
        }).start();
    }

    private void deleteExpenseDataFromDB(String flag, int pos, ExpenseInfoResponse.ExpenseListBean productListBean) {
        new Thread(() -> {
            switch (flag) {
                case AppConstants.ONLINE:
                    //delete here
                    getDataManager().deleteUsingExpenseId(productListBean.getExpenseId());
                    break;

                case AppConstants.OFFLINE:
                    if (productListBean.getExpenseId().equals(productListBean.getRecordKey())) {
                        getDataManager().deleteUsingExpenseId(productListBean.getExpenseId());
                    } else {
                        List<ExpenseMapping> expenseMappingList = new ArrayList<>();
                        List<ExpenseProductMapping> productMappingList = new ArrayList<>();

                        Expense expense = new Expense();

                        expense.setExpenseId(productListBean.getExpenseId());
                        expense.setUser_id(productListBean.getUser_id());
                        expense.setDateBought(productListBean.getDateBought());
                        expense.setCost(productListBean.getCost());
                        expense.setDescription(productListBean.getDescription());
                        expense.setDateApplied(productListBean.getDateApplied().isEmpty() ? null : CalenderUtils.getDateFormat(productListBean.getDateApplied().contains("/") ? CalenderUtils.formatDate(productListBean.getDateApplied(), AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT) : productListBean.getDateApplied(), AppConstants.SERVER_TIMESTAMP_FORMAT));
                        expense.setExpenseFor(productListBean.getExpenseFor());
                        expense.setStatus(productListBean.getStatus());
                        expense.setRecordKey(productListBean.getRecordKey());
                        expense.setCrd(productListBean.getCrd());
                        expense.setUpd(productListBean.getUpd());
                        expense.setEvent(AppConstants.DB_EVENT_DEL);
                        expense.setDataSync(AppConstants.DB_SYNC_FALSE);

                        for (ExpenseInfoResponse.ExpenseListBean.AppliedOnBean applyOnObj : productListBean.getAppliedOn()) {
                            ExpenseMapping expenseMapping = new ExpenseMapping();

                            expenseMapping.setExpenseMappingId(applyOnObj.getExpenseMappingId());
                            expenseMapping.setExpense_id(expense.getExpenseId());
                            expenseMapping.setAppliedOn(applyOnObj.getAppliedOn());
                            expenseMapping.setAppliedOnId(applyOnObj.getAppliedOnRecordKey());
                            expenseMapping.setStatus(applyOnObj.getStatus());
                            expenseMapping.setCrd(applyOnObj.getCrd());
                            expenseMapping.setUpd(applyOnObj.getUpd());
                            expenseMapping.setEvent(AppConstants.DB_EVENT_DEL);
                            expenseMapping.setDataSync(AppConstants.DB_SYNC_FALSE);
                            expenseMappingList.add(expenseMapping);
                        }

                        for (ExpenseInfoResponse.ExpenseListBean.ProductDetailsBean productDetailObj : productListBean.getProductDetails()) {

                            ExpenseProductMapping expenseProductMapping = new ExpenseProductMapping();

                            expenseProductMapping.setExpenseProductMappingId(productDetailObj.getExpenseProductMappingId());
                            expenseProductMapping.setExpense_id(expense.getExpenseId());
                            expenseProductMapping.setProduct_id(productDetailObj.getProduct_id());
                            expenseProductMapping.setQuantity(productDetailObj.getQuantity());
                            expenseProductMapping.setPrice(productDetailObj.getPrice());
                            expenseProductMapping.setStatus(productDetailObj.getStatus());
                            expenseProductMapping.setCrd(productDetailObj.getCrd());
                            expenseProductMapping.setUpd(productDetailObj.getUpd());
                            expenseProductMapping.setEvent(AppConstants.DB_EVENT_DEL);
                            expenseProductMapping.setDataSync(AppConstants.DB_SYNC_FALSE);
                            productMappingList.add(expenseProductMapping);
                        }
                        //delete all expense childs
                        getDataManager().deleteExpenseMappingUsingExpenseId(expense.getExpenseId());
                        getDataManager().deleteExpenseProdMappingUsingExpenseId(expense.getExpenseId());

                        getDataManager().updateExpense(expense);
                        getDataManager().insertAllExpenseMapping(expenseMappingList);
                        getDataManager().insertAllExpenseProdMapping(productMappingList);

                        //set sync status true
                        getDataManager().setLocalDataExistForSync(true);
                    }

                    handler.post(() -> {
                        list.remove(pos);
                        adapter.notifyItemRemoved(pos);

                        updateView();
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.expenseDelSuccessMsg), Toast.LENGTH_SHORT);
                    });
                    break;
            }

            AppLogger.d("database", "product delete success");
        }).start();
    }
    /*Local DB end here*/
}
