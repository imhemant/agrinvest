package com.agrinvestapp.ui.expense.dialog;

import com.agrinvestapp.data.model.other.IncomeOrExpenseApplyOnBean;

import java.util.List;

public interface ExpenseDialogCallback {
    void onDone(List<IncomeOrExpenseApplyOnBean> applyOnList, String data, String showData);
}
