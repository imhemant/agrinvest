package com.agrinvestapp.ui.expense.adapter;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.ExpenseInfoResponse;
import com.agrinvestapp.ui.base.ClickListener;
import com.agrinvestapp.ui.crop.CropActivity;
import com.agrinvestapp.utils.pagination.FooterLoader;

import java.util.List;


/**
 * Created by hemant
 * Date: 17/4/18
 * Time: 4:03 PM
 */

public class ExpenseInfoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEWTYPE_ITEM = 1;
    private final int VIEWTYPE_LOADER = 2;
    private List<ExpenseInfoResponse.ExpenseListBean> list;
    private boolean showLoader;
    private ClickListener clickListener;

    private int lastPos = -1;
    private boolean isClick = false;

    public ExpenseInfoAdapter(List<ExpenseInfoResponse.ExpenseListBean> list, ClickListener clickListener) {
        this.list = list;
        this.clickListener = clickListener;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder rvHolder, int position) {
        if (rvHolder instanceof FooterLoader) {
            FooterLoader loaderViewHolder = (FooterLoader) rvHolder;
            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }
            return;
        }
        ExpenseInfoAdapter.ViewHolder holder = ((ExpenseInfoAdapter.ViewHolder) rvHolder);

        ExpenseInfoResponse.ExpenseListBean bean = list.get(position);

        holder.tvTitle.setText(bean.getProductName());
        holder.tvApplyOn.setText(bean.getApplyOnName());
        if (bean.getApplyOnNameCount().isEmpty()){
            holder.tvApplyOnCount.setVisibility(View.GONE);
        }else{
            holder.tvApplyOnCount.setVisibility(View.VISIBLE);
            StringBuilder countBuilder = new StringBuilder();
            countBuilder.append("+").append(bean.getApplyOnNameCount()).append(" ").append(holder.tvTitle.getContext().getString(R.string.more));
            holder.tvApplyOnCount.setText(countBuilder);
        }

        StringBuilder builder = new StringBuilder();
        builder.append("$").append(bean.getCost());
        holder.tvCost.setText(builder);

        holder.menuView.setVisibility(bean.isSelected ? View.VISIBLE : View.GONE);

    }

    public void showLoading(boolean status) {
        showLoader = status;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case VIEWTYPE_ITEM:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_expense_info, parent, false);
                return new ViewHolder(view);

            case VIEWTYPE_LOADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pagination_item_loader, parent, false);
                return new FooterLoader(view);
        }
        return null;

    }

    @Override
    public int getItemViewType(int position) {
        if (position == list.size() - 1) {
            return showLoader ? VIEWTYPE_LOADER : VIEWTYPE_ITEM;
        }
        return VIEWTYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private void updateMenuUI(View menuView, LinearLayout llEdit, LinearLayout llDel, int status) {

        View sViewEdit = llEdit.getChildAt(0);
        View sViewDel = llDel.getChildAt(0);
        TextView tvEdit = (TextView) llEdit.getChildAt(1);
        TextView tvDel = (TextView) llDel.getChildAt(1);

        sViewEdit.setVisibility(View.INVISIBLE);
        sViewDel.setVisibility(View.INVISIBLE);
        tvEdit.setTextColor(sViewDel.getContext().getResources().getColor(R.color.colorLightBlack));
        tvDel.setTextColor(sViewDel.getContext().getResources().getColor(R.color.colorLightBlack));
        menuView.setVisibility(View.GONE);
        switch (status) {
            case 1:
                sViewEdit.setVisibility(View.VISIBLE);
                sViewDel.setVisibility(View.INVISIBLE);
                tvEdit.setTextColor(sViewDel.getContext().getResources().getColor(R.color.colorPrimaryDark));
                tvDel.setTextColor(sViewDel.getContext().getResources().getColor(R.color.colorLightBlack));
                break;

            case 2:
                sViewEdit.setVisibility(View.INVISIBLE);
                sViewDel.setVisibility(View.VISIBLE);
                tvEdit.setTextColor(sViewDel.getContext().getResources().getColor(R.color.colorLightBlack));
                tvDel.setTextColor(sViewDel.getContext().getResources().getColor(R.color.colorPrimaryDark));
                break;

            case 3:
                menuView.setVisibility(View.VISIBLE);
                break;
        }

    }


    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvTitle, tvCost, tvApplyOn, tvApplyOnCount;
        private ImageView imgMenu;
        private View menuView;
        private LinearLayout llEdit, llDel;

        ViewHolder(@NonNull View v) {
            super(v);
            tvTitle = v.findViewById(R.id.tvTitle);
            tvCost = v.findViewById(R.id.tvCost);
            tvApplyOn = v.findViewById(R.id.tvApplyOn);
            tvApplyOnCount = v.findViewById(R.id.tvApplyOnCount);
            imgMenu = v.findViewById(R.id.imgMenu);
            menuView = v.findViewById(R.id.menuView);
            llEdit = v.findViewById(R.id.llEdit);
            llDel = v.findViewById(R.id.llDel);

            if (CropActivity.fromPropertyId.isEmpty()){
                itemView.setOnClickListener(this);
                imgMenu.setOnClickListener(this);
                llEdit.setOnClickListener(this);
                llDel.setOnClickListener(this);
            }else{
                //Show only list when controller from property
                imgMenu.setVisibility(View.GONE);
            }
        }

        @Override
        public void onClick(@NonNull final View view) {
            switch (view.getId()) {

                case R.id.imgMenu:
                    if (lastPos != -1 && lastPos != getAdapterPosition()) {
                        // list.get(lastPos).isSelected = false;
                        notifyItemChanged(lastPos);
                    }

                    if (getAdapterPosition() != -1) {

                        if (menuView.getVisibility() == View.VISIBLE) {
                            updateMenuUI(menuView, llEdit, llDel, 0);

                            //  list.get(getAdapterPosition()).isSelected = false;
                        } else {
                            updateMenuUI(menuView, llEdit, llDel, 3);
                            //  list.get(getAdapterPosition()).isSelected = true;
                        }

                        lastPos = getAdapterPosition();
                    }
                    break;

                case R.id.llEdit:
                    updateMenuUI(menuView, llEdit, llDel, 1);
                    if (getAdapterPosition() != -1 && clickListener != null)
                        clickListener.onEditClick(getAdapterPosition());
                    break;

                case R.id.llDel:
                    updateMenuUI(menuView, llEdit, llDel, 2);
                    if (getAdapterPosition() != -1 && clickListener != null)
                        clickListener.onDeleteClick(getAdapterPosition());
                    break;

                default:
                    if (!isClick) {
                        isClick = true;
                        updateMenuUI(menuView, llEdit, llDel, 0);
                        if (getAdapterPosition() != -1 && clickListener != null)
                            clickListener.onItemClick(getAdapterPosition());
                    }

                    new Handler().postDelayed(() -> isClick = false, 3000);
                    break;
            }
        }
    }
}

