package com.agrinvestapp.ui.expense.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.ExpenseInfoResponse;

import java.util.List;


/**
 * Created by hemant
 * Date: 15/11/18
 * Time: 4:00 PM
 */

public class ProductExpenseDetailAdapter extends RecyclerView.Adapter<ProductExpenseDetailAdapter.ViewHolder> {

    private List<ExpenseInfoResponse.ExpenseListBean.ProductDetailsBean> list;

    public ProductExpenseDetailAdapter(List<ExpenseInfoResponse.ExpenseListBean.ProductDetailsBean> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ProductExpenseDetailAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_expense, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductExpenseDetailAdapter.ViewHolder holder, final int position) {
        ExpenseInfoResponse.ExpenseListBean.ProductDetailsBean bean = list.get(position);
        holder.tvName.setText(bean.getProductName());
        holder.tvQty.setText(bean.getQuantity());
        StringBuilder priceBuilder = new StringBuilder("");
        priceBuilder.append("$").append(bean.getPrice());
        holder.tvCost.setText(priceBuilder);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder  {

        private TextView tvName, tvQty, tvCost;

        ViewHolder(@NonNull View v) {
            super(v);
            tvName = v.findViewById(R.id.tvName);
            tvQty = v.findViewById(R.id.tvQty);
            tvCost = v.findViewById(R.id.tvCost);
        }


    }
}

