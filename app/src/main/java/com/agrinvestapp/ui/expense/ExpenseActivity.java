package com.agrinvestapp.ui.expense;

import android.os.Bundle;

import com.agrinvestapp.R;
import com.agrinvestapp.ui.base.BaseActivity;
import com.agrinvestapp.ui.expense.fragment.ExpenseFragment;

public class ExpenseActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expense);

        replaceFragment(ExpenseFragment.newInstance(), R.id.expenseFrame);
    }
}
