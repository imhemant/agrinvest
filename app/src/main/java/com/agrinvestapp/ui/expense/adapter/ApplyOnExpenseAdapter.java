package com.agrinvestapp.ui.expense.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.other.IncomeOrExpenseApplyOnBean;

import java.util.List;


/**
 * Created by hemant
 * Date: 15/11/18
 * Time: 4:00 PM
 */

public class ApplyOnExpenseAdapter extends RecyclerView.Adapter<ApplyOnExpenseAdapter.ViewHolder> {

    private List<IncomeOrExpenseApplyOnBean> list;
    private boolean isFromView = false;

    public ApplyOnExpenseAdapter(List<IncomeOrExpenseApplyOnBean> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ApplyOnExpenseAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sp_expense_apply, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ApplyOnExpenseAdapter.ViewHolder holder, final int position) {
        IncomeOrExpenseApplyOnBean bean = list.get(position);
        holder.tvName.setText(bean.getName());

        isFromView = true;
        holder.mCheckBox.setChecked(bean.getSelect());
        isFromView = false;

        holder.mCheckBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (!isFromView) {
                list.get(position).setSelect(isChecked);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvName;
        private CheckBox mCheckBox;

        ViewHolder(@NonNull View v) {
            super(v);
            tvName = v.findViewById(R.id.tvName);
            mCheckBox = v.findViewById(R.id.checkbox);
        }

    }
}

