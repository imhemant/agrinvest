package com.agrinvestapp.ui.base;

public interface ClickListener {
    void onItemClick(int pos);

    void onEditClick(int pos);

    void onDeleteClick(int pos);
}
