package com.agrinvestapp.ui.base.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.agrinvestapp.R;
import com.agrinvestapp.ui.base.BaseDialog;
import com.agrinvestapp.utils.AppConstants;

public class PlanUpgradeDialog extends BaseDialog implements View.OnClickListener {

    private static final String TAG = PlanUpgradeDialog.class.getSimpleName();
    private String msg = "";
    private String mTitle = "";
    private PlanUpgradeCallback callback;

    public static PlanUpgradeDialog newInstance(String msg, String mTitle, PlanUpgradeCallback callback) {

        Bundle args = new Bundle();
        args.putString(AppConstants.MSG, msg);
        args.putString(AppConstants.DIALOG_TITLE, mTitle);
        PlanUpgradeDialog fragment = new PlanUpgradeDialog();
        fragment.setOnClickListener(callback);
        fragment.setArguments(args);
        return fragment;
    }

    private void setOnClickListener(PlanUpgradeCallback callback) {
        this.callback = callback;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            msg = getArguments().getString(AppConstants.MSG);
            mTitle = getArguments().getString(AppConstants.DIALOG_TITLE);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_common, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setText(mTitle);

        TextView tvMsg = view.findViewById(R.id.tvMsg);
        tvMsg.setText(msg);

        view.findViewById(R.id.btnCancel).setOnClickListener(this);

        Button btnOk = view.findViewById(R.id.btnOk);
        btnOk.setText(getString(R.string.upgrade));
        btnOk.setOnClickListener(this);
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    public void onClick(View v) {
        hideKeyboard();
        switch (v.getId()) {
            case R.id.btnCancel:
                dismissDialog(TAG);
                break;

            case R.id.btnOk:
                dismissDialog(TAG);
                callback.onUpgradeClick();
                break;

        }
    }

}
