package com.agrinvestapp.ui.base.dialog;

public interface DeleteCallback {
    void onDataDelete();
}
