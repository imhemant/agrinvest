package com.agrinvestapp.ui.base.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.agrinvestapp.R;
import com.agrinvestapp.ui.base.BaseDialog;
import com.agrinvestapp.utils.AppConstants;

public class DeleteDialog extends BaseDialog implements View.OnClickListener {

    private static final String TAG = DeleteDialog.class.getSimpleName();
    private static final String STATUS = "status";
    private DeleteCallback callback;
    private String msg = "";
    private Boolean isExpenseApply = false;

    public static DeleteDialog newInstance(Boolean isExpenseApply, String msg, DeleteCallback callback) {

        Bundle args = new Bundle();
        args.putString(AppConstants.MSG, msg);
        args.putBoolean(STATUS, isExpenseApply);
        DeleteDialog fragment = new DeleteDialog();
        fragment.setOnDeleteListener(callback);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            msg = getArguments().getString(AppConstants.MSG);
            isExpenseApply = getArguments().getBoolean(STATUS);
        }
    }

    public void setOnDeleteListener(DeleteCallback callback) {
        this.callback = callback;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_delete, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (!msg.isEmpty()) {
            TextView tvMsg = view.findViewById(R.id.tvMsg);
            tvMsg.setText(msg);
        }

        Button btnDel = view.findViewById(R.id.btnDel);
        btnDel.setOnClickListener(this);
        Button btnCancel = view.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(this);

        //if any expense apply on product, property, parcel, animal and crop delete operation not apply
        if (isExpenseApply) {
            btnDel.setVisibility(View.GONE);
            btnCancel.setText(R.string.ok);
        }
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    public void onClick(View v) {
        hideKeyboard();
        switch (v.getId()) {
            case R.id.btnDel:
                dismissDialog(TAG);
                callback.onDataDelete();
                break;

            case R.id.btnCancel:
                dismissDialog(TAG);
                break;

        }
    }

}
