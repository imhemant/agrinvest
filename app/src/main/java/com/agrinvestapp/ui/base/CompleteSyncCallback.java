package com.agrinvestapp.ui.base;

/**
 * Created by hemant
 * Date: 12/10/18.
 */

public interface CompleteSyncCallback {
    void onSuccess(String flag);
}
