package com.agrinvestapp.ui.base.dialog;

/**
 * Created by mindiii on 26/12/18.
 */

public interface PlanUpgradeCallback {
    void onUpgradeClick();
}
