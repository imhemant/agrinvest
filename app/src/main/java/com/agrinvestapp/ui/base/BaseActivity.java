package com.agrinvestapp.ui.base;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.agrinvestapp.Agrinvest;
import com.agrinvestapp.R;
import com.agrinvestapp.data.AppDataManager;
import com.agrinvestapp.data.model.db.Animal;
import com.agrinvestapp.data.model.db.CropTreatment;
import com.agrinvestapp.data.model.db.Parcel;
import com.agrinvestapp.data.model.other.AnimalSyncResult;
import com.agrinvestapp.data.model.other.CalvingSyncResult;
import com.agrinvestapp.data.model.other.CropSyncResult;
import com.agrinvestapp.data.model.other.CropTreatmentSyncResult;
import com.agrinvestapp.data.model.other.ExpenseSyncResult;
import com.agrinvestapp.data.model.other.InseminationSyncResult;
import com.agrinvestapp.data.model.other.MoveToParcelSyncResult;
import com.agrinvestapp.data.model.other.PregnancyTestSyncResult;
import com.agrinvestapp.data.model.other.ProductSyncResult;
import com.agrinvestapp.data.model.other.TreatmentSyncResult;
import com.agrinvestapp.data.model.other.WeightSyncResult;
import com.agrinvestapp.ui.animal.fragment.CheckSyncAnimalCallback;
import com.agrinvestapp.ui.animal.fragment.SyncAnimalCallback;
import com.agrinvestapp.ui.base.dialog.SyncCallback;
import com.agrinvestapp.ui.base.dialog.SyncDialog;
import com.agrinvestapp.ui.crop.fragment.CheckSyncCropCallback;
import com.agrinvestapp.ui.crop.fragment.SyncCropCallback;
import com.agrinvestapp.ui.expense.CheckSyncExpenseCallback;
import com.agrinvestapp.ui.expense.SyncExpenseCallback;
import com.agrinvestapp.ui.main.MainActivity;
import com.agrinvestapp.ui.products.CheckSyncProductCallback;
import com.agrinvestapp.ui.products.SyncProductCallback;
import com.agrinvestapp.ui.property.fragment.CheckSyncParcelCallback;
import com.agrinvestapp.ui.property.fragment.SyncParcelCallback;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.AppLogger;
import com.agrinvestapp.utils.AppUtils;
import com.agrinvestapp.utils.CalenderUtils;
import com.agrinvestapp.utils.CommonUtils;
import com.agrinvestapp.utils.NetworkUtils;
import com.agrinvestapp.volley.VolleyJsonObjectListener;
import com.agrinvestapp.volley.VolleyRequest;
import com.android.volley.VolleyError;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.Task;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class BaseActivity extends AppCompatActivity {

    protected FusedLocationProviderClient mFusedLocationClient;
    protected LocationRequest locationRequest;
    @NonNull
    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(@NonNull LocationResult locationResult) {
            List<Location> locationList = locationResult.getLocations();
            if (locationList.size() > 0) {
                //The last location in the list is the newest
                Location location = locationList.get(locationList.size() - 1);

                setLatLng(location);
            }
        }
    };
    private Activity activity = this;
    private Handler handler = new Handler(Looper.getMainLooper());
    private BaseFragment.FragmentCallback fragmentBackPressListener;
    private Dialog mProgressBar;
    private Boolean doubleBackPress = false;

    private CompleteSyncCallback completeSyncCallback;

    protected Activity getActivity() {
        return activity;
    }

    protected AppDataManager getDataManager() {
        return Agrinvest.getDataManager();
    }

    public void replaceFragment(@NonNull Fragment fragmentHolder, Integer layoutId) {
        try {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            String fragmentName = fragmentHolder.getClass().getName();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            fragmentTransaction.replace(layoutId, fragmentHolder, fragmentName);
            fragmentTransaction.addToBackStack(fragmentName);
            fragmentTransaction.commit();
            hideKeyboard();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addFragment(@NonNull Fragment fragment, int layoutId, boolean addToBackStack) {
        try {
            String fragmentName = fragment.getClass().getName();

            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

            fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setEnterTransition(null);
            }
            fragmentTransaction.add(layoutId, fragment, fragmentName);
            if (addToBackStack) fragmentTransaction.addToBackStack(fragmentName);
            fragmentTransaction.commit();

            hideKeyboard();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Nullable
    public Fragment getCurrentFragment() {
        try {
            FragmentManager fragmentManager = getSupportFragmentManager();
            String fragmentTag = fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1).getName();
            return fragmentManager.findFragmentByTag(fragmentTag);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * location getting task start here
     * when location not available this method on gps when user click ok
     */
    protected void onGpsAutomatic() {
        int permissionLocation = ContextCompat.checkSelfPermission(activity,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {

            locationRequest = new LocationRequest();
            locationRequest.setInterval(3000);
            locationRequest.setFastestInterval(3000);
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);
            builder.setAlwaysShow(true);
            builder.setNeedBle(true);

            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            mFusedLocationClient.requestLocationUpdates(locationRequest, mLocationCallback, Looper.myLooper());

            Task<LocationSettingsResponse> task =
                    LocationServices.getSettingsClient(this).checkLocationSettings(builder.build());

            task.addOnCompleteListener(task1 -> {
                try {
                    //getting target response use below code
                    LocationSettingsResponse response = task1.getResult(ApiException.class);

                    // All location settings are satisfied. The client can initialize location
                    // requests here.
                    int permissionLocation1 = ContextCompat
                            .checkSelfPermission(activity,
                                    Manifest.permission.ACCESS_FINE_LOCATION);
                    if (permissionLocation1 == PackageManager.PERMISSION_GRANTED) {

                        mFusedLocationClient.getLastLocation()
                                .addOnSuccessListener(activity, location -> {
                                    // Got last known location. In some rare situations this can be null.
                                    if (location != null) {
                                        // Logic to handle location object
                                        setLatLng(location);
                                    } else {
                                        //Location not available
                                        AppLogger.e("Test", "Location not available");
                                    }
                                });
                    }
                } catch (ApiException exception) {
                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the
                            // user a dialog.
                            try {
                                // Cast to a resolvable exception.
                                ResolvableApiException resolvable = (ResolvableApiException) exception;
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                resolvable.startResolutionForResult(
                                        activity,
                                        AppConstants.REQUEST_CHECK_SETTINGS_GPS);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            } catch (ClassCastException e) {
                                // Ignore, should be an impossible error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }
    }

    /**
     * this method get location when available and store in static variable
     */
    public void updateLocation() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(activity);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(activity, location -> {
                    // Got last known location. In some rare situations this can be null.
                    if (location != null) {
                        // Logic to handle location object
                        setLatLng(location);
                    } else {
                        //Location not available
                        onGpsAutomatic();
                    }
                });
    }

    protected void setLatLng(@NonNull Location location) {
        Agrinvest.LATITUDE = location.getLatitude();
        Agrinvest.LONGITUDE = location.getLongitude();
        AppLogger.e("Location", String.valueOf(Agrinvest.LATITUDE));

        /*if (address.isEmpty()) {
            address = getAddressFromLatLng(Agrinvest.LATITUDE, Agrinvest.LONGITUDE);
            AppLogger.e("Location ", address);
        }*/
    }

    protected String getAddressFromLatLng(Double latitude, Double longitude) {
        String result;
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(activity, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);

            // String city = addresses.get(0).getLocality();
            //  String addressLine = addresses.get(0).getAddressLine(1);
            // String state = addresses.get(0).getAdminArea();
            // String country = addresses.get(0).getCountryName();

            result = addresses.get(0).getAddressLine(0);// Here 1 represent max location result to returned, by documents it recommended 1 to 5
        } catch (Exception e) {
            result = "";
        }
        return result;
    }
    /* location getting task end here */

    /* Request updates at startup */
    @Override
    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        if (mFusedLocationClient == null)
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocationClient.requestLocationUpdates(locationRequest, mLocationCallback, Looper.myLooper());
    }

    /* Remove the location listener updates when Activity is paused */
    @Override
    protected void onPause() {
        super.onPause();
        if (mFusedLocationClient == null)
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }

    @Override
    public void onBackPressed() {
        hideKeyboard();

        Handler handler = new Handler();
        Runnable runnable;

        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            Fragment fragment = getCurrentFragment();
            if (fragment != null && fragment instanceof BaseFragment.FragmentCallback) {
                fragmentBackPressListener = (BaseFragment.FragmentCallback) fragment;
            }

            if (fragmentBackPressListener != null) {
                fragmentBackPressListener.onBackPress();
            }

            super.onBackPressed();
        } else {
            if (activity instanceof MainActivity) {
                handler.postDelayed(runnable = () -> doubleBackPress = false, 1000);
                if (doubleBackPress) {
                    handler.removeCallbacks(runnable);
                    finish();
                } else {
                    CommonUtils.showToast(this, getString(R.string.alert_exit), Toast.LENGTH_SHORT);
                    doubleBackPress = true;
                }
            } else {
                finish();
            }
        }
    }

    public void updateLanguage(String appLanguage) {

        switch (appLanguage.toLowerCase()) {
            //English
            case "english":
                getDataManager().getUserInfo().setLanguage("english");
                AppUtils.setLanguage(this, "en");
                break;

            //Spanish
            case "spanish":
                getDataManager().getUserInfo().setLanguage("spanish");
                AppUtils.setLanguage(this, "es");
                break;

            //Portuguese
            case "portuguese":
                getDataManager().getUserInfo().setLanguage("portuguese");
                AppUtils.setLanguage(this, "pt");
                break;

            //French
            case "french":
                getDataManager().getUserInfo().setLanguage("french");
                AppUtils.setLanguage(this, "fr");
                break;

            //German
            case "german":
                getDataManager().getUserInfo().setLanguage("german");
                AppUtils.setLanguage(this, "de");
                break;
        }
    }

    protected void hideKeyboard() {
        if (getCurrentFocus() == null) return;

        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        assert inputMethodManager != null;
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    protected void showKeyboard(EditText editText) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    }

    protected void setLoading(Boolean isLoading) {
        //first cancel progress bar
        if (mProgressBar != null && mProgressBar.isShowing()) {
            mProgressBar.cancel();
        }
        // true then show loading
        if (isLoading) {
            mProgressBar = CommonUtils.showLoadingDialog(this, false);
        }
    }

    protected void setSyncLoading() {
        //first cancel progress bar
        if (mProgressBar != null && mProgressBar.isShowing()) {
            mProgressBar.cancel();
        }

        mProgressBar = CommonUtils.showLoadingDialog(this, true);
    }

    protected Boolean isNetworkConnected(Boolean isOfflineManage) {
        if (isOfflineManage) {
            String timer = getDataManager().getSyncTimer();
            String myTime = CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT);
            Long diff = CalenderUtils.getTimerDifference(timer, myTime);
            if (diff == null || diff >= AppConstants.MAX_TIME_FOR_SYNC_DIALOG) {
                getDataManager().setAppOnline(true);
            }

            Boolean flag = getDataManager().getAppOnline();
            return flag && NetworkUtils.isNetworkConnected(getApplicationContext());
        } else return NetworkUtils.isNetworkConnected(getApplicationContext());
    }

    /*Sync Local DB Start here*/
    private void checkParcelTableForSync(CheckSyncParcelCallback callback) {
        new Thread(() -> {
            List<Parcel> parcelSyncList = getDataManager().getLocalParcelListForSync();

            String data = parcelSyncList.isEmpty() ? "" : getDataManager().mGson.toJson(parcelSyncList);

            AppLogger.e("Parcel Sync Data", data);

            handler.post(() -> {
                if (data.isEmpty()) callback.onCallApi();
                else callback.onSync(data);
            });

        }).start();
    }

    private void doSyncLocalParcel(String data, SyncParcelCallback callback) {
        HashMap<String, String> mParams = new HashMap<>();
        mParams.put("parcelData", data);

        getDataManager().doServerSyncParcelApiCall(getDataManager().getHeader(), mParams)
                .getAsJsonObject(new VolleyJsonObjectListener() {

                    @Override
                    public void onVolleyJsonObject(JSONObject jsonObject) {
                        try {
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            if (status.equals("success")) callback.onSuccess();
                            else {
                                setLoading(false);
                                CommonUtils.showToast(activity, message, Toast.LENGTH_SHORT);
                            }

                        } catch (Exception e) {
                            setLoading(false);
                            CommonUtils.showToast(activity, getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyJsonException() {
                        setLoading(false);
                        CommonUtils.showToast(activity, getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(activity, error);
                    }
                });
    }

    private void checkAnimalTableForSync(CheckSyncAnimalCallback callback) {
        new Thread(() -> {
            List<AnimalSyncResult> animalListForSync = getDataManager().getLocalAnimalListForSync();

            String data = animalListForSync.isEmpty() ? "" : getDataManager().mGson.toJson(animalListForSync);

            AppLogger.e("Animal Sync Data", data);

            handler.post(() -> {
                if (data.isEmpty()) callback.onCallApi();
                else callback.onSync(data);
            });

        }).start();
    }

    private void doSyncLocalAnimal(String data, SyncAnimalCallback callback) {
        HashMap<String, String> mParams = new HashMap<>();
        mParams.put("animalData", data);

        getDataManager().doServerSyncAnimalApiCall(getDataManager().getHeader(), mParams)
                .getAsJsonObject(new VolleyJsonObjectListener() {

                    @Override
                    public void onVolleyJsonObject(JSONObject jsonObject) {
                        try {
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            if (status.equals("success")) callback.onSuccess();
                            else {
                                setLoading(false);
                                CommonUtils.showToast(activity, message, Toast.LENGTH_SHORT);
                            }

                        } catch (Exception e) {
                            setLoading(false);
                            CommonUtils.showToast(activity, getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyJsonException() {
                        setLoading(false);
                        CommonUtils.showToast(activity, getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(activity, error);
                    }
                });
    }

    private void checkTreatmentTableForSync(CheckSyncAnimalCallback callback) {
        new Thread(() -> {
           /* List<TreatmentSyncResult> treatmentList = getDataManager().getManageTreatmentList();

            List<TreatmentSyncResult> tempList = new ArrayList<>();
            for (TreatmentSyncResult tempBean : treatmentList) {
                Animal animal = getDataManager().getAnimalUsingAnimalId(tempBean.getTreatmentForId());
                tempBean.setTreatmentRecordKey(tempBean.getRecordKey());  //treatment record key
                tempBean.setTreatmentForId(animal.getAnimalId());
                tempBean.setRecordKey(animal.getRecordKey());  //animal record key
                List<TreatmentSyncResult.TreatmentListDataBean> treatmentMappingList = getDataManager().getManageTreatmentDetailList(tempBean.getTreatmentId());
                tempBean.setTreatmentListData(treatmentMappingList);
                tempList.add(tempBean);
            }
            String data;

            List<TreatmentSyncResult> treatmentAllList = getDataManager().getManageAllTreatmentList();
            for (TreatmentSyncResult tempBean : treatmentAllList) {
                Animal animal = getDataManager().getAnimalUsingAnimalId(tempBean.getTreatmentForId());
                tempBean.setEvent(AppConstants.DB_EVENT_EDIT);
                tempBean.setTreatmentRecordKey(tempBean.getRecordKey());  //treatment record key
                tempBean.setTreatmentForId(animal.getAnimalId());
                tempBean.setRecordKey(animal.getRecordKey());  //animal record key
                List<TreatmentSyncResult.TreatmentListDataBean> treatmentMappingList = getDataManager().getManageTreatmentDetailList(tempBean.getTreatmentId());

                if (!treatmentMappingList.isEmpty()) {
                    tempBean.setTreatmentListData(treatmentMappingList);
                    tempList.add(tempBean);
                }
            }
            data = tempList.isEmpty() ? "" : getDataManager().mGson.toJson(tempList);

            AppLogger.e("Treatment Sync Data", data);

            handler.post(() -> {
                if (data.isEmpty()) callback.onCallApi();
                else callback.onSync(data);
            });*/

            List<TreatmentSyncResult> treatmentList = getDataManager().getManageTreatmentList();

            List<TreatmentSyncResult> tempList = new ArrayList<>();

            for (TreatmentSyncResult tempBean : treatmentList) {
                Animal animal = getDataManager().getAnimalUsingAnimalId(tempBean.getTreatmentForId());
                tempBean.setTreatmentRecordKey(tempBean.getRecordKey());  //treatment record key
                tempBean.setTreatmentForId(animal.getAnimalId());
                tempBean.setRecordKey(animal.getRecordKey());  //animal record key
                List<TreatmentSyncResult.TreatmentListDataBean> treatmentMappingList = getDataManager().getManageTreatmentDetailList(tempBean.getTreatmentId());
                tempBean.setTreatmentListData(treatmentMappingList);
                tempList.add(tempBean);
            }
            String data;

            data = tempList.isEmpty() ? "" : getDataManager().mGson.toJson(tempList);

            AppLogger.e("Treatment Sync Data", data);

            handler.post(() -> {
                if (data.isEmpty()) callback.onCallApi();
                else callback.onSync(data);
            });
        }).start();
    }

    private void doSyncLocalTreatment(String data, SyncAnimalCallback callback) {
        HashMap<String, String> mParams = new HashMap<>();
        mParams.put("treatmentData", data);

        getDataManager().doServerSyncTreatmentApiCall(getDataManager().getHeader(), mParams)
                .getAsJsonObject(new VolleyJsonObjectListener() {

                    @Override
                    public void onVolleyJsonObject(JSONObject jsonObject) {
                        try {
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            if (status.equals("success")) callback.onSuccess();
                            else {
                                setLoading(false);
                                CommonUtils.showToast(activity, message, Toast.LENGTH_SHORT);
                            }

                        } catch (Exception e) {
                            setLoading(false);
                            CommonUtils.showToast(activity, getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyJsonException() {
                        setLoading(false);
                        CommonUtils.showToast(activity, getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(activity, error);
                    }
                });
    }

    private void checkInseminationTableForSync(CheckSyncAnimalCallback callback) {
        new Thread(() -> {
            List<InseminationSyncResult> list = getDataManager().getInseminationList();

            String data = list.isEmpty() ? "" : getDataManager().mGson.toJson(list);

            AppLogger.e("Animal Insemination Sync Data", data);

            handler.post(() -> {
                if (data.isEmpty()) callback.onCallApi();
                else callback.onSync(data);
            });
        }).start();
    }

    private void doSyncLocalInsemination(String data, SyncAnimalCallback callback) {
        HashMap<String, String> mParams = new HashMap<>();
        mParams.put("inseminationData", data);

        getDataManager().doServerSyncInseminationApiCall(getDataManager().getHeader(), mParams)
                .getAsJsonObject(new VolleyJsonObjectListener() {

                    @Override
                    public void onVolleyJsonObject(JSONObject jsonObject) {
                        try {
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            if (status.equals("success")) callback.onSuccess();
                            else {
                                setLoading(false);
                                CommonUtils.showToast(activity, message, Toast.LENGTH_SHORT);
                            }

                        } catch (Exception e) {
                            setLoading(false);
                            CommonUtils.showToast(activity, getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyJsonException() {
                        setLoading(false);
                        CommonUtils.showToast(activity, getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(activity, error);
                    }
                });
    }

    private void checkPregnancyTableForSync(CheckSyncAnimalCallback callback) {
        new Thread(() -> {
            List<PregnancyTestSyncResult> list = getDataManager().getPregnancyTestList();

            String data = list.isEmpty() ? "" : getDataManager().mGson.toJson(list);

            AppLogger.e("Animal Pregnancy Sync Data", data);

            handler.post(() -> {
                if (data.isEmpty()) callback.onCallApi();
                else callback.onSync(data);
            });
        }).start();
    }

    private void doSyncLocalPregnancy(String data, SyncAnimalCallback callback) {
        HashMap<String, String> mParams = new HashMap<>();
        mParams.put("pregnancyTestData", data);

        getDataManager().doServerSyncPregnancyTestApiCall(getDataManager().getHeader(), mParams)
                .getAsJsonObject(new VolleyJsonObjectListener() {

                    @Override
                    public void onVolleyJsonObject(JSONObject jsonObject) {
                        try {
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            if (status.equals("success")) callback.onSuccess();
                            else {
                                setLoading(false);
                                CommonUtils.showToast(activity, message, Toast.LENGTH_SHORT);
                            }

                        } catch (Exception e) {
                            setLoading(false);
                            CommonUtils.showToast(activity, getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyJsonException() {
                        setLoading(false);
                        CommonUtils.showToast(activity, getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(activity, error);
                    }
                });
    }

    private void checkMToParcelTableForSync(CheckSyncAnimalCallback callback) {
        new Thread(() -> {
            List<MoveToParcelSyncResult> list = getDataManager().getMToParcelList();

            String data = list.isEmpty() ? "" : getDataManager().mGson.toJson(list);

            AppLogger.e("Animal MToParcel Sync Data", data);

            handler.post(() -> {
                if (data.isEmpty()) callback.onCallApi();
                else callback.onSync(data);
            });
        }).start();
    }

    private void doSyncLocalMToParcel(String data, SyncAnimalCallback callback) {
        HashMap<String, String> mParams = new HashMap<>();
        mParams.put("movementData", data);

        getDataManager().doServerSyncMToParcelApiCall(getDataManager().getHeader(), mParams)
                .getAsJsonObject(new VolleyJsonObjectListener() {

                    @Override
                    public void onVolleyJsonObject(JSONObject jsonObject) {
                        try {
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            if (status.equals("success")) callback.onSuccess();
                            else {
                                setLoading(false);
                                CommonUtils.showToast(activity, message, Toast.LENGTH_SHORT);
                            }

                        } catch (Exception e) {
                            setLoading(false);
                            CommonUtils.showToast(activity, getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyJsonException() {
                        setLoading(false);
                        CommonUtils.showToast(activity, getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(activity, error);
                    }
                });
    }

    private void checkCalvingTableForSync(CheckSyncAnimalCallback callback) {
        new Thread(() -> {
            List<CalvingSyncResult> list = getDataManager().getCalvingList();

            String data = list.isEmpty() ? "" : getDataManager().mGson.toJson(list);

            AppLogger.e("Animal Calving Sync Data", data);

            handler.post(() -> {
                if (data.isEmpty()) callback.onCallApi();
                else callback.onSync(data);
            });
        }).start();
    }

    private void doSyncLocalCalving(String data, SyncAnimalCallback callback) {
        HashMap<String, String> mParams = new HashMap<>();
        mParams.put("calvingData", data);

        getDataManager().doServerSyncCalvingApiCall(getDataManager().getHeader(), mParams)
                .getAsJsonObject(new VolleyJsonObjectListener() {

                    @Override
                    public void onVolleyJsonObject(JSONObject jsonObject) {
                        try {
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            if (status.equals("success")) callback.onSuccess();
                            else {
                                setLoading(false);
                                CommonUtils.showToast(activity, message, Toast.LENGTH_SHORT);
                            }

                        } catch (Exception e) {
                            setLoading(false);
                            CommonUtils.showToast(activity, getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyJsonException() {
                        setLoading(false);
                        CommonUtils.showToast(activity, getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(activity, error);
                    }
                });
    }

    private void checkWeightTableForSync(CheckSyncAnimalCallback callback) {
        new Thread(() -> {
            List<WeightSyncResult> list = getDataManager().getWeightList();

            String data = list.isEmpty() ? "" : getDataManager().mGson.toJson(list);

            AppLogger.e("Animal Weight Sync Data", data);

            handler.post(() -> {
                if (data.isEmpty()) callback.onCallApi();
                else callback.onSync(data);
            });
        }).start();
    }

    private void doSyncLocalWeight(String data, SyncAnimalCallback callback) {
        HashMap<String, String> mParams = new HashMap<>();
        mParams.put("weightData", data);

        getDataManager().doServerSyncWeightApiCall(getDataManager().getHeader(), mParams)
                .getAsJsonObject(new VolleyJsonObjectListener() {

                    @Override
                    public void onVolleyJsonObject(JSONObject jsonObject) {
                        try {
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            if (status.equals("success")) callback.onSuccess();
                            else {
                                setLoading(false);
                                CommonUtils.showToast(activity, message, Toast.LENGTH_SHORT);
                            }

                        } catch (Exception e) {
                            setLoading(false);
                            CommonUtils.showToast(activity, getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyJsonException() {
                        setLoading(false);
                        CommonUtils.showToast(activity, getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(activity, error);
                    }
                });
    }

    private void checkCropTableForSync(CheckSyncCropCallback callback) {
        new Thread(() -> {
            List<CropSyncResult> list = getDataManager().getCropList(getDataManager().getUserInfo().getUserId());

            String data = list.isEmpty() ? "" : getDataManager().mGson.toJson(list);

            AppLogger.e("Crop Sync Data", data);

            handler.post(() -> {
                if (data.isEmpty()) callback.onCallApi();
                else callback.onSync(data);
            });
        }).start();
    }

    private void doSyncLocalCrop(String data, SyncCropCallback callback) {
        HashMap<String, String> mParams = new HashMap<>();
        mParams.put("cropDetail", data);

        getDataManager().doServerSyncCropApiCall(getDataManager().getHeader(), mParams)
                .getAsJsonObject(new VolleyJsonObjectListener() {

                    @Override
                    public void onVolleyJsonObject(JSONObject jsonObject) {
                        try {
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            if (status.equals("success")) callback.onSuccess();
                            else {
                                setLoading(false);
                                CommonUtils.showToast(activity, message, Toast.LENGTH_SHORT);
                            }

                        } catch (Exception e) {
                            setLoading(false);
                            CommonUtils.showToast(activity, getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyJsonException() {
                        setLoading(false);
                        CommonUtils.showToast(activity, getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(activity, error);
                    }
                });
    }

    private void checkCropTreatmentTableForSync(CheckSyncCropCallback callback) {
        new Thread(() -> {
            List<CropTreatment> treatmentList = getDataManager().getManageCropTreatmentList();

            List<CropTreatmentSyncResult> tempList = new ArrayList<>();

            for (CropTreatment tempBean : treatmentList) {
                CropTreatmentSyncResult cropTreatmentSyncResult = new CropTreatmentSyncResult();

                cropTreatmentSyncResult.setRequestType(tempBean.getEvent());
                cropTreatmentSyncResult.setRecordKey(getDataManager().getCropRK(tempBean.getCrop_id()));  //animal record key
                cropTreatmentSyncResult.setTreatmentName(tempBean.getTreatmentName());
                cropTreatmentSyncResult.setTreatmentRecordKey(tempBean.getRecordKey());  //treatment record key

                List<CropTreatmentSyncResult.TreatmentListDataBean> treatmentMappingList = getDataManager().getManageCropTreatmentDetailList(tempBean.getTreatmentId());
                cropTreatmentSyncResult.setTreatmentListData(treatmentMappingList);
                tempList.add(cropTreatmentSyncResult);
            }
            String data;

            data = tempList.isEmpty() ? "" : getDataManager().mGson.toJson(tempList);

            AppLogger.e("Crop Treatment Sync Data", data);

            handler.post(() -> {
                if (data.isEmpty()) callback.onCallApi();
                else callback.onSync(data);
            });

        }).start();
    }

    private void doSyncLocalCropTreatment(String data, SyncCropCallback callback) {
        HashMap<String, String> mParams = new HashMap<>();
        mParams.put("treatmentData", data);

        getDataManager().doServerSyncCropTreatmentApiCall(getDataManager().getHeader(), mParams)
                .getAsJsonObject(new VolleyJsonObjectListener() {

                    @Override
                    public void onVolleyJsonObject(JSONObject jsonObject) {
                        try {
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            if (status.equals("success")) callback.onSuccess();
                            else {
                                setLoading(false);
                                CommonUtils.showToast(activity, message, Toast.LENGTH_SHORT);
                            }

                        } catch (Exception e) {
                            setLoading(false);
                            CommonUtils.showToast(activity, getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyJsonException() {
                        setLoading(false);
                        CommonUtils.showToast(activity, getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(activity, error);
                    }
                });
    }

    private void checkProductTableForSync(CheckSyncProductCallback callback) {
        new Thread(() -> {
            List<ProductSyncResult> manageProductList = getDataManager().getManageProductList();


            String data = manageProductList.isEmpty() ? "" : getDataManager().mGson.toJson(manageProductList);

            AppLogger.e("Product Sync Data", data);

            handler.post(() -> {
                if (data.isEmpty()) callback.onCallApi();
                else callback.onSync(data);
            });

        }).start();
    }

    private void doSyncLocalProduct(String data, SyncProductCallback callback) {
        HashMap<String, String> mParams = new HashMap<>();
        mParams.put("productData", data);

        getDataManager().doServerSyncProductApiCall(getDataManager().getHeader(), mParams)
                .getAsJsonObject(new VolleyJsonObjectListener() {

                    @Override
                    public void onVolleyJsonObject(JSONObject jsonObject) {
                        try {
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            if (status.equals("success")) callback.onSuccess();
                            else {
                                setLoading(false);
                                CommonUtils.showToast(activity, message, Toast.LENGTH_SHORT);
                            }

                        } catch (Exception e) {
                            setLoading(false);
                            CommonUtils.showToast(activity, getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyJsonException() {
                        setLoading(false);
                        CommonUtils.showToast(activity, getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(activity, error);
                    }
                });
    }

    private void checkExpenseTableForSync(CheckSyncExpenseCallback callback) {
        new Thread(() -> {
            List<ExpenseSyncResult> expenseList = getDataManager().getManageExpenseList();

            for (ExpenseSyncResult mainBean : expenseList) {
                List<ExpenseSyncResult.AppliedOnDetail> appliedOnList = new ArrayList<>();

                switch (mainBean.getAppliedOn() == null ? "" : mainBean.getAppliedOn()) {
                    case "animal":
                        appliedOnList = getDataManager().getManageAnimalExpenseAppliedOnList(mainBean.getExpenseId());
                        break;

                    case "crop":
                        appliedOnList = getDataManager().getManageCropExpenseAppliedOnList(mainBean.getExpenseId());
                        break;

                    case "lot":
                        if (mainBean.getExpenseFor().equals("animal"))
                            appliedOnList = getDataManager().getManageAnimalLotExpenseAppliedOnList(mainBean.getExpenseId());
                        else
                            appliedOnList = getDataManager().getManageCropLotExpenseAppliedOnList(mainBean.getExpenseId());
                        break;

                    case "parcel":
                        appliedOnList = getDataManager().getManageParcelExpenseAppliedOnList(mainBean.getExpenseId());
                        break;

                    case "property":
                        appliedOnList = getDataManager().getManagePropertyExpenseAppliedOnList(mainBean.getExpenseId());
                        break;
                }

                List<ExpenseSyncResult.ProductDetail> productList = getDataManager().getManageExpenseProductList(mainBean.getExpenseId());
                mainBean.setAppliedOnDetail(appliedOnList);
                mainBean.setProductDetail(productList);
            }

            String data = expenseList.isEmpty() ? "" : getDataManager().mGson.toJson(expenseList);

            AppLogger.e("Expense Sync Data", data);

            handler.post(() -> {
                if (data.isEmpty()) callback.onCallApi();
                else callback.onSync(data);
            });

        }).start();
    }

    private void doSyncLocalExpense(String data, SyncExpenseCallback callback) {
        HashMap<String, String> mParams = new HashMap<>();
        mParams.put("expenseData", data);

        getDataManager().doServerSyncExpenseApiCall(getDataManager().getHeader(), mParams)
                .getAsJsonObject(new VolleyJsonObjectListener() {

                    @Override
                    public void onVolleyJsonObject(JSONObject jsonObject) {
                        try {
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            if (status.equals("success")) callback.onSuccess();
                            else {
                                setLoading(false);
                                CommonUtils.showToast(activity, message, Toast.LENGTH_SHORT);
                            }

                        } catch (Exception e) {
                            setLoading(false);
                            CommonUtils.showToast(activity, getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyJsonException() {
                        setLoading(false);
                        CommonUtils.showToast(activity, getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(activity, error);
                    }
                });
    }

    public void doSyncAppDB(Boolean isSync, CompleteSyncCallback completeSyncCallback) {
        this.completeSyncCallback = completeSyncCallback;

        if (isSync) {
            setSyncLoading();
            getDataManager().setAppOnline(true);
            getDataManager().setSyncTimer(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
            syncParcelStep1();
        } else {
            if (getDataManager().getLocalDataExistForSync()) {
                String timer = getDataManager().getSyncTimer();
                String myTime = CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT);
                Long diff = CalenderUtils.getTimerDifference(timer, myTime);

                if (diff == null || diff >= AppConstants.MAX_TIME_FOR_SYNC_DIALOG) {
                    SyncDialog.newInstance(new SyncCallback() {
                        @Override
                        public void onSyncClick() {
                            if (getActivity() instanceof MainActivity) {
                                setSyncLoading();
                                getDataManager().setAppOnline(true);
                                getDataManager().setSyncTimer(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                                syncParcelStep1();
                            } else {
                                Intent intent = new Intent(activity, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.putExtra(AppConstants.FROM_SYNC_DIALOG, true);
                                startActivity(intent);
                                finish();
                            }
                        }

                        @Override
                        public void onCancelClick() {
                            getDataManager().setAppOnline(false);
                            getDataManager().setSyncTimer(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                            if (completeSyncCallback != null)
                                completeSyncCallback.onSuccess(AppConstants.OFFLINE);
                        }
                    }).show(getActivity());
                } else if (completeSyncCallback != null)
                    completeSyncCallback.onSuccess(AppConstants.OFFLINE);
            } else if (completeSyncCallback != null)
                completeSyncCallback.onSuccess(AppConstants.ONLINE);
        }
    }

    public void doSyncAppDB(CompleteSyncCallback completeSyncCallback) {
        this.completeSyncCallback = completeSyncCallback;

        if (getDataManager().getLocalDataExistForSync()) {
            SyncDialog.newInstance(new SyncCallback() {
                @Override
                public void onSyncClick() {
                    setSyncLoading();
                    getDataManager().setAppOnline(true);
                    getDataManager().setSyncTimer(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    syncParcelStep1();
                }

                @Override
                public void onCancelClick() {
                    if (completeSyncCallback != null)
                        completeSyncCallback.onSuccess(AppConstants.ONLINE);
                }
            }).show(getActivity());
        } else if (completeSyncCallback != null)
            completeSyncCallback.onSuccess(AppConstants.ONLINE);
    }

    private void syncParcelStep1() {
        checkParcelTableForSync(new CheckSyncParcelCallback() {
            @Override
            public void onSync(String data) {
                //sync here
                doSyncLocalParcel(data, () -> syncAnimalStep2());
            }

            @Override
            public void onCallApi() {
                //call next api
                syncAnimalStep2();
            }
        });
    }

    private void syncAnimalStep2() {
        checkAnimalTableForSync(new CheckSyncAnimalCallback() {
            @Override
            public void onSync(String data) {
                //sync here
                doSyncLocalAnimal(data, () -> syncTreatmentStep3());
            }

            @Override
            public void onCallApi() {
                syncTreatmentStep3();
            }
        });
    }

    private void syncTreatmentStep3() {
        checkTreatmentTableForSync(new CheckSyncAnimalCallback() {
            @Override
            public void onSync(String data) {
//sync here
                doSyncLocalTreatment(data, () -> syncInseminationStep4());
            }

            @Override
            public void onCallApi() {
                syncInseminationStep4();
            }
        });
    }

    private void syncInseminationStep4() {
        checkInseminationTableForSync(new CheckSyncAnimalCallback() {
            @Override
            public void onSync(String data) {
                //sync here
                doSyncLocalInsemination(data, () -> syncPregnancyTestStep5());
            }

            @Override
            public void onCallApi() {
                syncPregnancyTestStep5();
            }
        });
    }

    private void syncPregnancyTestStep5() {
        checkPregnancyTableForSync(new CheckSyncAnimalCallback() {
            @Override
            public void onSync(String data) {
                //sync here
                doSyncLocalPregnancy(data, () -> syncMoveToParcelStep6());
            }

            @Override
            public void onCallApi() {
                syncMoveToParcelStep6();
            }
        });
    }

    private void syncMoveToParcelStep6() {
        checkMToParcelTableForSync(new CheckSyncAnimalCallback() {
            @Override
            public void onSync(String data) {
                //sync here
                doSyncLocalMToParcel(data, () -> syncCalvingStep7());
            }

            @Override
            public void onCallApi() {
                syncCalvingStep7();
            }
        });
    }

    private void syncCalvingStep7() {
        checkCalvingTableForSync(new CheckSyncAnimalCallback() {
            @Override
            public void onSync(String data) {
                //sync here
                doSyncLocalCalving(data, () -> syncWeightStep8());
            }

            @Override
            public void onCallApi() {
                syncWeightStep8();
            }
        });
    }

    private void syncWeightStep8() {
        checkWeightTableForSync(new CheckSyncAnimalCallback() {
            @Override
            public void onSync(String data) {
                //sync here
                doSyncLocalWeight(data, () -> syncCropStep9());
            }

            @Override
            public void onCallApi() {
                syncCropStep9();
            }
        });
    }

    private void syncCropStep9() {
        checkCropTableForSync(new CheckSyncCropCallback() {
            @Override
            public void onSync(String data) {
//sync here
                doSyncLocalCrop(data, () -> syncCropTreatmentStep10());
            }

            @Override
            public void onCallApi() {
                syncCropTreatmentStep10();
            }
        });
    }

    private void syncCropTreatmentStep10() {
        checkCropTreatmentTableForSync(new CheckSyncCropCallback() {
            @Override
            public void onSync(String data) {
//sync here
                doSyncLocalCropTreatment(data, () -> syncProductStep11());
            }

            @Override
            public void onCallApi() {
                syncProductStep11();
            }
        });
    }

    private void syncProductStep11() {
        checkProductTableForSync(new CheckSyncProductCallback() {
            @Override
            public void onSync(String data) {
//sync here
                doSyncLocalProduct(data, () -> syncProductStep12());
            }

            @Override
            public void onCallApi() {
                syncProductStep12();
            }
        });
    }

    private void syncProductStep12() {
        checkExpenseTableForSync(new CheckSyncExpenseCallback() {
            @Override
            public void onSync(String data) {
//sync here
                doSyncLocalExpense(data, () -> {
                    updateAllTableSyncStatus();
                    // setLoading(false);

                    if (completeSyncCallback != null)
                        completeSyncCallback.onSuccess(AppConstants.ONLINE);
                    AppLogger.d("database", "All data sync success");
                });
            }

            @Override
            public void onCallApi() {
                updateAllTableSyncStatus();
                // setLoading(false);
                if (completeSyncCallback != null)
                    completeSyncCallback.onSuccess(AppConstants.ONLINE);
                AppLogger.d("database", "All data sync success");
            }
        });
    }

    private void updateAllTableSyncStatus() {
        new Thread(() -> {
            getDataManager().parcelSyncUpdate();
            getDataManager().animalSyncUpdate();
            getDataManager().treatmentSyncUpdate();
            getDataManager().treatmentMappingSyncUpdate();
            getDataManager().inseminationSyncUpdate();
            getDataManager().pregnancyTestSyncUpdate();
            getDataManager().parcelMoveSyncUpdate();
            getDataManager().calvingSyncUpdate();
            getDataManager().weightSyncUpdate();
            getDataManager().cropSyncUpdate();
            getDataManager().cropTreatmentSyncUpdate();
            getDataManager().cropTreatmentMappingSyncUpdate();
            getDataManager().expenseSyncUpdate();
            getDataManager().expenseMappingSyncUpdate();
            getDataManager().expenseProductMappingSyncUpdate();

            getDataManager().setLocalDataExistForSync(false);
        }).start();
    }

    public void test() {
        checkTreatmentTableForSync(new CheckSyncAnimalCallback() {
            @Override
            public void onSync(String data) {

            }

            @Override
            public void onCallApi() {

            }
        });
    }
    /*Sync Local DB End here*/

}
