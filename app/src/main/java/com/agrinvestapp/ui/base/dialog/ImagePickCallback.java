package com.agrinvestapp.ui.base.dialog;

/**
 * Created by hemant.
 * Date: 30/8/18
 * Time: 6:17 PM
 */

public interface ImagePickCallback {

    void onCameraClick(ImagePickDialog imagePickDialog);

    void onGalleryClick(ImagePickDialog imagePickDialog);

}
