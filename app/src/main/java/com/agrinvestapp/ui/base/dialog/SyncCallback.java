package com.agrinvestapp.ui.base.dialog;

/**
 * Created by sharma on 28/11/18.
 */

public interface SyncCallback {
    void onSyncClick();

    void onCancelClick();
}
