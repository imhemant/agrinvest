package com.agrinvestapp.ui.base.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.agrinvestapp.R;
import com.agrinvestapp.ui.base.BaseDialog;


/**
 * Created by hemant
 * Date: 25/5/18
 * Time: 1:10 PM
 */


public class ImagePickDialog extends BaseDialog implements View.OnClickListener {

    private static final String TAG = ImagePickDialog.class.getSimpleName();

    private ImagePickCallback callback;

    public ImagePickDialog(){
        setIsBottomTrue();
    }

    public static ImagePickDialog newInstance(ImagePickCallback callback) {
        ImagePickDialog fragment = new ImagePickDialog();
        fragment.setCallback(callback);
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    private void setCallback(ImagePickCallback callback) {
        this.callback = callback;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_image_picker, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        view.findViewById(R.id.tvCancel).setOnClickListener(this);
        view.findViewById(R.id.tvGallery).setOnClickListener(this);
        view.findViewById(R.id.tvCamera).setOnClickListener(this);
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvCancel:
                dismiss();
                break;

            case R.id.tvGallery:
                callback.onGalleryClick(this);
                break;

            case R.id.tvCamera:
                callback.onCameraClick(this);
                break;
        }
    }
}

