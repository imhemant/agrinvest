package com.agrinvestapp.ui.base.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;

import com.agrinvestapp.Agrinvest;
import com.agrinvestapp.R;
import com.agrinvestapp.ui.main.MainActivity;

public class SyncDialog implements View.OnClickListener {

    private SyncCallback callback;
    private Dialog dialog;

    public static SyncDialog newInstance(SyncCallback callback) {
        SyncDialog fragment = new SyncDialog();
        fragment.setOnDeleteListener(callback);
        return fragment;
    }

    private void setOnDeleteListener(SyncCallback callback) {
        this.callback = callback;
    }

    public void show(Activity activity) {
        if (activity instanceof MainActivity) {
            MainActivity mainActivity = (MainActivity) activity;
            mainActivity.updateLanguage(Agrinvest.getDataManager().getUserInfo().getLanguage());
        }

        final RelativeLayout root = new RelativeLayout(activity);

        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));

        // creating the fullscreen dialog
        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setLayout(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        dialog.setCanceledOnTouchOutside(false);

        //tell the Dialog to use the dialog.xml as it's layout description
        dialog.setContentView(R.layout.dialog_sync);

        dialog.findViewById(R.id.btnSync).setOnClickListener(this);
        dialog.findViewById(R.id.btnCancel).setOnClickListener(this);

        dialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSync:
                dialog.dismiss();
                callback.onSyncClick();
                break;

            case R.id.btnCancel:
                dialog.dismiss();
                callback.onCancelClick();
                break;

        }
    }

}
