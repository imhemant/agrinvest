package com.agrinvestapp.ui.products;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

import com.agrinvestapp.R;
import com.agrinvestapp.ui.base.BaseActivity;
import com.agrinvestapp.ui.products.fragment.ProductsFragment;
import com.agrinvestapp.utils.AppConstants;

public class ProductActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        replaceFragment(ProductsFragment.newInstance(), R.id.productFrame);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case AppConstants.REQUEST_MULTIPLE_CAMERA_PERMISSIONS:
                Fragment fragment = getCurrentFragment();
                assert fragment != null;
                fragment.onRequestPermissionsResult(requestCode,permissions,grantResults);
                break;
        }
    }
}
