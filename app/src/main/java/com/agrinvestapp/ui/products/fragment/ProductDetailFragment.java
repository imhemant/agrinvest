package com.agrinvestapp.ui.products.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.ProductInfoResponse;
import com.agrinvestapp.ui.base.BaseFragment;
import com.squareup.picasso.Picasso;

public class ProductDetailFragment extends BaseFragment implements View.OnClickListener {

    private static final String PROD_MODEL = "prodModel";
    private ProductInfoResponse.ProductListBean productListBean;

    public static ProductDetailFragment newInstance(ProductInfoResponse.ProductListBean productListBean) {

        Bundle args = new Bundle();
        args.putParcelable(PROD_MODEL, productListBean);
        ProductDetailFragment fragment = new ProductDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            productListBean = getArguments().getParcelable(PROD_MODEL);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_product_detail, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
          /* toolbar view start */
        ImageView imgBack = view.findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);
        imgBack.setVisibility(View.VISIBLE);

        TextView tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.product_details);

        /* toolbar view end */

        view.findViewById(R.id.llMain).setOnClickListener(this);   //avoid bg click

        ImageView imgProduct = view.findViewById(R.id.imgProduct);
        imgProduct.setOnClickListener(this);
        TextView tvProductName = view.findViewById(R.id.tvProductName);
        TextView tvProdPriceOrUnit = view.findViewById(R.id.tvProdPriceOrUnit);
        TextView tvQty = view.findViewById(R.id.tvQty);
        TextView tvDescription = view.findViewById(R.id.tvDescription);

        if (productListBean != null) {
            if (!productListBean.getProductImage().isEmpty())
                Picasso.get().load(productListBean.getProductImage()).placeholder(R.drawable.ic_picture).into(imgProduct);

            tvProductName.setText(productListBean.getProductName());
            StringBuilder builder = new StringBuilder();
            builder.append("$").append(productListBean.getPrice()).append(" / ").append(getUnit(productListBean.getUnit().toLowerCase()));
            tvProdPriceOrUnit.setText(builder);
            tvQty.setText(productListBean.getQuantity());
            tvDescription.setText(productListBean.getDescription());
        }
    }

    private String getUnit(String unit) {
        switch (unit) {
            case "kg":
                unit = getString(R.string.kg);
                break;

            case "litre":
                unit = getString(R.string.litre);
                break;

            case "gram":
                unit = getString(R.string.gram);
                break;

            case "bag":
                unit = getString(R.string.bag);
                break;

            case "packet":
                unit = getString(R.string.packet);
                break;
        }

        return unit;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                getBaseActivity().onBackPressed();
                break;

            case R.id.imgProduct:
                getBaseActivity().addFragment(FullImageFragment.newInstance(productListBean.getProductImage()), R.id.productFrame, true);
                break;
        }
    }
}
