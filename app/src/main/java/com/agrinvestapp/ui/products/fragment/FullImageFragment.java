package com.agrinvestapp.ui.products.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.agrinvestapp.R;
import com.agrinvestapp.ui.base.BaseFragment;
import com.squareup.picasso.Picasso;


public class FullImageFragment extends BaseFragment implements View.OnClickListener {

    private String productImage="";

    public static FullImageFragment newInstance(String productImage) {

        Bundle args = new Bundle();
        args.putString("Image", productImage);
        FullImageFragment fragment = new FullImageFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments()!=null){
            productImage = getArguments().getString("Image");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_full_image, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
         /* toolbar view start */
        ImageView imgBack = view.findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);
        imgBack.setVisibility(View.VISIBLE);

        TextView tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.product_img);
        /* toolbar view end */

        view.findViewById(R.id.llMain).setOnClickListener(this);
        ImageView imgZoomView = view.findViewById(R.id.imgZoomView);

        if (!productImage.isEmpty()) Picasso.get().load(productImage).placeholder(R.drawable.ic_picture).into(imgZoomView);
        else Picasso.get().load(R.drawable.ic_picture).into(imgZoomView);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                getBaseActivity().onBackPressed();
                break;
        }
    }
}
