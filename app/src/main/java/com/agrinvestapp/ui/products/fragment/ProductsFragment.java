package com.agrinvestapp.ui.products.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.ProductInfoResponse;
import com.agrinvestapp.data.model.db.Product;
import com.agrinvestapp.ui.base.BaseFragment;
import com.agrinvestapp.ui.base.dialog.DeleteDialog;
import com.agrinvestapp.ui.base.dialog.PlanUpgradeDialog;
import com.agrinvestapp.ui.main.fragment.profile.PlanActivity;
import com.agrinvestapp.ui.products.adapter.ProductsAdapter;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.AppLogger;
import com.agrinvestapp.utils.AppUtils;
import com.agrinvestapp.utils.CommonUtils;
import com.agrinvestapp.volley.VolleyRequest;
import com.agrinvestapp.volley.VolleyResponseListener;
import com.android.volley.VolleyError;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdView;
import com.jaygoo.widget.OnRangeChangedListener;
import com.jaygoo.widget.RangeSeekBar;

import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class ProductsFragment extends BaseFragment implements View.OnClickListener {

    public static String productType;
    private TextView tvNoRecord, tvAllProduct, tvMyProduct, tvFilterCount;
    private EditText etProdName, etQty;
    private RangeSeekBar rangeSeekBar;
    private ImageView imgFilterCorner;
    private RelativeLayout rlFilter;
    private FrameLayout productCVFrame, frameAdd, frameCount;
    private ProductsAdapter productsAdapter;
    private List<ProductInfoResponse.ProductListBean> list;
    private NumberFormat numberFormat;
    private String tempMin, tempMax;
    private Boolean isRangeChange = false;

    private Boolean isTypeBusiness = false;

    private Handler handler = new Handler(Looper.getMainLooper());

    public static ProductsFragment newInstance() {
        productType = "allProduct";
        Bundle args = new Bundle();

        ProductsFragment fragment = new ProductsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        isTypeBusiness = getDataManager().getUserInfo().getUserType().equals(AppConstants.TYPE_BUSINESS);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_products, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        /* toolbar view start */
        ImageView imgBack = view.findViewById(R.id.imgBack);
        imgBack.setVisibility(View.VISIBLE);
        TextView tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.products);

        imgFilterCorner = view.findViewById(R.id.imgFilterCorner);
        productCVFrame = view.findViewById(R.id.productCVFrame);
        rlFilter = view.findViewById(R.id.rlFilter);

        frameAdd = view.findViewById(R.id.frameAddT);


        //filter view
        etProdName = view.findViewById(R.id.etProdName);
        etQty = view.findViewById(R.id.etQty);
        tvFilterCount = view.findViewById(R.id.tvFilterCount);
        frameCount = view.findViewById(R.id.frameCount);
        rangeSeekBar = view.findViewById(R.id.seekbar);
        rangeSeekBar.setValue(0,10000);
        numberFormat = NumberFormat.getNumberInstance(Locale.US);
        rangeSeekBar.getLeftSeekBar().setIndicatorText("$"+numberFormat.format(0));
        rangeSeekBar.getRightSeekBar().setIndicatorText("$"+numberFormat.format(10000));

        updateToolbar();
        /* toolbar view end */
        //avoid bg click
        tvAllProduct = view.findViewById(R.id.tvAllProduct);

        tvMyProduct = view.findViewById(R.id.tvMyProduct);

        onClickListener(imgBack, rlFilter, frameAdd, view.findViewById(R.id.btnApply), view.findViewById(R.id.btnReset),
                view.findViewById(R.id.rlMain), tvAllProduct, tvMyProduct);

        RecyclerView rvProducts = view.findViewById(R.id.rvProducts);
        list = new ArrayList<>();

        productsAdapter = new ProductsAdapter(list, new ProductsAdapter.ItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                getBaseActivity().addFragment(ProductDetailFragment.newInstance(list.get(pos)), R.id.productFrame, true);
            }

            @Override
            public void onEditClick(int pos) {
                getBaseActivity().addFragment(AddEditProductFragment.newInstance(ProductsFragment.this, list.get(pos)), R.id.productFrame, true);
            }

            @Override
            public void onDeleteClick(int pos) {
                new Thread(() -> {
                    String prodExpenseStatus = getDataManager().getProductExpenseApplyStatus(list.get(pos).getProductId(), list.get(pos).getRecordKey());
                    String msg = (prodExpenseStatus == null) ? "" : getString(R.string.deleteExpenseMsg);

                    handler.post(() -> DeleteDialog.newInstance(prodExpenseStatus != null, msg, () -> {

                        if (isNetworkConnected(true)) {
                            getBaseActivity().doSyncAppDB(false, flag -> {
                                switch (flag) {
                                    case AppConstants.ONLINE:
                                        deleteProduct(pos);
                                        break;

                                    case AppConstants.OFFLINE:
                                        doDeleteProductLocalDB(AppConstants.OFFLINE, pos, list.get(pos));
                                        break;
                                }
                            });
                        } else {
                            doDeleteProductLocalDB(AppConstants.OFFLINE, pos, list.get(pos));
                        }
                    }).show(getChildFragmentManager()));
                }).start();
            }
        });
        rvProducts.setAdapter(productsAdapter);

        tvNoRecord = view.findViewById(R.id.tvNoRecord);

        rangeSeekBar.setOnRangeChangedListener(new OnRangeChangedListener() {
            @Override
            public void onRangeChanged(RangeSeekBar view, float leftValue, float rightValue, boolean isFromUser) {
                isRangeChange = true;
                int left =Math.round(leftValue);
                int right = Math.round(rightValue);
                tempMin = String.valueOf(left);
                tempMax = String.valueOf(right);
                view.getLeftSeekBar().setIndicatorText("$"+numberFormat.format(left));
                view.getRightSeekBar().setIndicatorText("$"+numberFormat.format(right));
            }

            @Override
            public void onStartTrackingTouch(RangeSeekBar view, boolean isLeft) {
                //do what you want!!
            }

            @Override
            public void onStopTrackingTouch(RangeSeekBar view, boolean isLeft) {
                //do what you want!!
            }
        });
    }

    private void onClickListener(View... views){
        for (View view: views){
            view.setOnClickListener(this);
        }
    }

    private void updateToolbar() {
        frameAdd.setVisibility(productType.equalsIgnoreCase("AllProduct") ? View.GONE : View.VISIBLE);
        rlFilter.setVisibility(View.VISIBLE);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hitApi(productType, true);

        initAd(view);
    }

    public void hitApi(String productType, Boolean isUpdateDB) {
        if (isNetworkConnected(true)) {
            getBaseActivity().doSyncAppDB(false, flag -> {
                switch (flag) {
                    case AppConstants.ONLINE:
                        getProductList(productType, isUpdateDB);
                        break;

                    case AppConstants.OFFLINE:
                        if (isTypeBusiness) {
                            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_internet), Toast.LENGTH_SHORT);
                        } else {
                            getProductListFromDB(productType);
                        }
                        break;
                }
            });
        } else {
            if (isTypeBusiness) {
                CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_internet), Toast.LENGTH_SHORT);
            } else {
                getProductListFromDB(productType);
            }
        }
    }

    private void initAd(View view) {
        AdView mAdView = view.findViewById(R.id.adView);
        mAdView.setVisibility(View.VISIBLE);
        AppUtils.setAdBanner(mAdView);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                mAdView.setVisibility(View.GONE);
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when when the user is about to return
                // to the app after tapping on an ad.
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                getBaseActivity().onBackPressed();
                break;

            case R.id.rlFilter:
                if (imgFilterCorner.getVisibility() == View.VISIBLE) {
                    updateFilterView(0);
                } else {
                    updateFilterView(1);
                }
                break;

            case R.id.frameAddT:
                //free user can not add own product
                if (isTypeBusiness) {
                    if (!getDataManager().getUserInfo().getSubscriptionPlan().equalsIgnoreCase(AppConstants.PLAN_FREE))
                        getBaseActivity().addFragment(AddEditProductFragment.newInstance(ProductsFragment.this, null), R.id.productFrame, true);
                    else {
                        PlanUpgradeDialog.newInstance(getString(R.string.alert_free_product), getString(R.string.upgrade_to_business_plans), () -> startActivity(new Intent(getBaseActivity(), PlanActivity.class))).show(getChildFragmentManager());
                    }
                } else {
                    if (!getDataManager().getUserInfo().getSubscriptionPlan().equalsIgnoreCase(AppConstants.PLAN_FREE))
                        getBaseActivity().addFragment(AddEditProductFragment.newInstance(ProductsFragment.this, null), R.id.productFrame, true);
                    else {
                        PlanUpgradeDialog.newInstance(getString(R.string.alert_free_product), getString(R.string.upgrade_to_premium_plans), () -> startActivity(new Intent(getBaseActivity(), PlanActivity.class))).show(getChildFragmentManager());
                    }
                }

                break;

            case R.id.tvAllProduct:
                if (!productType.equalsIgnoreCase("allProduct")){
                    updateFilterCount(0,list.size());
                    productType = "allProduct";
                    updateToolbar();
                    tvAllProduct.setTextColor(getResources().getColor(R.color.colorWhite));
                    tvMyProduct.setTextColor(getResources().getColor(R.color.grey));
                    tvAllProduct.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                    tvMyProduct.setBackgroundColor(getResources().getColor(R.color.colorWhite));

                    if (isNetworkConnected(true)) {
                        getBaseActivity().doSyncAppDB(false, flag -> {
                            switch (flag) {
                                case AppConstants.ONLINE:
                                    getProductList(productType, false);
                                    break;

                                case AppConstants.OFFLINE:
                                    if (isTypeBusiness) {
                                        CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_internet), Toast.LENGTH_SHORT);
                                    } else {
                                        getProductListFromDB(productType);
                                    }
                                    break;
                            }
                        });
                    } else {
                        if (isTypeBusiness) {
                            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_internet), Toast.LENGTH_SHORT);
                        } else {
                            getProductListFromDB(productType);
                        }
                    }
                }
                break;

            case R.id.tvMyProduct:
                if (!productType.equalsIgnoreCase("myProduct")) {
                    updateFilterCount(0,list.size());
                    productType = "myProduct";
                    updateToolbar();
                    tvMyProduct.setTextColor(getResources().getColor(R.color.colorWhite));
                    tvAllProduct.setTextColor(getResources().getColor(R.color.grey));
                    tvMyProduct.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                    tvAllProduct.setBackgroundColor(getResources().getColor(R.color.colorWhite));

                    if (isNetworkConnected(true)) {
                        getBaseActivity().doSyncAppDB(false, flag -> {
                            switch (flag) {
                                case AppConstants.ONLINE:
                                    getProductList(productType, false);
                                    break;

                                case AppConstants.OFFLINE:
                                    if (isTypeBusiness) {
                                        CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_internet), Toast.LENGTH_SHORT);
                                    } else {
                                        getProductListFromDB(productType);
                                    }
                                    break;
                            }
                        });
                    } else {
                        if (isTypeBusiness) {
                            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_internet), Toast.LENGTH_SHORT);
                        } else {
                            getProductListFromDB(productType);
                        }
                    }
                }
                break;

            case R.id.btnApply:
                String tempProdName = etProdName.getText().toString().trim();
                String tempQty = etQty.getText().toString().trim();
                if (!tempProdName.isEmpty() || !tempQty.isEmpty() || isRangeChange)getFilterResult(tempProdName, tempQty, tempMin, tempMax);
                else {
                    rlFilter.callOnClick();
                    updateFilterCount(0,list.size());
                }
                break;

            case R.id.btnReset:
                rlFilter.callOnClick();
                updateFilterCount(0,list.size());
                getProductList(productType, false);
                break;
        }
    }

    //flag 0 -> All Product and 1 -> My Product
    private void updateFilterView(int flag) {
        switch (flag) {
            case 0:
                imgFilterCorner.setVisibility(View.GONE);
                productCVFrame.getChildAt(0).setVisibility(View.GONE);
                productCVFrame.getChildAt(1).setVisibility(View.VISIBLE);
                break;

            case 1:
                imgFilterCorner.setVisibility(View.VISIBLE);
                productCVFrame.getChildAt(1).setVisibility(View.GONE);
                productCVFrame.getChildAt(0).setVisibility(View.VISIBLE);
                break;
        }
    }

    private void getProductList(String flag, Boolean isUpdateDB) {
        setLoading(true);
        HashMap<String, String> mParams = new HashMap<>();
        mParams.put("requestType", flag); //myProduct or allProduct
        mParams.put("productName", "");
        mParams.put("quantity", "");
        mParams.put("minPrice", "");
        mParams.put("maxPrice", "");
        getDataManager().doServerProductInfoListApiCall(getDataManager().getHeader(), mParams)
                .getAsResponse(new VolleyResponseListener() {

                    @Override
                    public void onVolleyResponse(String response) {
                        setLoading(false);
                        ProductInfoResponse infoResponse = getDataManager().mGson.fromJson(response, ProductInfoResponse.class);

                        if (infoResponse.getStatus().equals("success")) {
                            list.clear();
                            list.addAll(infoResponse.getProductList());
                            productsAdapter.notifyDataSetChanged();

                            if (!list.isEmpty() && isUpdateDB){
                                //Todo check local db
                                updateProductDataDB(infoResponse.getProductList());
                            }
                            updateFilterView(0);
                            updateFilterCount(0,list.size());
                            updateNoRecordUI();
                        } else {
                            if (!infoResponse.getMessage().equalsIgnoreCase("Product not found")) {
                                CommonUtils.showToast(getBaseActivity(), infoResponse.getMessage(), Toast.LENGTH_SHORT);
                            }
                            list.clear();
                            updateNoRecordUI();
                        }
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        updateNoRecordUI();
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    private void updateNoRecordUI() {
        tvNoRecord.setVisibility(list.isEmpty() ? View.VISIBLE : View.GONE);
    }

    private void deleteProduct(int pos) {
        setLoading(true);

        HashMap<String, String> mParams = new HashMap<>();
        mParams.put("recordKey", list.get(pos).getRecordKey());
        getDataManager().doServerDeleteProductApiCall(getDataManager().getHeader(), mParams)
                .getAsResponse(new VolleyResponseListener() {

                    @Override
                    public void onVolleyResponse(String response) {
                        setLoading(false);

                        try {
                            JSONObject obj = new JSONObject(response);
                            String status= obj.getString("status");
                            String message= obj.getString("message");
                            if (status.equals("success")) {
                                //Todo manage local db add
                                doDeleteProductLocalDB(AppConstants.ONLINE, pos, list.get(pos));

                                list.remove(pos);
                                productsAdapter.notifyItemRemoved(pos);

                                updateNoRecordUI();
                                CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                            } else{
                                CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                            }
                        }catch (Exception e){
                            CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    //flag 0 -> gone count, 1 -> show count
    private void updateFilterCount(int flag, int size) {
        switch (flag) {
            case 0:
                frameCount.setVisibility(View.GONE);
                etProdName.setText("");
                etQty.setText("");
                rangeSeekBar.setValue(0, 10000);
                isRangeChange = false;
                break;

            case 1:
                frameCount.setVisibility(View.VISIBLE);
                tvFilterCount.setText(String.valueOf(size));
                break;
        }
    }

    /*Local DB Start here*/
    private void getProductListFromDB(String productType) {
        new Thread(() -> {
            list.clear();
            switch (productType) {
                case "allProduct":
                    list.addAll(getDataManager().getAllProductList());
                    break;

                case "myProduct":
                    list.addAll(getDataManager().getMyProductList(AppConstants.USER));
                    break;
            }

            handler.post(() -> {
                productsAdapter.notifyDataSetChanged();
                updateFilterView(0);
                updateFilterCount(0, list.size());
                updateNoRecordUI();
            });
        }).start();
    }

    private void updateProductDataDB(List<ProductInfoResponse.ProductListBean> productList) {
        new Thread(() -> {
            getDataManager().deleteAllProduct();
            List<Product> tempProductList = new ArrayList<>();

            for (ProductInfoResponse.ProductListBean productListBean : productList) {
                Product product = new Product();

                product.setProductId(productListBean.getProductId());
                product.setProductName(productListBean.getProductName());
                product.setProductImageThumb(productListBean.getProductImageThumb());
                product.setProductImage(productListBean.getProductImage());
                product.setPrice(Float.parseFloat(productListBean.getPrice()));
                product.setUnit(productListBean.getUnit());
                product.setQuantity(productListBean.getQuantity());
                product.setDescription(productListBean.getDescription());
                product.setRecordKey(productListBean.getRecordKey());
                product.setUser_id(productListBean.getUser_id());
                product.setUserType(productListBean.getUserType());
                product.setStatus(productListBean.getStatus());
                product.setCrd(productListBean.getCrd());
                product.setUpd(productListBean.getUpd());
                product.setEvent(AppConstants.DB_EVENT_ADD);
                product.setDataSync(AppConstants.DB_SYNC_TRUE);
                tempProductList.add(product);
            }
            getDataManager().insertAllProduct(tempProductList);
            AppLogger.d("database", "Db Add Product done");
        }).start();
    }

    private void doDeleteProductLocalDB(String flag, int pos, ProductInfoResponse.ProductListBean productListBean) {
        new Thread(() -> {
            switch (flag) {
                case AppConstants.ONLINE:
                    getDataManager().deleteUsingProductId(productListBean.getProductId());
                    break;

                case AppConstants.OFFLINE:
                    if (productListBean.getProductId().equals(productListBean.getRecordKey()))
                        getDataManager().deleteUsingProductId(productListBean.getProductId());
                    else {
                        Product product = new Product();

                        product.setProductId(productListBean.getProductId());
                        product.setProductName(productListBean.getProductName());
                        product.setProductImage(productListBean.getProductImage());
                        product.setPrice(Float.parseFloat(productListBean.getPrice()));
                        product.setUnit(productListBean.getUnit());
                        product.setQuantity(productListBean.getQuantity());
                        product.setDescription(productListBean.getDescription());
                        product.setRecordKey(productListBean.getRecordKey());
                        product.setUser_id(productListBean.getUser_id());
                        product.setUserType(productListBean.getUserType());
                        product.setStatus(productListBean.getStatus());
                        product.setCrd(productListBean.getCrd());
                        product.setUpd(productListBean.getUpd());
                        product.setProductImageThumb(productListBean.getProductImageThumb());
                        product.setEvent(AppConstants.DB_EVENT_DEL);
                        product.setDataSync(AppConstants.DB_SYNC_FALSE);
                        getDataManager().updateProduct(product);

                        //set sync status true
                        getDataManager().setLocalDataExistForSync(true);
                    }

                    handler.post(() -> {
                        list.remove(pos);
                        productsAdapter.notifyItemRemoved(pos);

                        updateNoRecordUI();
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.productDelSuccessMsg), Toast.LENGTH_SHORT);
                    });
                    break;
            }
            AppLogger.d("database", "product delete success");
        }).start();
    }

    private void getFilterResult(String prodName, String tempQty, String tempMin, String tempMax){
        new Thread(() -> {
            List<ProductInfoResponse.ProductListBean> localList  = new ArrayList<>();

            switch (productType) {
                case "allProduct":
                    if (!prodName.isEmpty() && !tempQty.isEmpty() && isRangeChange)
                        localList = getDataManager().productFilterAll(prodName, tempMin, tempMax, tempQty);
                    else if (!prodName.isEmpty() && tempQty.isEmpty() && !isRangeChange)
                        localList = getDataManager().productFilterAllName(prodName);
                    else if (prodName.isEmpty() && !tempQty.isEmpty() && !isRangeChange)
                        localList = getDataManager().productFilterAllQty(tempQty);
                    else if (prodName.isEmpty() && tempQty.isEmpty() && isRangeChange)
                        localList = getDataManager().productFilterAllPrice(tempMin, tempMax);
                    else if (!prodName.isEmpty() && !tempQty.isEmpty() && !isRangeChange)
                        localList = getDataManager().productFilterAllNameAndQty(prodName, tempQty);
                    else if (!prodName.isEmpty() && tempQty.isEmpty() && isRangeChange)
                        localList = getDataManager().productFilterAllNameAndPrice(prodName, tempMin, tempMax);
                    else if (prodName.isEmpty() && !tempQty.isEmpty() && isRangeChange)
                        localList = getDataManager().productFilterAllQtyAndPrice(tempQty, tempMin, tempMax);
                    break;

                case "myProduct":
                    if (!prodName.isEmpty() && !tempQty.isEmpty() && isRangeChange)
                        localList = getDataManager().productMyFilter(getDataManager().getUserInfo().getUserId(), prodName, tempMin, tempMax, tempQty);
                    else if (!prodName.isEmpty() && tempQty.isEmpty() && !isRangeChange)
                        localList = getDataManager().productMyFilterName(getDataManager().getUserInfo().getUserId(), prodName);
                    else if (prodName.isEmpty() && !tempQty.isEmpty() && !isRangeChange)
                        localList = getDataManager().productMyFilterQty(getDataManager().getUserInfo().getUserId(), tempQty);
                    else if (prodName.isEmpty() && tempQty.isEmpty() && isRangeChange)
                        localList = getDataManager().productMyFilterPrice(getDataManager().getUserInfo().getUserId(), tempMin, tempMax);
                    else if (!prodName.isEmpty() && !tempQty.isEmpty() && !isRangeChange)
                        localList = getDataManager().productMyFilterNameAndQty(getDataManager().getUserInfo().getUserId(), prodName, tempQty);
                    else if (!prodName.isEmpty() && tempQty.isEmpty() && isRangeChange)
                        localList = getDataManager().productMyFilterNameAndPrice(getDataManager().getUserInfo().getUserId(), prodName, tempMin, tempMax);
                    else if (prodName.isEmpty() && !tempQty.isEmpty() && isRangeChange)
                        localList = getDataManager().productMyFilterQtyAndPrice(getDataManager().getUserInfo().getUserId(), tempQty, tempMin, tempMax);
                    break;
            }

            List<ProductInfoResponse.ProductListBean> finalLocalList = localList;
            handler.post(() -> {
                hideKeyboard();
                rlFilter.callOnClick();
                list.clear();
                list.addAll(finalLocalList);
                productsAdapter.notifyDataSetChanged();
                updateFilterCount(1,list.size());
                updateNoRecordUI();
            });
        }).start();
    }
/*Local DB End here*/
}
