package com.agrinvestapp.ui.products.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.BuildConfig;
import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.ProductInfoResponse;
import com.agrinvestapp.data.model.db.Product;
import com.agrinvestapp.data.model.other.ProductUnit;
import com.agrinvestapp.ui.base.BaseFragment;
import com.agrinvestapp.ui.base.dialog.ImagePickCallback;
import com.agrinvestapp.ui.base.dialog.ImagePickDialog;
import com.agrinvestapp.ui.products.adapter.ProductUnitAdapter;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.AppLogger;
import com.agrinvestapp.utils.CalenderUtils;
import com.agrinvestapp.utils.CommonUtils;
import com.agrinvestapp.utils.Permission;
import com.agrinvestapp.volley.AppHelper;
import com.agrinvestapp.volley.VolleyMultipartRequest;
import com.agrinvestapp.volley.VolleyRequest;
import com.agrinvestapp.volley.VolleyResponseListener;
import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

public class AddEditProductFragment extends BaseFragment implements View.OnClickListener {

    private static final String MODEL_TAG = "productListBean";

    private Spinner spUnit;
    private EditText etProdName, etQty, etPrice, etDescription;
    private ImageView imgProduct, imgProductCircle;
    private TextView tvDesCount;

    private ArrayList<ProductUnit> unitList;
    private Bitmap productBitmap;
    private String mCurrentPhotoPath = "";

    private ProductInfoResponse.ProductListBean productListBean;
    private ProductsFragment productsFragment;
    private StringBuilder builder;

    private Boolean isClick = false;
    private Handler handler = new Handler(Looper.getMainLooper());

    public static AddEditProductFragment newInstance(ProductsFragment productsFragment, ProductInfoResponse.ProductListBean productListBean) {

        Bundle args = new Bundle();
        args.putParcelable(MODEL_TAG, productListBean);
        AddEditProductFragment fragment = new AddEditProductFragment();
        fragment.setInstance(productsFragment);
        fragment.setArguments(args);
        return fragment;
    }

    private void setInstance(ProductsFragment productsFragment) {
        this.productsFragment = productsFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments()!=null){
            productListBean = getArguments().getParcelable(MODEL_TAG);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_edit_product, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        /* toolbar view start */
        ImageView imgBack = view.findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);
        imgBack.setVisibility(View.VISIBLE);

        TextView tvTitle = view.findViewById(R.id.tvTitle);
        Button btnAdd = view.findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);

        /* toolbar view end */
        view.findViewById(R.id.llMain).setOnClickListener(this); //avoid bg click

        tvDesCount = view.findViewById(R.id.tvDesCount);
        imgProductCircle = view.findViewById(R.id.imgProductCircle);
        imgProductCircle.setOnClickListener(this);
        imgProduct = view.findViewById(R.id.imgProduct);
        imgProduct.setOnClickListener(this);

        etProdName = view.findViewById(R.id.etProdName);
        etQty = view.findViewById(R.id.etQty);
        etPrice = view.findViewById(R.id.etPrice);
        etDescription = view.findViewById(R.id.etDescription);

        spUnit = view.findViewById(R.id.spUnit);

        ProductUnitAdapter productUnitAdapter = new ProductUnitAdapter(getBaseActivity(), getList());
        spUnit.setAdapter(productUnitAdapter);

        if (productListBean!=null){
            tvTitle.setText(R.string.editProduct);
            btnAdd.setText(R.string.update);

            updateEditUI();
        }else tvTitle.setText(R.string.addProduct);

        etDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                builder = new StringBuilder();
                builder.append(s.length()).append("/").append("250");
                tvDesCount.setText(builder);
            }
        });
    }

    private ArrayList<ProductUnit> getList() {
        unitList = new ArrayList<>();

        ProductUnit productUnit = new ProductUnit();
        productUnit.localisationTitle = getString(R.string.unit);
        productUnit.unitName = "Unit";
        unitList.add(productUnit);

        productUnit = new ProductUnit();
        productUnit.localisationTitle = getString(R.string.kg);
        productUnit.unitName = "Kg";
        unitList.add(productUnit);

        productUnit = new ProductUnit();
        productUnit.localisationTitle = getString(R.string.litre);
        productUnit.unitName = "Litre";
        unitList.add(productUnit);

        productUnit = new ProductUnit();
        productUnit.localisationTitle = getString(R.string.gram);
        productUnit.unitName = "Gram";
        unitList.add(productUnit);

        productUnit = new ProductUnit();
        productUnit.localisationTitle = getString(R.string.bag);
        productUnit.unitName = "Bag";
        unitList.add(productUnit);

        productUnit = new ProductUnit();
        productUnit.localisationTitle = getString(R.string.packet);
        productUnit.unitName = "Packet";
        unitList.add(productUnit);

        return unitList;
    }

    private void updateEditUI() {
        if (!productListBean.getProductImage().isEmpty())
            updateImage(productListBean.getProductImageThumb());

        etProdName.setText(productListBean.getProductName());
        etQty.setText(productListBean.getQuantity());
        etPrice.setText(productListBean.getPrice());
        etDescription.setText(productListBean.getDescription());

        builder = new StringBuilder();
        builder.append(etDescription.length()).append("/").append("250");
        tvDesCount.setText(builder);
        for (int i = 0; i < unitList.size(); i++) {
            if (unitList.get(i).unitName.equalsIgnoreCase(productListBean.getUnit())) {
                spUnit.setSelection(i);
            }
        }
    }

    private void updateImage(String productImage) {
        imgProduct.setVisibility(View.GONE);
        if (!productImage.isEmpty()) Picasso.get().load(productImage).placeholder(R.drawable.ic_picture).into(imgProductCircle);
        else imgProductCircle.setImageBitmap(productBitmap);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                getBaseActivity().onBackPressed();
                break;

            case R.id.btnAdd:
                if (!isClick) {
                    isClick = true;
                    doAddProductOperation();
                }

                new Handler().postDelayed(() -> isClick = false, 3000);
                break;

            case R.id.imgProductCircle:
            case R.id.imgProduct:
                if (isNetworkConnected(true)) {
                    if (Permission.RequestMultiplePermissionCamera(getBaseActivity())) {
                        ImagePickDialog.newInstance(new ImagePickCallback() {
                            @Override
                            public void onCameraClick(ImagePickDialog imagePickDialog) {
                                dispatchTakePictureIntent();

                                imagePickDialog.dismiss();
                            }

                            @Override
                            public void onGalleryClick(ImagePickDialog imagePickDialog) {
                                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(photoPickerIntent, AppConstants.REQUEST_GALLERY);

                                imagePickDialog.dismiss();
                            }
                        }).show(getChildFragmentManager());
                    }
                } else
                    CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_offline_upload_image), Toast.LENGTH_SHORT);
                break;
        }
    }

    private void doAddProductOperation() {
        String tempProdName = etProdName.getText().toString().trim();
        String tempQty = etQty.getText().toString().trim();
        tempQty = tempQty.isEmpty() ? "" : String.valueOf(Math.round(Integer.parseInt(tempQty)));
        String tempUnit = unitList.get(spUnit.getSelectedItemPosition()).unitName.toLowerCase();
        float price = etPrice.getText().toString().isEmpty() ? 0 : Double.valueOf(etPrice.getText().toString()).floatValue();

        String tempPrice = String.format(Locale.getDefault(), "%.2f", price);
        String tempDes = etDescription.getText().toString().trim();

        if (isNetworkConnected(true)) {
            String finalTempQty = tempQty;
            getBaseActivity().doSyncAppDB(false, flag1 -> {
                switch (flag1) {
                    case AppConstants.ONLINE:
                        if (isVerifyInput(tempProdName, finalTempQty, tempUnit, tempPrice, tempDes)) {
                            if (productListBean == null)
                                doAddProduct(tempProdName, finalTempQty, tempUnit, tempPrice, tempDes, CalenderUtils.getTimestamp());
                            else if (!productListBean.getProductId().equalsIgnoreCase(productListBean.getRecordKey()))
                                doUpdateProduct(tempProdName, finalTempQty, tempUnit, tempPrice, tempDes);
                            else
                                doAddProduct(tempProdName, finalTempQty, tempUnit, tempPrice, tempDes, productListBean.getRecordKey());
                        }
                        break;

                    case AppConstants.OFFLINE:
                        if (isVerifyLocalDBInput(tempProdName, finalTempQty, tempUnit, tempPrice, tempDes)) {
                            if (productListBean == null)
                                doAddEditProductToLocalDB("add", tempProdName, finalTempQty, tempUnit, tempPrice, tempDes);
                            else if (!productListBean.getProductId().equalsIgnoreCase(productListBean.getRecordKey()))
                                doAddEditProductToLocalDB("edit", tempProdName, finalTempQty, tempUnit, tempPrice, tempDes);
                            else
                                doAddEditProductToLocalDB("edit", tempProdName, finalTempQty, tempUnit, tempPrice, tempDes);
                        }
                        break;
                }
            });
        } else {
            if (getDataManager().getUserInfo().getUserType().equals(AppConstants.TYPE_BUSINESS)) {
                CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_internet), Toast.LENGTH_SHORT);
            } else {
                if (isVerifyLocalDBInput(tempProdName, tempQty, tempUnit, tempPrice, tempDes)) {
                    if (productListBean == null)
                        doAddEditProductToLocalDB("add", tempProdName, tempQty, tempUnit, tempPrice, tempDes);
                    else if (!productListBean.getProductId().equalsIgnoreCase(productListBean.getRecordKey()))
                        doAddEditProductToLocalDB("edit", tempProdName, tempQty, tempUnit, tempPrice, tempDes);
                    else
                        doAddEditProductToLocalDB("edit", tempProdName, tempQty, tempUnit, tempPrice, tempDes);
                }
            }
        }
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getBaseActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getBaseActivity(),
                        BuildConfig.APPLICATION_ID + ".provider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, AppConstants.REQUEST_TAKE_PHOTO);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        @SuppressLint("SimpleDateFormat")
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getBaseActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private boolean isVerifyInput(String tempProdName, String tempQty, String tempUnit, String tempPrice, String tempDes) {
        if (productListBean==null && productBitmap==null && mCurrentPhotoPath.isEmpty()){
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_upload_image), Toast.LENGTH_SHORT);
            return false;
        } else if (tempProdName.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_prod_name), Toast.LENGTH_SHORT);
            return false;
        } else if (!(tempProdName.length() >= 3)) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_prod_length), Toast.LENGTH_SHORT);
            return false;
        } else if (tempQty.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_qty), Toast.LENGTH_SHORT);
            return false;
        } else if (Double.parseDouble(tempQty) <= 0) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_qty_zero), Toast.LENGTH_SHORT);
            return false;
        } else if (tempUnit.equalsIgnoreCase("Unit")) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_prod_unit), Toast.LENGTH_SHORT);
            return false;
        } else if (tempPrice.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_price), Toast.LENGTH_SHORT);
            return false;
        } else if (tempPrice.startsWith(".")) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_valid_price), Toast.LENGTH_SHORT);
            return false;
        } else if (Double.parseDouble(tempPrice) <= 0) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_price_zero), Toast.LENGTH_SHORT);
            return false;
        } else if (Double.parseDouble(tempPrice) > 10000) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_max_10k), Toast.LENGTH_SHORT);
            return false;
        } else if (tempDes.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_des), Toast.LENGTH_SHORT);
            return false;
        } else return true;
    }

    private boolean isVerifyLocalDBInput(String tempProdName, String tempQty, String tempUnit, String tempPrice, String tempDes) {
        if (productBitmap != null || !mCurrentPhotoPath.isEmpty()) {
            //In Edit case user not able to upload image
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_offline_upload_image2), Toast.LENGTH_SHORT);
            return false;
        } else if (tempProdName.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_prod_name), Toast.LENGTH_SHORT);
            return false;
        } else if (!(tempProdName.length() >= 3)) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_prod_length), Toast.LENGTH_SHORT);
            return false;
        } else if (tempQty.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_qty), Toast.LENGTH_SHORT);
            return false;
        } else if (Double.parseDouble(tempQty) <= 0) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_qty_zero), Toast.LENGTH_SHORT);
            return false;
        } else if (tempUnit.equalsIgnoreCase("Unit")) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_prod_unit), Toast.LENGTH_SHORT);
            return false;
        } else if (tempPrice.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_price), Toast.LENGTH_SHORT);
            return false;
        } else if (tempPrice.startsWith(".")) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_valid_price), Toast.LENGTH_SHORT);
            return false;
        } else if (Double.parseDouble(tempPrice) <= 0) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_price_zero), Toast.LENGTH_SHORT);
            return false;
        } else if (Double.parseDouble(tempPrice) > 10000) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_max_10k), Toast.LENGTH_SHORT);
            return false;
        } else if (tempDes.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_des), Toast.LENGTH_SHORT);
            return false;
        } else return true;
    }

    private void doAddProduct(String tempProdName, String tempQty, String tempUnit, String tempPrice, String tempDes, String recordKey) {
        setLoading(true);

        HashMap<String, String> mParameterMap = new HashMap<>();
        mParameterMap.put("productName", tempProdName);
        mParameterMap.put("quantity", tempQty);
        mParameterMap.put("unit", tempUnit);
        mParameterMap.put("price", tempPrice);
        mParameterMap.put("description", tempDes);
        mParameterMap.put("recordKey", recordKey);

        HashMap<String, VolleyMultipartRequest.DataPart> mDataParameterMap = new HashMap<>();

        if (productBitmap!=null){
            byte[] productImageInFile = AppHelper.getFileDataFromDrawable(productBitmap);
            mDataParameterMap.put("productImage", new VolleyMultipartRequest.DataPart("image.jpg", productImageInFile, "*/*"));
        }

        getDataManager().doServerAddProductApiCall(getDataManager().getHeader(), mParameterMap, mDataParameterMap)
                .getAsResponse(new VolleyResponseListener() {
                    @Override
                    public void onVolleyResponse(String response) {
                        setLoading(false);

                        try {
                            JSONObject obj = new JSONObject(response);
                            String status= obj.getString("status");
                            String message= obj.getString("message");

                            if (status.equals("success")) {
                                //Todo manage local db add
                                doAddUpdateProductLocalDB("add", obj, message);

                            } else {
                                CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                            }
                        }catch (Exception e){
                            CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    private void doUpdateProduct(String tempProdName, String tempQty, String tempUnit, String tempPrice, String tempDes) {
        setLoading(true);

        HashMap<String, String> mParameterMap = new HashMap<>();
        mParameterMap.put("productName", tempProdName);
        mParameterMap.put("quantity", tempQty);
        mParameterMap.put("unit", tempUnit);
        mParameterMap.put("price", tempPrice);
        mParameterMap.put("description", tempDes);
        mParameterMap.put("recordKey", productListBean.getRecordKey());

        HashMap<String, VolleyMultipartRequest.DataPart> mDataParameterMap = new HashMap<>();

        if (productBitmap!=null){
            byte[] productImageInFile = AppHelper.getFileDataFromDrawable(productBitmap);
            mDataParameterMap.put("productImage", new VolleyMultipartRequest.DataPart("image.jpg", productImageInFile, "*/*"));
        }

        getDataManager().doServerUpdateProductApiCall(getDataManager().getHeader(), mParameterMap, mDataParameterMap)
                .getAsResponse(new VolleyResponseListener() {
                    @Override
                    public void onVolleyResponse(String response) {
                        setLoading(false);

                        try {
                            JSONObject obj = new JSONObject(response);
                            String status= obj.getString("status");
                            String message= obj.getString("message");

                            if (status.equals("success")) {
                                //Todo manage local db update
                                doAddUpdateProductLocalDB("edit", obj, message);

                            } else {
                                CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                            }
                        }catch (Exception e){
                            CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case AppConstants.REQUEST_GALLERY:
                    try {
                        Uri uri = data.getData();
                        if (uri != null) {
                            CropImage.activity(uri).setCropShape(CropImageView.CropShape.RECTANGLE).setMinCropResultSize(300, 300).setMaxCropResultSize(4000, 4000).setAspectRatio(640, 480).start(getBaseActivity(), AddEditProductFragment.this);
                        } else {
                            Toast.makeText(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();
                        }
                    } catch (OutOfMemoryError e) {
                        CommonUtils.showToast(getActivity(),getString(R.string.alert_out_of_memory), Toast.LENGTH_SHORT);
                        e.printStackTrace();
                    } catch (Exception e) {
                        Toast.makeText(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                    break;

                case AppConstants.REQUEST_TAKE_PHOTO:
                    try {
                        Uri uri = Uri.fromFile(new File(mCurrentPhotoPath));
                        if (uri != null) {
                            // Calling Image Cropper
                            CropImage.activity(uri).setCropShape(CropImageView.CropShape.RECTANGLE).setMinCropResultSize(300, 300).setMaxCropResultSize(4000, 4000).setAspectRatio(640, 480).start(getBaseActivity(), AddEditProductFragment.this);
                        } else {
                            Toast.makeText(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();
                        }

                    } catch (OutOfMemoryError e) {
                        CommonUtils.showToast(getActivity(),getString(R.string.alert_out_of_memory), Toast.LENGTH_SHORT);
                        e.printStackTrace();
                    } catch (Exception e) {
                        Toast.makeText(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                    break;

                case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    if (result != null) {
                        Uri uri = result.getUri();
                        try {
                            productBitmap = MediaStore.Images.Media.getBitmap(getBaseActivity().getContentResolver(), uri);
                            updateImage("");
                        } catch (Exception e) {
                            Toast.makeText(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();
                        } catch (OutOfMemoryError e) {
                            Toast.makeText(getBaseActivity(), getString(R.string.alert_out_of_memory), Toast.LENGTH_SHORT).show();
                        }
                    }
                    break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case AppConstants.REQUEST_MULTIPLE_CAMERA_PERMISSIONS:
                if (grantResults.length > 0) {
                    boolean cameraPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean externalStoragePermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                    if (cameraPermission && externalStoragePermission) {
                        imgProduct.callOnClick();
                    } else {
                        CommonUtils.showToast(getActivity(),getString(R.string.permission_camera_deny), Toast.LENGTH_SHORT);
                    }
                }

                break;
        }
    }

    /* Local db start here */
    private void doAddUpdateProductLocalDB(String flag, JSONObject obj, String message) {
        new Thread(() -> {
            try {
                Product product = new Product();

                JSONObject subObj = obj.getJSONObject("productDetail");
                product.setProductId(subObj.getString("productId"));
                product.setProductName(subObj.getString("productName"));
                product.setProductImage(subObj.getString("productImage"));
                product.setPrice(Float.parseFloat(subObj.getString("price")));
                product.setUnit(subObj.getString("unit"));
                product.setQuantity(subObj.getString("quantity"));
                product.setDescription(subObj.getString("description"));
                product.setRecordKey(subObj.getString("recordKey"));
                product.setUser_id(subObj.getString("user_id"));
                product.setUserType(subObj.getString("userType"));
                product.setStatus(subObj.getString("status"));
                product.setProductImageThumb(subObj.getString("productImageThumb"));
                product.setCrd(subObj.getString("crd"));
                product.setUpd(subObj.getString("upd"));
                product.setDataSync(AppConstants.DB_SYNC_TRUE);

                switch (flag){
                    case "add":
                        product.setEvent(AppConstants.DB_EVENT_ADD);

                        //do not remove below line
                        getDataManager().deleteUsingProductId(product.getRecordKey());
                        getDataManager().insertProduct(product);
                        break;

                    case "edit":
                        product.setEvent(AppConstants.DB_EVENT_EDIT);
                        getDataManager().updateProduct(product);
                        break;
                }

                handler.post(() -> {
                    CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);

                    getBaseActivity().onBackPressed();
                    if (productsFragment != null)
                        productsFragment.hitApi("myProduct", false);
                });
                AppLogger.d("database", "product add update success");
            }catch (Exception e){
                AppLogger.e("Database", "Exception occure while add update product: "+e.getMessage());
            }
        }).start();
    }

    private void doAddEditProductToLocalDB(String flag, String tempProdName, String tempQty, String tempUnit, String tempPrice, String tempDes) {
        new Thread(() -> {
            Product product = new Product();

            product.setProductName(tempProdName);
            product.setPrice(Float.parseFloat(tempPrice));
            product.setQuantity(tempQty);
            product.setUnit(tempUnit);
            product.setDescription(tempDes);
            switch (flag) {

                case "add":
                    String rKey = CalenderUtils.getTimestamp();

                    product.setProductId(rKey);
                    product.setProductImage("");
                    product.setProductImageThumb("");
                    product.setRecordKey(rKey);
                    product.setUser_id(getDataManager().getUserInfo().getUserId());
                    product.setUserType(AppConstants.USER);
                    product.setStatus("1");
                    product.setCrd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    product.setUpd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    product.setEvent(AppConstants.DB_EVENT_ADD);
                    product.setDataSync(AppConstants.DB_SYNC_FALSE);
                    getDataManager().insertProduct(product);

                    handler.post(() -> {
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.productAddSuccessMsg), Toast.LENGTH_SHORT);
                        getBaseActivity().onBackPressed();
                        if (productsFragment != null) productsFragment.hitApi("myProduct", false);
                    });
                    break;

                case "edit":
                    product.setProductId(productListBean.getProductId());
                    product.setProductImage(productListBean.getProductImage());
                    product.setProductImageThumb(productListBean.getProductImageThumb());
                    product.setRecordKey(productListBean.getRecordKey());
                    product.setUser_id(productListBean.getUser_id());
                    product.setUserType(productListBean.getUserType());
                    product.setStatus(productListBean.getStatus());
                    product.setCrd(productListBean.getCrd());
                    product.setUpd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    product.setEvent(productListBean.getProductId().equals(productListBean.getRecordKey()) ? AppConstants.DB_EVENT_ADD : AppConstants.DB_EVENT_EDIT);
                    product.setDataSync(AppConstants.DB_SYNC_FALSE);
                    getDataManager().updateProduct(product);

                    handler.post(() -> {
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.productUpdateSuccessMsg), Toast.LENGTH_SHORT);
                        getBaseActivity().onBackPressed();
                        if (productsFragment != null) productsFragment.hitApi("myProduct", false);
                    });
                    break;
            }
            AppLogger.d("database", "product add update success");
            //set sync status true
            getDataManager().setLocalDataExistForSync(true);
        }).start();
    }
    /* Local db end here */
}
