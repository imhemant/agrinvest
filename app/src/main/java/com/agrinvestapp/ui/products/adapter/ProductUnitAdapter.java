package com.agrinvestapp.ui.products.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.other.ProductUnit;

import java.util.ArrayList;

/**
 * Created by hemant.
 * Date: 7/6/18
 * Time: 4:03 PM
 */

public class ProductUnitAdapter extends ArrayAdapter {

    private Activity activity;
    private ArrayList<ProductUnit> list;

    public ProductUnitAdapter(@NonNull Activity activity, ArrayList<ProductUnit> list) {
        super(activity, R.layout.item_name);
        this.activity = activity;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View view, @NonNull ViewGroup viewGroup) {
        String unitName = list.get(position).localisationTitle;

        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(activity);
            view = inflater.inflate(R.layout.item_name, viewGroup, false);
        }

        TextView tvSpinnerName = view.findViewById(R.id.tvSpinnerName);
        if (position == 0) {
            tvSpinnerName.setHintTextColor(activity.getResources().getColor(R.color.grey));
            tvSpinnerName.setHint(unitName);
        } else {
            tvSpinnerName.setText(unitName);
        }

        return view;
    }

    @Nullable
    @Override
    public View getDropDownView(int position, @Nullable View view, @NonNull ViewGroup viewGroup) {

        String unitName = list.get(position).localisationTitle;
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(activity);
            view = inflater.inflate(R.layout.item_name, viewGroup, false);
            view.setTag("");
        }

        TextView tvSpinnerName = view.findViewById(R.id.tvSpinnerName);
        tvSpinnerName.setText(unitName);

        tvSpinnerName.setPadding(10, 0, 0, 0);

        return view;
    }


}


