package com.agrinvestapp.ui.products;

public interface CheckSyncProductCallback {
    void onSync(String data);

    void onCallApi();
}
