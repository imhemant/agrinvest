package com.agrinvestapp.ui.products.adapter;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.ProductInfoResponse;
import com.agrinvestapp.ui.products.fragment.ProductsFragment;
import com.agrinvestapp.utils.pagination.FooterLoader;
import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chauthai.swipereveallayout.ViewBinderHelper;
import com.squareup.picasso.Picasso;

import java.util.List;


/**
 * Created by hemant
 * Date: 17/4/18
 * Time: 4:03 PM
 */

public class ProductsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEWTYPE_ITEM = 1;
    private final int VIEWTYPE_LOADER = 2;
    // This object helps you save/restore the open/close state of each view
    private final ViewBinderHelper viewBinderHelper = new ViewBinderHelper();
    private List<ProductInfoResponse.ProductListBean> list;
    private boolean showLoader;
    private ItemClickListener itemClickListener;

    private boolean isClick = false;


    public ProductsAdapter(List<ProductInfoResponse.ProductListBean> list, ItemClickListener itemClickListener) {
        this.list = list;
        this.itemClickListener = itemClickListener;
        viewBinderHelper.setOpenOnlyOne(true);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder rvHolder, int position) {
        if (rvHolder instanceof FooterLoader) {
            FooterLoader loaderViewHolder = (FooterLoader) rvHolder;
            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }
            return;
        }
        ProductsAdapter.ViewHolder holder = ((ProductsAdapter.ViewHolder) rvHolder);

        ProductInfoResponse.ProductListBean bean = list.get(position);

        viewBinderHelper.bind(holder.swipe, bean.getRecordKey());

        if (ProductsFragment.productType.equals("allProduct")) {
            holder.swipe.close(true);
            holder.swipe.setLockDrag(true);
        }

        if (!bean.getProductImageThumb().isEmpty())
            Picasso.get().load(bean.getProductImageThumb()).placeholder(R.drawable.ic_picture).fit().into(holder.imgProduct);
        else Picasso.get().load(R.drawable.ic_picture).fit().into(holder.imgProduct);
        holder.tvName.setText(bean.getProductName());
        StringBuilder builder = new StringBuilder();

        builder.append("$").append(bean.getPrice()).append(" / ").append(getUnit(bean.getUnit().toLowerCase(), holder.tvAmt.getContext()));
        holder.tvAmt.setText(builder);
        holder.tvDescription.setText(bean.getDescription());
    }

    private String getUnit(String unit, Context context) {
        switch (unit) {
            case "kg":
                unit = context.getString(R.string.kg);
                break;

            case "litre":
                unit = context.getString(R.string.litre);
                break;

            case "gram":
                unit = context.getString(R.string.gram);
                break;

            case "bag":
                unit = context.getString(R.string.bag);
                break;

            case "packet":
                unit = context.getString(R.string.packet);
                break;
        }

        return unit;
    }

    public void showLoading(boolean status) {
        showLoader = status;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case VIEWTYPE_ITEM:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_products, parent, false);
                return new ViewHolder(view);

            case VIEWTYPE_LOADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pagination_item_loader, parent, false);
                return new FooterLoader(view);
        }
        return null;

    }

    @Override
    public int getItemViewType(int position) {
        if (position == list.size() - 1) {
            return showLoader ? VIEWTYPE_LOADER : VIEWTYPE_ITEM;
        }
        return VIEWTYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface ItemClickListener {
        void onItemClick(int pos);

        void onEditClick(int pos);

        void onDeleteClick(int pos);
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView imgProduct;
        private TextView tvName, tvAmt, tvDescription;
        private LinearLayout llDelete, llEdit;
        private SwipeRevealLayout swipe;

        ViewHolder(@NonNull View v) {
            super(v);
            imgProduct = v.findViewById(R.id.imgProduct);
            tvName = v.findViewById(R.id.tvName);
            tvAmt = v.findViewById(R.id.tvAmt);
            tvDescription = v.findViewById(R.id.tvDescription);
            swipe = v.findViewById(R.id.swipe);


            llEdit = v.findViewById(R.id.llEdit);
            llDelete = v.findViewById(R.id.llDelete);

            v.findViewById(R.id.rlMainView).setOnClickListener(this);
            llEdit.setOnClickListener(this);
            llDelete.setOnClickListener(this);
        }

        @Override
        public void onClick(@NonNull final View view) {

            switch (view.getId()) {
                case R.id.llEdit:
                    if (getAdapterPosition() != -1) {
                        swipe.close(true);
                        itemClickListener.onEditClick(getAdapterPosition());
                    }
                    break;

                case R.id.llDelete:
                    if (getAdapterPosition() != -1) {
                        swipe.close(true);
                        itemClickListener.onDeleteClick(getAdapterPosition());
                    }
                    break;

                default:
                    if (!isClick) {
                        isClick = true;
                        if (getAdapterPosition() != -1) {
                            swipe.close(true);
                            itemClickListener.onItemClick(getAdapterPosition());
                        }
                    }
                    new Handler().postDelayed(() -> isClick = false, 3000);

                    break;
            }
        }
    }

}

