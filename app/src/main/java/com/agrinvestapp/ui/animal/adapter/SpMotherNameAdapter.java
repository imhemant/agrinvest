package com.agrinvestapp.ui.animal.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.MotherResponse;

import java.util.List;

/**
 * Created by hemant.
 * Date: 7/6/18
 * Time: 4:03 PM
 */

public class SpMotherNameAdapter extends ArrayAdapter {

    private Activity activity;
    private List<MotherResponse.AnimalMotherListBean> list;

    public SpMotherNameAdapter(@NonNull Activity activity, List<MotherResponse.AnimalMotherListBean> list) {
        super(activity, R.layout.item_name);
        this.activity = activity;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View view, @NonNull ViewGroup viewGroup) {
        MotherResponse.AnimalMotherListBean bean = list.get(position);

        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(activity);
            view = inflater.inflate(R.layout.item_name, viewGroup, false);
        }

        TextView tvSpinnerName = view.findViewById(R.id.tvSpinnerName);

        if (position == 0) {
            tvSpinnerName.setHint(bean.getTagNumber());
        } else {
            tvSpinnerName.setText(bean.getTagNumber());
        }

        return view;
    }

    @Nullable
    @Override
    public View getDropDownView(int position, @Nullable View view, @NonNull ViewGroup viewGroup) {

        MotherResponse.AnimalMotherListBean bean = list.get(position);
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(activity);
            view = inflater.inflate(R.layout.item_name, viewGroup, false);
            view.setTag("");
        }

        TextView tvSpinnerName = view.findViewById(R.id.tvSpinnerName);
        tvSpinnerName.setText(bean.getTagNumber());

        tvSpinnerName.setPadding(10, 0, 0, 0);

        return view;
    }


}


