package com.agrinvestapp.ui.animal.fragment.pregnancy;


import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.AnimalInfoResponse;
import com.agrinvestapp.data.model.api.PregnancyTestInfoResponse;
import com.agrinvestapp.data.model.db.PregnancyTest;
import com.agrinvestapp.ui.animal.fragment.pregnancy.adapter.PregnancyTestAdapter;
import com.agrinvestapp.ui.animal.fragment.pregnancy.dialog.PregnancyCallback;
import com.agrinvestapp.ui.animal.fragment.pregnancy.dialog.PregnancyDialog;
import com.agrinvestapp.ui.base.BaseFragment;
import com.agrinvestapp.ui.base.dialog.DeleteDialog;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.AppLogger;
import com.agrinvestapp.utils.CalenderUtils;
import com.agrinvestapp.utils.CommonUtils;
import com.agrinvestapp.volley.VolleyJsonObjectListener;
import com.agrinvestapp.volley.VolleyRequest;
import com.agrinvestapp.volley.VolleyResponseListener;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PregnancyFragment extends BaseFragment implements View.OnClickListener, PregnancyCallback {

    private TextView tvNoRecord;
    private RecyclerView rvPregnancy;

    private List<PregnancyTestInfoResponse.PregnancyListBean> list;

    private Handler handler = new Handler(Looper.getMainLooper());

    private AnimalInfoResponse.AnimalListBean animalInfoBean;
    private PregnancyTestAdapter pregnancyTestAdapter;

    public static PregnancyFragment newInstance(AnimalInfoResponse.AnimalListBean animalInfoBean) {

        Bundle args = new Bundle();
        args.putParcelable(AppConstants.ANIMAL_MODEL, animalInfoBean);
        PregnancyFragment fragment = new PregnancyFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            animalInfoBean = getArguments().getParcelable(AppConstants.ANIMAL_MODEL);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pregnancy, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
         /* toolbar view start */
        ImageView imgBack = view.findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);
        imgBack.setVisibility(View.VISIBLE);

        TextView tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.pregnancy_test);

        FrameLayout frameAdd = view.findViewById(R.id.frameAddT);
        frameAdd.setVisibility(View.VISIBLE);
        frameAdd.setOnClickListener(this);
        /* toolbar view end */
        view.findViewById(R.id.rlMain).setOnClickListener(this); //avoid bg click
        tvNoRecord = view.findViewById(R.id.tvNoRecord);

        rvPregnancy = view.findViewById(R.id.rvPregnancy);
        list = new ArrayList<>();

        pregnancyTestAdapter = new PregnancyTestAdapter(list, new PregnancyTestAdapter.ItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                //not require
            }

            @Override
            public void onEditClick(int pos) {
                //not require
            }

            @Override
            public void onDeleteClick(int pos) {
                DeleteDialog.newInstance(false, "", () -> {
                    if (isNetworkConnected(true)) {
                        getBaseActivity().doSyncAppDB(false, flag -> {
                            switch (flag) {
                                case AppConstants.ONLINE:
                                    doAddDelPregnancyTest(pos, AppConstants.REQUEST_TYPE_DEL, list.get(pos));
                                    break;

                                case AppConstants.OFFLINE:
                                    doAddDelPregnancyTestLocal(pos, AppConstants.REQUEST_TYPE_DEL, list.get(pos));
                                    break;
                            }
                        });
                    } else
                        doAddDelPregnancyTestLocal(pos, AppConstants.REQUEST_TYPE_DEL, list.get(pos));
                }).show(getChildFragmentManager());
            }
        });
        rvPregnancy.setAdapter(pregnancyTestAdapter);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (isNetworkConnected(true)) {
            getBaseActivity().doSyncAppDB(false, flag -> {
                switch (flag) {
                    case AppConstants.ONLINE:
                        getPregnancyInfo();
                        break;

                    case AppConstants.OFFLINE:
                        getPregnancyTestInfoFromDB();
                        break;
                }
            });
        }
        else {
            getPregnancyTestInfoFromDB();
        }
    }

    private void getPregnancyInfo() {
        setLoading(true);

        HashMap<String, String> mParameterMap = new HashMap<>();
        mParameterMap.put("requestType", AppConstants.REQUEST_TYPE_PREGNANCY);
        mParameterMap.put("animalId", animalInfoBean.getAnimalId());
        mParameterMap.put("recordKey", animalInfoBean.getRecordKey());
        getDataManager().doServerTreatmentInfoListApiCall(getDataManager().getHeader(), mParameterMap)
                .getAsResponse(new VolleyResponseListener() {
                    @Override
                    public void onVolleyResponse(String response) {
                        setLoading(false);
                        PregnancyTestInfoResponse infoResponse = getDataManager().mGson.fromJson(response, PregnancyTestInfoResponse.class);

                        if (infoResponse.getStatus().equals("success")) {
                            list.clear();
                            list.addAll(infoResponse.getPregnancyList());

                            updateUI();
                            if (!list.isEmpty()) {
                                //Todo manage local db add all data
                                doAddAllPregnancyTestLocalDB(infoResponse.getPregnancyList());
                            }

                            pregnancyTestAdapter.notifyDataSetChanged();
                        } else {
                            updateUI();
                            if (infoResponse.getMessage().equals(getString(R.string.no_record_found))) {
                                list.clear();
                                pregnancyTestAdapter.notifyDataSetChanged();
                            }
                            if (!infoResponse.getMessage().equals(getString(R.string.no_record_found)))
                                CommonUtils.showToast(getBaseActivity(), infoResponse.getMessage(), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        updateUI();
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    private void updateUI() {
        tvNoRecord.setVisibility(list.isEmpty() ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                getBaseActivity().onBackPressed();
                break;

            case R.id.frameAddT:
                if (list.size() < 10) {
                    PregnancyDialog.newInstance(0, PregnancyFragment.this).show(getChildFragmentManager());
                } else
                    CommonUtils.showToast(getBaseActivity(), getString(R.string.pregnancyLimit), Toast.LENGTH_SHORT);

                break;
        }
    }

    private void doAddDelPregnancyTest(int pos, String flag, PregnancyTestInfoResponse.PregnancyListBean bean) {
        setLoading(true);

        HashMap<String, String> mParams = new HashMap<>();
        mParams.put("requestType", flag);
        mParams.put("recordKey", animalInfoBean.getRecordKey());  //animal record key
        mParams.put("pregnancyName", bean.getPregnancyName());
        mParams.put("pregnancyDate", bean.getPregnancyDate());
        mParams.put("pregnancyDescription", bean.getPregnancyDescription());

        switch (flag) {
            case AppConstants.REQUEST_TYPE_ADD:
                mParams.put("pregnancyId", bean.getPregnancyId());
                mParams.put("pregnancyRecordKey", bean.getRecordKey());
                break;

            case AppConstants.REQUEST_TYPE_DEL:
                mParams.put("pregnancyId", bean.getPregnancyId());
                mParams.put("pregnancyRecordKey", bean.getRecordKey());
                break;
        }

        getDataManager().doServerPregnancyTestAddDelApiCall(getDataManager().getHeader(), mParams)
                .getAsJsonObject(new VolleyJsonObjectListener() {

                    @Override
                    public void onVolleyJsonObject(JSONObject obj) {
                        setLoading(false);

                        try {
                            String status = obj.getString("status");
                            String message = obj.getString("message");

                            if (status.equals("success")) {

                                switch (flag) {
                                    case AppConstants.REQUEST_TYPE_ADD:
                                        JSONObject jsonObject = obj.getJSONObject("pregnancyDetail");

                                        PregnancyTestInfoResponse.PregnancyListBean bean = new PregnancyTestInfoResponse.PregnancyListBean();
                                        bean.setPregnancyId(jsonObject.getString("pregnancyId"));
                                        bean.setAnimal_id(jsonObject.getString("animal_id"));
                                        bean.setPregnancyName(jsonObject.getString("pregnancyName"));
                                        bean.setPregnancyDate(jsonObject.getString("pregnancyDate"));
                                        bean.setPregnancyDescription(jsonObject.getString("pregnancyDescription"));
                                        bean.setRecordKey(jsonObject.getString("recordKey"));
                                        bean.setCrd(jsonObject.getString("crd"));
                                        bean.setUpd(jsonObject.getString("upd"));

                                        list.add(pos, bean);
                                        pregnancyTestAdapter.notifyItemInserted(pos);

                                        rvPregnancy.smoothScrollToPosition(pos);
                                        updateLocalDB(flag, list.get(pos));

                                        CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                                        break;

                                    case AppConstants.REQUEST_TYPE_DEL:
                                        updateLocalDB(flag, list.get(pos));

                                        list.remove(pos);
                                        pregnancyTestAdapter.notifyItemRemoved(pos);
                                        CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                                        break;
                                }

                                updateUI();

                            } else
                                CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                        } catch (Exception e) {
                            CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyJsonException() {
                        setLoading(false);
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });

    }

    @Override
    public void onAdd(PregnancyTestInfoResponse.PregnancyListBean bean) {
        if (isNetworkConnected(true)) {
            getBaseActivity().doSyncAppDB(false, flag -> {
                switch (flag) {
                    case AppConstants.ONLINE:
                        doAddDelPregnancyTest(0, AppConstants.REQUEST_TYPE_ADD, bean);
                        break;

                    case AppConstants.OFFLINE:
                        doAddDelPregnancyTestLocal(0, AppConstants.REQUEST_TYPE_ADD, bean);
                        break;
                }
            });
        }
        else doAddDelPregnancyTestLocal(0, AppConstants.REQUEST_TYPE_ADD, bean);
    }

    /*Local DB Start here*/
    private void getPregnancyTestInfoFromDB() {
        setLoading(true);

        new Thread(() -> {
            list.clear();
            List<PregnancyTestInfoResponse.PregnancyListBean> pregnancyTestList = getDataManager().getPregnancyTestList(animalInfoBean.getAnimalId());

            list.addAll(pregnancyTestList);

            handler.post(() -> {
                setLoading(false);
                pregnancyTestAdapter.notifyDataSetChanged();
                updateUI();
            });
        }).start();
    }

    private void doAddAllPregnancyTestLocalDB(List<PregnancyTestInfoResponse.PregnancyListBean> infoResponse) {

        new Thread(() -> {
            getDataManager().deleteAllPregnancyTest();

            List<PregnancyTest> pregnancyTestList = new ArrayList<>();
            for (PregnancyTestInfoResponse.PregnancyListBean mainBean : infoResponse) {
                PregnancyTest pregnancyTest = new PregnancyTest();

                pregnancyTest.setPregnancyId(mainBean.getPregnancyId());
                pregnancyTest.setAnimal_id(mainBean.getAnimal_id());
                pregnancyTest.setPregnancyName(mainBean.getPregnancyName());
                pregnancyTest.setPregnancyDate(mainBean.getPregnancyDate());
                pregnancyTest.setPregnancyDescription(mainBean.getPregnancyDescription());
                pregnancyTest.setRecordKey(mainBean.getRecordKey());
                pregnancyTest.setCrd(mainBean.getCrd());
                pregnancyTest.setUpd(mainBean.getUpd());
                pregnancyTest.setDataSync(AppConstants.DB_SYNC_TRUE);
                pregnancyTest.setEvent(AppConstants.DB_EVENT_ADD);

                pregnancyTestList.add(pregnancyTest);

            }

            getDataManager().insertAllPregnancyTest(pregnancyTestList);
            AppLogger.d("database", "PregnancyTest add success");
        }).start();
    }

    private void doAddDelPregnancyTestLocal(int pos, String flag, PregnancyTestInfoResponse.PregnancyListBean bean) {
        new Thread(() -> {
            switch (flag) {
                case AppConstants.REQUEST_TYPE_ADD:
                    PregnancyTest pregnancyTest = new PregnancyTest();

                    pregnancyTest.setPregnancyId(bean.getRecordKey());  //primary key
                    pregnancyTest.setAnimal_id(animalInfoBean.getAnimalId());
                    pregnancyTest.setPregnancyName(bean.getPregnancyName());
                    pregnancyTest.setPregnancyDate(bean.getPregnancyDate());
                    pregnancyTest.setPregnancyDescription(bean.getPregnancyDescription());
                    pregnancyTest.setRecordKey(bean.getRecordKey());
                    pregnancyTest.setCrd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    pregnancyTest.setUpd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    pregnancyTest.setDataSync(AppConstants.DB_SYNC_FALSE);
                    pregnancyTest.setEvent(AppConstants.DB_EVENT_ADD);

                    getDataManager().insertPregnancyTest(pregnancyTest);

                    //set sync status true
                    getDataManager().setLocalDataExistForSync(true);

                    AppLogger.d("database", "PregnancyTest add success");

                    handler.post(() -> {
                        list.add(pos, bean);
                        pregnancyTestAdapter.notifyItemInserted(pos);
                        rvPregnancy.smoothScrollToPosition(pos);
                        updateUI();
                    });
                    break;

                case AppConstants.REQUEST_TYPE_DEL:
                    if (bean.getPregnancyId().equals(bean.getRecordKey())) {
                        getDataManager().deleteUsingPregnancyTestId(bean.getPregnancyId());
                    } else {
                        pregnancyTest = new PregnancyTest();

                        pregnancyTest.setPregnancyId(bean.getPregnancyId());  //primary key
                        pregnancyTest.setAnimal_id(animalInfoBean.getAnimalId());
                        pregnancyTest.setPregnancyName(bean.getPregnancyName());
                        pregnancyTest.setPregnancyDate(bean.getPregnancyDate());
                        pregnancyTest.setPregnancyDescription(bean.getPregnancyDescription());
                        pregnancyTest.setRecordKey(bean.getRecordKey());
                        pregnancyTest.setCrd(bean.getCrd());
                        pregnancyTest.setUpd(bean.getUpd());
                        pregnancyTest.setDataSync(AppConstants.DB_SYNC_FALSE);
                        pregnancyTest.setEvent(AppConstants.DB_EVENT_DEL);

                        getDataManager().updatePregnancyTest(pregnancyTest);

                        //set sync status true
                        getDataManager().setLocalDataExistForSync(true);
                    }
                    AppLogger.d("database", "PregnancyTest del success");

                    handler.post(() -> {
                        list.remove(pos);
                        pregnancyTestAdapter.notifyItemRemoved(pos);
                        updateUI();
                    });
                    break;
            }

        }).start();
    }

    private void updateLocalDB(String flag, PregnancyTestInfoResponse.PregnancyListBean bean) {
        new Thread(() -> {
            switch (flag) {
                case AppConstants.REQUEST_TYPE_ADD:
                    PregnancyTest pregnancyTest = new PregnancyTest();

                    pregnancyTest.setPregnancyId(bean.getPregnancyId());  //primary key
                    pregnancyTest.setAnimal_id(animalInfoBean.getAnimalId());
                    pregnancyTest.setPregnancyName(bean.getPregnancyName());
                    pregnancyTest.setPregnancyDate(bean.getPregnancyDate());
                    pregnancyTest.setPregnancyDescription(bean.getPregnancyDescription());
                    pregnancyTest.setRecordKey(bean.getRecordKey());
                    pregnancyTest.setCrd(bean.getCrd());
                    pregnancyTest.setUpd(bean.getUpd());
                    pregnancyTest.setDataSync(AppConstants.DB_SYNC_FALSE);
                    pregnancyTest.setEvent(AppConstants.DB_EVENT_ADD);

                    getDataManager().insertPregnancyTest(pregnancyTest);

                    //set sync status true
                    getDataManager().setLocalDataExistForSync(true);

                    AppLogger.d("database", "PregnancyTest add success");
                    break;

                case AppConstants.REQUEST_TYPE_DEL:
                    getDataManager().deleteUsingPregnancyTestId(bean.getPregnancyId());
                    AppLogger.d("database", "PregnancyTest del success");
                    break;
            }

        }).start();

    }
    /*Local DB Start here*/
}
