package com.agrinvestapp.ui.animal.fragment.treatments;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.TreatmentDetailResponse;
import com.agrinvestapp.data.model.api.TreatmentInfoResponse;
import com.agrinvestapp.data.model.db.Treatment;
import com.agrinvestapp.data.model.db.TreatmentMapping;
import com.agrinvestapp.ui.animal.fragment.treatments.adapter.TreatmentDetailAdapter;
import com.agrinvestapp.ui.animal.fragment.treatments.dialog.TreatmentDetailCallback;
import com.agrinvestapp.ui.animal.fragment.treatments.dialog.TreatmentsDialog;
import com.agrinvestapp.ui.base.BaseFragment;
import com.agrinvestapp.ui.base.dialog.DeleteDialog;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.AppLogger;
import com.agrinvestapp.utils.CalenderUtils;
import com.agrinvestapp.utils.CommonUtils;
import com.agrinvestapp.volley.VolleyRequest;
import com.agrinvestapp.volley.VolleyResponseListener;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TreatmentDetailFragment extends BaseFragment implements View.OnClickListener, BaseFragment.FragmentCallback, TreatmentDetailCallback {

    private static final String MODEL = "model";

    private TextView tvNoRecord;

    private TreatmentInfoResponse.TreatmentDetailBean treatmentsBean;
    private List<TreatmentDetailResponse.TreatmentDetailBean> list;
    private TreatmentDetailAdapter treatmentAdapter;
    private RecyclerView rvTreatmentDetail;
    private TreatmentFragment treatmentFragment;

    private Handler handler = new Handler(Looper.getMainLooper());

    public static TreatmentDetailFragment newInstance(TreatmentFragment treatmentFragment, TreatmentInfoResponse.TreatmentDetailBean treatmentDetailBean) {
        Bundle args = new Bundle();
        args.putParcelable(MODEL, treatmentDetailBean);
        TreatmentDetailFragment fragment = new TreatmentDetailFragment();
        fragment.setInstance(treatmentFragment);
        fragment.setArguments(args);
        return fragment;
    }

    private void setInstance(TreatmentFragment treatmentFragment) {
        this.treatmentFragment = treatmentFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            treatmentsBean = getArguments().getParcelable(MODEL);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_treatment_detail, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        /* toolbar view start */
        ImageView imgBack = view.findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);
        imgBack.setVisibility(View.VISIBLE);

        TextView tvTitle = view.findViewById(R.id.tvTitle);
        assert treatmentsBean != null;
        tvTitle.setText(treatmentsBean.getTreatmentName());

        FrameLayout frameAdd = view.findViewById(R.id.frameAddT);
        frameAdd.setVisibility(View.VISIBLE);
        frameAdd.setOnClickListener(this);
        /* toolbar view end */
        view.findViewById(R.id.rlMain).setOnClickListener(this); //avoid bg click
        tvNoRecord = view.findViewById(R.id.tvNoRecord);

        list = new ArrayList<>();

        rvTreatmentDetail = view.findViewById(R.id.rvTreatmentDetail);
        treatmentAdapter = new TreatmentDetailAdapter(list, new TreatmentDetailAdapter.ItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                //Not require
            }

            @Override
            public void onEditClick(int pos) {
                TreatmentsDialog treatmentDialog = new TreatmentsDialog();
                treatmentDialog.setOnTreatmentAddEditListener(pos, treatmentsBean, list.get(pos), TreatmentDetailFragment.this);

                treatmentDialog.show(getChildFragmentManager());
            }

            @Override
            public void onDeleteClick(int pos) {
                DeleteDialog.newInstance(false, "", () -> {
                    if (isNetworkConnected(true)) {
                        getBaseActivity().doSyncAppDB(false, flag -> {
                            switch (flag) {
                                case AppConstants.ONLINE:
                                    doAddTreatmentDetails(pos, AppConstants.REQUEST_TYPE_DEL, null);
                                    break;

                                case AppConstants.OFFLINE:
                                    TreatmentDetailResponse.TreatmentDetailBean treatmentDetailBean = null;
                                    doAddEditDelTreatmentLocalDB(pos, AppConstants.REQUEST_TYPE_DEL, treatmentDetailBean);
                                    break;
                            }
                        });
                    } else {
                        TreatmentDetailResponse.TreatmentDetailBean treatmentDetailBean = null;
                        doAddEditDelTreatmentLocalDB(pos, AppConstants.REQUEST_TYPE_DEL, treatmentDetailBean);
                    }
                }).show(getChildFragmentManager());
            }
        });

        rvTreatmentDetail.setAdapter(treatmentAdapter);

        updateTreatmentDetailList();
    }

    private void updateTreatmentDetailList() {
        new Thread(() -> {
            if (treatmentsBean != null && treatmentsBean.getTreatmentList() != null) {
                List<TreatmentDetailResponse.TreatmentDetailBean> tempList = new ArrayList<>();
                for (TreatmentInfoResponse.TreatmentDetailBean.TreatmentListBean tempBean : treatmentsBean.getTreatmentList()) {
                    TreatmentDetailResponse.TreatmentDetailBean beanList = new TreatmentDetailResponse.TreatmentDetailBean();
                    beanList.setTreatmentMappingId(tempBean.getTreatmentMappingId());
                    beanList.setTreatment_id(tempBean.getTreatment_id());
                    beanList.setTreatmentDate(tempBean.getTreatmentDate());
                    beanList.setTreatmentDescription(tempBean.getTreatmentDescription());
                    beanList.setRecordKey(tempBean.getRecordKey());
                    beanList.setCrd(tempBean.getCrd());
                    beanList.setUpd(tempBean.getUpd());

                    tempList.add(beanList);
                }

                if (!tempList.isEmpty()) {
                    list.clear();
                    list.addAll(tempList);
                }
            }

            handler.post(() -> {
                treatmentAdapter.notifyDataSetChanged();
                updateUi();
            });
        }).start();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                getBaseActivity().onBackPressed();
                break;

            case R.id.frameAddT:
                if (list.size() < 20) {
                    TreatmentsDialog treatmentDialog = new TreatmentsDialog();
                    treatmentDialog.setOnTreatmentAddEditListener(0, treatmentsBean, null, TreatmentDetailFragment.this);

                    treatmentDialog.show(getChildFragmentManager());
                } else
                    CommonUtils.showToast(getBaseActivity(), getString(R.string.treatmentDetailLimit), Toast.LENGTH_SHORT);
                break;
        }
    }

    private void doAddTreatmentDetails(int pos, String flag, TreatmentDetailResponse.TreatmentDetailBean treatmentDetailBean) {
        setLoading(true);
        HashMap<String, String> mParams = new HashMap<>();
        mParams.put("requestType", flag);
        mParams.put("recordKey", treatmentsBean.getRecordKey());

        switch (flag) {
            case AppConstants.REQUEST_TYPE_ADD:
                mParams.put("treatmentId", "");
                mParams.put("treatmentDate", treatmentDetailBean.getTreatmentDate());
                mParams.put("treatmentDescription", treatmentDetailBean.getTreatmentDescription());
                mParams.put("mappingRecordKey", CalenderUtils.getTimestamp());
                break;

            case AppConstants.REQUEST_TYPE_EDIT:
                mParams.put("treatmentId", list.get(pos).getTreatment_id());
                mParams.put("mappingRecordKey", list.get(pos).getRecordKey());
                mParams.put("treatmentDate", treatmentDetailBean.getTreatmentDate());
                mParams.put("treatmentDescription", treatmentDetailBean.getTreatmentDescription());
                break;

            case AppConstants.REQUEST_TYPE_DEL:
                mParams.put("treatmentId", list.get(pos).getTreatment_id());
                mParams.put("mappingRecordKey", list.get(pos).getRecordKey());
                mParams.put("treatmentDate", list.get(pos).getTreatmentDate());
                mParams.put("treatmentDescription", list.get(pos).getTreatmentDescription());
                break;
        }

        getDataManager().doServerTreatmentDetailAddEditDelApiCall(getDataManager().getHeader(), mParams)
                .getAsResponse(new VolleyResponseListener() {
                    @Override
                    public void onVolleyResponse(String response) {
                        setLoading(false);
                        TreatmentDetailResponse infoResponse = getDataManager().mGson.fromJson(response, TreatmentDetailResponse.class);

                        if (infoResponse.getStatus().equals("success")) {
                            switch (flag) {
                                case AppConstants.REQUEST_TYPE_ADD:
                                    list.add(pos, infoResponse.getTreatmentDetail());
                                    treatmentAdapter.notifyItemInserted(pos);
                                    rvTreatmentDetail.smoothScrollToPosition(0);
                                    CommonUtils.showToast(getBaseActivity(), infoResponse.getMessage(), Toast.LENGTH_SHORT);
                                    //Todo add to local db
                                    doAddEditDelTreatmentLocalDB(pos, flag, list);
                                    break;

                                case AppConstants.REQUEST_TYPE_EDIT:
                                    list.set(pos, infoResponse.getTreatmentDetail());
                                    treatmentAdapter.notifyItemChanged(pos);
                                    CommonUtils.showToast(getBaseActivity(), infoResponse.getMessage(), Toast.LENGTH_SHORT);
                                    //Todo update to local db
                                    doAddEditDelTreatmentLocalDB(pos, flag, list);
                                    break;

                                case AppConstants.REQUEST_TYPE_DEL:
                                    //Todo del to local db
                                    doAddEditDelTreatmentLocalDB(pos, flag, list);
                                    CommonUtils.showToast(getBaseActivity(), infoResponse.getMessage(), Toast.LENGTH_SHORT);
                                    break;
                            }
                            updateUi();

                        } else {
                            updateUi();
                            if (infoResponse.getMessage().equals(getString(R.string.no_record_found))) {
                                list.clear();
                                treatmentAdapter.notifyDataSetChanged();
                            }
                            if (!infoResponse.getMessage().equals(getString(R.string.no_record_found)))
                                CommonUtils.showToast(getBaseActivity(), infoResponse.getMessage(), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    private void updateUi() {
        tvNoRecord.setVisibility(list.isEmpty() ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onAdd(TreatmentDetailResponse.TreatmentDetailBean treatmentDetailBean) {
        if (isNetworkConnected(true)) {
            getBaseActivity().doSyncAppDB(false, flag -> {
                switch (flag) {
                    case AppConstants.ONLINE:
                        doAddTreatmentDetails(0, AppConstants.REQUEST_TYPE_ADD, treatmentDetailBean);
                        break;

                    case AppConstants.OFFLINE:
                        doAddEditDelTreatmentLocalDB(0, AppConstants.REQUEST_TYPE_ADD, treatmentDetailBean);
                        break;
                }
            });
        } else doAddEditDelTreatmentLocalDB(0, AppConstants.REQUEST_TYPE_ADD, treatmentDetailBean);
    }

    @Override
    public void onEdit(int pos, TreatmentDetailResponse.TreatmentDetailBean treatmentDetailBean) {
        if (isNetworkConnected(true)) {
            getBaseActivity().doSyncAppDB(false, flag -> {
                switch (flag) {
                    case AppConstants.ONLINE:
                        doAddTreatmentDetails(pos, AppConstants.REQUEST_TYPE_EDIT, treatmentDetailBean);
                        break;

                    case AppConstants.OFFLINE:
                        doAddEditDelTreatmentLocalDB(pos, AppConstants.REQUEST_TYPE_EDIT, treatmentDetailBean);
                        break;
                }
            });
        }
        else doAddEditDelTreatmentLocalDB(pos, AppConstants.REQUEST_TYPE_EDIT, treatmentDetailBean);
    }

    @Override
    public void onBackPress() {
        if (treatmentFragment != null) treatmentFragment.hitTreatmentApi();
    }

    /*Local DB Start here*/
    private void doAddEditDelTreatmentLocalDB(int pos, String flag, List<TreatmentDetailResponse.TreatmentDetailBean> list) {
        new Thread(() -> {
            TreatmentMapping treatmentMapping = new TreatmentMapping();
            switch (flag) {
                case AppConstants.REQUEST_TYPE_ADD:
                    TreatmentDetailResponse.TreatmentDetailBean tempBean = list.get(pos);

                    treatmentMapping.setTreatmentMappingId(tempBean.getTreatmentMappingId());
                    treatmentMapping.setTreatment_id(tempBean.getTreatment_id());
                    treatmentMapping.setTreatmentDate(tempBean.getTreatmentDate());
                    treatmentMapping.setTreatmentFor(treatmentsBean.getTreatmentFor());
                    treatmentMapping.setTreatmentDescription(tempBean.getTreatmentDescription());
                    treatmentMapping.setCrd(tempBean.getCrd());
                    treatmentMapping.setUpd(tempBean.getUpd());
                    treatmentMapping.setDataSync(AppConstants.DB_SYNC_TRUE);
                    treatmentMapping.setEvent(AppConstants.DB_EVENT_ADD);
                    getDataManager().insertTreatmentDetail(treatmentMapping);
                    AppLogger.d("database", "treatment detail add success");
                    break;

                case AppConstants.REQUEST_TYPE_EDIT:
                    tempBean = list.get(pos);

                    treatmentMapping.setTreatmentMappingId(tempBean.getTreatmentMappingId());
                    treatmentMapping.setTreatment_id(tempBean.getTreatment_id());
                    treatmentMapping.setTreatmentDate(tempBean.getTreatmentDate());
                    treatmentMapping.setTreatmentFor(treatmentsBean.getTreatmentFor());
                    treatmentMapping.setTreatmentDescription(tempBean.getTreatmentDescription());
                    treatmentMapping.setCrd(tempBean.getCrd());
                    treatmentMapping.setUpd(tempBean.getUpd());
                    treatmentMapping.setDataSync(AppConstants.DB_SYNC_TRUE);
                    treatmentMapping.setEvent(AppConstants.DB_EVENT_EDIT);

                    getDataManager().updateTreatmentDetail(treatmentMapping);
                    AppLogger.d("database", "treatment detail update success");
                    break;

                case AppConstants.REQUEST_TYPE_DEL:
                    tempBean = list.get(pos);
                    getDataManager().deleteUsingTreatmentDetailMappingId(tempBean.getTreatmentMappingId());

                    handler.post(() -> {
                        list.remove(pos);
                        treatmentAdapter.notifyItemRemoved(pos);
                        updateUi();
                    });
                    AppLogger.d("database", "treatment detail delete success");
                    break;
            }

        }).start();
    }

    private void doAddEditDelTreatmentLocalDB(int pos, String flag, TreatmentDetailResponse.TreatmentDetailBean mainBean) {
//Todo check this
        new Thread(() -> {
            TreatmentMapping treatmentMapping = new TreatmentMapping();

            switch (flag){
                case AppConstants.REQUEST_TYPE_ADD:

                    //for ui update
                    TreatmentDetailResponse.TreatmentDetailBean tdlBean = new TreatmentDetailResponse.TreatmentDetailBean();
                    tdlBean.setTreatmentMappingId(mainBean.getRecordKey());
                    tdlBean.setTreatment_id(treatmentsBean.getTreatmentId());
                    tdlBean.setTreatmentDate(mainBean.getTreatmentDate());
                    tdlBean.setTreatmentDescription(mainBean.getTreatmentDescription());
                    tdlBean.setRecordKey(mainBean.getRecordKey());
                    tdlBean.setCrd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    tdlBean.setUpd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));

                    list.add(pos, tdlBean);
                    handler.post(() -> {
                        treatmentAdapter.notifyItemInserted(pos);
                        rvTreatmentDetail.smoothScrollToPosition(0);
                        updateUi();
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.treatmentAddSuccessMsg), Toast.LENGTH_SHORT);
                    });

                    treatmentMapping.setTreatmentMappingId(mainBean.getRecordKey());
                    treatmentMapping.setTreatment_id(treatmentsBean.getTreatmentId());
                    treatmentMapping.setTreatmentDate(mainBean.getTreatmentDate());
                    treatmentMapping.setTreatmentFor(treatmentsBean.getTreatmentFor());
                    treatmentMapping.setTreatmentDescription(mainBean.getTreatmentDescription());
                    treatmentMapping.setRecordKey(mainBean.getRecordKey());
                    treatmentMapping.setCrd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    treatmentMapping.setUpd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    treatmentMapping.setDataSync(AppConstants.DB_SYNC_FALSE);
                    treatmentMapping.setEvent(AppConstants.DB_EVENT_ADD);
                    getDataManager().insertTreatmentDetail(treatmentMapping);

                    //update treatment flag
                    Treatment treatment = getDataManager().getTreatmentInfo(treatmentsBean.getTreatmentId());
                    treatment.setDataSync(AppConstants.DB_SYNC_FALSE);
                    treatment.setEvent((treatmentsBean.getTreatmentId().equals(treatment.getRecordKey())) ? AppConstants.DB_EVENT_ADD : AppConstants.DB_EVENT_EDIT);
                    getDataManager().updateTreatment(treatment);

                    //set sync status true
                    getDataManager().setLocalDataExistForSync(true);
                    AppLogger.d("database", "treatment detail add success");
                    break;

                case AppConstants.REQUEST_TYPE_EDIT:
                    TreatmentDetailResponse.TreatmentDetailBean myBean = list.get(pos);

                    //for ui update
                    tdlBean = new TreatmentDetailResponse.TreatmentDetailBean();
                    tdlBean.setTreatmentMappingId(myBean.getTreatmentMappingId());
                    tdlBean.setTreatment_id(myBean.getTreatment_id());
                    tdlBean.setTreatmentDate(mainBean.getTreatmentDate());
                    tdlBean.setTreatmentDescription(mainBean.getTreatmentDescription());
                    tdlBean.setRecordKey(myBean.getRecordKey());
                    tdlBean.setCrd(myBean.getCrd());
                    tdlBean.setUpd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));

                    list.set(pos, tdlBean);
                    handler.post(() -> {
                        treatmentAdapter.notifyItemChanged(pos);
                        updateUi();
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.treatmentUpdateSuccessMsg), Toast.LENGTH_SHORT);
                    });

                    treatmentMapping.setTreatmentMappingId(myBean.getTreatmentMappingId());
                    treatmentMapping.setTreatment_id(myBean.getTreatment_id());
                    treatmentMapping.setTreatmentDate(mainBean.getTreatmentDate());
                    treatmentMapping.setTreatmentFor(treatmentsBean.getTreatmentFor());
                    treatmentMapping.setTreatmentDescription(mainBean.getTreatmentDescription());
                    treatmentMapping.setRecordKey(myBean.getRecordKey());
                    treatmentMapping.setCrd(myBean.getCrd());
                    treatmentMapping.setUpd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    treatmentMapping.setDataSync(AppConstants.DB_SYNC_FALSE);
                    treatmentMapping.setEvent(myBean.getTreatmentMappingId().equals(myBean.getRecordKey()) ? AppConstants.DB_EVENT_ADD : AppConstants.DB_EVENT_EDIT);
                    getDataManager().updateTreatmentDetail(treatmentMapping);

                    //update treatment flag
                    treatment = getDataManager().getTreatmentInfo(myBean.getTreatment_id());
                    treatment.setDataSync(AppConstants.DB_SYNC_FALSE);
                    treatment.setEvent((myBean.getTreatment_id().equals(treatment.getRecordKey())) ? AppConstants.DB_EVENT_ADD : AppConstants.DB_EVENT_EDIT);
                    getDataManager().updateTreatment(treatment);

                    //set sync status true
                    getDataManager().setLocalDataExistForSync(true);
                    AppLogger.d("database", "treatment detail update success");
                    break;

                case AppConstants.REQUEST_TYPE_DEL:
                    myBean = list.get(pos);

                    if (myBean.getTreatmentMappingId().equals(myBean.getRecordKey())) {
                        getDataManager().deleteUsingTreatmentDetailMappingId(myBean.getTreatmentMappingId());
                    } else {
                        treatmentMapping.setTreatmentMappingId(myBean.getTreatmentMappingId());
                        treatmentMapping.setTreatment_id(myBean.getTreatment_id());
                        treatmentMapping.setTreatmentDate(myBean.getTreatmentDate());
                        treatmentMapping.setTreatmentFor(treatmentsBean.getTreatmentFor());
                        treatmentMapping.setTreatmentDescription(myBean.getTreatmentDescription());
                        treatmentMapping.setRecordKey(myBean.getRecordKey());
                        treatmentMapping.setCrd(myBean.getCrd());
                        treatmentMapping.setUpd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                        treatmentMapping.setDataSync(AppConstants.DB_SYNC_FALSE);
                        treatmentMapping.setEvent(AppConstants.DB_EVENT_DEL);
                        getDataManager().updateTreatmentDetail(treatmentMapping);


                        //update treatment flag
                        treatment = getDataManager().getTreatmentInfo(myBean.getTreatment_id());
                        treatment.setDataSync(AppConstants.DB_SYNC_FALSE);
                        treatment.setEvent((myBean.getTreatment_id().equals(treatment.getRecordKey())) ? AppConstants.DB_EVENT_ADD : AppConstants.DB_EVENT_EDIT);
                        getDataManager().updateTreatment(treatment);

                        //set sync status true
                        getDataManager().setLocalDataExistForSync(true);
                    }
                    AppLogger.d("database", "treatment detail delete success");

                    handler.post(() -> {
                        list.remove(pos);
                        treatmentAdapter.notifyItemRemoved(pos);
                        updateUi();
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.treatmentDelSuccessMsg), Toast.LENGTH_SHORT);
                    });
                    break;
            }

        }).start();
    }
/*Local DB End here*/
}
