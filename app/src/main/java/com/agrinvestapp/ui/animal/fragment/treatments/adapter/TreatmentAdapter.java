package com.agrinvestapp.ui.animal.fragment.treatments.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.TreatmentInfoResponse;
import com.agrinvestapp.utils.pagination.FooterLoader;
import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chauthai.swipereveallayout.ViewBinderHelper;

import java.util.List;


/**
 * Created by hemant
 * Date: 17/4/18
 * Time: 4:03 PM
 */

public class TreatmentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEWTYPE_ITEM = 1;
    private final int VIEWTYPE_LOADER = 2;
    // This object helps you save/restore the open/close state of each view
    private final ViewBinderHelper viewBinderHelper = new ViewBinderHelper();
    private List<TreatmentInfoResponse.TreatmentDetailBean> list;
    private boolean showLoader;
    private ItemClickListener itemClickListener;


    public TreatmentAdapter(List<TreatmentInfoResponse.TreatmentDetailBean> list, ItemClickListener itemClickListener) {
        this.list = list;
        this.itemClickListener = itemClickListener;
        viewBinderHelper.setOpenOnlyOne(true);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder rvHolder, int position) {
        if (rvHolder instanceof FooterLoader) {
            FooterLoader loaderViewHolder = (FooterLoader) rvHolder;
            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }
            return;
        }
        TreatmentAdapter.ViewHolder holder = ((TreatmentAdapter.ViewHolder) rvHolder);

        viewBinderHelper.bind(holder.swipe, list.get(position).getTreatmentId());

        if (!list.get(position).isSelected) {
            holder.swipe.close(true);
        }
        holder.tvName.setText(list.get(position).getTreatmentName());
    }

    public void showLoading(boolean status) {
        showLoader = status;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case VIEWTYPE_ITEM:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_treatment, parent, false);
                return new ViewHolder(view);

            case VIEWTYPE_LOADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pagination_item_loader, parent, false);
                return new FooterLoader(view);
        }
        return null;

    }

    @Override
    public int getItemViewType(int position) {
        if (position == list.size() - 1) {
            return showLoader ? VIEWTYPE_LOADER : VIEWTYPE_ITEM;
        }
        return VIEWTYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface ItemClickListener {
        void onItemClick(int pos);

        void onEditClick(int pos);

        void onDeleteClick(int pos);
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvName;
        private FrameLayout frameEdit, frameDelete;
        private SwipeRevealLayout swipe;

        ViewHolder(@NonNull View v) {
            super(v);
            tvName = v.findViewById(R.id.tvName);
            swipe = v.findViewById(R.id.swipe);
            frameEdit = v.findViewById(R.id.frameEdit);
            frameDelete = v.findViewById(R.id.frameDelete);

            v.findViewById(R.id.rlMain).setOnClickListener(this);
            frameEdit.setOnClickListener(this);
            frameDelete.setOnClickListener(this);

        }

        @Override
        public void onClick(@NonNull final View view) {

            switch (view.getId()) {
                case R.id.frameEdit:
                    if (getAdapterPosition() != -1) {
                        swipe.close(true);
                        itemClickListener.onEditClick(getAdapterPosition());
                    }
                    break;

                case R.id.frameDelete:
                    if (getAdapterPosition() != -1) {
                        swipe.close(true);
                        itemClickListener.onDeleteClick(getAdapterPosition());
                    }
                    break;

                case R.id.rlMain:
                    if (getAdapterPosition() != -1) {
                        itemClickListener.onItemClick(getAdapterPosition());
                    }
                    break;
            }
        }
    }

}

