package com.agrinvestapp.ui.animal.fragment.treatments;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.AnimalInfoResponse;
import com.agrinvestapp.data.model.api.TreatmentInfoResponse;
import com.agrinvestapp.data.model.db.Treatment;
import com.agrinvestapp.data.model.db.TreatmentMapping;
import com.agrinvestapp.ui.animal.fragment.treatments.adapter.TreatmentAdapter;
import com.agrinvestapp.ui.animal.fragment.treatments.dialog.TreatmentCallback;
import com.agrinvestapp.ui.animal.fragment.treatments.dialog.TreatmentNameDialog;
import com.agrinvestapp.ui.base.BaseFragment;
import com.agrinvestapp.ui.base.dialog.DeleteDialog;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.AppLogger;
import com.agrinvestapp.utils.CalenderUtils;
import com.agrinvestapp.utils.CommonUtils;
import com.agrinvestapp.volley.VolleyJsonObjectListener;
import com.agrinvestapp.volley.VolleyRequest;
import com.agrinvestapp.volley.VolleyResponseListener;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TreatmentFragment extends BaseFragment implements View.OnClickListener, TreatmentCallback {

    private TextView tvNoRecord;

    private AnimalInfoResponse.AnimalListBean animalInfoBean;
    private List<TreatmentInfoResponse.TreatmentDetailBean> list;
    private TreatmentAdapter treatmentAdapter;
    private RecyclerView rvTreatments;
    private Handler handler = new Handler(Looper.getMainLooper());

    public static TreatmentFragment newInstance(AnimalInfoResponse.AnimalListBean animalInfoBean) {

        Bundle args = new Bundle();

        args.putParcelable(AppConstants.ANIMAL_MODEL, animalInfoBean);
        TreatmentFragment fragment = new TreatmentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            animalInfoBean = getArguments().getParcelable(AppConstants.ANIMAL_MODEL);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_treatments, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        /* toolbar view start */
        ImageView imgBack = view.findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);
        imgBack.setVisibility(View.VISIBLE);

        TextView tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.treatments);

        FrameLayout frameAdd = view.findViewById(R.id.frameAddT);
        frameAdd.setVisibility(View.VISIBLE);
        frameAdd.setOnClickListener(this);
        /* toolbar view end */
        view.findViewById(R.id.rlMain).setOnClickListener(this); //avoid bg click
        tvNoRecord = view.findViewById(R.id.tvNoRecord);

        list = new ArrayList<>();

        rvTreatments = view.findViewById(R.id.rvTreatments);
        treatmentAdapter = new TreatmentAdapter(list, new TreatmentAdapter.ItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                getBaseActivity().addFragment(TreatmentDetailFragment.newInstance(TreatmentFragment.this, list.get(pos)), R.id.animalFrame, true);
            }

            @Override
            public void onEditClick(int pos) {
                TreatmentNameDialog treatmentDialog = new TreatmentNameDialog();
                treatmentDialog.setOnTreatmentAddEditListener(pos, list.get(pos).getTreatmentName(), TreatmentFragment.this);

                treatmentDialog.show(getChildFragmentManager());
            }

            @Override
            public void onDeleteClick(int pos) {
                DeleteDialog.newInstance(false, "", () -> {
                    if (isNetworkConnected(true)) {
                        getBaseActivity().doSyncAppDB(false, flag -> {
                            switch (flag) {
                                case AppConstants.ONLINE:
                                    doAddEditDelTreatmentApi(pos, AppConstants.REQUEST_TYPE_DEL, list.get(pos).getTreatmentName(), list.get(pos).getRecordKey());
                                    break;

                                case AppConstants.OFFLINE:
                                    doAddEditDelTreatmentLocalDB(pos, AppConstants.DB_EVENT_DEL, "");
                                    break;
                            }
                        });
                    } else {
                        doAddEditDelTreatmentLocalDB(pos, AppConstants.DB_EVENT_DEL, "");
                    }
                }).show(getChildFragmentManager());
            }
        });

        rvTreatments.setAdapter(treatmentAdapter);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hitTreatmentApi();
    }

    public void hitTreatmentApi() {
        if (isNetworkConnected(true)) {
            getBaseActivity().doSyncAppDB(false, flag -> {
                switch (flag) {
                    case AppConstants.ONLINE:
                        getTreatmentInfo();
                        break;

                    case AppConstants.OFFLINE:
                        getTreatmentInfoFromDB();
                        break;
                }
            });
        } else {
            getTreatmentInfoFromDB();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                getBaseActivity().onBackPressed();
                break;

            case R.id.frameAddT:
                if (list.size() < 10) {
                    TreatmentNameDialog treatmentDialog = new TreatmentNameDialog();
                    treatmentDialog.setOnTreatmentAddEditListener(0, "", TreatmentFragment.this);

                    treatmentDialog.show(getChildFragmentManager());
                } else
                    CommonUtils.showToast(getBaseActivity(), getString(R.string.treatmentLimit), Toast.LENGTH_SHORT);

                break;
        }
    }

    private void getTreatmentInfo() {
        setLoading(true);

        HashMap<String, String> mParameterMap = new HashMap<>();
        mParameterMap.put("requestType", AppConstants.REQUEST_TYPE_TREAT);
        mParameterMap.put("animalId", animalInfoBean.getAnimalId());
        mParameterMap.put("recordKey", animalInfoBean.getRecordKey());
        getDataManager().doServerTreatmentInfoListApiCall(getDataManager().getHeader(), mParameterMap)
                .getAsResponse(new VolleyResponseListener() {
                    @Override
                    public void onVolleyResponse(String response) {
                        setLoading(false);
                        TreatmentInfoResponse infoResponse = getDataManager().mGson.fromJson(response, TreatmentInfoResponse.class);

                        if (infoResponse.getStatus().equals("success")) {
                            list.clear();
                            list.addAll(infoResponse.getTreatmentDetail());

                            updateUi();
                            if (!list.isEmpty()) {
                                //Todo manage local db add all data
                                doAddTreatmentsLocalDB(infoResponse);
                            }

                            treatmentAdapter.notifyDataSetChanged();
                        } else {
                            updateUi();
                            if (infoResponse.getMessage().equals(getString(R.string.no_record_found))) {
                                list.clear();
                                treatmentAdapter.notifyDataSetChanged();
                            }
                            if (!infoResponse.getMessage().equals(getString(R.string.no_record_found)))
                                CommonUtils.showToast(getBaseActivity(), infoResponse.getMessage(), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        updateUi();
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    private void doAddEditDelTreatmentApi(int pos, String requestType, String name, String treatRk) {
        setLoading(true);

        HashMap<String, String> mParameterMap = new HashMap<>();
        mParameterMap.put("requestType", requestType);
        mParameterMap.put("animalId", animalInfoBean.getAnimalId());
        mParameterMap.put("recordKey", animalInfoBean.getRecordKey());
        mParameterMap.put("treatmentName", name);
        mParameterMap.put("treatmentRecordKey", treatRk);

        getDataManager().doServerTreatmentAddEditDelApiCall(getDataManager().getHeader(), mParameterMap)
                .getAsJsonObject(new VolleyJsonObjectListener() {

                    @Override
                    public void onVolleyJsonObject(JSONObject jsonObject) {
                        setLoading(false);
                        try {
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            if (status.equals("success")) {
                                switch (requestType) {
                                    case AppConstants.DB_EVENT_ADD:
                                        JSONObject obj = jsonObject.getJSONObject("treatmentDetail");
                                        TreatmentInfoResponse.TreatmentDetailBean bean = new TreatmentInfoResponse.TreatmentDetailBean();
                                        bean.setTreatmentId(obj.getString("treatmentId"));
                                        bean.setTreatmentName(obj.getString("treatmentName"));
                                        bean.setTreatmentForId(obj.getString("treatmentForId"));
                                        bean.setTreatmentFor(obj.getString("treatmentFor"));
                                        bean.setRecordKey(obj.getString("recordKey"));
                                        bean.setCrd(obj.getString("crd"));
                                        bean.setUpd(obj.getString("upd"));
                                        list.add(pos, bean);
                                        treatmentAdapter.notifyItemInserted(pos);
                                        rvTreatments.smoothScrollToPosition(0);
                                        //Todo add to local db
                                        doAddEditDelTreatmentLocalDB(requestType, list.get(pos));

                                        CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                                        break;

                                    case AppConstants.DB_EVENT_EDIT:
                                        JSONObject obj2 = jsonObject.getJSONObject("treatmentDetail");
                                        TreatmentInfoResponse.TreatmentDetailBean bean2 = new TreatmentInfoResponse.TreatmentDetailBean();
                                        bean2.setTreatmentId(obj2.getString("treatmentId"));
                                        bean2.setTreatmentName(obj2.getString("treatmentName"));
                                        bean2.setTreatmentForId(obj2.getString("treatmentForId"));
                                        bean2.setTreatmentFor(obj2.getString("treatmentFor"));
                                        bean2.setRecordKey(obj2.getString("recordKey"));
                                        bean2.setCrd(obj2.getString("crd"));
                                        bean2.setUpd(obj2.getString("upd"));

                                        bean2.setTreatmentList(list.get(pos).getTreatmentList());
                                        list.set(pos, bean2);
                                        treatmentAdapter.notifyItemChanged(pos);
                                        //Todo update to local db
                                        doAddEditDelTreatmentLocalDB(requestType, list.get(pos));

                                        CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                                        break;

                                    case AppConstants.DB_EVENT_DEL:
                                        //Todo del to local db
                                        doAddEditDelTreatmentLocalDB(requestType, list.get(pos));

                                        list.remove(pos);
                                        treatmentAdapter.notifyItemRemoved(pos);

                                        CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                                        break;
                                }

                                updateUi();
                            } else {
                                CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                            }
                        } catch (Exception e) {
                            CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyJsonException() {
                        setLoading(false);
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    private void updateUi() {
        tvNoRecord.setVisibility(list.isEmpty() ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onAdd(String name) {
        if (isNetworkConnected(true)) {
            getBaseActivity().doSyncAppDB(false, flag -> {
                switch (flag) {
                    case AppConstants.ONLINE:
                        doAddEditDelTreatmentApi(0, AppConstants.REQUEST_TYPE_ADD, name, CalenderUtils.getTimestamp());
                        break;

                    case AppConstants.OFFLINE:
                        doAddEditDelTreatmentLocalDB(0, AppConstants.DB_EVENT_ADD, name);
                        break;
                }
            });
        } else {
            doAddEditDelTreatmentLocalDB(0, AppConstants.DB_EVENT_ADD, name);
        }
    }

    @Override
    public void onEdit(int pos, String name) {
        if (isNetworkConnected(true)) {
            getBaseActivity().doSyncAppDB(false, flag -> {
                switch (flag) {
                    case AppConstants.ONLINE:
                        doAddEditDelTreatmentApi(pos, AppConstants.REQUEST_TYPE_EDIT, name, list.get(pos).getRecordKey());
                        break;

                    case AppConstants.OFFLINE:
                        doAddEditDelTreatmentLocalDB(pos, AppConstants.DB_EVENT_EDIT, name);
                        break;
                }
            });
        } else {
            doAddEditDelTreatmentLocalDB(pos, AppConstants.DB_EVENT_EDIT, name);
        }
    }

    /*Local DB Start here*/
    private void getTreatmentInfoFromDB() {
        setLoading(true);

        new Thread(() -> {
            list.clear();
            List<TreatmentInfoResponse.TreatmentDetailBean> treatmentList = getDataManager().getTreatmentList(animalInfoBean.getAnimalId());

            List<TreatmentInfoResponse.TreatmentDetailBean> tempList = new ArrayList<>();
            for (TreatmentInfoResponse.TreatmentDetailBean tempBean : treatmentList) {
                List<TreatmentInfoResponse.TreatmentDetailBean.TreatmentListBean> treatmentMappingList = getDataManager().getTreatmentDetailList(tempBean.getTreatmentId());
                tempBean.setTreatmentList(treatmentMappingList);
                tempList.add(tempBean);
            }
            list.addAll(tempList);

            handler.post(() -> {
                setLoading(false);
                treatmentAdapter.notifyDataSetChanged();
                updateUi();
            });
        }).start();
    }

    private void doAddTreatmentsLocalDB(TreatmentInfoResponse infoResponse) {
        new Thread(() -> {
            getDataManager().deleteAllTreatment();
            getDataManager().deleteAllTreatmentDetail();

            List<Treatment> treatmentList = new ArrayList<>();
            List<TreatmentMapping> treatmentMappingList = new ArrayList<>();
            for (TreatmentInfoResponse.TreatmentDetailBean mainBean : infoResponse.getTreatmentDetail()) {
                Treatment treatment = new Treatment();

                treatment.setTreatmentId(mainBean.getTreatmentId());
                treatment.setTreatmentName(mainBean.getTreatmentName());
                treatment.setTreatmentForId(mainBean.getTreatmentForId());
                treatment.setTreatmentFor(mainBean.getTreatmentFor());
                treatment.setRecordKey(mainBean.getRecordKey());
                treatment.setCrd(mainBean.getCrd());
                treatment.setUpd(mainBean.getUpd());
                treatment.setDataSync(AppConstants.DB_SYNC_TRUE);
                treatment.setEvent(AppConstants.DB_EVENT_ADD);

                treatmentList.add(treatment);

                for (TreatmentInfoResponse.TreatmentDetailBean.TreatmentListBean subBean : mainBean.getTreatmentList()) {
                    TreatmentMapping treatmentMapping = new TreatmentMapping();

                    treatmentMapping.setTreatmentMappingId(subBean.getTreatmentMappingId());
                    treatmentMapping.setTreatment_id(subBean.getTreatment_id());
                    treatmentMapping.setTreatmentDate(subBean.getTreatmentDate());
                    treatmentMapping.setTreatmentFor(mainBean.getTreatmentFor());
                    treatmentMapping.setTreatmentDescription(subBean.getTreatmentDescription());
                    treatmentMapping.setCrd(subBean.getCrd());
                    treatmentMapping.setUpd(subBean.getUpd());
                    treatmentMapping.setDataSync(AppConstants.DB_SYNC_TRUE);
                    treatmentMapping.setEvent(AppConstants.DB_EVENT_ADD);

                    treatmentMappingList.add(treatmentMapping);
                }
            }

            getDataManager().insertAllTreatment(treatmentList);
            getDataManager().insertAllTreatmentDetail(treatmentMappingList);
            AppLogger.d("database", "treatment and treatment detail success");
        }).start();
    }

    private void doAddEditDelTreatmentLocalDB(String flag, TreatmentInfoResponse.TreatmentDetailBean mainBean) {
        new Thread(() -> {

            switch (flag) {
                case AppConstants.REQUEST_TYPE_ADD:
                    Treatment treatment = new Treatment();
                    treatment.setTreatmentId(mainBean.getTreatmentId());
                    treatment.setTreatmentName(mainBean.getTreatmentName());
                    treatment.setTreatmentForId(mainBean.getTreatmentForId());
                    treatment.setTreatmentFor(mainBean.getTreatmentFor());
                    treatment.setRecordKey(mainBean.getRecordKey());
                    treatment.setCrd(mainBean.getCrd());
                    treatment.setUpd(mainBean.getUpd());
                    treatment.setDataSync(AppConstants.DB_SYNC_TRUE);
                    treatment.setEvent(AppConstants.DB_EVENT_ADD);

                    getDataManager().insertTreatment(treatment);
                    AppLogger.d("database", "Treament add success");
                    break;

                case AppConstants.REQUEST_TYPE_EDIT:
                    treatment = new Treatment();
                    treatment.setTreatmentId(mainBean.getTreatmentId());
                    treatment.setTreatmentName(mainBean.getTreatmentName());
                    treatment.setTreatmentForId(mainBean.getTreatmentForId());
                    treatment.setTreatmentFor(mainBean.getTreatmentFor());
                    treatment.setRecordKey(mainBean.getRecordKey());
                    treatment.setCrd(mainBean.getCrd());
                    treatment.setUpd(mainBean.getUpd());
                    treatment.setDataSync(AppConstants.DB_SYNC_TRUE);
                    treatment.setEvent(AppConstants.DB_EVENT_EDIT);

                    getDataManager().updateTreatment(treatment);
                    AppLogger.d("database", "Treament update success");
                    break;

                case AppConstants.REQUEST_TYPE_DEL:
                    getDataManager().deleteUsingTreatmentId(mainBean.getTreatmentId());
                    AppLogger.d("database", "Treament delete success");
                    break;
            }
        }).start();
    }

    private void doAddEditDelTreatmentLocalDB(int pos, String flag, String name) {
        new Thread(() -> {
            switch (flag) {
                case AppConstants.REQUEST_TYPE_ADD:
                    String timestamp = CalenderUtils.getTimestamp();
                    Treatment treatment = new Treatment();
                    treatment.setTreatmentId(timestamp);
                    treatment.setTreatmentName(name);
                    treatment.setTreatmentForId(animalInfoBean.getAnimalId());
                    treatment.setTreatmentFor(AppConstants.TREATMENT_FOR_ANIMAL);
                    treatment.setRecordKey(timestamp);
                    treatment.setCrd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    treatment.setUpd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    treatment.setDataSync(AppConstants.DB_SYNC_FALSE);
                    treatment.setEvent(AppConstants.DB_EVENT_ADD);

                    getDataManager().insertTreatment(treatment);
                    AppLogger.d("database", "Treament add success");

                    TreatmentInfoResponse.TreatmentDetailBean bean = new TreatmentInfoResponse.TreatmentDetailBean();
                    bean.setTreatmentId(timestamp);
                    bean.setTreatmentName(name);
                    bean.setTreatmentForId(animalInfoBean.getAnimalId());
                    bean.setTreatmentFor(AppConstants.TREATMENT_FOR_ANIMAL);
                    bean.setRecordKey(timestamp);
                    bean.setCrd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    bean.setUpd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    list.add(pos, bean);
                    handler.post(() -> {
                        treatmentAdapter.notifyItemInserted(pos);
                        rvTreatments.smoothScrollToPosition(0);
                        updateUi();
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.treatmentAddSuccessMsg), Toast.LENGTH_SHORT);
                    });

                    //set sync status true
                    getDataManager().setLocalDataExistForSync(true);
                    break;

                case AppConstants.REQUEST_TYPE_EDIT:
                    treatment = new Treatment();
                    TreatmentInfoResponse.TreatmentDetailBean tempBean = list.get(pos);
                    treatment.setTreatmentId(tempBean.getTreatmentId());
                    treatment.setTreatmentName(name);
                    treatment.setTreatmentForId(animalInfoBean.getAnimalId());
                    treatment.setTreatmentFor(AppConstants.TREATMENT_FOR_ANIMAL);
                    treatment.setRecordKey(tempBean.getRecordKey());
                    treatment.setCrd(tempBean.getCrd());
                    treatment.setUpd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    treatment.setDataSync(AppConstants.DB_SYNC_FALSE);
                    treatment.setEvent(tempBean.getTreatmentId().equals(tempBean.getRecordKey()) ? AppConstants.DB_EVENT_ADD : AppConstants.DB_EVENT_EDIT);

                    getDataManager().updateTreatment(treatment);
                    AppLogger.d("database", "Treament update success");

                    bean = new TreatmentInfoResponse.TreatmentDetailBean();
                    bean.setTreatmentId(tempBean.getTreatmentId());
                    bean.setTreatmentName(name);
                    bean.setTreatmentForId(animalInfoBean.getAnimalId());
                    bean.setTreatmentFor(AppConstants.TREATMENT_FOR_ANIMAL);
                    bean.setRecordKey(tempBean.getRecordKey());
                    bean.setCrd(tempBean.getCrd());
                    bean.setUpd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    list.set(pos, bean);
                    handler.post(() -> {
                        treatmentAdapter.notifyItemChanged(pos);
                        updateUi();
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.treatmentUpdateSuccessMsg), Toast.LENGTH_SHORT);
                    });

                    //set sync status true
                    getDataManager().setLocalDataExistForSync(true);
                    break;

                case AppConstants.REQUEST_TYPE_DEL:
                    tempBean = list.get(pos);
                    if (tempBean.getTreatmentId().equals(tempBean.getRecordKey())) {
                        getDataManager().deleteUsingTreatmentId(tempBean.getTreatmentId());
                    } else {
                        treatment = new Treatment();
                        treatment.setTreatmentId(tempBean.getTreatmentId());
                        treatment.setTreatmentName(name);
                        treatment.setTreatmentForId(animalInfoBean.getAnimalId());
                        treatment.setTreatmentFor(AppConstants.TREATMENT_FOR_ANIMAL);
                        treatment.setRecordKey(tempBean.getRecordKey());
                        treatment.setCrd(tempBean.getCrd());
                        treatment.setUpd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                        treatment.setDataSync(AppConstants.DB_SYNC_FALSE);
                        treatment.setEvent(AppConstants.DB_EVENT_DEL);

                        getDataManager().updateTreatment(treatment);

                        //set sync status true
                        getDataManager().setLocalDataExistForSync(true);
                    }
                    AppLogger.d("database", "Treament delete success");

                    list.remove(pos);
                    handler.post(() -> {
                        treatmentAdapter.notifyItemRemoved(pos);
                        updateUi();
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.treatmentDelSuccessMsg), Toast.LENGTH_SHORT);
                    });
                    break;
            }
        }).start();
    }
    /*Local DB End here*/
}
