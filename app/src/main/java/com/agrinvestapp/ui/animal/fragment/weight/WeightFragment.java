package com.agrinvestapp.ui.animal.fragment.weight;


import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.AnimalInfoResponse;
import com.agrinvestapp.data.model.api.WeightInfoResponse;
import com.agrinvestapp.data.model.db.Weight;
import com.agrinvestapp.ui.animal.fragment.weight.adapter.WeightAdapter;
import com.agrinvestapp.ui.animal.fragment.weight.dialog.WeightCallback;
import com.agrinvestapp.ui.animal.fragment.weight.dialog.WeightDialog;
import com.agrinvestapp.ui.base.BaseFragment;
import com.agrinvestapp.ui.base.dialog.DeleteDialog;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.AppLogger;
import com.agrinvestapp.utils.CalenderUtils;
import com.agrinvestapp.utils.CommonUtils;
import com.agrinvestapp.volley.VolleyJsonObjectListener;
import com.agrinvestapp.volley.VolleyRequest;
import com.agrinvestapp.volley.VolleyResponseListener;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class WeightFragment extends BaseFragment implements View.OnClickListener, WeightCallback {

    private TextView tvNoRecord;
    private RecyclerView rvWeight;

    private AnimalInfoResponse.AnimalListBean animalInfoBean;
    private WeightAdapter weightAdapter;
    private List<WeightInfoResponse.WeightListBean> list;

    private Handler handler = new Handler(Looper.getMainLooper());

    public static WeightFragment newInstance(AnimalInfoResponse.AnimalListBean animalInfoBean) {

        Bundle args = new Bundle();
        args.putParcelable(AppConstants.ANIMAL_MODEL, animalInfoBean);
        WeightFragment fragment = new WeightFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            animalInfoBean = getArguments().getParcelable(AppConstants.ANIMAL_MODEL);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_weight, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
         /* toolbar view start */
        ImageView imgBack = view.findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);
        imgBack.setVisibility(View.VISIBLE);

        TextView tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.weight);

        FrameLayout frameAdd = view.findViewById(R.id.frameAddT);
        frameAdd.setVisibility(View.VISIBLE);
        frameAdd.setOnClickListener(this);
        /* toolbar view end */
        view.findViewById(R.id.rlMain).setOnClickListener(this); //avoid bg click
        tvNoRecord = view.findViewById(R.id.tvNoRecord);

        rvWeight = view.findViewById(R.id.rvWeight);
        list = new ArrayList<>();

        weightAdapter = new WeightAdapter(list, pos -> DeleteDialog.newInstance(false, "", () -> {
            if (isNetworkConnected(true)) {
                getBaseActivity().doSyncAppDB(false, flag -> {
                    switch (flag) {
                        case AppConstants.ONLINE:
                            doAddDelWeight(pos, AppConstants.REQUEST_TYPE_DEL, list.get(pos));
                            break;

                        case AppConstants.OFFLINE:
                            doAddDelWeightLocal(pos, AppConstants.REQUEST_TYPE_DEL, list.get(pos));
                            break;
                    }
                });
            } else doAddDelWeightLocal(pos, AppConstants.REQUEST_TYPE_DEL, list.get(pos));
        }).show(getChildFragmentManager()));
        rvWeight.setAdapter(weightAdapter);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (isNetworkConnected(true)) {
            getBaseActivity().doSyncAppDB(false, flag -> {
                switch (flag) {
                    case AppConstants.ONLINE:
                        getWeightInfo();
                        break;

                    case AppConstants.OFFLINE:
                        getWeightInfoFromDB();
                        break;
                }
            });
        }
        else {
            getWeightInfoFromDB();
        }
    }

    private void getWeightInfo() {
        setLoading(true);

        HashMap<String, String> mParameterMap = new HashMap<>();
        mParameterMap.put("requestType", AppConstants.REQUEST_TYPE_WEIGHT);
        mParameterMap.put("animalId", animalInfoBean.getAnimalId());
        mParameterMap.put("recordKey", animalInfoBean.getRecordKey());
        getDataManager().doServerTreatmentInfoListApiCall(getDataManager().getHeader(), mParameterMap)
                .getAsResponse(new VolleyResponseListener() {
                    @Override
                    public void onVolleyResponse(String response) {
                        setLoading(false);
                        WeightInfoResponse infoResponse = getDataManager().mGson.fromJson(response, WeightInfoResponse.class);

                        if (infoResponse.getStatus().equals("success")) {
                            list.clear();
                            list.addAll(infoResponse.getWeightList());

                            updateUI();
                            if (!list.isEmpty()) {
                                //Todo manage local db add all data
                                doAddAllWeightLocalDB(infoResponse.getWeightList());
                            }

                            weightAdapter.notifyDataSetChanged();
                        } else {
                            updateUI();
                            if (infoResponse.getMessage().equals(getString(R.string.no_record_found))) {
                                list.clear();
                                weightAdapter.notifyDataSetChanged();
                            }
                            if (!infoResponse.getMessage().equals(getString(R.string.no_record_found)))
                                CommonUtils.showToast(getBaseActivity(), infoResponse.getMessage(), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        updateUI();
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    private void updateUI() {
        tvNoRecord.setVisibility(list.isEmpty() ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                getBaseActivity().onBackPressed();
                break;

            case R.id.frameAddT:
                if (list.size() < 10) {
                    WeightDialog.newInstance(0, WeightFragment.this).show(getChildFragmentManager());
                } else
                    CommonUtils.showToast(getBaseActivity(), getString(R.string.weightLimit), Toast.LENGTH_SHORT);

                break;
        }
    }

    private void doAddDelWeight(int pos, String flag, WeightInfoResponse.WeightListBean bean) {
        setLoading(true);

        HashMap<String, String> mParams = new HashMap<>();
        mParams.put("requestType", flag);
        mParams.put("recordKey", animalInfoBean.getRecordKey());  //animal record key
        mParams.put("weight", bean.getWeight());
        mParams.put("weightDate", bean.getWeightDate());
        mParams.put("weightUnit", bean.getWeightUnit());

        switch (flag) {
            case AppConstants.REQUEST_TYPE_ADD:
                mParams.put("weightId", bean.getWeightId());
                mParams.put("weightRecordKey", bean.getRecordKey());
                break;

            case AppConstants.REQUEST_TYPE_DEL:
                mParams.put("weightId", bean.getWeightId());
                mParams.put("weightRecordKey", bean.getRecordKey());
                break;
        }

        getDataManager().doServerWeightAddDelApiCall(getDataManager().getHeader(), mParams)
                .getAsJsonObject(new VolleyJsonObjectListener() {

                    @Override
                    public void onVolleyJsonObject(JSONObject obj) {
                        setLoading(false);

                        try {
                            String status = obj.getString("status");
                            String message = obj.getString("message");

                            if (status.equals("success")) {

                                switch (flag) {
                                    case AppConstants.REQUEST_TYPE_ADD:
                                        JSONObject jsonObject = obj.getJSONObject("weightDetail");

                                        WeightInfoResponse.WeightListBean bean = new WeightInfoResponse.WeightListBean();
                                        bean.setWeightId(jsonObject.getString("weightId"));
                                        bean.setAnimal_id(jsonObject.getString("animal_id"));
                                        bean.setWeight(jsonObject.getString("weight"));
                                        bean.setWeightUnit(jsonObject.getString("weightUnit"));
                                        bean.setWeightDate(jsonObject.getString("weightDate"));
                                        bean.setRecordKey(jsonObject.getString("recordKey"));
                                        bean.setCrd(jsonObject.getString("crd"));
                                        bean.setUpd(jsonObject.getString("upd"));

                                        list.add(pos, bean);
                                        weightAdapter.notifyItemInserted(pos);

                                        rvWeight.smoothScrollToPosition(pos);
                                        updateLocalDB(flag, list.get(pos));
                                        CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                                        break;

                                    case AppConstants.REQUEST_TYPE_DEL:
                                        updateLocalDB(flag, list.get(pos));

                                        list.remove(pos);
                                        weightAdapter.notifyItemRemoved(pos);
                                        CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                                        break;
                                }

                                updateUI();

                            } else
                                CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                        } catch (Exception e) {
                            CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyJsonException() {
                        setLoading(false);
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    @Override
    public void onAdd(WeightInfoResponse.WeightListBean bean) {
        if (isNetworkConnected(true)) {
            getBaseActivity().doSyncAppDB(false, flag -> {
                switch (flag) {
                    case AppConstants.ONLINE:
                        doAddDelWeight(0, AppConstants.REQUEST_TYPE_ADD, bean);
                        break;

                    case AppConstants.OFFLINE:
                        doAddDelWeightLocal(0, AppConstants.REQUEST_TYPE_ADD, bean);
                        break;
                }
            });
        }
        else doAddDelWeightLocal(0, AppConstants.REQUEST_TYPE_ADD, bean);
    }


    /*Local DB Start here*/
    private void getWeightInfoFromDB() {
        setLoading(true);

        new Thread(() -> {
            list.clear();
            List<WeightInfoResponse.WeightListBean> weightList = getDataManager().getWeightList(animalInfoBean.getAnimalId());

            list.addAll(weightList);

            handler.post(() -> {
                setLoading(false);
                weightAdapter.notifyDataSetChanged();
                updateUI();
            });
        }).start();
    }

    private void doAddAllWeightLocalDB(List<WeightInfoResponse.WeightListBean> infoResponse) {

        new Thread(() -> {
            getDataManager().deleteAllWeight();

            List<Weight> weightList = new ArrayList<>();
            for (WeightInfoResponse.WeightListBean mainBean : infoResponse) {
                Weight weight = new Weight();

                weight.setWeightId(mainBean.getWeightId());
                weight.setAnimal_id(mainBean.getAnimal_id());
                weight.setWeight(mainBean.getWeight());
                weight.setWeightUnit(mainBean.getWeightUnit());
                weight.setWeightDate(mainBean.getWeightDate());
                weight.setRecordKey(mainBean.getRecordKey());
                weight.setCrd(mainBean.getCrd());
                weight.setUpd(mainBean.getUpd());
                weight.setDataSync(AppConstants.DB_SYNC_TRUE);
                weight.setEvent(AppConstants.DB_EVENT_ADD);

                weightList.add(weight);

            }

            getDataManager().insertAllWeight(weightList);
            AppLogger.d("database", "Weight add success");
        }).start();
    }

    private void doAddDelWeightLocal(int pos, String flag, WeightInfoResponse.WeightListBean bean) {
        new Thread(() -> {
            switch (flag) {
                case AppConstants.REQUEST_TYPE_ADD:
                    Weight weight = new Weight();

                    weight.setWeightId(bean.getRecordKey());  //primary key
                    weight.setAnimal_id(animalInfoBean.getAnimalId());
                    weight.setWeight(bean.getWeight());
                    weight.setWeightUnit(bean.getWeightUnit());
                    weight.setWeightDate(bean.getWeightDate());
                    weight.setRecordKey(bean.getRecordKey());
                    weight.setCrd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    weight.setUpd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    weight.setDataSync(AppConstants.DB_SYNC_FALSE);
                    weight.setEvent(AppConstants.DB_EVENT_ADD);

                    getDataManager().insertWeight(weight);

                    //set sync status true
                    getDataManager().setLocalDataExistForSync(true);
                    AppLogger.d("database", "Weight add success");

                    handler.post(() -> {
                        list.add(pos, bean);
                        weightAdapter.notifyItemInserted(pos);
                        rvWeight.smoothScrollToPosition(pos);
                        updateUI();
                    });
                    break;

                case AppConstants.REQUEST_TYPE_DEL:
                    if (bean.getWeightId().equals(bean.getRecordKey())) {
                        getDataManager().deleteUsingWeightId(bean.getWeightId());
                    } else {
                        weight = new Weight();

                        weight.setWeightId(bean.getWeightId());  //primary key
                        weight.setAnimal_id(animalInfoBean.getAnimalId());
                        weight.setWeight(bean.getWeight());
                        weight.setWeightUnit(bean.getWeightUnit());
                        weight.setWeightDate(bean.getWeightDate());
                        weight.setRecordKey(bean.getRecordKey());
                        weight.setCrd(bean.getCrd());
                        weight.setUpd(bean.getUpd());
                        weight.setDataSync(AppConstants.DB_SYNC_FALSE);
                        weight.setEvent(AppConstants.DB_EVENT_DEL);

                        getDataManager().updateWeight(weight);

                        //set sync status true
                        getDataManager().setLocalDataExistForSync(true);
                    }
                    AppLogger.d("database", "Weight del success");

                    handler.post(() -> {
                        list.remove(pos);
                        weightAdapter.notifyItemRemoved(pos);
                        updateUI();
                    });
                    break;
            }

        }).start();
    }

    private void updateLocalDB(String flag, WeightInfoResponse.WeightListBean bean) {
        new Thread(() -> {
            switch (flag) {
                case AppConstants.REQUEST_TYPE_ADD:
                    Weight weight = new Weight();

                    weight.setWeightId(bean.getWeightId());  //primary key
                    weight.setAnimal_id(animalInfoBean.getAnimalId());
                    weight.setWeight(bean.getWeight());
                    weight.setWeightUnit(bean.getWeightUnit());
                    weight.setWeightDate(bean.getWeightDate());
                    weight.setRecordKey(bean.getRecordKey());
                    weight.setCrd(bean.getCrd());
                    weight.setUpd(bean.getUpd());
                    weight.setDataSync(AppConstants.DB_SYNC_FALSE);
                    weight.setEvent(AppConstants.DB_EVENT_ADD);

                    getDataManager().insertWeight(weight);

                    //set sync status true
                    getDataManager().setLocalDataExistForSync(true);
                    AppLogger.d("database", "Weight add success");
                    break;

                case AppConstants.REQUEST_TYPE_DEL:
                    getDataManager().deleteUsingWeightId(bean.getWeightId());
                    AppLogger.d("database", "Weight del success");
                    break;
            }

        }).start();

    }
    /*Local DB End here*/

}
