package com.agrinvestapp.ui.animal.fragment.calving.dialog;

import com.agrinvestapp.data.model.api.CalvingInfoResponse;

public interface CalvingCallback {
    void onAdd(CalvingInfoResponse.CalvingListBean bean);
}
