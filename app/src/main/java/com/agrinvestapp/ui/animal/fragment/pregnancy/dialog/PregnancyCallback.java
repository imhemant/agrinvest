package com.agrinvestapp.ui.animal.fragment.pregnancy.dialog;

import com.agrinvestapp.data.model.api.PregnancyTestInfoResponse;

public interface PregnancyCallback {
    void onAdd(PregnancyTestInfoResponse.PregnancyListBean bean);
}
