package com.agrinvestapp.ui.animal.dialog;

public interface CostToDateCallback {
    void onDataAdd(String fromDate, String toDate, String ttlCost);
}
