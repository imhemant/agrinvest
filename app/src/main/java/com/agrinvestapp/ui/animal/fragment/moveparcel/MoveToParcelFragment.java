package com.agrinvestapp.ui.animal.fragment.moveparcel;


import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.AnimalInfoResponse;
import com.agrinvestapp.data.model.api.ParcelMovementInfoResponse;
import com.agrinvestapp.data.model.db.Animal;
import com.agrinvestapp.data.model.db.ParcelMovement;
import com.agrinvestapp.ui.animal.fragment.AnimalDetailFragment;
import com.agrinvestapp.ui.animal.fragment.moveparcel.adapter.MoveParcelAdapter;
import com.agrinvestapp.ui.animal.fragment.moveparcel.dialog.MoveParcelCallback;
import com.agrinvestapp.ui.animal.fragment.moveparcel.dialog.MoveParcelDialog;
import com.agrinvestapp.ui.base.BaseFragment;
import com.agrinvestapp.ui.base.dialog.DeleteDialog;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.AppLogger;
import com.agrinvestapp.utils.CalenderUtils;
import com.agrinvestapp.utils.CommonUtils;
import com.agrinvestapp.volley.VolleyJsonObjectListener;
import com.agrinvestapp.volley.VolleyRequest;
import com.agrinvestapp.volley.VolleyResponseListener;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MoveToParcelFragment extends BaseFragment implements View.OnClickListener, BaseFragment.FragmentCallback, MoveParcelCallback {

    private TextView tvNoRecord;
    private RecyclerView rvMoveParcel;
    private List<ParcelMovementInfoResponse.ParcelMovementListBean> list;
    private MoveParcelAdapter moveParcelAdapter;
    private AnimalInfoResponse.AnimalListBean animalInfoBean;
    private AnimalDetailFragment animalDetailInstance;

    private Handler handler = new Handler(Looper.getMainLooper());

    public static MoveToParcelFragment newInstance(AnimalInfoResponse.AnimalListBean animalInfoBean, AnimalDetailFragment animalDetailInstance) {

        Bundle args = new Bundle();
        args.putParcelable(AppConstants.ANIMAL_MODEL, animalInfoBean);
        MoveToParcelFragment fragment = new MoveToParcelFragment();
        fragment.setInstance(animalDetailInstance);
        fragment.setArguments(args);
        return fragment;
    }

    private void setInstance(AnimalDetailFragment animalDetailInstance) {
        this.animalDetailInstance = animalDetailInstance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            animalInfoBean = getArguments().getParcelable(AppConstants.ANIMAL_MODEL);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_move_to_parcel, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
         /* toolbar view start */
        ImageView imgBack = view.findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);
        imgBack.setVisibility(View.VISIBLE);

        TextView tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.parcel_movement);

        FrameLayout frameAdd = view.findViewById(R.id.frameAddT);
        frameAdd.setVisibility(View.VISIBLE);
        frameAdd.setOnClickListener(this);
        /* toolbar view end */
        view.findViewById(R.id.rlMain).setOnClickListener(this); //avoid bg click
        tvNoRecord = view.findViewById(R.id.tvNoRecord);

        rvMoveParcel = view.findViewById(R.id.rvMoveParcel);
        list = new ArrayList<>();

        moveParcelAdapter = new MoveParcelAdapter(list, pos -> DeleteDialog.newInstance(false, "", () -> {
            if (isNetworkConnected(true)) {
                getBaseActivity().doSyncAppDB(false, flag -> {
                    switch (flag) {
                        case AppConstants.ONLINE:
                            doAddDelMoveToParcel(pos, AppConstants.REQUEST_TYPE_DEL, list.get(pos));
                            break;

                        case AppConstants.OFFLINE:
                            doAddDelMoveToParcelLocal(pos, AppConstants.REQUEST_TYPE_DEL, list.get(pos));
                            break;
                    }
                });
            } else doAddDelMoveToParcelLocal(pos, AppConstants.REQUEST_TYPE_DEL, list.get(pos));
        }).show(getChildFragmentManager()));
        rvMoveParcel.setAdapter(moveParcelAdapter);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (isNetworkConnected(true)) {
            getBaseActivity().doSyncAppDB(false, flag -> {
                switch (flag) {
                    case AppConstants.ONLINE:
                        getMoveToParcelInfo();
                        break;

                    case AppConstants.OFFLINE:
                        getMoveToParcelInfoFromDB();
                        break;
                }
            });
        }
        else {
            getMoveToParcelInfoFromDB();
        }
    }

    private void updateUI() {
        tvNoRecord.setVisibility(list.isEmpty() ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                getBaseActivity().onBackPressed();
                break;

            case R.id.frameAddT:
                if (list.size() < 50) {
                    MoveParcelDialog.newInstance(0, animalInfoBean.getParcel_id(), MoveToParcelFragment.this).show(getChildFragmentManager());
                } else
                    CommonUtils.showToast(getBaseActivity(), getString(R.string.mtoParcelLimit), Toast.LENGTH_SHORT);

                break;
        }
    }

    private void getMoveToParcelInfo() {
        setLoading(true);

        HashMap<String, String> mParameterMap = new HashMap<>();
        mParameterMap.put("requestType", AppConstants.REQUEST_TYPE_PMOVEMENT);
        mParameterMap.put("animalId", animalInfoBean.getAnimalId());
        mParameterMap.put("recordKey", animalInfoBean.getRecordKey());
        getDataManager().doServerTreatmentInfoListApiCall(getDataManager().getHeader(), mParameterMap)
                .getAsResponse(new VolleyResponseListener() {
                    @Override
                    public void onVolleyResponse(String response) {
                        setLoading(false);
                        ParcelMovementInfoResponse infoResponse = getDataManager().mGson.fromJson(response, ParcelMovementInfoResponse.class);

                        if (infoResponse.getStatus().equals("success")) {
                            list.clear();
                            list.addAll(infoResponse.getParcelMovementList());

                            updateUI();
                            if (!list.isEmpty()) {
                                //Todo manage local db add all data
                                doAddAllMoveToParcelLocalDB(infoResponse.getParcelMovementList());
                            }

                            moveParcelAdapter.notifyDataSetChanged();
                        } else {
                            updateUI();
                            if (infoResponse.getMessage().equals(getString(R.string.no_record_found))) {
                                list.clear();
                                moveParcelAdapter.notifyDataSetChanged();
                            }
                            if (!infoResponse.getMessage().equals(getString(R.string.no_record_found)))
                                CommonUtils.showToast(getBaseActivity(), infoResponse.getMessage(), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        updateUI();
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    private void doAddDelMoveToParcel(int pos, String flag, ParcelMovementInfoResponse.ParcelMovementListBean bean) {
        setLoading(true);

        HashMap<String, String> mParams = new HashMap<>();
        mParams.put("requestType", flag);
        mParams.put("recordKey", animalInfoBean.getRecordKey());  //animal record key
        mParams.put("movementFromRecordKey", animalInfoBean.getParcelRecordKey());
        mParams.put("movementDate", bean.getMovementDate());

        switch (flag) {
            case AppConstants.REQUEST_TYPE_ADD:
                mParams.put("movementToRecordKey", bean.getToRecordKey());
                mParams.put("parcelMovementId", bean.getParcelMovementId());
                mParams.put("movementRecordKey", bean.getRecordKey());
                break;

            case AppConstants.REQUEST_TYPE_DEL:
                mParams.put("parcelMovementId", bean.getParcelMovementId());
                mParams.put("movementRecordKey", bean.getRecordKey());
                break;
        }

        getDataManager().doServerParcelMovementAddDelApiCall(getDataManager().getHeader(), mParams)
                .getAsJsonObject(new VolleyJsonObjectListener() {

                    @Override
                    public void onVolleyJsonObject(JSONObject obj) {
                        setLoading(false);

                        try {
                            String status = obj.getString("status");
                            String message = obj.getString("message");

                            if (status.equals("success")) {

                                switch (flag) {
                                    case AppConstants.REQUEST_TYPE_ADD:
                                        JSONObject jsonObject = obj.getJSONObject("movementDetail");

                                        ParcelMovementInfoResponse.ParcelMovementListBean tempBean = new ParcelMovementInfoResponse.ParcelMovementListBean();

                                        tempBean.setToName(bean.getToName());
                                        tempBean.setFromName(animalInfoBean.getParcelName());
                                        tempBean.setParcelMovementId(jsonObject.getString("parcelMovementId"));
                                        tempBean.setAnimal_id(jsonObject.getString("animal_id"));
                                        tempBean.setMovementFrom(jsonObject.getString("movementFrom"));
                                        tempBean.setMovementTo(jsonObject.getString("movementTo"));
                                        tempBean.setMovementDate(jsonObject.getString("movementDate"));
                                        tempBean.setRecordKey(jsonObject.getString("recordKey"));
                                        tempBean.setCrd(jsonObject.getString("crd"));
                                        tempBean.setUpd(jsonObject.getString("upd"));

                                        list.add(pos, tempBean);
                                        moveParcelAdapter.notifyItemInserted(pos);

                                        rvMoveParcel.smoothScrollToPosition(pos);

                                        updateLocalDB(flag, list.get(pos));

                                        CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                                        break;

                                    case AppConstants.REQUEST_TYPE_DEL:
                                        updateLocalDB(flag, list.get(pos));

                                        list.remove(pos);
                                        moveParcelAdapter.notifyItemRemoved(pos);

                                        CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                                        break;
                                }

                                updateUI();

                            } else
                                CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                        } catch (Exception e) {
                            CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyJsonException() {
                        setLoading(false);
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    @Override
    public void onAdd(ParcelMovementInfoResponse.ParcelMovementListBean bean) {
        if (isNetworkConnected(true)) {
            getBaseActivity().doSyncAppDB(false, flag -> {
                switch (flag) {
                    case AppConstants.ONLINE:
                        doAddDelMoveToParcel(0, AppConstants.REQUEST_TYPE_ADD, bean);
                        break;

                    case AppConstants.OFFLINE:
                        doAddDelMoveToParcelLocal(0, AppConstants.REQUEST_TYPE_ADD, bean);
                        break;
                }
            });
        }
        else doAddDelMoveToParcelLocal(0, AppConstants.REQUEST_TYPE_ADD, bean);
    }

    /*Local DB Start here*/
    private void getMoveToParcelInfoFromDB() {
        setLoading(true);

        new Thread(() -> {
            list.clear();
            List<ParcelMovementInfoResponse.ParcelMovementListBean> parcelMovementList = getDataManager().getParcelMovementList(animalInfoBean.getAnimalId());

            list.addAll(parcelMovementList);

            handler.post(() -> {
                setLoading(false);
                moveParcelAdapter.notifyDataSetChanged();
                updateUI();
            });
        }).start();
    }

    private void doAddAllMoveToParcelLocalDB(List<ParcelMovementInfoResponse.ParcelMovementListBean> infoResponse) {
        new Thread(() -> {
            getDataManager().deleteAllParcelMovement();

            List<ParcelMovement> parcelMovementList = new ArrayList<>();
            for (ParcelMovementInfoResponse.ParcelMovementListBean mainBean : infoResponse) {
                ParcelMovement parcelMovement = new ParcelMovement();

                parcelMovement.setParcelMovementId(mainBean.getParcelMovementId());
                parcelMovement.setAnimal_id(mainBean.getAnimal_id());
                parcelMovement.setMovementFrom(mainBean.getMovementFrom());
                parcelMovement.setMovementTo(mainBean.getMovementTo());
                parcelMovement.setMovementDate(mainBean.getMovementDate());
                parcelMovement.setRecordKey(mainBean.getRecordKey());
                parcelMovement.setCrd(mainBean.getCrd());
                parcelMovement.setUpd(mainBean.getUpd());
                parcelMovement.setDataSync(AppConstants.DB_SYNC_TRUE);
                parcelMovement.setEvent(AppConstants.DB_EVENT_ADD);

                parcelMovementList.add(parcelMovement);
            }

            getDataManager().insertAllParcelMovement(parcelMovementList);
            AppLogger.d("database", "ParcelMovement add success");
        }).start();
    }

    private void doAddDelMoveToParcelLocal(int pos, String flag, ParcelMovementInfoResponse.ParcelMovementListBean bean) {
        new Thread(() -> {
            switch (flag) {
                case AppConstants.REQUEST_TYPE_ADD:
                    ParcelMovement parcelMovement = new ParcelMovement();

                    parcelMovement.setParcelMovementId(bean.getRecordKey());  //primary key
                    parcelMovement.setAnimal_id(animalInfoBean.getAnimalId());
                    parcelMovement.setMovementFrom(animalInfoBean.getParcel_id());
                    parcelMovement.setMovementTo(bean.getMovementTo());
                    parcelMovement.setMovementDate(bean.getMovementDate());
                    parcelMovement.setRecordKey(bean.getRecordKey());
                    parcelMovement.setCrd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    parcelMovement.setUpd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    parcelMovement.setDataSync(AppConstants.DB_SYNC_FALSE);
                    parcelMovement.setEvent(AppConstants.DB_EVENT_ADD);

                    getDataManager().insertParcelMovement(parcelMovement);

                    Animal animal = getDataManager().getAnimalUsingAnimalId(animalInfoBean.getAnimalId());
                    animal.setParcel_id(bean.getMovementTo());
                    animal.setEvent(animal.getAnimalId().equals(animal.getRecordKey()) ? AppConstants.DB_EVENT_ADD : AppConstants.DB_EVENT_EDIT);
                    animal.setDataSync(AppConstants.DB_SYNC_FALSE);
                    getDataManager().updateAnimal(animal);

                    //set sync status true
                    getDataManager().setLocalDataExistForSync(true);
                    AppLogger.d("database", "ParcelMovement add success");

                    handler.post(() -> {
                        bean.setMovementFrom(animalInfoBean.getParcel_id());
                        bean.setToName(bean.getToName());
                        bean.setFromName(animalInfoBean.getParcelName());

                        updateAnimalInfo(bean);

                        list.add(pos, bean);
                        moveParcelAdapter.notifyItemInserted(pos);
                        rvMoveParcel.smoothScrollToPosition(pos);
                        updateUI();
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.parcelMoveAddSuccess), Toast.LENGTH_SHORT);
                    });
                    break;

                case AppConstants.REQUEST_TYPE_DEL:
                    if (bean.getParcelMovementId().equals(bean.getRecordKey())) {
                        getDataManager().deleteUsingParcelMovementId(bean.getParcelMovementId());
                    } else {
                        parcelMovement = new ParcelMovement();

                        parcelMovement.setParcelMovementId(bean.getParcelMovementId());  //primary key
                        parcelMovement.setAnimal_id(animalInfoBean.getAnimalId());
                        parcelMovement.setMovementFrom(animalInfoBean.getParcel_id());
                        parcelMovement.setMovementTo(bean.getMovementTo());
                        parcelMovement.setMovementDate(bean.getMovementDate());
                        parcelMovement.setRecordKey(bean.getRecordKey());
                        parcelMovement.setCrd(bean.getCrd());
                        parcelMovement.setUpd(bean.getUpd());
                        parcelMovement.setDataSync(AppConstants.DB_SYNC_FALSE);
                        parcelMovement.setEvent(AppConstants.DB_EVENT_DEL);

                        getDataManager().updateParcelMovement(parcelMovement);

                        updateAnimal(AppConstants.DB_EVENT_EDIT, AppConstants.DB_SYNC_FALSE, bean);

                        //set sync status true
                        getDataManager().setLocalDataExistForSync(true);
                    }
                    AppLogger.d("database", "ParcelMovement del success");

                    handler.post(() -> {
                        list.remove(pos);
                        moveParcelAdapter.notifyItemRemoved(pos);
                        updateUI();
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.parcelMoveDelSuccess), Toast.LENGTH_SHORT);
                    });
                    break;
            }

        }).start();
    }

    private void updateAnimalInfo(ParcelMovementInfoResponse.ParcelMovementListBean bean) {
        animalInfoBean.setParcel_id(bean.getMovementTo());
        animalInfoBean.setParcelName(bean.getToName());
    }

    private void updateLocalDB(String flag, ParcelMovementInfoResponse.ParcelMovementListBean bean) {
        new Thread(() -> {
            switch (flag) {
                case AppConstants.REQUEST_TYPE_ADD:
                    ParcelMovement parcelMovement = new ParcelMovement();

                    parcelMovement.setParcelMovementId(bean.getParcelMovementId());  //primary key
                    parcelMovement.setAnimal_id(animalInfoBean.getAnimalId());
                    parcelMovement.setMovementFrom(bean.getMovementFrom());
                    parcelMovement.setMovementTo(bean.getMovementTo());
                    parcelMovement.setMovementDate(bean.getMovementDate());
                    parcelMovement.setRecordKey(bean.getRecordKey());
                    parcelMovement.setCrd(bean.getCrd());
                    parcelMovement.setUpd(bean.getUpd());
                    parcelMovement.setDataSync(AppConstants.DB_SYNC_TRUE);
                    parcelMovement.setEvent(AppConstants.DB_EVENT_ADD);

                    getDataManager().insertParcelMovement(parcelMovement);

                    Animal animal = getDataManager().getAnimalUsingAnimalId(animalInfoBean.getAnimalId());
                    animal.setParcel_id(bean.getMovementTo());
                    animal.setEvent(animal.getAnimalId().equals(animal.getRecordKey()) ? AppConstants.DB_EVENT_ADD : AppConstants.DB_EVENT_EDIT);
                    animal.setDataSync(AppConstants.DB_SYNC_TRUE);
                    getDataManager().updateAnimal(animal);

                    AppLogger.d("database", "ParcelMovement add success");

                    updateAnimalInfo(bean);

                    updateAnimal(AppConstants.DB_EVENT_EDIT, AppConstants.DB_SYNC_TRUE, bean);
                    break;

                case AppConstants.REQUEST_TYPE_DEL:
                    getDataManager().deleteUsingParcelMovementId(bean.getParcelMovementId());
                    AppLogger.d("database", "ParcelMovement del success");

                    //updateAnimal(AppConstants.DB_EVENT_EDIT,AppConstants.DB_SYNC_TRUE, bean);
                    break;
            }

        }).start();

    }

    private void updateAnimal(String event, Boolean dbSyncStatus, ParcelMovementInfoResponse.ParcelMovementListBean bean) {
        //update animal
        Animal animal = new Animal();
        animal.setAnimalId(animalInfoBean.getAnimalId());
        animal.setParcel_id(bean.getMovementTo());
        animal.setLotId(animalInfoBean.getLotId());
        animal.setTagNumber(animalInfoBean.getTagNumber());
        animal.setBornOrBuyDate(animalInfoBean.getBornOrBuyDate());
        animal.setMother(animalInfoBean.getMother());
        animal.setBoughtFrom(animalInfoBean.getBoughtFrom());
        animal.setDescription(animalInfoBean.getDescription());
        animal.setDismant(animalInfoBean.getDismant());
        animal.setSaleDate(animalInfoBean.getSaleDate().isEmpty() ? null : CalenderUtils.getDateFormat(animalInfoBean.getSaleDate().contains("/") ? CalenderUtils.formatDate(animalInfoBean.getSaleDate(), AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT) : animalInfoBean.getSaleDate(), AppConstants.SERVER_TIMESTAMP_FORMAT));
        animal.setMarkingOrTagging(animalInfoBean.getMarkingOrTagging());
        animal.setSalePrice(animalInfoBean.getSalePrice());
        animal.setSaleQuantity(animalInfoBean.getSaleQuantity());
        animal.setSaleTotalPrice(animalInfoBean.getSaleTotalPrice());
        animal.setCostFrom(animalInfoBean.getCostFrom());
        animal.setCostTo(animalInfoBean.getCostTo());
        animal.setCostTotal(animalInfoBean.getCostTotal());
        animal.setStatus(animalInfoBean.getStatus());
        animal.setRecordKey(animalInfoBean.getRecordKey());
        animal.setCrd(animalInfoBean.getCrd());
        animal.setUpd(animalInfoBean.getUpd());
        animal.setEvent(event);
        animal.setDataSync(dbSyncStatus);

        getDataManager().updateAnimal(animal);
        AppLogger.d("database", "Animal update success");
    }

    @Override
    public void onBackPress() {
        if (animalDetailInstance != null) animalDetailInstance.updateParcel(animalInfoBean);
    }
    /*Local DB End here*/
}