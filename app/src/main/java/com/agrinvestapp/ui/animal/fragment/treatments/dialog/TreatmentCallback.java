package com.agrinvestapp.ui.animal.fragment.treatments.dialog;

public interface TreatmentCallback {
    void onAdd(String name);

    void onEdit(int pos, String name);
}
