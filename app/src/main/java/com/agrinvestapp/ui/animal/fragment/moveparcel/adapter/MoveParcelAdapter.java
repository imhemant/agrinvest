package com.agrinvestapp.ui.animal.fragment.moveparcel.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.ParcelMovementInfoResponse;
import com.agrinvestapp.utils.pagination.FooterLoader;
import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chauthai.swipereveallayout.ViewBinderHelper;

import java.util.List;

/**
 * Created by hemant
 * Date: 17/4/18
 * Time: 4:03 PM
 */

public class MoveParcelAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEWTYPE_ITEM = 1;
    private final int VIEWTYPE_LOADER = 2;
    // This object helps you save/restore the open/close state of each view
    private final ViewBinderHelper viewBinderHelper = new ViewBinderHelper();
    private List<ParcelMovementInfoResponse.ParcelMovementListBean> list;
    private boolean showLoader;
    private ItemClickListener itemClickListener;

    private StringBuilder builder;

    public MoveParcelAdapter(List<ParcelMovementInfoResponse.ParcelMovementListBean> list, ItemClickListener itemClickListener) {
        this.list = list;
        this.itemClickListener = itemClickListener;
        viewBinderHelper.setOpenOnlyOne(true);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder rvHolder, int position) {
        if (rvHolder instanceof FooterLoader) {
            FooterLoader loaderViewHolder = (FooterLoader) rvHolder;
            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }
            return;
        }
        MoveParcelAdapter.ViewHolder holder = ((MoveParcelAdapter.ViewHolder) rvHolder);

        ParcelMovementInfoResponse.ParcelMovementListBean bean = list.get(position);

        viewBinderHelper.bind(holder.swipe, bean.getRecordKey());
        builder = new StringBuilder("");
        builder.append(bean.getFromName()).append(" ").append(holder.tvDate.getContext().getString(R.string.to)).append(" ").append(bean.getToName());
        holder.tvName.setText(builder);
        holder.tvDate.setText(bean.getMovementDate());
    }

    public void showLoading(boolean status) {
        showLoader = status;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case VIEWTYPE_ITEM:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_move_to_parcel, parent, false);
                return new ViewHolder(view);

            case VIEWTYPE_LOADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pagination_item_loader, parent, false);
                return new FooterLoader(view);
        }
        return null;

    }

    @Override
    public int getItemViewType(int position) {
        if (position == list.size() - 1) {
            return showLoader ? VIEWTYPE_LOADER : VIEWTYPE_ITEM;
        }
        return VIEWTYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface ItemClickListener {
        void onTrashClick(int pos);
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvName, tvDate;
        private LinearLayout llDelete;
        private SwipeRevealLayout swipe;

        ViewHolder(@NonNull View v) {
            super(v);
            tvName = v.findViewById(R.id.tvName);
            tvDate = v.findViewById(R.id.tvDate);
            llDelete = v.findViewById(R.id.llDelete);
            swipe = v.findViewById(R.id.swipe);

            llDelete.setOnClickListener(this);
        }

        @Override
        public void onClick(@NonNull final View view) {
            if (getAdapterPosition() != -1) {
                swipe.close(true);
                itemClickListener.onTrashClick(getAdapterPosition());
            }
        }
    }

}

