package com.agrinvestapp.ui.animal.fragment;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.AnimalInfoResponse;
import com.agrinvestapp.data.model.api.MotherResponse;
import com.agrinvestapp.data.model.db.Animal;
import com.agrinvestapp.data.model.db.Parcel;
import com.agrinvestapp.ui.animal.adapter.SpMotherNameAdapter;
import com.agrinvestapp.ui.animal.adapter.SpParcelNameAdapter;
import com.agrinvestapp.ui.base.BaseFragment;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.AppLogger;
import com.agrinvestapp.utils.CalenderUtils;
import com.agrinvestapp.utils.CommonUtils;
import com.agrinvestapp.volley.VolleyJsonObjectListener;
import com.agrinvestapp.volley.VolleyRequest;
import com.agrinvestapp.volley.VolleyResponseListener;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class AddEditAnimalFragment extends BaseFragment implements View.OnClickListener {

    private TextView tvBBDate;
    private EditText etLotId, etTagNumber, etBoughtFrom, etDescription;

    private AnimalInfoFragment animalInfoFragment;
    private Boolean isAdd = true;

    private List<Parcel> parcelList;
    private List<MotherResponse.AnimalMotherListBean> motherList;

    private Spinner spParcel, spMother;
    private SpParcelNameAdapter spParcelNameAdapter;
    private SpMotherNameAdapter spMotherNameAdapter;
    private AnimalInfoResponse.AnimalListBean animalInfoBean;

    private Boolean isClick = false;

    private Handler handler = new Handler(Looper.getMainLooper());

    // the callback received when the user "sets" the Date in the
    // DatePickerDialog
    @NonNull
    private DatePickerDialog.OnDateSetListener datePicker = (view, selectedYear, selectedMonth, selectedDay) -> {
        selectedMonth += 1;

        StringBuilder date = new StringBuilder("");
        date.append(selectedDay).append("/").append(selectedMonth).append("/").append(selectedYear);

        tvBBDate.setText(CalenderUtils.formatDate(String.valueOf(date), AppConstants.TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT));

    };

    protected void setInstance(AnimalInfoFragment animalInfoFragment, Boolean isAdd) {
        this.animalInfoFragment = animalInfoFragment;
        this.isAdd = isAdd;
    }

    protected void setAnimalModel(AnimalInfoResponse.AnimalListBean animalInfoBean) {
        this.animalInfoBean = animalInfoBean;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_edit_animal, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
          /* toolbar view start */
        ImageView imgBack = view.findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);
        imgBack.setVisibility(View.VISIBLE);

        TextView tvTitle = view.findViewById(R.id.tvTitle);

        Button tvAdd = view.findViewById(R.id.btnAddAnimal);
        if (isAdd) {
            tvTitle.setText(R.string.add_animal_info);
            tvAdd.setText(getString(R.string.add));
        } else {
            tvTitle.setText(R.string.edit_animal_info);
            tvAdd.setText(getString(R.string.update));
        }
        tvAdd.setOnClickListener(this);
        /* toolbar view end */
        view.findViewById(R.id.llMain).setOnClickListener(this); //avoid bg click

        etLotId = view.findViewById(R.id.etLotId);
        etTagNumber = view.findViewById(R.id.etTagNumber);
        etBoughtFrom = view.findViewById(R.id.etBoughtFrom);
        etDescription = view.findViewById(R.id.etDescription);

        tvBBDate = view.findViewById(R.id.tvBBDate);
        tvBBDate.setOnClickListener(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initSpinner(view);
    }

    private void initSpinner(View view) {
        spParcel = view.findViewById(R.id.spParcel);
        spMother = view.findViewById(R.id.spMother);

        if (!isAdd) {
            spParcel.setEnabled(false);
        }

        parcelList = new ArrayList<>();
        Parcel bean = new Parcel();
        bean.setParcelId("");
        bean.setParcelName(getString(R.string.select_parcel));
        bean.setProperty_id("");
        bean.setRecordKey("");
        parcelList.add(bean);

        spParcelNameAdapter = new SpParcelNameAdapter(getBaseActivity(), parcelList);
        spParcel.setAdapter(spParcelNameAdapter);

        motherList = new ArrayList<>();
        MotherResponse.AnimalMotherListBean motherListBean = new MotherResponse.AnimalMotherListBean();
        motherListBean.setTagNumber(getString(R.string.select_mother));
        motherList.add(motherListBean);

        MotherResponse.AnimalMotherListBean motherListBean1 = new MotherResponse.AnimalMotherListBean();
        motherListBean1.setTagNumber(getString(R.string.none));
        motherList.add(motherListBean1);
        spMotherNameAdapter = new SpMotherNameAdapter(getBaseActivity(), motherList);
        spMother.setAdapter(spMotherNameAdapter);

        if (isAdd) {
            if (isNetworkConnected(true)) {
                if (getDataManager().getLocalDataExistForSync()) {
                    getSpLocalDBData();
                } else {
                    getSpParcelApi();
                    getSpMotherApi();
                }
            } else getSpLocalDBData();
        } else {
            if (animalInfoBean != null) updateAnimalUi();
            if (isNetworkConnected(true)) {
                if (getDataManager().getLocalDataExistForSync()) {
                    getSpLocalDBData();
                } else {
                    getSpParcelApi();
                    getSpMotherApi();
                }
            } else getSpLocalDBData();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.imgBack:
                getBaseActivity().onBackPressed();
                break;

            case R.id.btnAddAnimal:
                if (!isClick) {
                    isClick = true;

                    if (isNetworkConnected(true)) getBaseActivity().doSyncAppDB(false, flag1 -> {
                        switch (flag1) {
                            case AppConstants.ONLINE:
                                if (isAdd) doAddAnimal();
                                else if (animalInfoBean != null && !animalInfoBean.getAnimalId().equalsIgnoreCase(animalInfoBean.getRecordKey()))
                                    updateAnimal();
                                else doAddAnimal();
                                break;

                            case AppConstants.OFFLINE:
                                if (isAdd) doAddEditAnimalToLocalDB("add");
                                else if (animalInfoBean != null && !animalInfoBean.getAnimalId().equalsIgnoreCase(animalInfoBean.getRecordKey()))
                                    doAddEditAnimalToLocalDB("edit");
                                else if (!isAdd) doAddEditAnimalToLocalDB("edit");
                                break;
                        }
                    });
                    else {
                        if (isAdd) doAddEditAnimalToLocalDB("add");
                        else if (animalInfoBean != null && !animalInfoBean.getAnimalId().equalsIgnoreCase(animalInfoBean.getRecordKey()))
                            doAddEditAnimalToLocalDB("edit");
                        else if (!isAdd) doAddEditAnimalToLocalDB("edit");
                    }
                }

                new Handler().postDelayed(() -> isClick = false, 3000);
                break;

            case R.id.tvBBDate:
                getDate();
                break;

        }
    }

    private void getDate() {
        String selectedDate = tvBBDate.getText().toString().trim();

        Calendar newCalender = Calendar.getInstance();

        int day, month, year;

        if (selectedDate.isEmpty()) {
            day = newCalender.get(Calendar.DAY_OF_MONTH);
            month = newCalender.get(Calendar.MONTH);
            year = newCalender.get(Calendar.YEAR);
        } else {
            String[] date = selectedDate.split("/");

            day = Integer.parseInt(date[0]);
            month = Integer.parseInt(date[1]) - 1;
            year = Integer.parseInt(date[2]);
        }

        DatePickerDialog datePickerDialog = new DatePickerDialog(getBaseActivity(), datePicker, year, month, day);
        Date maxDate = CalenderUtils.getDateFormat(CalenderUtils.getCurrentDate(), AppConstants.TIMESTAMP_FORMAT);
        assert maxDate != null;
        datePickerDialog.getDatePicker().setMaxDate(maxDate.getTime());

        datePickerDialog.show();
    }

    private void getSpParcelApi() {
        setLoading(true);
        getDataManager().doServerGetParcelListApiCall(getDataManager().getHeader())
                .getAsJsonObject(new VolleyJsonObjectListener() {

                    @Override
                    public void onVolleyJsonObject(JSONObject jsonObject) {
                        setLoading(false);

                        try {
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            if (status.equals("success")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("parcelList");

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject obj = jsonArray.getJSONObject(i);
                                    Parcel parcel = new Parcel();
                                    parcel.setParcelId(obj.getString("parcelId"));
                                    parcel.setParcelName(obj.getString("parcelName"));
                                    parcel.setProperty_id(obj.getString("property_id"));
                                    parcel.setRecordKey(obj.getString("recordKey"));
                                    parcelList.add(parcel);
                                }

                                spParcelNameAdapter.notifyDataSetChanged();

                                updateSp("parcel");
                            } else {
                                if (!message.equalsIgnoreCase(getString(R.string.no_record_found2)))
                                    CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                            }
                        } catch (Exception e) {
                            CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyJsonException() {
                        setLoading(false);
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    private void getSpMotherApi() {
        setLoading(true);
        getDataManager().doServerGetAnimalListApiCall(getDataManager().getHeader())
                .getAsResponse(new VolleyResponseListener() {
                    @Override
                    public void onVolleyResponse(String response) {
                        setLoading(false);

                        MotherResponse motherResponse = getDataManager().mGson.fromJson(response, MotherResponse.class);
                        if (motherResponse.getStatus().equals("success")) {
                            motherList.addAll(motherResponse.getAnimalMotherList());

                            if (!isAdd) {
                                //for edit case if same tag number exist in list then remove
                                for (int i = 0; i < motherList.size(); i++) {
                                    if (animalInfoBean.getTagNumber().equals(motherList.get(i).getTagNumber())) {
                                        motherList.remove(i);
                                        break;
                                    }
                                }
                            }

                            spMotherNameAdapter.notifyDataSetChanged();

                            updateSp("mother");
                        } else {
                            if (!motherResponse.getMessage().equalsIgnoreCase(getString(R.string.no_record_found2)))
                                CommonUtils.showToast(getBaseActivity(), motherResponse.getMessage(), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    private void updateSp(String flag) {
        if (!isAdd) {
            switch (flag) {
                case "parcel":
                    for (int i = 0; i < parcelList.size(); i++) {
                        if (animalInfoBean.getParcelName().equalsIgnoreCase(parcelList.get(i).getParcelName())) {
                            spParcel.setSelection(i);
                            break;
                        }
                    }
                    break;

                case "mother":
                    if (animalInfoBean.getMother().isEmpty()) {
                        spMother.setSelection(1);
                    } else {
                        for (int i = 0; i < motherList.size(); i++) {
                            if (animalInfoBean.getMother().equalsIgnoreCase(motherList.get(i).getTagNumber())) {
                                spMother.setSelection(i);
                                break;
                            }
                        }
                    }
                    break;
            }
        }
    }

    private void doAddAnimal() {
        //in online case we use parcel record key
        String tempParcelRk = parcelList.get(spParcel.getSelectedItemPosition()).getRecordKey().trim();
        String tempLotId = etLotId.getText().toString().trim();
        String tempTagNum = etTagNumber.getText().toString().trim();
        String tempBBDate = tvBBDate.getText().toString().trim();
        String tempMother = motherList.get(spMother.getSelectedItemPosition()).getTagNumber().trim();
        String tempBoughtFrom = etBoughtFrom.getText().toString().trim();
        String tempDes = etDescription.getText().toString().trim();

        tempMother = (tempMother.equalsIgnoreCase(getString(R.string.select_mother)) | tempMother.equalsIgnoreCase(getString(R.string.none))) ? "" : tempMother;
        String finalTempMother = tempMother;
        new Thread(() -> {

            String tempDBTagNum = getDataManager().getTagNumber(getDataManager().getUserInfo().getUserId(), tempTagNum);
            tempDBTagNum = (tempDBTagNum == null) ? "" : tempDBTagNum;
            String finalTempDBTagNum = tempDBTagNum;
            handler.post(() -> {
                if (verifyInputs(tempParcelRk, tempLotId, tempTagNum, finalTempDBTagNum, tempBBDate, finalTempMother, tempBoughtFrom, tempDes)) {
                    setLoading(true);

                    HashMap<String, String> mParameterMap = new HashMap<>();
                    mParameterMap.put("parcelRecordKey", tempParcelRk);
                    mParameterMap.put("lotId", tempLotId);
                    mParameterMap.put("tagNumber", tempTagNum);
                    mParameterMap.put("bornOrBuyDate", tempBBDate);
                    mParameterMap.put("mother", finalTempMother);
                    mParameterMap.put("boughtFrom", tempBoughtFrom);
                    mParameterMap.put("description", tempDes);
                    mParameterMap.put("recordKey", CalenderUtils.getTimestamp());

                    getDataManager().doServerAddAnimalApiCall(getDataManager().getHeader(), mParameterMap)
                            .getAsJsonObject(new VolleyJsonObjectListener() {
                                @Override
                                public void onVolleyJsonObject(JSONObject jsonObject) {
                                    setLoading(false);

                                    try {
                                        String status = jsonObject.getString("status");
                                        String message = jsonObject.getString("message");

                                        if (status.equals("success")) {
                                            //Todo manage local db add Done
                                            doAddEditAnimalToLocalDB("add", jsonObject);

                                            CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                                            getBaseActivity().onBackPressed();
                                            if (animalInfoFragment != null)
                                                animalInfoFragment.hitApi();
                                        } else {
                                            CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                                        }
                                    } catch (Exception e) {
                                        CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                                    }
                                }

                                @Override
                                public void onVolleyJsonException() {
                                    setLoading(false);
                                    CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                                }

                                @Override
                                public void onVolleyError(VolleyError error) {
                                    setLoading(false);
                                    VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                                }
                            });
                }
            });
        }).start();

    }

    /*Update Animal start here */
    private void updateAnimalUi() {
        etLotId.setText(animalInfoBean.getLotId());
        etTagNumber.setText(animalInfoBean.getTagNumber());
        etBoughtFrom.setText(animalInfoBean.getBoughtFrom());
        etDescription.setText(animalInfoBean.getDescription());
        tvBBDate.setText(CalenderUtils.formatDate(animalInfoBean.getBornOrBuyDate(), AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT));
    }

    private void updateAnimal() {
        String tempParcel = parcelList.get(spParcel.getSelectedItemPosition()).getRecordKey().trim();
        String tempLotId = etLotId.getText().toString().trim();
        String tempTagNum = etTagNumber.getText().toString().trim();
        String tempBBDate = tvBBDate.getText().toString().trim();
        String tempMother = motherList.get(spMother.getSelectedItemPosition()).getTagNumber().trim();
        String tempBoughtFrom = etBoughtFrom.getText().toString().trim();
        String tempDes = etDescription.getText().toString().trim();
        tempMother = (tempMother.equalsIgnoreCase(getString(R.string.select_mother)) | tempMother.equalsIgnoreCase(getString(R.string.none))) ? "" : tempMother;

        String finalTempMother = tempMother;
        new Thread(() -> {
            String tempDBTagNum = getDataManager().getTagNumber(getDataManager().getUserInfo().getUserId(), tempTagNum);
            tempDBTagNum = (tempDBTagNum == null) ? "" : tempDBTagNum;
            String finalTempDBTagNum = tempDBTagNum;
            handler.post(() -> {
                if (verifyInputs(tempParcel, tempLotId, tempTagNum, finalTempDBTagNum, tempBBDate, finalTempMother, tempBoughtFrom, tempDes)) {
                    setLoading(true);

                    HashMap<String, String> mParameterMap = new HashMap<>();
                    mParameterMap.put("animalId", animalInfoBean.getAnimalId());
                    mParameterMap.put("parcelRecordKey", tempParcel);
                    mParameterMap.put("lotId", tempLotId);
                    mParameterMap.put("tagNumber", tempTagNum);
                    mParameterMap.put("bornOrBuyDate", tempBBDate);
                    mParameterMap.put("mother", finalTempMother);
                    mParameterMap.put("boughtFrom", tempBoughtFrom);
                    mParameterMap.put("description", tempDes);
                    mParameterMap.put("recordKey", animalInfoBean.getRecordKey());

                    getDataManager().doServerUpdateAnimalDetailApiCall(getDataManager().getHeader(), mParameterMap)
                            .getAsJsonObject(new VolleyJsonObjectListener() {

                                @Override
                                public void onVolleyJsonObject(JSONObject jsonObject) {
                                    setLoading(false);

                                    try {
                                        String status = jsonObject.getString("status");
                                        String message = jsonObject.getString("message");

                                        if (status.equals("success")) {
                                            //Todo manage local db update Done
                                            doAddEditAnimalToLocalDB("edit", jsonObject);
                                            CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                                            getBaseActivity().onBackPressed();
                                            if (animalInfoFragment != null)
                                                animalInfoFragment.hitApi();
                                        } else {
                                            CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                                        }
                                    } catch (Exception e) {
                                        CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                                    }
                                }

                                @Override
                                public void onVolleyJsonException() {
                                    setLoading(false);
                                    CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                                }

                                @Override
                                public void onVolleyError(VolleyError error) {
                                    setLoading(false);
                                    VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                                }
                            });
                }
            });
        }).start();
    }
    /*Update Animal end here */

    private boolean verifyInputs(String tempParcel, String tempLotId, String tempTagNum, String tempDBTagNum, String tempBBDate, String tempMother, String tempBoughtFrom, String tempDes) {
        if (tempParcel.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_select_parcel), Toast.LENGTH_SHORT);
            return false;
        } else if (tempLotId.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_lotId), Toast.LENGTH_SHORT);
            return false;
        } else if (!(tempLotId.length() >= 3 && tempLotId.length() <= 10)) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_lot_length), Toast.LENGTH_SHORT);
            return false;
        } else if (tempTagNum.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_tagNum), Toast.LENGTH_SHORT);
            return false;
        } else if (!(tempTagNum.length() >= 3 && tempTagNum.length() <= 10)) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_tag_length), Toast.LENGTH_SHORT);
            return false;
        } else if (isAdd && tempDBTagNum.equalsIgnoreCase(tempTagNum)) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_same_tag), Toast.LENGTH_SHORT);
            return false;
        } else if (tempBBDate.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_select_bbDate), Toast.LENGTH_SHORT);
            return false;
        }/* else if (tempMother.equalsIgnoreCase(getString(R.string.select_mother))) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_select_mother), Toast.LENGTH_SHORT);
            return false;
        }*/ else if (tempBoughtFrom.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_bought_from), Toast.LENGTH_SHORT);
            return false;
        } else if (tempDes.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_des), Toast.LENGTH_SHORT);
            return false;
        } else if (!isAdd) {
            if (animalInfoBean.getTagNumber().equals(tempTagNum)) {
                return true;
            } else if (tempDBTagNum.equalsIgnoreCase(tempTagNum)) {
                CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_same_tag), Toast.LENGTH_SHORT);
                return false;
            } else return true;
        } else return true;
    }

    /*Local DB Start here*/
    private void getSpLocalDBData() {
        new Thread(() -> {
            List<Parcel> loadAllParcel = getDataManager().loadAllParcel();
            List<MotherResponse.AnimalMotherListBean> loadAllAnimal = getDataManager().getAnimalMotherList(getDataManager().getUserInfo().getUserId(), animalInfoBean != null ? animalInfoBean.getAnimalId() : "");
            if (parcelList.size() < 1) parcelList.clear();
            if (motherList.size() < 1) motherList.clear();
            parcelList.addAll(loadAllParcel);
            motherList.addAll(loadAllAnimal);
            handler.post(() -> {
                updateSp("mother");
                updateSp("parcel");
                spParcelNameAdapter.notifyDataSetChanged();
                spMotherNameAdapter.notifyDataSetChanged();
            });
        }).start();
    }

    private void doAddEditAnimalToLocalDB(String flag, JSONObject jsonObject) {
        new Thread(() -> {
            try {
                JSONObject obj = jsonObject.getJSONObject("animalDetail");

                Animal animal = new Animal();

                animal.setAnimalId(obj.getString("animalId"));
                animal.setParcel_id(obj.getString("parcel_id"));
                animal.setLotId(obj.getString("lotId"));
                animal.setTagNumber(obj.getString("tagNumber"));
                animal.setBornOrBuyDate(obj.getString("bornOrBuyDate"));
                animal.setMother(obj.getString("mother"));
                animal.setBoughtFrom(obj.getString("boughtFrom"));
                animal.setDescription(obj.getString("description"));
                animal.setDismant(obj.getString("dismant"));
                animal.setMarkingOrTagging(obj.getString("markingOrTagging"));
                String saleDate = obj.getString("saleDate");
                animal.setSaleDate(saleDate.isEmpty() ? null : CalenderUtils.getDateFormat(saleDate.contains("/") ? CalenderUtils.formatDate(saleDate, AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT) : saleDate, AppConstants.SERVER_TIMESTAMP_FORMAT));
                animal.setSalePrice(obj.getString("salePrice"));
                animal.setSaleQuantity(obj.getString("saleQuantity"));
                animal.setSaleTotalPrice(obj.getString("saleTotalPrice"));
                animal.setCostFrom(obj.getString("costFrom"));
                animal.setCostTo(obj.getString("costTo"));
                animal.setCostTotal(obj.getString("costTotal"));
                animal.setStatus(obj.getString("status"));
                animal.setRecordKey(obj.getString("recordKey"));
                animal.setCrd(obj.getString("crd"));
                animal.setUpd(obj.getString("upd"));

                switch (flag) {
                    case "add":
                        animal.setDataSync(AppConstants.DB_SYNC_TRUE);
                        animal.setEvent(AppConstants.DB_EVENT_ADD);
                        getDataManager().insertAnimal(animal);
                        break;

                    case "edit":
                        animal.setDataSync(AppConstants.DB_SYNC_TRUE);
                        animal.setEvent(AppConstants.DB_EVENT_EDIT);
                        getDataManager().updateAnimal(animal);
                        break;
                }
            } catch (Exception e) {
                AppLogger.d("database", "add or edit success");
            }
        }).start();
    }

    private void doAddEditAnimalToLocalDB(String flag) {
        //in offline case we use parcel id
        String tempParcel = parcelList.get(spParcel.getSelectedItemPosition()).getParcelId().trim();
        String tempLotId = etLotId.getText().toString().trim();
        String tempTagNum = etTagNumber.getText().toString().trim();
        String tempBBDate = tvBBDate.getText().toString().trim();
        String tempMother = motherList.get(spMother.getSelectedItemPosition()).getTagNumber().trim();
        String tempBoughtFrom = etBoughtFrom.getText().toString().trim();
        String tempDes = etDescription.getText().toString().trim();

        tempMother = (tempMother.equalsIgnoreCase(getString(R.string.select_mother)) | tempMother.equalsIgnoreCase(getString(R.string.none))) ? "" : tempMother;
        String finalTempMother = tempMother;


        new Thread(() -> {

            String tempDBTagNum = getDataManager().getTagNumber(getDataManager().getUserInfo().getUserId(), tempTagNum);
            tempDBTagNum = (tempDBTagNum == null) ? "" : tempDBTagNum;
            String finalTempDBTagNum = tempDBTagNum;
            handler.post(() -> {
                if (verifyInputs(tempParcel, tempLotId, tempTagNum, finalTempDBTagNum, tempBBDate, finalTempMother, tempBoughtFrom, tempDes)) {
                    Animal animal = new Animal();

                    animal.setParcel_id(tempParcel);
                    animal.setLotId(tempLotId);
                    animal.setTagNumber(tempTagNum);
                    animal.setBornOrBuyDate(CalenderUtils.formatDate(tempBBDate, AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT));
                    animal.setMother(finalTempMother);
                    animal.setBoughtFrom(tempBoughtFrom);
                    animal.setDescription(tempDes);

                    switch (flag) {
                        case "add":
                            String rKey = CalenderUtils.getTimestamp();
                            animal.setAnimalId(rKey);
                            animal.setDismant("0000-00-00");
                            animal.setSaleDate(CalenderUtils.getDateFormat("0000-00-00", AppConstants.SERVER_TIMESTAMP_FORMAT));
                            animal.setMarkingOrTagging("0000-00-00");
                            animal.setSalePrice("0");
                            animal.setSaleQuantity("0");
                            animal.setSaleTotalPrice("0");
                            animal.setCostFrom("0000-00-00");
                            animal.setCostTo("0000-00-00");
                            animal.setCostTotal("");
                            animal.setStatus("1");
                            animal.setCrd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                            animal.setUpd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                            animal.setRecordKey(rKey);
                            animal.setEvent(AppConstants.DB_EVENT_ADD);
                            animal.setDataSync(AppConstants.DB_SYNC_FALSE);

                            //set sync status true
                            getDataManager().setLocalDataExistForSync(true);
                            new Thread(() -> {
                                getDataManager().insertAnimal(animal);
                                handler.post(() -> {
                                    CommonUtils.showToast(getBaseActivity(), getString(R.string.animalAddSuccessMsg), Toast.LENGTH_SHORT);
                                    getBaseActivity().onBackPressed();
                                    if (animalInfoFragment != null) animalInfoFragment.hitApi();
                                });
                            }).start();
                            break;

                        case "edit":
                            animal.setAnimalId(animalInfoBean.getAnimalId());
                            animal.setDismant(animalInfoBean.getDismant());
                            animal.setSaleDate(animalInfoBean.getSaleDate().isEmpty() ? null : CalenderUtils.getDateFormat(animalInfoBean.getSaleDate().contains("/") ? CalenderUtils.formatDate(animalInfoBean.getSaleDate(), AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT) : animalInfoBean.getSaleDate(), AppConstants.SERVER_TIMESTAMP_FORMAT));
                            animal.setMarkingOrTagging(animalInfoBean.getMarkingOrTagging());
                            animal.setSalePrice(animalInfoBean.getSalePrice());
                            animal.setSaleQuantity(animalInfoBean.getSaleQuantity());
                            animal.setSaleTotalPrice(animalInfoBean.getSaleTotalPrice());
                            animal.setCostFrom(animalInfoBean.getCostFrom());
                            animal.setCostTo(animalInfoBean.getCostTo());
                            animal.setCostTotal(animalInfoBean.getCostTotal());
                            animal.setStatus(animalInfoBean.getStatus());
                            animal.setCrd(animalInfoBean.getCrd());
                            animal.setUpd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                            animal.setRecordKey(animalInfoBean.getRecordKey());
                            animal.setEvent(animalInfoBean.getAnimalId().equals(animalInfoBean.getRecordKey()) ? AppConstants.DB_EVENT_ADD : AppConstants.DB_EVENT_EDIT);
                            animal.setDataSync(AppConstants.DB_SYNC_FALSE);

                            //set sync status true
                            getDataManager().setLocalDataExistForSync(true);
                            new Thread(() -> {
                                getDataManager().updateAnimal(animal);
                                handler.post(() -> {
                                    CommonUtils.showToast(getBaseActivity(), getString(R.string.animalUpdateSuccessMsg), Toast.LENGTH_SHORT);
                                    getBaseActivity().onBackPressed();
                                    if (animalInfoFragment != null) animalInfoFragment.hitApi();
                                });
                            }).start();
                            break;
                    }
                }
            });
        }).start();
    }
    /*Local DB End here*/
}
