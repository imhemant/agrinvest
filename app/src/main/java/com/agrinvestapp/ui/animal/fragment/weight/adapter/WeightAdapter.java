package com.agrinvestapp.ui.animal.fragment.weight.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.WeightInfoResponse;
import com.agrinvestapp.utils.pagination.FooterLoader;
import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chauthai.swipereveallayout.ViewBinderHelper;

import java.util.List;

/**
 * Created by hemant
 * Date: 17/4/18
 * Time: 4:03 PM
 */

public class WeightAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEWTYPE_ITEM = 1;
    private final int VIEWTYPE_LOADER = 2;
    // This object helps you save/restore the open/close state of each view
    private final ViewBinderHelper viewBinderHelper = new ViewBinderHelper();
    private List<WeightInfoResponse.WeightListBean> list;
    private boolean showLoader;
    private ItemClickListener itemClickListener;

    public WeightAdapter(List<WeightInfoResponse.WeightListBean> list, ItemClickListener itemClickListener) {
        this.list = list;
        this.itemClickListener = itemClickListener;
        viewBinderHelper.setOpenOnlyOne(true);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder rvHolder, int position) {
        if (rvHolder instanceof FooterLoader) {
            FooterLoader loaderViewHolder = (FooterLoader) rvHolder;
            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }
            return;
        }
        WeightAdapter.ViewHolder holder = ((WeightAdapter.ViewHolder) rvHolder);

        WeightInfoResponse.WeightListBean bean = list.get(position);

        viewBinderHelper.bind(holder.swipe, bean.getRecordKey());

        holder.tvDate.setText(bean.getWeightDate());

        holder.tvWeight.setText(bean.getWeight());
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(bean.getWeightUnit().substring(0, 1).toUpperCase())
                .append(bean.getWeightUnit().substring(1).toLowerCase());
        holder.tvWeightType.setText(stringBuilder);
    }

    public void showLoading(boolean status) {
        showLoader = status;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case VIEWTYPE_ITEM:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_weight, parent, false);
                return new ViewHolder(view);

            case VIEWTYPE_LOADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pagination_item_loader, parent, false);
                return new FooterLoader(view);
        }
        return null;

    }

    @Override
    public int getItemViewType(int position) {
        if (position == list.size() - 1) {
            return showLoader ? VIEWTYPE_LOADER : VIEWTYPE_ITEM;
        }
        return VIEWTYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface ItemClickListener {
        void onTrashClick(int pos);
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvDate, tvWeight, tvWeightType;
        private ImageView trash;
        private SwipeRevealLayout swipe;

        ViewHolder(@NonNull View v) {
            super(v);
            tvDate = v.findViewById(R.id.tvDate);
            tvWeight = v.findViewById(R.id.tvWeight);
            tvWeightType = v.findViewById(R.id.tvWeightType);
            trash = v.findViewById(R.id.trash);
            swipe = v.findViewById(R.id.swipe);

            trash.setOnClickListener(this);
        }

        @Override
        public void onClick(@NonNull final View view) {
            if (getAdapterPosition() != -1) {
                swipe.close(true);
                itemClickListener.onTrashClick(getAdapterPosition());
            }
        }
    }

}

