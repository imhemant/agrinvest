package com.agrinvestapp.ui.animal.dialog;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.ui.base.BaseDialog;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.CalenderUtils;
import com.agrinvestapp.utils.CommonUtils;

import java.util.Calendar;
import java.util.Date;


public class CostToDateDialog extends BaseDialog implements View.OnClickListener {

    private static final String TAG = CostToDateDialog.class.getSimpleName();
    private static final String FROM = "from";
    private static final String TO = "to";
    private static final String COST = "cost";

    private CostToDateCallback callback;

    private EditText etTtlCost;
    private TextView tvFromDate, tvToDate;

    private StringBuilder fromDate, toDate;
    private String tempFromDate = "", tempToDate = "", tempTtlCost = "";
    private Boolean isFromSelect = false;
    // the callback received when the user "sets" the Date in the
// DatePickerDialog
    @NonNull
    private DatePickerDialog.OnDateSetListener datePicker = (view, selectedYear, selectedMonth, selectedDay) -> {
        selectedMonth += 1;

        if (isFromSelect) {
            fromDate = new StringBuilder("");
            fromDate.append(selectedDay).append("/").append(selectedMonth).append("/").append(selectedYear);

            tvFromDate.setText(CalenderUtils.formatDate(String.valueOf(fromDate), AppConstants.TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT));
        } else {
            toDate = new StringBuilder("");
            toDate.append(selectedDay).append("/").append(selectedMonth).append("/").append(selectedYear);
            tvToDate.setText(CalenderUtils.formatDate(String.valueOf(toDate), AppConstants.TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT));
        }
    };

    public static CostToDateDialog newInstance(String fromDate, String toDate, String ttlCost, CostToDateCallback callback) {

        Bundle args = new Bundle();
        args.putString(FROM, fromDate);
        args.putString(TO, toDate);
        args.putString(COST, ttlCost);
        CostToDateDialog fragment = new CostToDateDialog();
        fragment.setOnAddEditListener(callback);
        fragment.setArguments(args);
        return fragment;
    }

    private void setOnAddEditListener(CostToDateCallback callback) {
        this.callback = callback;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            tempFromDate = getArguments().getString(FROM);
            tempToDate = getArguments().getString(TO);
            tempTtlCost = getArguments().getString(COST);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_cost_to_date, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        etTtlCost = view.findViewById(R.id.etTtlCost);

        tvFromDate = view.findViewById(R.id.tvFromDate);
        tvFromDate.setOnClickListener(this);
        tvToDate = view.findViewById(R.id.tvToDate);
        tvToDate.setOnClickListener(this);

        Button btnAdd = view.findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);
        view.findViewById(R.id.btnCancel).setOnClickListener(this);

        if (!tempFromDate.equals("0000-00-00")) {
            btnAdd.setText(getString(R.string.update));
            tempFromDate = tempFromDate.contains("-") ? CalenderUtils.formatDate(tempFromDate, AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT) : tempFromDate;
            tempToDate = tempToDate.contains("-") ? CalenderUtils.formatDate(tempToDate, AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT) : tempToDate;

            tvFromDate.setText(tempFromDate);
            tvToDate.setText(tempToDate);
            etTtlCost.setText(tempTtlCost);
            etTtlCost.setSelection(tempTtlCost.length());
        }
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAdd:
                tempFromDate = tvFromDate.getText().toString().trim();
                tempToDate = tvToDate.getText().toString().trim();
                tempTtlCost = etTtlCost.getText().toString().trim();

                if (verifyInput(tempFromDate, tempToDate, tempTtlCost)) {
                    callback.onDataAdd(tempFromDate, tempToDate, tempTtlCost);
                    dismissDialog(TAG);
                }
                break;

            case R.id.btnCancel:
                dismissDialog(TAG);
                break;

            case R.id.tvFromDate:
                isFromSelect = true;
                getDate(tvFromDate.getText().toString().trim());
                break;

            case R.id.tvToDate:
                isFromSelect = false;
                getDate(tvToDate.getText().toString().trim());
                break;

        }
    }

    private boolean verifyInput(String fromDate, String toDate, String ttlCost) {
        if (fromDate.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_select_fdate), Toast.LENGTH_SHORT);
            return false;
        } else if (toDate.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_select_tdate), Toast.LENGTH_SHORT);
            return false;
        } else if (ttlCost.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_ttlCost), Toast.LENGTH_SHORT);
            return false;
        } else if (ttlCost.startsWith(".")) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_valid_ttlCost), Toast.LENGTH_SHORT);
            return false;
        } else if (Double.parseDouble(ttlCost) <= 0) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_ttlCost_zero), Toast.LENGTH_SHORT);
            return false;
        } else return true;
    }

    private void getDate(String selectedDate) {
        //date 26/06/2018

        Calendar newCalender = Calendar.getInstance();

        int day, month, year;

        if (selectedDate.isEmpty()) {
            day = newCalender.get(Calendar.DAY_OF_MONTH);
            month = newCalender.get(Calendar.MONTH);
            year = newCalender.get(Calendar.YEAR);
        } else {
            String[] date = selectedDate.split("/");

            day = Integer.parseInt(date[0]);
            month = Integer.parseInt(date[1]) - 1;
            year = Integer.parseInt(date[2]);
        }

        DatePickerDialog datePickerDialog = new DatePickerDialog(getBaseActivity(), datePicker, year, month, day);
        Date maxDate = CalenderUtils.getDateFormat(CalenderUtils.getCurrentDate(), AppConstants.TIMESTAMP_FORMAT);
        assert maxDate != null;
        datePickerDialog.getDatePicker().setMaxDate(maxDate.getTime());

        if (!isFromSelect && !tvFromDate.getText().toString().isEmpty() && tvToDate.getText().toString().isEmpty()) {
            Date cDate = CalenderUtils.getDateFormat(tvFromDate.getText().toString(), AppConstants.TIMESTAMP_FORMAT);
            assert cDate != null;
            datePickerDialog.getDatePicker().setMinDate(cDate.getTime());
        } else if (isFromSelect && tvFromDate.getText().toString().isEmpty() && !tvToDate.getText().toString().isEmpty()) {
            Date cDate = CalenderUtils.getDateFormat(tvToDate.getText().toString(), AppConstants.TIMESTAMP_FORMAT);
            assert cDate != null;
            datePickerDialog.getDatePicker().setMaxDate(cDate.getTime());
        } else if (isFromSelect && !tvFromDate.getText().toString().isEmpty() && !tvToDate.getText().toString().isEmpty()) {
            Date cDateMax = CalenderUtils.getDateFormat(tvToDate.getText().toString(), AppConstants.TIMESTAMP_FORMAT);
            assert cDateMax != null;
            datePickerDialog.getDatePicker().setMaxDate(cDateMax.getTime());
        } else if (!isFromSelect && !tvFromDate.getText().toString().isEmpty() && !tvToDate.getText().toString().isEmpty()) {
            Date cDateMin = CalenderUtils.getDateFormat(tvFromDate.getText().toString(), AppConstants.TIMESTAMP_FORMAT);
            assert cDateMin != null;
            datePickerDialog.getDatePicker().setMinDate(cDateMin.getTime());
        }
        datePickerDialog.show();
    }

}
