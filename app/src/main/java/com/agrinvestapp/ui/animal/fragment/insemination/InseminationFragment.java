package com.agrinvestapp.ui.animal.fragment.insemination;


import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.AnimalInfoResponse;
import com.agrinvestapp.data.model.api.InseminationInfoResponse;
import com.agrinvestapp.data.model.db.Insemination;
import com.agrinvestapp.ui.animal.fragment.insemination.adapter.InseminationAdapter;
import com.agrinvestapp.ui.animal.fragment.insemination.dialog.InseminationCallback;
import com.agrinvestapp.ui.animal.fragment.insemination.dialog.InseminationDialog;
import com.agrinvestapp.ui.base.BaseFragment;
import com.agrinvestapp.ui.base.dialog.DeleteDialog;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.AppLogger;
import com.agrinvestapp.utils.CalenderUtils;
import com.agrinvestapp.utils.CommonUtils;
import com.agrinvestapp.volley.VolleyJsonObjectListener;
import com.agrinvestapp.volley.VolleyRequest;
import com.agrinvestapp.volley.VolleyResponseListener;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class InseminationFragment extends BaseFragment implements View.OnClickListener, InseminationCallback {

    private TextView tvNoRecord;
    private RecyclerView rvInsemination;

    private InseminationAdapter inseminationAdapter;
    private List<InseminationInfoResponse.InseminationListBean> list;

    private AnimalInfoResponse.AnimalListBean animalInfoBean;

    private Handler handler = new Handler(Looper.getMainLooper());

    public static InseminationFragment newInstance(AnimalInfoResponse.AnimalListBean animalInfoBean) {

        Bundle args = new Bundle();
        args.putParcelable(AppConstants.ANIMAL_MODEL, animalInfoBean);
        InseminationFragment fragment = new InseminationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            animalInfoBean = getArguments().getParcelable(AppConstants.ANIMAL_MODEL);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_insemination, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
         /* toolbar view start */
        ImageView imgBack = view.findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);
        imgBack.setVisibility(View.VISIBLE);

        TextView tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.insemination);

        FrameLayout frameAdd = view.findViewById(R.id.frameAddT);
        frameAdd.setVisibility(View.VISIBLE);
        frameAdd.setOnClickListener(this);
        /* toolbar view end */
        view.findViewById(R.id.rlMain).setOnClickListener(this); //avoid bg click
        tvNoRecord = view.findViewById(R.id.tvNoRecord);

        rvInsemination = view.findViewById(R.id.rvInsemination);
        list = new ArrayList<>();

        inseminationAdapter = new InseminationAdapter(list, new InseminationAdapter.ItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                //not require
            }

            @Override
            public void onEditClick(int pos) {
//not require
            }

            @Override
            public void onDeleteClick(int pos) {
                DeleteDialog.newInstance(false, "", () -> {
                    if (isNetworkConnected(true)) {
                        getBaseActivity().doSyncAppDB(false, flag -> {
                            switch (flag) {
                                case AppConstants.ONLINE:
                                    doAddEditDelInsemination(pos, AppConstants.REQUEST_TYPE_DEL, list.get(pos));
                                    break;

                                case AppConstants.OFFLINE:
                                    doAddEditDelInseminationLocal(pos, AppConstants.REQUEST_TYPE_DEL, list.get(pos));
                                    break;
                            }
                        });
                    } else
                        doAddEditDelInseminationLocal(pos, AppConstants.REQUEST_TYPE_DEL, list.get(pos));
                }).show(getChildFragmentManager());
            }
        });
        rvInsemination.setAdapter(inseminationAdapter);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (isNetworkConnected(true)) {
            getBaseActivity().doSyncAppDB(false, flag -> {
                switch (flag) {
                    case AppConstants.ONLINE:
                        getInseminationInfo();
                        break;

                    case AppConstants.OFFLINE:
                        getInseminationInfoFromDB();
                        break;
                }
            });
        }
        else {
            getInseminationInfoFromDB();
        }
    }

    private void getInseminationInfo() {
        setLoading(true);

        HashMap<String, String> mParameterMap = new HashMap<>();
        mParameterMap.put("requestType", AppConstants.REQUEST_TYPE_INSEMINATION);
        mParameterMap.put("animalId", animalInfoBean.getAnimalId());
        mParameterMap.put("recordKey", animalInfoBean.getRecordKey());
        getDataManager().doServerTreatmentInfoListApiCall(getDataManager().getHeader(), mParameterMap)
                .getAsResponse(new VolleyResponseListener() {
                    @Override
                    public void onVolleyResponse(String response) {
                        setLoading(false);
                        InseminationInfoResponse infoResponse = getDataManager().mGson.fromJson(response, InseminationInfoResponse.class);

                        if (infoResponse.getStatus().equals("success")) {
                            list.clear();
                            list.addAll(infoResponse.getInseminationList());

                            updateUI();
                            if (!list.isEmpty()) {
                                //Todo manage local db add all data
                                doAddAllInseminationLocalDB(infoResponse.getInseminationList());
                            }

                            inseminationAdapter.notifyDataSetChanged();
                        } else {
                            updateUI();
                            if (infoResponse.getMessage().equals(getString(R.string.no_record_found))) {
                                list.clear();
                                inseminationAdapter.notifyDataSetChanged();
                            }
                            if (!infoResponse.getMessage().equals(getString(R.string.no_record_found)))
                                CommonUtils.showToast(getBaseActivity(), infoResponse.getMessage(), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        updateUI();
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    private void updateUI() {
        tvNoRecord.setVisibility(list.isEmpty() ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                getBaseActivity().onBackPressed();
                break;

            case R.id.frameAddT:
                if (list.size() < 10) {
                    InseminationDialog.newInstance(0, InseminationFragment.this).show(getChildFragmentManager());
                } else
                    CommonUtils.showToast(getBaseActivity(), getString(R.string.inseminationLimit), Toast.LENGTH_SHORT);

                break;
        }
    }

    private void doAddEditDelInsemination(int pos, String flag, InseminationInfoResponse.InseminationListBean bean) {
        setLoading(true);

        HashMap<String, String> mParams = new HashMap<>();
        mParams.put("requestType", flag);
        mParams.put("recordKey", animalInfoBean.getRecordKey());  //animal record key
        mParams.put("inseminationName", bean.getInseminationName());
        mParams.put("inseminationDate", bean.getInseminationDate());
        mParams.put("inseminationDescription", bean.getInseminationDescription());

        switch (flag) {
            case AppConstants.REQUEST_TYPE_ADD:
                mParams.put("inseminationId", bean.getInseminationId());
                mParams.put("inseminationRecordKey", bean.getRecordKey());
                break;

            case AppConstants.REQUEST_TYPE_DEL:
                mParams.put("inseminationId", bean.getInseminationId());
                mParams.put("inseminationRecordKey", bean.getRecordKey());
                break;
        }

        getDataManager().doServerInseminationAddDelApiCall(getDataManager().getHeader(), mParams)
                .getAsJsonObject(new VolleyJsonObjectListener() {

                    @Override
                    public void onVolleyJsonObject(JSONObject obj) {
                        setLoading(false);

                        try {
                            String status = obj.getString("status");
                            String message = obj.getString("message");

                            if (status.equals("success")) {

                                switch (flag) {
                                    case AppConstants.REQUEST_TYPE_ADD:
                                        JSONObject jsonObject = obj.getJSONObject("inseminationDetail");

                                        InseminationInfoResponse.InseminationListBean bean = new InseminationInfoResponse.InseminationListBean();

                                        bean.setInseminationId(jsonObject.getString("inseminationId"));
                                        bean.setAnimal_id(jsonObject.getString("animal_id"));
                                        bean.setInseminationName(jsonObject.getString("inseminationName"));
                                        bean.setInseminationDate(jsonObject.getString("inseminationDate"));
                                        bean.setInseminationDescription(jsonObject.getString("inseminationDescription"));
                                        bean.setRecordKey(jsonObject.getString("recordKey"));
                                        bean.setCrd(jsonObject.getString("crd"));
                                        bean.setUpd(jsonObject.getString("upd"));

                                        list.add(pos, bean);
                                        inseminationAdapter.notifyItemInserted(pos);

                                        rvInsemination.smoothScrollToPosition(pos);
                                        updateLocalDB(flag, list.get(pos));

                                        CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                                        break;

                                    case AppConstants.REQUEST_TYPE_DEL:
                                        updateLocalDB(flag, list.get(pos));

                                        list.remove(pos);
                                        inseminationAdapter.notifyItemRemoved(pos);

                                        CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                                        break;
                                }

                                updateUI();

                            } else
                                CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                        } catch (Exception e) {
                            CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyJsonException() {
                        setLoading(false);
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    @Override
    public void onAdd(InseminationInfoResponse.InseminationListBean bean) {
        if (isNetworkConnected(true)) {
            getBaseActivity().doSyncAppDB(false, flag -> {
                switch (flag) {
                    case AppConstants.ONLINE:
                        doAddEditDelInsemination(0, AppConstants.REQUEST_TYPE_ADD, bean);
                        break;

                    case AppConstants.OFFLINE:
                        doAddEditDelInseminationLocal(0, AppConstants.REQUEST_TYPE_ADD, bean);
                        break;
                }
            });
        }
        else doAddEditDelInseminationLocal(0, AppConstants.REQUEST_TYPE_ADD, bean);
    }

    /*Local DB Start here*/
    private void getInseminationInfoFromDB() {
        setLoading(true);

        new Thread(() -> {
            list.clear();
            List<InseminationInfoResponse.InseminationListBean> inseminationList = getDataManager().getInseminationList(animalInfoBean.getAnimalId());

            list.addAll(inseminationList);

            handler.post(() -> {
                setLoading(false);
                inseminationAdapter.notifyDataSetChanged();
                updateUI();
            });
        }).start();
    }

    private void doAddAllInseminationLocalDB(List<InseminationInfoResponse.InseminationListBean> infoResponse) {

        new Thread(() -> {
            getDataManager().deleteAllInsemination();

            List<Insemination> inseminationList = new ArrayList<>();
            for (InseminationInfoResponse.InseminationListBean mainBean : infoResponse) {
                Insemination insemination = new Insemination();

                insemination.setInseminationId(mainBean.getInseminationId());
                insemination.setAnimal_id(mainBean.getAnimal_id());
                insemination.setInseminationName(mainBean.getInseminationName());
                insemination.setInseminationDate(mainBean.getInseminationDate());
                insemination.setInseminationDescription(mainBean.getInseminationDescription());
                insemination.setRecordKey(mainBean.getRecordKey());
                insemination.setCrd(mainBean.getCrd());
                insemination.setUpd(mainBean.getUpd());
                insemination.setDataSync(AppConstants.DB_SYNC_TRUE);
                insemination.setEvent(AppConstants.DB_EVENT_ADD);

                inseminationList.add(insemination);
            }

            getDataManager().insertAllInsemination(inseminationList);
            AppLogger.d("database", "Insemination add success");
        }).start();
    }

    private void doAddEditDelInseminationLocal(int pos, String flag, InseminationInfoResponse.InseminationListBean bean) {
        new Thread(() -> {
            switch (flag) {
                case AppConstants.REQUEST_TYPE_ADD:
                    Insemination insemination = new Insemination();

                    insemination.setInseminationId(bean.getRecordKey());  //primary key
                    insemination.setAnimal_id(animalInfoBean.getAnimalId());
                    insemination.setInseminationName(bean.getInseminationName());
                    insemination.setInseminationDate(bean.getInseminationDate());
                    insemination.setInseminationDescription(bean.getInseminationDescription());
                    insemination.setRecordKey(bean.getRecordKey());
                    insemination.setCrd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    insemination.setUpd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    insemination.setDataSync(AppConstants.DB_SYNC_FALSE);
                    insemination.setEvent(AppConstants.DB_EVENT_ADD);

                    getDataManager().insertInsemination(insemination);

                    //set sync status true
                    getDataManager().setLocalDataExistForSync(true);
                    AppLogger.d("database", "Insemination add success");

                    handler.post(() -> {
                        list.add(pos, bean);
                        inseminationAdapter.notifyItemInserted(pos);
                        rvInsemination.smoothScrollToPosition(pos);
                        updateUI();
                    });
                    break;

                case AppConstants.REQUEST_TYPE_DEL:
                    if (bean.getInseminationId().equals(bean.getRecordKey())) {
                        getDataManager().deleteUsingInseminationId(bean.getInseminationId());
                    } else {
                        insemination = new Insemination();

                        insemination.setInseminationId(bean.getInseminationId());  //primary key
                        insemination.setAnimal_id(animalInfoBean.getAnimalId());
                        insemination.setInseminationName(bean.getInseminationName());
                        insemination.setInseminationDate(bean.getInseminationDate());
                        insemination.setInseminationDescription(bean.getInseminationDescription());
                        insemination.setRecordKey(bean.getRecordKey());
                        insemination.setCrd(bean.getCrd());
                        insemination.setUpd(bean.getUpd());
                        insemination.setDataSync(AppConstants.DB_SYNC_FALSE);
                        insemination.setEvent(AppConstants.DB_EVENT_DEL);

                        getDataManager().updateInsemination(insemination);

                        //set sync status true
                        getDataManager().setLocalDataExistForSync(true);
                    }
                    AppLogger.d("database", "Insemination del success");

                    handler.post(() -> {
                        list.remove(pos);
                        inseminationAdapter.notifyItemRemoved(pos);
                        updateUI();
                    });
                    break;
            }

        }).start();
    }

    private void updateLocalDB(String flag, InseminationInfoResponse.InseminationListBean bean) {
        new Thread(() -> {
            switch (flag) {
                case AppConstants.REQUEST_TYPE_ADD:
                    Insemination insemination = new Insemination();

                    insemination.setInseminationId(bean.getInseminationId());
                    insemination.setAnimal_id(bean.getAnimal_id());
                    insemination.setInseminationName(bean.getInseminationName());
                    insemination.setInseminationDate(bean.getInseminationDate());
                    insemination.setInseminationDescription(bean.getInseminationDescription());
                    insemination.setRecordKey(bean.getRecordKey());
                    insemination.setCrd(bean.getCrd());
                    insemination.setUpd(bean.getUpd());
                    insemination.setDataSync(AppConstants.DB_SYNC_TRUE);
                    insemination.setEvent(AppConstants.DB_EVENT_ADD);

                    getDataManager().insertInsemination(insemination);
                    AppLogger.d("database", "Insemination add success");
                    break;

                case AppConstants.REQUEST_TYPE_DEL:
                    getDataManager().deleteUsingInseminationId(bean.getInseminationId());
                    AppLogger.d("database", "Insemination del success");
                    break;
            }

        }).start();

    }
    /*Local DB Start here*/

}
