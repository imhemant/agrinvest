package com.agrinvestapp.ui.animal.fragment.weight.dialog;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.WeightInfoResponse;
import com.agrinvestapp.ui.animal.fragment.weight.WeightFragment;
import com.agrinvestapp.ui.base.BaseDialog;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.CalenderUtils;
import com.agrinvestapp.utils.CommonUtils;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by hemant
 * Date: 8/10/18.
 */

public class WeightDialog extends BaseDialog implements View.OnClickListener {

    private static final String TAG = WeightDialog.class.getSimpleName();

    private WeightCallback callback;

    private EditText etWeight;
    private TextView tvDate, tvKg, tvPound;
    private int pos;
    private String weightType = "pounds";
    // the callback received when the user "sets" the Date in the
    // DatePickerDialog
    @NonNull
    private DatePickerDialog.OnDateSetListener datePicker = (view, selectedYear, selectedMonth, selectedDay) -> {
        selectedMonth += 1;

        StringBuilder date = new StringBuilder("");
        date.append(selectedDay).append("/").append(selectedMonth).append("/").append(selectedYear);

        tvDate.setText(CalenderUtils.formatDate(String.valueOf(date), AppConstants.TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT));
    };

    public static WeightDialog newInstance(int pos, WeightFragment weightFragment) {

        Bundle args = new Bundle();

        WeightDialog fragment = new WeightDialog();
        fragment.setOnWeightAddEditListener(pos, weightFragment);
        fragment.setArguments(args);
        return fragment;
    }

    public void setOnWeightAddEditListener(int pos, WeightCallback callback) {
        this.pos = pos;
        this.callback = callback;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_add_edit_weight, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        etWeight = view.findViewById(R.id.etWeight);
        tvDate = view.findViewById(R.id.tvDate);
        TextView btnAdd = view.findViewById(R.id.btnAdd);
        TextView btnCancel = view.findViewById(R.id.btnCancel);

        /*if (treatmentDetailBean != null) {
            btnAdd.setText(getString(R.string.update));
            tvDate.setText(treatmentDetailBean.getTreatmentDate());
            etDescription.setText(treatmentDetailBean.getTreatmentDescription());
            buffer = new StringBuilder("");
            buffer.append(treatmentDetailBean.getTreatmentDescription().length()).append("/").append("150");
            tvDesCount.setText(buffer);
        }*/

        tvKg = view.findViewById(R.id.tvKg);
        tvKg.setOnClickListener(this);
        tvPound = view.findViewById(R.id.tvPound);
        tvPound.setOnClickListener(this);

        tvDate.setOnClickListener(this);
        btnAdd.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvDate:
                getDate();
                break;

            //btn Add or Edit
            case R.id.btnAdd:
                String tempDate = tvDate.getText().toString().trim();
                String tempWeight = etWeight.getText().toString().trim();
                if (verifyInput(tempDate, tempWeight)) {
                    WeightInfoResponse.WeightListBean bean = new WeightInfoResponse.WeightListBean();

                    bean.setWeightId("");
                    bean.setWeight(tempWeight);
                    bean.setWeightUnit(weightType);
                    bean.setWeightDate(tempDate);
                    bean.setRecordKey(CalenderUtils.getTimestamp());

                    callback.onAdd(bean);

                    dismissDialog(TAG);
                }
                break;

            case R.id.tvKg:
                weightType = "kg";
                tvKg.setTextColor(getResources().getColor(R.color.colorWhite));
                tvPound.setTextColor(getResources().getColor(R.color.grey));
                tvKg.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                tvPound.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                break;

            case R.id.tvPound:
                weightType = "pounds";
                tvPound.setTextColor(getResources().getColor(R.color.colorWhite));
                tvKg.setTextColor(getResources().getColor(R.color.grey));
                tvPound.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                tvKg.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                break;

            case R.id.btnCancel:
                dismissDialog(TAG);
                break;

        }
    }

    private boolean verifyInput(String date, String tempWeight) {
        if (date.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_select_date), Toast.LENGTH_SHORT);
            return false;
        } else if (tempWeight.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_weight), Toast.LENGTH_SHORT);
            return false;
        } else if (tempWeight.startsWith(".")) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_valid_weight), Toast.LENGTH_SHORT);
            return false;
        } else if (Double.parseDouble(tempWeight) <= 0) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_weight_zero), Toast.LENGTH_SHORT);
            return false;
        } else return true;
    }

    private void getDate() {
        String selectedDate = tvDate.getText().toString().trim();

        Calendar newCalender = Calendar.getInstance();

        int day, month, year;

        if (selectedDate.isEmpty()) {
            day = newCalender.get(Calendar.DAY_OF_MONTH);
            month = newCalender.get(Calendar.MONTH);
            year = newCalender.get(Calendar.YEAR);
        } else {
            String[] date = selectedDate.split("/");

            day = Integer.parseInt(date[0]);
            month = Integer.parseInt(date[1]) - 1;
            year = Integer.parseInt(date[2]);
        }

        DatePickerDialog datePickerDialog = new DatePickerDialog(getBaseActivity(), datePicker, year, month, day);
        Date maxDate = CalenderUtils.getDateFormat(CalenderUtils.getCurrentDate(), AppConstants.TIMESTAMP_FORMAT);
        assert maxDate != null;
        datePickerDialog.getDatePicker().setMaxDate(maxDate.getTime());

        datePickerDialog.show();
    }

}

