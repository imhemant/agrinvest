package com.agrinvestapp.ui.animal.fragment.insemination.dialog;

import com.agrinvestapp.data.model.api.InseminationInfoResponse;

public interface InseminationCallback {
    void onAdd(InseminationInfoResponse.InseminationListBean bean);
}
