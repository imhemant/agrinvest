package com.agrinvestapp.ui.animal.fragment.calving.dialog;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.CalvingInfoResponse;
import com.agrinvestapp.ui.animal.fragment.calving.CalvingFragment;
import com.agrinvestapp.ui.base.BaseDialog;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.CalenderUtils;
import com.agrinvestapp.utils.CommonUtils;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by hemant
 * Date: 8/10/18.
 */

public class CalvingDialog extends BaseDialog implements View.OnClickListener {

    private static final String TAG = CalvingDialog.class.getSimpleName();

    private CalvingCallback callback;

    private EditText etNumCalving;
    private TextView tvDate;
    private int pos;
    // the callback received when the user "sets" the Date in the
    // DatePickerDialog
    @NonNull
    private DatePickerDialog.OnDateSetListener datePicker = (view, selectedYear, selectedMonth, selectedDay) -> {
        selectedMonth += 1;

        StringBuilder date = new StringBuilder("");
        date.append(selectedDay).append("/").append(selectedMonth).append("/").append(selectedYear);

        tvDate.setText(CalenderUtils.formatDate(String.valueOf(date), AppConstants.TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT));
    };

    public static CalvingDialog newInstance(int pos, CalvingFragment calvingFragment) {

        Bundle args = new Bundle();

        CalvingDialog fragment = new CalvingDialog();
        fragment.setOnCalvingAddEditListener(pos, calvingFragment);
        fragment.setArguments(args);
        return fragment;
    }

    public void setOnCalvingAddEditListener(int pos, CalvingCallback callback) {
        this.pos = pos;
        this.callback = callback;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_add_edit_calving, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tvDate = view.findViewById(R.id.tvDate);
        etNumCalving = view.findViewById(R.id.etNumCalving);
        tvDate = view.findViewById(R.id.tvDate);
        TextView btnAdd = view.findViewById(R.id.btnAdd);
        TextView btnCancel = view.findViewById(R.id.btnCancel);

        /*if (treatmentDetailBean != null) {
            btnAdd.setText(getString(R.string.update));
            tvDate.setText(treatmentDetailBean.getTreatmentDate());
            etDescription.setText(treatmentDetailBean.getTreatmentDescription());
        }*/

        tvDate.setOnClickListener(this);
        btnAdd.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvDate:
                getDate();
                break;

            //btn Add or Edit
            case R.id.btnAdd:
                String tempNumCalving = etNumCalving.getText().toString().trim();
                String tempDate = tvDate.getText().toString().trim();
                if (verifyInput(tempNumCalving, tempDate)) {
                    CalvingInfoResponse.CalvingListBean bean = new CalvingInfoResponse.CalvingListBean();

                    bean.setCalvingId("");
                    bean.setCalvingNumber(tempNumCalving);
                    bean.setCalvingDate(tempDate);
                    bean.setRecordKey(CalenderUtils.getTimestamp());

                    callback.onAdd(bean);

                    dismissDialog(TAG);
                }
                break;

            case R.id.btnCancel:
                dismissDialog(TAG);
                break;

        }
    }

    private boolean verifyInput(String tempNumCalving, String date) {
        if (tempNumCalving.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_num_calving), Toast.LENGTH_SHORT);
            return false;
        } else if (date.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_select_date), Toast.LENGTH_SHORT);
            return false;
        } else return true;
    }

    private void getDate() {
        String selectedDate = tvDate.getText().toString().trim();

        Calendar newCalender = Calendar.getInstance();

        int day, month, year;

        if (selectedDate.isEmpty()) {
            day = newCalender.get(Calendar.DAY_OF_MONTH);
            month = newCalender.get(Calendar.MONTH);
            year = newCalender.get(Calendar.YEAR);
        } else {
            String[] date = selectedDate.split("/");

            day = Integer.parseInt(date[0]);
            month = Integer.parseInt(date[1]) - 1;
            year = Integer.parseInt(date[2]);
        }

        DatePickerDialog datePickerDialog = new DatePickerDialog(getBaseActivity(), datePicker, year, month, day);

        Date maxDate = CalenderUtils.getDateFormat(CalenderUtils.getCurrentDate(), AppConstants.TIMESTAMP_FORMAT);
        assert maxDate != null;
        datePickerDialog.getDatePicker().setMaxDate(maxDate.getTime());

        datePickerDialog.show();
    }

}

