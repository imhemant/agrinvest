package com.agrinvestapp.ui.animal.fragment.weight.dialog;

import com.agrinvestapp.data.model.api.WeightInfoResponse;

public interface WeightCallback {
    void onAdd(WeightInfoResponse.WeightListBean bean);
}
