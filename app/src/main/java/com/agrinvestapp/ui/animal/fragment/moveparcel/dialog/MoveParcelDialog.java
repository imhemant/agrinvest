package com.agrinvestapp.ui.animal.fragment.moveparcel.dialog;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.ParcelMovementInfoResponse;
import com.agrinvestapp.data.model.db.Parcel;
import com.agrinvestapp.ui.animal.adapter.SpParcelNameAdapter;
import com.agrinvestapp.ui.animal.fragment.moveparcel.MoveToParcelFragment;
import com.agrinvestapp.ui.base.BaseDialog;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.CalenderUtils;
import com.agrinvestapp.utils.CommonUtils;
import com.agrinvestapp.volley.VolleyJsonObjectListener;
import com.agrinvestapp.volley.VolleyRequest;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by hemant
 * Date: 8/10/18.
 */

public class MoveParcelDialog extends BaseDialog implements View.OnClickListener {

    private static final String TAG = MoveParcelDialog.class.getSimpleName();

    private MoveParcelCallback callback;

    private TextView tvDate;
    private Spinner spParcel;
    private int pos;
    private String parcel_id;

    private SpParcelNameAdapter spParcelNameAdapter;
    private List<Parcel> parcelList;
    // the callback received when the user "sets" the Date in the
    // DatePickerDialog
    @NonNull
    private DatePickerDialog.OnDateSetListener datePicker = (view, selectedYear, selectedMonth, selectedDay) -> {
        selectedMonth += 1;

        StringBuilder date = new StringBuilder("");
        date.append(selectedDay).append("/").append(selectedMonth).append("/").append(selectedYear);

        tvDate.setText(CalenderUtils.formatDate(String.valueOf(date), AppConstants.TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT));
    };

    public static MoveParcelDialog newInstance(int pos, String parcel_id, MoveToParcelFragment callback) {

        Bundle args = new Bundle();

        MoveParcelDialog fragment = new MoveParcelDialog();
        fragment.setOnMoveParcelAddEditListener(pos, parcel_id, callback);
        fragment.setArguments(args);
        return fragment;
    }

    private void setOnMoveParcelAddEditListener(int pos, String parcel_id, MoveParcelCallback callback) {
        this.pos = pos;
        this.parcel_id = parcel_id;
        this.callback = callback;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_different_parcel, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        spParcel = view.findViewById(R.id.spParcel);
        tvDate = view.findViewById(R.id.tvDate);
        TextView btnAdd = view.findViewById(R.id.btnAdd);
        TextView btnCancel = view.findViewById(R.id.btnCancel);

        //Edit
        /*if (treatmentDetailBean != null) {
            btnAdd.setText(getString(R.string.update));
            tvDate.setText(treatmentDetailBean.getTreatmentDate());
            etDescription.setText(treatmentDetailBean.getTreatmentDescription());
            buffer.append(treatmentDetailBean.getTreatmentDescription().length()).append("/").append("150");
            tvDesCount.setText(buffer);
        }*/

        parcelList = new ArrayList<>();
        Parcel bean = new Parcel();
        bean.setParcelId("");
        bean.setParcelName(getString(R.string.select_parcel));
        bean.setProperty_id("");
        parcelList.add(0, bean);

        spParcelNameAdapter = new SpParcelNameAdapter(getBaseActivity(), parcelList);
        spParcel.setAdapter(spParcelNameAdapter);

        tvDate.setOnClickListener(this);
        btnAdd.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (isNetworkConnected(true)) {
            if (getDataManager().getLocalDataExistForSync()) {
                getSpParcelLocalDBData();
            } else {
                getSpParcelApi();
            }
        } else getSpParcelLocalDBData();
    }

    private void getSpParcelLocalDBData() {
        new Thread(() -> {
            List<Parcel> loadAllParcel = getDataManager().loadAllParcelById(parcel_id);
            if (parcelList.size() < 1) parcelList.clear();
            parcelList.addAll(loadAllParcel);

            new Handler(Looper.getMainLooper()).post(() -> spParcelNameAdapter.notifyDataSetChanged());
        }).start();
    }

    private void getSpParcelApi() {
        setLoading(true);
        getDataManager().doServerGetParcelListApiCall(getDataManager().getHeader())
                .getAsJsonObject(new VolleyJsonObjectListener() {

                    @Override
                    public void onVolleyJsonObject(JSONObject jsonObject) {
                        setLoading(false);

                        try {
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            if (status.equals("success")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("parcelList");

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject obj = jsonArray.getJSONObject(i);
                                    Parcel parcel = new Parcel();
                                    parcel.setParcelId(obj.getString("parcelId"));
                                    parcel.setParcelName(obj.getString("parcelName"));
                                    parcel.setProperty_id(obj.getString("property_id"));
                                    parcel.setRecordKey(obj.getString("recordKey"));
                                    if (!parcel.getParcelId().equals(parcel_id))
                                        parcelList.add(parcel);
                                }

                                spParcelNameAdapter.notifyDataSetChanged();
                            } else {
                                if (!message.equalsIgnoreCase(getString(R.string.no_record_found2)))
                                    CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                            }
                        } catch (Exception e) {
                            CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyJsonException() {
                        setLoading(false);
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvDate:
                getDate();
                break;

            //btn Add or Edit
            case R.id.btnAdd:
                String tempParcel = parcelList.get(spParcel.getSelectedItemPosition()).getParcelId().trim();
                String tempDate = tvDate.getText().toString().trim();
                if (verifyInput(tempParcel, tempDate)) {
                    ParcelMovementInfoResponse.ParcelMovementListBean movementListBean = new ParcelMovementInfoResponse.ParcelMovementListBean();
                    movementListBean.setParcelMovementId("");
                    movementListBean.setMovementTo(tempParcel);
                    movementListBean.setToName(parcelList.get(spParcel.getSelectedItemPosition()).getParcelName().trim());
                    movementListBean.setMovementDate(tempDate);
                    movementListBean.setToRecordKey(parcelList.get(spParcel.getSelectedItemPosition()).getRecordKey().trim());
                    movementListBean.setRecordKey(CalenderUtils.getTimestamp());
                    callback.onAdd(movementListBean);

                    dismissDialog(TAG);
                }
                break;

            case R.id.btnCancel:
                dismissDialog(TAG);
                break;
        }
    }

    private boolean verifyInput(String name, String date) {
        if (name.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_select_parcel), Toast.LENGTH_SHORT);
            return false;
        } else if (date.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_select_date), Toast.LENGTH_SHORT);
            return false;
        } else return true;
    }

    private void getDate() {
        String selectedDate = tvDate.getText().toString().trim();

        Calendar newCalender = Calendar.getInstance();

        int day, month, year;

        if (selectedDate.isEmpty()) {
            day = newCalender.get(Calendar.DAY_OF_MONTH);
            month = newCalender.get(Calendar.MONTH);
            year = newCalender.get(Calendar.YEAR);
        } else {
            String[] date = selectedDate.split("/");

            day = Integer.parseInt(date[0]);
            month = Integer.parseInt(date[1]) - 1;
            year = Integer.parseInt(date[2]);
        }

        DatePickerDialog datePickerDialog = new DatePickerDialog(getBaseActivity(), datePicker, year, month, day);
        Date maxDate = CalenderUtils.getDateFormat(CalenderUtils.getCurrentDate(), AppConstants.TIMESTAMP_FORMAT);
        assert maxDate != null;
        datePickerDialog.getDatePicker().setMaxDate(maxDate.getTime());

        datePickerDialog.show();
    }

}

