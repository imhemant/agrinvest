package com.agrinvestapp.ui.animal.fragment.moveparcel.dialog;

import com.agrinvestapp.data.model.api.ParcelMovementInfoResponse;

public interface MoveParcelCallback {
    void onAdd(ParcelMovementInfoResponse.ParcelMovementListBean bean);
}
