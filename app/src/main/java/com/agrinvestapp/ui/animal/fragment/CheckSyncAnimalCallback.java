package com.agrinvestapp.ui.animal.fragment;

public interface CheckSyncAnimalCallback {
    void onSync(String data);

    void onCallApi();
}
