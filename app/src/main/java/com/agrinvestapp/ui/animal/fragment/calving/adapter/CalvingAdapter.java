package com.agrinvestapp.ui.animal.fragment.calving.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.CalvingInfoResponse;
import com.agrinvestapp.utils.pagination.FooterLoader;
import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chauthai.swipereveallayout.ViewBinderHelper;

import java.util.List;


/**
 * Created by hemant
 * Date: 17/4/18
 * Time: 4:03 PM
 */

public class CalvingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEWTYPE_ITEM = 1;
    private final int VIEWTYPE_LOADER = 2;
    // This object helps you save/restore the open/close state of each view
    private final ViewBinderHelper viewBinderHelper = new ViewBinderHelper();
    private List<CalvingInfoResponse.CalvingListBean> list;
    private boolean showLoader;
    private ItemClickListener itemClickListener;


    public CalvingAdapter(List<CalvingInfoResponse.CalvingListBean> list, ItemClickListener itemClickListener) {
        this.list = list;
        this.itemClickListener = itemClickListener;
        viewBinderHelper.setOpenOnlyOne(true);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder rvHolder, int position) {
        if (rvHolder instanceof FooterLoader) {
            FooterLoader loaderViewHolder = (FooterLoader) rvHolder;
            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }
            return;
        }
        CalvingAdapter.ViewHolder holder = ((CalvingAdapter.ViewHolder) rvHolder);

        CalvingInfoResponse.CalvingListBean bean = list.get(position);

        viewBinderHelper.bind(holder.swipe, bean.getRecordKey());

        holder.tvNum.setText(bean.getCalvingNumber());
        holder.tvDate.setText(bean.getCalvingDate());
    }

    public void showLoading(boolean status) {
        showLoader = status;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case VIEWTYPE_ITEM:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_calving, parent, false);
                return new ViewHolder(view);

            case VIEWTYPE_LOADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pagination_item_loader, parent, false);
                return new FooterLoader(view);
        }
        return null;

    }

    @Override
    public int getItemViewType(int position) {
        if (position == list.size() - 1) {
            return showLoader ? VIEWTYPE_LOADER : VIEWTYPE_ITEM;
        }
        return VIEWTYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface ItemClickListener {
        void onItemClick(int pos);

        void onEditClick(int pos);

        void onDeleteClick(int pos);
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvNum, tvDate;
        private FrameLayout frameDelete;
        private SwipeRevealLayout swipe;

        ViewHolder(@NonNull View v) {
            super(v);
            tvNum = v.findViewById(R.id.tvNum);
            tvDate = v.findViewById(R.id.tvDate);
            swipe = v.findViewById(R.id.swipe);
            frameDelete = v.findViewById(R.id.frameDelete);

            frameDelete.setOnClickListener(this);
        }

        @Override
        public void onClick(@NonNull final View view) {

            switch (view.getId()) {
                case R.id.frameDelete:
                    if (getAdapterPosition() != -1) {
                        swipe.close(true);
                        itemClickListener.onDeleteClick(getAdapterPosition());
                    }
                    break;

                default:
                    if (getAdapterPosition() != -1) {
                        swipe.close(true);
                        itemClickListener.onItemClick(getAdapterPosition());
                    }
                    break;
            }
        }
    }

}

