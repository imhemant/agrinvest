package com.agrinvestapp.ui.animal;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.agrinvestapp.R;
import com.agrinvestapp.ui.animal.fragment.AnimalInfoFragment;
import com.agrinvestapp.ui.base.BaseActivity;
import com.agrinvestapp.utils.AppConstants;

public class AnimalActivity extends BaseActivity {

    public static String fromPropertyId ="";
    private Boolean isReplace = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);
        fromPropertyId = "";
        if (getIntent()!=null && getIntent().hasExtra(AppConstants.KEY_FROM_PROPERTY)){
            fromPropertyId = getIntent().getStringExtra(AppConstants.KEY_FROM_PROPERTY);
        }

        replaceFragment(new AnimalInfoFragment(), R.id.animalFrame);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isReplace){
            Fragment fragment = getCurrentFragment();
            assert fragment != null;
            if (fragment instanceof AnimalInfoFragment) {
                AnimalInfoFragment infoFragment = (AnimalInfoFragment) fragment;
                infoFragment.hitApi();
            }
        }else isReplace = false;
    }
}
