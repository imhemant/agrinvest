package com.agrinvestapp.ui.animal.fragment.insemination.dialog;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.InseminationInfoResponse;
import com.agrinvestapp.ui.animal.fragment.insemination.InseminationFragment;
import com.agrinvestapp.ui.base.BaseDialog;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.CalenderUtils;
import com.agrinvestapp.utils.CommonUtils;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by hemant
 * Date: 8/10/18.
 */

public class InseminationDialog extends BaseDialog implements View.OnClickListener {

    private static final String TAG = InseminationDialog.class.getSimpleName();

    private InseminationCallback callback;

    private EditText etName, etDescription;
    private TextView tvDate, tvDesCount;
    private int pos;
    private StringBuilder builder;
    // the callback received when the user "sets" the Date in the
    // DatePickerDialog
    @NonNull
    private DatePickerDialog.OnDateSetListener datePicker = (view, selectedYear, selectedMonth, selectedDay) -> {
        selectedMonth += 1;

        StringBuilder date = new StringBuilder("");
        date.append(selectedDay).append("/").append(selectedMonth).append("/").append(selectedYear);

        tvDate.setText(CalenderUtils.formatDate(String.valueOf(date), AppConstants.TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT));
    };

    public static InseminationDialog newInstance(int pos, InseminationFragment inseminationFragment) {

        Bundle args = new Bundle();

        InseminationDialog fragment = new InseminationDialog();
        fragment.setOnInseminationAddListener(pos, inseminationFragment);
        fragment.setArguments(args);
        return fragment;
    }

    public void setOnInseminationAddListener(int pos, InseminationCallback callback) {
        this.pos = pos;
        this.callback = callback;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_add_edit_insemination, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        etName = view.findViewById(R.id.etName);

        etDescription = view.findViewById(R.id.etDescription);
        tvDate = view.findViewById(R.id.tvDate);
        tvDesCount = view.findViewById(R.id.tvDesCount);
        TextView btnAdd = view.findViewById(R.id.btnAdd);
        TextView btnCancel = view.findViewById(R.id.btnCancel);

        //edit case
        /*if (treatmentDetailBean != null) {
            btnAdd.setText(getString(R.string.update));
            tvDate.setText(treatmentDetailBean.getTreatmentDate());
            etDescription.setText(treatmentDetailBean.getTreatmentDescription());
            buffer.append(treatmentDetailBean.getTreatmentDescription().length()).append("/").append("150");
            tvDesCount.setText(buffer);
        }*/

        tvDate.setOnClickListener(this);
        btnAdd.setOnClickListener(this);
        btnCancel.setOnClickListener(this);

        etDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                builder = new StringBuilder("");
                builder.append(s.length()).append("/").append("150");
                tvDesCount.setText(builder);
            }
        });
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.tvDate:
                getDate();
                break;

            //btn Add or Edit
            case R.id.btnAdd:
                String tempName = etName.getText().toString().trim();
                String tempDate = tvDate.getText().toString().trim();
                String tempDes = etDescription.getText().toString().trim();
                if (verifyInput(tempName, tempDate, tempDes)) {
                    InseminationInfoResponse.InseminationListBean bean = new InseminationInfoResponse.InseminationListBean();
                    bean.setInseminationId("");
                    bean.setInseminationName(tempName);
                    bean.setInseminationDate(tempDate);
                    bean.setInseminationDescription(tempDes);
                    bean.setRecordKey(CalenderUtils.getTimestamp());

                    callback.onAdd(bean);

                    dismissDialog(TAG);
                }
                break;

            case R.id.btnCancel:
                dismissDialog(TAG);
                break;

        }
    }

    private boolean verifyInput(String name, String date, String description) {
        if (name.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_insemination), Toast.LENGTH_SHORT);
            return false;
        } else if (!(name.length() >= 3)) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_insemination_length), Toast.LENGTH_SHORT);
            return false;
        } else if (date.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_select_date), Toast.LENGTH_SHORT);
            return false;
        } else if (description.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_des), Toast.LENGTH_SHORT);
            return false;
        } else return true;
    }

    private void getDate() {
        String selectedDate = tvDate.getText().toString().trim();

        Calendar newCalender = Calendar.getInstance();

        int day, month, year;

        if (selectedDate.isEmpty()) {
            day = newCalender.get(Calendar.DAY_OF_MONTH);
            month = newCalender.get(Calendar.MONTH);
            year = newCalender.get(Calendar.YEAR);
        } else {
            String[] date = selectedDate.split("/");

            day = Integer.parseInt(date[0]);
            month = Integer.parseInt(date[1]) - 1;
            year = Integer.parseInt(date[2]);
        }

        DatePickerDialog datePickerDialog = new DatePickerDialog(getBaseActivity(), datePicker, year, month, day);
        Date maxDate = CalenderUtils.getDateFormat(CalenderUtils.getCurrentDate(), AppConstants.TIMESTAMP_FORMAT);
        assert maxDate != null;
        datePickerDialog.getDatePicker().setMaxDate(maxDate.getTime());

        datePickerDialog.show();
    }

}

