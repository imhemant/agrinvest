package com.agrinvestapp.ui.animal.fragment.treatments.dialog;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.ui.base.BaseDialog;
import com.agrinvestapp.utils.CommonUtils;


public class TreatmentNameDialog extends BaseDialog implements View.OnClickListener {

    private static final String TAG = TreatmentNameDialog.class.getSimpleName();

    private TreatmentCallback callback;

    private EditText etName;
    private int pos;
    private String name;

    private Handler handler = new Handler(Looper.getMainLooper());

    public void setOnTreatmentAddEditListener(int pos, String name, TreatmentCallback callback) {
        this.pos = pos;
        this.name = name;
        this.callback = callback;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_add_edit_treatment_name, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        etName = view.findViewById(R.id.etName);
        etName.setText(name);

        TextView tvTitle = view.findViewById(R.id.tvTitle);
        TextView btnAdd = view.findViewById(R.id.btnAdd);
        TextView btnCancel = view.findViewById(R.id.btnCancel);

        if (!name.isEmpty()) {
            tvTitle.setText(getString(R.string.edit_treatment));
            btnAdd.setText(getString(R.string.update));  //update_treatment
            etName.setSelection(name.length());
        }

        btnAdd.setOnClickListener(this);
        btnCancel.setOnClickListener(this);

    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnAdd:
                String tempName = etName.getText().toString().trim();
                if (verifyInput(tempName)) {
                    if (name.isEmpty())callback.onAdd(tempName);
                    else callback.onEdit(pos,tempName);
                    dismissDialog(TAG);
                }
                break;

            case R.id.btnCancel:
                dismissDialog(TAG);
                break;

        }
    }

    private boolean verifyInput(String treatName) {
        if (treatName.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_treat), Toast.LENGTH_SHORT);
            return false;
        } else if (!(treatName.length() >= 3)) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_treat_length), Toast.LENGTH_SHORT);
            return false;
        } else return true;
    }

}
