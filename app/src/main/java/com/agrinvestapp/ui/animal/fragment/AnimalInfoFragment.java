package com.agrinvestapp.ui.animal.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.AnimalInfoResponse;
import com.agrinvestapp.data.model.db.Animal;
import com.agrinvestapp.ui.animal.AnimalActivity;
import com.agrinvestapp.ui.animal.adapter.AnimalInfoAdapter;
import com.agrinvestapp.ui.base.BaseFragment;
import com.agrinvestapp.ui.base.ClickListener;
import com.agrinvestapp.ui.base.dialog.DeleteDialog;
import com.agrinvestapp.ui.base.dialog.PlanUpgradeDialog;
import com.agrinvestapp.ui.main.fragment.profile.PlanActivity;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.AppLogger;
import com.agrinvestapp.utils.AppUtils;
import com.agrinvestapp.utils.CalenderUtils;
import com.agrinvestapp.utils.CommonUtils;
import com.agrinvestapp.volley.VolleyJsonObjectListener;
import com.agrinvestapp.volley.VolleyRequest;
import com.agrinvestapp.volley.VolleyResponseListener;
import com.android.volley.VolleyError;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AnimalInfoFragment extends BaseFragment implements View.OnClickListener {

    private TextView tvNoRecord;
    private Button btnAddAnimal;
    private FrameLayout frameAdd;

    private AnimalInfoAdapter adapter;

    private List<AnimalInfoResponse.AnimalListBean> list;

    private Handler handler = new Handler(Looper.getMainLooper());
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_animal_info, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        /* toolbar view start */
        ImageView imgBack = view.findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);
        imgBack.setVisibility(View.VISIBLE);

        TextView tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.animal); //animal_info
        /* toolbar view end */
        view.findViewById(R.id.rlMain).setOnClickListener(this); //avoid bg click

        RecyclerView rvAnimals = view.findViewById(R.id.rvAnimals);
        list = new ArrayList<>();

        adapter = new AnimalInfoAdapter(list, new ClickListener() {
            @Override
            public void onItemClick(int pos) {
                AnimalDetailFragment animalDetailFragment = new AnimalDetailFragment();
                animalDetailFragment.setInstance(AnimalInfoFragment.this, list.get(pos));
                getBaseActivity().addFragment(animalDetailFragment, R.id.animalFrame, true);
            }

            @Override
            public void onEditClick(int pos) {
                AddEditAnimalFragment animalFragment = new AddEditAnimalFragment();
                animalFragment.setInstance(AnimalInfoFragment.this, false);
                animalFragment.setAnimalModel(list.get(pos));
                getBaseActivity().addFragment(animalFragment, R.id.animalFrame, true);
            }

            @Override
            public void onDeleteClick(int pos) {
                new Thread(() -> {
                    String animalExpenseStatus = getDataManager().getExpenseApplyStatus(list.get(pos).getAnimalId(), list.get(pos).getRecordKey());
                    String msg = (animalExpenseStatus == null) ? "" : getString(R.string.deleteExpenseMsg);

                    handler.post(() -> DeleteDialog.newInstance(animalExpenseStatus != null, msg, () -> {
                        if (isNetworkConnected(true)) {
                            getBaseActivity().doSyncAppDB(false, flag -> {
                                switch (flag) {
                                    case AppConstants.ONLINE:
                                        if (!list.get(pos).getAnimalId().equals(list.get(pos).getRecordKey()))
                                            doDeleteAnimalInfo(pos);
                                        else
                                            doDeleteAnimalFromLocalDB(AppConstants.ONLINE, pos, list.get(pos));
                                        break;

                                    case AppConstants.OFFLINE:
                                        doDeleteAnimalFromLocalDB(AppConstants.OFFLINE, pos, list.get(pos));
                                        break;
                                }
                            });

                        } else doDeleteAnimalFromLocalDB(AppConstants.OFFLINE, pos, list.get(pos));
                    }).show(getChildFragmentManager()));
                }).start();
            }
        });

        rvAnimals.setAdapter(adapter);

        tvNoRecord = view.findViewById(R.id.tvNoRecord);
        frameAdd = view.findViewById(R.id.frameAdd);
        frameAdd.setOnClickListener(this);
        btnAddAnimal = view.findViewById(R.id.btnAddAnimal);
        btnAddAnimal.setOnClickListener(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hitApi();
        initAd(view);
    }

    private void initAd(View view) {
        AdView mAdView = view.findViewById(R.id.adView);
        mAdView.setVisibility(View.VISIBLE);
        AppUtils.setAdBanner(mAdView, frameAdd);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                AppUtils.setMargins(frameAdd, 0, 0, 15, 15);
                mAdView.setVisibility(View.GONE);
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when when the user is about to return
                // to the app after tapping on an ad.
            }
        });
    }

    public void hitApi() {
        if (isNetworkConnected(true)) {
            getBaseActivity().doSyncAppDB(false, flag -> {
               /* if (isDashBoardController())
                    getAnimalInfo();
                else getAnimalInfoFromDB();*/

                getAnimalInfoFromDB();
            });
        }else getAnimalInfoFromDB();
    }

    private void updateView() {
        if (AnimalActivity.fromPropertyId.isEmpty()){
            if (list.isEmpty()) {
                frameAdd.setVisibility(View.GONE);
                tvNoRecord.setVisibility(View.VISIBLE);
                btnAddAnimal.setVisibility(View.VISIBLE);
            }
        }else{
            frameAdd.setVisibility(View.GONE);
            btnAddAnimal.setVisibility(View.GONE);
            if (list.isEmpty())tvNoRecord.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                getBaseActivity().onBackPressed();
                break;

            case R.id.btnAddAnimal:
                btnAddAnimal.setClickable(false);
                AddEditAnimalFragment animalFragment = new AddEditAnimalFragment();
                animalFragment.setInstance(this, true);
                getBaseActivity().addFragment(animalFragment, R.id.animalFrame, true);

                new Handler().postDelayed(() -> btnAddAnimal.setClickable(true), 2000);
                break;

            case R.id.frameAdd:
                //free user can add upto 50 animal
                String planStatus = getDataManager().getUserInfo().getSubscriptionPlan();
                if (planStatus.equalsIgnoreCase(AppConstants.PLAN_FREE) && list.size() < AppConstants.FREE_ANIMAL_LIMIT) {
                    addAnimal();
                } else if (!planStatus.equalsIgnoreCase(AppConstants.PLAN_FREE)) {
                    addAnimal();
                } else {
                    PlanUpgradeDialog.newInstance(getString(R.string.alert_free_animal), getString(R.string.upgrade_to_premium_plans), () -> startActivity(new Intent(getBaseActivity(), PlanActivity.class))).show(getChildFragmentManager());
                }
                break;
        }
    }

    private void addAnimal() {
        frameAdd.setClickable(false);
        AddEditAnimalFragment animalFragment = new AddEditAnimalFragment();
        animalFragment.setInstance(this, true);
        getBaseActivity().addFragment(animalFragment, R.id.animalFrame, true);

        new Handler().postDelayed(() -> frameAdd.setClickable(true), 2000);
    }

    private void getAnimalInfo() {
        setLoading(true);
        getDataManager().doServerAnimalInfoApiCall(getDataManager().getHeader())
                .getAsResponse(new VolleyResponseListener() {
                    @Override
                    public void onVolleyResponse(String response) {
                        setLoading(false);
                        AnimalInfoResponse animalInfoResponse = getDataManager().mGson.fromJson(response, AnimalInfoResponse.class);

                        if (animalInfoResponse.getStatus().equals("success")) {
                            list.clear();
                            list.addAll(animalInfoResponse.getAnimalList());

                            updateView();
                            if (!list.isEmpty()) {
                                frameAdd.setVisibility(View.VISIBLE);
                                tvNoRecord.setVisibility(View.GONE);
                                btnAddAnimal.setVisibility(View.GONE);

                                //Todo manage local db add all data
                                updateAnimalDataDB(animalInfoResponse);
                            }

                            adapter.notifyDataSetChanged();
                        } else {
                            updateView();
                            if (animalInfoResponse.getMessage().equals(getString(R.string.no_record_found))) {
                                list.clear();
                                adapter.notifyDataSetChanged();
                            }
                            if (!animalInfoResponse.getMessage().equals(getString(R.string.no_record_found)))
                                CommonUtils.showToast(getBaseActivity(), animalInfoResponse.getMessage(), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        updateView();
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    private void doDeleteAnimalInfo(int pos) {
        setLoading(true);
        AnimalInfoResponse.AnimalListBean animalListBean = list.get(pos);
        HashMap<String, String> mParams = new HashMap<>();
        mParams.put("recordKey", animalListBean.getRecordKey());

        getDataManager().doServerDeleteAnimalInfoApiCall(getDataManager().getHeader(), mParams)
                .getAsJsonObject(new VolleyJsonObjectListener() {

                    @Override
                    public void onVolleyJsonObject(JSONObject jsonObject) {
                        setLoading(false);
                        try {
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            if (status.equals("success")) {
                                //Todo delete property from local db Done
                                doDeleteAnimalFromLocalDB(AppConstants.ONLINE, pos, list.get(pos));
                                updateView();
                                CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                            } else
                                CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                        } catch (Exception e) {
                            CommonUtils.showToast(getBaseActivity(), getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyJsonException() {
                        setLoading(false);
                        CommonUtils.showToast(getBaseActivity(), getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    private Boolean isDashBoardController(){
        return AnimalActivity.fromPropertyId.isEmpty();
    }

    /*Local DB start here*/
    private void getAnimalInfoFromDB() {
        setLoading(true);
        new Thread(() -> {
            //All animal list
            if (isDashBoardController()){
                List<AnimalInfoResponse.AnimalListBean> localList = getDataManager().getAnimalList(getDataManager().getUserInfo().getUserId());
                setLoading(false);

                //update on ui thread
                if (!localList.isEmpty()) {
                    handler.post(() -> {
                        frameAdd.setVisibility(View.VISIBLE);
                        tvNoRecord.setVisibility(View.GONE);
                        btnAddAnimal.setVisibility(View.GONE);

                        list.clear();
                        list.addAll(localList);

                        adapter.notifyDataSetChanged();
                        updateView();
                    });
                } else handler.post(() -> {
                    list.clear();
                    adapter.notifyDataSetChanged();
                    updateView();
                });
            }else {
                //property animal list
                List<AnimalInfoResponse.AnimalListBean> localList = getDataManager().getPropertyAnimalList(AnimalActivity.fromPropertyId);
                setLoading(false);

                //update on ui thread
                if (!localList.isEmpty()) {
                    handler.post(() -> {
                      /*  frameAdd.setVisibility(View.VISIBLE);
                        tvNoRecord.setVisibility(View.GONE);
                        btnAddAnimal.setVisibility(View.GONE);*/
                        list.clear();
                        list.addAll(localList);

                        adapter.notifyDataSetChanged();
                        updateView();
                    });
                } else handler.post(() -> {
                    list.clear();
                    adapter.notifyDataSetChanged();
                    updateView();
                });
            }
        }).start();
    }

    private void doDeleteAnimalFromLocalDB(String flag, int pos, AnimalInfoResponse.AnimalListBean animal) {
        new Thread(() -> {
            switch (flag) {
                case AppConstants.ONLINE:
                    //delete here
                    getDataManager().deleteUsingAnimalId(animal.getAnimalId());
                    AppLogger.d("database", "animal delete success");

                    handler.post(() -> {
                        list.remove(pos);
                        adapter.notifyItemRemoved(pos);
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.animalDelSuccessMsg), Toast.LENGTH_SHORT);
                        updateView();
                    });
                    break;

                case AppConstants.OFFLINE:
                    if (animal.getAnimalId().equals(animal.getRecordKey())) {
                        //delete here
                        getDataManager().deleteUsingAnimalId(animal.getAnimalId());
                        AppLogger.d("database", "animal delete success");

                        handler.post(() -> {
                            list.remove(pos);
                            adapter.notifyItemRemoved(pos);
                            CommonUtils.showToast(getBaseActivity(), getString(R.string.animalDelSuccessMsg), Toast.LENGTH_SHORT);
                            updateView();
                        });
                    } else {
                        //update event
                        Animal dbAnimalBean = new Animal();

                        dbAnimalBean.setAnimalId(animal.getAnimalId());
                        dbAnimalBean.setParcel_id(animal.getParcel_id());
                        dbAnimalBean.setLotId(animal.getLotId());
                        dbAnimalBean.setTagNumber(animal.getTagNumber());
                        dbAnimalBean.setBornOrBuyDate(animal.getBornOrBuyDate());
                        dbAnimalBean.setMother(animal.getMother());
                        dbAnimalBean.setBoughtFrom(animal.getBoughtFrom());
                        dbAnimalBean.setDescription(animal.getDescription());
                        dbAnimalBean.setDismant(animal.getDismant());
                        dbAnimalBean.setSaleDate(animal.getSaleDate().isEmpty() ? null : CalenderUtils.getDateFormat(animal.getSaleDate().contains("/") ? CalenderUtils.formatDate(animal.getSaleDate(), AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT) : animal.getSaleDate(), AppConstants.SERVER_TIMESTAMP_FORMAT));
                        dbAnimalBean.setMarkingOrTagging(animal.getMarkingOrTagging());
                        dbAnimalBean.setSalePrice(animal.getSalePrice());
                        dbAnimalBean.setSaleQuantity(animal.getSaleQuantity());
                        dbAnimalBean.setSaleTotalPrice(animal.getSaleTotalPrice());
                        dbAnimalBean.setCostFrom(animal.getCostFrom());
                        dbAnimalBean.setCostTo(animal.getCostTo());
                        dbAnimalBean.setCostTotal(animal.getCostTotal());
                        dbAnimalBean.setStatus(animal.getStatus());
                        dbAnimalBean.setRecordKey(animal.getRecordKey());
                        dbAnimalBean.setCrd(animal.getCrd());
                        dbAnimalBean.setUpd(animal.getUpd());
                        dbAnimalBean.setEvent(AppConstants.DB_EVENT_DEL);
                        dbAnimalBean.setDataSync(AppConstants.DB_SYNC_FALSE);

                        getDataManager().updateAnimal(dbAnimalBean);
                        AppLogger.d("database", "animal delete success");

                        //set sync status true
                        getDataManager().setLocalDataExistForSync(true);

                        handler.post(() -> {
                            list.remove(pos);
                            adapter.notifyItemRemoved(pos);
                            CommonUtils.showToast(getBaseActivity(), getString(R.string.animalDelSuccessMsg), Toast.LENGTH_SHORT);
                            updateView();
                        });
                    }
                    break;
            }
        }).start();
    }

    private void updateAnimalDataDB(AnimalInfoResponse animalInfoResponse) {
        new Thread(() -> {
            AppLogger.d("database", "property, rain data and parcel table clear success");
            getDataManager().deleteAllAnimal();
            List<Animal> animalList = new ArrayList<>();

            for (AnimalInfoResponse.AnimalListBean animalListBean : animalInfoResponse.getAnimalList()) {

                Animal animal = new Animal();

                animal.setAnimalId(animalListBean.getAnimalId());
                animal.setParcel_id(animalListBean.getParcel_id());
                animal.setLotId(animalListBean.getLotId());
                animal.setTagNumber(animalListBean.getTagNumber());
                animal.setBornOrBuyDate(animalListBean.getBornOrBuyDate());
                animal.setMother(animalListBean.getMother());
                animal.setBoughtFrom(animalListBean.getBoughtFrom());
                animal.setDescription(animalListBean.getDescription());
                animal.setDismant(animalListBean.getDismant());
                animal.setSaleDate(animalListBean.getSaleDate().isEmpty() ? null : CalenderUtils.getDateFormat(animalListBean.getSaleDate().contains("/") ? CalenderUtils.formatDate(animalListBean.getSaleDate(), AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT) : animalListBean.getSaleDate(), AppConstants.SERVER_TIMESTAMP_FORMAT));
                animal.setMarkingOrTagging(animalListBean.getMarkingOrTagging());
                animal.setSalePrice(animalListBean.getSalePrice());
                animal.setSaleQuantity(animalListBean.getSaleQuantity());
                animal.setSaleTotalPrice(animalListBean.getSaleTotalPrice());
                animal.setCostFrom(animalListBean.getCostFrom());
                animal.setCostTo(animalListBean.getCostTo());
                animal.setCostTotal(animalListBean.getCostTotal());
                animal.setStatus(animalListBean.getStatus());
                animal.setRecordKey(animalListBean.getRecordKey());
                animal.setCrd(animalListBean.getCrd());
                animal.setUpd(animalListBean.getUpd());
                animal.setEvent(AppConstants.DB_EVENT_ADD);
                animal.setDataSync(AppConstants.DB_SYNC_TRUE);
                animalList.add(animal);
            }
            getDataManager().insertAllAnimal(animalList);
            AppLogger.d("database", "Db Add Animal done");
        }).start();
    }
    /*Local DB end here*/
}
