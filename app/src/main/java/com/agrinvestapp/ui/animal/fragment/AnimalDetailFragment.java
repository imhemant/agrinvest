package com.agrinvestapp.ui.animal.fragment;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.AnimalInfoResponse;
import com.agrinvestapp.data.model.db.Animal;
import com.agrinvestapp.ui.animal.dialog.CostToDateDialog;
import com.agrinvestapp.ui.animal.fragment.calving.CalvingFragment;
import com.agrinvestapp.ui.animal.fragment.insemination.InseminationFragment;
import com.agrinvestapp.ui.animal.fragment.moveparcel.MoveToParcelFragment;
import com.agrinvestapp.ui.animal.fragment.pregnancy.PregnancyFragment;
import com.agrinvestapp.ui.animal.fragment.treatments.TreatmentFragment;
import com.agrinvestapp.ui.animal.fragment.weight.WeightFragment;
import com.agrinvestapp.ui.base.BaseFragment;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.AppLogger;
import com.agrinvestapp.utils.CalenderUtils;
import com.agrinvestapp.utils.CommonUtils;
import com.agrinvestapp.volley.VolleyJsonObjectListener;
import com.agrinvestapp.volley.VolleyRequest;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class AnimalDetailFragment extends BaseFragment implements View.OnClickListener, BaseFragment.FragmentCallback, TextWatcher {

    private DecimalFormat decimalFormat = new DecimalFormat(".##");
    private AnimalInfoFragment animalInfoFragment;
    private AnimalInfoResponse.AnimalListBean animalInfoBean;
    private ImageView imgBBDate, imgMTDate, imgDismantDate, imgSaleDate, imgCtoDate;
    private CheckBox cbBBDate, cbMTDate, cbDismantDate, cbSale, cbCToDate;
    private TextView tvParcelTitle, tvBBDate, tvMTDate, tvDismantDate, tvSaleDate, tvSaleAmt, tvCtoDate;
    private EditText etSalePrice, etSaleQty;
    private LinearLayout llSale;

    private String costFrom = "", costTo = "", costTotal = "";
    private StringBuilder builder;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_animal_detail, container, false);
        initView(view);
        return view;
    }

    protected void setInstance(AnimalInfoFragment animalInfoFragment, AnimalInfoResponse.AnimalListBean animalInfoBean) {
        this.animalInfoFragment = animalInfoFragment;
        this.animalInfoBean = animalInfoBean;
    }

    private void initView(View view) {
           /* toolbar view start */
        ImageView imgBack = view.findViewById(R.id.imgBack);
        imgBack.setVisibility(View.VISIBLE);

        TextView tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.animal_details);
        /* toolbar view end */
        //avoid bg click

        onClickListener(imgBack, view.findViewById(R.id.llMain), view.findViewById(R.id.rlTreatment), view.findViewById(R.id.rlInsemination),
                view.findViewById(R.id.rlPregnancyTest), view.findViewById(R.id.rlMToParcel), view.findViewById(R.id.rlCalving),
                view.findViewById(R.id.rlWeight));
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        updateUi(view);
    }

    private void updateUi(View view) {
        TextView tvTagNum = view.findViewById(R.id.tvTagNum);
        TextView tvLotId = view.findViewById(R.id.tvLotId);
        tvParcelTitle = view.findViewById(R.id.tvParcelTitle);
        TextView tvMother = view.findViewById(R.id.tvMother);
        TextView tvBrought = view.findViewById(R.id.tvBrought);
        TextView tvDescription = view.findViewById(R.id.tvDescription);

        tvBBDate = view.findViewById(R.id.tvBBDate);
        tvMTDate = view.findViewById(R.id.tvMTDate);
        tvDismantDate = view.findViewById(R.id.tvDismantDate);
        tvSaleDate = view.findViewById(R.id.tvSaleDate);
        tvCtoDate = view.findViewById(R.id.tvCtoDate);

        cbBBDate = view.findViewById(R.id.cbBBDate);
        cbMTDate = view.findViewById(R.id.cbMTDate);
        cbDismantDate = view.findViewById(R.id.cbDismantDate);
        cbSale = view.findViewById(R.id.cbSale);
        cbCToDate = view.findViewById(R.id.cbCToDate);

        onClickListener(tvBBDate, tvMTDate, tvDismantDate, tvSaleDate, tvCtoDate,cbBBDate, cbMTDate, cbDismantDate,
                cbSale, cbCToDate);

        imgBBDate = view.findViewById(R.id.imgBBDate);
        imgMTDate = view.findViewById(R.id.imgMTDate);
        imgDismantDate = view.findViewById(R.id.imgDismantDate);
        imgSaleDate = view.findViewById(R.id.imgSaleDate);
        imgCtoDate = view.findViewById(R.id.imgCtoDate);
        llSale = view.findViewById(R.id.llSale);
        etSalePrice = view.findViewById(R.id.etSalePrice);
        etSaleQty = view.findViewById(R.id.etSaleQty);
        tvSaleAmt = view.findViewById(R.id.tvSaleAmt);

        etSalePrice.addTextChangedListener(this);
        etSaleQty.addTextChangedListener(this);

        if (animalInfoBean != null) {
            StringBuilder tagBuilder = new StringBuilder();
            tagBuilder.append("#").append(animalInfoBean.getTagNumber());
            tvTagNum.setText(tagBuilder);
            StringBuilder ltBuilder = new StringBuilder();
            ltBuilder.append("#").append(animalInfoBean.getLotId());
            tvLotId.setText(ltBuilder);
            tvParcelTitle.setText(animalInfoBean.getParcelName());
            tvMother.setText(animalInfoBean.getMother().isEmpty() ? getString(R.string.mother) : animalInfoBean.getMother());
            tvBrought.setText(animalInfoBean.getBoughtFrom());
            tvDescription.setText(animalInfoBean.getDescription());
        }
        updateView();
    }

    public void updateParcel(AnimalInfoResponse.AnimalListBean animalInfoBean) {
        this.animalInfoBean = animalInfoBean;
        tvParcelTitle.setText(this.animalInfoBean.getParcelName());
    }

    private void updateView() {
        if (animalInfoBean != null) {

            if (!animalInfoBean.getBornOrBuyDate().equals("0000-00-00")) {
                cbBBDate.setClickable(false);
                cbBBDate.setChecked(true);
                imgBBDate.setVisibility(View.GONE);
                tvBBDate.setText(CalenderUtils.formatDate(animalInfoBean.getBornOrBuyDate(), AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT));
            } else {
                cbBBDate.setClickable(true);
                cbBBDate.setChecked(false);
                tvBBDate.setText("");
            }

            if (!animalInfoBean.getMarkingOrTagging().equals("0000-00-00")) {
                cbMTDate.setClickable(false);
                cbMTDate.setChecked(true);
                imgMTDate.setVisibility(View.GONE);
                tvMTDate.setText(CalenderUtils.formatDate(animalInfoBean.getMarkingOrTagging(), AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT));
            } else {
                cbMTDate.setClickable(true);
                cbMTDate.setChecked(false);
                tvMTDate.setText("");
            }

            if (!animalInfoBean.getDismant().equals("0000-00-00")) {
                cbDismantDate.setClickable(false);
                cbDismantDate.setChecked(true);
                imgDismantDate.setVisibility(View.GONE);
                tvDismantDate.setText(CalenderUtils.formatDate(animalInfoBean.getDismant(), AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT));
            } else {
                cbDismantDate.setClickable(true);
                cbDismantDate.setChecked(false);
            }

            if (!animalInfoBean.getSaleDate().equals("0000-00-00") && !animalInfoBean.getSaleDate().equals("0002-11-30")) {
                cbSale.setClickable(false);
                cbSale.setChecked(true);
                imgSaleDate.setVisibility(View.GONE);
                tvSaleDate.setText(CalenderUtils.formatDate(animalInfoBean.getSaleDate(), AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT));
                llSale.setVisibility(View.VISIBLE);
                etSalePrice.setText(animalInfoBean.getSalePrice());
                etSaleQty.setText(animalInfoBean.getSaleQuantity());
                tvSaleAmt.setText(animalInfoBean.getSaleTotalPrice());
            } else {
                cbSale.setClickable(true);
                cbSale.setChecked(false);
                tvSaleDate.setText("");
                etSalePrice.setText("");
                etSaleQty.setText("");
                tvSaleAmt.setText("");
            }

            if (!animalInfoBean.getCostFrom().equals("0000-00-00")) {
                costFrom = CalenderUtils.formatDate(animalInfoBean.getCostFrom(), AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT);
                costTo = CalenderUtils.formatDate(animalInfoBean.getCostTo(), AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT);
                costTotal = animalInfoBean.getCostTotal();

                cbCToDate.setClickable(false);
                cbCToDate.setChecked(true);
                imgCtoDate.setVisibility(View.GONE);

                builder = new StringBuilder("");
                builder.append(costFrom).append(" ")
                        .append(getString(R.string.to)).append(" ")
                        .append(costTo);
                tvCtoDate.setText(builder);
            } else {
                cbCToDate.setClickable(true);
                cbCToDate.setChecked(false);
                costFrom = "";
                costTo = "";
                costTotal = "";
            }
        }
    }

    private void onClickListener(View... views){
        for (View view: views){
            view.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        hideKeyboard();
        switch (v.getId()) {
            case R.id.imgBack:
                getBaseActivity().onBackPressed();
                break;

            case R.id.rlTreatment:
                getBaseActivity().addFragment(TreatmentFragment.newInstance(animalInfoBean), R.id.animalFrame, true);
                break;

            case R.id.rlInsemination:
                getBaseActivity().addFragment(InseminationFragment.newInstance(animalInfoBean), R.id.animalFrame, true);
                break;

            case R.id.rlPregnancyTest:
                getBaseActivity().addFragment(PregnancyFragment.newInstance(animalInfoBean), R.id.animalFrame, true);
                break;

            case R.id.rlMToParcel:
                getBaseActivity().addFragment(MoveToParcelFragment.newInstance(animalInfoBean, AnimalDetailFragment.this), R.id.animalFrame, true);
                break;

            case R.id.rlCalving:
                getBaseActivity().addFragment(CalvingFragment.newInstance(animalInfoBean), R.id.animalFrame, true);
                break;

            case R.id.rlWeight:
                getBaseActivity().addFragment(WeightFragment.newInstance(animalInfoBean), R.id.animalFrame, true);
                break;

            case R.id.cbBBDate:
                imgBBDate.setVisibility(cbBBDate.isChecked() ? View.VISIBLE : View.GONE);
                break;

            case R.id.cbMTDate:
                imgMTDate.setVisibility(cbMTDate.isChecked() ? View.VISIBLE : View.GONE);
                break;

            case R.id.cbDismantDate:
                imgDismantDate.setVisibility(cbDismantDate.isChecked() ? View.VISIBLE : View.GONE);
                break;

            case R.id.cbSale:
                imgSaleDate.setVisibility(cbSale.isChecked() ? View.VISIBLE : View.GONE);
                llSale.setVisibility(cbSale.isChecked() ? View.VISIBLE : View.GONE);
                break;

            case R.id.cbCToDate:
                imgCtoDate.setVisibility(cbCToDate.isChecked() ? View.VISIBLE : View.GONE);
                break;

            case R.id.tvBBDate:
                if (cbBBDate.isChecked()) getDate("bbDate");
                break;

            case R.id.tvMTDate:
                if (cbMTDate.isChecked()) getDate("mtDate");
                break;

            case R.id.tvDismantDate:
                if (cbDismantDate.isChecked()) getDate("dismantDate");
                break;

            case R.id.tvSaleDate:
                if (cbSale.isChecked()) {
                    String price = etSalePrice.getText().toString().trim();
                    String qty = etSaleQty.getText().toString().trim();
                    if (verifySale(price, qty)) {
                        getDate("saleDate");
                    }
                }
                break;

            case R.id.tvCtoDate:
                if (cbCToDate.isChecked()) {

                    CostToDateDialog.newInstance(animalInfoBean.getCostFrom(), animalInfoBean.getCostTo(),
                            animalInfoBean.getCostTotal(), (fromDate, toDate, ttlCost) -> {
                                imgCtoDate.setVisibility(View.GONE);
                                cbCToDate.setClickable(false);

                                costFrom = fromDate;
                                costTo = toDate;
                                costTotal = ttlCost;

                                if (isNetworkConnected(true))
                                    getBaseActivity().doSyncAppDB(false, flag -> {
                                        switch (flag) {
                                            case AppConstants.ONLINE:
                                                updateAnimalDetail();
                                                break;

                                            case AppConstants.OFFLINE:
                                                builder = new StringBuilder("");
                                                builder.append(costFrom).append(" ")
                                                        .append(getString(R.string.to)).append(" ")
                                                        .append(costTo);
                                                tvCtoDate.setText(builder);
                                                animalInfoBean.setCostFrom(fromDate);
                                                animalInfoBean.setCostTo(toDate);
                                                animalInfoBean.setCostTotal(ttlCost);
                                                updateAnimalHistoryDB(false, animalInfoBean);
                                                break;
                                        }
                                    });
                                else {
                                    builder = new StringBuilder("");
                                    builder.append(costFrom).append(" ")
                                            .append(getString(R.string.to)).append(" ")
                                            .append(costTo);
                                    tvCtoDate.setText(builder);
                                    animalInfoBean.setCostFrom(fromDate);
                                    animalInfoBean.setCostTo(toDate);
                                    animalInfoBean.setCostTotal(ttlCost);
                                    updateAnimalHistoryDB(false, animalInfoBean);
                                }
                            }).show(getChildFragmentManager());
                }
                break;

        }
    }

    private boolean verifySale(String price, String qty) {
        if (price.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_sprice), Toast.LENGTH_SHORT);
            return false;
        } else if (price.startsWith(".")) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_valid_sprice), Toast.LENGTH_SHORT);
            return false;
        } else if (Double.parseDouble(price) <= 0) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_sprice_zero), Toast.LENGTH_SHORT);
            return false;
        } else if (qty.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_qty), Toast.LENGTH_SHORT);
            return false;
        } else if (qty.startsWith(".")) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_valid_qty), Toast.LENGTH_SHORT);
            return false;
        } else if (Double.parseDouble(qty) <= 0) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_qty_zero), Toast.LENGTH_SHORT);
            return false;
        } else return true;
    }

    private void updateAnimalDetail() {
        setLoading(true);

        HashMap<String, String> mParams = new HashMap<>();
        mParams.put("animalId", (animalInfoBean.getAnimalId().equals(animalInfoBean.getRecordKey()) ? "" : animalInfoBean.getAnimalId()));
        mParams.put("recordKey", animalInfoBean.getRecordKey());
        mParams.put("bornOrBuyDate", tvBBDate.getText().toString().trim());
        mParams.put("markingOrTagging", tvMTDate.getText().toString().trim());
        mParams.put("dismant", tvDismantDate.getText().toString().trim());
        mParams.put("saleDate", tvSaleDate.getText().toString().trim());
        mParams.put("salePrice", etSalePrice.getText().toString().trim());
        mParams.put("saleQuantity", etSaleQty.getText().toString().trim());
        mParams.put("saleTotalPrice", tvSaleAmt.getText().toString());
        mParams.put("costFrom", costFrom);
        mParams.put("costTo", costTo);
        mParams.put("costTotal", costTotal);

        getDataManager().doServerUpdateAnimalHistoryApiCall(getDataManager().getHeader(), mParams)
                .getAsJsonObject(new VolleyJsonObjectListener() {

                    @Override
                    public void onVolleyJsonObject(JSONObject jsonObject) {
                        setLoading(false);

                        try {
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            if (status.equals("success")) {
                                JSONObject subObj = jsonObject.getJSONObject("animalDetail");
                                animalInfoBean.setPropertyId(subObj.getString("propertyId"));
                                animalInfoBean.setAnimalId(subObj.getString("animalId"));
                                animalInfoBean.setParcel_id(subObj.getString("parcel_id"));
                                animalInfoBean.setLotId(subObj.getString("lotId"));
                                animalInfoBean.setTagNumber(subObj.getString("tagNumber"));
                                animalInfoBean.setBornOrBuyDate(subObj.getString("bornOrBuyDate"));
                                animalInfoBean.setMother(subObj.getString("mother"));
                                animalInfoBean.setBoughtFrom(subObj.getString("boughtFrom"));
                                animalInfoBean.setDescription(subObj.getString("description"));
                                animalInfoBean.setDismant(subObj.getString("dismant"));
                                animalInfoBean.setMarkingOrTagging(subObj.getString("markingOrTagging"));
                                animalInfoBean.setSaleDate(subObj.getString("saleDate"));
                                animalInfoBean.setSalePrice(subObj.getString("salePrice"));
                                animalInfoBean.setSaleQuantity(subObj.getString("saleQuantity"));
                                animalInfoBean.setSaleTotalPrice(subObj.getString("saleTotalPrice"));
                                animalInfoBean.setCostFrom(subObj.getString("costFrom"));
                                animalInfoBean.setCostTo(subObj.getString("costTo"));
                                animalInfoBean.setCostTotal(subObj.getString("costTotal"));
                                animalInfoBean.setStatus(subObj.getString("status"));
                                animalInfoBean.setRecordKey(subObj.getString("recordKey"));
                                animalInfoBean.setCrd(subObj.getString("crd"));
                                animalInfoBean.setUpd(subObj.getString("upd"));
                                updateView();
                                //Todo manage local db update all data
                                updateAnimalHistoryDB(true, animalInfoBean);
                                CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                            } else {
                                if (!message.equals(getString(R.string.no_record_found)))
                                    CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                            }
                        } catch (Exception e) {
                            CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyJsonException() {
                        setLoading(false);
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    private void updateAnimalHistoryDB(Boolean isSync, AnimalInfoResponse.AnimalListBean animalInfoBean) {
        //Todo manage local db update all data
        new Thread(() -> {
            Animal animal = new Animal();

            animal.setAnimalId(animalInfoBean.getAnimalId());
            animal.setParcel_id(animalInfoBean.getParcel_id());
            animal.setLotId(animalInfoBean.getLotId());
            animal.setTagNumber(animalInfoBean.getTagNumber());
            animal.setBornOrBuyDate(animalInfoBean.getBornOrBuyDate().contains("-") ? animalInfoBean.getBornOrBuyDate() : CalenderUtils.formatDate(animalInfoBean.getBornOrBuyDate(), AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT));
            animal.setMother(animalInfoBean.getMother());
            animal.setBoughtFrom(animalInfoBean.getBoughtFrom());
            animal.setDescription(animalInfoBean.getDescription());
            animal.setDismant(animalInfoBean.getDismant().contains("-") ? animalInfoBean.getDismant() : CalenderUtils.formatDate(animalInfoBean.getDismant(), AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT));
            animal.setSaleDate(animalInfoBean.getSaleDate().isEmpty() ? null : CalenderUtils.getDateFormat(animalInfoBean.getSaleDate().contains("/") ? CalenderUtils.formatDate(animalInfoBean.getSaleDate(), AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT) : animalInfoBean.getSaleDate(), AppConstants.SERVER_TIMESTAMP_FORMAT));
            animal.setMarkingOrTagging(animalInfoBean.getMarkingOrTagging().contains("-") ? animalInfoBean.getMarkingOrTagging() : CalenderUtils.formatDate(animalInfoBean.getMarkingOrTagging(), AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT));
            animal.setSalePrice(animalInfoBean.getSalePrice());
            animal.setSaleQuantity(animalInfoBean.getSaleQuantity());
            animal.setSaleTotalPrice(animalInfoBean.getSaleTotalPrice());
            animal.setCostFrom(animalInfoBean.getCostFrom().contains("-")?animalInfoBean.getCostFrom():CalenderUtils.formatDate(animalInfoBean.getCostFrom(), AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT));
            animal.setCostTo(animalInfoBean.getCostTo().contains("-")?animalInfoBean.getCostTo():CalenderUtils.formatDate(animalInfoBean.getCostTo(), AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT));
            animal.setCostTotal(animalInfoBean.getCostTotal());
            animal.setStatus(animalInfoBean.getStatus());
            animal.setRecordKey(animalInfoBean.getRecordKey());
            animal.setCrd(animalInfoBean.getCrd());
            animal.setUpd(animalInfoBean.getUpd());
            animal.setEvent(animalInfoBean.getAnimalId().equals(animalInfoBean.getRecordKey()) ? AppConstants.DB_EVENT_ADD : AppConstants.DB_EVENT_EDIT);
            animal.setDataSync(isSync ? AppConstants.DB_SYNC_TRUE : AppConstants.DB_SYNC_FALSE);

            getDataManager().updateAnimal(animal);

            //set sync status true
            getDataManager().setLocalDataExistForSync(true);
            AppLogger.d("database", "animal detail update success");

            new Handler(Looper.getMainLooper()).post(() -> {
                if (!isSync)
                    CommonUtils.showToast(getBaseActivity(), getString(R.string.animalHistSuccessMsg), Toast.LENGTH_SHORT);
            });
        }).start();
    }

    private void getDate(String flag) {
        String selectedDate = "";
        switch (flag) {
            case "bbDate":
                selectedDate = tvBBDate.getText().toString().trim();
                break;
            case "mtDate":
                selectedDate = tvMTDate.getText().toString().trim();
                break;
            case "dismantDate":
                selectedDate = tvDismantDate.getText().toString().trim();
                break;
            case "saleDate":
                selectedDate = tvSaleDate.getText().toString().trim();
                break;
        }

        Calendar newCalender = Calendar.getInstance();

        int day, month, year;

        if (selectedDate.isEmpty()) {
            day = newCalender.get(Calendar.DAY_OF_MONTH);
            month = newCalender.get(Calendar.MONTH);
            year = newCalender.get(Calendar.YEAR);
        } else {
            String[] date = selectedDate.split("/");

            day = Integer.parseInt(date[0]);
            month = Integer.parseInt(date[1]) - 1;
            year = Integer.parseInt(date[2]);
        }

        DatePickerDialog datePickerDialog = new DatePickerDialog(getBaseActivity(), (view, selectedYear, selectedMonth, selectedDay) -> {
            selectedMonth += 1;

            StringBuilder date = new StringBuilder("");
            date.append(selectedDay).append("/").append(selectedMonth).append("/").append(selectedYear);

            switch (flag) {
                case "bbDate":
                    imgBBDate.setVisibility(View.GONE);
                    tvBBDate.setText(CalenderUtils.formatDate(String.valueOf(date), AppConstants.TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT));
                    cbBBDate.setClickable(false);
                    if (isNetworkConnected(true))
                        getBaseActivity().doSyncAppDB(false, flag1 -> {
                            switch (flag1) {
                                case AppConstants.ONLINE:
                                    updateAnimalDetail();
                                    break;

                                case AppConstants.OFFLINE:
                                    animalInfoBean.setBornOrBuyDate(tvBBDate.getText().toString().trim());
                                    updateAnimalHistoryDB(false, animalInfoBean);
                                    break;
                            }
                        });
                    else {
                        animalInfoBean.setBornOrBuyDate(tvBBDate.getText().toString().trim());
                        updateAnimalHistoryDB(false, animalInfoBean);
                    }
                    break;

                case "mtDate":
                    imgMTDate.setVisibility(View.GONE);
                    tvMTDate.setText(CalenderUtils.formatDate(String.valueOf(date), AppConstants.TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT));
                    cbMTDate.setClickable(false);
                    if (isNetworkConnected(true))
                        getBaseActivity().doSyncAppDB(false, flag1 -> {
                            switch (flag1) {
                                case AppConstants.ONLINE:
                                    updateAnimalDetail();
                                    break;

                                case AppConstants.OFFLINE:
                                    animalInfoBean.setMarkingOrTagging(tvMTDate.getText().toString().trim());
                                    updateAnimalHistoryDB(false, animalInfoBean);
                                    break;
                            }
                        });
                    else {
                        animalInfoBean.setMarkingOrTagging(tvMTDate.getText().toString().trim());
                        updateAnimalHistoryDB(false, animalInfoBean);
                    }
                    break;

                case "dismantDate":
                    imgDismantDate.setVisibility(View.GONE);
                    tvDismantDate.setText(CalenderUtils.formatDate(String.valueOf(date), AppConstants.TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT));
                    cbDismantDate.setClickable(false);
                    if (isNetworkConnected(true))
                        getBaseActivity().doSyncAppDB(false, flag1 -> {
                            switch (flag1) {
                                case AppConstants.ONLINE:
                                    updateAnimalDetail();
                                    break;

                                case AppConstants.OFFLINE:
                                    animalInfoBean.setDismant(tvDismantDate.getText().toString().trim());
                                    updateAnimalHistoryDB(false, animalInfoBean);
                                    break;
                            }
                        });
                    else {
                        animalInfoBean.setDismant(tvDismantDate.getText().toString().trim());
                        updateAnimalHistoryDB(false, animalInfoBean);
                    }
                    break;

                case "saleDate":
                    imgSaleDate.setVisibility(View.GONE);
                    tvSaleDate.setText(CalenderUtils.formatDate(String.valueOf(date), AppConstants.TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT));
                    cbSale.setClickable(false);
                    if (isNetworkConnected(true))
                        getBaseActivity().doSyncAppDB(false, flag1 -> {
                            switch (flag1) {
                                case AppConstants.ONLINE:
                                    updateAnimalDetail();
                                    break;

                                case AppConstants.OFFLINE:
                                    animalInfoBean.setSaleTotalPrice(tvSaleAmt.getText().toString());
                                    animalInfoBean.setSaleQuantity(etSaleQty.getText().toString().trim());
                                    animalInfoBean.setSalePrice(etSalePrice.getText().toString().trim());
                                    animalInfoBean.setSaleDate(tvSaleDate.getText().toString().trim());
                                    updateAnimalHistoryDB(false, animalInfoBean);
                                    break;
                            }
                        });
                    else {
                        animalInfoBean.setSaleTotalPrice(tvSaleAmt.getText().toString());
                        animalInfoBean.setSaleQuantity(etSaleQty.getText().toString().trim());
                        animalInfoBean.setSalePrice(etSalePrice.getText().toString().trim());
                        animalInfoBean.setSaleDate(tvSaleDate.getText().toString().trim());
                        updateAnimalHistoryDB(false, animalInfoBean);
                    }
                    break;
            }

        }, year, month, day);

        Date maxDate = CalenderUtils.getDateFormat(CalenderUtils.getCurrentDate(), AppConstants.TIMESTAMP_FORMAT);
        assert maxDate != null;
        datePickerDialog.getDatePicker().setMaxDate(maxDate.getTime());

        datePickerDialog.show();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        try {
            if (!s.toString().startsWith(".")) {
                long price = etSalePrice.getText().toString().isEmpty() ? 0 : Double.valueOf(etSalePrice.getText().toString()).longValue();
                long qty = etSaleQty.getText().toString().isEmpty() ? 0 : Double.valueOf(etSaleQty.getText().toString()).longValue();

                tvSaleAmt.setText(String.valueOf(decimalFormat.format(price * qty)));
            }
        } catch (Exception ignored) {
        }
    }

    @Override
    public void onBackPress() {
        if (animalInfoBean != null) animalInfoFragment.hitApi();
    }
}
