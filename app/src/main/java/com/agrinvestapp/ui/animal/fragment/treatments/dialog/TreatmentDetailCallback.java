package com.agrinvestapp.ui.animal.fragment.treatments.dialog;

import com.agrinvestapp.data.model.api.TreatmentDetailResponse;

public interface TreatmentDetailCallback {
    void onAdd(TreatmentDetailResponse.TreatmentDetailBean treatmentDetailBean);

    void onEdit(int pos, TreatmentDetailResponse.TreatmentDetailBean treatmentDetailBean);
}
