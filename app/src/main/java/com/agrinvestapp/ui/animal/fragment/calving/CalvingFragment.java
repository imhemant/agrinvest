package com.agrinvestapp.ui.animal.fragment.calving;


import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.AnimalInfoResponse;
import com.agrinvestapp.data.model.api.CalvingInfoResponse;
import com.agrinvestapp.data.model.db.Calving;
import com.agrinvestapp.ui.animal.fragment.calving.adapter.CalvingAdapter;
import com.agrinvestapp.ui.animal.fragment.calving.dialog.CalvingCallback;
import com.agrinvestapp.ui.animal.fragment.calving.dialog.CalvingDialog;
import com.agrinvestapp.ui.base.BaseFragment;
import com.agrinvestapp.ui.base.dialog.DeleteDialog;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.AppLogger;
import com.agrinvestapp.utils.CalenderUtils;
import com.agrinvestapp.utils.CommonUtils;
import com.agrinvestapp.volley.VolleyJsonObjectListener;
import com.agrinvestapp.volley.VolleyRequest;
import com.agrinvestapp.volley.VolleyResponseListener;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CalvingFragment extends BaseFragment implements View.OnClickListener, CalvingCallback {

    private TextView tvNoRecord;
    private RecyclerView rvCalving;

    private CalvingAdapter calvingAdapter;
    private List<CalvingInfoResponse.CalvingListBean> list;
    private AnimalInfoResponse.AnimalListBean animalInfoBean;

    private Handler handler = new Handler(Looper.getMainLooper());

    public static CalvingFragment newInstance(AnimalInfoResponse.AnimalListBean animalInfoBean) {

        Bundle args = new Bundle();
        args.putParcelable(AppConstants.ANIMAL_MODEL, animalInfoBean);
        CalvingFragment fragment = new CalvingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            animalInfoBean = getArguments().getParcelable(AppConstants.ANIMAL_MODEL);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_calving, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
         /* toolbar view start */
        ImageView imgBack = view.findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);
        imgBack.setVisibility(View.VISIBLE);

        TextView tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.calving);

        FrameLayout frameAdd = view.findViewById(R.id.frameAddT);
        frameAdd.setVisibility(View.VISIBLE);
        frameAdd.setOnClickListener(this);
        /* toolbar view end */
        view.findViewById(R.id.rlMain).setOnClickListener(this); //avoid bg click
        tvNoRecord = view.findViewById(R.id.tvNoRecord);

        rvCalving = view.findViewById(R.id.rvCalving);
        list = new ArrayList<>();

        calvingAdapter = new CalvingAdapter(list, new CalvingAdapter.ItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                //not require
            }

            @Override
            public void onEditClick(int pos) {
                //not require
            }

            @Override
            public void onDeleteClick(int pos) {
                DeleteDialog.newInstance(false, "", () -> {
                    if (isNetworkConnected(true)) {
                        getBaseActivity().doSyncAppDB(false, flag -> {
                            switch (flag) {
                                case AppConstants.ONLINE:
                                    doAddDelCalving(pos, AppConstants.REQUEST_TYPE_DEL, list.get(pos));
                                    break;

                                case AppConstants.OFFLINE:
                                    doAddDelCalvingLocal(pos, AppConstants.REQUEST_TYPE_DEL, list.get(pos));
                                    break;
                            }
                        });
                    } else doAddDelCalvingLocal(pos, AppConstants.REQUEST_TYPE_DEL, list.get(pos));
                }).show(getChildFragmentManager());
            }
        });
        rvCalving.setAdapter(calvingAdapter);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (isNetworkConnected(true)) {
            getBaseActivity().doSyncAppDB(false, flag -> {
                switch (flag) {
                    case AppConstants.ONLINE:
                        getCalvingInfo();
                        break;

                    case AppConstants.OFFLINE:
                        getCalvingInfoFromDB();
                        break;
                }
            });
        }
        else {
            getCalvingInfoFromDB();
        }
    }

    private void getCalvingInfo() {
        setLoading(true);

        HashMap<String, String> mParameterMap = new HashMap<>();
        mParameterMap.put("requestType", AppConstants.REQUEST_TYPE_CALVING);
        mParameterMap.put("animalId", animalInfoBean.getAnimalId());
        mParameterMap.put("recordKey", animalInfoBean.getRecordKey());
        getDataManager().doServerTreatmentInfoListApiCall(getDataManager().getHeader(), mParameterMap)
                .getAsResponse(new VolleyResponseListener() {
                    @Override
                    public void onVolleyResponse(String response) {
                        setLoading(false);
                        CalvingInfoResponse infoResponse = getDataManager().mGson.fromJson(response, CalvingInfoResponse.class);

                        if (infoResponse.getStatus().equals("success")) {
                            list.clear();
                            list.addAll(infoResponse.getCalvingList());

                            updateUI();
                            if (!list.isEmpty()) {
                                //Todo manage local db add all data
                                doAddAllCalvingLocalDB(infoResponse.getCalvingList());
                            }

                            calvingAdapter.notifyDataSetChanged();
                        } else {
                            updateUI();
                            if (infoResponse.getMessage().equals(getString(R.string.no_record_found))) {
                                list.clear();
                                calvingAdapter.notifyDataSetChanged();
                            }
                            if (!infoResponse.getMessage().equals(getString(R.string.no_record_found)))
                                CommonUtils.showToast(getBaseActivity(), infoResponse.getMessage(), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        updateUI();
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    private void updateUI() {
        tvNoRecord.setVisibility(list.isEmpty() ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                getBaseActivity().onBackPressed();
                break;

            case R.id.frameAddT:
                if (list.size() < 10) {
                    CalvingDialog.newInstance(0, CalvingFragment.this).show(getChildFragmentManager());
                } else
                    CommonUtils.showToast(getBaseActivity(), getString(R.string.calvingLimit), Toast.LENGTH_SHORT);

                break;
        }
    }

    private void doAddDelCalving(int pos, String flag, CalvingInfoResponse.CalvingListBean bean) {
        setLoading(true);

        HashMap<String, String> mParams = new HashMap<>();
        mParams.put("requestType", flag);
        mParams.put("recordKey", animalInfoBean.getRecordKey());  //animal record key
        mParams.put("calvingDate", bean.getCalvingDate());
        mParams.put("calvingNumber", bean.getCalvingNumber());

        switch (flag) {
            case AppConstants.REQUEST_TYPE_ADD:
                mParams.put("calvingId", bean.getCalvingId());
                mParams.put("calvingRecordKey", bean.getRecordKey());
                break;

            case AppConstants.REQUEST_TYPE_DEL:
                mParams.put("calvingId", bean.getCalvingId());
                mParams.put("calvingRecordKey", bean.getRecordKey());
                break;
        }

        getDataManager().doServerCalvingAddDelApiCall(getDataManager().getHeader(), mParams)
                .getAsJsonObject(new VolleyJsonObjectListener() {

                    @Override
                    public void onVolleyJsonObject(JSONObject obj) {
                        setLoading(false);

                        try {
                            String status = obj.getString("status");
                            String message = obj.getString("message");

                            if (status.equals("success")) {

                                switch (flag) {
                                    case AppConstants.REQUEST_TYPE_ADD:
                                        JSONObject jsonObject = obj.getJSONObject("calvingDetail");

                                        CalvingInfoResponse.CalvingListBean bean = new CalvingInfoResponse.CalvingListBean();

                                        bean.setCalvingId(jsonObject.getString("calvingId"));
                                        bean.setAnimal_id(jsonObject.getString("animal_id"));
                                        bean.setCalvingNumber(jsonObject.getString("calvingNumber"));
                                        bean.setCalvingDate(jsonObject.getString("calvingDate"));
                                        bean.setRecordKey(jsonObject.getString("recordKey"));
                                        bean.setCrd(jsonObject.getString("crd"));
                                        bean.setUpd(jsonObject.getString("upd"));

                                        list.add(pos, bean);
                                        calvingAdapter.notifyItemInserted(pos);

                                        rvCalving.smoothScrollToPosition(pos);
                                        updateLocalDB(flag, list.get(pos));
                                        CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                                        break;

                                    case AppConstants.REQUEST_TYPE_DEL:
                                        updateLocalDB(flag, list.get(pos));

                                        list.remove(pos);
                                        calvingAdapter.notifyItemRemoved(pos);
                                        CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                                        break;
                                }

                                updateUI();

                            } else
                                CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                        } catch (Exception e) {
                            CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyJsonException() {
                        setLoading(false);
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    @Override
    public void onAdd(CalvingInfoResponse.CalvingListBean bean) {
        if (isNetworkConnected(true)) {
            getBaseActivity().doSyncAppDB(false, flag -> {
                switch (flag) {
                    case AppConstants.ONLINE:
                        doAddDelCalving(0, AppConstants.REQUEST_TYPE_ADD, bean);
                        break;

                    case AppConstants.OFFLINE:
                        doAddDelCalvingLocal(0, AppConstants.REQUEST_TYPE_ADD, bean);
                        break;
                }
            });
        }
        else doAddDelCalvingLocal(0, AppConstants.REQUEST_TYPE_ADD, bean);
    }

    /*Local DB Start here*/
    private void getCalvingInfoFromDB() {
        setLoading(true);

        new Thread(() -> {
            list.clear();
            List<CalvingInfoResponse.CalvingListBean> calvingList = getDataManager().getCalvingList(animalInfoBean.getAnimalId());

            list.addAll(calvingList);

            handler.post(() -> {
                setLoading(false);
                calvingAdapter.notifyDataSetChanged();
                updateUI();
            });
        }).start();
    }

    private void doAddAllCalvingLocalDB(List<CalvingInfoResponse.CalvingListBean> infoResponse) {

        new Thread(() -> {
            getDataManager().deleteAllCalving();

            List<Calving> calvingList = new ArrayList<>();
            for (CalvingInfoResponse.CalvingListBean mainBean : infoResponse) {
                Calving calving = new Calving();

                calving.setCalvingId(mainBean.getCalvingId());
                calving.setAnimal_id(mainBean.getAnimal_id());
                calving.setCalvingNumber(mainBean.getCalvingNumber());
                calving.setCalvingDate(mainBean.getCalvingDate());
                calving.setRecordKey(mainBean.getRecordKey());
                calving.setCrd(mainBean.getCrd());
                calving.setUpd(mainBean.getUpd());
                calving.setDataSync(AppConstants.DB_SYNC_TRUE);
                calving.setEvent(AppConstants.DB_EVENT_ADD);

                calvingList.add(calving);
            }

            getDataManager().insertAllCalving(calvingList);
            AppLogger.d("database", "Calving add success");
        }).start();
    }

    private void doAddDelCalvingLocal(int pos, String flag, CalvingInfoResponse.CalvingListBean bean) {
        new Thread(() -> {
            switch (flag) {
                case AppConstants.REQUEST_TYPE_ADD:
                    Calving calving = new Calving();

                    calving.setCalvingId(bean.getRecordKey());  //primary key
                    calving.setAnimal_id(animalInfoBean.getAnimalId());
                    calving.setCalvingNumber(bean.getCalvingNumber());
                    calving.setCalvingDate(bean.getCalvingDate());
                    calving.setRecordKey(bean.getRecordKey());
                    calving.setCrd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    calving.setUpd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));
                    calving.setDataSync(AppConstants.DB_SYNC_FALSE);
                    calving.setEvent(AppConstants.DB_EVENT_ADD);

                    getDataManager().insertCalving(calving);

                    //set sync status true
                    getDataManager().setLocalDataExistForSync(true);
                    AppLogger.d("database", "Calving add success");

                    handler.post(() -> {
                        list.add(pos, bean);
                        calvingAdapter.notifyItemInserted(pos);
                        rvCalving.smoothScrollToPosition(pos);
                        updateUI();
                    });
                    break;

                case AppConstants.REQUEST_TYPE_DEL:
                    if (bean.getCalvingId().equals(bean.getRecordKey())) {
                        getDataManager().deleteUsingCalvingId(bean.getCalvingId());
                    } else {
                        calving = new Calving();

                        calving.setCalvingId(bean.getCalvingId());  //primary key
                        calving.setAnimal_id(animalInfoBean.getAnimalId());
                        calving.setCalvingNumber(bean.getCalvingNumber());
                        calving.setCalvingDate(bean.getCalvingDate());
                        calving.setRecordKey(bean.getRecordKey());
                        calving.setCrd(bean.getCrd());
                        calving.setUpd(bean.getUpd());
                        calving.setDataSync(AppConstants.DB_SYNC_FALSE);
                        calving.setEvent(AppConstants.DB_EVENT_DEL);

                        getDataManager().updateCalving(calving);

                        //set sync status true
                        getDataManager().setLocalDataExistForSync(true);
                    }
                    AppLogger.d("database", "Calving del success");

                    handler.post(() -> {
                        list.remove(pos);
                        calvingAdapter.notifyItemRemoved(pos);
                        updateUI();
                    });
                    break;
            }

        }).start();
    }

    private void updateLocalDB(String flag, CalvingInfoResponse.CalvingListBean bean) {
        new Thread(() -> {
            switch (flag) {
                case AppConstants.REQUEST_TYPE_ADD:
                    Calving calving = new Calving();

                    calving.setCalvingId(bean.getCalvingId());  //primary key
                    calving.setAnimal_id(animalInfoBean.getAnimalId());
                    calving.setCalvingNumber(bean.getCalvingNumber());
                    calving.setCalvingDate(bean.getCalvingDate());
                    calving.setRecordKey(bean.getRecordKey());
                    calving.setCrd(bean.getCrd());
                    calving.setUpd(bean.getUpd());
                    calving.setDataSync(AppConstants.DB_SYNC_FALSE);
                    calving.setEvent(AppConstants.DB_EVENT_ADD);

                    getDataManager().insertCalving(calving);

                    //set sync status true
                    getDataManager().setLocalDataExistForSync(true);
                    AppLogger.d("database", "Calving add success");
                    break;

                case AppConstants.REQUEST_TYPE_DEL:
                    getDataManager().deleteUsingCalvingId(bean.getCalvingId());
                    AppLogger.d("database", "Calving del success");
                    break;
            }

        }).start();

    }
    /*Local DB End here*/

}