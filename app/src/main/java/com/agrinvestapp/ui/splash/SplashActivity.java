package com.agrinvestapp.ui.splash;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import com.agrinvestapp.Agrinvest;
import com.agrinvestapp.ui.auth.AuthActivity;
import com.agrinvestapp.ui.main.MainActivity;
import com.agrinvestapp.utils.AppLogger;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;


public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        updateLocation();

        new Handler().postDelayed(() -> {
            if (Agrinvest.getDataManager().isLoggedIn()) {
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
            } else {
                startActivity(new Intent(SplashActivity.this, AuthActivity.class));
            }
            finish();
        }, 2000);
    }

    private void updateLocation() {
        FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mFusedLocationClient.getLastLocation().addOnSuccessListener(this, location -> {
            if (location != null) {
                // Logic to handle location object
                Agrinvest.LATITUDE = location.getLatitude();
                Agrinvest.LONGITUDE = location.getLongitude();
                AppLogger.e("Location", String.valueOf(Agrinvest.LATITUDE));
            } else {
                //Location not available
                AppLogger.e("Splash", "Location not available");
            }
        });

    }

}
