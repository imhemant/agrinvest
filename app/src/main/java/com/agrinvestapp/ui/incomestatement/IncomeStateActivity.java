package com.agrinvestapp.ui.incomestatement;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.db.Animal;
import com.agrinvestapp.data.model.db.Crop;
import com.agrinvestapp.data.model.db.Parcel;
import com.agrinvestapp.data.model.other.IncomeBean;
import com.agrinvestapp.data.model.other.IncomeOrExpenseApplyBean;
import com.agrinvestapp.data.model.other.IncomeOrExpenseApplyOnBean;
import com.agrinvestapp.ui.base.BaseActivity;
import com.agrinvestapp.ui.base.ClickListener;
import com.agrinvestapp.ui.incomestatement.adapter.IncomeApplyAdapter;
import com.agrinvestapp.ui.incomestatement.dialog.IncomeApplyOnDialog;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.CalenderUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class IncomeStateActivity extends BaseActivity implements View.OnClickListener {

    public static String incomeType;
    private TextView tvAnimal, tvCrop, tvFromDate, tvToDate, tvApplyOn, tvTtlExpense, tvSaleAmt;

    private StringBuilder fromDate, toDate, expenseAmt, expenseSale;
    private Boolean isFromSelect = false;

    private String applyOn = "", tempApplyOn = "";

    private List<IncomeOrExpenseApplyOnBean> finalApplyList;

    private List<IncomeOrExpenseApplyBean> animalList;
    private List<IncomeOrExpenseApplyBean> cropList;
    private List<IncomeOrExpenseApplyBean> applyList;
    private List<IncomeOrExpenseApplyOnBean> applyOnList;

    private IncomeApplyAdapter applyAdapter;

    private Boolean isClick = false;

    private Handler handler = new Handler(Looper.getMainLooper());

    // the callback received when the user "sets" the Date in the
// DatePickerDialog
    @NonNull
    private DatePickerDialog.OnDateSetListener datePicker = (view, selectedYear, selectedMonth, selectedDay) -> {
        selectedMonth += 1;

        if (isFromSelect) {
            fromDate = new StringBuilder("");
            fromDate.append(selectedDay).append("/").append(selectedMonth).append("/").append(selectedYear);

            tvFromDate.setText(CalenderUtils.formatDate(String.valueOf(fromDate), AppConstants.TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT));

            if (!finalApplyList.isEmpty()) {
                doCalculation(applyOn, finalApplyList);
            }
        } else {
            toDate = new StringBuilder("");
            toDate.append(selectedDay).append("/").append(selectedMonth).append("/").append(selectedYear);
            tvToDate.setText(CalenderUtils.formatDate(String.valueOf(toDate), AppConstants.TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT));

            if (!finalApplyList.isEmpty()) {
                doCalculation(applyOn, finalApplyList);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_income_statement);

        if (isNetworkConnected(true)) {
            doSyncAppDB(false, flag -> {
            });
        }
        initView();
        initApplyRecyclerView();
    }

    private void initView() {
          /* toolbar view start */
        ImageView imgBack = findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);
        imgBack.setVisibility(View.VISIBLE);

        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.income);  //income_statement
        /* toolbar view end */

        incomeType = "animal";
        tvAnimal = findViewById(R.id.tvAnimal);
        tvCrop = findViewById(R.id.tvCrop);
        tvApplyOn = findViewById(R.id.tvApplyOn);
        tvFromDate = findViewById(R.id.tvFromDate);
        tvToDate = findViewById(R.id.tvToDate);
        tvTtlExpense = findViewById(R.id.tvTtlExpense);
        tvSaleAmt = findViewById(R.id.tvSaleAmt);

        fromDate = new StringBuilder("");
        toDate = new StringBuilder("");

        onClickListener(tvAnimal, tvCrop, tvFromDate, tvToDate, findViewById(R.id.rlApplyOn));
    }

    private void onClickListener(View... views) {
        for (View view : views) {
            view.setOnClickListener(this);
        }
    }

    private void initApplyRecyclerView() {
        RecyclerView rvApplyOn = findViewById(R.id.rvApplyOn);
        RecyclerView.LayoutManager manager = new GridLayoutManager(this, 3);
        rvApplyOn.setLayoutManager(manager);

        animalList = new ArrayList<>();
        cropList = new ArrayList<>();
        applyList = new ArrayList<>();
        applyOnList = new ArrayList<>();
        finalApplyList = new ArrayList<>();

        IncomeOrExpenseApplyBean bean = new IncomeOrExpenseApplyBean();
        bean.isSelect = false;
        bean.title = "Animal";
        bean.localisationTitle = getString(R.string.animal);
        animalList.add(bean);

        bean = new IncomeOrExpenseApplyBean();
        bean.isSelect = false;
        bean.title = "Crop";
        bean.localisationTitle = getString(R.string.crop);
        cropList.add(bean);

        bean = new IncomeOrExpenseApplyBean();
        bean.isSelect = false;
        bean.title = "Lot";
        bean.localisationTitle = getString(R.string.lot);
        animalList.add(bean);
        cropList.add(bean);

        bean = new IncomeOrExpenseApplyBean();
        bean.isSelect = false;
        bean.title = "Parcel";
        bean.localisationTitle = getString(R.string.parcel);
        animalList.add(bean);
        cropList.add(bean);

        bean = new IncomeOrExpenseApplyBean();
        bean.isSelect = false;
        bean.title = "Property";
        bean.localisationTitle = getString(R.string.property);
        animalList.add(bean);
        cropList.add(bean);

        applyList.addAll(animalList);

        applyAdapter = new IncomeApplyAdapter(applyList, new ClickListener() {
            @Override
            public void onItemClick(int pos) {
                applyOn = applyList.get(pos).title;
                tempApplyOn = "";
                tvApplyOn.setText("");

                updateAppliedOnList(applyOn);
            }

            @Override
            public void onEditClick(int pos) {
//not usable
            }

            @Override
            public void onDeleteClick(int pos) {
//not usable
            }
        });
        rvApplyOn.setAdapter(applyAdapter);
    }

    private void updateAppliedOnList(String applyOn) {
        new Thread(() -> {
            applyOnList.clear();

            List<IncomeOrExpenseApplyOnBean> localList = new ArrayList<>();

            switch (applyOn.toLowerCase()) {
                case "animal":
                    localList = getDataManager().getAnimalApplyOnInfo();
                    break;

                case "lot":
                    if (incomeType.equals("animal"))
                        localList = getDataManager().getAnimalLotApplyOnInfo();
                    else localList = getDataManager().getCropLotApplyOnInfo();
                    break;

                case "parcel":
                    localList = getDataManager().getParcelApplyOnInfo();
                    break;

                case "property":
                    localList = getDataManager().getPropertyApplyOnInfo();
                    break;

                case "crop":
                    localList = getDataManager().getCropApplyOnInfo();
                    break;
            }

            applyOnList.addAll(localList);

        }).start();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;

            case R.id.tvAnimal:
                if (!incomeType.equalsIgnoreCase("animal")) {
                    incomeType = "animal";
                    resetState(incomeType);
                    tvAnimal.setTextColor(getResources().getColor(R.color.colorWhite));
                    tvCrop.setTextColor(getResources().getColor(R.color.grey));
                    tvAnimal.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                    tvCrop.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                }
                break;

            case R.id.tvCrop:
                if (!incomeType.equalsIgnoreCase("crop")) {
                    incomeType = "crop";
                    resetState(incomeType);

                    tvCrop.setTextColor(getResources().getColor(R.color.colorWhite));
                    tvAnimal.setTextColor(getResources().getColor(R.color.grey));
                    tvCrop.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                    tvAnimal.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                }
                break;

            case R.id.tvFromDate:
                isFromSelect = true;
                getDate(tvFromDate.getText().toString().trim());
                break;

            case R.id.tvToDate:
                isFromSelect = false;
                getDate(tvToDate.getText().toString().trim());
                break;

            //open dialog
            case R.id.rlApplyOn:
                if (!isClick) {
                    isClick = true;

                    ArrayList<IncomeOrExpenseApplyOnBean> expenseApplyOnBeans = new ArrayList<>();
                    for (IncomeOrExpenseApplyOnBean a : applyOnList) {
                        IncomeOrExpenseApplyOnBean applyOnBean = new IncomeOrExpenseApplyOnBean();
                        applyOnBean.setSelect(a.getSelect());
                        applyOnBean.setRecordKey(a.getRecordKey());
                        applyOnBean.setName(a.getName());
                        expenseApplyOnBeans.add(applyOnBean);
                    }
                    IncomeApplyOnDialog.newInstance(expenseApplyOnBeans, (tempApplyOnList, data, showData, finalApplyList) -> {
                        applyOnList.clear();
                        applyOnList.addAll(tempApplyOnList);
                        tempApplyOn = data;
                        tvApplyOn.setText(showData);
                        this.finalApplyList = finalApplyList;
                        doCalculation(applyOn, finalApplyList);
                    }).show(getSupportFragmentManager());
                }

                new Handler().postDelayed(() -> isClick = false, 3000);

                break;
        }
    }

    private void doCalculation(String applyOn, List<IncomeOrExpenseApplyOnBean> finalApplyList) {
        new Thread(() -> {
            switch (applyOn.toLowerCase()) {
                case "animal":
                    doAnimalExpenseCalculation(finalApplyList);
                    break;

                case "lot":
                    if (incomeType.equals("animal"))
                        doAnimalLotExpenseCalculation(finalApplyList);
                    else doCropLotExpenseCalculation(finalApplyList);
                    break;

                case "parcel":
                    if (incomeType.equals("animal"))
                        doAnimalParcelExpenseCalculation(finalApplyList);
                    else doCropParcelExpenseCalculation(finalApplyList);
                    break;

                case "property":
                    if (incomeType.equals("animal"))
                        doAnimalPropertyExpenseCalculation(finalApplyList);
                    else doCropPropertyExpenseCalculation(finalApplyList);
                    break;

                case "crop":
                    doCropExpenseCalculation(finalApplyList);
                    break;

            }
        }).start();
    }

    private void resetState(String state) {
        if (state.equals("animal")) {
            if (applyAdapter.lastPos != -1) {
                cropList.get(applyAdapter.lastPos).isSelect = false;
                applyAdapter.lastPos = -1;
            }
            applyList.clear();
            applyList.addAll(animalList);
            applyAdapter.notifyDataSetChanged();
        } else {
            if (applyAdapter.lastPos != -1) {
                animalList.get(applyAdapter.lastPos).isSelect = false;
                applyAdapter.lastPos = -1;
            }
            applyList.clear();
            applyList.addAll(cropList);
            applyAdapter.notifyDataSetChanged();
        }

        tvFromDate.setText("");
        tvToDate.setText("");
        fromDate = new StringBuilder("");
        toDate = new StringBuilder("");
        isFromSelect = false;

        applyOn = "";
        tempApplyOn = "";
        tvApplyOn.setText("");
        finalApplyList.clear();
        applyOnList.clear();
        tvTtlExpense.setText(getString(R.string.d0));
        tvSaleAmt.setText(getString(R.string.d0));
    }

    private void getDate(String selectedDate) {
        //date 26/06/2018

        Calendar newCalender = Calendar.getInstance();

        int day, month, year;

        if (selectedDate.isEmpty()) {
            day = newCalender.get(Calendar.DAY_OF_MONTH);
            month = newCalender.get(Calendar.MONTH);
            year = newCalender.get(Calendar.YEAR);
        } else {
            String[] date = selectedDate.split("/");

            day = Integer.parseInt(date[0]);
            month = Integer.parseInt(date[1]) - 1;
            year = Integer.parseInt(date[2]);
        }

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, datePicker, year, month, day);
        Date maxDate = CalenderUtils.getDateFormat(CalenderUtils.getCurrentDate(), AppConstants.TIMESTAMP_FORMAT);
        assert maxDate != null;
        datePickerDialog.getDatePicker().setMaxDate(maxDate.getTime());

        if (!isFromSelect && !tvFromDate.getText().toString().isEmpty() && tvToDate.getText().toString().isEmpty()) {
            Date cDate = CalenderUtils.getDateFormat(tvFromDate.getText().toString(), AppConstants.TIMESTAMP_FORMAT);
            assert cDate != null;
            datePickerDialog.getDatePicker().setMinDate(cDate.getTime());
        } else if (isFromSelect && tvFromDate.getText().toString().isEmpty() && !tvToDate.getText().toString().isEmpty()) {
            Date cDate = CalenderUtils.getDateFormat(tvToDate.getText().toString(), AppConstants.TIMESTAMP_FORMAT);
            assert cDate != null;
            datePickerDialog.getDatePicker().setMaxDate(cDate.getTime());
        } else if (isFromSelect && !tvFromDate.getText().toString().isEmpty() && !tvToDate.getText().toString().isEmpty()) {
            Date cDateMax = CalenderUtils.getDateFormat(tvToDate.getText().toString(), AppConstants.TIMESTAMP_FORMAT);
            assert cDateMax != null;
            datePickerDialog.getDatePicker().setMaxDate(cDateMax.getTime());
        } else if (!isFromSelect && !tvFromDate.getText().toString().isEmpty() && !tvToDate.getText().toString().isEmpty()) {
            Date cDateMin = CalenderUtils.getDateFormat(tvFromDate.getText().toString(), AppConstants.TIMESTAMP_FORMAT);
            assert cDateMin != null;
            datePickerDialog.getDatePicker().setMinDate(cDateMin.getTime());
        }
        datePickerDialog.show();
    }

    /*Animal Calculation Start here*/
    private void doAnimalExpenseCalculation(List<IncomeOrExpenseApplyOnBean> finalApplyList) {
        Float animalAmt = 0f;
        Float propertySaleAmt = 0f;
        for (IncomeOrExpenseApplyOnBean bean : finalApplyList) {

            /*calculation of animal Expense*/
            Animal animal = getDataManager().getAnimalUsingAnimalId(bean.getRecordKey());
            List<String> expenseIdListForAnimal = getDataManager().getExpenseIdFromExpenseMapping(animal.getAnimalId(), animal.getRecordKey());

            /*calculation of parcel single animal Expense*/
            for (String expenseId : expenseIdListForAnimal) {
                String cost = getCost(expenseId, AppConstants.EXPENSE_FOR_ANIMAL);
                String count = getDataManager().getExpenseCountFromExpenseMapping(expenseId);

                animalAmt += Float.parseFloat(cost.isEmpty() ? "0" : cost) / Integer.parseInt(count.isEmpty() ? "1" : count);
            }

             /*calculation of animal sale*/
            String animalSalePrice = getSaleAmt(animal.getAnimalId());
            propertySaleAmt += Float.parseFloat(animalSalePrice.isEmpty() ? "0" : animalSalePrice);

            /*calculation of animallot Expense*/
            Float animalLotAmt = 0f;
            String animalLotId = getDataManager().getAnimalLotIdUsingAnimal(animal.getAnimalId());

            List<String> expenseIdListForAnimalLot = getDataManager().getExpenseIdFromExpenseMapping(animalLotId);

            for (String expenseId : expenseIdListForAnimalLot) {
                String cost = getCost(expenseId, AppConstants.EXPENSE_FOR_ANIMAL);
                String count = getDataManager().getExpenseCountFromExpenseMapping(expenseId);

                animalLotAmt += Float.parseFloat(cost.isEmpty() ? "0" : cost) / Integer.parseInt(count.isEmpty() ? "1" : count);
            }

            /*calculation of lot amt for animal*/
            String animalCount = getDataManager().getAnimalCountUsingAnimalLotId(animalLotId);

            animalLotAmt = (animalLotAmt / Integer.parseInt(animalCount.isEmpty() ? "1" : animalCount));
            animalAmt += animalLotAmt;


            /*calculation of parcel Expense*/
            Float parcelAmt = 0f;
            Parcel parcel = getDataManager().getParcel(animal.getParcel_id());
            List<String> expenseIdListForParcel = getDataManager().getExpenseIdFromExpenseMapping(parcel.getParcelId(), parcel.getRecordKey());

            /*calculation of single parcel Expense*/
            for (String expenseId : expenseIdListForParcel) {
                String cost = getCost(expenseId, AppConstants.EXPENSE_FOR_ANIMAL);
                String count = getDataManager().getExpenseCountFromExpenseMapping(expenseId);

                parcelAmt += Float.parseFloat(cost.isEmpty() ? "0" : cost) / Integer.parseInt(count.isEmpty() ? "1" : count);
            }
            String animalParcelCount = getDataManager().getAnimalCountUsingParcel(parcel.getParcelId());
            parcelAmt = parcelAmt / Integer.parseInt(animalParcelCount.isEmpty() ? "1" : animalParcelCount);
            animalAmt += parcelAmt;


            /*calculation of property Expense*/
            List<String> expenseIdList = getDataManager().getExpenseIdFromExpenseMapping(parcel.getProperty_id());
            Float propertyAmt = 0f;
            for (String expenseId : expenseIdList) {
                String cost = getCost(expenseId, AppConstants.EXPENSE_FOR_ANIMAL);
                String count = getDataManager().getExpenseCountFromExpenseMapping(expenseId);

                propertyAmt += Float.parseFloat(cost.isEmpty() ? "0" : cost) / Integer.parseInt(count.isEmpty() ? "1" : count);
            }
            String parcelCount = getDataManager().getParcelCountUsingProperty(parcel.getProperty_id());
            propertyAmt = propertyAmt / Integer.parseInt(parcelCount.isEmpty() ? "1" : parcelCount);
            propertyAmt = propertyAmt / Integer.parseInt(animalParcelCount.isEmpty() ? "1" : animalParcelCount);

            animalAmt += propertyAmt;
        }

        Float finalAnimalAmt = animalAmt;
        Float finalPropertySaleAmt = propertySaleAmt;
        handler.post(() -> {
            expenseAmt = new StringBuilder("");

            expenseAmt.append("$").append(String.format(Locale.getDefault(), "%.0f", finalAnimalAmt));
            tvTtlExpense.setText(expenseAmt.toString());

            expenseSale = new StringBuilder("");
            expenseSale.append("$").append(String.format(Locale.getDefault(), "%.0f", finalPropertySaleAmt));
            tvSaleAmt.setText(expenseSale.toString());
        });
    }

    private void doAnimalLotExpenseCalculation(List<IncomeOrExpenseApplyOnBean> finalApplyList) {
        Float animalLotAmt = 0f;
        Float propertySaleAmt = 0f;
        for (IncomeOrExpenseApplyOnBean bean : finalApplyList) {
              /*calculation of animallot Expense*/

            List<String> expenseIdListForAnimalLot = getDataManager().getExpenseIdFromExpenseMapping(bean.getRecordKey());

            for (String expenseId : expenseIdListForAnimalLot) {
                String cost = getCost(expenseId, AppConstants.EXPENSE_FOR_ANIMAL);
                String count = getDataManager().getExpenseCountFromExpenseMapping(expenseId);

                animalLotAmt += Float.parseFloat(cost.isEmpty() ? "0" : cost) / Integer.parseInt(count.isEmpty() ? "1" : count);
            }

            /*calculation of animal Expense*/
            List<Animal> animalList = getDataManager().getAnimalUsingAnimalLotId(bean.getRecordKey());

            /*calculation of parcel single animal Expense*/

            for (Animal animal : animalList) {

                List<String> expenseIdListForAnimal = getDataManager().getExpenseIdFromExpenseMapping(animal.getAnimalId(), animal.getRecordKey());
                Float animalAmt = 0f;
                for (String expenseId : expenseIdListForAnimal) {
                    String cost = getCost(expenseId, AppConstants.EXPENSE_FOR_ANIMAL);
                    String count = getDataManager().getExpenseCountFromExpenseMapping(expenseId);

                    animalAmt += Float.parseFloat(cost.isEmpty() ? "0" : cost) / Integer.parseInt(count.isEmpty() ? "1" : count);
                }
                animalLotAmt += animalAmt;

                 /*calculation of animal sale*/
                String animalSalePrice = getSaleAmt(animal.getAnimalId());
                propertySaleAmt += Float.parseFloat(animalSalePrice.isEmpty() ? "0" : animalSalePrice);

                Float parcelAmt = 0f;
                Parcel parcel = getDataManager().getParcel(animal.getParcel_id());
                List<String> expenseIdListForParcel = getDataManager().getExpenseIdFromExpenseMapping(parcel.getParcelId(), parcel.getRecordKey());

            /*calculation of single parcel Expense*/
                for (String expenseId : expenseIdListForParcel) {
                    String cost = getCost(expenseId, AppConstants.EXPENSE_FOR_ANIMAL);
                    String count = getDataManager().getExpenseCountFromExpenseMapping(expenseId);

                    parcelAmt += Float.parseFloat(cost.isEmpty() ? "0" : cost) / Integer.parseInt(count.isEmpty() ? "1" : count);
                }
                String animalParcelCount = getDataManager().getAnimalCountUsingParcel(parcel.getParcelId());
                parcelAmt = parcelAmt / Integer.parseInt(animalParcelCount.isEmpty() ? "1" : animalParcelCount);

                animalLotAmt += parcelAmt;

                  /*calculation of property Expense*/
                List<String> expenseIdList = getDataManager().getExpenseIdFromExpenseMapping(parcel.getProperty_id());
                Float propertyAmt = 0f;
                for (String expenseId : expenseIdList) {
                    String cost = getCost(expenseId, AppConstants.EXPENSE_FOR_ANIMAL);
                    String count = getDataManager().getExpenseCountFromExpenseMapping(expenseId);

                    propertyAmt += Float.parseFloat(cost.isEmpty() ? "0" : cost) / Integer.parseInt(count.isEmpty() ? "1" : count);
                }
                String parcelCount = getDataManager().getParcelCountUsingProperty(parcel.getProperty_id());
                propertyAmt = propertyAmt / Integer.parseInt(parcelCount.isEmpty() ? "1" : parcelCount);
                propertyAmt = propertyAmt / Integer.parseInt(animalParcelCount.isEmpty() ? "1" : animalParcelCount);
                animalLotAmt += propertyAmt;

            }
        }
        Float finalAnimalLotAmt = animalLotAmt;
        Float finalPropertySaleAmt = propertySaleAmt;
        handler.post(() -> {
            expenseAmt = new StringBuilder("");

            expenseAmt.append("$").append(String.format(Locale.getDefault(), "%.0f", finalAnimalLotAmt));
            tvTtlExpense.setText(expenseAmt.toString());

            expenseSale = new StringBuilder("");

            expenseSale.append("$").append(String.format(Locale.getDefault(), "%.0f", finalPropertySaleAmt));
            tvSaleAmt.setText(expenseSale.toString());
        });
    }

    private void doAnimalParcelExpenseCalculation(List<IncomeOrExpenseApplyOnBean> finalApplyList) {
        Float parcelAmt = 0f;
        Float propertySaleAmt = 0f;
        for (IncomeOrExpenseApplyOnBean bean : finalApplyList) {

            /*calculation of parcel Expense*/
            Parcel parcel = getDataManager().getParcel(bean.getRecordKey());
            List<String> expenseIdListForParcel = getDataManager().getExpenseIdFromExpenseMapping(parcel.getParcelId(), parcel.getRecordKey());

            /*calculation of single parcel Expense*/
            for (String expenseId : expenseIdListForParcel) {
                String cost = getCost(expenseId, AppConstants.EXPENSE_FOR_ANIMAL);
                String count = getDataManager().getExpenseCountFromExpenseMapping(expenseId);

                parcelAmt += Float.parseFloat(cost.isEmpty() ? "0" : cost) / Integer.parseInt(count.isEmpty() ? "1" : count);
            }


            /*calculation of parcel animal Expense*/
            List<IncomeBean> animalIdList = getDataManager().getAnimalIdUsingParcel(parcel.getParcelId(), parcel.getRecordKey());

            Float animalAmt = 0f;
            for (IncomeBean incomeBeanA : animalIdList) {
                List<String> expenseIdListForAnimal = getDataManager().getExpenseIdFromExpenseMapping(incomeBeanA.id, incomeBeanA.recordKey);

                /*calculation of parcel single animal Expense*/
                for (String expenseId : expenseIdListForAnimal) {
                    String cost = getCost(expenseId, AppConstants.EXPENSE_FOR_ANIMAL);
                    String count = getDataManager().getExpenseCountFromExpenseMapping(expenseId);

                    animalAmt += Float.parseFloat(cost.isEmpty() ? "0" : cost) / Integer.parseInt(count.isEmpty() ? "1" : count);
                }

                  /*calculation of animal sale*/
                String animalSalePrice = getSaleAmt(incomeBeanA.id);
                propertySaleAmt += Float.parseFloat(animalSalePrice.isEmpty() ? "0" : animalSalePrice);

                /*calculation of animallot Expense*/
                Float animalLotAmt = 0f;
                String animalLotId = getDataManager().getAnimalLotIdUsingAnimal(incomeBeanA.id);

                List<String> expenseIdListForAnimalLot = getDataManager().getExpenseIdFromExpenseMapping(animalLotId);

                for (String expenseId : expenseIdListForAnimalLot) {
                    String cost = getCost(expenseId, AppConstants.EXPENSE_FOR_ANIMAL);
                    String count = getDataManager().getExpenseCountFromExpenseMapping(expenseId);

                    animalLotAmt += Float.parseFloat(cost.isEmpty() ? "0" : cost) / Integer.parseInt(count.isEmpty() ? "1" : count);
                }

                /*calculation of lot amt for animal*/
                String animalCount = getDataManager().getAnimalCountUsingAnimalLotId(animalLotId);

                animalLotAmt = (animalLotAmt / Integer.parseInt(animalCount.isEmpty() ? "1" : animalCount));
                animalAmt += animalLotAmt;
            }
            parcelAmt += animalAmt;


            /*calculation of property Expense*/

            List<String> expenseIdList = getDataManager().getExpenseIdFromExpenseMapping(parcel.getProperty_id());
            Float propertyAmt = 0f;
            for (String expenseId : expenseIdList) {
                String cost = getCost(expenseId, AppConstants.EXPENSE_FOR_ANIMAL);
                String count = getDataManager().getExpenseCountFromExpenseMapping(expenseId);

                propertyAmt += Float.parseFloat(cost.isEmpty() ? "0" : cost) / Integer.parseInt(count.isEmpty() ? "1" : count);
            }
            String parcelCount = getDataManager().getParcelCountUsingProperty(parcel.getProperty_id());
            propertyAmt = propertyAmt / Integer.parseInt(parcelCount.isEmpty() ? "1" : parcelCount);

            parcelAmt += propertyAmt;
        }
        Float finalParcelAmt = parcelAmt;
        Float finalPropertySaleAmt = propertySaleAmt;
        handler.post(() -> {
            expenseAmt = new StringBuilder("");

            expenseAmt.append("$").append(String.format(Locale.getDefault(), "%.0f", finalParcelAmt));
            tvTtlExpense.setText(expenseAmt.toString());

            expenseSale = new StringBuilder("");

            expenseSale.append("$").append(String.format(Locale.getDefault(), "%.0f", finalPropertySaleAmt));
            tvSaleAmt.setText(expenseSale.toString());
        });
    }

    private void doAnimalPropertyExpenseCalculation(List<IncomeOrExpenseApplyOnBean> finalApplyList) {
        Float propertyAmt = 0f;
        Float propertySaleAmt = 0f;
        for (IncomeOrExpenseApplyOnBean bean : finalApplyList) {

            /*calculation of property Expense*/

            //bean.getRecordKey() is propertyId
            List<String> expenseIdList = getDataManager().getExpenseIdFromExpenseMapping(bean.getRecordKey());

            for (String expenseId : expenseIdList) {
                String cost = getCost(expenseId, AppConstants.EXPENSE_FOR_ANIMAL);
                String count = getDataManager().getExpenseCountFromExpenseMapping(expenseId);
                propertyAmt += Float.parseFloat(cost.isEmpty() ? "0" : cost) / Integer.parseInt(count.isEmpty() ? "1" : count);
            }

            /*calculation of parcel Expense*/
            List<IncomeBean> parcelIdList = getDataManager().getPropertyExpenseFromParcel(bean.getRecordKey());

            Float parcelAmt = 0f;
            for (IncomeBean incomeBeanP : parcelIdList) {
                List<String> expenseIdListForParcel = getDataManager().getExpenseIdFromExpenseMapping(incomeBeanP.id, incomeBeanP.recordKey);

                /*calculation of single parcel Expense*/
                for (String expenseId : expenseIdListForParcel) {
                    String cost = getCost(expenseId, AppConstants.EXPENSE_FOR_ANIMAL);
                    String count = getDataManager().getExpenseCountFromExpenseMapping(expenseId);

                    parcelAmt += Float.parseFloat(cost.isEmpty() ? "0" : cost) / Integer.parseInt(count.isEmpty() ? "1" : count);
                }

                /*calculation of parcel animal Expense*/
                List<IncomeBean> animalIdList = getDataManager().getAnimalIdUsingParcel(incomeBeanP.id, incomeBeanP.recordKey);

                Float animalAmt = 0f;
                for (IncomeBean incomeBeanA : animalIdList) {
                    List<String> expenseIdListForAnimal = getDataManager().getExpenseIdFromExpenseMapping(incomeBeanA.id, incomeBeanA.recordKey);

                    /*calculation of parcel single animal Expense*/
                    for (String expenseId : expenseIdListForAnimal) {
                        String cost = getCost(expenseId, AppConstants.EXPENSE_FOR_ANIMAL);
                        String count = getDataManager().getExpenseCountFromExpenseMapping(expenseId);

                        animalAmt += Float.parseFloat(cost.isEmpty() ? "0" : cost) / Integer.parseInt(count.isEmpty() ? "1" : count);
                    }


                    /*calculation of animal sale*/
                    String animalSalePrice = getSaleAmt(incomeBeanA.id);
                    propertySaleAmt += Float.parseFloat(animalSalePrice.isEmpty() ? "0" : animalSalePrice);

                    /*calculation of animallot Expense*/
                    Float animalLotAmt = 0f;
                    String animalLotId = getDataManager().getAnimalLotIdUsingAnimal(incomeBeanA.id);

                    List<String> expenseIdListForAnimalLot = getDataManager().getExpenseIdFromExpenseMapping(animalLotId);

                    for (String expenseId : expenseIdListForAnimalLot) {
                        String cost = getCost(expenseId, AppConstants.EXPENSE_FOR_ANIMAL);
                        String count = getDataManager().getExpenseCountFromExpenseMapping(expenseId);

                        animalLotAmt += Float.parseFloat(cost.isEmpty() ? "0" : cost) / Integer.parseInt(count.isEmpty() ? "1" : count);
                    }

                    /*calculation of lot amt for animal*/
                    String animalCount = getDataManager().getAnimalCountUsingAnimalLotId(animalLotId);

                    animalLotAmt = (animalLotAmt / Integer.parseInt(animalCount.isEmpty() ? "1" : animalCount));
                    animalAmt += animalLotAmt;
                }
                parcelAmt += animalAmt;
            }
            propertyAmt += parcelAmt;
        }
        Float finalPropertyAmt = propertyAmt;
        Float finalPropertySaleAmt = propertySaleAmt;
        handler.post(() -> {
            expenseAmt = new StringBuilder("");

            expenseAmt.append("$").append(String.format(Locale.getDefault(), "%.0f", finalPropertyAmt));
            tvTtlExpense.setText(expenseAmt.toString());

            expenseSale = new StringBuilder("");
            expenseSale.append("$").append(String.format(Locale.getDefault(), "%.0f", finalPropertySaleAmt));
            tvSaleAmt.setText(expenseSale.toString());
        });
    }
    /*Animal Calculation End here*/

    /*Crop Calculation Start here*/
    private void doCropExpenseCalculation(List<IncomeOrExpenseApplyOnBean> finalApplyList) {
        Float cropAmt = 0f;
        Float propertySaleAmt = 0f;
        for (IncomeOrExpenseApplyOnBean bean : finalApplyList) {
            /* calculation of crop Expense */

            Crop crop = getDataManager().getCrop(bean.getRecordKey());
            List<String> expenseIdListForCrop = getDataManager().getExpenseIdFromExpenseMapping(crop.getCropId(), crop.getRecordKey());

            /* calculation of parcel single crop Expense */
            for (String expenseId : expenseIdListForCrop) {
                String cost = getCost(expenseId, AppConstants.EXPENSE_FOR_CROP);
                String count = getDataManager().getExpenseCountFromExpenseMapping(expenseId);

                cropAmt += Float.parseFloat(cost.isEmpty() ? "0" : cost) / Integer.parseInt(count.isEmpty() ? "1" : count);
            }

             /*calculation of crop sale*/
            String cropSalePrice = getSaleAmt(bean.getRecordKey());
            propertySaleAmt += Float.parseFloat(cropSalePrice.isEmpty() ? "0" : cropSalePrice);

             /*calculation of croplot Expense*/
            Float cropLotAmt = 0f;
            String cropLotId = getDataManager().getCropLotIdUsingCrop(crop.getCropId());

            List<String> expenseIdListForCropLot = getDataManager().getExpenseIdFromExpenseMapping(cropLotId);

            for (String expenseId : expenseIdListForCropLot) {
                String cost = getCost(expenseId, AppConstants.EXPENSE_FOR_CROP);
                String count = getDataManager().getExpenseCountFromExpenseMapping(expenseId);

                cropLotAmt += Float.parseFloat(cost.isEmpty() ? "0" : cost) / Integer.parseInt(count.isEmpty() ? "1" : count);
            }

            /*calculation of lot amt for crop*/
            String cropCount = getDataManager().getCropCountUsingCropLotId(cropLotId);

            cropLotAmt = (cropLotAmt / Integer.parseInt(cropCount.isEmpty() ? "1" : cropCount));
            cropAmt += cropLotAmt;


            /*calculation of parcel Expense*/
            Float parcelAmt = 0f;
            Parcel parcel = getDataManager().getParcel(crop.getParcel_id());
            List<String> expenseIdListForParcel = getDataManager().getExpenseIdFromExpenseMapping(parcel.getParcelId(), parcel.getRecordKey());

            /*calculation of single parcel Expense*/
            for (String expenseId : expenseIdListForParcel) {
                String cost = getCost(expenseId, AppConstants.EXPENSE_FOR_CROP);
                String count = getDataManager().getExpenseCountFromExpenseMapping(expenseId);

                parcelAmt += Float.parseFloat(cost.isEmpty() ? "0" : cost) / Integer.parseInt(count.isEmpty() ? "1" : count);
            }
            String cropParcelCount = getDataManager().getCropCountUsingParcel(parcel.getParcelId());
            parcelAmt = parcelAmt / Integer.parseInt(cropParcelCount.isEmpty() ? "1" : cropParcelCount);
            cropAmt += parcelAmt;


            /*calculation of property Expense*/
            List<String> expenseIdList = getDataManager().getExpenseIdFromExpenseMapping(parcel.getProperty_id());
            Float propertyAmt = 0f;
            for (String expenseId : expenseIdList) {
                String cost = getCost(expenseId, AppConstants.EXPENSE_FOR_CROP);
                String count = getDataManager().getExpenseCountFromExpenseMapping(expenseId);

                propertyAmt += Float.parseFloat(cost.isEmpty() ? "0" : cost) / Integer.parseInt(count.isEmpty() ? "1" : count);
            }
            String parcelCount = getDataManager().getParcelCountUsingProperty(parcel.getProperty_id());
            propertyAmt = propertyAmt / Integer.parseInt(parcelCount.isEmpty() ? "1" : parcelCount);
            propertyAmt = propertyAmt / Integer.parseInt(cropParcelCount.isEmpty() ? "1" : cropParcelCount);

            cropAmt += propertyAmt;
        }

        Float finalCropAmt = cropAmt;
        Float finalPropertySaleAmt = propertySaleAmt;
        handler.post(() -> {
            expenseAmt = new StringBuilder("");
            expenseAmt.append("$").append(String.format(Locale.getDefault(), "%.0f", finalCropAmt));
            tvTtlExpense.setText(expenseAmt.toString());

            expenseSale = new StringBuilder("");

            expenseSale.append("$").append(String.format(Locale.getDefault(), "%.0f", finalPropertySaleAmt));
            tvSaleAmt.setText(expenseSale.toString());
        });
    }

    private void doCropLotExpenseCalculation(List<IncomeOrExpenseApplyOnBean> finalApplyList) {
        Float cropLotAmt = 0f;
        Float propertySaleAmt = 0f;
        for (IncomeOrExpenseApplyOnBean bean : finalApplyList) {
              /*calculation of croplot Expense*/

            List<String> expenseIdListForCropLot = getDataManager().getExpenseIdFromExpenseMapping(bean.getRecordKey());

            for (String expenseId : expenseIdListForCropLot) {
                String cost = getCost(expenseId, AppConstants.EXPENSE_FOR_CROP);
                String count = getDataManager().getExpenseCountFromExpenseMapping(expenseId);

                cropLotAmt += Float.parseFloat(cost.isEmpty() ? "0" : cost) / Integer.parseInt(count.isEmpty() ? "1" : count);
            }

            /*calculation of crop Expense*/
            List<Crop> cropList = getDataManager().getCropUsingCropLotId(bean.getRecordKey());
            /*calculation of parcel single crop Expense*/

            for (Crop crop : cropList) {
                List<String> expenseIdListForCrop = getDataManager().getExpenseIdFromExpenseMapping(crop.getCropId(), crop.getRecordKey());
                Float cropAmt = 0f;
                for (String expenseId : expenseIdListForCrop) {
                    String cost = getCost(expenseId, AppConstants.EXPENSE_FOR_CROP);
                    String count = getDataManager().getExpenseCountFromExpenseMapping(expenseId);

                    cropAmt += Float.parseFloat(cost.isEmpty() ? "0" : cost) / Integer.parseInt(count.isEmpty() ? "1" : count);
                }
                cropLotAmt += cropAmt;

                /*calculation of crop sale*/
                String cropSalePrice = getSaleAmt(crop.getCropId());
                propertySaleAmt += Float.parseFloat(cropSalePrice.isEmpty() ? "0" : cropSalePrice);

                Float parcelAmt = 0f;
                Parcel parcel = getDataManager().getParcel(crop.getParcel_id());
                List<String> expenseIdListForParcel = getDataManager().getExpenseIdFromExpenseMapping(parcel.getParcelId(), parcel.getRecordKey());

            /*calculation of single parcel Expense*/
                for (String expenseId : expenseIdListForParcel) {
                    String cost = getCost(expenseId, AppConstants.EXPENSE_FOR_CROP);
                    String count = getDataManager().getExpenseCountFromExpenseMapping(expenseId);

                    parcelAmt += Float.parseFloat(cost.isEmpty() ? "0" : cost) / Integer.parseInt(count.isEmpty() ? "1" : count);
                }
                String cropParcelCount = getDataManager().getCropCountUsingParcel(parcel.getParcelId());
                parcelAmt = parcelAmt / Integer.parseInt(cropParcelCount.isEmpty() ? "1" : cropParcelCount);

                cropLotAmt += parcelAmt;

                  /*calculation of property Expense*/
                List<String> expenseIdList = getDataManager().getExpenseIdFromExpenseMapping(parcel.getProperty_id());
                Float propertyAmt = 0f;
                for (String expenseId : expenseIdList) {
                    String cost = getCost(expenseId, AppConstants.EXPENSE_FOR_CROP);
                    String count = getDataManager().getExpenseCountFromExpenseMapping(expenseId);

                    propertyAmt += Float.parseFloat(cost.isEmpty() ? "0" : cost) / Integer.parseInt(count.isEmpty() ? "1" : count);
                }
                String parcelCount = getDataManager().getParcelCountUsingProperty(parcel.getProperty_id());
                propertyAmt = propertyAmt / Integer.parseInt(parcelCount.isEmpty() ? "1" : parcelCount);
                propertyAmt = propertyAmt / Integer.parseInt(cropParcelCount.isEmpty() ? "1" : cropParcelCount);
                cropLotAmt += propertyAmt;
            }
        }
        Float finalCropLotAmt = cropLotAmt;
        Float finalPropertySaleAmt = propertySaleAmt;
        handler.post(() -> {
            expenseAmt = new StringBuilder("");
            expenseAmt.append("$").append(String.format(Locale.getDefault(), "%.0f", finalCropLotAmt));
            tvTtlExpense.setText(expenseAmt.toString());

            expenseSale = new StringBuilder("");
            expenseSale.append("$").append(String.format(Locale.getDefault(), "%.0f", finalPropertySaleAmt));
            tvSaleAmt.setText(expenseSale.toString());
        });
    }

    private void doCropParcelExpenseCalculation(List<IncomeOrExpenseApplyOnBean> finalApplyList) {
        Float parcelAmt = 0f;
        Float propertySaleAmt = 0f;
        for (IncomeOrExpenseApplyOnBean bean : finalApplyList) {

            /*calculation of parcel Expense*/
            Parcel parcel = getDataManager().getParcel(bean.getRecordKey());
            List<String> expenseIdListForParcel = getDataManager().getExpenseIdFromExpenseMapping(parcel.getParcelId(), parcel.getRecordKey());

            /*calculation of single parcel Expense*/
            for (String expenseId : expenseIdListForParcel) {
                String cost = getCost(expenseId, AppConstants.EXPENSE_FOR_CROP);
                String count = getDataManager().getExpenseCountFromExpenseMapping(expenseId);

                parcelAmt += Float.parseFloat(cost.isEmpty() ? "0" : cost) / Integer.parseInt(count.isEmpty() ? "1" : count);
            }


            /*calculation of parcel crop Expense*/
            List<IncomeBean> cropIdList = getDataManager().getCropIdUsingParcel(parcel.getParcelId(), parcel.getRecordKey());

            Float cropAmt = 0f;
            for (IncomeBean incomeBeanA : cropIdList) {
                List<String> expenseIdListForCrop = getDataManager().getExpenseIdFromExpenseMapping(incomeBeanA.id, incomeBeanA.recordKey);

                /*calculation of parcel single crop Expense*/
                for (String expenseId : expenseIdListForCrop) {
                    String cost = getCost(expenseId, AppConstants.EXPENSE_FOR_CROP);
                    String count = getDataManager().getExpenseCountFromExpenseMapping(expenseId);

                    cropAmt += Float.parseFloat(cost.isEmpty() ? "0" : cost) / Integer.parseInt(count.isEmpty() ? "1" : count);
                }

                 /*calculation of crop sale*/
                String cropSalePrice = getSaleAmt(incomeBeanA.id);
                propertySaleAmt += Float.parseFloat(cropSalePrice.isEmpty() ? "0" : cropSalePrice);

                /*calculation of croplot Expense*/
                Float cropLotAmt = 0f;
                String cropLotId = getDataManager().getCropLotIdUsingCrop(incomeBeanA.id);

                List<String> expenseIdListForCropLot = getDataManager().getExpenseIdFromExpenseMapping(cropLotId);

                for (String expenseId : expenseIdListForCropLot) {
                    String cost = getCost(expenseId, AppConstants.EXPENSE_FOR_CROP);
                    String count = getDataManager().getExpenseCountFromExpenseMapping(expenseId);

                    cropLotAmt += Float.parseFloat(cost.isEmpty() ? "0" : cost) / Integer.parseInt(count.isEmpty() ? "1" : count);
                }

                /*calculation of lot amt for crop*/
                String cropCount = getDataManager().getCropCountUsingCropLotId(cropLotId);

                cropLotAmt = (cropLotAmt / Integer.parseInt(cropCount.isEmpty() ? "1" : cropCount));
                cropAmt += cropLotAmt;
            }
            parcelAmt += cropAmt;


            /*calculation of property Expense*/

            List<String> expenseIdList = getDataManager().getExpenseIdFromExpenseMapping(parcel.getProperty_id());
            Float propertyAmt = 0f;
            for (String expenseId : expenseIdList) {
                String cost = getCost(expenseId, AppConstants.EXPENSE_FOR_CROP);
                String count = getDataManager().getExpenseCountFromExpenseMapping(expenseId);

                propertyAmt += Float.parseFloat(cost.isEmpty() ? "0" : cost) / Integer.parseInt(count.isEmpty() ? "1" : count);
            }
            String parcelCount = getDataManager().getParcelCountUsingProperty(parcel.getProperty_id());
            propertyAmt = propertyAmt / Integer.parseInt(parcelCount.isEmpty() ? "1" : parcelCount);

            parcelAmt += propertyAmt;
        }
        Float finalParcelAmt = parcelAmt;
        Float finalPropertySaleAmt = propertySaleAmt;
        handler.post(() -> {
            expenseAmt = new StringBuilder("");
            expenseAmt.append("$").append(String.format(Locale.getDefault(), "%.0f", finalParcelAmt));
            tvTtlExpense.setText(expenseAmt.toString());

            expenseSale = new StringBuilder("");
            expenseSale.append("$").append(String.format(Locale.getDefault(), "%.0f", finalPropertySaleAmt));
            tvSaleAmt.setText(expenseSale.toString());
        });
    }

    private void doCropPropertyExpenseCalculation(List<IncomeOrExpenseApplyOnBean> finalApplyList) {
        Float propertyAmt = 0f;
        Float propertySaleAmt = 0f;
        for (IncomeOrExpenseApplyOnBean bean : finalApplyList) {

            /*calculation of property Expense*/

            //bean.getRecordKey() is propertyId
            List<String> expenseIdList = getDataManager().getExpenseIdFromExpenseMapping(bean.getRecordKey());

            for (String expenseId : expenseIdList) {
                String cost = getCost(expenseId, AppConstants.EXPENSE_FOR_CROP);
                String count = getDataManager().getExpenseCountFromExpenseMapping(expenseId);
                propertyAmt += Float.parseFloat(cost.isEmpty() ? "0" : cost) / Integer.parseInt(count.isEmpty() ? "1" : count);
            }

            /*calculation of parcel Expense*/
            List<IncomeBean> parcelIdList = getDataManager().getPropertyExpenseFromParcel(bean.getRecordKey());

            Float parcelAmt = 0f;
            for (IncomeBean incomeBeanP : parcelIdList) {
                List<String> expenseIdListForParcel = getDataManager().getExpenseIdFromExpenseMapping(incomeBeanP.id, incomeBeanP.recordKey);

                /*calculation of single parcel Expense*/
                for (String expenseId : expenseIdListForParcel) {
                    String cost = getCost(expenseId, AppConstants.EXPENSE_FOR_CROP);
                    String count = getDataManager().getExpenseCountFromExpenseMapping(expenseId);

                    parcelAmt += Float.parseFloat(cost.isEmpty() ? "0" : cost) / Integer.parseInt(count.isEmpty() ? "1" : count);
                }

                /*calculation of parcel crop Expense*/
                List<IncomeBean> cropIdList = getDataManager().getCropIdUsingParcel(incomeBeanP.id, incomeBeanP.recordKey);

                Float cropAmt = 0f;
                for (IncomeBean incomeBeanA : cropIdList) {
                    List<String> expenseIdListForCrop = getDataManager().getExpenseIdFromExpenseMapping(incomeBeanA.id, incomeBeanA.recordKey);

                    /*calculation of parcel single crop Expense*/
                    for (String expenseId : expenseIdListForCrop) {
                        String cost = getCost(expenseId, AppConstants.EXPENSE_FOR_CROP);
                        String count = getDataManager().getExpenseCountFromExpenseMapping(expenseId);

                        cropAmt += Float.parseFloat(cost.isEmpty() ? "0" : cost) / Integer.parseInt(count.isEmpty() ? "1" : count);
                    }


                    /*calculation of crop sale*/
                    String cropSalePrice = getSaleAmt(incomeBeanA.id);
                    propertySaleAmt += Float.parseFloat(cropSalePrice.isEmpty() ? "0" : cropSalePrice);

                    /*calculation of croplot Expense*/
                    Float cropLotAmt = 0f;
                    String cropLotId = getDataManager().getCropLotIdUsingCrop(incomeBeanA.id);

                    List<String> expenseIdListForCropLot = getDataManager().getExpenseIdFromExpenseMapping(cropLotId);

                    for (String expenseId : expenseIdListForCropLot) {
                        String cost = getCost(expenseId, AppConstants.EXPENSE_FOR_CROP);
                        String count = getDataManager().getExpenseCountFromExpenseMapping(expenseId);

                        cropLotAmt += Float.parseFloat(cost.isEmpty() ? "0" : cost) / Integer.parseInt(count.isEmpty() ? "1" : count);
                    }

                    /*calculation of lot amt for crop*/
                    String cropCount = getDataManager().getCropCountUsingCropLotId(cropLotId);

                    cropLotAmt = (cropLotAmt / Integer.parseInt(cropCount.isEmpty() ? "1" : cropCount));
                    cropAmt += cropLotAmt;
                }
                parcelAmt += cropAmt;
            }
            propertyAmt += parcelAmt;
        }
        Float finalPropertyAmt = propertyAmt;
        Float finalPropertySaleAmt = propertySaleAmt;
        handler.post(() -> {
            expenseAmt = new StringBuilder("");
            expenseAmt.append("$").append(String.format(Locale.getDefault(), "%.0f", finalPropertyAmt));
            tvTtlExpense.setText(expenseAmt.toString());

            expenseSale = new StringBuilder("");
            expenseSale.append("$").append(String.format(Locale.getDefault(), "%.0f", finalPropertySaleAmt));
            tvSaleAmt.setText(expenseSale.toString());
        });
    }
    /*Crop Calculation End here*/

    private String getSaleAmt(String animalOrCropId) {
        String salePrice;
        if (incomeType.equals("animal")) {
            if (!fromDate.toString().isEmpty() && toDate.toString().isEmpty()) {
                String tempFromDate = CalenderUtils.formatDate(String.valueOf(fromDate), AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT);
                salePrice = getDataManager().getTtlSaleFromAnimal(animalOrCropId, CalenderUtils.getDateFormat(tempFromDate, AppConstants.SERVER_TIMESTAMP_FORMAT));
                return salePrice == null ? "" : salePrice;
            } else if (!fromDate.toString().isEmpty() && !toDate.toString().isEmpty()) {
                String tempFromDate = CalenderUtils.formatDate(String.valueOf(fromDate), AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT);
                String tempToDate = CalenderUtils.formatDate(String.valueOf(toDate), AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT);
                salePrice = getDataManager().getTtlSaleFromAnimal(animalOrCropId, CalenderUtils.getDateFormat(tempFromDate, AppConstants.SERVER_TIMESTAMP_FORMAT), CalenderUtils.getDateFormat(tempToDate, AppConstants.SERVER_TIMESTAMP_FORMAT));
                return salePrice == null ? "" : salePrice;
            } else {
                salePrice = getDataManager().getTtlSaleFromAnimal(animalOrCropId);
                return salePrice == null ? "" : salePrice;
            }
        } else {
            if (!fromDate.toString().isEmpty() && toDate.toString().isEmpty()) {
                String tempFromDate = CalenderUtils.formatDate(String.valueOf(fromDate), AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT);
                salePrice = getDataManager().getTtlSaleFromCrop(animalOrCropId, CalenderUtils.getDateFormat(tempFromDate, AppConstants.SERVER_TIMESTAMP_FORMAT));
                return salePrice == null ? "" : salePrice;
            } else if (!fromDate.toString().isEmpty() && !toDate.toString().isEmpty()) {
                String tempFromDate = CalenderUtils.formatDate(String.valueOf(fromDate), AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT);
                String tempToDate = CalenderUtils.formatDate(String.valueOf(toDate), AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT);
                salePrice = getDataManager().getTtlSaleFromCrop(animalOrCropId, CalenderUtils.getDateFormat(tempFromDate, AppConstants.SERVER_TIMESTAMP_FORMAT), CalenderUtils.getDateFormat(tempToDate, AppConstants.SERVER_TIMESTAMP_FORMAT));
                return salePrice == null ? "" : salePrice;
            } else {
                salePrice = getDataManager().getTtlSaleFromCrop(animalOrCropId);
                return salePrice == null ? "" : salePrice;
            }
        }
    }

    private String getCost(String expenseId, String expenseFor) {
        String cost;
        if (!fromDate.toString().isEmpty() && toDate.toString().isEmpty()) {
            String tempFromDate = CalenderUtils.formatDate(String.valueOf(fromDate), AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT);
            cost = getDataManager().getExpenseCostFromExpense(expenseId, expenseFor, CalenderUtils.getDateFormat(tempFromDate, AppConstants.SERVER_TIMESTAMP_FORMAT));
            return cost == null ? "" : cost;
        } else if (!fromDate.toString().isEmpty() && !toDate.toString().isEmpty()) {
            String tempFromDate = CalenderUtils.formatDate(String.valueOf(fromDate), AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT);
            String tempToDate = CalenderUtils.formatDate(String.valueOf(toDate), AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT);
            cost = getDataManager().getExpenseCostFromExpense(expenseId, expenseFor, CalenderUtils.getDateFormat(tempFromDate, AppConstants.SERVER_TIMESTAMP_FORMAT), CalenderUtils.getDateFormat(tempToDate, AppConstants.SERVER_TIMESTAMP_FORMAT));
            return cost == null ? "" : cost;
        } else {
            cost = getDataManager().getExpenseCostFromExpense(expenseId, expenseFor);
            return cost == null ? "" : cost;
        }
    }
}
