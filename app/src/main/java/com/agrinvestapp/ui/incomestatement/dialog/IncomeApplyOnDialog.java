package com.agrinvestapp.ui.incomestatement.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.other.IncomeOrExpenseApplyOnBean;
import com.agrinvestapp.ui.base.BaseDialog;
import com.agrinvestapp.ui.incomestatement.adapter.ApplyOnIncomeAdapter;
import com.agrinvestapp.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;


public class IncomeApplyOnDialog extends BaseDialog implements View.OnClickListener {

    private static final String TAG = IncomeApplyOnDialog.class.getSimpleName();

    private IncomeDialogCallback callback;
    private List<IncomeOrExpenseApplyOnBean> applyOnList;


    public static IncomeApplyOnDialog newInstance(List<IncomeOrExpenseApplyOnBean> applyOnList, IncomeDialogCallback callback) {

        Bundle args = new Bundle();

        IncomeApplyOnDialog fragment = new IncomeApplyOnDialog();
        fragment.setOnAddEditListener(applyOnList, callback);
        fragment.setArguments(args);
        return fragment;
    }

    private void setOnAddEditListener(List<IncomeOrExpenseApplyOnBean> applyOnList, IncomeDialogCallback callback) {
        this.applyOnList = applyOnList;
        this.callback = callback;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_expense_applyon, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView rvApplyOnWhat = view.findViewById(R.id.rvApplyOnWhat);
        view.findViewById(R.id.btnCancel).setOnClickListener(this);
        view.findViewById(R.id.btnDone).setOnClickListener(this);
        TextView tvNoRecord = view.findViewById(R.id.tvNoRecord);

        tvNoRecord.setVisibility(applyOnList.isEmpty() ? View.VISIBLE : View.GONE);

        ApplyOnIncomeAdapter spApplyOnAdapter = new ApplyOnIncomeAdapter(applyOnList);

        //Fixed height when list contain more than 10 data
        ViewGroup.LayoutParams params = rvApplyOnWhat.getLayoutParams();
        int size = applyOnList.size();
        if (size > 10) {
            params.height = (int) getResources().getDimension(R.dimen.fourTwenty);
        }
        rvApplyOnWhat.setLayoutParams(params);

        rvApplyOnWhat.setAdapter(spApplyOnAdapter);
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCancel:
                dismissDialog(TAG);
                break;

            case R.id.btnDone:
                StringBuilder applyOnBuilder = new StringBuilder("");
                StringBuilder applyOnShowBuilder = new StringBuilder("");
                List<IncomeOrExpenseApplyOnBean> applyOnFinalList = new ArrayList<>();
                for (IncomeOrExpenseApplyOnBean applyOnBean : applyOnList) {
                    if (applyOnBean.getSelect()) {
                        applyOnFinalList.add(applyOnBean);
                        applyOnBuilder.append(applyOnBean.getRecordKey()).append(",");
                        applyOnShowBuilder.append(applyOnBean.getName()).append(", ");
                    }
                }
                String tempApplyOn = applyOnBuilder.toString().trim();
                String tempApplyOnShow = applyOnShowBuilder.toString().trim();
                tempApplyOn = tempApplyOn.isEmpty() ? "" : tempApplyOn.substring(0, tempApplyOn.length() - 1);
                tempApplyOnShow = tempApplyOnShow.isEmpty() ? "" : tempApplyOnShow.substring(0, tempApplyOnShow.length() - 1);

                if (verifyInput(tempApplyOn)) {
                    dismissDialog(TAG);
                    callback.onDone(applyOnList, tempApplyOn, tempApplyOnShow, applyOnFinalList);
                }
                break;
        }
    }

    private boolean verifyInput(String tempApplyOn) {
        if (tempApplyOn.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_select_expense_option), Toast.LENGTH_SHORT);
            return false;
        } else return true;
    }

}
