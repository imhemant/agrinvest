package com.agrinvestapp.ui.incomestatement.dialog;

import com.agrinvestapp.data.model.other.IncomeOrExpenseApplyOnBean;

import java.util.List;

public interface IncomeDialogCallback {
    void onDone(List<IncomeOrExpenseApplyOnBean> applyOnList, String data, String showData, List<IncomeOrExpenseApplyOnBean> applyOnFinalList);
}
