package com.agrinvestapp.ui.property.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.AnimalInfoResponse;
import com.agrinvestapp.utils.pagination.FooterLoader;

import java.util.List;


/**
 * Created by hemant
 * Date: 17/4/18
 * Time: 4:03 PM
 */

public class ParcelAnimalListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEWTYPE_ITEM = 1;
    private final int VIEWTYPE_LOADER = 2;
    private List<AnimalInfoResponse.AnimalListBean> list;
    private boolean showLoader;
    private StringBuilder builder;


    public ParcelAnimalListAdapter(List<AnimalInfoResponse.AnimalListBean> list) {
        this.list = list;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder rvHolder, int position) {
        if (rvHolder instanceof FooterLoader) {
            FooterLoader loaderViewHolder = (FooterLoader) rvHolder;
            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }
            return;
        }
        ParcelAnimalListAdapter.ViewHolder holder = ((ParcelAnimalListAdapter.ViewHolder) rvHolder);
        builder = new StringBuilder("");
        builder.append("#").append(list.get(position).getTagNumber());
        holder.tvTag.setText(builder);

        builder = new StringBuilder("");
        builder.append("#").append(list.get(position).getLotId());
        holder.tvLotId.setText(builder);
    }

    public void showLoading(boolean status) {
        showLoader = status;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case VIEWTYPE_ITEM:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_parcel_detail, parent, false);
                return new ViewHolder(view);

            case VIEWTYPE_LOADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pagination_item_loader, parent, false);
                return new FooterLoader(view);
        }
        return null;

    }

    @Override
    public int getItemViewType(int position) {
        if (position == list.size() - 1) {
            return showLoader ? VIEWTYPE_LOADER : VIEWTYPE_ITEM;
        }
        return VIEWTYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvTag, tvLotId;

        ViewHolder(@NonNull View v) {
            super(v);
            tvTag = v.findViewById(R.id.tvTag);
            tvLotId = v.findViewById(R.id.tvLotId);
            v.findViewById(R.id.cvMain).setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            //nothing
        }
    }

}

