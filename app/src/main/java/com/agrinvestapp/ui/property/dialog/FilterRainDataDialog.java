package com.agrinvestapp.ui.property.dialog;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.ui.base.BaseDialog;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.CalenderUtils;
import com.agrinvestapp.utils.CommonUtils;

import java.util.Calendar;
import java.util.Date;


public class FilterRainDataDialog extends BaseDialog implements View.OnClickListener {

    private static final String TAG = FilterRainDataDialog.class.getSimpleName();

    private FilterRainDataCallback callback;

    private TextView tvFromDate, tvToDate;

    private StringBuilder fromDate, toDate;
    private String strFromDate, strToDate;
    private Boolean isFromSelect = false;

    // the callback received when the user "sets" the Date in the
    // DatePickerDialog
    @NonNull
    private DatePickerDialog.OnDateSetListener datePicker = (view, selectedYear, selectedMonth, selectedDay) -> {
        selectedMonth += 1;

        if (isFromSelect) {
            fromDate = new StringBuilder("");
            fromDate.append(selectedDay).append("/").append(selectedMonth).append("/").append(selectedYear);

            tvFromDate.setText(CalenderUtils.formatDate(String.valueOf(fromDate), AppConstants.TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT));
        } else {
            toDate = new StringBuilder("");
            toDate.append(selectedDay).append("/").append(selectedMonth).append("/").append(selectedYear);
            tvToDate.setText(CalenderUtils.formatDate(String.valueOf(toDate), AppConstants.TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT));
        }
    };

    public void setOnRainFilterListener(String fromDate, String toDate, FilterRainDataCallback callback) {
        strFromDate = fromDate.contains("-") ? CalenderUtils.formatDate(fromDate, AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT) : fromDate;
        strToDate = toDate.contains("-") ? CalenderUtils.formatDate(toDate, AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT) : toDate;
        this.callback = callback;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_filter_rain_data, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tvFromDate = view.findViewById(R.id.tvFromDate);
        tvFromDate.setOnClickListener(this);
        tvToDate = view.findViewById(R.id.tvToDate);
        tvToDate.setOnClickListener(this);

        tvFromDate.setText(strFromDate);
        tvToDate.setText(strToDate);

        view.findViewById(R.id.btnDone).setOnClickListener(this);
        view.findViewById(R.id.btnReset).setOnClickListener(this);
        view.findViewById(R.id.frameCancel).setOnClickListener(this);
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnDone:
                String fromDate = tvFromDate.getText().toString().trim();
                String toDate = tvToDate.getText().toString().trim();
                if (verifyInput(fromDate)) {
                    dismissDialog(TAG);
                    callback.onFilterData(fromDate, toDate);
                }
                break;

            case R.id.frameCancel:
                dismissDialog(TAG);
                break;

            case R.id.btnReset:
                callback.onResetClick();
                dismissDialog(TAG);
                break;

            case R.id.tvFromDate:
                isFromSelect = true;
                getDate(tvFromDate.getText().toString().trim());
                break;

            case R.id.tvToDate:
                isFromSelect = false;
                getDate(tvToDate.getText().toString().trim());
                break;
        }
    }

    private boolean verifyInput(String fromDate) {
        if (fromDate.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_select_fdate), Toast.LENGTH_SHORT);
            return false;
        } else return true;
    }

    private void getDate(String selectedDate) {
        //date 26/06/2018

        Calendar newCalender = Calendar.getInstance();

        int day, month, year;

        if (selectedDate.isEmpty()) {
            day = newCalender.get(Calendar.DAY_OF_MONTH);
            month = newCalender.get(Calendar.MONTH);
            year = newCalender.get(Calendar.YEAR);
        } else {
            String[] date = selectedDate.split("/");

            day = Integer.parseInt(date[0]);
            month = Integer.parseInt(date[1]) - 1;
            year = Integer.parseInt(date[2]);
        }

        DatePickerDialog datePickerDialog = new DatePickerDialog(getBaseActivity(), datePicker, year, month, day);
        Date maxDate = CalenderUtils.getDateFormat(CalenderUtils.getCurrentDate(), AppConstants.TIMESTAMP_FORMAT);
        assert maxDate != null;
        datePickerDialog.getDatePicker().setMaxDate(maxDate.getTime());

        if (!isFromSelect && !tvFromDate.getText().toString().isEmpty() && tvToDate.getText().toString().isEmpty()) {
            Date cDate = CalenderUtils.getDateFormat(tvFromDate.getText().toString(), AppConstants.TIMESTAMP_FORMAT);
            assert cDate != null;
            datePickerDialog.getDatePicker().setMinDate(cDate.getTime());
        } else if (isFromSelect && tvFromDate.getText().toString().isEmpty() && !tvToDate.getText().toString().isEmpty()) {
            Date cDate = CalenderUtils.getDateFormat(tvToDate.getText().toString(), AppConstants.TIMESTAMP_FORMAT);
            assert cDate != null;
            datePickerDialog.getDatePicker().setMaxDate(cDate.getTime());
        } else if (isFromSelect && !tvFromDate.getText().toString().isEmpty() && !tvToDate.getText().toString().isEmpty()) {
            Date cDateMax = CalenderUtils.getDateFormat(tvToDate.getText().toString(), AppConstants.TIMESTAMP_FORMAT);
            assert cDateMax != null;
            datePickerDialog.getDatePicker().setMaxDate(cDateMax.getTime());
        } else if (!isFromSelect && !tvFromDate.getText().toString().isEmpty() && !tvToDate.getText().toString().isEmpty()) {
            Date cDateMin = CalenderUtils.getDateFormat(tvFromDate.getText().toString(), AppConstants.TIMESTAMP_FORMAT);
            assert cDateMin != null;
            datePickerDialog.getDatePicker().setMinDate(cDateMin.getTime());
        }
        datePickerDialog.show();
    }

}
