package com.agrinvestapp.ui.property.fragment;

public interface CheckSyncParcelCallback {
    void onSync(String data);

    void onCallApi();
}
