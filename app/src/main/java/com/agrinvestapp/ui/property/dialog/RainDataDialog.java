package com.agrinvestapp.ui.property.dialog;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.RainBeanResponse;
import com.agrinvestapp.ui.base.BaseDialog;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.CalenderUtils;
import com.agrinvestapp.utils.CommonUtils;

import java.util.Calendar;
import java.util.Date;


public class RainDataDialog extends BaseDialog implements View.OnClickListener {

    private static final String TAG = RainDataDialog.class.getSimpleName();

    private RainDataCallback callback;

    private EditText etScale;
    private TextView tvMM, tvInch, tvFromDate, tvToDate;

    private String scaleType = "mm";
    private StringBuilder fromDate, toDate;
    private Boolean isFromSelect = false;

    // the callback received when the user "sets" the Date in the
// DatePickerDialog
    @NonNull
    private DatePickerDialog.OnDateSetListener datePicker = (view, selectedYear, selectedMonth, selectedDay) -> {
        selectedMonth += 1;

        if (isFromSelect) {
            fromDate = new StringBuilder("");
            fromDate.append(selectedDay).append("/").append(selectedMonth).append("/").append(selectedYear);

            tvFromDate.setText(CalenderUtils.formatDate(String.valueOf(fromDate), AppConstants.TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT));
        } else {
            toDate = new StringBuilder("");
            toDate.append(selectedDay).append("/").append(selectedMonth).append("/").append(selectedYear);
            tvToDate.setText(CalenderUtils.formatDate(String.valueOf(toDate), AppConstants.TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT));
        }
    };

    public void setOnAddListener(RainDataCallback callback) {
        this.callback = callback;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_add_rain, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        etScale = view.findViewById(R.id.etScale);
        tvFromDate = view.findViewById(R.id.tvFromDate);
        tvToDate = view.findViewById(R.id.tvToDate);
        tvMM = view.findViewById(R.id.tvMM);
        tvInch = view.findViewById(R.id.tvInch);

        onClickListener(tvFromDate, tvToDate, tvMM, tvInch, view.findViewById(R.id.btnAdd), view.findViewById(R.id.btnCancel));
    }

    private void onClickListener(View... views){
        for (View view: views){
            view.setOnClickListener(this);
        }
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAdd:
                String scale = etScale.getText().toString().trim();
                String fromDate = tvFromDate.getText().toString().trim();
                String toDate = tvToDate.getText().toString().trim();
                if (verifyInput(scale, fromDate)) {
                    RainBeanResponse.RainRecordListBean bean = new RainBeanResponse.RainRecordListBean();
                    bean.setRaindataId("a" + CalenderUtils.getTimestamp());
                    bean.setFromDate(fromDate);
                    bean.setToDate(toDate);
                    bean.setQuantity(scale);
                    bean.setQuantityUnite(scaleType);

                    callback.onRainDataAdd(bean);
                    dismissDialog(TAG);
                }
                break;

            case R.id.btnCancel:
                dismissDialog(TAG);
                break;

            case R.id.tvFromDate:
                isFromSelect = true;
                getDate(tvFromDate.getText().toString().trim());
                break;

            case R.id.tvToDate:
                isFromSelect = false;
                getDate(tvToDate.getText().toString().trim());
                break;

            case R.id.tvMM:
                scaleType = "mm";
                tvMM.setTextColor(getResources().getColor(R.color.colorWhite));
                tvInch.setTextColor(getResources().getColor(R.color.grey));
                tvMM.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                tvInch.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                break;

            case R.id.tvInch:
                scaleType = "inch";
                tvInch.setTextColor(getResources().getColor(R.color.colorWhite));
                tvMM.setTextColor(getResources().getColor(R.color.grey));
                tvInch.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                tvMM.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                break;
        }
    }

    private boolean verifyInput(String scale, String fromDate) {
        if (scale.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_scale), Toast.LENGTH_SHORT);
            return false;
        } else if (scale.startsWith(".")) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_valid_scale), Toast.LENGTH_SHORT);
            return false;
        } else if (Double.parseDouble(scale) <= 0) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_scale_zero), Toast.LENGTH_SHORT);
            return false;
        } else if (fromDate.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_select_fdate), Toast.LENGTH_SHORT);
            return false;
        } else return true;
    }

    private void getDate(String selectedDate) {
        //date 26/06/2018

        Calendar newCalender = Calendar.getInstance();

        int day, month, year;

        if (selectedDate.isEmpty()) {
            day = newCalender.get(Calendar.DAY_OF_MONTH);
            month = newCalender.get(Calendar.MONTH);
            year = newCalender.get(Calendar.YEAR);
        } else {
            String[] date = selectedDate.split("/");

            day = Integer.parseInt(date[0]);
            month = Integer.parseInt(date[1]) - 1;
            year = Integer.parseInt(date[2]);
        }

        DatePickerDialog datePickerDialog = new DatePickerDialog(getBaseActivity(), datePicker, year, month, day);
        Date maxDate = CalenderUtils.getDateFormat(CalenderUtils.getCurrentDate(), AppConstants.TIMESTAMP_FORMAT);
        assert maxDate != null;
        datePickerDialog.getDatePicker().setMaxDate(maxDate.getTime());

        if (!isFromSelect && !tvFromDate.getText().toString().isEmpty() && tvToDate.getText().toString().isEmpty()) {
            Date cDate = CalenderUtils.getDateFormat(tvFromDate.getText().toString(), AppConstants.TIMESTAMP_FORMAT);
            assert cDate != null;
            datePickerDialog.getDatePicker().setMinDate(cDate.getTime());
        } else if (isFromSelect && tvFromDate.getText().toString().isEmpty() && !tvToDate.getText().toString().isEmpty()) {
            Date cDate = CalenderUtils.getDateFormat(tvToDate.getText().toString(), AppConstants.TIMESTAMP_FORMAT);
            assert cDate != null;
            datePickerDialog.getDatePicker().setMaxDate(cDate.getTime());
        } else if (isFromSelect && !tvFromDate.getText().toString().isEmpty() && !tvToDate.getText().toString().isEmpty()) {
            Date cDateMax = CalenderUtils.getDateFormat(tvToDate.getText().toString(), AppConstants.TIMESTAMP_FORMAT);
            assert cDateMax != null;
            datePickerDialog.getDatePicker().setMaxDate(cDateMax.getTime());
        } else if (!isFromSelect && !tvFromDate.getText().toString().isEmpty() && !tvToDate.getText().toString().isEmpty()) {
            Date cDateMin = CalenderUtils.getDateFormat(tvFromDate.getText().toString(), AppConstants.TIMESTAMP_FORMAT);
            assert cDateMin != null;
            datePickerDialog.getDatePicker().setMinDate(cDateMin.getTime());
        }
        datePickerDialog.show();
    }

}
