package com.agrinvestapp.ui.property.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.RainBeanResponse;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.CalenderUtils;
import com.agrinvestapp.utils.pagination.FooterLoader;
import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chauthai.swipereveallayout.ViewBinderHelper;

import java.util.List;

/**
 * Created by hemant
 * Date: 17/4/18
 * Time: 4:03 PM
 */

public class RainDataAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEWTYPE_ITEM = 1;
    private final int VIEWTYPE_LOADER = 2;
    // This object helps you save/restore the open/close state of each view
    private final ViewBinderHelper viewBinderHelper = new ViewBinderHelper();
    private List<RainBeanResponse.RainRecordListBean> list;
    private boolean showLoader;
    private ItemClickListener itemClickListener;

    public RainDataAdapter(List<RainBeanResponse.RainRecordListBean> list) {
        this.list = list;
    }

    public RainDataAdapter(List<RainBeanResponse.RainRecordListBean> list, ItemClickListener itemClickListener) {
        this.list = list;
        this.itemClickListener = itemClickListener;
        viewBinderHelper.setOpenOnlyOne(true);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder rvHolder, int position) {
        if (rvHolder instanceof FooterLoader) {
            FooterLoader loaderViewHolder = (FooterLoader) rvHolder;
            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }
            return;
        }
        RainDataAdapter.ViewHolder holder = ((RainDataAdapter.ViewHolder) rvHolder);

        RainBeanResponse.RainRecordListBean bean = list.get(position);

        viewBinderHelper.bind(holder.swipe, bean.getRaindataId());
        if (!list.get(position).isSelected) holder.swipe.close(true);

        StringBuilder builder = new StringBuilder("");

        if (itemClickListener == null) {
            holder.swipe.setLockDrag(true);

            if (bean.getToDate().contains("0000-00-00"))
                builder.append(CalenderUtils.formatDate(bean.getFromDate(), AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT));
            else
                builder.append(CalenderUtils.formatDate(bean.getFromDate(), AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT))
                        .append(" ").append(holder.tvDate.getContext().getString(R.string.to)).append(" ")
                        .append(CalenderUtils.formatDate(bean.getToDate(), AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT));
        } else {
            if (bean.getToDate().isEmpty() || bean.getToDate().contains("0000-00-00"))
                if (bean.getFromDate().contains("-"))
                    builder.append(CalenderUtils.formatDate(bean.getFromDate(), AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT));
                else builder.append(bean.getFromDate());
            else if (bean.getFromDate().contains("-")) {
                builder.append(CalenderUtils.formatDate(bean.getFromDate(), AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT))
                        .append(" ").append(holder.tvDate.getContext().getString(R.string.to)).append(" ")
                        .append(CalenderUtils.formatDate(bean.getToDate(), AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT));
            } else {
                builder.append(bean.getFromDate()).append(" ").append(holder.tvDate.getContext().getString(R.string.to)).append(" ").append(bean.getToDate());
            }
        }

        holder.tvDate.setText(builder);

        holder.tvScale.setText(bean.getQuantity());
        holder.tvScaleType.setText(bean.getQuantityUnite().equals("mm") ? holder.tvDate.getContext().getString(R.string.mm) : holder.tvDate.getContext().getString(R.string.inch));
    }

    public void showLoading(boolean status) {
        showLoader = status;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case VIEWTYPE_ITEM:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rain_data, parent, false);
                return new ViewHolder(view);

            case VIEWTYPE_LOADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pagination_item_loader, parent, false);
                return new FooterLoader(view);
        }
        return null;

    }

    @Override
    public int getItemViewType(int position) {
        if (position == list.size() - 1) {
            return showLoader ? VIEWTYPE_LOADER : VIEWTYPE_ITEM;
        }
        return VIEWTYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface ItemClickListener {
        void onItemClick(int pos);
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvDate, tvScale, tvScaleType;
        private ImageView trash;
        private SwipeRevealLayout swipe;

        ViewHolder(@NonNull View v) {
            super(v);
            tvDate = v.findViewById(R.id.tvDate);
            tvScale = v.findViewById(R.id.tvScale);
            tvScaleType = v.findViewById(R.id.tvScaleType);
            trash = v.findViewById(R.id.trash);
            swipe = v.findViewById(R.id.swipe);

            trash.setOnClickListener(this);
        }

        @Override
        public void onClick(@NonNull final View view) {
            if (getAdapterPosition() != -1) {
                swipe.close(true);
                itemClickListener.onItemClick(getAdapterPosition());
            }
        }
    }

}

