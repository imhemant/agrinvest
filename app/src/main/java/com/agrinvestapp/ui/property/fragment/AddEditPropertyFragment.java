package com.agrinvestapp.ui.property.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.Agrinvest;
import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.EditPropertyDetailResponse;
import com.agrinvestapp.data.model.api.PropertyInfoResponse;
import com.agrinvestapp.data.model.api.RainBeanResponse;
import com.agrinvestapp.data.model.db.Property;
import com.agrinvestapp.data.model.db.RainData;
import com.agrinvestapp.ui.base.BaseFragment;
import com.agrinvestapp.ui.base.dialog.DeleteDialog;
import com.agrinvestapp.ui.property.adapter.RainDataAdapter;
import com.agrinvestapp.ui.property.dialog.RainDataDialog;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.AppLogger;
import com.agrinvestapp.utils.CommonUtils;
import com.agrinvestapp.utils.Permission;
import com.agrinvestapp.volley.VolleyJsonObjectListener;
import com.agrinvestapp.volley.VolleyRequest;
import com.agrinvestapp.volley.VolleyResponseListener;
import com.android.volley.VolleyError;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class AddEditPropertyFragment extends BaseFragment implements OnMapReadyCallback, View.OnClickListener{

    private static final String TAG = AddEditPropertyFragment.class.getName();

    private PropertyInfoFragment propertyInfoFragment;
    private Boolean isAdd = true;

    private MapView mapView;
    private GoogleMap googleMap;

    private Bundle mapViewBundle;

    private Double latitude = 0.0, longitude = 0.0;
    private LatLng latLng;
    private Boolean isPlaceApiAddress = false;

    private TextView tvNoRecord, tvAddress;
    private NestedScrollView scrollView;

    private List<RainBeanResponse.RainRecordListBean> rainList;
    private RainDataAdapter rainDataAdapter;
    private PropertyInfoResponse.PropertyInfoBean propertyInfoBean;

    private TextView tvAcre, tvHact, tvDesCount;
    private EditText etName, etDescription, etSize;
    private FrameLayout mapFrame;

    private Boolean isSatelliteMode = false;

    private String scaleType = "acres";

    //Edit fields
    private Integer count = 0;
    private EditPropertyDetailResponse editPropertyDetailResponse;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(AppConstants.MAP_VIEW_BUNDLE_KEY);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_edit_property, container, false);
        initView(view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // when the map is ready to be used.
        mapView = view.findViewById(R.id.map);
        assert mapView != null;
        mapView.onCreate(mapViewBundle);
        mapView.onResume();
        mapView.getMapAsync(this);

        if (!isAdd && propertyInfoBean != null) {
            if (isNetworkConnected(false)) getPropertyDetails();
            else
                CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_internet), Toast.LENGTH_SHORT);
        }
    }

    private void initView(View view) {
        latitude = Agrinvest.LATITUDE;
        longitude = Agrinvest.LONGITUDE;

        /* toolbar view start */
        ImageView imgBack = view.findViewById(R.id.imgBack);
        imgBack.setVisibility(View.VISIBLE);
        TextView tvTitle = view.findViewById(R.id.tvTitle);

        TextView tvAdd = view.findViewById(R.id.tvAdd);
        tvAdd.setVisibility(View.VISIBLE);
        if (isAdd) {
            tvTitle.setText(R.string.add_property_info);
            tvAdd.setText(getString(R.string.add));
        } else {
            tvTitle.setText(R.string.edit_property_info);
            tvAdd.setText(getString(R.string.update));
        }

        /* toolbar view end */

        rainList = new ArrayList<>();
        RecyclerView rvRainData = view.findViewById(R.id.rvRainData);
        rainDataAdapter = new RainDataAdapter(rainList, pos -> DeleteDialog.newInstance(false, "", () -> {
            if (rainList.get(pos).getRaindataId().contains("a")) {
                rainList.remove(pos);
                rainDataAdapter.notifyItemRemoved(pos);
                CommonUtils.showToast(getBaseActivity(), getString(R.string.rainRecordDelSuccess), Toast.LENGTH_SHORT);
            } else {
                if (isNetworkConnected(true)) doDeleteRainData(pos);
                else
                    CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_internet), Toast.LENGTH_SHORT);
            }
        }).show(getChildFragmentManager()));
        rvRainData.setAdapter(rainDataAdapter);

        tvNoRecord = view.findViewById(R.id.tvNoRecord);
        scrollView = view.findViewById(R.id.scrollView);

        mapFrame = view.findViewById(R.id.mapFrame);
        tvAcre = view.findViewById(R.id.tvAcre);
        tvHact = view.findViewById(R.id.tvHact);
        tvAddress = view.findViewById(R.id.tvAddress);

        etName = view.findViewById(R.id.etName);
        tvDesCount = view.findViewById(R.id.tvDesCount);
        etDescription = view.findViewById(R.id.etDescription);
        etSize = view.findViewById(R.id.etSize);

        onClickListener(imgBack, tvAdd, mapFrame, tvAcre, tvHact, tvAddress, view.findViewById(R.id.imgAdd));

        etDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                StringBuilder builder = new StringBuilder("");
                builder.append(s.length()).append("/").append("400");
                tvDesCount.setText(builder);
            }
        });
    }

    private void onClickListener(View... views){
        for (View view: views){
            view.setOnClickListener(this);
        }
    }

    protected void setInstance(PropertyInfoFragment propertyInfoFragment, Boolean isAdd) {
        this.propertyInfoFragment = propertyInfoFragment;
        this.isAdd = isAdd;
    }

    protected void setPropertyModel(PropertyInfoResponse.PropertyInfoBean propertyInfoBean) {
        this.propertyInfoBean = propertyInfoBean;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                getBaseActivity().onBackPressed();
                break;

            case R.id.mapFrame:
                ImageView img = (ImageView) mapFrame.getChildAt(0);
                if (isSatelliteMode) {
                    isSatelliteMode = false;

                    mapFrame.setBackground(getResources().getDrawable(R.drawable.circle_green));
                    img.setColorFilter(ContextCompat.getColor(getBaseActivity(), R.color.colorWhite), PorterDuff.Mode.SRC_IN);

                    //for normal view of map:
                    googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    updateMapUI(false);
                } else {
                    isSatelliteMode = true;

                    mapFrame.setBackground(getResources().getDrawable(R.drawable.circle_white));
                    img.setColorFilter(ContextCompat.getColor(getBaseActivity(), R.color.colorPrimaryDark), PorterDuff.Mode.SRC_IN);

                    //for satellite view of map:
                    googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                    updateMapUI(false);
                }
                break;

            case R.id.imgAdd:
                RainDataDialog rainDataDialog = new RainDataDialog();
                rainDataDialog.setOnAddListener(bean -> {
                    tvNoRecord.setVisibility(View.GONE);
                    rainList.add(0, bean);
                    rainDataAdapter.notifyDataSetChanged();
                    scrollView.fullScroll(View.FOCUS_DOWN);
                });
                rainDataDialog.show(getChildFragmentManager());
                break;

            case R.id.tvAcre:
                scaleType = "acres";
                tvAcre.setTextColor(getResources().getColor(R.color.colorWhite));
                tvHact.setTextColor(getResources().getColor(R.color.grey));
                tvAcre.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                tvHact.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                break;

            case R.id.tvHact:
                scaleType = "hectares";
                tvHact.setTextColor(getResources().getColor(R.color.colorWhite));
                tvAcre.setTextColor(getResources().getColor(R.color.grey));
                tvHact.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                tvAcre.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                break;

            case R.id.tvAdd:
                if (isNetworkConnected(true)) {
                    if (isAdd) doAddProperty();
                    else if (propertyInfoBean != null) updateProperty();
                } else
                    CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_internet), Toast.LENGTH_SHORT);
                break;

            case R.id.tvAddress:
                tvAddress.setClickable(false);
                try {
                    Intent intent =
                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                                    .build(getBaseActivity());
                    startActivityForResult(intent, AppConstants.PLACE_AUTOCOMPLETE_REQUEST_CODE);
                } catch (GooglePlayServicesRepairableException e) {
                    CommonUtils.showToast(getActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                } catch (GooglePlayServicesNotAvailableException e) {
                    CommonUtils.showToast(getActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                }
                new Handler().postDelayed(() -> tvAddress.setClickable(true), 2000);
                break;
        }
    }

    private synchronized void updateMapUI(Boolean isPlaceAddress) {
        latLng = new LatLng(latitude, longitude);
        isPlaceApiAddress = isPlaceAddress;
        if (latitude != 0.0) {
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(16));
        }
    }

    private void doAddProperty() {
        String addressTemp = tvAddress.getText().toString().trim();
        String propTemp = etName.getText().toString().trim();
        String desTemp = etDescription.getText().toString().trim();
        String sizeTemp = etSize.getText().toString().trim();
        if (verifyInputs(addressTemp, propTemp, sizeTemp)) {
            setLoading(true);

            HashMap<String, String> mParameterMap = new HashMap<>();
            mParameterMap.put("propertyName", propTemp);
            mParameterMap.put("location", addressTemp);
            mParameterMap.put("longitude", (longitude == 0.0) ? "" : String.valueOf(longitude));
            mParameterMap.put("latitude", (latitude == 0.0) ? "" : String.valueOf(latitude));
            mParameterMap.put("size", sizeTemp);
            mParameterMap.put("sizeUnite", scaleType);
            mParameterMap.put("description", desTemp);
            mParameterMap.put("rainData", rainList.isEmpty() ? "" : getDataManager().mGson.toJson(rainList));

            getDataManager().doServerAddPropertyApiCall(getDataManager().getHeader(), mParameterMap)
                    .getAsJsonObject(new VolleyJsonObjectListener() {
                        @Override
                        public void onVolleyJsonObject(JSONObject jsonObject) {
                            setLoading(false);

                            try {
                                String status = jsonObject.getString("status");
                                String message = jsonObject.getString("message");

                                if (status.equals("success")) {
                                    //Todo manage local db add Done
                                    doAddPropertyToLocalDB(jsonObject);

                                    new Handler().postDelayed(() -> {
                                        CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                                        getBaseActivity().onBackPressed();
                                        if (propertyInfoFragment != null) propertyInfoFragment.hitApi();
                                    },200);
                                } else {
                                    CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                                }
                            } catch (Exception e) {
                                CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                            }
                        }

                        @Override
                        public void onVolleyJsonException() {
                            setLoading(false);
                            CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }

                        @Override
                        public void onVolleyError(VolleyError error) {
                            setLoading(false);
                            VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                        }
                    });
        }
    }

    private boolean verifyInputs(String address, String propTemp, String sizeTemp) {
        if (address.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_address), Toast.LENGTH_SHORT);
            return false;
        } else if (propTemp.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_property_name), Toast.LENGTH_SHORT);
            return false;
        } else if (sizeTemp.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_size), Toast.LENGTH_SHORT);
            return false;
        } else if (sizeTemp.startsWith(".")) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_valid_size), Toast.LENGTH_SHORT);
            return false;
        } else if (Double.parseDouble(sizeTemp) <= 0) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_size_zero), Toast.LENGTH_SHORT);
            return false;
        }/* else if (!isAdd && isEditedData(address, propTemp, sizeTemp)) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_update_data), Toast.LENGTH_SHORT);
            return false;
        } */ else return true;
    }

    private boolean isEditedData(String address, String propTemp, String sizeTemp) {
        if (!address.equals(editPropertyDetailResponse.getPropertyData().getLocation())) {
            return false;
        } else if (!propTemp.equals(editPropertyDetailResponse.getPropertyData().getPropertyName())) {
            return false;
        } else if (!sizeTemp.equals(editPropertyDetailResponse.getPropertyData().getSize())) {
            return false;
        } else if (!etDescription.getText().toString().trim().equals(editPropertyDetailResponse.getPropertyData().getDescription())) {
            return false;
        } else if (!scaleType.equals(editPropertyDetailResponse.getPropertyData().getSizeUnite())) {
            return false;
        } else return rainList.equals(editPropertyDetailResponse.getPropertyRainData());

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle mapViewBundle = outState.getBundle(AppConstants.MAP_VIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(AppConstants.MAP_VIEW_BUNDLE_KEY, mapViewBundle);
        }

        mapView.onSaveInstanceState(mapViewBundle);
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    private void handlePermissionTask() {
        new Thread(() -> {
            final boolean isPermissionAllow = Permission.checkLocationPermission(getBaseActivity());

            getBaseActivity().runOnUiThread(() -> {
                if (isPermissionAllow && Agrinvest.LATITUDE != 0.0) {
                    getBaseActivity().updateLocation();
                }
            });
        }).start();
    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        this.googleMap.getUiSettings().setCompassEnabled(false);
        this.googleMap.getUiSettings().setMapToolbarEnabled(false);
        this.googleMap.getUiSettings().setScrollGesturesEnabled(true);

        this.googleMap.setOnCameraIdleListener(() -> {
            latLng = googleMap.getCameraPosition().target;

            if (isAdd) {
                latitude = latLng.latitude;
                longitude = latLng.longitude;

                if (latitude != 0.0 && longitude != 0.0) {
                    if (!isPlaceApiAddress) {
                        tvAddress.setText(getAddressFromLatLng(latitude, longitude));
                    } else isPlaceApiAddress = false;
                }
            } else {
                if (count > 1) {
                    latitude = latLng.latitude;
                    longitude = latLng.longitude;

                    if (latitude != 0.0 && longitude != 0.0) {
                        if (!isPlaceApiAddress) {
                            tvAddress.setText(getAddressFromLatLng(latitude, longitude));
                        } else isPlaceApiAddress = false;
                    }
                }
                count++;
            }
        });

        handlePermissionTask();
        if (ActivityCompat.checkSelfPermission(getBaseActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getBaseActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            setupMap();
            return;
        }
        this.googleMap.setMyLocationEnabled(true);

        setupMap();
    }

    private void setupMap() {
        if (Agrinvest.LATITUDE != 0.0) {
            updateMapUI(false);
        } else {
            getBaseActivity().updateLocation();
        }
    }

    /*Update property start here */
    private void getPropertyDetails() {
        setLoading(true);

        HashMap<String, String> mParameterMap = new HashMap<>();
        mParameterMap.put("propertyId", propertyInfoBean.getPropertyId());

        getDataManager().doServerEditPropertyDetailApiCall(getDataManager().getHeader(), mParameterMap)
                .getAsResponse(new VolleyResponseListener() {
                    @Override
                    public void onVolleyResponse(String response) {
                        setLoading(false);
                        editPropertyDetailResponse = getDataManager().mGson.fromJson(response, EditPropertyDetailResponse.class);

                        if (editPropertyDetailResponse.getStatus().equals("success")) {
                            updatePropertyUi(editPropertyDetailResponse);
                        } else {
                            CommonUtils.showToast(getBaseActivity(), editPropertyDetailResponse.getMessage(), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    private void updatePropertyUi(EditPropertyDetailResponse editPropertyDetailResponse) {
        tvAddress.setText(editPropertyDetailResponse.getPropertyData().getLocation());
        etName.setText(editPropertyDetailResponse.getPropertyData().getPropertyName());
        etDescription.setText(editPropertyDetailResponse.getPropertyData().getDescription());
        etSize.setText(editPropertyDetailResponse.getPropertyData().getSize());

        latitude = (editPropertyDetailResponse.getPropertyData().getLatitude().isEmpty()) ? 0.0 : Double.parseDouble(editPropertyDetailResponse.getPropertyData().getLatitude());
        longitude = (editPropertyDetailResponse.getPropertyData().getLongitude().isEmpty()) ? 0.0 : Double.parseDouble(editPropertyDetailResponse.getPropertyData().getLongitude());

        if (editPropertyDetailResponse.getPropertyData().getSizeUnite().contains("acres"))
            tvAcre.callOnClick();
        else tvHact.callOnClick();

        rainList.clear();
        rainList.addAll(editPropertyDetailResponse.getPropertyRainData());
        tvNoRecord.setVisibility(rainList.isEmpty() ? View.VISIBLE : View.GONE);
        rainDataAdapter.notifyDataSetChanged();
        updateMapUI(false);
    }

    private void updateProperty() {

        String addressTemp = tvAddress.getText().toString().trim();
        String propTemp = etName.getText().toString().trim();
        String desTemp = etDescription.getText().toString().trim();
        String sizeTemp = etSize.getText().toString().trim();
        if (verifyInputs(addressTemp, propTemp, sizeTemp)) {
            setLoading(true);

            String rainObject = getDataManager().mGson.toJson(rainList);
            HashMap<String, String> mParameterMap = new HashMap<>();
            mParameterMap.put("propertyId", propertyInfoBean.getPropertyId());
            mParameterMap.put("propertyName", propTemp);
            mParameterMap.put("location", addressTemp);
            mParameterMap.put("longitude", (longitude == 0.0) ? "" : String.valueOf(longitude));
            mParameterMap.put("latitude", (latitude == 0.0) ? "" : String.valueOf(latitude));
            mParameterMap.put("size", sizeTemp);
            mParameterMap.put("sizeUnite", scaleType);
            mParameterMap.put("description", desTemp);
            mParameterMap.put("rainData", rainList.isEmpty() ? "" : rainObject);

            getDataManager().doServerUpdatePropertyDetailApiCall(getDataManager().getHeader(), mParameterMap)
                    .getAsJsonObject(new VolleyJsonObjectListener() {

                        @Override
                        public void onVolleyJsonObject(JSONObject jsonObject) {
                            setLoading(false);

                            try {
                                String status = jsonObject.getString("status");
                                String message = jsonObject.getString("message");

                                if (status.equals("success")) {
                                    //Todo manage local db update Done
                                    doUpdatePropertyToLocalDB(jsonObject);
                                    CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                                    getBaseActivity().onBackPressed();
                                    if (propertyInfoFragment != null) propertyInfoFragment.hitApi();
                                } else {
                                    CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                                }
                            } catch (Exception e) {
                                CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                            }
                        }

                        @Override
                        public void onVolleyJsonException() {
                            setLoading(false);
                            CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }

                        @Override
                        public void onVolleyError(VolleyError error) {
                            setLoading(false);
                            VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                        }
                    });
        }
    }

    private void doDeleteRainData(int pos) {
        setLoading(true);

        HashMap<String, String> mParameterMap = new HashMap<>();
        mParameterMap.put("raindataId", rainList.get(pos).getRaindataId());

        getDataManager().doServerDeleteRainRecordApiCall(getDataManager().getHeader(), mParameterMap)
                .getAsJsonObject(new VolleyJsonObjectListener() {


                    @Override
                    public void onVolleyJsonObject(JSONObject jsonObject) {
                        setLoading(false);

                        try {
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            if (status.equals("success")) {
                                CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                                //Todo delete rain from local db Done
                                doDeleteRainDataFromLocalDB(rainList.get(pos).getRaindataId());
                                rainList.remove(pos);
                                rainDataAdapter.notifyItemRemoved(pos);

                                tvNoRecord.setVisibility(rainList.isEmpty() ? View.VISIBLE : View.GONE);

                            } else {
                                CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                            }
                        } catch (Exception e) {
                            CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyJsonException() {
                        setLoading(false);
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }
    /*Update property end here */

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode){
            case AppConstants.PLACE_AUTOCOMPLETE_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Place place = PlaceAutocomplete.getPlace(getBaseActivity(), data);

                    latitude = place.getLatLng().latitude;
                    longitude = place.getLatLng().longitude;
                    tvAddress.setText(place.getAddress());
                    updateMapUI(true);
                    AppLogger.e(TAG, "Place details received: " + place.getName());
                } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                    //Status status = PlaceAutocomplete.getStatus(getBaseActivity(), data);
                    CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                } else if (resultCode == RESULT_CANCELED) {
                    // The user canceled the operation.
                    AppLogger.i(TAG, "Place: " + "Result Cancel");
                }
                break;

            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }

    }

    /*Local DB Start here*/
    private void doAddPropertyToLocalDB(JSONObject jsonObject) {
        new Thread(() -> {
            try {
                JSONObject propertyJson = jsonObject.getJSONObject("propertyData");

                Property propertyBean = new Property();
                propertyBean.setPropertyId(propertyJson.getString("propertyId"));
                propertyBean.setPropertyName(propertyJson.getString("propertyName"));
                propertyBean.setUser_id(getDataManager().getUserInfo().getUserId());
                propertyBean.setLocation(propertyJson.getString("location"));
                propertyBean.setLongitude(propertyJson.getString("longitude"));
                propertyBean.setLatitude(propertyJson.getString("latitude"));
                propertyBean.setSize(propertyJson.getString("size"));
                propertyBean.setSizeUnite(propertyJson.getString("sizeUnite"));
                propertyBean.setDescription(propertyJson.getString("description"));
                propertyBean.setCrd(propertyJson.getString("crd"));
                propertyBean.setUpd(propertyJson.getString("upd"));
                propertyBean.setEvent(AppConstants.DB_EVENT_ADD);
                propertyBean.setDataSync(AppConstants.DB_SYNC_TRUE);

                getDataManager().insertProperty(propertyBean);

                JSONArray rainArray = jsonObject.getJSONArray("propertyRainData");

                List<RainData> rainDataList = new ArrayList<>();
                for (int i = 0; i < rainArray.length(); i++) {
                    JSONObject obj = rainArray.getJSONObject(i);
                    RainData rainBean = new RainData();
                    rainBean.setRaindataId(obj.getString("raindataId"));
                    rainBean.setProperty_id(obj.getString("property_id"));
                    rainBean.setFromDate(obj.getString("fromDate"));
                    rainBean.setToDate(obj.getString("toDate"));
                    rainBean.setQuantity(obj.getString("quantity"));
                    rainBean.setQuantityUnite(obj.getString("quantityUnite"));
                    rainBean.setCrd(obj.getString("crd"));
                    rainBean.setUpd(obj.getString("upd"));
                    rainBean.setEvent(AppConstants.DB_EVENT_ADD);
                    rainBean.setDataSync(AppConstants.DB_SYNC_TRUE);
                    rainDataList.add(rainBean);
                }
                getDataManager().insertAllRainData(rainDataList);
                AppLogger.d("database", "Db Add property done");

            } catch (Exception r) {
                AppLogger.d("database", " add property and rain data fail " + r.getMessage());
            }
        }).start();
    }

    private void doDeleteRainDataFromLocalDB(String rainId) {
        new Thread(() -> {
            getDataManager().deleteRainData(rainId);
            AppLogger.d("database", "rain data delete success");
        }).start();
    }

    private void doUpdatePropertyToLocalDB(JSONObject jsonObject) {
        new Thread(() -> {
            try {
                JSONObject propertyJson = jsonObject.getJSONObject("propertyData");

                Property propertyBean = new Property();
                propertyBean.setPropertyId(propertyJson.getString("propertyId"));
                propertyBean.setPropertyName(propertyJson.getString("propertyName"));
                propertyBean.setUser_id(getDataManager().getUserInfo().getUserId());
                propertyBean.setLocation(propertyJson.getString("location"));
                propertyBean.setLongitude(propertyJson.getString("longitude"));
                propertyBean.setLatitude(propertyJson.getString("latitude"));
                propertyBean.setSize(propertyJson.getString("size"));
                propertyBean.setSizeUnite(propertyJson.getString("sizeUnite"));
                propertyBean.setDescription(propertyJson.getString("description"));
                propertyBean.setCrd(propertyJson.getString("crd"));
                propertyBean.setUpd(propertyJson.getString("upd"));
                propertyBean.setEvent(AppConstants.DB_EVENT_EDIT);
                propertyBean.setDataSync(AppConstants.DB_SYNC_TRUE);

                getDataManager().updateProperty(propertyBean);
                getDataManager().deleteRainDataUsingPropertyId(propertyBean.getPropertyId());

                JSONArray rainArray = jsonObject.getJSONArray("propertyRainData");

                List<RainData> rainDataList = new ArrayList<>();
                for (int i = 0; i < rainArray.length(); i++) {
                    JSONObject obj = rainArray.getJSONObject(i);
                    RainData rainBean = new RainData();
                    rainBean.setRaindataId(obj.getString("raindataId"));
                    rainBean.setProperty_id(obj.getString("property_id"));
                    rainBean.setFromDate(obj.getString("fromDate"));
                    rainBean.setToDate(obj.getString("toDate"));
                    rainBean.setQuantity(obj.getString("quantity"));
                    rainBean.setQuantityUnite(obj.getString("quantityUnite"));
                    rainBean.setCrd(obj.getString("crd"));
                    rainBean.setUpd(obj.getString("upd"));
                    rainBean.setEvent(AppConstants.DB_EVENT_EDIT);
                    rainBean.setDataSync(AppConstants.DB_SYNC_TRUE);
                    rainDataList.add(rainBean);
                }
                getDataManager().insertAllRainData(rainDataList);
                AppLogger.d("database insert", " property and rain data update success");
            } catch (Exception e) {
                AppLogger.d("database error", " update property and rain data fail " + e.getMessage());
            }
        }).start();
    }
    /*Local DB End here*/

}
