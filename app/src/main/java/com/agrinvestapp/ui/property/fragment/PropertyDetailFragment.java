package com.agrinvestapp.ui.property.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.PropertyDetailResponse;
import com.agrinvestapp.data.model.api.PropertyInfoResponse;
import com.agrinvestapp.data.model.db.Parcel;
import com.agrinvestapp.data.model.other.CommonIdBean;
import com.agrinvestapp.ui.animal.AnimalActivity;
import com.agrinvestapp.ui.base.BaseFragment;
import com.agrinvestapp.ui.base.dialog.DeleteDialog;
import com.agrinvestapp.ui.crop.CropActivity;
import com.agrinvestapp.ui.property.adapter.ParcelAdapter;
import com.agrinvestapp.ui.property.dialog.ParcelCallback;
import com.agrinvestapp.ui.property.dialog.ParcelDialog;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.AppLogger;
import com.agrinvestapp.utils.CalenderUtils;
import com.agrinvestapp.utils.CommonUtils;
import com.agrinvestapp.volley.VolleyJsonObjectListener;
import com.agrinvestapp.volley.VolleyRequest;
import com.agrinvestapp.volley.VolleyResponseListener;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PropertyDetailFragment extends BaseFragment implements View.OnClickListener, BaseFragment.FragmentCallback, ParcelCallback {

    private LinearLayout llMain;
    private TextView tvPropertyTitle, tvAddress, tvAnimalCount, tvCropCount, tvRainCount, tvSize, tvDescription, tvNoRecord;
    private ParcelAdapter parcelAdapter;
    private List<PropertyDetailResponse.PropertyDetailsBean.ParcelBean> parcelList;
    private PropertyInfoResponse.PropertyInfoBean propertyInfoBean;

    //main thread
    private Handler handler = new Handler(Looper.getMainLooper());

    private PropertyInfoFragment propertyInfoFragment;

    protected void setInstance(PropertyInfoFragment propertyInfoFragment, PropertyInfoResponse.PropertyInfoBean propertyInfoBean) {
        this.propertyInfoFragment = propertyInfoFragment;
        this.propertyInfoBean = propertyInfoBean;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_property_detail, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        /* toolbar view start */
        ImageView imgBack = view.findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);
        imgBack.setVisibility(View.VISIBLE);

        TextView tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.property_details);
        /* toolbar view end */

        llMain = view.findViewById(R.id.llMain);
        llMain.setVisibility(View.GONE);
        llMain.setOnClickListener(this);  //avoid bg click

        tvPropertyTitle = view.findViewById(R.id.tvPropertyTitle);
        tvAddress = view.findViewById(R.id.tvAddress);
        tvAnimalCount = view.findViewById(R.id.tvAnimalCount);
        tvCropCount = view.findViewById(R.id.tvCropCount);
        tvRainCount = view.findViewById(R.id.tvRainCount);
        tvSize = view.findViewById(R.id.tvSize);
        tvDescription = view.findViewById(R.id.tvDescription);
        tvNoRecord = view.findViewById(R.id.tvNoRecord);

        view.findViewById(R.id.imgAddFile).setOnClickListener(this);
        view.findViewById(R.id.llAnimalRecord).setOnClickListener(this);
        view.findViewById(R.id.llCropRecord).setOnClickListener(this);
        view.findViewById(R.id.llRainRecord).setOnClickListener(this);

        RecyclerView rvParcels = view.findViewById(R.id.rvParcels);
        parcelList = new ArrayList<>();
        parcelAdapter = new ParcelAdapter(parcelList, new ParcelAdapter.ItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                getBaseActivity().addFragment(ParcelDetailFragment.newInstance(parcelList.get(pos)), R.id.propertyFrame, true);
            }

            @Override
            public void onEditClick(int pos) {
                ParcelDialog parcelDialog = new ParcelDialog();
                parcelDialog.setOnParcelAddEditListener(pos, propertyInfoBean.getPropertyId(), parcelList.get(pos).getParcelName(), PropertyDetailFragment.this);
                parcelDialog.show(getChildFragmentManager());
            }

            @Override
            public void onDeleteClick(int pos) {
                setLoading(true);

                new Thread(() -> {
                    String msg = getExpenseStatus(pos);

                    handler.post(() -> {
                        setLoading(false);

                        DeleteDialog.newInstance(msg.equals(getString(R.string.deleteExpenseMsg)), msg, () -> {
                            if (isNetworkConnected(true)) {
                                getBaseActivity().doSyncAppDB(false, flag -> {
                                    switch (flag) {
                                        case AppConstants.ONLINE:
                                            doDeleteParcelData(pos);
                                            break;

                                        case AppConstants.OFFLINE:
                                            doAddEditDelParcelToLocalDB("del", pos, parcelList.get(pos).getParcelId(), parcelList.get(pos).getParcelName());
                                            break;
                                    }
                                });
                            } else
                                doAddEditDelParcelToLocalDB("del", pos, parcelList.get(pos).getParcelId(), parcelList.get(pos).getParcelName());
                        }).show(getChildFragmentManager());
                    });
                }).start();
            }
        });
        rvParcels.setAdapter(parcelAdapter);
    }

    private String getExpenseStatus(int pos) {
        String msg = getString(R.string.are_you_sure_you_want_to_delete_parcel);
        String parcelExpenseStatus = getDataManager().getExpenseApplyStatus(parcelList.get(pos).getParcelId(), parcelList.get(pos).getRecordKey());

        if (parcelExpenseStatus == null) {
            List<CommonIdBean> animals = getDataManager().getAnimals(parcelList.get(pos).getParcelId());

            if (animals.isEmpty()) {
                List<CommonIdBean> crops = getDataManager().getCrops(parcelList.get(pos).getParcelId());

                for (CommonIdBean cropBean : crops) {
                    String cropExpenseStatus = getDataManager().getExpenseApplyStatus(cropBean.id, cropBean.recordKey);
                    if (cropExpenseStatus != null) {
                        msg = getString(R.string.deleteExpenseMsg);
                        break;
                    }
                }
            } else {
                for (CommonIdBean animalBean : animals) {
                    String animalExpenseStatus = getDataManager().getExpenseApplyStatus(animalBean.id, animalBean.recordKey);
                    if (animalExpenseStatus != null) {
                        msg = getString(R.string.deleteExpenseMsg);
                        break;
                    }
                }
            }
        } else {
            msg = getString(R.string.deleteExpenseMsg);
        }

        return msg;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (isNetworkConnected(true)) {
            getBaseActivity().doSyncAppDB(false, flag -> {
                switch (flag) {
                    case AppConstants.ONLINE:
                        getPropertyDetails();
                        break;

                    case AppConstants.OFFLINE:
                        getPropertyDetailsFromDB();
                        break;
                }
            });
        }
        else getPropertyDetailsFromDB();
    }

    private void getPropertyDetails() {
        setLoading(true);

        HashMap<String, String> mParams = new HashMap<>();
        mParams.put("propertyId", propertyInfoBean.getPropertyId());

        getDataManager().doServerPropertyDetailApiCall(getDataManager().getHeader(), mParams)
                .getAsResponse(new VolleyResponseListener() {
                    @Override
                    public void onVolleyResponse(String response) {
                        setLoading(false);
                        PropertyDetailResponse propertyDetailResponse = getDataManager().mGson.fromJson(response, PropertyDetailResponse.class);

                        if (propertyDetailResponse.getStatus().equals("success")) {
                            updateUi(propertyDetailResponse.getPropertyDetails());
                        } else {
                            CommonUtils.showToast(getBaseActivity(), propertyDetailResponse.getMessage(), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    private void updateUi(PropertyDetailResponse.PropertyDetailsBean propertyDetails) {

        tvPropertyTitle.setText(propertyDetails.getPropertyName());
        tvAddress.setText(propertyDetails.getLocation());
        tvAnimalCount.setText(propertyDetails.getNoOfAnimal());
        tvCropCount.setText(propertyDetails.getNoOfCrop());
        tvRainCount.setText((propertyDetails.getRainCount().isEmpty()) ? "0" : propertyDetails.getRainCount());


        StringBuilder builder = new StringBuilder();
        String tempUnit = propertyDetails.getSizeUnite().equalsIgnoreCase("acres") ? getString(R.string.acres) : getString(R.string.hectares);
        builder.append(propertyDetails.getSize()).append(" ").append(tempUnit.substring(0, 1).toUpperCase()).append(tempUnit.substring(1).toLowerCase());
        tvSize.setText(builder);
        tvDescription.setText((propertyDetails.getDescription().isEmpty()) ? getString(R.string.na) : propertyDetails.getDescription());

        parcelList.clear();
        parcelList.addAll(propertyDetails.getParcel());

        tvNoRecord.setVisibility(parcelList.isEmpty() ? View.VISIBLE : View.GONE);

        parcelAdapter.notifyDataSetChanged();
        llMain.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                getBaseActivity().onBackPressed();
                break;

            case R.id.llAnimalRecord:
                Intent intent = new Intent(getBaseActivity(), AnimalActivity.class);
                intent.putExtra(AppConstants.KEY_FROM_PROPERTY, propertyInfoBean.getPropertyId());
                startActivity(intent);
                break;

            case R.id.llCropRecord:
                intent = new Intent(getBaseActivity(), CropActivity.class);
                intent.putExtra(AppConstants.KEY_FROM_PROPERTY, propertyInfoBean.getPropertyId());
                startActivity(intent);
                break;

            case R.id.llRainRecord:
                getBaseActivity().addFragment(RainRecordFragment.newInstance(propertyInfoBean), R.id.propertyFrame, true);
                break;

            case R.id.imgAddFile:
                ParcelDialog parcelDialog = new ParcelDialog();
                parcelDialog.setOnParcelAddEditListener(0, propertyInfoBean.getPropertyId(), "", PropertyDetailFragment.this);
                parcelDialog.show(getChildFragmentManager());
                break;
        }
    }

    private void doAddParcel(String parcelName) {
        setLoading(true);
        HashMap<String, String> mParams = new HashMap<>();
        mParams.put("parcelName", parcelName);
        mParams.put("propertyId", propertyInfoBean.getPropertyId());
        mParams.put("recordKey", CalenderUtils.getTimestamp());

        getDataManager().doServerAddParcelApiCall(getDataManager().getHeader(), mParams)
                .getAsJsonObject(new VolleyJsonObjectListener() {

                    @Override
                    public void onVolleyJsonObject(JSONObject jsonObject) {
                        setLoading(false);
                        try {
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            if (status.equals("success")) {
                                PropertyDetailResponse.PropertyDetailsBean.ParcelBean bean = new PropertyDetailResponse.PropertyDetailsBean.ParcelBean();
                                JSONObject obj = jsonObject.getJSONObject("parcelDetail");

                                bean.setParcelId(obj.getString("parcelId"));
                                bean.setParcelName(obj.getString("parcelName"));
                                bean.setProperty_id(obj.getString("property_id"));
                                bean.setRecordKey(obj.getString("recordKey"));
                                bean.setStatus(obj.getString("status"));
                                bean.setCrd(obj.getString("crd"));
                                bean.setUpd(obj.getString("upd"));
                                parcelList.add(0, bean);
                                //Todo add parcel to local db done
                                doAddEditDelParcelToLocalDB("add", bean, propertyInfoBean.getPropertyId());

                                tvNoRecord.setVisibility(parcelList.isEmpty() ? View.VISIBLE : View.GONE);
                                parcelAdapter.notifyDataSetChanged();
                                CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                            } else
                                CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                        } catch (Exception e) {
                            CommonUtils.showToast(getBaseActivity(), getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyJsonException() {
                        setLoading(false);
                        CommonUtils.showToast(getBaseActivity(), getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    private void doEditParcel(int pos, String parcelName) {
        setLoading(true);
        HashMap<String, String> mParams = new HashMap<>();
        mParams.put("propertyId", propertyInfoBean.getPropertyId());
        mParams.put("recordKey", parcelList.get(pos).getRecordKey());
        mParams.put("parcelName", parcelName);

        getDataManager().doServerUpdateParcelApiCall(getDataManager().getHeader(), mParams)
                .getAsJsonObject(new VolleyJsonObjectListener() {

                    @Override
                    public void onVolleyJsonObject(JSONObject jsonObject) {
                        setLoading(false);
                        try {
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            if (status.equals("success")) {
                                PropertyDetailResponse.PropertyDetailsBean.ParcelBean bean = new PropertyDetailResponse.PropertyDetailsBean.ParcelBean();
                                JSONObject obj = jsonObject.getJSONObject("parcelDetail");

                                bean.setParcelId(obj.getString("parcelId"));
                                bean.setParcelName(obj.getString("parcelName"));
                                bean.setProperty_id(obj.getString("property_id"));
                                bean.setRecordKey(obj.getString("recordKey"));
                                bean.setStatus(obj.getString("status"));
                                bean.setCrd(obj.getString("crd"));
                                bean.setUpd(obj.getString("upd"));
                                parcelList.set(pos, bean);
                                //Todo edit parcel to local db done
                                doAddEditDelParcelToLocalDB("edit", bean, propertyInfoBean.getPropertyId());

                                tvNoRecord.setVisibility(parcelList.isEmpty() ? View.VISIBLE : View.GONE);
                                parcelAdapter.notifyDataSetChanged();
                                CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                            } else
                                CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                        } catch (Exception e) {
                            CommonUtils.showToast(getBaseActivity(), getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyJsonException() {
                        setLoading(false);
                        CommonUtils.showToast(getBaseActivity(), getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    private void doDeleteParcelData(int pos) {
        setLoading(true);
        HashMap<String, String> mParams = new HashMap<>();
        mParams.put("propertyId", propertyInfoBean.getPropertyId());
        mParams.put("recordKey", parcelList.get(pos).getRecordKey());

        getDataManager().doServerDeleteParcelApiCall(getDataManager().getHeader(), mParams)
                .getAsJsonObject(new VolleyJsonObjectListener() {

                    @Override
                    public void onVolleyJsonObject(JSONObject jsonObject) {
                        setLoading(false);
                        try {
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            if (status.equals("success")) {
                                //Todo del parcel to local db done
                                doAddEditDelParcelToLocalDB("del", parcelList.get(pos), propertyInfoBean.getPropertyId());

                                parcelList.remove(pos);
                                tvNoRecord.setVisibility(parcelList.isEmpty() ? View.VISIBLE : View.GONE);
                                parcelAdapter.notifyItemRemoved(pos);
                                CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                            } else
                                CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                        } catch (Exception e) {
                            CommonUtils.showToast(getBaseActivity(), getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyJsonException() {
                        setLoading(false);
                        CommonUtils.showToast(getBaseActivity(), getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    /*Local DB start here*/
    private void getPropertyDetailsFromDB() {
        setLoading(true);
        new Thread(() -> {

            PropertyDetailResponse.PropertyDetailsBean localPropertyBean = getDataManager().getPropertyDetail(propertyInfoBean.getPropertyId());
            List<PropertyDetailResponse.PropertyDetailsBean.ParcelBean> localParcelList = getDataManager().getParcelList(propertyInfoBean.getPropertyId());
            String animalCount = getDataManager().getAnimalCount(propertyInfoBean.getPropertyId());
            String cropCount = getDataManager().getCropCount(propertyInfoBean.getPropertyId());
            setLoading(false);
            if (localPropertyBean != null) {
                //update on ui thread
                handler.post(() -> updateUiDB(localPropertyBean, animalCount, cropCount, localParcelList));
            }

        }).start();
    }

    private void updateUiDB(PropertyDetailResponse.PropertyDetailsBean localPropertyBean, String animalCount, String cropCount, List<PropertyDetailResponse.PropertyDetailsBean.ParcelBean> localParcelList) {
        tvPropertyTitle.setText(localPropertyBean.getPropertyName());
        tvAddress.setText(localPropertyBean.getLocation());
        tvAnimalCount.setText(animalCount);
        tvCropCount.setText(cropCount);
        tvRainCount.setText((localPropertyBean.getRainCount().isEmpty()) ? "0" : localPropertyBean.getRainCount());

        StringBuilder builder = new StringBuilder();
        String tempUnit = localPropertyBean.getSizeUnite().equalsIgnoreCase("acres") ? getString(R.string.acres) : getString(R.string.hectares);
        builder.append(localPropertyBean.getSize()).append(" ")
                .append(tempUnit.substring(0, 1).toUpperCase())
                .append(tempUnit.substring(1).toLowerCase());
        tvSize.setText(builder);
        tvDescription.setText((localPropertyBean.getDescription() != null && localPropertyBean.getDescription().isEmpty()) ? getString(R.string.na) : localPropertyBean.getDescription());

        parcelList.clear();
        parcelList.addAll(localParcelList);

        tvNoRecord.setVisibility(parcelList.isEmpty() ? View.VISIBLE : View.GONE);

        parcelAdapter.notifyDataSetChanged();
        llMain.setVisibility(View.VISIBLE);
    }

    private void doAddEditDelParcelToLocalDB(String flag, int pos, String parcelId, String parcelName) {
        new Thread(() -> {
            //Todo check parcel name for local db
            Parcel parcel = new Parcel();

            parcel.setProperty_id(propertyInfoBean.getPropertyId());
            parcel.setParcelId(parcelId);
            parcel.setParcelName(parcelName);
            parcel.setCrd(CalenderUtils.getTimestamp(AppConstants.DB_TIMESTAMP_FORMAT));

            //local add
            if (parcelId.equalsIgnoreCase(parcelName)) {
                parcel.setEvent(AppConstants.DB_EVENT_ADD);
                parcel.setDataSync(AppConstants.DB_SYNC_FALSE);

                //set sync status true
                getDataManager().setLocalDataExistForSync(true);

                switch (flag) {
                    //add parcel to local db
                    case "add":
                        //parcel.setParcelId(CalenderUtils.getTimestamp());
                        parcel.setRecordKey(CalenderUtils.getTimestamp());
                        getDataManager().insertParcel(parcel);
                        AppLogger.d("database ", "parcel add success ");
                        break;

                    //edit parcel to local db
                    case "edit":
                        // parcel.setParcelId(parcelList.get(pos).getParcelId());
                        parcel.setRecordKey(parcelList.get(pos).getRecordKey());
                        getDataManager().updateParcel(parcel);
                        AppLogger.d("database ", "parcel edit success ");
                        break;

                    //del parcel to local db
                    case "del":
                        getDataManager().deleteUsingParcelId(parcelId);
                        AppLogger.d("database ", "parcel delete success ");
                        break;
                }
            } else {  //online operation
                switch (flag) {
                    case "add":
                        //parcel.setParcelId(parcelId);
                        parcel.setRecordKey(CalenderUtils.getTimestamp());
                        parcel.setEvent(AppConstants.DB_EVENT_ADD);
                        parcel.setDataSync(AppConstants.DB_SYNC_FALSE);
                        getDataManager().insertParcel(parcel);

                        //set sync status true
                        getDataManager().setLocalDataExistForSync(true);
                        break;

                    //online parcel data edit
                    case "edit":
                        //  parcel.setParcelId(parcelId);
                        parcel.setRecordKey(parcelList.get(pos).getRecordKey());
                        parcel.setEvent(AppConstants.DB_EVENT_EDIT);
                        parcel.setDataSync(AppConstants.DB_SYNC_FALSE);
                        getDataManager().updateParcel(parcel);

                        //set sync status true
                        getDataManager().setLocalDataExistForSync(true);
                        break;

                    //online parcel data del
                    case "del":
                        // parcel.setParcelId(parcelId);
                        parcel.setRecordKey(parcelList.get(pos).getRecordKey());
                        parcel.setEvent(AppConstants.DB_EVENT_DEL);
                        parcel.setDataSync(AppConstants.DB_SYNC_FALSE);
                        getDataManager().updateParcel(parcel);

                        //set sync status true
                        getDataManager().setLocalDataExistForSync(true);

                        //delete all child when parcel event is delete
                        getDataManager().deleteAnimalUsingParcelId(parcelId);
                        getDataManager().deleteCropUsingParcelId(parcelId);
                        break;
                }
            }

            String animalCount = getDataManager().getAnimalCount(propertyInfoBean.getPropertyId());
            String cropCount = getDataManager().getCropCount(propertyInfoBean.getPropertyId());

            //switch from bg to main thread
            handler.post(() -> {
                PropertyDetailResponse.PropertyDetailsBean.ParcelBean bean = new PropertyDetailResponse.PropertyDetailsBean.ParcelBean();
                bean.setParcelId(parcelId);
                bean.setParcelName(parcelName);
                bean.setProperty_id(propertyInfoBean.getPropertyId());
                bean.setRecordKey(parcel.getRecordKey());
                bean.setStatus(parcel.getStatus());
                bean.setCrd(parcel.getCrd());
                bean.setUpd(parcel.getUpd());

                //print msg according to flag...
                switch (flag) {
                    case "add":
                        parcelList.add(0, bean);
                        parcelAdapter.notifyDataSetChanged();
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.parcelAddSuccessMsg), Toast.LENGTH_SHORT);
                        break;

                    case "edit":
                        parcelList.set(pos, bean);
                        parcelAdapter.notifyItemChanged(pos);
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.parcelUpdateSuccessMsg), Toast.LENGTH_SHORT);
                        break;

                    case "del":
                        tvAnimalCount.setText(animalCount);
                        tvCropCount.setText(cropCount);

                        parcelList.remove(pos);
                        parcelAdapter.notifyItemRemoved(pos);
                        CommonUtils.showToast(getBaseActivity(), getString(R.string.parcelDelSuccessMsg), Toast.LENGTH_SHORT);
                        break;
                }

                tvNoRecord.setVisibility(parcelList.isEmpty() ? View.VISIBLE : View.GONE);
            });

        }).start();
    }

    private void doAddEditDelParcelToLocalDB(String flag, PropertyDetailResponse.PropertyDetailsBean.ParcelBean bean, String propertyId) {
        new Thread(() -> {
            Parcel parcel = new Parcel();
            parcel.setProperty_id(propertyId);
            parcel.setParcelId(bean.getParcelId());
            parcel.setParcelName(bean.getParcelName());
            parcel.setRecordKey(bean.getRecordKey());
            parcel.setCrd(bean.getCrd());
            parcel.setUpd(bean.getUpd());

            switch (flag) {
                case "add":
                    parcel.setEvent(AppConstants.DB_EVENT_ADD);
                    parcel.setDataSync(AppConstants.DB_SYNC_TRUE);
                    getDataManager().insertParcel(parcel);
                    AppLogger.d("database ", "parcel add success ");
                    break;

                case "edit":
                    parcel.setEvent(AppConstants.DB_EVENT_EDIT);
                    parcel.setDataSync(AppConstants.DB_SYNC_TRUE);
                    getDataManager().updateParcel(parcel);
                    AppLogger.d("database ", "parcel update success ");
                    break;

                case "del":
                    getDataManager().deleteUsingParcelId(bean.getParcelId());

                    String animalCount = getDataManager().getAnimalCount(propertyInfoBean.getPropertyId());
                    String cropCount = getDataManager().getCropCount(propertyInfoBean.getPropertyId());
                    //update ui data
                    handler.post(() -> {
                        tvAnimalCount.setText(animalCount);
                        tvCropCount.setText(cropCount);
                    });
                    AppLogger.d("database ", "parcel delete success ");
                    break;
            }

        }).start();
    }
    /*Local DB end here*/

    @Override
    public void onBackPress() {
        if (propertyInfoBean != null) propertyInfoFragment.hitApi();
    }

    @Override
    public void onParcelAdd(String parcelName) {
        if (isNetworkConnected(true)) {
            getBaseActivity().doSyncAppDB(false, flag -> {
                switch (flag) {
                    case AppConstants.ONLINE:
                        doAddParcel(parcelName);
                        break;

                    case AppConstants.OFFLINE:
                        doAddEditDelParcelToLocalDB("add", 0, parcelName, parcelName);
                        break;
                }
            });
        } else doAddEditDelParcelToLocalDB("add", 0, parcelName, parcelName);
    }

    @Override
    public void onParcelEdit(int pos, String parcelName) {
        if (isNetworkConnected(true)) {
            getBaseActivity().doSyncAppDB(false, flag -> {
                switch (flag) {
                    case AppConstants.ONLINE:
                        doEditParcel(pos, parcelName);
                        break;

                    case AppConstants.OFFLINE:
                        doAddEditDelParcelToLocalDB("edit", pos, parcelList.get(pos).getParcelId(), parcelName);
                        break;
                }
            });
        } else
            doAddEditDelParcelToLocalDB("edit", pos, parcelList.get(pos).getParcelId(), parcelName);
    }

}
