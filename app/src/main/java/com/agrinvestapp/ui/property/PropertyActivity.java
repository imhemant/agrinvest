package com.agrinvestapp.ui.property;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.agrinvestapp.R;
import com.agrinvestapp.ui.base.BaseActivity;
import com.agrinvestapp.ui.property.fragment.PropertyInfoFragment;

public class PropertyActivity extends BaseActivity {

    private Boolean isReplace = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property);

        replaceFragment(new PropertyInfoFragment(), R.id.propertyFrame);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isReplace){
            Fragment fragment = getCurrentFragment();
            assert fragment != null;
            if (fragment instanceof PropertyInfoFragment) {
                PropertyInfoFragment infoFragment = (PropertyInfoFragment) fragment;
                infoFragment.hitApi();
            }
        }else isReplace =false;
    }
}
