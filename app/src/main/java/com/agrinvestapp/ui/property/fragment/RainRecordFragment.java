package com.agrinvestapp.ui.property.fragment;


import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.PropertyInfoResponse;
import com.agrinvestapp.data.model.api.RainBeanResponse;
import com.agrinvestapp.ui.base.BaseFragment;
import com.agrinvestapp.ui.property.adapter.RainDataAdapter;
import com.agrinvestapp.ui.property.dialog.FilterRainDataCallback;
import com.agrinvestapp.ui.property.dialog.FilterRainDataDialog;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.CalenderUtils;
import com.agrinvestapp.utils.CommonUtils;
import com.agrinvestapp.utils.pagination.RecyclerViewScrollListener;
import com.agrinvestapp.volley.VolleyRequest;
import com.agrinvestapp.volley.VolleyResponseListener;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class RainRecordFragment extends BaseFragment implements View.OnClickListener {

    private static final String MODEL_TAG = "propertyInfoBean";

    private List<RainBeanResponse.RainRecordListBean> rainList;
    private RainDataAdapter rainDataAdapter;
    private RecyclerViewScrollListener scrollListener;

    private TextView tvNoRecord, tvFilterCount;
    private FrameLayout frameCount;

    private PropertyInfoResponse.PropertyInfoBean propertyInfoBean;
    private String fromDate = "", toDate = "";
    private Integer offset = 0;

    public static RainRecordFragment newInstance(PropertyInfoResponse.PropertyInfoBean propertyInfoBean) {

        Bundle args = new Bundle();
        args.putParcelable(MODEL_TAG, propertyInfoBean);

        RainRecordFragment fragment = new RainRecordFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            propertyInfoBean = getArguments().getParcelable(MODEL_TAG);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_rain_record, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        /* parent view start */
        ImageView imgBack = view.findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);
        imgBack.setVisibility(View.VISIBLE);

        RelativeLayout rlFilter = view.findViewById(R.id.rlFilter);
        rlFilter.setVisibility(View.VISIBLE);
        rlFilter.setOnClickListener(this);
        frameCount = view.findViewById(R.id.frameCount);
        frameCount.setVisibility(View.GONE);
        tvFilterCount = view.findViewById(R.id.tvFilterCount);
        /*ImageView imgRight = view.findViewById(R.id.imgRight);
        imgRight.setOnClickListener(this);
        imgRight.setImageDrawable(getResources().getDrawable(R.drawable.ic_filter));
        imgRight.setVisibility(View.VISIBLE);*/

        TextView tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.rainDataRecord);

        /* parent view end */

        rainList = new ArrayList<>();

        tvNoRecord = view.findViewById(R.id.tvNoRecord);
        tvNoRecord.setVisibility(rainList.isEmpty() ? View.VISIBLE : View.GONE);
        view.findViewById(R.id.mainFrame).setOnClickListener(this);  //avoid bg click

        RecyclerView rvRainData = view.findViewById(R.id.rvRainData);
        rainDataAdapter = new RainDataAdapter(rainList);
        rvRainData.setAdapter(rainDataAdapter);

        scrollListener = new RecyclerViewScrollListener() {
            @Override
            public void onLoadMore() {
                rainDataAdapter.showLoading(true);
                rainDataAdapter.notifyDataSetChanged();
                offset += AppConstants.LIMIT;
                getRainRecord(offset);
            }
        };
        rvRainData.addOnScrollListener(scrollListener);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDataFromServerOrDB(offset);
    }

    private void getDataFromServerOrDB(Integer offset) {
        if (isNetworkConnected(true)) getRainRecord(offset);
        else getRainRecordFromDB(offset);
    }

    private void getRainRecord(Integer offset) {
        if (offset == 0) setLoading(true);

        HashMap<String, String> mParams = new HashMap<>();
        mParams.put("propertyId", propertyInfoBean.getPropertyId());
        mParams.put("fromDate", fromDate);
        mParams.put("toDate", toDate);
        mParams.put("limit", String.valueOf(AppConstants.LIMIT));
        mParams.put("start", String.valueOf(offset));

        getDataManager().doServerRainRecordApiCall(getDataManager().getHeader(), mParams)
                .getAsResponse(new VolleyResponseListener() {
                    @Override
                    public void onVolleyResponse(String response) {
                        setLoading(false);
                        RainBeanResponse rainBean = getDataManager().mGson.fromJson(response, RainBeanResponse.class);

                        try {
                            if (rainBean.getStatus().equals("success")) {
                                updateUi(rainBean.getRainRecordList(), offset);
                            } else {
                                if (!rainBean.getMessage().equals(getString(R.string.no_record_found)) && !rainBean.getMessage().equals(getString(R.string.no_record_found2)))
                                    CommonUtils.showToast(getBaseActivity(), rainBean.getMessage(), Toast.LENGTH_SHORT);

                                hideFooterLoader();

                                if (offset == 0 && (rainBean.getMessage().equals(getString(R.string.no_record_found)) || rainBean.getMessage().equals(getString(R.string.no_record_found2)))) {
                                    rainList.clear();
                                    tvNoRecord.setVisibility(rainList.isEmpty() ? View.VISIBLE : View.GONE);
                                    frameCount.setVisibility(!fromDate.isEmpty() ? View.VISIBLE : View.GONE);
                                    tvFilterCount.setText(String.valueOf(rainList.size()));
                                    rainDataAdapter.notifyDataSetChanged();
                                }
                            }
                        } catch (Exception ignored) {
                        }
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        hideFooterLoader();
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    private void hideFooterLoader() {
        rainDataAdapter.showLoading(false);
        rainDataAdapter.notifyDataSetChanged();
    }

    private void updateUi(List<RainBeanResponse.RainRecordListBean> recordList, Integer offset) {
        hideFooterLoader();

        if (offset == 0) rainList.clear();
        rainList.addAll(recordList);
        tvNoRecord.setVisibility(rainList.isEmpty() ? View.VISIBLE : View.GONE);
        frameCount.setVisibility(!fromDate.isEmpty() ? View.VISIBLE : View.GONE);
        tvFilterCount.setText(String.valueOf(rainList.size()));
        rainDataAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                getBaseActivity().onBackPressed();
                break;

            case R.id.rlFilter:  //filter click
                FilterRainDataDialog filterRainDataDialog = new FilterRainDataDialog();
                filterRainDataDialog.setOnRainFilterListener(fromDate, toDate, new FilterRainDataCallback() {
                    @Override
                    public void onFilterData(String tempFromDate, String tempToDate) {
                        fromDate = tempFromDate;
                        toDate = tempToDate;
                        /*scrollListener.onDataCleared();
                        offset=0;
                        getDataFromServerOrDB(offset);*/
                        getRainRecordFromDB(0);
                    }

                    @Override
                    public void onResetClick() {
                        fromDate = "";
                        toDate = "";
                        scrollListener.onDataCleared();
                        offset = 0;
                        getDataFromServerOrDB(offset);
                    }
                });
                filterRainDataDialog.show(getChildFragmentManager());
                break;
        }
    }

    /*Local DB start here*/
    private void getRainRecordFromDB(Integer offset) {
        setLoading(true);
        new Thread(() -> {
            List<RainBeanResponse.RainRecordListBean> localRainList;

            if (!fromDate.isEmpty() && !toDate.isEmpty()) {
                fromDate = CalenderUtils.formatDate(fromDate, AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT);
                toDate = CalenderUtils.formatDate(toDate, AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT);

                localRainList = getDataManager().getFilterRainDataList(propertyInfoBean.getPropertyId(), fromDate, toDate);
            } else if (!fromDate.isEmpty()) {
                fromDate = CalenderUtils.formatDate(fromDate, AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT);
                localRainList = getDataManager().getFilterRainDataList(propertyInfoBean.getPropertyId(), fromDate);
            } else {
                localRainList = getDataManager().getRainDataList(propertyInfoBean.getPropertyId());
            }

            new Handler(Looper.getMainLooper()).post(() -> {
                setLoading(false);
                updateUi(localRainList, offset);
            });

        }).start();
    }
    /*Local DB end here*/

}
