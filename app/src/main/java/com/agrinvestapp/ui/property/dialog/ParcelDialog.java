package com.agrinvestapp.ui.property.dialog;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.db.Parcel;
import com.agrinvestapp.ui.base.BaseDialog;
import com.agrinvestapp.utils.CommonUtils;


public class ParcelDialog extends BaseDialog implements View.OnClickListener {

    private static final String TAG = ParcelDialog.class.getSimpleName();

    private ParcelCallback callback;

    private EditText etName;
    private String parcelName = "", propertyId;
    private int pos;

    private Handler handler = new Handler(Looper.getMainLooper());

    public void setOnParcelAddEditListener(int pos, String propertyId, String parcelName, ParcelCallback callback) {
        this.pos = pos;
        this.propertyId = propertyId;
        this.parcelName = parcelName;
        this.callback = callback;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_add_edit_parcel, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        etName = view.findViewById(R.id.etName);
        etName.setText(parcelName);

        TextView tvParcelTitle = view.findViewById(R.id.tvParcelTitle);
        TextView btnAdd = view.findViewById(R.id.btnAdd);
        TextView btnCancel = view.findViewById(R.id.btnCancel);

        if (!parcelName.isEmpty()) {
            tvParcelTitle.setText(getString(R.string.edit_parcel));
            btnAdd.setText(getString(R.string.update));  //update_parcel
            etName.setSelection(parcelName.length());
        }

        btnAdd.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAdd:
                String tempName = etName.getText().toString().trim();
                if (verifyInput(tempName)) {
                    new Thread(() -> {

                        Parcel parcel = getDataManager().findByParcelName(getDataManager().getUserInfo().getUserId(), tempName);

                        handler.post(() -> {
                            if (parcelName.isEmpty()) {
                                if (parcel != null && parcel.getParcelName().equalsIgnoreCase(tempName)) {
                                    CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_parcel_exist), Toast.LENGTH_SHORT);
                                } else {
                                    callback.onParcelAdd(tempName);
                                    dismissDialog(TAG);
                                }
                            } else {
                                if (parcelName.equalsIgnoreCase(tempName)) {
                                    CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_parcel_update), Toast.LENGTH_SHORT);
                                } else if (parcel != null && parcel.getParcelName().equalsIgnoreCase(tempName)) {
                                    CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_parcel_exist), Toast.LENGTH_SHORT);
                                } else {
                                    callback.onParcelEdit(pos, tempName);
                                    dismissDialog(TAG);
                                }
                            }
                        });

                    }).start();
                }
                break;

            case R.id.btnCancel:
                dismissDialog(TAG);
                break;

        }
    }

    private boolean verifyInput(String parcelName) {
        if (parcelName.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_parcel), Toast.LENGTH_SHORT);
            return false;
        } else if (!(parcelName.length() >= 3)) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_parcel_length), Toast.LENGTH_SHORT);
            return false;
        } else return true;
    }

}
