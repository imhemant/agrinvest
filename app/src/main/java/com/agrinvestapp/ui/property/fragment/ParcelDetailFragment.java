package com.agrinvestapp.ui.property.fragment;


import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.AnimalInfoResponse;
import com.agrinvestapp.data.model.api.CropInfoResponse;
import com.agrinvestapp.data.model.api.PropertyDetailResponse;
import com.agrinvestapp.ui.base.BaseFragment;
import com.agrinvestapp.ui.property.adapter.ParcelAnimalListAdapter;
import com.agrinvestapp.ui.property.adapter.ParcelCropListAdapter;

import java.util.ArrayList;
import java.util.List;


public class ParcelDetailFragment extends BaseFragment implements View.OnClickListener {

    private FrameLayout frameAnimalTab,frameCropTab;
    private TextView tvNoRecord;
    private List<AnimalInfoResponse.AnimalListBean> animalList;
    private List<CropInfoResponse.CroplListBean> cropList;

    private RecyclerView rvParcelAnimalDetail, rvParcelCropDetail;
    private ParcelAnimalListAdapter animalListAdapter;
    private ParcelCropListAdapter cropListAdapter;

    private PropertyDetailResponse.PropertyDetailsBean.ParcelBean parcelBean;

    private Handler handler = new Handler(Looper.getMainLooper());

    public static ParcelDetailFragment newInstance(PropertyDetailResponse.PropertyDetailsBean.ParcelBean parcelBean) {

        Bundle args = new Bundle();

        ParcelDetailFragment fragment = new ParcelDetailFragment();
        fragment.setBean(parcelBean);
        fragment.setArguments(args);
        return fragment;
    }

    private void setBean(PropertyDetailResponse.PropertyDetailsBean.ParcelBean parcelBean) {
        this.parcelBean = parcelBean;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_parcel_detail, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
           /* toolbar view start */
        ImageView imgBack = view.findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);
        imgBack.setVisibility(View.VISIBLE);

        TextView tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.parcelDetails);
        /* toolbar view end */
        view.findViewById(R.id.llMain).setOnClickListener(this);  //avoid bg click

        TextView tvParcelTitle = view.findViewById(R.id.tvParcelTitle);
        frameAnimalTab = view.findViewById(R.id.frameAnimalTab);
        frameCropTab = view.findViewById(R.id.frameCropTab);

        tvNoRecord = view.findViewById(R.id.tvNoRecord);
        rvParcelAnimalDetail = view.findViewById(R.id.rvParcelAnimalDetail);
        rvParcelCropDetail = view.findViewById(R.id.rvParcelCropDetail);

        frameAnimalTab.setOnClickListener(this);
        frameCropTab.setOnClickListener(this);

        animalList = new ArrayList<>();
        animalListAdapter = new ParcelAnimalListAdapter(animalList);
        rvParcelAnimalDetail.setAdapter(animalListAdapter);

        cropList = new ArrayList<>();
        cropListAdapter = new ParcelCropListAdapter(cropList);
        rvParcelCropDetail.setAdapter(cropListAdapter);

        tvParcelTitle.setText(parcelBean != null ? parcelBean.getParcelName() : getString(R.string.na));

        getAnimalAndCropFromDB();
    }

    private void getAnimalAndCropFromDB() {
        new Thread(() -> {
            if (parcelBean != null) {
                animalList.clear();
                cropList.clear();
                animalList.addAll(getDataManager().getParcelAnimalList(parcelBean.getParcelId()));
                cropList.addAll(getDataManager().getParcelCropList(parcelBean.getParcelId()));
            }

            handler.post(() -> updateListUI(0));
        }).start();
    }

    private void updateListUI(int pos) {
        switch (pos) {
            case 0:
                rvParcelCropDetail.setVisibility(View.GONE);
                rvParcelAnimalDetail.setVisibility(View.VISIBLE);
                tvNoRecord.setVisibility(animalList.isEmpty() ? View.VISIBLE : View.GONE);
                animalListAdapter.notifyDataSetChanged();
                break;

            case 1:
                rvParcelAnimalDetail.setVisibility(View.GONE);
                rvParcelCropDetail.setVisibility(View.VISIBLE);
                tvNoRecord.setVisibility(cropList.isEmpty() ? View.VISIBLE : View.GONE);
                cropListAdapter.notifyDataSetChanged();
                break;
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgBack:
                getBaseActivity().onBackPressed();
                break;

            case R.id.frameAnimalTab:
                updateTabUI(0);
                break;

            case R.id.frameCropTab:
                updateTabUI(1);
                break;
        }
    }

    private void updateTabUI(int pos) {
        TextView tvAnimal = (TextView) frameAnimalTab.getChildAt(0);
        View viewAnimal = frameAnimalTab.getChildAt(1);

        TextView tvCrop = (TextView) frameCropTab.getChildAt(0);
        View viewCrop = frameCropTab.getChildAt(1);

        switch (pos) {
            case 0:
                updateListUI(pos);
                tvAnimal.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                tvCrop.setTextColor(getResources().getColor(R.color.colorLightBlack));

                viewAnimal.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                viewCrop.setBackgroundColor(getResources().getColor(R.color.colorTransparent));
                break;

            case 1:
                updateListUI(pos);
                tvCrop.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                tvAnimal.setTextColor(getResources().getColor(R.color.colorLightBlack));

                viewCrop.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                viewAnimal.setBackgroundColor(getResources().getColor(R.color.colorTransparent));
                break;
        }
    }
}
