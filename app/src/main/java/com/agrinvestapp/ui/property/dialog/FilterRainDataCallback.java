package com.agrinvestapp.ui.property.dialog;

public interface FilterRainDataCallback {
    void onFilterData(String fromDate, String toDate);

    void onResetClick();
}
