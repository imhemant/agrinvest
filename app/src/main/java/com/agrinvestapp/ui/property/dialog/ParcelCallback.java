package com.agrinvestapp.ui.property.dialog;

public interface ParcelCallback {
    void onParcelAdd(String parcelName);

    void onParcelEdit(int pos, String parcelName);
}
