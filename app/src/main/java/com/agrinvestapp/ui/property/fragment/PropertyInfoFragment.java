package com.agrinvestapp.ui.property.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.PropertyInfoResponse;
import com.agrinvestapp.data.model.db.Animal;
import com.agrinvestapp.data.model.db.Parcel;
import com.agrinvestapp.data.model.db.Property;
import com.agrinvestapp.data.model.db.RainData;
import com.agrinvestapp.data.model.other.CommonIdBean;
import com.agrinvestapp.ui.base.BaseFragment;
import com.agrinvestapp.ui.base.ClickListener;
import com.agrinvestapp.ui.base.dialog.DeleteDialog;
import com.agrinvestapp.ui.base.dialog.PlanUpgradeDialog;
import com.agrinvestapp.ui.main.fragment.profile.PlanActivity;
import com.agrinvestapp.ui.property.adapter.PropertyInfoAdapter;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.AppLogger;
import com.agrinvestapp.utils.AppUtils;
import com.agrinvestapp.utils.CalenderUtils;
import com.agrinvestapp.utils.CommonUtils;
import com.agrinvestapp.volley.VolleyJsonObjectListener;
import com.agrinvestapp.volley.VolleyRequest;
import com.agrinvestapp.volley.VolleyResponseListener;
import com.android.volley.VolleyError;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PropertyInfoFragment extends BaseFragment implements View.OnClickListener {

    private TextView tvNoRecord;
    private Button btnAddProperty;
    private FrameLayout frameAdd;

    private PropertyInfoAdapter adapter;

    private List<PropertyInfoResponse.PropertyInfoBean> list;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_property_info, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        /* toolbar view start */
        ImageView imgBack = view.findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);
        imgBack.setVisibility(View.VISIBLE);

        TextView tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.property); //property_info
        /* toolbar view end */

        RecyclerView rvProperties = view.findViewById(R.id.rvProperties);
        list = new ArrayList<>();

        adapter = new PropertyInfoAdapter(list, new ClickListener() {
            @Override
            public void onItemClick(int pos) {
                PropertyDetailFragment propertyDetailFragment = new PropertyDetailFragment();
                propertyDetailFragment.setInstance(PropertyInfoFragment.this, list.get(pos));
                getBaseActivity().addFragment(propertyDetailFragment, R.id.propertyFrame, true);
            }

            @Override
            public void onEditClick(int pos) {
                if (isNetworkConnected(false)) {
                    AddEditPropertyFragment propertyFragment = new AddEditPropertyFragment();
                    propertyFragment.setInstance(PropertyInfoFragment.this, false);
                    propertyFragment.setPropertyModel(list.get(pos));
                    getBaseActivity().addFragment(propertyFragment, R.id.propertyFrame, true);
                } else
                    CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_internet), Toast.LENGTH_SHORT);
            }

            @Override
            public void onDeleteClick(int pos) {
                setLoading(true);

                new Thread(() -> {
                    String msg = getExpenseStatus(pos);

                    new Handler(Looper.getMainLooper()).post(() -> {
                        setLoading(false);
                        DeleteDialog.newInstance(!msg.isEmpty(), msg, () -> {
                            if (isNetworkConnected(false)) doDeletePropertyInfo(pos);
                            else
                                CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_internet), Toast.LENGTH_SHORT);
                        }).show(getChildFragmentManager());
                    });
                }).start();
            }
        });

        rvProperties.setAdapter(adapter);

        tvNoRecord = view.findViewById(R.id.tvNoRecord);
        frameAdd = view.findViewById(R.id.frameAdd);
        frameAdd.setOnClickListener(this);
        btnAddProperty = view.findViewById(R.id.btnAddProperty);
        btnAddProperty.setOnClickListener(this);
    }

    private String getExpenseStatus(int pos) {
        String msg = "";
        String propertyExpenseStatus = getDataManager().getExpenseApplyStatus(list.get(pos).getPropertyId());

        if (propertyExpenseStatus == null) {
            List<CommonIdBean> parcelList = getDataManager().getPropertyParcel(list.get(pos).getPropertyId());

            parcelLoop:
            for (CommonIdBean parcelBean : parcelList) {
                String parcelExpenseStatus = getDataManager().getExpenseApplyStatus(parcelBean.id, parcelBean.recordKey);

                if (parcelExpenseStatus == null) {
                    List<CommonIdBean> animals = getDataManager().getAnimals(parcelBean.id, parcelBean.recordKey);

                    if (animals.isEmpty()) {
                        List<CommonIdBean> crops = getDataManager().getCrops(parcelBean.id, parcelBean.recordKey);

                        for (CommonIdBean cropBean : crops) {
                            String cropExpenseStatus = getDataManager().getExpenseApplyStatus(cropBean.id, cropBean.recordKey);
                            if (cropExpenseStatus != null) {
                                msg = getString(R.string.deleteExpenseMsg);
                                break parcelLoop;
                            }
                        }
                    } else {
                        for (CommonIdBean animalBean : animals) {
                            String animalExpenseStatus = getDataManager().getExpenseApplyStatus(animalBean.id, animalBean.recordKey);
                            if (animalExpenseStatus != null) {
                                msg = getString(R.string.deleteExpenseMsg);
                                break parcelLoop;
                            }
                        }
                    }
                } else {
                    msg = getString(R.string.deleteExpenseMsg);
                    break;
                }
            }

        } else {
            msg = getString(R.string.deleteExpenseMsg);
        }

        return msg;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hitApi();

        initAd(view);
    }

    private void initAd(View view) {
        AdView mAdView = view.findViewById(R.id.adView);
        mAdView.setVisibility(View.VISIBLE);
        AppUtils.setAdBanner(mAdView, frameAdd);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                AppUtils.setMargins(frameAdd, 0, 0, 15, 15);
                mAdView.setVisibility(View.GONE);
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when when the user is about to return
                // to the app after tapping on an ad.
            }
        });
    }

    public void hitApi() {
        if (isNetworkConnected(true)) {
            getBaseActivity().doSyncAppDB(false, flag -> {
                //getPropertyInfo();

                //get data from db
                getPropertyInfoFromDB();
            });
        }
        else getPropertyInfoFromDB();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                getBaseActivity().onBackPressed();
                break;

            case R.id.btnAddProperty:
                btnAddProperty.setClickable(false);
                AddEditPropertyFragment propertyFragment = new AddEditPropertyFragment();
                propertyFragment.setInstance(this, true);
                getBaseActivity().addFragment(propertyFragment, R.id.propertyFrame, true);

                new Handler().postDelayed(() -> btnAddProperty.setClickable(true), 2000);
                break;

            case R.id.frameAdd:
                //free user can add 1 property only
                if (!getDataManager().getUserInfo().getSubscriptionPlan().equalsIgnoreCase(AppConstants.PLAN_FREE)) {
                    frameAdd.setClickable(false);
                    propertyFragment = new AddEditPropertyFragment();
                    propertyFragment.setInstance(this, true);
                    getBaseActivity().addFragment(propertyFragment, R.id.propertyFrame, true);

                    new Handler().postDelayed(() -> frameAdd.setClickable(true), 2000);
                } else {
                    PlanUpgradeDialog.newInstance(getString(R.string.alert_free_property), getString(R.string.upgrade_to_premium_plans), () -> startActivity(new Intent(getBaseActivity(), PlanActivity.class))).show(getChildFragmentManager());
                }
                break;
        }
    }

    private void getPropertyInfo() {
        setLoading(true);
        getDataManager().doServerPropertyInfoApiCall(getDataManager().getHeader())
                .getAsResponse(new VolleyResponseListener() {
                    @Override
                    public void onVolleyResponse(String response) {
                        setLoading(false);
                        PropertyInfoResponse propertyInfoResponse = getDataManager().mGson.fromJson(response, PropertyInfoResponse.class);

                        if (propertyInfoResponse.getStatus().equals("success")) {
                            list.clear();
                            list.addAll(propertyInfoResponse.getPropertyInfo());

                            updateView();
                            if (!list.isEmpty()) {
                                frameAdd.setVisibility(View.VISIBLE);
                                tvNoRecord.setVisibility(View.GONE);
                                btnAddProperty.setVisibility(View.GONE);

                                //Todo manage local db add all data
                                updatePropertyDataDB(propertyInfoResponse);
                            }

                            adapter.notifyDataSetChanged();
                        } else {
                            updateView();
                            if (propertyInfoResponse.getMessage().equals(getString(R.string.no_record_found))) {
                                list.clear();
                                adapter.notifyDataSetChanged();
                            }
                            if (!propertyInfoResponse.getMessage().equals(getString(R.string.no_record_found)))
                                CommonUtils.showToast(getBaseActivity(), propertyInfoResponse.getMessage(), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        updateView();
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    private void updateView() {
        if (list.isEmpty()) {
            frameAdd.setVisibility(View.GONE);
            tvNoRecord.setVisibility(View.VISIBLE);
            btnAddProperty.setVisibility(View.VISIBLE);
        }
    }

    private void doDeletePropertyInfo(int pos) {
        setLoading(true);
        PropertyInfoResponse.PropertyInfoBean propertyInfoBean = list.get(pos);
        HashMap<String, String> mParams = new HashMap<>();
        mParams.put("propertyId", propertyInfoBean.getPropertyId());

        getDataManager().doServerDeletePropertyInfoApiCall(getDataManager().getHeader(), mParams)
                .getAsJsonObject(new VolleyJsonObjectListener() {

                    @Override
                    public void onVolleyJsonObject(JSONObject jsonObject) {
                        setLoading(false);
                        try {
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            if (status.equals("success")) {
                                //Todo delete property from local db Done
                                doDeletePropertyAndRainDataFromLocalDB(list.get(pos).getPropertyId());
                                list.remove(pos);
                                adapter.notifyItemRemoved(pos);
                                updateView();
                                CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                            } else
                                CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                        } catch (Exception e) {
                            CommonUtils.showToast(getBaseActivity(), getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onVolleyJsonException() {
                        setLoading(false);
                        CommonUtils.showToast(getBaseActivity(), getResources().getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onVolleyError(VolleyError error) {
                        setLoading(false);
                        VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                    }
                });
    }

    /*Local DB start here*/
    private void getPropertyInfoFromDB() {
        setLoading(true);
        new Thread(() -> {
            List<PropertyInfoResponse.PropertyInfoBean> localList = getDataManager().getPropertyList(getDataManager().getUserInfo().getUserId());
            setLoading(false);

            //update on ui thread
            Handler handler = new Handler(Looper.getMainLooper());
            if (!localList.isEmpty()) {

                handler.post(() -> {
                    frameAdd.setVisibility(View.VISIBLE);
                    tvNoRecord.setVisibility(View.GONE);
                    btnAddProperty.setVisibility(View.GONE);

                    list.clear();
                    list.addAll(localList);

                    adapter.notifyDataSetChanged();
                    updateView();
                });
            } else handler.post(() -> {
                list.clear();
                adapter.notifyDataSetChanged();
                updateView();
            });

        }).start();
    }

    private void doDeletePropertyAndRainDataFromLocalDB(String propertyId) {
        new Thread(() -> {
            getDataManager().deleteProperty(propertyId);
            getDataManager().deleteRainDataUsingPropertyId(propertyId);
            getDataManager().deleteParcelUsingPropertyId(propertyId);
            AppLogger.d("database", "property and rain data delete success");
        }).start();
    }

    private void updatePropertyDataDB(PropertyInfoResponse propertyInfoResponse) {
        new Thread(() -> {
            getDataManager().deleteAllProperty();
            getDataManager().deleteAllRainData();
            getDataManager().deleteAllParcel();

            AppLogger.d("database", "property, rain data and parcel table clear success");

            List<PropertyInfoResponse.PropertyInfoBean> propertyInfoBeanList = propertyInfoResponse.getPropertyInfo();

            List<Property> propertyList = new ArrayList<>();
            List<RainData> rainDataList = new ArrayList<>();
            List<Parcel> parcelList = new ArrayList<>();
            List<Animal> animalList = new ArrayList<>();

            for (PropertyInfoResponse.PropertyInfoBean property:propertyInfoBeanList) {

                Property propertyBean = new Property();
                propertyBean.setPropertyId(property.getPropertyId());
                propertyBean.setPropertyName(property.getPropertyName());
                propertyBean.setUser_id(property.getUser_id());
                propertyBean.setLocation(property.getLocation());
                propertyBean.setLongitude(property.getLongitude());
                propertyBean.setLatitude(property.getLatitude());
                propertyBean.setSize(property.getSize());
                propertyBean.setSizeUnite(property.getSizeUnite());
                propertyBean.setDescription(property.getDescription());
                propertyBean.setCrd(property.getCrd());
                propertyBean.setUpd(property.getUpd());
                propertyBean.setStatus(property.getStatus());
                propertyBean.setEvent(AppConstants.DB_EVENT_ADD);
                propertyBean.setDataSync(AppConstants.DB_SYNC_TRUE);
                propertyList.add(propertyBean);

                for (PropertyInfoResponse.PropertyInfoBean.RaindataListBean raindataListBean: property.getRaindataList()) {

                    RainData rainBean = new RainData();
                    rainBean.setRaindataId(raindataListBean.getRaindataId());
                    rainBean.setProperty_id(raindataListBean.getProperty_id());
                    rainBean.setFromDate(raindataListBean.getFromDate());
                    rainBean.setToDate(raindataListBean.getToDate());
                    rainBean.setQuantity(raindataListBean.getQuantity());
                    rainBean.setQuantityUnite(raindataListBean.getQuantityUnite());
                    rainBean.setCrd(raindataListBean.getCrd());
                    rainBean.setUpd(raindataListBean.getUpd());
                    rainBean.setStatus(raindataListBean.getStatus());
                    rainBean.setEvent(AppConstants.DB_EVENT_ADD);
                    rainBean.setDataSync(AppConstants.DB_SYNC_TRUE);
                    rainDataList.add(rainBean);
                }

                for (PropertyInfoResponse.PropertyInfoBean.ParcelListBean parcelListBean: property.getParcelList()) {

                    Parcel parcel = new Parcel();
                    parcel.setProperty_id(parcelListBean.getProperty_id());
                    parcel.setParcelId(parcelListBean.getParcelId());
                    parcel.setParcelName(parcelListBean.getParcelName());
                    parcel.setRecordKey(parcelListBean.getParcelRecordKey());
                    parcel.setCrd(parcelListBean.getCrd());
                    parcel.setUpd(parcelListBean.getUpd());
                    parcel.setStatus(parcelListBean.getStatus());
                    parcel.setEvent(AppConstants.DB_EVENT_ADD);
                    parcel.setDataSync(AppConstants.DB_SYNC_TRUE);
                    parcelList.add(parcel);
                }


                for (PropertyInfoResponse.PropertyInfoBean.AnimalListBean animalListBean: property.getAnimalList()) {

                    Animal animal = new Animal();
                    animal.setAnimalId(animalListBean.getAnimalId());
                    animal.setParcel_id(animalListBean.getParcel_id());
                    animal.setLotId(animalListBean.getLotId());
                    animal.setTagNumber(animalListBean.getTagNumber());
                    animal.setBornOrBuyDate(animalListBean.getBornOrBuyDate());
                    animal.setMother(animalListBean.getMother());
                    animal.setBoughtFrom(animalListBean.getBoughtFrom());
                    animal.setDescription(animalListBean.getDescription());
                    animal.setDismant(animalListBean.getDismant());
                    animal.setSaleDate(animalListBean.getSaleDate().isEmpty() ? null : CalenderUtils.getDateFormat(animalListBean.getSaleDate().contains("/") ? CalenderUtils.formatDate(animalListBean.getSaleDate(), AppConstants.TIMESTAMP_FORMAT, AppConstants.SERVER_TIMESTAMP_FORMAT) : animalListBean.getSaleDate(), AppConstants.SERVER_TIMESTAMP_FORMAT));
                    animal.setMarkingOrTagging(animalListBean.getMarkingOrTagging());
                    animal.setSalePrice(animalListBean.getSalePrice());
                    animal.setSaleQuantity(animalListBean.getSaleQuantity());
                    animal.setSaleTotalPrice(animalListBean.getSaleTotalPrice());
                    animal.setCostFrom(animalListBean.getCostFrom());
                    animal.setCostTo(animalListBean.getCostTo());
                    animal.setCostTotal(animalListBean.getCostTotal());
                    animal.setStatus(animalListBean.getStatus());
                    animal.setRecordKey(animalListBean.getRecordKey());
                    animal.setCrd(animalListBean.getCrd());
                    animal.setUpd(animalListBean.getUpd());
                    animal.setEvent(AppConstants.DB_EVENT_ADD);
                    animal.setDataSync(AppConstants.DB_SYNC_TRUE);
                    animalList.add(animal);
                }
            }
            getDataManager().insertAllProperty(propertyList);
            getDataManager().insertAllRainData(rainDataList);
            getDataManager().insertAllParcel(parcelList);
            getDataManager().insertAllAnimal(animalList);
            AppLogger.d("database", "Db Add property, animal, rainData, parcel done");
        }).start();
    }
    /*Local DB end here*/
}
