package com.agrinvestapp.ui.property.dialog;

import com.agrinvestapp.data.model.api.RainBeanResponse;

public interface RainDataCallback {
    void onRainDataAdd(RainBeanResponse.RainRecordListBean bean);
}
