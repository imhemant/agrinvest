package com.agrinvestapp.ui.auth;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.RegistrationResponse;
import com.agrinvestapp.data.model.db.UserInfoBean;
import com.agrinvestapp.ui.base.BaseFragment;
import com.agrinvestapp.ui.main.MainActivity;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.CommonUtils;
import com.agrinvestapp.volley.VolleyRequest;
import com.agrinvestapp.volley.VolleyResponseListener;
import com.android.volley.VolleyError;

import java.util.HashMap;

public class SignupFragment extends BaseFragment implements View.OnClickListener {

    private EditText etEmail, etName, etPwd, etCont_Num;
    private CheckBox checkBusiness;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_signup, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        checkBusiness = view.findViewById(R.id.checkBusiness);
        etEmail = view.findViewById(R.id.etEmail);
        etEmail.setFilters(new InputFilter[]{CommonUtils.noSpaceFilter()});

        etName = view.findViewById(R.id.etName);

        etPwd = view.findViewById(R.id.etPwd);
        etPwd.setFilters(new InputFilter[]{CommonUtils.noSpaceFilter()});

        etCont_Num = view.findViewById(R.id.etCont_Num);
        etCont_Num.setFilters(new InputFilter[]{CommonUtils.noSpaceFilter()});

        view.findViewById(R.id.btnSignUp).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSignUp:
                if (isNetworkConnected(false)) doSignUp();
                else
                    CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_internet), Toast.LENGTH_SHORT);
                break;
        }
    }

    private void doSignUp() {
        String emailTemp = etEmail.getText().toString().trim();
        String nameTemp = etName.getText().toString().trim();
        String pwdTemp = etPwd.getText().toString().trim();
        String contNumTemp = etCont_Num.getText().toString().trim();
        if (verifyCredential(emailTemp, nameTemp, pwdTemp, contNumTemp)) {
            setLoading(true);

            HashMap<String, String> mParameterMap = new HashMap<>();
            mParameterMap.put("name", nameTemp);
            mParameterMap.put("email", emailTemp);
            mParameterMap.put("password", pwdTemp);
            mParameterMap.put("profileImage", "");
            mParameterMap.put("contact_number", contNumTemp);
            mParameterMap.put("deviceType", AppConstants.DEVICE_TYPE);
            mParameterMap.put("deviceToken", "");
            if (checkBusiness.isChecked()) mParameterMap.put("userType", "business");

            getDataManager().doServerRegistrationApiCall(mParameterMap)
                    .getAsResponse(new VolleyResponseListener() {

                        @Override
                        public void onVolleyResponse(String response) {
                            setLoading(false);
                            RegistrationResponse registrationResponse = getDataManager().mGson.fromJson(response, RegistrationResponse.class);

                            if (registrationResponse.getStatus().equals("success")) {
                                UserInfoBean userInfoBean = new UserInfoBean();

                                userInfoBean.setUserId(registrationResponse.getUserDetail().getUserId());
                                userInfoBean.setName(registrationResponse.getUserDetail().getName());
                                userInfoBean.setEmail(registrationResponse.getUserDetail().getEmail());
                                userInfoBean.setPassword(registrationResponse.getUserDetail().getPassword());
                                userInfoBean.setLanguage(registrationResponse.getUserDetail().getLanguage());
                                userInfoBean.setContactNumber(registrationResponse.getUserDetail().getContactNumber());
                                userInfoBean.setLocation(registrationResponse.getUserDetail().getLocation());
                                userInfoBean.setLatitude(registrationResponse.getUserDetail().getLatitude());
                                userInfoBean.setLongitude(registrationResponse.getUserDetail().getLongitude());
                                userInfoBean.setDeviceType(registrationResponse.getUserDetail().getDeviceType());
                                userInfoBean.setDeviceToken(registrationResponse.getUserDetail().getDeviceToken());
                                userInfoBean.setSocialId(registrationResponse.getUserDetail().getSocialId());
                                userInfoBean.setSocialType(registrationResponse.getUserDetail().getSocialType());
                                userInfoBean.setAuthToken(registrationResponse.getUserDetail().getAuthToken());
                                userInfoBean.setStatus(registrationResponse.getUserDetail().getStatus());
                                userInfoBean.setUserType(registrationResponse.getUserDetail().getUserType());
                                userInfoBean.setProfileImage(registrationResponse.getUserDetail().getProfileImage());
                                userInfoBean.setSubscriptionPlan(registrationResponse.getUserDetail().getSubscriptionPlan());
                                userInfoBean.setSubscriptionSaleId(registrationResponse.getUserDetail().getSubscriptionSaleId());
                                userInfoBean.setLocalDataExistForSync(false);
                                userInfoBean.setSyncTimer("");
                                userInfoBean.setAppOnline(true);

                                //Session data
                                getDataManager().setUserInfo(userInfoBean);
                                //Local db
                                new Thread(() -> getDataManager().insertUser(userInfoBean)).start();
                                startActivity(new Intent(getBaseActivity(), MainActivity.class));
                                getBaseActivity().finish();
                            } else {
                                CommonUtils.showToast(getBaseActivity(), registrationResponse.getMessage(), Toast.LENGTH_SHORT);
                            }
                        }

                        @Override
                        public void onVolleyError(VolleyError error) {
                            setLoading(false);
                            VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                        }
                    });
        }
    }

    private boolean verifyCredential(String emailTemp, String nameTemp, String pwdTemp, String contNumTemp) {
        if (emailTemp.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_email), Toast.LENGTH_SHORT);
            return false;
        } else if (!CommonUtils.isEmailValid(emailTemp)) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_valid_email), Toast.LENGTH_SHORT);
            return false;
        } else if (nameTemp.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_name), Toast.LENGTH_SHORT);
            return false;
        } else if (!(nameTemp.length() >= 3)) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_name_length), Toast.LENGTH_SHORT);
            return false;
        } else if (contNumTemp.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_number), Toast.LENGTH_SHORT);
            return false;
        } else if (!(contNumTemp.length() >= 7 && contNumTemp.length() <= 12)) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_cont_length), Toast.LENGTH_SHORT);
            return false;
        } else if (pwdTemp.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_pwd), Toast.LENGTH_SHORT);
            return false;
        } else if (!(pwdTemp.length() >= 6 && pwdTemp.length() <= 16)) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_length_pwd), Toast.LENGTH_SHORT);
            return false;
        } else return true;
    }
}
