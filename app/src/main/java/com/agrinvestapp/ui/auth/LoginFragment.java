package com.agrinvestapp.ui.auth;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.data.model.api.LoginResponse;
import com.agrinvestapp.data.model.db.UserInfoBean;
import com.agrinvestapp.ui.auth.dialog.ForgotPwdDialog;
import com.agrinvestapp.ui.base.BaseFragment;
import com.agrinvestapp.ui.main.MainActivity;
import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.CommonUtils;
import com.agrinvestapp.volley.VolleyRequest;
import com.agrinvestapp.volley.VolleyResponseListener;
import com.android.volley.VolleyError;

import java.util.HashMap;

public class LoginFragment extends BaseFragment implements View.OnClickListener {

    private EditText etEmail, etPwd;
    private CheckBox checkLogIn;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        checkLogIn = view.findViewById(R.id.checkLogIn);

        etEmail = view.findViewById(R.id.etEmail);
        etEmail.setFilters(new InputFilter[]{CommonUtils.noSpaceFilter()});

        etPwd = view.findViewById(R.id.etPwd);
        etPwd.setFilters(new InputFilter[]{CommonUtils.noSpaceFilter()});

        view.findViewById(R.id.btnLogIn).setOnClickListener(this);
        view.findViewById(R.id.tvForgotPwd).setOnClickListener(this);

        String[] remCre = getDataManager().getRememberMe();

        if (!remCre[0].isEmpty()) {
            etEmail.setText(remCre[0]);
            etPwd.setText(remCre[1]);
            checkLogIn.setChecked(true);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogIn:
                if (isNetworkConnected(false)) doLogin();
                else
                    CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_internet), Toast.LENGTH_SHORT);
                break;

            case R.id.tvForgotPwd:
                ForgotPwdDialog forgotPwdDialog = new ForgotPwdDialog();
                forgotPwdDialog.show(getChildFragmentManager());
                break;
        }
    }

    private void doLogin() {
        String emailTemp = etEmail.getText().toString().trim();
        String pwdTemp = etPwd.getText().toString().trim();
        if (verifyCredential(emailTemp, pwdTemp)) {
            setLoading(true);

            HashMap<String, String> mParameterMap = new HashMap<>();
            mParameterMap.put("email", emailTemp);
            mParameterMap.put("password", pwdTemp);
            mParameterMap.put("deviceToken", "");
            mParameterMap.put("deviceType", AppConstants.DEVICE_TYPE);

            getDataManager().doServerLoginApiCall(mParameterMap)
                    .getAsResponse(new VolleyResponseListener() {
                        @Override
                        public void onVolleyResponse(String response) {
                            setLoading(false);
                            LoginResponse loginResponse = getDataManager().mGson.fromJson(response, LoginResponse.class);

                            if (loginResponse.getStatus().equals("success")) {
                                UserInfoBean userInfoBean = new UserInfoBean();

                                userInfoBean.setUserId(loginResponse.getUserDetail().getUserId());
                                userInfoBean.setName(loginResponse.getUserDetail().getName());
                                userInfoBean.setEmail(loginResponse.getUserDetail().getEmail());
                                userInfoBean.setPassword(loginResponse.getUserDetail().getPassword());
                                userInfoBean.setLanguage(loginResponse.getUserDetail().getLanguage());
                                userInfoBean.setContactNumber(loginResponse.getUserDetail().getContactNumber());
                                userInfoBean.setLocation(loginResponse.getUserDetail().getLocation());
                                userInfoBean.setLatitude(loginResponse.getUserDetail().getLatitude());
                                userInfoBean.setLongitude(loginResponse.getUserDetail().getLongitude());
                                userInfoBean.setDeviceType(loginResponse.getUserDetail().getDeviceType());
                                userInfoBean.setDeviceToken(loginResponse.getUserDetail().getDeviceToken());
                                userInfoBean.setSocialId(loginResponse.getUserDetail().getSocialId());
                                userInfoBean.setSocialType(loginResponse.getUserDetail().getSocialType());
                                userInfoBean.setAuthToken(loginResponse.getUserDetail().getAuthToken());
                                userInfoBean.setStatus(loginResponse.getUserDetail().getStatus());
                                userInfoBean.setUserType(loginResponse.getUserDetail().getUserType());
                                userInfoBean.setProfileImage(loginResponse.getUserDetail().getProfileImage());
                                userInfoBean.setSubscriptionPlan(loginResponse.getUserDetail().getSubscriptionPlan());
                                userInfoBean.setSubscriptionSaleId(loginResponse.getUserDetail().getSubscriptionSaleId());
                                userInfoBean.setLocalDataExistForSync(false);
                                userInfoBean.setSyncTimer("");
                                userInfoBean.setAppOnline(true);

                                if (checkLogIn.isChecked())
                                    getDataManager().setRememberMe(emailTemp, pwdTemp);
                                else getDataManager().setRememberMe("", "");

                                //Session data
                                getDataManager().setUserInfo(userInfoBean);
                                //Local db
                                new Thread(() -> {
                                    if (getDataManager().getExistingUserIdFromDB()!=null &&
                                            getDataManager().getExistingUserIdFromDB().equals(userInfoBean.getUserId()))
                                        getDataManager().insertUser(userInfoBean);
                                    else{
                                        getDataManager().clearAllTable();
                                        getDataManager().insertUser(userInfoBean);
                                    }
                                }).start();
                                startActivity(new Intent(getBaseActivity(), MainActivity.class));
                                getBaseActivity().finish();
                            } else {
                                CommonUtils.showToast(getBaseActivity(), loginResponse.getMessage(), Toast.LENGTH_SHORT);
                            }
                        }

                        @Override
                        public void onVolleyError(VolleyError error) {
                            setLoading(false);
                            VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                        }
                    });
        }
    }

    private boolean verifyCredential(String emailTemp, String pwdTemp) {
        if (emailTemp.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_email), Toast.LENGTH_SHORT);
            return false;
        } else if (!CommonUtils.isEmailValid(emailTemp)) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_valid_email), Toast.LENGTH_SHORT);
            return false;
        } else if (pwdTemp.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_pwd), Toast.LENGTH_SHORT);
            return false;
        } else if (!(pwdTemp.length() >= 6 && pwdTemp.length() <= 16)) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_length_pwd), Toast.LENGTH_SHORT);
            return false;
        } else return true;
    }
}
