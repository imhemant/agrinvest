package com.agrinvestapp.ui.auth.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.ui.base.BaseDialog;
import com.agrinvestapp.utils.CommonUtils;
import com.agrinvestapp.volley.VolleyJsonObjectListener;
import com.agrinvestapp.volley.VolleyRequest;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.HashMap;


public class ForgotPwdDialog extends BaseDialog implements View.OnClickListener {

    private static final String TAG = ForgotPwdDialog.class.getSimpleName();

    private EditText etEmail;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_forgot_pwd, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.btnSubmit).setOnClickListener(this);
        view.findViewById(R.id.btnCancel).setOnClickListener(this);

        etEmail = view.findViewById(R.id.etEmail);
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    public void onClick(View v) {
        hideKeyboard();
        switch (v.getId()) {
            case R.id.btnSubmit:
                if (isNetworkConnected(false)) {
                    doForgotPwdApiCall();
                } else
                    CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_internet), Toast.LENGTH_SHORT);
                break;

            case R.id.btnCancel:
                dismissDialog(TAG);
                break;

        }
    }

    private boolean verifyCredential(String emailTemp) {
        if (emailTemp.isEmpty()) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_enter_email), Toast.LENGTH_SHORT);
            return false;
        } else if (!CommonUtils.isEmailValid(emailTemp)) {
            CommonUtils.showToast(getBaseActivity(), getString(R.string.alert_valid_email), Toast.LENGTH_SHORT);
            return false;
        } else return true;
    }

    private void doForgotPwdApiCall() {
        String emailTemp = etEmail.getText().toString().trim();
        if (verifyCredential(emailTemp)) {
            setLoading(true);

            HashMap<String, String> mParameterMap = new HashMap<>();
            mParameterMap.put("email", emailTemp);

            getDataManager().doServerForgotPwdApiCall(mParameterMap)
                    .getAsJsonObject(new VolleyJsonObjectListener() {
                        @Override
                        public void onVolleyJsonObject(JSONObject jsonObject) {
                            setLoading(false);

                            try {
                                String status = jsonObject.getString("status");
                                String message = jsonObject.getString("message");

                                if (status.equals("success")) {
                                    CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                                } else {
                                    CommonUtils.showToast(getBaseActivity(), message, Toast.LENGTH_SHORT);
                                }
                            } catch (Exception e) {
                                CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                            }
                            dismissDialog(TAG);
                        }

                        @Override
                        public void onVolleyJsonException() {
                            setLoading(false);
                            CommonUtils.showToast(getBaseActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT);
                            dismissDialog(TAG);
                        }

                        @Override
                        public void onVolleyError(VolleyError error) {
                            setLoading(false);
                            VolleyRequest.volleyErrorHandle(getBaseActivity(), error);
                            dismissDialog(TAG);
                        }
                    });
        }
    }

}
