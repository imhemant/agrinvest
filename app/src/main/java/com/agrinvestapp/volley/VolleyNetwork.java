package com.agrinvestapp.volley;

public class VolleyNetwork {

    /**
     * private constructor to prevent instantiation of this class
     */
    private VolleyNetwork() {
    }

    /**
     * Method to make GET request
     *
     * @param url The url on which request is to be made
     * @return The GetRequestBuilder
     */
    public static VolleyRequest.GetRequestBuilder get(String url) {
        return new VolleyRequest.GetRequestBuilder(url);
    }

    /**
     * Method to make POST request
     *
     * @param url The url on which request is to be made
     * @return The PostRequestBuilder
     */
    public static VolleyRequest.PostRequestBuilder post(String url) {
        return new VolleyRequest.PostRequestBuilder(url);
    }

    /**
     * Method to make MULTIPARTPOST request
     *
     * @param url The url on which request is to be made
     * @return The PostRequestBuilder
     */
    //Under development
    public static VolleyRequest.MultipartPostRequestBuilder postMultipart(String url) {
        return new VolleyRequest.MultipartPostRequestBuilder(url);
    }


}
