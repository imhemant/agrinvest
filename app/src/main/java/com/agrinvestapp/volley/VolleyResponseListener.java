package com.agrinvestapp.volley;

import com.android.volley.VolleyError;

/**
 * Created by hemant.
 * Date: 31/8/18
 * Time: 7:29 PM
 */

public interface VolleyResponseListener {

    void onVolleyResponse(String response);

    void onVolleyError(VolleyError error);
}
