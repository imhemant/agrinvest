package com.agrinvestapp.volley;

/*
 * Created by hemant
 * Date: 13/12/17.
 */


import android.support.annotation.NonNull;

class ServerResponseCode {


    @NonNull
    static String getMessageCode(int code) {
        String valueOfMessage = "";
        switch (code) {

            case 101:
                valueOfMessage = "Continue";
                break;
            case 200:
                valueOfMessage = "Ok";
                break;
            case 202:
                valueOfMessage = "Accepted";
                break;
            case 203:
                valueOfMessage = "Non-Authoritative Information";
                break;
            case 204:
                valueOfMessage = "No Content";
                break;
            case 300:
                valueOfMessage = "Multiple Choices";
                break;
            case 302:
                valueOfMessage = "Found";
                break;
            case 304:
                valueOfMessage = "Not Modified";
                break;
            case 305:
                valueOfMessage = "Use Proxy";
                break;
            case 400:
                valueOfMessage = "Your session is expired please login again";
                break;
            case 404:
                valueOfMessage = "Not Found";
                break;
            case 502:
                valueOfMessage = "Bad Gateway";
                break;
            case 503:
                valueOfMessage = "Service Unavailable";
                break;
            case 504:
                valueOfMessage = "Gateway Timeout";
                break;
            case 505:
                valueOfMessage = "HTTP Version Not Supported";
                break;

        }
        return valueOfMessage;
    }
}