package com.agrinvestapp.volley;

import com.android.volley.VolleyError;

import org.json.JSONArray;

/**
 * Created by hemant.
 * Date: 31/8/18
 * Time: 7:29 PM
 */

public interface VolleyJsonArrayListener {

    void onVolleyJsonArray(JSONArray jsonArray);

    void onVolleyJsonException();

    void onVolleyError(VolleyError error);
}
