package com.agrinvestapp.volley;

import com.android.volley.VolleyError;

import org.json.JSONObject;

/**
 * Created by hemant.
 * Date: 31/8/18
 * Time: 7:29 PM
 */

public interface VolleyJsonObjectListener {

    void onVolleyJsonObject(JSONObject jsonObject);

    void onVolleyJsonException();

    void onVolleyError(VolleyError error);
}
