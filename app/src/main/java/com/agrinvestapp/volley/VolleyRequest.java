package com.agrinvestapp.volley;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.agrinvestapp.Agrinvest;
import com.agrinvestapp.R;
import com.agrinvestapp.utils.AppLogger;
import com.agrinvestapp.utils.CalenderUtils;
import com.agrinvestapp.utils.CommonUtils;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hemant
 * Date: 5/4/18
 */

public class VolleyRequest {

    private final static String TAG = VolleyRequest.class.getSimpleName();

    private int requestType;
    private int mMethod;
    private String mUrl;
    private String mTag;
    private int mRetryTime;
    private HashMap<String, String> mHeadersMap;
    private HashMap<String, String> mParameterMap;
    private HashMap<String, VolleyMultipartRequest.DataPart> mDataParameterMap;

    private RequestQueue mRequestQueue;

    public VolleyRequest(VolleyRequest.GetRequestBuilder builder) {
        this.requestType = RequestType.SINGLEPART;
        this.mRetryTime = builder.mRetryTime;
        this.mMethod = builder.mMethod;
        this.mUrl = builder.mUrl;
        this.mTag = builder.mTag;
        this.mHeadersMap = builder.mHeadersMap;
    }

    public VolleyRequest(VolleyRequest.PostRequestBuilder builder) {
        this.requestType = RequestType.SINGLEPART;
        this.mMethod = builder.mMethod;
        this.mRetryTime = builder.mRetryTime;
        this.mUrl = builder.mUrl;
        this.mTag = builder.mTag;
        this.mHeadersMap = builder.mHeadersMap;
        this.mParameterMap = builder.mParameterMap;
    }

    public VolleyRequest(VolleyRequest.MultipartPostRequestBuilder builder) {
        this.requestType = RequestType.MULTIPART;
        this.mMethod = builder.mMethod;
        this.mRetryTime = builder.mRetryTime;
        this.mUrl = builder.mUrl;
        this.mTag = builder.mTag;
        this.mHeadersMap = builder.mHeadersMap;
        this.mParameterMap = builder.mParameterMap;
        this.mDataParameterMap = builder.mDataParameterMap;
    }

    @NonNull
    public static String volleyErrorHandle(@NonNull Activity activity, VolleyError error) {
        NetworkResponse networkResponse = error.networkResponse;
        String errorMessage = "";
        if (networkResponse == null) {
            if (error.getClass().equals(TimeoutError.class)) {
                errorMessage = activity.getString(R.string.alert_volley_timeout);
                return errorMessage;
            } else if (error.getClass().equals(NoConnectionError.class)) {
                errorMessage = activity.getString(R.string.alert_volley_server_error);
                return errorMessage;
            } else return errorMessage;
        } else {
            String result = new String(networkResponse.data);
            try {
                JSONObject response = new JSONObject(result);
                String status = response.getString("responseCode");
                String message = response.getString("message");
                String userStatus = response.getString("user_status");

                AppLogger.e("Error Status", "" + status);
                AppLogger.e("Error Message", message);

                if (status.equals("300")) {
                    //for inactive "user_status": "0"
                    //for auth token expire  "user_status": "2"

                    if (userStatus.equals("0")) {
                        CommonUtils.showCustomAlert(activity, activity.getResources().getString(R.string.alert), message);
                    } else {
                        CommonUtils.showLogoutAlert(activity, activity.getResources().getString(R.string.session_expired), activity.getResources().getString(R.string.your_session_is_expired_please_login_again));
                    }
                    Agrinvest.getDataManager().logout(activity);

                } else if (networkResponse.statusCode == 404) {
                    errorMessage = "Resource not found";
                    CommonUtils.showCustomAlert(activity, "Alert", errorMessage);
                } else if (networkResponse.statusCode == 500) {
                    errorMessage = message + "Oops! Something went wrong";
                    CommonUtils.showCustomAlert(activity, "Alert", errorMessage);
                } else {
                    errorMessage = ServerResponseCode.getMessageCode(networkResponse.statusCode);
                    CommonUtils.showCustomAlert(activity, "Alert", errorMessage);
                }

            } catch (JSONException e) {
                CommonUtils.showCustomAlert(activity, "Alert", activity.getResources().getString(R.string.something_wrong));
                e.printStackTrace();
            }
        }
        return errorMessage;
    }

    public void getAsResponse(VolleyResponseListener listener) {

        switch (requestType){
            case RequestType.SINGLEPART:
                StringRequest stringRequest = new StringRequest(mMethod, mUrl,
                        response -> {
                            AppLogger.e(mTag, "onResponse: " + response);
                            listener.onVolleyResponse(response);
                        },
                        listener::onVolleyError) {

                    @NonNull
                    @Override
                    public Map<String, String> getHeaders() {
                        return mHeadersMap;
                    }

                    @NonNull
                    @Override
                    protected Map<String, String> getParams() {
                        return mParameterMap;
                    }
                };

                addToRequestQueue(stringRequest, mTag);
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                        mRetryTime, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                break;

            case RequestType.MULTIPART:
                VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(mMethod, mUrl,
                        response -> {
                            String resultResponse = new String(response.data);
                            listener.onVolleyResponse(resultResponse);

                        }, listener::onVolleyError) {
                    @NonNull
                    @Override
                    public Map<String, String> getHeaders() {
                        return mHeadersMap;
                    }

                    @NonNull
                    @Override
                    protected Map<String, String> getParams() {
                        return mParameterMap;
                    }

                    @NonNull
                    @Override
                    protected Map<String, DataPart> getByteData() {
                        return mDataParameterMap;
                    }
                };
                addToRequestQueue(multipartRequest, mTag);
                multipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                        mRetryTime, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                break;
        }

    }

    public void getAsJsonObject(VolleyJsonObjectListener listener) {

        switch (requestType){
            case RequestType.SINGLEPART:
                StringRequest stringRequest = new StringRequest(mMethod, mUrl,
                        response -> {
                            AppLogger.e(mTag, "onResponse: " + response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                listener.onVolleyJsonObject(jsonObject);
                            } catch (Exception e) {
                                e.printStackTrace();
                                listener.onVolleyJsonException();
                            }
                        },
                        listener::onVolleyError) {

                    @NonNull
                    @Override
                    public Map<String, String> getHeaders() {
                        return mHeadersMap;
                    }

                    @NonNull
                    @Override
                    protected Map<String, String> getParams() {
                        return mParameterMap;
                    }
                };

                addToRequestQueue(stringRequest, mTag);
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                        mRetryTime, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                break;

            case RequestType.MULTIPART:
                VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(mMethod, mUrl,
                        response -> {
                            String resultResponse = new String(response.data);
                            try {
                                JSONObject jsonObject = new JSONObject(resultResponse);
                                listener.onVolleyJsonObject(jsonObject);
                            } catch (Exception e) {
                                e.printStackTrace();
                                listener.onVolleyJsonException();
                            }

                        }, listener::onVolleyError) {
                    @NonNull
                    @Override
                    public Map<String, String> getHeaders() {
                        return mHeadersMap;
                    }

                    @NonNull
                    @Override
                    protected Map<String, String> getParams() {
                        return mParameterMap;
                    }

                    @NonNull
                    @Override
                    protected Map<String, DataPart> getByteData() {
                        return mDataParameterMap;
                    }
                };
                addToRequestQueue(multipartRequest, mTag);
                multipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                        mRetryTime, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                break;

        }
    }

    public void getAsJsonArray(VolleyJsonArrayListener listener) {
        switch (requestType){
            case RequestType.SINGLEPART:
                StringRequest stringRequest = new StringRequest(mMethod, mUrl,
                        response -> {
                            AppLogger.e(mTag, "onResponse: " + response);
                            try {
                                JSONArray jsonArray = new JSONArray(response);
                                listener.onVolleyJsonArray(jsonArray);
                            } catch (Exception e) {
                                e.printStackTrace();
                                listener.onVolleyJsonException();
                            }
                        },
                        listener::onVolleyError) {

                    @NonNull
                    @Override
                    public Map<String, String> getHeaders() {
                        return mHeadersMap;
                    }

                    @NonNull
                    @Override
                    protected Map<String, String> getParams() {
                        return mParameterMap;
                    }
                };

                addToRequestQueue(stringRequest, mTag);
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                        mRetryTime, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                break;

            case RequestType.MULTIPART:
                VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(mMethod, mUrl,
                        response -> {
                            String resultResponse = new String(response.data);
                            try {
                                JSONArray jsonArray = new JSONArray(resultResponse);
                                listener.onVolleyJsonArray(jsonArray);
                            } catch (Exception e) {
                                e.printStackTrace();
                                listener.onVolleyJsonException();
                            }

                        }, listener::onVolleyError) {
                    @NonNull
                    @Override
                    public Map<String, String> getHeaders() {
                        return mHeadersMap;
                    }

                    @NonNull
                    @Override
                    protected Map<String, String> getParams() {
                        return mParameterMap;
                    }

                    @NonNull
                    @Override
                    protected Map<String, DataPart> getByteData() {
                        return mDataParameterMap;
                    }
                };
                addToRequestQueue(multipartRequest, mTag);
                multipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                        mRetryTime, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                break;

        }
    }

    /**
     * Get current request queue.
     *
     * @return RequestQueue
     */
    private RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(Agrinvest.getInstance().getApplicationContext().getApplicationContext());
        }
        return mRequestQueue;
    }

    private <T> void addToRequestQueue(@NonNull Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(@NonNull Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(@NonNull Object tag) {
        if (mRequestQueue != null) {
            AppLogger.e(TAG, "cancel api");
            mRequestQueue.cancelAll(tag);
        }
    }

    public static class GetRequestBuilder<T extends GetRequestBuilder> implements VolleyRequestBuilder {

        private int mMethod = Request.Method.GET;
        private String mUrl;
        private String mTag;
        private int mRetryTime = 30000;
        private HashMap<String, String> mHeadersMap = new HashMap<>();
        private HashMap<String, String> mParameterMap = new HashMap<>();

        public GetRequestBuilder(String url) {
            this.mUrl = url;
            this.mMethod = Request.Method.GET;
        }

        public GetRequestBuilder(String url, int method) {
            this.mUrl = url;
            this.mMethod = method;
        }

        @Override
        public T setTag(String tag) {
            mTag = tag;
            return (T) this;
        }

        @Override
        public T addRetryTime(int mRetryTime) {
            this.mRetryTime = mRetryTime;
            return (T) this;
        }

        @Override
        public T addHeaders(String key, String value) {
            assert key != null && value != null;
            mHeadersMap.put(key, value);
            return (T) this;
        }

        @Override
        public T addHeaders(Map<String, String> headerMap) {
            if (headerMap != null) {
                for (HashMap.Entry<String, String> entry : headerMap.entrySet()) {
                    addHeaders(entry.getKey(), entry.getValue());
                }
            }
            return (T) this;
        }

        @Override
        public T addParameter(String key, String value) {
            assert key != null && value != null;
            mParameterMap.put(key, value);
            return (T) this;
        }


        @Override
        public T addParameter(Map<String, String> parameterMap) {
            if (parameterMap != null) {
                for (HashMap.Entry<String, String> entry : parameterMap.entrySet()) {
                    addParameter(entry.getKey(), entry.getValue());
                }
            }
            return (T) this;
        }

        public VolleyRequest build() {
            return new VolleyRequest(this);
        }
    }

    public static class PostRequestBuilder<T extends PostRequestBuilder> implements VolleyRequestBuilder {

        private int mMethod = Request.Method.POST;
        private String mUrl;
        private String mTag;
        private HashMap<String, String> mHeadersMap = new HashMap<>();
        private HashMap<String, String> mParameterMap = new HashMap<>();
        private int mRetryTime = 30000;

        public PostRequestBuilder(String url) {
            this.mUrl = url;
            this.mMethod = Request.Method.POST;
        }

        public PostRequestBuilder(String url, int method) {
            this.mUrl = url;
            this.mMethod = method;
        }

        @Override
        public T setTag(String tag) {
            mTag = tag;
            return (T) this;
        }

        @Override
        public T addRetryTime(int mRetryTime) {
            this.mRetryTime = mRetryTime;
            return (T) this;
        }

        @Override
        public T addParameter(String key, String value) {
            assert key != null && value != null;
            mParameterMap.put(key, value);
            return (T) this;
        }

        @Override
        public T addParameter(Map<String, String> parameterMap) {
            if (parameterMap != null) {
                for (HashMap.Entry<String, String> entry : parameterMap.entrySet()) {
                    addParameter(entry.getKey(), entry.getValue());
                }
            }
            return (T) this;
        }


        @Override
        public T addHeaders(String key, String value) {
            assert key != null && value != null;
            mHeadersMap.put(key, value);
            return (T) this;
        }

        @Override
        public T addHeaders(Map<String, String> headerMap) {
            if (headerMap != null) {
                for (HashMap.Entry<String, String> entry : headerMap.entrySet()) {
                    addHeaders(entry.getKey(), entry.getValue());
                }
            }
            return (T) this;
        }

        public VolleyRequest build() {
            return new VolleyRequest(this);
        }
    }

    public static class MultipartPostRequestBuilder<T extends MultipartPostRequestBuilder> implements VolleyRequestBuilder {

        private int mMethod = Request.Method.POST;
        private String mUrl;
        private String mTag;
        private HashMap<String, String> mHeadersMap = new HashMap<>();
        private HashMap<String, String> mParameterMap = new HashMap<>();
        private HashMap<String, VolleyMultipartRequest.DataPart> mDataParameterMap = new HashMap<>();
        private int mRetryTime = 30000;

        public MultipartPostRequestBuilder(String url) {
            this.mUrl = url;
            this.mMethod = Request.Method.POST;
        }

        public MultipartPostRequestBuilder(String url, int method) {
            this.mUrl = url;
            this.mMethod = method;
        }

        @Override
        public T setTag(String tag) {
            mTag = tag;
            return (T) this;
        }

        @Override
        public T addRetryTime(int mRetryTime) {
            this.mRetryTime = mRetryTime;
            return (T) this;
        }

        @Override
        public T addParameter(String key, String value) {
            assert key != null && value != null;
            mParameterMap.put(key, value);
            return (T) this;
        }

        @Override
        public T addParameter(Map<String, String> parameterMap) {
            if (parameterMap != null) {
                for (HashMap.Entry<String, String> entry : parameterMap.entrySet()) {
                    addParameter(entry.getKey(), entry.getValue());
                }
            }
            return (T) this;
        }

        public T addDataParameter(String key, VolleyMultipartRequest.DataPart value) {
            assert key != null && value != null;
            mDataParameterMap.put(key, value);
            return (T) this;
        }

        public T addDataParameter(Map<String, VolleyMultipartRequest.DataPart> parameterMap) {
            if (parameterMap != null) {
                for (HashMap.Entry<String, VolleyMultipartRequest.DataPart> entry : parameterMap.entrySet()) {
                    addDataParameter(entry.getKey(), entry.getValue());
                }
            }
            return (T) this;
        }

        @Override
        public T addHeaders(String key, String value) {
            assert key != null && value != null;
            mHeadersMap.put(key, value);
            return (T) this;
        }

        @Override
        public T addHeaders(Map<String, String> headerMap) {
            if (headerMap != null) {
                for (HashMap.Entry<String, String> entry : headerMap.entrySet()) {
                    addHeaders(entry.getKey(), entry.getValue());
                }
            }
            return (T) this;
        }

        public VolleyRequest build() {
            return new VolleyRequest(this);
        }
    }
}
