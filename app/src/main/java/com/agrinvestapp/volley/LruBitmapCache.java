package com.agrinvestapp.volley;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.util.DisplayMetrics;
import android.util.LruCache;

import com.android.volley.toolbox.ImageLoader;


/**
 * Created by hemant
 * Date: 13/12/17
 */


public class LruBitmapCache extends LruCache<String, Bitmap>
        implements ImageLoader.ImageCache {

    private LruBitmapCache(int maxSize) {
        super(maxSize);
    }

    LruBitmapCache(@NonNull Context ctx) {
        this(getCacheSize(ctx));
    }

    // Returns a cache size equal to approximately three screens worth of images.
    private static int getCacheSize(@NonNull Context ctx) {
        final DisplayMetrics displayMetrics = ctx.getResources().
                getDisplayMetrics();
        final int screenWidth = displayMetrics.widthPixels;
        final int screenHeight = displayMetrics.heightPixels;
        // 4 bytes per pixel
        final int screenBytes = screenWidth * screenHeight * 4;

        return screenBytes * 3;
    }

    @Override
    protected int sizeOf(String key, @NonNull Bitmap value) {
        return value.getRowBytes() * value.getHeight();
    }

    @Override
    public Bitmap getBitmap(String url) {
        return get(url);
    }

    @Override
    public void putBitmap(String url, Bitmap bitmap) {
        put(url, bitmap);
    }
}