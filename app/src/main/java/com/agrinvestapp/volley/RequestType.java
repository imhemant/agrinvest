package com.agrinvestapp.volley;

/**
 * Created by hemant
 * Date: 31/10/18.
 */

public interface RequestType {
    int SINGLEPART = 0;
    int MULTIPART=1;
}
