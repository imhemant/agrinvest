package com.agrinvestapp.volley;

import java.util.Map;

public interface VolleyRequestBuilder {

    VolleyRequestBuilder addHeaders(String key, String value);

    VolleyRequestBuilder addHeaders(Map<String, String> headerMap);

    VolleyRequestBuilder addParameter(String key, String value);

    VolleyRequestBuilder addParameter(Map<String, String> paramMap);

    VolleyRequestBuilder setTag(String tag);

    VolleyRequestBuilder addRetryTime(int retryTime);

}
