package com.agrinvestapp.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class DateChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        //Log.e("Test0", "ACTION_DATE_CHANGED received");
        String action = intent.getAction();
        if (action != null && action.equals(Intent.ACTION_DATE_CHANGED)) {
            Log.e("Test", "ACTION_DATE_CHANGED received");
            //Toast.makeText(context, "ACTION_DATE_CHANGED received", Toast.LENGTH_SHORT).show();
        }
    }
}
