package com.agrinvestapp.utils.pagination;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.agrinvestapp.R;


/**
 * Created by hemant
 * Date: 12/05/18
 */

public final class FooterLoader extends RecyclerView.ViewHolder {

    public ProgressBar mProgressBar;

    public FooterLoader(@NonNull View itemView) {
        super(itemView);
        mProgressBar = itemView.findViewById(R.id.progressbar);
    }
}
