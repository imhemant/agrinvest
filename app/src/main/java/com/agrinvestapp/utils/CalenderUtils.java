package com.agrinvestapp.utils;

import android.support.annotation.NonNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by hemant.
 * Date: 30/8/18
 * Time: 2:34 PM
 */

public final class CalenderUtils {

    private CalenderUtils() {
        // This class is not publicly instantiable
    }

    public static String getTimestamp(String format) {
        return new SimpleDateFormat(format, Locale.US).format(new Date());
    }

    public static String getTimestamp() {
        return String.valueOf(new Date().getTime());
    }

    public static String format12HourTime(String time, @NonNull String pFormat, @NonNull String dFormat) {
        try {
            SimpleDateFormat parseFormat = new SimpleDateFormat(pFormat, Locale.US);
            SimpleDateFormat displayFormat = new SimpleDateFormat(dFormat, Locale.US);
            Date dTime = parseFormat.parse(time);
            return displayFormat.format(dTime);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String formatDate(String date, @NonNull String pFormat, @NonNull String dFormat) {
        try {
            SimpleDateFormat parseFormat = new SimpleDateFormat(pFormat, Locale.getDefault());
            SimpleDateFormat displayFormat = new SimpleDateFormat(dFormat, Locale.getDefault());
            Date dTime = parseFormat.parse(date);
            return displayFormat.format(dTime);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static Date getDateFormat(String date, @NonNull String format) {
        try {
            SimpleDateFormat parseFormat = new SimpleDateFormat(format, Locale.getDefault());

            return parseFormat.parse(date);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Long getTimerDifference(String timer, String myTime) {
        SimpleDateFormat formatter = new SimpleDateFormat(AppConstants.DB_TIMESTAMP_FORMAT, Locale.getDefault());

        try {
            Date startDate = formatter.parse(timer);

            Date endDate = formatter.parse(myTime);

            long diffInMilliSec = endDate.getTime() - startDate.getTime();

            return (diffInMilliSec / (1000 * 60)) % 60;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    @NonNull
    public static String getCurrentDate() {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH) + 1;
        int day = c.get(Calendar.DAY_OF_MONTH);
        return (day + "/" + month + "/" + year);

    }

    @NonNull
    public static String getCurrentTime() {
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        return (hour + ":" + minute);
    }

}
