package com.agrinvestapp.utils;

/**
 * Created by hemant
 * Date: 10/4/18.
 */

public final class AppConstants {

    public static final String DEVICE_TYPE = "2";

    //Pagination Limit
    public static final int LIMIT = 10;

    public static final int PAYMENT_CALLBACK = 50;
    public static final int REQUEST_TAKE_PHOTO = 51;
    public static final int REQUEST_GALLERY = 52;
    public static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 53;
    // common file and image path
    public static final int COMMON_REGI_FILE_REQUEST_CODE = 54;
    public static final String TIMESTAMP_FORMAT = "dd/MM/yyyy";
    public static final String SERVER_TIMESTAMP_FORMAT = "yyyy-MM-dd";
    public static final String DB_TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss";
    // key for run time permissions
    public final static int REQUEST_CHECK_SETTINGS_GPS = 96;
    public static final int REQUEST_MULTIPLE_CAMERA_PERMISSIONS = 98;
    public static final String MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey";
    public static final String VIEW_BUNDLE_KEY = "ViewPager";
    public static final String DB_EVENT_ADD = "add";
    public static final String DB_EVENT_EDIT = "edit";
    public static final String DB_EVENT_DEL = "delete";
    public static final String REQUEST_TYPE_ADD = "add";
    public static final String REQUEST_TYPE_EDIT = "edit";
    public static final String REQUEST_TYPE_DEL = "delete";
    public static final String ONLINE = "online";
    public static final String OFFLINE = "offline";
    public static final String REQUEST_TYPE_TREAT = "treatment";
    public static final String REQUEST_TYPE_INSEMINATION = "insemination";
    public static final String REQUEST_TYPE_PREGNANCY = "pregnancy";
    public static final String REQUEST_TYPE_CALVING = "calving";
    public static final String REQUEST_TYPE_WEIGHT = "weight";
    public static final String REQUEST_TYPE_PMOVEMENT = "movement";
    public static final Boolean DB_SYNC_TRUE = true;
    public static final Boolean DB_SYNC_FALSE = false;
    public static final String TREATMENT_FOR_ANIMAL = "1";
    public static final String ANIMAL_MODEL = "animalBean";
    public static final String CROP_MODEL = "cropBean";
    public static final String EXPENSE_FOR_ANIMAL = "animal";
    public static final String EXPENSE_FOR_CROP = "crop";
    public static final String MODEL = "model";
    public static final String USER = "user";
    public static final String MSG = "msg";
    public static final String DIALOG_TITLE = "dialog_title";
    public static final String KEY_FROM_DASHBOARD = "keyFromDashboard";
    public static final String KEY_FROM_ANIMAL = "keyFromAnimal";
    public static final String KEY_FROM_PROPERTY = "keyFromProperty";
    public static final String KEY_FROM_TNC = "keyFromTnc";
    public static final String KEY_FROM_PP = "keyFromPP"; //privacy policy
    public static final String KEY_FROM_ABOUT_US = "keyFromAboutUs";
    public static final String FROM_SYNC_DIALOG = "fromSyncDialog";
    public static final int MAX_TIME_FOR_SYNC_DIALOG = 15;
    public final static String PLAN_FREE = "free";
    public final static String PLAN_PREMIUM = "premium";
    public static final int FREE_ANIMAL_LIMIT = 50;
    public static final int FREE_CROP_LIMIT = 3;

    static final int REQUEST_MULTIPLE_PERMISSIONS = 97;
    static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    static final int MY_PERMISSIONS_REQUEST_CAMERA = 101;
    public static String TNC = "";
    public static String PRIVACY_POLICY = "";
    public static String ABOUT_US = "";
    public static String WEB_PAYMENT = "";
    public static String TYPE_BUSINESS = "business";

    private AppConstants() {
        // This class is not publicly instantiable
    }

}
