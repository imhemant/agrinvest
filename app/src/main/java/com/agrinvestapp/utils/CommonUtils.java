package com.agrinvestapp.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.text.InputFilter;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.agrinvestapp.R;
import com.agrinvestapp.ui.auth.AuthActivity;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by hemant
 * Date: 07/08/18.
 */

public final class CommonUtils {

    private static Toast toast;

    private CommonUtils() {
        // This utility class is not publicly instantiable
    }

    public static boolean isEmailValid(String email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static InputFilter noSpaceFilter() {
        return (source, start, end, dest, dstart, dend) -> {
            for (int i = start; i < end; i++) {
                if (Character.isWhitespace(source.charAt(i))) {
                    return "";
                }
            }
            return null;
        };
    }

    public static String loadJSONFromAsset(Context context, String jsonFileName) throws IOException {
        AssetManager manager = context.getAssets();
        InputStream is = manager.open(jsonFileName);

        int size = is.available();
        byte[] buffer = new byte[size];
        // is.read(buffer);
        is.close();

        return new String(buffer, "UTF-8");
    }

    public static Dialog showLoadingDialog(Context context, Boolean isSync) {
        Dialog dialog = new Dialog(context, (context instanceof AuthActivity) ? R.style.ProgressAuthTheme : R.style.ProgressHomeTheme);
        dialog.show();
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        dialog.setContentView(R.layout.custom_progress);

        new Handler(Looper.getMainLooper()).post(() -> {
            TextView tvLoading = dialog.findViewById(R.id.tvLoading);
            tvLoading.setText(isSync ? context.getString(R.string.syncData) : context.getString(R.string.loading));
        });

        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

    public static void showCustomAlert(Activity activity, String title, String message) {

        LayoutInflater inflater = activity.getLayoutInflater();
        @SuppressLint("InflateParams") View layout = inflater.inflate(R.layout.my_alert_layout, null);
        TextView tv_title = layout.findViewById(R.id.tv_title);
        TextView msgTv = layout.findViewById(R.id.tv_msg);
        tv_title.setText(title);
        msgTv.setText(message);
        Toast toast = new Toast(activity);
        toast.setGravity(Gravity.FILL_HORIZONTAL | Gravity.TOP, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }

    public static void showLogoutAlert(Activity activity, String title, String message) {

        LayoutInflater inflater = activity.getLayoutInflater();
        @SuppressLint("InflateParams") View layout = inflater.inflate(R.layout.my_alert_layout, null);
        TextView tv_title = layout.findViewById(R.id.tv_title);
        TextView msgTv = layout.findViewById(R.id.tv_msg);
        tv_title.setText(title);
        msgTv.setText(message);
        Toast toast = new Toast(activity);
        toast.setGravity(Gravity.FILL_HORIZONTAL | Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }

    public static void showToast(Activity activity, String message, int len) {
        if (toast != null) toast.cancel();

        // Create the object once.
        toast = Toast.makeText(activity, message, len);
        toast.show();
    }

    public static void snackbar(Activity activity, @NonNull View coordinatorLayout, @NonNull String message) {
        if (toast != null) toast.cancel();
        Snackbar snackbar = Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.parseColor("#ffffff"));  //old color 1976d2
        textView.setGravity(Gravity.CENTER);
        snackbar.setActionTextColor(Color.parseColor("#1976d2"));
        sbView.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimaryDark));  //Color.WHITE
        snackbar.show();
    }
}
