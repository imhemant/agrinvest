package com.agrinvestapp.data.model.other;

/**
 * Created by hemant
 * Date: 9/10/18.
 */

public final class AnimalSyncResult {

    private String requestType;
    private String recordKey;
    private String parcelRecordKey;
    private String lotId;
    private String tagNumber;
    private String bornOrBuyDate;
    private String markingOrTagging;
    private String mother;
    private String boughtFrom;
    private String description;
    private String dismant;
    private String saleDate;
    private String salePrice;
    private String saleQuantity;
    private String saleTotalPrice;
    private String costFrom;
    private String costTo;
    private String costTotal;
    private Boolean isDataSync;

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getRecordKey() {
        return recordKey;
    }

    public void setRecordKey(String recordKey) {
        this.recordKey = recordKey;
    }

    public String getParcelRecordKey() {
        return parcelRecordKey;
    }

    public void setParcelRecordKey(String parcelRecordKey) {
        this.parcelRecordKey = parcelRecordKey;
    }

    public String getLotId() {
        return lotId;
    }

    public void setLotId(String lotId) {
        this.lotId = lotId;
    }

    public String getTagNumber() {
        return tagNumber;
    }

    public void setTagNumber(String tagNumber) {
        this.tagNumber = tagNumber;
    }

    public String getBornOrBuyDate() {
        return bornOrBuyDate;
    }

    public void setBornOrBuyDate(String bornOrBuyDate) {
        this.bornOrBuyDate = bornOrBuyDate;
    }

    public String getMarkingOrTagging() {
        return markingOrTagging;
    }

    public void setMarkingOrTagging(String markingOrTagging) {
        this.markingOrTagging = markingOrTagging;
    }

    public String getMother() {
        return mother;
    }

    public void setMother(String mother) {
        this.mother = mother;
    }

    public String getBoughtFrom() {
        return boughtFrom;
    }

    public void setBoughtFrom(String boughtFrom) {
        this.boughtFrom = boughtFrom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDismant() {
        return dismant;
    }

    public void setDismant(String dismant) {
        this.dismant = dismant;
    }

    public String getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(String saleDate) {
        this.saleDate = saleDate;
    }

    public String getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(String salePrice) {
        this.salePrice = salePrice;
    }

    public String getSaleQuantity() {
        return saleQuantity;
    }

    public void setSaleQuantity(String saleQuantity) {
        this.saleQuantity = saleQuantity;
    }

    public String getSaleTotalPrice() {
        return saleTotalPrice;
    }

    public void setSaleTotalPrice(String saleTotalPrice) {
        this.saleTotalPrice = saleTotalPrice;
    }

    public String getCostFrom() {
        return costFrom;
    }

    public void setCostFrom(String costFrom) {
        this.costFrom = costFrom;
    }

    public String getCostTo() {
        return costTo;
    }

    public void setCostTo(String costTo) {
        this.costTo = costTo;
    }

    public String getCostTotal() {
        return costTotal;
    }

    public void setCostTotal(String costTotal) {
        this.costTotal = costTotal;
    }

    public Boolean getDataSync() {
        return isDataSync;
    }

    public void setDataSync(Boolean dataSync) {
        isDataSync = dataSync;
    }
}
