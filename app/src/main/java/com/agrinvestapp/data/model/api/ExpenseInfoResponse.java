package com.agrinvestapp.data.model.api;

import android.arch.persistence.room.Ignore;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by hemant
 * Date: 14/11/18.
 */

public final class ExpenseInfoResponse {


    /**
     * status : success
     * message : OK
     * expenseList : [{"expenseId":"47","user_id":"1","dateBought":"2018-11-19","cost":"5205","description":"","dateApplied":"2018-11-19","expenseFor":"animal","recordKey":"1542622798137","status":"1","crd":"2018-11-19 10:20:00","upd":"2018-11-19 10:20:00","productCount":"2","productName":"Apple","appliedOn":[{"appliedName":"Tag 2","appliedOnRecordKey":"1540551049215","expenseMappingId":"124","expense_id":"47","appliedOn":"animal","appliedOnId":"40","status":"1","crd":"2018-11-19 10:20:00","upd":"2018-11-19 10:20:00"}],"productDetails":[{"productName":"Apple","productRecordKey":"1541164719.55715","expenseProductMappingId":"107","expense_id":"47","product_id":"52","quantity":"2","price":"1235","status":"1","crd":"2018-11-19 10:20:00","upd":"2018-11-19 10:20:00"},{"productName":"Food","productRecordKey":"1541064070.15815","expenseProductMappingId":"108","expense_id":"47","product_id":"35","quantity":"3","price":"500","status":"1","crd":"2018-11-19 10:20:00","upd":"2018-11-19 10:20:00"}]},{"expenseId":"26","user_id":"1","dateBought":"2018-11-17","cost":"4326","description":"The Facebook platform or encounter with the Facebook platform","dateApplied":"2018-11-17","expenseFor":"animal","recordKey":"1542456971894","status":"1","crd":"2018-11-17 12:16:14","upd":"2018-11-17 12:16:14","productCount":"6","productName":"Food","appliedOn":[{"appliedName":"Tag 1","appliedOnRecordKey":"1540551015091","expenseMappingId":"53","expense_id":"26","appliedOn":"animal","appliedOnId":"39","status":"1","crd":"2018-11-17 12:16:14","upd":"2018-11-17 12:16:14"},{"appliedName":"Tag 2","appliedOnRecordKey":"1540551049215","expenseMappingId":"54","expense_id":"26","appliedOn":"animal","appliedOnId":"40","status":"1","crd":"2018-11-17 12:16:14","upd":"2018-11-17 12:16:14"}],"productDetails":[{"productName":"Food","productRecordKey":"1541064070.15815","expenseProductMappingId":"47","expense_id":"26","product_id":"35","quantity":"1","price":"500","status":"1","crd":"2018-11-17 12:16:14","upd":"2018-11-17 12:16:14"},{"productName":"Banana","productRecordKey":"1541063633.31899","expenseProductMappingId":"48","expense_id":"26","product_id":"34","quantity":"2","price":"678","status":"1","crd":"2018-11-17 12:16:14","upd":"2018-11-17 12:16:14"},{"productName":"Apple","productRecordKey":"1541164719.55715","expenseProductMappingId":"49","expense_id":"26","product_id":"52","quantity":"2","price":"1235","status":"1","crd":"2018-11-17 12:16:14","upd":"2018-11-17 12:16:14"}]},{"expenseId":"21","user_id":"1","dateBought":"2018-11-17","cost":"60000","description":"","dateApplied":"2018-11-17","expenseFor":"animal","recordKey":"1542451883101","status":"1","crd":"2018-11-17 10:51:25","upd":"2018-11-17 10:51:25","productCount":"2","productName":"Tomato","appliedOn":[{"appliedName":"Tag 2","appliedOnRecordKey":"1540551049215","expenseMappingId":"46","expense_id":"21","appliedOn":"animal","appliedOnId":"40","status":"1","crd":"2018-11-17 10:51:25","upd":"2018-11-17 10:51:25"},{"appliedName":"Tag 1","appliedOnRecordKey":"1540551015091","expenseMappingId":"47","expense_id":"21","appliedOn":"animal","appliedOnId":"39","status":"1","crd":"2018-11-17 10:51:25","upd":"2018-11-17 10:51:25"}],"productDetails":[{"productName":"Tomato","productRecordKey":"3820531542276934","expenseProductMappingId":"23","expense_id":"21","product_id":"66","quantity":"12","price":"5000","status":"1","crd":"2018-11-17 10:51:25","upd":"2018-11-17 10:51:25"}]},{"expenseId":"16","user_id":"1","dateBought":"2018-11-17","cost":"9940","description":"Thanks Again! I Have To Come In","dateApplied":"2018-11-17","expenseFor":"animal","recordKey":"1542437353176","status":"1","crd":"2018-11-17 06:49:15","upd":"2018-11-20 10:20:27","productCount":"10","productName":"Tomato","appliedOn":[{"appliedName":"Shd","appliedOnRecordKey":"33","expenseMappingId":"24","expense_id":"16","appliedOn":"property","appliedOnId":"33","status":"1","crd":"2018-11-17 06:49:15","upd":"2018-11-17 06:49:15"},{"appliedName":"The test","appliedOnRecordKey":"30","expenseMappingId":"25","expense_id":"16","appliedOn":"property","appliedOnId":"30","status":"1","crd":"2018-11-17 06:49:15","upd":"2018-11-17 06:49:15"},{"appliedName":"The day","appliedOnRecordKey":"29","expenseMappingId":"26","expense_id":"16","appliedOn":"property","appliedOnId":"29","status":"1","crd":"2018-11-17 06:49:15","upd":"2018-11-17 06:49:15"},{"appliedName":"Test","appliedOnRecordKey":"27","expenseMappingId":"27","expense_id":"16","appliedOn":"property","appliedOnId":"27","status":"1","crd":"2018-11-17 06:49:15","upd":"2018-11-17 06:49:15"},{"appliedName":"Sh farm","appliedOnRecordKey":"25","expenseMappingId":"28","expense_id":"16","appliedOn":"property","appliedOnId":"25","status":"1","crd":"2018-11-17 06:49:15","upd":"2018-11-17 06:49:15"}],"productDetails":[{"productName":"Tomato","productRecordKey":"3820531542276934","expenseProductMappingId":"14","expense_id":"16","product_id":"66","quantity":"1","price":"1235","status":"1","crd":"2018-11-17 06:49:15","upd":"2018-11-20 10:17:55"},{"productName":"Apple","productRecordKey":"1541164719.55715","expenseProductMappingId":"110","expense_id":"16","product_id":"52","quantity":"1","price":"1235","status":"1","crd":"2018-11-20 10:17:27","upd":"2018-11-20 10:20:27"}]},{"expenseId":"10","user_id":"1","dateBought":"2018-11-16","cost":"22997","description":"","dateApplied":"2018-11-16","expenseFor":"animal","recordKey":"1542381057090","status":"1","crd":"2018-11-16 15:10:58","upd":"2018-11-16 15:10:58","productCount":"8","productName":"Food","appliedOn":[{"appliedName":"Lot 2","appliedOnRecordKey":"Lot 2","expenseMappingId":"13","expense_id":"10","appliedOn":"lot","appliedOnId":"Lot 2","status":"1","crd":"2018-11-16 15:10:58","upd":"2018-11-16 15:10:58"},{"appliedName":"Lot 1","appliedOnRecordKey":"Lot 1","expenseMappingId":"14","expense_id":"10","appliedOn":"lot","appliedOnId":"Lot 1","status":"1","crd":"2018-11-16 15:10:58","upd":"2018-11-16 15:10:58"}],"productDetails":[{"productName":"Food","productRecordKey":"1541064070.15815","expenseProductMappingId":"5","expense_id":"10","product_id":"35","quantity":"1","price":"500","status":"1","crd":"2018-11-16 15:10:58","upd":"2018-11-16 15:10:58"},{"productName":"Apple","productRecordKey":"1541164719.55715","expenseProductMappingId":"6","expense_id":"10","product_id":"52","quantity":"2","price":"1235","status":"1","crd":"2018-11-16 15:10:58","upd":"2018-11-16 15:10:58"},{"productName":"Rest assured that I have a gre","productRecordKey":"1542002386961","expenseProductMappingId":"7","expense_id":"10","product_id":"59","quantity":"3","price":"9","status":"1","crd":"2018-11-16 15:10:58","upd":"2018-11-16 15:10:58"},{"productName":"Tomato","productRecordKey":"3820531542276934","expenseProductMappingId":"8","expense_id":"10","product_id":"66","quantity":"4","price":"5000","status":"1","crd":"2018-11-16 15:10:58","upd":"2018-11-16 15:10:58"}]}]
     */

    private String status;
    private String message;
    private List<ExpenseListBean> expenseList;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ExpenseListBean> getExpenseList() {
        return expenseList;
    }

    public void setExpenseList(List<ExpenseListBean> expenseList) {
        this.expenseList = expenseList;
    }

    public static class ExpenseListBean implements Parcelable{
        public static final Creator<ExpenseListBean> CREATOR = new Creator<ExpenseListBean>() {
            @Override
            public ExpenseListBean createFromParcel(Parcel in) {
                return new ExpenseListBean(in);
            }

            @Override
            public ExpenseListBean[] newArray(int size) {
                return new ExpenseListBean[size];
            }
        };
        /**
         * expenseId : 47
         * user_id : 1
         * dateBought : 2018-11-19
         * cost : 5205
         * description :
         * dateApplied : 2018-11-19
         * expenseFor : animal
         * recordKey : 1542622798137
         * status : 1
         * crd : 2018-11-19 10:20:00
         * upd : 2018-11-19 10:20:00
         * productCount : 2
         * productName : Apple
         * appliedOn : [{"appliedName":"Tag 2","appliedOnRecordKey":"1540551049215","expenseMappingId":"124","expense_id":"47","appliedOn":"animal","appliedOnId":"40","status":"1","crd":"2018-11-19 10:20:00","upd":"2018-11-19 10:20:00"}]
         * productDetails : [{"productName":"Apple","productRecordKey":"1541164719.55715","expenseProductMappingId":"107","expense_id":"47","product_id":"52","quantity":"2","price":"1235","status":"1","crd":"2018-11-19 10:20:00","upd":"2018-11-19 10:20:00"},{"productName":"Food","productRecordKey":"1541064070.15815","expenseProductMappingId":"108","expense_id":"47","product_id":"35","quantity":"3","price":"500","status":"1","crd":"2018-11-19 10:20:00","upd":"2018-11-19 10:20:00"}]
         */

        @Ignore
        public Boolean isSelected = false;
        private String expenseId;
        private String user_id;
        private String dateBought;
        private String cost;
        private String description;
        private String dateApplied;
        private String expenseFor;
        private String recordKey;
        private String status;
        private String crd;
        private String upd;
        private String productCount;
        private String productName;
        private String applyOnName;
        @Ignore
        private String applyOnNameCount;
        @Ignore
        private List<AppliedOnBean> appliedOn;
        @Ignore
        private List<ProductDetailsBean> productDetails;

        public ExpenseListBean(){}

        protected ExpenseListBean(Parcel in) {
            byte tmpIsSelected = in.readByte();
            isSelected = tmpIsSelected == 0 ? null : tmpIsSelected == 1;
            expenseId = in.readString();
            user_id = in.readString();
            dateBought = in.readString();
            cost = in.readString();
            description = in.readString();
            dateApplied = in.readString();
            expenseFor = in.readString();
            recordKey = in.readString();
            status = in.readString();
            crd = in.readString();
            upd = in.readString();
            productCount = in.readString();
            productName = in.readString();
            applyOnName = in.readString();
            applyOnNameCount = in.readString();
            appliedOn = in.createTypedArrayList(AppliedOnBean.CREATOR);
            productDetails = in.createTypedArrayList(ProductDetailsBean.CREATOR);
        }

        public String getExpenseId() {
            return expenseId;
        }

        public void setExpenseId(String expenseId) {
            this.expenseId = expenseId;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getDateBought() {
            return dateBought;
        }

        public void setDateBought(String dateBought) {
            this.dateBought = dateBought;
        }

        public String getCost() {
            return cost;
        }

        public void setCost(String cost) {
            this.cost = cost;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getDateApplied() {
            return dateApplied;
        }

        public void setDateApplied(String dateApplied) {
            this.dateApplied = dateApplied;
        }

        public String getExpenseFor() {
            return expenseFor;
        }

        public void setExpenseFor(String expenseFor) {
            this.expenseFor = expenseFor;
        }

        public String getRecordKey() {
            return recordKey;
        }

        public void setRecordKey(String recordKey) {
            this.recordKey = recordKey;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCrd() {
            return crd;
        }

        public void setCrd(String crd) {
            this.crd = crd;
        }

        public String getUpd() {
            return upd;
        }

        public void setUpd(String upd) {
            this.upd = upd;
        }

        public String getProductCount() {
            return productCount;
        }

        public void setProductCount(String productCount) {
            this.productCount = productCount;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getApplyOnName() {
            return applyOnName;
        }

        public void setApplyOnName(String applyOnName) {
            this.applyOnName = applyOnName;
        }

        public String getApplyOnNameCount() {
            return applyOnNameCount;
        }

        public void setApplyOnNameCount(String applyOnNameCount) {
            this.applyOnNameCount = applyOnNameCount;
        }

        public List<AppliedOnBean> getAppliedOn() {
            return appliedOn;
        }

        public void setAppliedOn(List<AppliedOnBean> appliedOn) {
            this.appliedOn = appliedOn;
        }

        public List<ProductDetailsBean> getProductDetails() {
            return productDetails;
        }

        public void setProductDetails(List<ProductDetailsBean> productDetails) {
            this.productDetails = productDetails;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeByte((byte) (isSelected == null ? 0 : isSelected ? 1 : 2));
            dest.writeString(expenseId);
            dest.writeString(user_id);
            dest.writeString(dateBought);
            dest.writeString(cost);
            dest.writeString(description);
            dest.writeString(dateApplied);
            dest.writeString(expenseFor);
            dest.writeString(recordKey);
            dest.writeString(status);
            dest.writeString(crd);
            dest.writeString(upd);
            dest.writeString(productCount);
            dest.writeString(productName);
            dest.writeString(applyOnName);
            dest.writeString(applyOnNameCount);
            dest.writeTypedList(appliedOn);
            dest.writeTypedList(productDetails);
        }

        public static class AppliedOnBean implements Parcelable{
            public static final Creator<AppliedOnBean> CREATOR = new Creator<AppliedOnBean>() {
                @Override
                public AppliedOnBean createFromParcel(Parcel in) {
                    return new AppliedOnBean(in);
                }

                @Override
                public AppliedOnBean[] newArray(int size) {
                    return new AppliedOnBean[size];
                }
            };
            /**
             * appliedName : Tag 2
             * appliedOnRecordKey : 1540551049215
             * expenseMappingId : 124
             * expense_id : 47
             * appliedOn : animal
             * appliedOnId : 40
             * status : 1
             * crd : 2018-11-19 10:20:00
             * upd : 2018-11-19 10:20:00
             */

            private String appliedName;
            private String appliedOnRecordKey;
            private String expenseMappingId;
            private String expense_id;
            private String appliedOn;
            private String appliedOnId;
            private String status;
            private String crd;
            private String upd;

            public AppliedOnBean(){}

            protected AppliedOnBean(Parcel in) {
                appliedName = in.readString();
                appliedOnRecordKey = in.readString();
                expenseMappingId = in.readString();
                expense_id = in.readString();
                appliedOn = in.readString();
                appliedOnId = in.readString();
                status = in.readString();
                crd = in.readString();
                upd = in.readString();
            }

            public String getAppliedName() {
                return appliedName;
            }

            public void setAppliedName(String appliedName) {
                this.appliedName = appliedName;
            }

            public String getAppliedOnRecordKey() {
                return appliedOnRecordKey;
            }

            public void setAppliedOnRecordKey(String appliedOnRecordKey) {
                this.appliedOnRecordKey = appliedOnRecordKey;
            }

            public String getExpenseMappingId() {
                return expenseMappingId;
            }

            public void setExpenseMappingId(String expenseMappingId) {
                this.expenseMappingId = expenseMappingId;
            }

            public String getExpense_id() {
                return expense_id;
            }

            public void setExpense_id(String expense_id) {
                this.expense_id = expense_id;
            }

            public String getAppliedOn() {
                return appliedOn;
            }

            public void setAppliedOn(String appliedOn) {
                this.appliedOn = appliedOn;
            }

            public String getAppliedOnId() {
                return appliedOnId;
            }

            public void setAppliedOnId(String appliedOnId) {
                this.appliedOnId = appliedOnId;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getCrd() {
                return crd;
            }

            public void setCrd(String crd) {
                this.crd = crd;
            }

            public String getUpd() {
                return upd;
            }

            public void setUpd(String upd) {
                this.upd = upd;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(appliedName);
                dest.writeString(appliedOnRecordKey);
                dest.writeString(expenseMappingId);
                dest.writeString(expense_id);
                dest.writeString(appliedOn);
                dest.writeString(appliedOnId);
                dest.writeString(status);
                dest.writeString(crd);
                dest.writeString(upd);
            }
        }

        public static class ProductDetailsBean implements Parcelable{
            public static final Creator<ProductDetailsBean> CREATOR = new Creator<ProductDetailsBean>() {
                @Override
                public ProductDetailsBean createFromParcel(Parcel in) {
                    return new ProductDetailsBean(in);
                }

                @Override
                public ProductDetailsBean[] newArray(int size) {
                    return new ProductDetailsBean[size];
                }
            };
            /**
             * productName : Apple
             * productRecordKey : 1541164719.55715
             * expenseProductMappingId : 107
             * expense_id : 47
             * product_id : 52
             * quantity : 2
             * price : 1235
             * status : 1
             * crd : 2018-11-19 10:20:00
             * upd : 2018-11-19 10:20:00
             */

            private String productName;
            private String productRecordKey;
            private String expenseProductMappingId;
            private String expense_id;
            private String product_id;
            private String quantity;
            private String price;
            private String status;
            private String crd;
            private String upd;

            public ProductDetailsBean(){}

            protected ProductDetailsBean(Parcel in) {
                productName = in.readString();
                productRecordKey = in.readString();
                expenseProductMappingId = in.readString();
                expense_id = in.readString();
                product_id = in.readString();
                quantity = in.readString();
                price = in.readString();
                status = in.readString();
                crd = in.readString();
                upd = in.readString();
            }

            public String getProductName() {
                return productName;
            }

            public void setProductName(String productName) {
                this.productName = productName;
            }

            public String getProductRecordKey() {
                return productRecordKey;
            }

            public void setProductRecordKey(String productRecordKey) {
                this.productRecordKey = productRecordKey;
            }

            public String getExpenseProductMappingId() {
                return expenseProductMappingId;
            }

            public void setExpenseProductMappingId(String expenseProductMappingId) {
                this.expenseProductMappingId = expenseProductMappingId;
            }

            public String getExpense_id() {
                return expense_id;
            }

            public void setExpense_id(String expense_id) {
                this.expense_id = expense_id;
            }

            public String getProduct_id() {
                return product_id;
            }

            public void setProduct_id(String product_id) {
                this.product_id = product_id;
            }

            public String getQuantity() {
                return quantity;
            }

            public void setQuantity(String quantity) {
                this.quantity = quantity;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getCrd() {
                return crd;
            }

            public void setCrd(String crd) {
                this.crd = crd;
            }

            public String getUpd() {
                return upd;
            }

            public void setUpd(String upd) {
                this.upd = upd;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(productName);
                dest.writeString(productRecordKey);
                dest.writeString(expenseProductMappingId);
                dest.writeString(expense_id);
                dest.writeString(product_id);
                dest.writeString(quantity);
                dest.writeString(price);
                dest.writeString(status);
                dest.writeString(crd);
                dest.writeString(upd);
            }
        }
    }
}
