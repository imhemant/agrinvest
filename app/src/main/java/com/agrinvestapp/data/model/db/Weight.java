package com.agrinvestapp.data.model.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hemant
 * Date: 9/10/18.
 */
@Entity(tableName = "weight", indices = {@Index(value = {"animal_id"})},
        foreignKeys = @ForeignKey(entity = Animal.class,
                parentColumns = "animalId",
                childColumns = "animal_id",
                onDelete = ForeignKey.CASCADE))
public final class Weight {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "weightId")
    private String weightId = "";

    /*  @ForeignKey(entity = Parcel.class,
              parentColumns = "parcelId",
              childColumns = "parcel_id")*/
    @ColumnInfo(name = "animal_id")
    private String animal_id;

    @ColumnInfo(name = "weight")
    private String weight;

    @ColumnInfo(name = "weightUnit")
    private String weightUnit;

    @ColumnInfo(name = "weightDate")
    private String weightDate;

    @ColumnInfo(name = "recordKey")
    private String recordKey;

    @ColumnInfo(name = "crd")
    private String crd;

    @ColumnInfo(name = "upd")
    private String upd;

    @ColumnInfo(name = "isDataSync")
    private Boolean isDataSync;

    @SerializedName("requestType")
    @ColumnInfo(name = "event")
    private String event;

    @NonNull
    public String getWeightId() {
        return weightId;
    }

    public void setWeightId(@NonNull String weightId) {
        this.weightId = weightId;
    }

    public String getAnimal_id() {
        return animal_id;
    }

    public void setAnimal_id(String animal_id) {
        this.animal_id = animal_id;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getWeightUnit() {
        return weightUnit;
    }

    public void setWeightUnit(String weightUnit) {
        this.weightUnit = weightUnit;
    }

    public String getWeightDate() {
        return weightDate;
    }

    public void setWeightDate(String weightDate) {
        this.weightDate = weightDate;
    }

    public String getRecordKey() {
        return recordKey;
    }

    public void setRecordKey(String recordKey) {
        this.recordKey = recordKey;
    }

    public String getCrd() {
        return crd;
    }

    public void setCrd(String crd) {
        this.crd = crd;
    }

    public String getUpd() {
        return upd;
    }

    public void setUpd(String upd) {
        this.upd = upd;
    }

    public Boolean getDataSync() {
        return isDataSync;
    }

    public void setDataSync(Boolean dataSync) {
        isDataSync = dataSync;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }
}
