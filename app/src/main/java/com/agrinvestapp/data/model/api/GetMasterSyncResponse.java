package com.agrinvestapp.data.model.api;

import java.util.List;

/**
 * Created by hemant
 * Date: 25/10/18.
 */

public final class GetMasterSyncResponse {

    /**
     * status : success
     * message : Success
     * masterData : [{"propertyId":"4","user_id":"1","propertyName":"Test property","location":"Krishna Tower, 208, 2nd Floor, OPP. Scheme-140, Main Rd, Pipliyahana, Indore, Madhya Pradesh 452016, India","longitude":"75.90907495468855","latitude":"22.705161452064246","size":"12","sizeUnite":"acres","description":"The Facebook group mein auto or email us back for the Facebook group for event locations in advertising","status":"1","crd":"2018-10-18 04:59:30","upd":"2018-10-18 12:27:36","rainCount":"1","parcelCount":"4","raindataList":[{"raindataId":"16","property_id":"4","fromDate":"2018-10-18","toDate":"2018-10-18","quantity":"12","quantityUnite":"mm","status":"1","crd":"2018-10-18 06:57:36","upd":"2018-10-18 06:57:36"}],"parcelList":[{"parcelId":"27","parcelName":"Test parcel 3","property_id":"4","recordKey":"1540189435858","status":"0","crd":"2018-10-22 09:01:17","upd":"2018-10-22 09:01:17","animalList":[{"propertyId":"4","parcelName":"Test parcel 3","parcelRecordKey":"1540189435858","animalId":"12","parcel_id":"27","lotId":"Lot 2","tagNumber":"Tag 2","bornOrBuyDate":"2018-10-18","mother":"Tag1","boughtFrom":"The Facebook group for event l","description":"The Facebook group for event locations in your team to score if ads are developed","dismant":"0000-00-00","markingOrTagging":"2018-10-18","saleDate":"2018-10-16","salePrice":"12","saleQuantity":"10","saleTotalPrice":"120.0","costFrom":"2018-10-11","costTo":"2018-10-20","costTotal":"23","status":"0","recordKey":"1539838872959","crd":"2018-10-18 05:01:15","upd":"2018-10-22 12:07:04","treatmentList":[{"treatmentId":"26","treatmentName":"Treatment 2","treatmentForId":"12","treatmentFor":"1","recordKey":"1540207404185","crd":"2018-10-22 12:07:03","upd":"2018-10-22 12:07:03","mappingList":[{"treatmentMappingId":"29","treatment_id":"26","treatmentDate":"2018-10-21","treatmentDescription":"The Facebook platform of choice not sure if","recordKey":"1540207428421","crd":"2018-10-22 12:07:03","upd":"2018-10-22 12:07:03"},{"treatmentMappingId":"28","treatment_id":"26","treatmentDate":"2018-10-22","treatmentDescription":"The Facebook page and press the menu or email","recordKey":"1540207411566","crd":"2018-10-22 12:07:03","upd":"2018-10-22 12:07:03"},{"treatmentMappingId":"27","treatment_id":"26","treatmentDate":"2018-10-21","treatmentDescription":"The Facebook platform of choice not sure if","recordKey":"1540207428421","crd":"2018-10-22 12:07:03","upd":"2018-10-22 12:07:03"},{"treatmentMappingId":"26","treatment_id":"26","treatmentDate":"2018-10-22","treatmentDescription":"The Facebook page and press the menu or email","recordKey":"1540207411566","crd":"2018-10-22 12:07:03","upd":"2018-10-22 12:07:03"}]}],"inseminationList":[{"inseminationId":"8","animal_id":"12","inseminationName":"Insemination 1","inseminationDate":"2018-10-18","inseminationDescription":"The Facebook page and press the day","recordKey":"1539851741940","crd":"2018-10-18 08:35:44","upd":"2018-10-18 08:35:44"}],"pregnancyTestList":[{"pregnancyId":"13","animal_id":"22","pregnancyName":"Pregnancy test 1","pregnancyDate":"2018-10-24","pregnancyDescription":"Thanks again and have to get a few things that I have","recordKey":"1540359821140","crd":"2018-10-24 05:43:46","upd":"2018-10-24 05:43:46"}],"calvingList":[{"calvingId":"12","animal_id":"12","calvingNumber":"hello I am 6263636646363636366","calvingDate":"2018-10-18","recordKey":"1539848598862","crd":"2018-10-18 07:43:22","upd":"2018-10-18 07:43:22"}],"weightList":[{"weightId":"27","animal_id":"12","weight":"1111","weightUnit":"kg","weightDate":"2018-10-18","recordKey":"1539851826062","crd":"2018-10-18 08:37:08","upd":"2018-10-18 08:37:08"},{"weightId":"25","animal_id":"12","weight":"1235","weightUnit":"pounds","weightDate":"2018-10-10","recordKey":"1539844642086","crd":"2018-10-18 06:37:25","upd":"2018-10-23 10:32:19"}],"parcelMovementList":[{"fromName":"Test parcel 1","fromRecordKey":"1539838785325","toName":"Test parcel 3","toRecordKey":"1540189435858","parcelMovementId":"47","animal_id":"12","movementFrom":"17","movementTo":"27","movementDate":"2018-10-22","recordKey":"1540207203662","crd":"2018-10-22 12:07:04","upd":"2018-10-22 12:07:04"},{"fromName":"Test parcel","fromRecordKey":"1539838825533","toName":"Test parcel 1","toRecordKey":"1539838785325","parcelMovementId":"22","animal_id":"12","movementFrom":"19","movementTo":"17","movementDate":"2018-10-18","recordKey":"1539851780761","crd":"2018-10-18 08:36:23","upd":"2018-10-18 08:36:23"}]}],"cropList":[{"propertyId":"4","parcelName":"Test parcel 3","parcelRecordKey":"1540189435858","cropId":"31","parcel_id":"27","cropName":"Crop 1","lotId":"Lot 1","description":"The Facebook page and the day of the","recordKey":"1540385001428","preparationDate":"0000-00-00","plantingDate":"0000-00-00","harvestDate":"0000-00-00","saleDate":"0000-00-00","salePrice":"","saleQuantity":"","saleTotalPrice":"","costFrom":"0000-00-00","costTo":"0000-00-00","costTotal":"","status":"0","crd":"2018-10-24 12:43:23","upd":"2018-10-24 12:43:23"}]},{"parcelId":"19","parcelName":"Test parcel","property_id":"4","recordKey":"1539838825533","status":"0","crd":"2018-10-18 05:00:27","upd":"2018-10-18 05:00:27","animalList":[{"propertyId":"4","parcelName":"Test parcel","parcelRecordKey":"1539838825533","animalId":"22","parcel_id":"19","lotId":"Lot 1234","tagNumber":"Tag 123","bornOrBuyDate":"2018-10-20","mother":"","boughtFrom":"Mother and I","description":"Thanks for the heads up and I will be in touch with the same thing as the same is true for you to be there at my house at all I just have to get a new phone","dismant":"2018-10-18","markingOrTagging":"2018-10-17","saleDate":"2018-10-23","salePrice":"25","saleQuantity":"65","saleTotalPrice":"1625.0","costFrom":"2018-10-01","costTo":"2018-10-24","costTotal":"25000","status":"0","recordKey":"1540358167870","crd":"2018-10-24 05:16:12","upd":"2018-10-24 07:08:43","treatmentList":[{"treatmentId":"31","treatmentName":"Treatment 3","treatmentForId":"22","treatmentFor":"1","recordKey":"1540364777515","crd":"2018-10-24 07:06:20","upd":"2018-10-24 07:06:20","mappingList":[{"treatmentMappingId":"35","treatment_id":"31","treatmentDate":"2018-10-23","treatmentDescription":"Hello my name or email us at sjdjd","recordKey":"1540364817075","crd":"2018-10-24 07:07:02","upd":"2018-10-24 07:07:11"}]},{"treatmentId":"30","treatmentName":"Treatment 2","treatmentForId":"22","treatmentFor":"1","recordKey":"1540359919024","crd":"2018-10-24 05:45:23","upd":"2018-10-24 05:45:23","mappingList":[{"treatmentMappingId":"33","treatment_id":"30","treatmentDate":"2018-10-24","treatmentDescription":"Thanks again for the opportunity and I hope you can find the right to know that you are","recordKey":"1540359953606","crd":"2018-10-24 05:45:58","upd":"2018-10-24 05:45:58"},{"treatmentMappingId":"32","treatment_id":"30","treatmentDate":"2018-10-22","treatmentDescription":"Thanks again and happy holidays to you too and your family have a","recordKey":"1540359933436","crd":"2018-10-24 05:45:37","upd":"2018-10-24 05:45:47"}]}],"inseminationList":[{"inseminationId":"22","animal_id":"22","inseminationName":"Insemination 1","inseminationDate":"2018-10-24","inseminationDescription":"The Facebook page and the other","recordKey":"1540364864271","crd":"2018-10-24 07:07:46","upd":"2018-10-24 07:07:46"},{"inseminationId":"20","animal_id":"22","inseminationName":"Insemination 1","inseminationDate":"2018-10-24","inseminationDescription":"Thanks again and happy holidays to see if I could get the info from the same company that I","recordKey":"1540359843514","crd":"2018-10-24 05:44:08","upd":"2018-10-24 05:44:08"}],"pregnancyTestList":[{"pregnancyId":"13","animal_id":"22","pregnancyName":"Pregnancy test 1","pregnancyDate":"2018-10-24","pregnancyDescription":"Thanks again and have to get a few things that I have","recordKey":"1540359821140","crd":"2018-10-24 05:43:46","upd":"2018-10-24 05:43:46"}],"calvingList":[{"calvingId":"31","animal_id":"22","calvingNumber":"the Facebook","calvingDate":"2018-10-24","recordKey":"1540364901364","crd":"2018-10-24 07:08:23","upd":"2018-10-24 07:08:23"}],"weightList":[{"weightId":"40","animal_id":"22","weight":"123","weightUnit":"pounds","weightDate":"2018-10-24","recordKey":"1540361952771","crd":"2018-10-24 06:19:17","upd":"2018-10-24 06:19:17"},{"weightId":"38","animal_id":"22","weight":"123","weightUnit":"pound","weightDate":"2018-10-24","recordKey":"1540358325710","crd":"2018-10-24 05:18:50","upd":"2018-10-24 05:18:50"}],"parcelMovementList":[{"fromName":"The Facebook","fromRecordKey":"1540364424893","toName":"Test parcel","toRecordKey":"1539838825533","parcelMovementId":"57","animal_id":"22","movementFrom":"32","movementTo":"19","movementDate":"2018-10-24","recordKey":"1540364921143","crd":"2018-10-24 07:08:43","upd":"2018-10-24 07:08:43"},{"fromName":"Test parcel 1","fromRecordKey":"1539838785325","toName":"The Facebook","toRecordKey":"1540364424893","parcelMovementId":"56","animal_id":"22","movementFrom":"17","movementTo":"32","movementDate":"2018-10-24","recordKey":"1540364587511","crd":"2018-10-24 07:03:09","upd":"2018-10-24 07:03:09"}]},{"propertyId":"4","parcelName":"Test parcel","parcelRecordKey":"1539838825533","animalId":"14","parcel_id":"19","lotId":"Lot 3","tagNumber":"Tag 3","bornOrBuyDate":"2018-10-10","mother":"Tag 2","boughtFrom":"The Facebook group","description":"The Facebook group for event professionals logo hello","dismant":"0000-00-00","markingOrTagging":"0000-00-00","saleDate":"0000-00-00","salePrice":"","saleQuantity":"","saleTotalPrice":"","costFrom":"2018-10-19","costTo":"2018-10-22","costTotal":"12","status":"0","recordKey":"1539839914909","crd":"2018-10-18 05:18:37","upd":"2018-10-22 11:14:36","treatmentList":[{"treatmentId":"28","treatmentName":"Treatment 3","treatmentForId":"14","treatmentFor":"1","recordKey":"1540304512302","crd":"2018-10-23 14:21:54","upd":"2018-10-23 14:21:54","mappingList":[{"treatmentMappingId":"31","treatment_id":"28","treatmentDate":"2018-10-18","treatmentDescription":"The Facebook platform of choice I","recordKey":"1540304523105","crd":"2018-10-23 14:22:05","upd":"2018-10-23 14:22:15"}]},{"treatmentId":"17","treatmentName":"Treatment 2","treatmentForId":"14","treatmentFor":"1","recordKey":"1539861195430","crd":"2018-10-18 11:13:18","upd":"2018-10-18 11:13:18","mappingList":[{"treatmentMappingId":"15","treatment_id":"17","treatmentDate":"2018-10-18","treatmentDescription":"The Facebook platform you also get","recordKey":"1539865434398","crd":"2018-10-18 12:23:56","upd":"2018-10-18 12:23:56"}]},{"treatmentId":"10","treatmentName":"Treatment 1","treatmentForId":"14","treatmentFor":"1","recordKey":"1539842029512","crd":"2018-10-18 05:53:51","upd":"2018-10-18 05:53:51","mappingList":[{"treatmentMappingId":"10","treatment_id":"10","treatmentDate":"2018-10-10","treatmentDescription":"The Facebook platform or email","recordKey":"1539842042342","crd":"2018-10-18 05:54:04","upd":"2018-10-18 05:54:04"}]}],"inseminationList":[{"inseminationId":"6","animal_id":"14","inseminationName":"Insemination 1","inseminationDate":"2018-10-18","inseminationDescription":"The Facebook page and the","recordKey":"1539842062163","crd":"2018-10-18 05:54:24","upd":"2018-10-18 05:54:24"}],"pregnancyTestList":[{"pregnancyId":"4","animal_id":"14","pregnancyName":"Pregnancy 1","pregnancyDate":"2018-10-18","pregnancyDescription":"Hello my name or email","recordKey":"1539842084582","crd":"2018-10-18 05:54:47","upd":"2018-10-18 05:54:47"}],"calvingList":[{"calvingId":"25","animal_id":"14","calvingNumber":"the Facebook","calvingDate":"2018-10-22","recordKey":"1540204598310","crd":"2018-10-22 11:14:37","upd":"2018-10-22 11:14:37"}],"weightList":[{"weightId":"35","animal_id":"14","weight":"12","weightUnit":"kg","weightDate":"2018-10-17","recordKey":"1540204607216","crd":"2018-10-22 11:14:38","upd":"2018-10-22 11:14:38"},{"weightId":"26","animal_id":"14","weight":"12","weightUnit":"pounds","weightDate":"2018-10-18","recordKey":"1539848692210","crd":"2018-10-18 07:44:55","upd":"2018-10-23 10:32:19"},{"weightId":"19","animal_id":"14","weight":"2500","weightUnit":"pounds","weightDate":"2018-10-18","recordKey":"1539842124695","crd":"2018-10-18 05:55:27","upd":"2018-10-23 10:32:19"}],"parcelMovementList":[]}],"cropList":[]},{"parcelId":"17","parcelName":"Test parcel 1","property_id":"4","recordKey":"1539838785325","status":"0","crd":"2018-10-18 04:59:47","upd":"2018-10-18 04:59:47","animalList":[],"cropList":[]}]},{"propertyId":"1","user_id":"1","propertyName":"The Facebook","location":"2, Indore - Bhopal Rd, Brajeshwari Extension, Greater Brajeshwari, Indore, Madhya Pradesh 452016, India","longitude":"75.90894855558872","latitude":"22.70481968249627","size":"12","sizeUnite":"acres","description":"Hello I have any questions or comments please","status":"1","crd":"2018-10-15 10:32:22","upd":"2018-10-18 01:10:19","rainCount":"0","parcelCount":"3","raindataList":[],"parcelList":[{"parcelId":"32","parcelName":"The Facebook","property_id":"1","recordKey":"1540364424893","status":"0","crd":"2018-10-24 07:00:27","upd":"2018-10-24 07:00:27","animalList":[],"cropList":[]},{"parcelId":"16","parcelName":"My parcel 2","property_id":"1","recordKey":"1539783968625","status":"0","crd":"2018-10-17 13:46:11","upd":"2018-10-17 13:46:11","animalList":[{"propertyId":"1","parcelName":"My parcel 2","parcelRecordKey":"1539783968625","animalId":"11","parcel_id":"16","lotId":"Lot1","tagNumber":"Tag1","bornOrBuyDate":"2018-10-17","mother":"","boughtFrom":"Hello my name or email","description":"The Facebook platform you and your family and","dismant":"0000-00-00","markingOrTagging":"2018-10-17","saleDate":"2018-10-11","salePrice":"12","saleQuantity":"08","saleTotalPrice":"96.0","costFrom":"2018-10-01","costTo":"2018-10-18","costTotal":"123","status":"0","recordKey":"1539779007691","crd":"2018-10-17 12:23:29","upd":"2018-10-22 09:10:07","treatmentList":[{"treatmentId":"6","treatmentName":"Treatment","treatmentForId":"11","treatmentFor":"1","recordKey":"1539781670300","crd":"2018-10-17 13:07:53","upd":"2018-10-17 13:28:15","mappingList":[]}],"inseminationList":[{"inseminationId":"4","animal_id":"11","inseminationName":"Insemination 2","inseminationDate":"2018-10-18","inseminationDescription":"The Facebook group for the day of the","recordKey":"1539839310636","crd":"2018-10-18 05:08:32","upd":"2018-10-18 05:08:32"},{"inseminationId":"3","animal_id":"11","inseminationName":"Insemination 1","inseminationDate":"2018-10-17","inseminationDescription":"The day of the day of the day.","recordKey":"1539783555926","crd":"2018-10-17 13:39:18","upd":"2018-10-17 13:39:18"}],"pregnancyTestList":[{"pregnancyId":"2","animal_id":"11","pregnancyName":"Pregnancy 1","pregnancyDate":"2018-10-17","pregnancyDescription":"The day and I am not able to get the day and time of","recordKey":"1539783733545","crd":"2018-10-17 13:42:18","upd":"2018-10-17 13:42:18"}],"calvingList":[{"calvingId":"4","animal_id":"11","calvingNumber":"23","calvingDate":"2018-10-17","recordKey":"1539783867935","crd":"2018-10-17 13:44:32","upd":"2018-10-17 13:44:32"}],"weightList":[{"weightId":"18","animal_id":"11","weight":"123","weightUnit":"pounds","weightDate":"2018-10-17","recordKey":"1539783850816","crd":"2018-10-17 13:44:15","upd":"2018-10-23 10:32:19"}],"parcelMovementList":[{"fromName":"My parcel 1","fromRecordKey":"1539771544337","toName":"My parcel 2","toRecordKey":"1539783968625","parcelMovementId":"12","animal_id":"11","movementFrom":"14","movementTo":"16","movementDate":"2018-10-18","recordKey":"1539797941921","crd":"2018-10-17 17:39:03","upd":"2018-10-17 17:39:03"},{"fromName":"My parcel 2","fromRecordKey":"1539783968625","toName":"My parcel 1","toRecordKey":"1539771544337","parcelMovementId":"11","animal_id":"11","movementFrom":"16","movementTo":"14","movementDate":"2018-10-17","recordKey":"1539786236759","crd":"2018-10-17 14:23:57","upd":"2018-10-17 14:23:57"}]}],"cropList":[]}]}]
     */

    private String status;
    private String message;
    private MasterDataBean masterData;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MasterDataBean getMasterData() {
        return masterData;
    }

    public void setMasterData(MasterDataBean masterData) {
        this.masterData = masterData;
    }

    public static class MasterDataBean {
        /**
         * propertyId : 4
         * user_id : 1
         * propertyName : Test property
         * location : Krishna Tower, 208, 2nd Floor, OPP. Scheme-140, Main Rd, Pipliyahana, Indore, Madhya Pradesh 452016, India
         * longitude : 75.90907495468855
         * latitude : 22.705161452064246
         * size : 12
         * sizeUnite : acres
         * description : The Facebook group mein auto or email us back for the Facebook group for event locations in advertising
         * status : 1
         * crd : 2018-10-18 04:59:30
         * upd : 2018-10-18 12:27:36
         * rainCount : 1
         * parcelCount : 4
         * raindataList : [{"raindataId":"16","property_id":"4","fromDate":"2018-10-18","toDate":"2018-10-18","quantity":"12","quantityUnite":"mm","status":"1","crd":"2018-10-18 06:57:36","upd":"2018-10-18 06:57:36"}]
         * parcelList : [{"parcelId":"27","parcelName":"Test parcel 3","property_id":"4","recordKey":"1540189435858","status":"0","crd":"2018-10-22 09:01:17","upd":"2018-10-22 09:01:17","animalList":[{"propertyId":"4","parcelName":"Test parcel 3","parcelRecordKey":"1540189435858","animalId":"12","parcel_id":"27","lotId":"Lot 2","tagNumber":"Tag 2","bornOrBuyDate":"2018-10-18","mother":"Tag1","boughtFrom":"The Facebook group for event l","description":"The Facebook group for event locations in your team to score if ads are developed","dismant":"0000-00-00","markingOrTagging":"2018-10-18","saleDate":"2018-10-16","salePrice":"12","saleQuantity":"10","saleTotalPrice":"120.0","costFrom":"2018-10-11","costTo":"2018-10-20","costTotal":"23","status":"0","recordKey":"1539838872959","crd":"2018-10-18 05:01:15","upd":"2018-10-22 12:07:04","treatmentList":[{"treatmentId":"26","treatmentName":"Treatment 2","treatmentForId":"12","treatmentFor":"1","recordKey":"1540207404185","crd":"2018-10-22 12:07:03","upd":"2018-10-22 12:07:03","mappingList":[{"treatmentMappingId":"29","treatment_id":"26","treatmentDate":"2018-10-21","treatmentDescription":"The Facebook platform of choice not sure if","recordKey":"1540207428421","crd":"2018-10-22 12:07:03","upd":"2018-10-22 12:07:03"},{"treatmentMappingId":"28","treatment_id":"26","treatmentDate":"2018-10-22","treatmentDescription":"The Facebook page and press the menu or email","recordKey":"1540207411566","crd":"2018-10-22 12:07:03","upd":"2018-10-22 12:07:03"},{"treatmentMappingId":"27","treatment_id":"26","treatmentDate":"2018-10-21","treatmentDescription":"The Facebook platform of choice not sure if","recordKey":"1540207428421","crd":"2018-10-22 12:07:03","upd":"2018-10-22 12:07:03"},{"treatmentMappingId":"26","treatment_id":"26","treatmentDate":"2018-10-22","treatmentDescription":"The Facebook page and press the menu or email","recordKey":"1540207411566","crd":"2018-10-22 12:07:03","upd":"2018-10-22 12:07:03"}]}],"inseminationList":[{"inseminationId":"8","animal_id":"12","inseminationName":"Insemination 1","inseminationDate":"2018-10-18","inseminationDescription":"The Facebook page and press the day","recordKey":"1539851741940","crd":"2018-10-18 08:35:44","upd":"2018-10-18 08:35:44"}],"pregnancyTestList":[{"pregnancyId":"13","animal_id":"22","pregnancyName":"Pregnancy test 1","pregnancyDate":"2018-10-24","pregnancyDescription":"Thanks again and have to get a few things that I have","recordKey":"1540359821140","crd":"2018-10-24 05:43:46","upd":"2018-10-24 05:43:46"}],"calvingList":[{"calvingId":"12","animal_id":"12","calvingNumber":"hello I am 6263636646363636366","calvingDate":"2018-10-18","recordKey":"1539848598862","crd":"2018-10-18 07:43:22","upd":"2018-10-18 07:43:22"}],"weightList":[{"weightId":"27","animal_id":"12","weight":"1111","weightUnit":"kg","weightDate":"2018-10-18","recordKey":"1539851826062","crd":"2018-10-18 08:37:08","upd":"2018-10-18 08:37:08"},{"weightId":"25","animal_id":"12","weight":"1235","weightUnit":"pounds","weightDate":"2018-10-10","recordKey":"1539844642086","crd":"2018-10-18 06:37:25","upd":"2018-10-23 10:32:19"}],"parcelMovementList":[{"fromName":"Test parcel 1","fromRecordKey":"1539838785325","toName":"Test parcel 3","toRecordKey":"1540189435858","parcelMovementId":"47","animal_id":"12","movementFrom":"17","movementTo":"27","movementDate":"2018-10-22","recordKey":"1540207203662","crd":"2018-10-22 12:07:04","upd":"2018-10-22 12:07:04"},{"fromName":"Test parcel","fromRecordKey":"1539838825533","toName":"Test parcel 1","toRecordKey":"1539838785325","parcelMovementId":"22","animal_id":"12","movementFrom":"19","movementTo":"17","movementDate":"2018-10-18","recordKey":"1539851780761","crd":"2018-10-18 08:36:23","upd":"2018-10-18 08:36:23"}]}],"cropList":[{"propertyId":"4","parcelName":"Test parcel 3","parcelRecordKey":"1540189435858","cropId":"31","parcel_id":"27","cropName":"Crop 1","lotId":"Lot 1","description":"The Facebook page and the day of the","recordKey":"1540385001428","preparationDate":"0000-00-00","plantingDate":"0000-00-00","harvestDate":"0000-00-00","saleDate":"0000-00-00","salePrice":"","saleQuantity":"","saleTotalPrice":"","costFrom":"0000-00-00","costTo":"0000-00-00","costTotal":"","status":"0","crd":"2018-10-24 12:43:23","upd":"2018-10-24 12:43:23"}]},{"parcelId":"19","parcelName":"Test parcel","property_id":"4","recordKey":"1539838825533","status":"0","crd":"2018-10-18 05:00:27","upd":"2018-10-18 05:00:27","animalList":[{"propertyId":"4","parcelName":"Test parcel","parcelRecordKey":"1539838825533","animalId":"22","parcel_id":"19","lotId":"Lot 1234","tagNumber":"Tag 123","bornOrBuyDate":"2018-10-20","mother":"","boughtFrom":"Mother and I","description":"Thanks for the heads up and I will be in touch with the same thing as the same is true for you to be there at my house at all I just have to get a new phone","dismant":"2018-10-18","markingOrTagging":"2018-10-17","saleDate":"2018-10-23","salePrice":"25","saleQuantity":"65","saleTotalPrice":"1625.0","costFrom":"2018-10-01","costTo":"2018-10-24","costTotal":"25000","status":"0","recordKey":"1540358167870","crd":"2018-10-24 05:16:12","upd":"2018-10-24 07:08:43","treatmentList":[{"treatmentId":"31","treatmentName":"Treatment 3","treatmentForId":"22","treatmentFor":"1","recordKey":"1540364777515","crd":"2018-10-24 07:06:20","upd":"2018-10-24 07:06:20","mappingList":[{"treatmentMappingId":"35","treatment_id":"31","treatmentDate":"2018-10-23","treatmentDescription":"Hello my name or email us at sjdjd","recordKey":"1540364817075","crd":"2018-10-24 07:07:02","upd":"2018-10-24 07:07:11"}]},{"treatmentId":"30","treatmentName":"Treatment 2","treatmentForId":"22","treatmentFor":"1","recordKey":"1540359919024","crd":"2018-10-24 05:45:23","upd":"2018-10-24 05:45:23","mappingList":[{"treatmentMappingId":"33","treatment_id":"30","treatmentDate":"2018-10-24","treatmentDescription":"Thanks again for the opportunity and I hope you can find the right to know that you are","recordKey":"1540359953606","crd":"2018-10-24 05:45:58","upd":"2018-10-24 05:45:58"},{"treatmentMappingId":"32","treatment_id":"30","treatmentDate":"2018-10-22","treatmentDescription":"Thanks again and happy holidays to you too and your family have a","recordKey":"1540359933436","crd":"2018-10-24 05:45:37","upd":"2018-10-24 05:45:47"}]}],"inseminationList":[{"inseminationId":"22","animal_id":"22","inseminationName":"Insemination 1","inseminationDate":"2018-10-24","inseminationDescription":"The Facebook page and the other","recordKey":"1540364864271","crd":"2018-10-24 07:07:46","upd":"2018-10-24 07:07:46"},{"inseminationId":"20","animal_id":"22","inseminationName":"Insemination 1","inseminationDate":"2018-10-24","inseminationDescription":"Thanks again and happy holidays to see if I could get the info from the same company that I","recordKey":"1540359843514","crd":"2018-10-24 05:44:08","upd":"2018-10-24 05:44:08"}],"pregnancyTestList":[{"pregnancyId":"13","animal_id":"22","pregnancyName":"Pregnancy test 1","pregnancyDate":"2018-10-24","pregnancyDescription":"Thanks again and have to get a few things that I have","recordKey":"1540359821140","crd":"2018-10-24 05:43:46","upd":"2018-10-24 05:43:46"}],"calvingList":[{"calvingId":"31","animal_id":"22","calvingNumber":"the Facebook","calvingDate":"2018-10-24","recordKey":"1540364901364","crd":"2018-10-24 07:08:23","upd":"2018-10-24 07:08:23"}],"weightList":[{"weightId":"40","animal_id":"22","weight":"123","weightUnit":"pounds","weightDate":"2018-10-24","recordKey":"1540361952771","crd":"2018-10-24 06:19:17","upd":"2018-10-24 06:19:17"},{"weightId":"38","animal_id":"22","weight":"123","weightUnit":"pound","weightDate":"2018-10-24","recordKey":"1540358325710","crd":"2018-10-24 05:18:50","upd":"2018-10-24 05:18:50"}],"parcelMovementList":[{"fromName":"The Facebook","fromRecordKey":"1540364424893","toName":"Test parcel","toRecordKey":"1539838825533","parcelMovementId":"57","animal_id":"22","movementFrom":"32","movementTo":"19","movementDate":"2018-10-24","recordKey":"1540364921143","crd":"2018-10-24 07:08:43","upd":"2018-10-24 07:08:43"},{"fromName":"Test parcel 1","fromRecordKey":"1539838785325","toName":"The Facebook","toRecordKey":"1540364424893","parcelMovementId":"56","animal_id":"22","movementFrom":"17","movementTo":"32","movementDate":"2018-10-24","recordKey":"1540364587511","crd":"2018-10-24 07:03:09","upd":"2018-10-24 07:03:09"}]},{"propertyId":"4","parcelName":"Test parcel","parcelRecordKey":"1539838825533","animalId":"14","parcel_id":"19","lotId":"Lot 3","tagNumber":"Tag 3","bornOrBuyDate":"2018-10-10","mother":"Tag 2","boughtFrom":"The Facebook group","description":"The Facebook group for event professionals logo hello","dismant":"0000-00-00","markingOrTagging":"0000-00-00","saleDate":"0000-00-00","salePrice":"","saleQuantity":"","saleTotalPrice":"","costFrom":"2018-10-19","costTo":"2018-10-22","costTotal":"12","status":"0","recordKey":"1539839914909","crd":"2018-10-18 05:18:37","upd":"2018-10-22 11:14:36","treatmentList":[{"treatmentId":"28","treatmentName":"Treatment 3","treatmentForId":"14","treatmentFor":"1","recordKey":"1540304512302","crd":"2018-10-23 14:21:54","upd":"2018-10-23 14:21:54","mappingList":[{"treatmentMappingId":"31","treatment_id":"28","treatmentDate":"2018-10-18","treatmentDescription":"The Facebook platform of choice I","recordKey":"1540304523105","crd":"2018-10-23 14:22:05","upd":"2018-10-23 14:22:15"}]},{"treatmentId":"17","treatmentName":"Treatment 2","treatmentForId":"14","treatmentFor":"1","recordKey":"1539861195430","crd":"2018-10-18 11:13:18","upd":"2018-10-18 11:13:18","mappingList":[{"treatmentMappingId":"15","treatment_id":"17","treatmentDate":"2018-10-18","treatmentDescription":"The Facebook platform you also get","recordKey":"1539865434398","crd":"2018-10-18 12:23:56","upd":"2018-10-18 12:23:56"}]},{"treatmentId":"10","treatmentName":"Treatment 1","treatmentForId":"14","treatmentFor":"1","recordKey":"1539842029512","crd":"2018-10-18 05:53:51","upd":"2018-10-18 05:53:51","mappingList":[{"treatmentMappingId":"10","treatment_id":"10","treatmentDate":"2018-10-10","treatmentDescription":"The Facebook platform or email","recordKey":"1539842042342","crd":"2018-10-18 05:54:04","upd":"2018-10-18 05:54:04"}]}],"inseminationList":[{"inseminationId":"6","animal_id":"14","inseminationName":"Insemination 1","inseminationDate":"2018-10-18","inseminationDescription":"The Facebook page and the","recordKey":"1539842062163","crd":"2018-10-18 05:54:24","upd":"2018-10-18 05:54:24"}],"pregnancyTestList":[{"pregnancyId":"4","animal_id":"14","pregnancyName":"Pregnancy 1","pregnancyDate":"2018-10-18","pregnancyDescription":"Hello my name or email","recordKey":"1539842084582","crd":"2018-10-18 05:54:47","upd":"2018-10-18 05:54:47"}],"calvingList":[{"calvingId":"25","animal_id":"14","calvingNumber":"the Facebook","calvingDate":"2018-10-22","recordKey":"1540204598310","crd":"2018-10-22 11:14:37","upd":"2018-10-22 11:14:37"}],"weightList":[{"weightId":"35","animal_id":"14","weight":"12","weightUnit":"kg","weightDate":"2018-10-17","recordKey":"1540204607216","crd":"2018-10-22 11:14:38","upd":"2018-10-22 11:14:38"},{"weightId":"26","animal_id":"14","weight":"12","weightUnit":"pounds","weightDate":"2018-10-18","recordKey":"1539848692210","crd":"2018-10-18 07:44:55","upd":"2018-10-23 10:32:19"},{"weightId":"19","animal_id":"14","weight":"2500","weightUnit":"pounds","weightDate":"2018-10-18","recordKey":"1539842124695","crd":"2018-10-18 05:55:27","upd":"2018-10-23 10:32:19"}],"parcelMovementList":[]}],"cropList":[]},{"parcelId":"17","parcelName":"Test parcel 1","property_id":"4","recordKey":"1539838785325","status":"0","crd":"2018-10-18 04:59:47","upd":"2018-10-18 04:59:47","animalList":[],"cropList":[]}]
         */

        private List<PropertyListBean> propertyList;
        private List<ProductListBean> productList;
        private List<ExpenseListBean> expenseList;

        public List<PropertyListBean> getPropertyList() {
            return propertyList;
        }

        public void setPropertyList(List<PropertyListBean> propertyList) {
            this.propertyList = propertyList;
        }

        public List<ProductListBean> getProductList() {
            return productList;
        }

        public void setProductList(List<ProductListBean> productList) {
            this.productList = productList;
        }

        public List<ExpenseListBean> getExpenseList() {
            return expenseList;
        }

        public void setExpenseList(List<ExpenseListBean> expenseList) {
            this.expenseList = expenseList;
        }

        public static class PropertyListBean {
            private String propertyId;
            private String user_id;
            private String propertyName;
            private String location;
            private String longitude;
            private String latitude;
            private String size;
            private String sizeUnite;
            private String description;
            private String status;
            private String crd;
            private String upd;
            private String rainCount;
            private String parcelCount;
            private List<RaindataListBean> raindataList;
            private List<ParcelListBean> parcelList;

            public String getPropertyId() {
                return propertyId;
            }

            public void setPropertyId(String propertyId) {
                this.propertyId = propertyId;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getPropertyName() {
                return propertyName;
            }

            public void setPropertyName(String propertyName) {
                this.propertyName = propertyName;
            }

            public String getLocation() {
                return location;
            }

            public void setLocation(String location) {
                this.location = location;
            }

            public String getLongitude() {
                return longitude;
            }

            public void setLongitude(String longitude) {
                this.longitude = longitude;
            }

            public String getLatitude() {
                return latitude;
            }

            public void setLatitude(String latitude) {
                this.latitude = latitude;
            }

            public String getSize() {
                return size;
            }

            public void setSize(String size) {
                this.size = size;
            }

            public String getSizeUnite() {
                return sizeUnite;
            }

            public void setSizeUnite(String sizeUnite) {
                this.sizeUnite = sizeUnite;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getCrd() {
                return crd;
            }

            public void setCrd(String crd) {
                this.crd = crd;
            }

            public String getUpd() {
                return upd;
            }

            public void setUpd(String upd) {
                this.upd = upd;
            }

            public String getRainCount() {
                return rainCount;
            }

            public void setRainCount(String rainCount) {
                this.rainCount = rainCount;
            }

            public String getParcelCount() {
                return parcelCount;
            }

            public void setParcelCount(String parcelCount) {
                this.parcelCount = parcelCount;
            }

            public List<RaindataListBean> getRaindataList() {
                return raindataList;
            }

            public void setRaindataList(List<RaindataListBean> raindataList) {
                this.raindataList = raindataList;
            }

            public List<ParcelListBean> getParcelList() {
                return parcelList;
            }

            public void setParcelList(List<ParcelListBean> parcelList) {
                this.parcelList = parcelList;
            }

            public static class RaindataListBean {
                /**
                 * raindataId : 16
                 * property_id : 4
                 * fromDate : 2018-10-18
                 * toDate : 2018-10-18
                 * quantity : 12
                 * quantityUnite : mm
                 * status : 1
                 * crd : 2018-10-18 06:57:36
                 * upd : 2018-10-18 06:57:36
                 */

                private String raindataId;
                private String property_id;
                private String fromDate;
                private String toDate;
                private String quantity;
                private String quantityUnite;
                private String status;
                private String crd;
                private String upd;

                public String getRaindataId() {
                    return raindataId;
                }

                public void setRaindataId(String raindataId) {
                    this.raindataId = raindataId;
                }

                public String getProperty_id() {
                    return property_id;
                }

                public void setProperty_id(String property_id) {
                    this.property_id = property_id;
                }

                public String getFromDate() {
                    return fromDate;
                }

                public void setFromDate(String fromDate) {
                    this.fromDate = fromDate;
                }

                public String getToDate() {
                    return toDate;
                }

                public void setToDate(String toDate) {
                    this.toDate = toDate;
                }

                public String getQuantity() {
                    return quantity;
                }

                public void setQuantity(String quantity) {
                    this.quantity = quantity;
                }

                public String getQuantityUnite() {
                    return quantityUnite;
                }

                public void setQuantityUnite(String quantityUnite) {
                    this.quantityUnite = quantityUnite;
                }

                public String getStatus() {
                    return status;
                }

                public void setStatus(String status) {
                    this.status = status;
                }

                public String getCrd() {
                    return crd;
                }

                public void setCrd(String crd) {
                    this.crd = crd;
                }

                public String getUpd() {
                    return upd;
                }

                public void setUpd(String upd) {
                    this.upd = upd;
                }
            }

            public static class ParcelListBean {
                /**
                 * parcelId : 27
                 * parcelName : Test parcel 3
                 * property_id : 4
                 * recordKey : 1540189435858
                 * status : 0
                 * crd : 2018-10-22 09:01:17
                 * upd : 2018-10-22 09:01:17
                 * animalList : [{"propertyId":"4","parcelName":"Test parcel 3","parcelRecordKey":"1540189435858","animalId":"12","parcel_id":"27","lotId":"Lot 2","tagNumber":"Tag 2","bornOrBuyDate":"2018-10-18","mother":"Tag1","boughtFrom":"The Facebook group for event l","description":"The Facebook group for event locations in your team to score if ads are developed","dismant":"0000-00-00","markingOrTagging":"2018-10-18","saleDate":"2018-10-16","salePrice":"12","saleQuantity":"10","saleTotalPrice":"120.0","costFrom":"2018-10-11","costTo":"2018-10-20","costTotal":"23","status":"0","recordKey":"1539838872959","crd":"2018-10-18 05:01:15","upd":"2018-10-22 12:07:04","treatmentList":[{"treatmentId":"26","treatmentName":"Treatment 2","treatmentForId":"12","treatmentFor":"1","recordKey":"1540207404185","crd":"2018-10-22 12:07:03","upd":"2018-10-22 12:07:03","mappingList":[{"treatmentMappingId":"29","treatment_id":"26","treatmentDate":"2018-10-21","treatmentDescription":"The Facebook platform of choice not sure if","recordKey":"1540207428421","crd":"2018-10-22 12:07:03","upd":"2018-10-22 12:07:03"},{"treatmentMappingId":"28","treatment_id":"26","treatmentDate":"2018-10-22","treatmentDescription":"The Facebook page and press the menu or email","recordKey":"1540207411566","crd":"2018-10-22 12:07:03","upd":"2018-10-22 12:07:03"},{"treatmentMappingId":"27","treatment_id":"26","treatmentDate":"2018-10-21","treatmentDescription":"The Facebook platform of choice not sure if","recordKey":"1540207428421","crd":"2018-10-22 12:07:03","upd":"2018-10-22 12:07:03"},{"treatmentMappingId":"26","treatment_id":"26","treatmentDate":"2018-10-22","treatmentDescription":"The Facebook page and press the menu or email","recordKey":"1540207411566","crd":"2018-10-22 12:07:03","upd":"2018-10-22 12:07:03"}]}],"inseminationList":[{"inseminationId":"8","animal_id":"12","inseminationName":"Insemination 1","inseminationDate":"2018-10-18","inseminationDescription":"The Facebook page and press the day","recordKey":"1539851741940","crd":"2018-10-18 08:35:44","upd":"2018-10-18 08:35:44"}],"pregnancyTestList":[{"pregnancyId":"13","animal_id":"22","pregnancyName":"Pregnancy test 1","pregnancyDate":"2018-10-24","pregnancyDescription":"Thanks again and have to get a few things that I have","recordKey":"1540359821140","crd":"2018-10-24 05:43:46","upd":"2018-10-24 05:43:46"}],"calvingList":[{"calvingId":"12","animal_id":"12","calvingNumber":"hello I am 6263636646363636366","calvingDate":"2018-10-18","recordKey":"1539848598862","crd":"2018-10-18 07:43:22","upd":"2018-10-18 07:43:22"}],"weightList":[{"weightId":"27","animal_id":"12","weight":"1111","weightUnit":"kg","weightDate":"2018-10-18","recordKey":"1539851826062","crd":"2018-10-18 08:37:08","upd":"2018-10-18 08:37:08"},{"weightId":"25","animal_id":"12","weight":"1235","weightUnit":"pounds","weightDate":"2018-10-10","recordKey":"1539844642086","crd":"2018-10-18 06:37:25","upd":"2018-10-23 10:32:19"}],"parcelMovementList":[{"fromName":"Test parcel 1","fromRecordKey":"1539838785325","toName":"Test parcel 3","toRecordKey":"1540189435858","parcelMovementId":"47","animal_id":"12","movementFrom":"17","movementTo":"27","movementDate":"2018-10-22","recordKey":"1540207203662","crd":"2018-10-22 12:07:04","upd":"2018-10-22 12:07:04"},{"fromName":"Test parcel","fromRecordKey":"1539838825533","toName":"Test parcel 1","toRecordKey":"1539838785325","parcelMovementId":"22","animal_id":"12","movementFrom":"19","movementTo":"17","movementDate":"2018-10-18","recordKey":"1539851780761","crd":"2018-10-18 08:36:23","upd":"2018-10-18 08:36:23"}]}]
                 * cropList : [{"propertyId":"4","parcelName":"Test parcel 3","parcelRecordKey":"1540189435858","cropId":"31","parcel_id":"27","cropName":"Crop 1","lotId":"Lot 1","description":"The Facebook page and the day of the","recordKey":"1540385001428","preparationDate":"0000-00-00","plantingDate":"0000-00-00","harvestDate":"0000-00-00","saleDate":"0000-00-00","salePrice":"","saleQuantity":"","saleTotalPrice":"","costFrom":"0000-00-00","costTo":"0000-00-00","costTotal":"","status":"0","crd":"2018-10-24 12:43:23","upd":"2018-10-24 12:43:23"}]
                 */

                private String parcelId;
                private String parcelName;
                private String property_id;
                private String recordKey;
                private String status;
                private String crd;
                private String upd;
                private List<AnimalListBean> animalList;
                private List<CropListBean> cropList;

                public String getParcelId() {
                    return parcelId;
                }

                public void setParcelId(String parcelId) {
                    this.parcelId = parcelId;
                }

                public String getParcelName() {
                    return parcelName;
                }

                public void setParcelName(String parcelName) {
                    this.parcelName = parcelName;
                }

                public String getProperty_id() {
                    return property_id;
                }

                public void setProperty_id(String property_id) {
                    this.property_id = property_id;
                }

                public String getRecordKey() {
                    return recordKey;
                }

                public void setRecordKey(String recordKey) {
                    this.recordKey = recordKey;
                }

                public String getStatus() {
                    return status;
                }

                public void setStatus(String status) {
                    this.status = status;
                }

                public String getCrd() {
                    return crd;
                }

                public void setCrd(String crd) {
                    this.crd = crd;
                }

                public String getUpd() {
                    return upd;
                }

                public void setUpd(String upd) {
                    this.upd = upd;
                }

                public List<AnimalListBean> getAnimalList() {
                    return animalList;
                }

                public void setAnimalList(List<AnimalListBean> animalList) {
                    this.animalList = animalList;
                }

                public List<CropListBean> getCropList() {
                    return cropList;
                }

                public void setCropList(List<CropListBean> cropList) {
                    this.cropList = cropList;
                }

                public static class AnimalListBean {
                    /**
                     * propertyId : 4
                     * parcelName : Test parcel 3
                     * parcelRecordKey : 1540189435858
                     * animalId : 12
                     * parcel_id : 27
                     * lotId : Lot 2
                     * tagNumber : Tag 2
                     * bornOrBuyDate : 2018-10-18
                     * mother : Tag1
                     * boughtFrom : The Facebook group for event l
                     * description : The Facebook group for event locations in your team to score if ads are developed
                     * dismant : 0000-00-00
                     * markingOrTagging : 2018-10-18
                     * saleDate : 2018-10-16
                     * salePrice : 12
                     * saleQuantity : 10
                     * saleTotalPrice : 120.0
                     * costFrom : 2018-10-11
                     * costTo : 2018-10-20
                     * costTotal : 23
                     * status : 0
                     * recordKey : 1539838872959
                     * crd : 2018-10-18 05:01:15
                     * upd : 2018-10-22 12:07:04
                     * treatmentList : [{"treatmentId":"26","treatmentName":"Treatment 2","treatmentForId":"12","treatmentFor":"1","recordKey":"1540207404185","crd":"2018-10-22 12:07:03","upd":"2018-10-22 12:07:03","mappingList":[{"treatmentMappingId":"29","treatment_id":"26","treatmentDate":"2018-10-21","treatmentDescription":"The Facebook platform of choice not sure if","recordKey":"1540207428421","crd":"2018-10-22 12:07:03","upd":"2018-10-22 12:07:03"},{"treatmentMappingId":"28","treatment_id":"26","treatmentDate":"2018-10-22","treatmentDescription":"The Facebook page and press the menu or email","recordKey":"1540207411566","crd":"2018-10-22 12:07:03","upd":"2018-10-22 12:07:03"},{"treatmentMappingId":"27","treatment_id":"26","treatmentDate":"2018-10-21","treatmentDescription":"The Facebook platform of choice not sure if","recordKey":"1540207428421","crd":"2018-10-22 12:07:03","upd":"2018-10-22 12:07:03"},{"treatmentMappingId":"26","treatment_id":"26","treatmentDate":"2018-10-22","treatmentDescription":"The Facebook page and press the menu or email","recordKey":"1540207411566","crd":"2018-10-22 12:07:03","upd":"2018-10-22 12:07:03"}]}]
                     * inseminationList : [{"inseminationId":"8","animal_id":"12","inseminationName":"Insemination 1","inseminationDate":"2018-10-18","inseminationDescription":"The Facebook page and press the day","recordKey":"1539851741940","crd":"2018-10-18 08:35:44","upd":"2018-10-18 08:35:44"}]
                     * pregnancyTestList : [{"pregnancyId":"13","animal_id":"22","pregnancyName":"Pregnancy test 1","pregnancyDate":"2018-10-24","pregnancyDescription":"Thanks again and have to get a few things that I have","recordKey":"1540359821140","crd":"2018-10-24 05:43:46","upd":"2018-10-24 05:43:46"}]
                     * calvingList : [{"calvingId":"12","animal_id":"12","calvingNumber":"hello I am 6263636646363636366","calvingDate":"2018-10-18","recordKey":"1539848598862","crd":"2018-10-18 07:43:22","upd":"2018-10-18 07:43:22"}]
                     * weightList : [{"weightId":"27","animal_id":"12","weight":"1111","weightUnit":"kg","weightDate":"2018-10-18","recordKey":"1539851826062","crd":"2018-10-18 08:37:08","upd":"2018-10-18 08:37:08"},{"weightId":"25","animal_id":"12","weight":"1235","weightUnit":"pounds","weightDate":"2018-10-10","recordKey":"1539844642086","crd":"2018-10-18 06:37:25","upd":"2018-10-23 10:32:19"}]
                     * parcelMovementList : [{"fromName":"Test parcel 1","fromRecordKey":"1539838785325","toName":"Test parcel 3","toRecordKey":"1540189435858","parcelMovementId":"47","animal_id":"12","movementFrom":"17","movementTo":"27","movementDate":"2018-10-22","recordKey":"1540207203662","crd":"2018-10-22 12:07:04","upd":"2018-10-22 12:07:04"},{"fromName":"Test parcel","fromRecordKey":"1539838825533","toName":"Test parcel 1","toRecordKey":"1539838785325","parcelMovementId":"22","animal_id":"12","movementFrom":"19","movementTo":"17","movementDate":"2018-10-18","recordKey":"1539851780761","crd":"2018-10-18 08:36:23","upd":"2018-10-18 08:36:23"}]
                     */

                    private String propertyId;
                    private String parcelName;
                    private String parcelRecordKey;
                    private String animalId;
                    private String parcel_id;
                    private String lotId;
                    private String tagNumber;
                    private String bornOrBuyDate;
                    private String mother;
                    private String boughtFrom;
                    private String description;
                    private String dismant;
                    private String markingOrTagging;
                    private String saleDate;
                    private String salePrice;
                    private String saleQuantity;
                    private String saleTotalPrice;
                    private String costFrom;
                    private String costTo;
                    private String costTotal;
                    private String status;
                    private String recordKey;
                    private String crd;
                    private String upd;
                    private List<TreatmentListBean> treatmentList;
                    private List<InseminationListBean> inseminationList;
                    private List<PregnancyTestListBean> pregnancyTestList;
                    private List<CalvingListBean> calvingList;
                    private List<WeightListBean> weightList;
                    private List<ParcelMovementListBean> parcelMovementList;

                    public String getPropertyId() {
                        return propertyId;
                    }

                    public void setPropertyId(String propertyId) {
                        this.propertyId = propertyId;
                    }

                    public String getParcelName() {
                        return parcelName;
                    }

                    public void setParcelName(String parcelName) {
                        this.parcelName = parcelName;
                    }

                    public String getParcelRecordKey() {
                        return parcelRecordKey;
                    }

                    public void setParcelRecordKey(String parcelRecordKey) {
                        this.parcelRecordKey = parcelRecordKey;
                    }

                    public String getAnimalId() {
                        return animalId;
                    }

                    public void setAnimalId(String animalId) {
                        this.animalId = animalId;
                    }

                    public String getParcel_id() {
                        return parcel_id;
                    }

                    public void setParcel_id(String parcel_id) {
                        this.parcel_id = parcel_id;
                    }

                    public String getLotId() {
                        return lotId;
                    }

                    public void setLotId(String lotId) {
                        this.lotId = lotId;
                    }

                    public String getTagNumber() {
                        return tagNumber;
                    }

                    public void setTagNumber(String tagNumber) {
                        this.tagNumber = tagNumber;
                    }

                    public String getBornOrBuyDate() {
                        return bornOrBuyDate;
                    }

                    public void setBornOrBuyDate(String bornOrBuyDate) {
                        this.bornOrBuyDate = bornOrBuyDate;
                    }

                    public String getMother() {
                        return mother;
                    }

                    public void setMother(String mother) {
                        this.mother = mother;
                    }

                    public String getBoughtFrom() {
                        return boughtFrom;
                    }

                    public void setBoughtFrom(String boughtFrom) {
                        this.boughtFrom = boughtFrom;
                    }

                    public String getDescription() {
                        return description;
                    }

                    public void setDescription(String description) {
                        this.description = description;
                    }

                    public String getDismant() {
                        return dismant;
                    }

                    public void setDismant(String dismant) {
                        this.dismant = dismant;
                    }

                    public String getMarkingOrTagging() {
                        return markingOrTagging;
                    }

                    public void setMarkingOrTagging(String markingOrTagging) {
                        this.markingOrTagging = markingOrTagging;
                    }

                    public String getSaleDate() {
                        return saleDate;
                    }

                    public void setSaleDate(String saleDate) {
                        this.saleDate = saleDate;
                    }

                    public String getSalePrice() {
                        return salePrice;
                    }

                    public void setSalePrice(String salePrice) {
                        this.salePrice = salePrice;
                    }

                    public String getSaleQuantity() {
                        return saleQuantity;
                    }

                    public void setSaleQuantity(String saleQuantity) {
                        this.saleQuantity = saleQuantity;
                    }

                    public String getSaleTotalPrice() {
                        return saleTotalPrice;
                    }

                    public void setSaleTotalPrice(String saleTotalPrice) {
                        this.saleTotalPrice = saleTotalPrice;
                    }

                    public String getCostFrom() {
                        return costFrom;
                    }

                    public void setCostFrom(String costFrom) {
                        this.costFrom = costFrom;
                    }

                    public String getCostTo() {
                        return costTo;
                    }

                    public void setCostTo(String costTo) {
                        this.costTo = costTo;
                    }

                    public String getCostTotal() {
                        return costTotal;
                    }

                    public void setCostTotal(String costTotal) {
                        this.costTotal = costTotal;
                    }

                    public String getStatus() {
                        return status;
                    }

                    public void setStatus(String status) {
                        this.status = status;
                    }

                    public String getRecordKey() {
                        return recordKey;
                    }

                    public void setRecordKey(String recordKey) {
                        this.recordKey = recordKey;
                    }

                    public String getCrd() {
                        return crd;
                    }

                    public void setCrd(String crd) {
                        this.crd = crd;
                    }

                    public String getUpd() {
                        return upd;
                    }

                    public void setUpd(String upd) {
                        this.upd = upd;
                    }

                    public List<TreatmentListBean> getTreatmentList() {
                        return treatmentList;
                    }

                    public void setTreatmentList(List<TreatmentListBean> treatmentList) {
                        this.treatmentList = treatmentList;
                    }

                    public List<InseminationListBean> getInseminationList() {
                        return inseminationList;
                    }

                    public void setInseminationList(List<InseminationListBean> inseminationList) {
                        this.inseminationList = inseminationList;
                    }

                    public List<PregnancyTestListBean> getPregnancyTestList() {
                        return pregnancyTestList;
                    }

                    public void setPregnancyTestList(List<PregnancyTestListBean> pregnancyTestList) {
                        this.pregnancyTestList = pregnancyTestList;
                    }

                    public List<CalvingListBean> getCalvingList() {
                        return calvingList;
                    }

                    public void setCalvingList(List<CalvingListBean> calvingList) {
                        this.calvingList = calvingList;
                    }

                    public List<WeightListBean> getWeightList() {
                        return weightList;
                    }

                    public void setWeightList(List<WeightListBean> weightList) {
                        this.weightList = weightList;
                    }

                    public List<ParcelMovementListBean> getParcelMovementList() {
                        return parcelMovementList;
                    }

                    public void setParcelMovementList(List<ParcelMovementListBean> parcelMovementList) {
                        this.parcelMovementList = parcelMovementList;
                    }

                    public static class TreatmentListBean {
                        /**
                         * treatmentId : 26
                         * treatmentName : Treatment 2
                         * treatmentForId : 12
                         * treatmentFor : 1
                         * recordKey : 1540207404185
                         * crd : 2018-10-22 12:07:03
                         * upd : 2018-10-22 12:07:03
                         * mappingList : [{"treatmentMappingId":"29","treatment_id":"26","treatmentDate":"2018-10-21","treatmentDescription":"The Facebook platform of choice not sure if","recordKey":"1540207428421","crd":"2018-10-22 12:07:03","upd":"2018-10-22 12:07:03"},{"treatmentMappingId":"28","treatment_id":"26","treatmentDate":"2018-10-22","treatmentDescription":"The Facebook page and press the menu or email","recordKey":"1540207411566","crd":"2018-10-22 12:07:03","upd":"2018-10-22 12:07:03"},{"treatmentMappingId":"27","treatment_id":"26","treatmentDate":"2018-10-21","treatmentDescription":"The Facebook platform of choice not sure if","recordKey":"1540207428421","crd":"2018-10-22 12:07:03","upd":"2018-10-22 12:07:03"},{"treatmentMappingId":"26","treatment_id":"26","treatmentDate":"2018-10-22","treatmentDescription":"The Facebook page and press the menu or email","recordKey":"1540207411566","crd":"2018-10-22 12:07:03","upd":"2018-10-22 12:07:03"}]
                         */

                        private String treatmentId;
                        private String treatmentName;
                        private String treatmentForId;
                        private String treatmentFor;
                        private String recordKey;
                        private String crd;
                        private String upd;
                        private List<MappingListBean> mappingList;

                        public String getTreatmentId() {
                            return treatmentId;
                        }

                        public void setTreatmentId(String treatmentId) {
                            this.treatmentId = treatmentId;
                        }

                        public String getTreatmentName() {
                            return treatmentName;
                        }

                        public void setTreatmentName(String treatmentName) {
                            this.treatmentName = treatmentName;
                        }

                        public String getTreatmentForId() {
                            return treatmentForId;
                        }

                        public void setTreatmentForId(String treatmentForId) {
                            this.treatmentForId = treatmentForId;
                        }

                        public String getTreatmentFor() {
                            return treatmentFor;
                        }

                        public void setTreatmentFor(String treatmentFor) {
                            this.treatmentFor = treatmentFor;
                        }

                        public String getRecordKey() {
                            return recordKey;
                        }

                        public void setRecordKey(String recordKey) {
                            this.recordKey = recordKey;
                        }

                        public String getCrd() {
                            return crd;
                        }

                        public void setCrd(String crd) {
                            this.crd = crd;
                        }

                        public String getUpd() {
                            return upd;
                        }

                        public void setUpd(String upd) {
                            this.upd = upd;
                        }

                        public List<MappingListBean> getMappingList() {
                            return mappingList;
                        }

                        public void setMappingList(List<MappingListBean> mappingList) {
                            this.mappingList = mappingList;
                        }

                        public static class MappingListBean {
                            /**
                             * treatmentMappingId : 29
                             * treatment_id : 26
                             * treatmentDate : 2018-10-21
                             * treatmentDescription : The Facebook platform of choice not sure if
                             * recordKey : 1540207428421
                             * crd : 2018-10-22 12:07:03
                             * upd : 2018-10-22 12:07:03
                             */

                            private String treatmentMappingId;
                            private String treatment_id;
                            private String treatmentDate;
                            private String treatmentDescription;
                            private String recordKey;
                            private String crd;
                            private String upd;

                            public String getTreatmentMappingId() {
                                return treatmentMappingId;
                            }

                            public void setTreatmentMappingId(String treatmentMappingId) {
                                this.treatmentMappingId = treatmentMappingId;
                            }

                            public String getTreatment_id() {
                                return treatment_id;
                            }

                            public void setTreatment_id(String treatment_id) {
                                this.treatment_id = treatment_id;
                            }

                            public String getTreatmentDate() {
                                return treatmentDate;
                            }

                            public void setTreatmentDate(String treatmentDate) {
                                this.treatmentDate = treatmentDate;
                            }

                            public String getTreatmentDescription() {
                                return treatmentDescription;
                            }

                            public void setTreatmentDescription(String treatmentDescription) {
                                this.treatmentDescription = treatmentDescription;
                            }

                            public String getRecordKey() {
                                return recordKey;
                            }

                            public void setRecordKey(String recordKey) {
                                this.recordKey = recordKey;
                            }

                            public String getCrd() {
                                return crd;
                            }

                            public void setCrd(String crd) {
                                this.crd = crd;
                            }

                            public String getUpd() {
                                return upd;
                            }

                            public void setUpd(String upd) {
                                this.upd = upd;
                            }
                        }
                    }

                    public static class InseminationListBean {
                        /**
                         * inseminationId : 8
                         * animal_id : 12
                         * inseminationName : Insemination 1
                         * inseminationDate : 2018-10-18
                         * inseminationDescription : The Facebook page and press the day
                         * recordKey : 1539851741940
                         * crd : 2018-10-18 08:35:44
                         * upd : 2018-10-18 08:35:44
                         */

                        private String inseminationId;
                        private String animal_id;
                        private String inseminationName;
                        private String inseminationDate;
                        private String inseminationDescription;
                        private String recordKey;
                        private String crd;
                        private String upd;

                        public String getInseminationId() {
                            return inseminationId;
                        }

                        public void setInseminationId(String inseminationId) {
                            this.inseminationId = inseminationId;
                        }

                        public String getAnimal_id() {
                            return animal_id;
                        }

                        public void setAnimal_id(String animal_id) {
                            this.animal_id = animal_id;
                        }

                        public String getInseminationName() {
                            return inseminationName;
                        }

                        public void setInseminationName(String inseminationName) {
                            this.inseminationName = inseminationName;
                        }

                        public String getInseminationDate() {
                            return inseminationDate;
                        }

                        public void setInseminationDate(String inseminationDate) {
                            this.inseminationDate = inseminationDate;
                        }

                        public String getInseminationDescription() {
                            return inseminationDescription;
                        }

                        public void setInseminationDescription(String inseminationDescription) {
                            this.inseminationDescription = inseminationDescription;
                        }

                        public String getRecordKey() {
                            return recordKey;
                        }

                        public void setRecordKey(String recordKey) {
                            this.recordKey = recordKey;
                        }

                        public String getCrd() {
                            return crd;
                        }

                        public void setCrd(String crd) {
                            this.crd = crd;
                        }

                        public String getUpd() {
                            return upd;
                        }

                        public void setUpd(String upd) {
                            this.upd = upd;
                        }
                    }

                    public static class PregnancyTestListBean {
                        /**
                         * pregnancyId : 13
                         * animal_id : 22
                         * pregnancyName : Pregnancy test 1
                         * pregnancyDate : 2018-10-24
                         * pregnancyDescription : Thanks again and have to get a few things that I have
                         * recordKey : 1540359821140
                         * crd : 2018-10-24 05:43:46
                         * upd : 2018-10-24 05:43:46
                         */

                        private String pregnancyId;
                        private String animal_id;
                        private String pregnancyName;
                        private String pregnancyDate;
                        private String pregnancyDescription;
                        private String recordKey;
                        private String crd;
                        private String upd;

                        public String getPregnancyId() {
                            return pregnancyId;
                        }

                        public void setPregnancyId(String pregnancyId) {
                            this.pregnancyId = pregnancyId;
                        }

                        public String getAnimal_id() {
                            return animal_id;
                        }

                        public void setAnimal_id(String animal_id) {
                            this.animal_id = animal_id;
                        }

                        public String getPregnancyName() {
                            return pregnancyName;
                        }

                        public void setPregnancyName(String pregnancyName) {
                            this.pregnancyName = pregnancyName;
                        }

                        public String getPregnancyDate() {
                            return pregnancyDate;
                        }

                        public void setPregnancyDate(String pregnancyDate) {
                            this.pregnancyDate = pregnancyDate;
                        }

                        public String getPregnancyDescription() {
                            return pregnancyDescription;
                        }

                        public void setPregnancyDescription(String pregnancyDescription) {
                            this.pregnancyDescription = pregnancyDescription;
                        }

                        public String getRecordKey() {
                            return recordKey;
                        }

                        public void setRecordKey(String recordKey) {
                            this.recordKey = recordKey;
                        }

                        public String getCrd() {
                            return crd;
                        }

                        public void setCrd(String crd) {
                            this.crd = crd;
                        }

                        public String getUpd() {
                            return upd;
                        }

                        public void setUpd(String upd) {
                            this.upd = upd;
                        }
                    }

                    public static class CalvingListBean {
                        /**
                         * calvingId : 12
                         * animal_id : 12
                         * calvingNumber : hello I am 6263636646363636366
                         * calvingDate : 2018-10-18
                         * recordKey : 1539848598862
                         * crd : 2018-10-18 07:43:22
                         * upd : 2018-10-18 07:43:22
                         */

                        private String calvingId;
                        private String animal_id;
                        private String calvingNumber;
                        private String calvingDate;
                        private String recordKey;
                        private String crd;
                        private String upd;

                        public String getCalvingId() {
                            return calvingId;
                        }

                        public void setCalvingId(String calvingId) {
                            this.calvingId = calvingId;
                        }

                        public String getAnimal_id() {
                            return animal_id;
                        }

                        public void setAnimal_id(String animal_id) {
                            this.animal_id = animal_id;
                        }

                        public String getCalvingNumber() {
                            return calvingNumber;
                        }

                        public void setCalvingNumber(String calvingNumber) {
                            this.calvingNumber = calvingNumber;
                        }

                        public String getCalvingDate() {
                            return calvingDate;
                        }

                        public void setCalvingDate(String calvingDate) {
                            this.calvingDate = calvingDate;
                        }

                        public String getRecordKey() {
                            return recordKey;
                        }

                        public void setRecordKey(String recordKey) {
                            this.recordKey = recordKey;
                        }

                        public String getCrd() {
                            return crd;
                        }

                        public void setCrd(String crd) {
                            this.crd = crd;
                        }

                        public String getUpd() {
                            return upd;
                        }

                        public void setUpd(String upd) {
                            this.upd = upd;
                        }
                    }

                    public static class WeightListBean {
                        /**
                         * weightId : 27
                         * animal_id : 12
                         * weight : 1111
                         * weightUnit : kg
                         * weightDate : 2018-10-18
                         * recordKey : 1539851826062
                         * crd : 2018-10-18 08:37:08
                         * upd : 2018-10-18 08:37:08
                         */

                        private String weightId;
                        private String animal_id;
                        private String weight;
                        private String weightUnit;
                        private String weightDate;
                        private String recordKey;
                        private String crd;
                        private String upd;

                        public String getWeightId() {
                            return weightId;
                        }

                        public void setWeightId(String weightId) {
                            this.weightId = weightId;
                        }

                        public String getAnimal_id() {
                            return animal_id;
                        }

                        public void setAnimal_id(String animal_id) {
                            this.animal_id = animal_id;
                        }

                        public String getWeight() {
                            return weight;
                        }

                        public void setWeight(String weight) {
                            this.weight = weight;
                        }

                        public String getWeightUnit() {
                            return weightUnit;
                        }

                        public void setWeightUnit(String weightUnit) {
                            this.weightUnit = weightUnit;
                        }

                        public String getWeightDate() {
                            return weightDate;
                        }

                        public void setWeightDate(String weightDate) {
                            this.weightDate = weightDate;
                        }

                        public String getRecordKey() {
                            return recordKey;
                        }

                        public void setRecordKey(String recordKey) {
                            this.recordKey = recordKey;
                        }

                        public String getCrd() {
                            return crd;
                        }

                        public void setCrd(String crd) {
                            this.crd = crd;
                        }

                        public String getUpd() {
                            return upd;
                        }

                        public void setUpd(String upd) {
                            this.upd = upd;
                        }
                    }

                    public static class ParcelMovementListBean {
                        /**
                         * fromName : Test parcel 1
                         * fromRecordKey : 1539838785325
                         * toName : Test parcel 3
                         * toRecordKey : 1540189435858
                         * parcelMovementId : 47
                         * animal_id : 12
                         * movementFrom : 17
                         * movementTo : 27
                         * movementDate : 2018-10-22
                         * recordKey : 1540207203662
                         * crd : 2018-10-22 12:07:04
                         * upd : 2018-10-22 12:07:04
                         */

                        private String fromName;
                        private String fromRecordKey;
                        private String toName;
                        private String toRecordKey;
                        private String parcelMovementId;
                        private String animal_id;
                        private String movementFrom;
                        private String movementTo;
                        private String movementDate;
                        private String recordKey;
                        private String crd;
                        private String upd;

                        public String getFromName() {
                            return fromName;
                        }

                        public void setFromName(String fromName) {
                            this.fromName = fromName;
                        }

                        public String getFromRecordKey() {
                            return fromRecordKey;
                        }

                        public void setFromRecordKey(String fromRecordKey) {
                            this.fromRecordKey = fromRecordKey;
                        }

                        public String getToName() {
                            return toName;
                        }

                        public void setToName(String toName) {
                            this.toName = toName;
                        }

                        public String getToRecordKey() {
                            return toRecordKey;
                        }

                        public void setToRecordKey(String toRecordKey) {
                            this.toRecordKey = toRecordKey;
                        }

                        public String getParcelMovementId() {
                            return parcelMovementId;
                        }

                        public void setParcelMovementId(String parcelMovementId) {
                            this.parcelMovementId = parcelMovementId;
                        }

                        public String getAnimal_id() {
                            return animal_id;
                        }

                        public void setAnimal_id(String animal_id) {
                            this.animal_id = animal_id;
                        }

                        public String getMovementFrom() {
                            return movementFrom;
                        }

                        public void setMovementFrom(String movementFrom) {
                            this.movementFrom = movementFrom;
                        }

                        public String getMovementTo() {
                            return movementTo;
                        }

                        public void setMovementTo(String movementTo) {
                            this.movementTo = movementTo;
                        }

                        public String getMovementDate() {
                            return movementDate;
                        }

                        public void setMovementDate(String movementDate) {
                            this.movementDate = movementDate;
                        }

                        public String getRecordKey() {
                            return recordKey;
                        }

                        public void setRecordKey(String recordKey) {
                            this.recordKey = recordKey;
                        }

                        public String getCrd() {
                            return crd;
                        }

                        public void setCrd(String crd) {
                            this.crd = crd;
                        }

                        public String getUpd() {
                            return upd;
                        }

                        public void setUpd(String upd) {
                            this.upd = upd;
                        }
                    }
                }

                public static class CropListBean {
                    /**
                     * propertyId : 4
                     * parcelName : Test parcel 3
                     * parcelRecordKey : 1540189435858
                     * cropId : 31
                     * parcel_id : 27
                     * cropName : Crop 1
                     * lotId : Lot 1
                     * description : The Facebook page and the day of the
                     * recordKey : 1540385001428
                     * preparationDate : 0000-00-00
                     * plantingDate : 0000-00-00
                     * harvestDate : 0000-00-00
                     * saleDate : 0000-00-00
                     * salePrice :
                     * saleQuantity :
                     * saleTotalPrice :
                     * costFrom : 0000-00-00
                     * costTo : 0000-00-00
                     * costTotal :
                     * status : 0
                     * crd : 2018-10-24 12:43:23
                     * upd : 2018-10-24 12:43:23
                     */

                    private String propertyId;
                    private String parcelName;
                    private String parcelRecordKey;
                    private String cropId;
                    private String parcel_id;
                    private String cropName;
                    private String lotId;
                    private String description;
                    private String recordKey;
                    private String preparationDate;
                    private String plantingDate;
                    private String harvestDate;
                    private String saleDate;
                    private String salePrice;
                    private String saleQuantity;
                    private String saleTotalPrice;
                    private String costFrom;
                    private String costTo;
                    private String costTotal;
                    private String status;
                    private String crd;
                    private String upd;
                    private List<TreatmentListBean> treatmentList;

                    public String getPropertyId() {
                        return propertyId;
                    }

                    public void setPropertyId(String propertyId) {
                        this.propertyId = propertyId;
                    }

                    public String getParcelName() {
                        return parcelName;
                    }

                    public void setParcelName(String parcelName) {
                        this.parcelName = parcelName;
                    }

                    public String getParcelRecordKey() {
                        return parcelRecordKey;
                    }

                    public void setParcelRecordKey(String parcelRecordKey) {
                        this.parcelRecordKey = parcelRecordKey;
                    }

                    public String getCropId() {
                        return cropId;
                    }

                    public void setCropId(String cropId) {
                        this.cropId = cropId;
                    }

                    public String getParcel_id() {
                        return parcel_id;
                    }

                    public void setParcel_id(String parcel_id) {
                        this.parcel_id = parcel_id;
                    }

                    public String getCropName() {
                        return cropName;
                    }

                    public void setCropName(String cropName) {
                        this.cropName = cropName;
                    }

                    public String getLotId() {
                        return lotId;
                    }

                    public void setLotId(String lotId) {
                        this.lotId = lotId;
                    }

                    public String getDescription() {
                        return description;
                    }

                    public void setDescription(String description) {
                        this.description = description;
                    }

                    public String getRecordKey() {
                        return recordKey;
                    }

                    public void setRecordKey(String recordKey) {
                        this.recordKey = recordKey;
                    }

                    public String getPreparationDate() {
                        return preparationDate;
                    }

                    public void setPreparationDate(String preparationDate) {
                        this.preparationDate = preparationDate;
                    }

                    public String getPlantingDate() {
                        return plantingDate;
                    }

                    public void setPlantingDate(String plantingDate) {
                        this.plantingDate = plantingDate;
                    }

                    public String getHarvestDate() {
                        return harvestDate;
                    }

                    public void setHarvestDate(String harvestDate) {
                        this.harvestDate = harvestDate;
                    }

                    public String getSaleDate() {
                        return saleDate;
                    }

                    public void setSaleDate(String saleDate) {
                        this.saleDate = saleDate;
                    }

                    public String getSalePrice() {
                        return salePrice;
                    }

                    public void setSalePrice(String salePrice) {
                        this.salePrice = salePrice;
                    }

                    public String getSaleQuantity() {
                        return saleQuantity;
                    }

                    public void setSaleQuantity(String saleQuantity) {
                        this.saleQuantity = saleQuantity;
                    }

                    public String getSaleTotalPrice() {
                        return saleTotalPrice;
                    }

                    public void setSaleTotalPrice(String saleTotalPrice) {
                        this.saleTotalPrice = saleTotalPrice;
                    }

                    public String getCostFrom() {
                        return costFrom;
                    }

                    public void setCostFrom(String costFrom) {
                        this.costFrom = costFrom;
                    }

                    public String getCostTo() {
                        return costTo;
                    }

                    public void setCostTo(String costTo) {
                        this.costTo = costTo;
                    }

                    public String getCostTotal() {
                        return costTotal;
                    }

                    public void setCostTotal(String costTotal) {
                        this.costTotal = costTotal;
                    }

                    public String getStatus() {
                        return status;
                    }

                    public void setStatus(String status) {
                        this.status = status;
                    }

                    public String getCrd() {
                        return crd;
                    }

                    public void setCrd(String crd) {
                        this.crd = crd;
                    }

                    public String getUpd() {
                        return upd;
                    }

                    public void setUpd(String upd) {
                        this.upd = upd;
                    }

                    public List<TreatmentListBean> getTreatmentList() {
                        return treatmentList;
                    }

                    public void setTreatmentList(List<TreatmentListBean> treatmentList) {
                        this.treatmentList = treatmentList;
                    }

                    public static class TreatmentListBean {
                        /**
                         * treatmentId : 39
                         * crop_id : 71
                         * treatmentName : Test 3
                         * recordKey : 1543326968144
                         * crd : 2018-11-27 13:56:10
                         * upd : 2018-11-27 14:11:28
                         * mappingList : []
                         */

                        private String treatmentId;
                        private String crop_id;
                        private String treatmentName;
                        private String recordKey;
                        private String crd;
                        private String upd;
                        private List<MappingListBean> mappingList;

                        public String getTreatmentId() {
                            return treatmentId;
                        }

                        public void setTreatmentId(String treatmentId) {
                            this.treatmentId = treatmentId;
                        }

                        public String getCrop_id() {
                            return crop_id;
                        }

                        public void setCrop_id(String crop_id) {
                            this.crop_id = crop_id;
                        }

                        public String getTreatmentName() {
                            return treatmentName;
                        }

                        public void setTreatmentName(String treatmentName) {
                            this.treatmentName = treatmentName;
                        }

                        public String getRecordKey() {
                            return recordKey;
                        }

                        public void setRecordKey(String recordKey) {
                            this.recordKey = recordKey;
                        }

                        public String getCrd() {
                            return crd;
                        }

                        public void setCrd(String crd) {
                            this.crd = crd;
                        }

                        public String getUpd() {
                            return upd;
                        }

                        public void setUpd(String upd) {
                            this.upd = upd;
                        }

                        public List<MappingListBean> getMappingList() {
                            return mappingList;
                        }

                        public void setMappingList(List<MappingListBean> mappingList) {
                            this.mappingList = mappingList;
                        }

                        public static class MappingListBean {
                            /**
                             * treatmentMappingId : 63
                             * treatment_id : 38
                             * treatmentDate : 2018-11-21
                             * treatmentDescription : The
                             * recordKey : 1543471516602
                             * crd : 2018-11-29 06:05:39
                             * upd : 2018-11-29 06:05:39
                             */

                            private String treatmentMappingId;
                            private String treatment_id;
                            private String treatmentDate;
                            private String treatmentDescription;
                            private String recordKey;
                            private String crd;
                            private String upd;

                            public String getTreatmentMappingId() {
                                return treatmentMappingId;
                            }

                            public void setTreatmentMappingId(String treatmentMappingId) {
                                this.treatmentMappingId = treatmentMappingId;
                            }

                            public String getTreatment_id() {
                                return treatment_id;
                            }

                            public void setTreatment_id(String treatment_id) {
                                this.treatment_id = treatment_id;
                            }

                            public String getTreatmentDate() {
                                return treatmentDate;
                            }

                            public void setTreatmentDate(String treatmentDate) {
                                this.treatmentDate = treatmentDate;
                            }

                            public String getTreatmentDescription() {
                                return treatmentDescription;
                            }

                            public void setTreatmentDescription(String treatmentDescription) {
                                this.treatmentDescription = treatmentDescription;
                            }

                            public String getRecordKey() {
                                return recordKey;
                            }

                            public void setRecordKey(String recordKey) {
                                this.recordKey = recordKey;
                            }

                            public String getCrd() {
                                return crd;
                            }

                            public void setCrd(String crd) {
                                this.crd = crd;
                            }

                            public String getUpd() {
                                return upd;
                            }

                            public void setUpd(String upd) {
                                this.upd = upd;
                            }
                        }
                    }
                }
            }

        }

        public static class ProductListBean {
            /**
             * productId : 66
             * productName : Tomato
             * productImage : https://dev.agrinvestonline.com//uploads/product/5ZoKrOsGxBSzCXRv.jpg
             * price : 5000
             * unit : packet
             * quantity : 1500
             * description : Test the Product details
             * recordKey : 3820531542276934
             * user_id : 0
             * userType : admin
             * status : 1
             * crd : 2018-11-15 10:15:34
             * upd : 2018-11-15 10:15:34
             * "productImageThumb":"https:\/\/dev.agrinvestonline.com\/\/uploads\/product\/thumb\/xbC40hMXu3Y1Ja6N.jpg"
             */

            private String productId;
            private String productName;
            private String productImage;
            private String price;
            private String unit;
            private String quantity;
            private String description;
            private String recordKey;
            private String user_id;
            private String userType;
            private String status;
            private String crd;
            private String upd;
            private String productImageThumb;

            public String getProductId() {
                return productId;
            }

            public void setProductId(String productId) {
                this.productId = productId;
            }

            public String getProductName() {
                return productName;
            }

            public void setProductName(String productName) {
                this.productName = productName;
            }

            public String getProductImage() {
                return productImage;
            }

            public void setProductImage(String productImage) {
                this.productImage = productImage;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getUnit() {
                return unit;
            }

            public void setUnit(String unit) {
                this.unit = unit;
            }

            public String getQuantity() {
                return quantity;
            }

            public void setQuantity(String quantity) {
                this.quantity = quantity;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getRecordKey() {
                return recordKey;
            }

            public void setRecordKey(String recordKey) {
                this.recordKey = recordKey;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getUserType() {
                return userType;
            }

            public void setUserType(String userType) {
                this.userType = userType;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getCrd() {
                return crd;
            }

            public void setCrd(String crd) {
                this.crd = crd;
            }

            public String getUpd() {
                return upd;
            }

            public void setUpd(String upd) {
                this.upd = upd;
            }

            public String getProductImageThumb() {
                return productImageThumb;
            }

            public void setProductImageThumb(String productImageThumb) {
                this.productImageThumb = productImageThumb;
            }
        }

        public static class ExpenseListBean {
            /**
             * expenseId : 102
             * user_id : 1
             * dateBought : 2018-11-01
             * cost : 500000
             * description : Yesterday Was My Favorite Thing In Life That
             * dateApplied : 2018-11-23
             * expenseFor : animal
             * recordKey : 1542981904139
             * status : 1
             * crd : 2018-11-23 14:05:04
             * upd : 2018-11-23 14:05:04
             * productCount : 3
             * productName : Tomato
             * appliedOn : [{"appliedName":"The Facebook","appliedOnRecordKey":"28","expenseMappingId":"349","expense_id":"102","appliedOn":"property","appliedOnId":"28","status":"1","crd":"2018-11-23 14:05:27","upd":"2018-11-23 14:05:27"},{"appliedName":"The day","appliedOnRecordKey":"29","expenseMappingId":"350","expense_id":"102","appliedOn":"property","appliedOnId":"29","status":"1","crd":"2018-11-23 14:05:27","upd":"2018-11-23 14:05:27"},{"appliedName":"The test","appliedOnRecordKey":"30","expenseMappingId":"351","expense_id":"102","appliedOn":"property","appliedOnId":"30","status":"1","crd":"2018-11-23 14:05:27","upd":"2018-11-23 14:05:27"}]
             * productDetails : [{"productName":"Tomato","productRecordKey":"3820531542276934","expenseProductMappingId":"354","expense_id":"102","product_id":"66","quantity":"100","price":"5000","status":"1","crd":"2018-11-23 14:05:27","upd":"2018-11-23 14:05:27"}]
             */

            private String expenseId;
            private String user_id;
            private String dateBought;
            private String cost;
            private String description;
            private String dateApplied;
            private String expenseFor;
            private String recordKey;
            private String status;
            private String crd;
            private String upd;
            private String productCount;
            private String productName;
            private List<AppliedOnBean> appliedOn;
            private List<ProductDetailsBean> productDetails;

            public String getExpenseId() {
                return expenseId;
            }

            public void setExpenseId(String expenseId) {
                this.expenseId = expenseId;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getDateBought() {
                return dateBought;
            }

            public void setDateBought(String dateBought) {
                this.dateBought = dateBought;
            }

            public String getCost() {
                return cost;
            }

            public void setCost(String cost) {
                this.cost = cost;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getDateApplied() {
                return dateApplied;
            }

            public void setDateApplied(String dateApplied) {
                this.dateApplied = dateApplied;
            }

            public String getExpenseFor() {
                return expenseFor;
            }

            public void setExpenseFor(String expenseFor) {
                this.expenseFor = expenseFor;
            }

            public String getRecordKey() {
                return recordKey;
            }

            public void setRecordKey(String recordKey) {
                this.recordKey = recordKey;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getCrd() {
                return crd;
            }

            public void setCrd(String crd) {
                this.crd = crd;
            }

            public String getUpd() {
                return upd;
            }

            public void setUpd(String upd) {
                this.upd = upd;
            }

            public String getProductCount() {
                return productCount;
            }

            public void setProductCount(String productCount) {
                this.productCount = productCount;
            }

            public String getProductName() {
                return productName;
            }

            public void setProductName(String productName) {
                this.productName = productName;
            }

            public List<AppliedOnBean> getAppliedOn() {
                return appliedOn;
            }

            public void setAppliedOn(List<AppliedOnBean> appliedOn) {
                this.appliedOn = appliedOn;
            }

            public List<ProductDetailsBean> getProductDetails() {
                return productDetails;
            }

            public void setProductDetails(List<ProductDetailsBean> productDetails) {
                this.productDetails = productDetails;
            }

            public static class AppliedOnBean {
                /**
                 * appliedName : The Facebook
                 * appliedOnRecordKey : 28
                 * expenseMappingId : 349
                 * expense_id : 102
                 * appliedOn : property
                 * appliedOnId : 28
                 * status : 1
                 * crd : 2018-11-23 14:05:27
                 * upd : 2018-11-23 14:05:27
                 */

                private String appliedName;
                private String appliedOnRecordKey;
                private String expenseMappingId;
                private String expense_id;
                private String appliedOn;
                private String appliedOnId;
                private String status;
                private String crd;
                private String upd;

                public String getAppliedName() {
                    return appliedName;
                }

                public void setAppliedName(String appliedName) {
                    this.appliedName = appliedName;
                }

                public String getAppliedOnRecordKey() {
                    return appliedOnRecordKey;
                }

                public void setAppliedOnRecordKey(String appliedOnRecordKey) {
                    this.appliedOnRecordKey = appliedOnRecordKey;
                }

                public String getExpenseMappingId() {
                    return expenseMappingId;
                }

                public void setExpenseMappingId(String expenseMappingId) {
                    this.expenseMappingId = expenseMappingId;
                }

                public String getExpense_id() {
                    return expense_id;
                }

                public void setExpense_id(String expense_id) {
                    this.expense_id = expense_id;
                }

                public String getAppliedOn() {
                    return appliedOn;
                }

                public void setAppliedOn(String appliedOn) {
                    this.appliedOn = appliedOn;
                }

                public String getAppliedOnId() {
                    return appliedOnId;
                }

                public void setAppliedOnId(String appliedOnId) {
                    this.appliedOnId = appliedOnId;
                }

                public String getStatus() {
                    return status;
                }

                public void setStatus(String status) {
                    this.status = status;
                }

                public String getCrd() {
                    return crd;
                }

                public void setCrd(String crd) {
                    this.crd = crd;
                }

                public String getUpd() {
                    return upd;
                }

                public void setUpd(String upd) {
                    this.upd = upd;
                }
            }

            public static class ProductDetailsBean {
                /**
                 * productName : Tomato
                 * productRecordKey : 3820531542276934
                 * expenseProductMappingId : 354
                 * expense_id : 102
                 * product_id : 66
                 * quantity : 100
                 * price : 5000
                 * status : 1
                 * crd : 2018-11-23 14:05:27
                 * upd : 2018-11-23 14:05:27
                 */

                private String productName;
                private String productRecordKey;
                private String expenseProductMappingId;
                private String expense_id;
                private String product_id;
                private String quantity;
                private String price;
                private String status;
                private String crd;
                private String upd;

                public String getProductName() {
                    return productName;
                }

                public void setProductName(String productName) {
                    this.productName = productName;
                }

                public String getProductRecordKey() {
                    return productRecordKey;
                }

                public void setProductRecordKey(String productRecordKey) {
                    this.productRecordKey = productRecordKey;
                }

                public String getExpenseProductMappingId() {
                    return expenseProductMappingId;
                }

                public void setExpenseProductMappingId(String expenseProductMappingId) {
                    this.expenseProductMappingId = expenseProductMappingId;
                }

                public String getExpense_id() {
                    return expense_id;
                }

                public void setExpense_id(String expense_id) {
                    this.expense_id = expense_id;
                }

                public String getProduct_id() {
                    return product_id;
                }

                public void setProduct_id(String product_id) {
                    this.product_id = product_id;
                }

                public String getQuantity() {
                    return quantity;
                }

                public void setQuantity(String quantity) {
                    this.quantity = quantity;
                }

                public String getPrice() {
                    return price;
                }

                public void setPrice(String price) {
                    this.price = price;
                }

                public String getStatus() {
                    return status;
                }

                public void setStatus(String status) {
                    this.status = status;
                }

                public String getCrd() {
                    return crd;
                }

                public void setCrd(String crd) {
                    this.crd = crd;
                }

                public String getUpd() {
                    return upd;
                }

                public void setUpd(String upd) {
                    this.upd = upd;
                }
            }
        }
    }
}
