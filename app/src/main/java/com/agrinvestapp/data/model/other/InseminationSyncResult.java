package com.agrinvestapp.data.model.other;

/**
 * Created by hemant
 * Date: 9/10/18.
 */

public final class InseminationSyncResult {

    private String requestType;

    private String recordKey; //Animal recordKey

    private String inseminationName;

    private String inseminationRecordKey;

    private String inseminationId;

    private String inseminationDate;

    private String inseminationDescription;

    private Boolean isDataSync;

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getRecordKey() {
        return recordKey;
    }

    public void setRecordKey(String recordKey) {
        this.recordKey = recordKey;
    }

    public String getInseminationName() {
        return inseminationName;
    }

    public void setInseminationName(String inseminationName) {
        this.inseminationName = inseminationName;
    }

    public String getInseminationRecordKey() {
        return inseminationRecordKey;
    }

    public void setInseminationRecordKey(String inseminationRecordKey) {
        this.inseminationRecordKey = inseminationRecordKey;
    }

    public String getInseminationId() {
        return inseminationId;
    }

    public void setInseminationId(String inseminationId) {
        this.inseminationId = inseminationId;
    }

    public String getInseminationDate() {
        return inseminationDate;
    }

    public void setInseminationDate(String inseminationDate) {
        this.inseminationDate = inseminationDate;
    }

    public String getInseminationDescription() {
        return inseminationDescription;
    }

    public void setInseminationDescription(String inseminationDescription) {
        this.inseminationDescription = inseminationDescription;
    }

    public Boolean getDataSync() {
        return isDataSync;
    }

    public void setDataSync(Boolean dataSync) {
        isDataSync = dataSync;
    }
}
