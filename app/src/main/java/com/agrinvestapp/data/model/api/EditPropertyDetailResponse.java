package com.agrinvestapp.data.model.api;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public final class EditPropertyDetailResponse {

    /**
     * status : success
     * message : Property detail
     * propertyData : {"propertyId":"454","propertyName":"Testing 23","location":"5-C, Godhra Hwy, Brajeshwari Extension, Greater Brajeshwari, Indore, Madhya Pradesh 452001, India","longitude":"75.90839769691229","latitude":"22.706328101575465","size":"25","sizeUnite":"hectare","description":"hello world with programming APIs in the future of our users to virtually go anywhere in the future of our"}
     * propertyRainData : [{"raindataId":"57","property_id":"454","fromDate":"2018-09-10","toDate":"2018-09-12","quantity":"20","quantityUnite":"mm"},{"raindataId":"58","property_id":"454","fromDate":"2018-09-12","toDate":"0000-00-00","quantity":"23","quantityUnite":"mm"},{"raindataId":"59","property_id":"454","fromDate":"2018-09-11","toDate":"0000-00-00","quantity":"45","quantityUnite":"mm"},{"raindataId":"60","property_id":"454","fromDate":"2018-09-15","toDate":"0000-00-00","quantity":"46","quantityUnite":"mm"}]
     */

    private String status;
    private String message;
    private PropertyDataBean propertyData;
    @SerializedName("propertyRainData")
    private List<RainBeanResponse.RainRecordListBean> propertyRainData;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public PropertyDataBean getPropertyData() {
        return propertyData;
    }

    public void setPropertyData(PropertyDataBean propertyData) {
        this.propertyData = propertyData;
    }

    public List<RainBeanResponse.RainRecordListBean> getPropertyRainData() {
        return propertyRainData;
    }

    public void setPropertyRainData(List<RainBeanResponse.RainRecordListBean> propertyRainData) {
        this.propertyRainData = propertyRainData;
    }

    public static class PropertyDataBean {
        /**
         * propertyId : 454
         * propertyName : Testing 23
         * location : 5-C, Godhra Hwy, Brajeshwari Extension, Greater Brajeshwari, Indore, Madhya Pradesh 452001, India
         * longitude : 75.90839769691229
         * latitude : 22.706328101575465
         * size : 25
         * sizeUnite : hectare
         * description : hello world with programming APIs in the future of our users to virtually go anywhere in the future of our
         */

        private String propertyId;
        private String propertyName;
        private String location;
        private String longitude;
        private String latitude;
        private String size;
        private String sizeUnite;
        private String description;

        public String getPropertyId() {
            return propertyId;
        }

        public void setPropertyId(String propertyId) {
            this.propertyId = propertyId;
        }

        public String getPropertyName() {
            return propertyName;
        }

        public void setPropertyName(String propertyName) {
            this.propertyName = propertyName;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getSize() {
            return size;
        }

        public void setSize(String size) {
            this.size = size;
        }

        public String getSizeUnite() {
            return sizeUnite;
        }

        public void setSizeUnite(String sizeUnite) {
            this.sizeUnite = sizeUnite;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }

}
