package com.agrinvestapp.data.model.api;

import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.CalenderUtils;

import java.util.List;

/**
 * Created by hemant
 * Date: 9/10/18.
 */

public final class WeightInfoResponse {

    /**
     * status : success
     * message : Success
     * weightList : [{"weightId":"2","animal_id":"94","weight":"20","weightUnit":"pound","weightDate":"2018-03-02","recordKey":"9856525551","crd":"2018-10-09 07:42:09","upd":"2018-10-09 07:42:09"}]
     */

    private String status;
    private String message;
    private List<WeightListBean> weightList;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<WeightListBean> getWeightList() {
        return weightList;
    }

    public void setWeightList(List<WeightListBean> weightList) {
        this.weightList = weightList;
    }

    public static class WeightListBean {
        /**
         * weightId : 2
         * animal_id : 94
         * weight : 20
         * weightUnit : pound
         * weightDate : 2018-03-02
         * recordKey : 9856525551
         * crd : 2018-10-09 07:42:09
         * upd : 2018-10-09 07:42:09
         */

        private String weightId;
        private String animal_id;
        private String weight;
        private String weightUnit;
        private String weightDate;
        private String recordKey;
        private String crd;
        private String upd;

        public String getWeightId() {
            return weightId;
        }

        public void setWeightId(String weightId) {
            this.weightId = weightId;
        }

        public String getAnimal_id() {
            return animal_id;
        }

        public void setAnimal_id(String animal_id) {
            this.animal_id = animal_id;
        }

        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
        }

        public String getWeightUnit() {
            return weightUnit;
        }

        public void setWeightUnit(String weightUnit) {
            this.weightUnit = weightUnit;
        }

        public String getWeightDate() {
            return weightDate.contains("-") ? CalenderUtils.formatDate(weightDate, AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT) : weightDate;
        }

        public void setWeightDate(String weightDate) {
            this.weightDate = weightDate;
        }

        public String getRecordKey() {
            return recordKey;
        }

        public void setRecordKey(String recordKey) {
            this.recordKey = recordKey;
        }

        public String getCrd() {
            return crd;
        }

        public void setCrd(String crd) {
            this.crd = crd;
        }

        public String getUpd() {
            return upd;
        }

        public void setUpd(String upd) {
            this.upd = upd;
        }
    }
}
