package com.agrinvestapp.data.model.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hemant
 * Date: 9/10/18.
 */

@Entity(tableName = "pregnancy_test", indices = {@Index(value = {"animal_id"})},
        foreignKeys = @ForeignKey(entity = Animal.class,
                parentColumns = "animalId",
                childColumns = "animal_id",
                onDelete = ForeignKey.CASCADE))
public final class PregnancyTest {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "pregnancyId")
    private String pregnancyId;

    /*  @ForeignKey(entity = Parcel.class,
              parentColumns = "parcelId",
              childColumns = "parcel_id")*/
    @ColumnInfo(name = "animal_id")
    private String animal_id;

    @ColumnInfo(name = "pregnancyName")
    private String pregnancyName;

    @ColumnInfo(name = "pregnancyDate")
    private String pregnancyDate;

    @ColumnInfo(name = "pregnancyDescription")
    private String pregnancyDescription;

    @ColumnInfo(name = "recordKey")
    private String recordKey;

    @ColumnInfo(name = "crd")
    private String crd;

    @ColumnInfo(name = "upd")
    private String upd;

    @ColumnInfo(name = "isDataSync")
    private Boolean isDataSync;

    @SerializedName("requestType")
    @ColumnInfo(name = "event")
    private String event;

    @NonNull
    public String getPregnancyId() {
        return pregnancyId;
    }

    public void setPregnancyId(@NonNull String pregnancyId) {
        this.pregnancyId = pregnancyId;
    }

    public String getAnimal_id() {
        return animal_id;
    }

    public void setAnimal_id(String animal_id) {
        this.animal_id = animal_id;
    }

    public String getPregnancyName() {
        return pregnancyName;
    }

    public void setPregnancyName(String pregnancyName) {
        this.pregnancyName = pregnancyName;
    }

    public String getPregnancyDate() {
        return pregnancyDate;
    }

    public void setPregnancyDate(String pregnancyDate) {
        this.pregnancyDate = pregnancyDate;
    }

    public String getPregnancyDescription() {
        return pregnancyDescription;
    }

    public void setPregnancyDescription(String pregnancyDescription) {
        this.pregnancyDescription = pregnancyDescription;
    }

    public String getRecordKey() {
        return recordKey;
    }

    public void setRecordKey(String recordKey) {
        this.recordKey = recordKey;
    }

    public String getCrd() {
        return crd;
    }

    public void setCrd(String crd) {
        this.crd = crd;
    }

    public String getUpd() {
        return upd;
    }

    public void setUpd(String upd) {
        this.upd = upd;
    }

    public Boolean getDataSync() {
        return isDataSync;
    }

    public void setDataSync(Boolean dataSync) {
        isDataSync = dataSync;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }
}
