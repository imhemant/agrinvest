package com.agrinvestapp.data.model.api;

import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.CalenderUtils;

import java.util.List;

/**
 * Created by hemant
 * Date: 9/10/18.
 */

public final class PregnancyTestInfoResponse {

    /**
     * status : success
     * message : Success
     * pregnancyList : [{"pregnancyId":"5","animal_id":"27","pregnancyName":"PT1Test edit gggg","pregnancyDate":"2018-01-21","pregnancyDescription":"edit testgfgfgdfgdg","recordKey":"333333333333","crd":"2018-10-08 17:58:52","upd":"2018-10-08 17:59:10"},{"pregnancyId":"6","animal_id":"27","pregnancyName":"PT1Test edit gggg","pregnancyDate":"2018-01-21","pregnancyDescription":"edit testgfgfgdfgdg","recordKey":"3333333333335","crd":"2018-10-08 17:58:52","upd":"2018-10-08 17:59:10"}]
     */

    private String status;
    private String message;
    private List<PregnancyListBean> pregnancyList;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<PregnancyListBean> getPregnancyList() {
        return pregnancyList;
    }

    public void setPregnancyList(List<PregnancyListBean> pregnancyList) {
        this.pregnancyList = pregnancyList;
    }

    public static class PregnancyListBean {
        /**
         * pregnancyId : 5
         * animal_id : 27
         * pregnancyName : PT1Test edit gggg
         * pregnancyDate : 2018-01-21
         * pregnancyDescription : edit testgfgfgdfgdg
         * recordKey : 333333333333
         * crd : 2018-10-08 17:58:52
         * upd : 2018-10-08 17:59:10
         */

        private String pregnancyId;
        private String animal_id;
        private String pregnancyName;
        private String pregnancyDate;
        private String pregnancyDescription;
        private String recordKey;
        private String crd;
        private String upd;

        public String getPregnancyId() {
            return pregnancyId;
        }

        public void setPregnancyId(String pregnancyId) {
            this.pregnancyId = pregnancyId;
        }

        public String getAnimal_id() {
            return animal_id;
        }

        public void setAnimal_id(String animal_id) {
            this.animal_id = animal_id;
        }

        public String getPregnancyName() {
            return pregnancyName;
        }

        public void setPregnancyName(String pregnancyName) {
            this.pregnancyName = pregnancyName;
        }

        public String getPregnancyDate() {
            return pregnancyDate.contains("-") ? CalenderUtils.formatDate(pregnancyDate, AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT) : pregnancyDate;
        }

        public void setPregnancyDate(String pregnancyDate) {
            this.pregnancyDate = pregnancyDate;
        }

        public String getPregnancyDescription() {
            return pregnancyDescription;
        }

        public void setPregnancyDescription(String pregnancyDescription) {
            this.pregnancyDescription = pregnancyDescription;
        }

        public String getRecordKey() {
            return recordKey;
        }

        public void setRecordKey(String recordKey) {
            this.recordKey = recordKey;
        }

        public String getCrd() {
            return crd;
        }

        public void setCrd(String crd) {
            this.crd = crd;
        }

        public String getUpd() {
            return upd;
        }

        public void setUpd(String upd) {
            this.upd = upd;
        }
    }
}
