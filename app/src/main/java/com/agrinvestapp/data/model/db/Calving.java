package com.agrinvestapp.data.model.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hemant
 * Date: 9/10/18.
 */

@Entity(tableName = "calving", indices = {@Index(value = {"animal_id"})},
        foreignKeys = @ForeignKey(entity = Animal.class,
                parentColumns = "animalId",
                childColumns = "animal_id",
                onDelete = ForeignKey.CASCADE))
public final class Calving {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "calvingId")
    private String calvingId = "";

    /*  @ForeignKey(entity = Parcel.class,
              parentColumns = "parcelId",
              childColumns = "parcel_id")*/
    @ColumnInfo(name = "animal_id")
    private String animal_id;

    @ColumnInfo(name = "calvingNumber")
    private String calvingNumber;

    @ColumnInfo(name = "calvingDate")
    private String calvingDate;

    @ColumnInfo(name = "recordKey")
    private String recordKey;

    @ColumnInfo(name = "crd")
    private String crd;

    @ColumnInfo(name = "upd")
    private String upd;

    @ColumnInfo(name = "isDataSync")
    private Boolean isDataSync;

    @SerializedName("requestType")
    @ColumnInfo(name = "event")
    private String event;

    @NonNull
    public String getCalvingId() {
        return calvingId;
    }

    public void setCalvingId(@NonNull String calvingId) {
        this.calvingId = calvingId;
    }

    public String getAnimal_id() {
        return animal_id;
    }

    public void setAnimal_id(String animal_id) {
        this.animal_id = animal_id;
    }

    public String getCalvingNumber() {
        return calvingNumber;
    }

    public void setCalvingNumber(String calvingNumber) {
        this.calvingNumber = calvingNumber;
    }

    public String getCalvingDate() {
        return calvingDate;
    }

    public void setCalvingDate(String calvingDate) {
        this.calvingDate = calvingDate;
    }

    public String getRecordKey() {
        return recordKey;
    }

    public void setRecordKey(String recordKey) {
        this.recordKey = recordKey;
    }

    public String getCrd() {
        return crd;
    }

    public void setCrd(String crd) {
        this.crd = crd;
    }

    public String getUpd() {
        return upd;
    }

    public void setUpd(String upd) {
        this.upd = upd;
    }

    public Boolean getDataSync() {
        return isDataSync;
    }

    public void setDataSync(Boolean dataSync) {
        isDataSync = dataSync;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }
}
