package com.agrinvestapp.data.model.other;

import android.arch.persistence.room.Ignore;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by hemant
 * Date: 22/10/18.
 */

public final class TreatmentSyncResult {
    /**
     * request_type : add
     * animalId : 60
     * recordKey : 1538201968717
     * treatmentId : 48
     * treatmentName : Sahi hai
     * treatmentRecordKey : 121212121121
     * treatmentListData : [{"request_type":"add","treatmentMappingId":"","treatmentDate":"20/2/2018","treatmentDescription":"hello1"},{"request_type":"add","treatmentMappingId":"","treatmentDate":"2/2/2018","treatmentDescription":"hello2"}]
     */

    @SerializedName("requestType")
    private String event;

    @SerializedName("animalId")
    private String treatmentForId;

    private String recordKey;
    private String treatmentId;
    private String treatmentName;
    private String treatmentRecordKey;

    @Ignore
    private List<TreatmentListDataBean> treatmentListData;

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }


    public String getTreatmentForId() {
        return treatmentForId;
    }

    public void setTreatmentForId(String treatmentForId) {
        this.treatmentForId = treatmentForId;
    }

    public String getRecordKey() {
        return recordKey;
    }

    public void setRecordKey(String recordKey) {
        this.recordKey = recordKey;
    }

    public String getTreatmentId() {
        return treatmentId;
    }

    public void setTreatmentId(String treatmentId) {
        this.treatmentId = treatmentId;
    }

    public String getTreatmentName() {
        return treatmentName;
    }

    public void setTreatmentName(String treatmentName) {
        this.treatmentName = treatmentName;
    }

    public String getTreatmentRecordKey() {
        return treatmentRecordKey;
    }

    public void setTreatmentRecordKey(String treatmentRecordKey) {
        this.treatmentRecordKey = treatmentRecordKey;
    }

    public List<TreatmentListDataBean> getTreatmentListData() {
        return treatmentListData;
    }

    public void setTreatmentListData(List<TreatmentListDataBean> treatmentListData) {
        this.treatmentListData = treatmentListData;
    }

    public static class TreatmentListDataBean {
        /**
         * request_type : add
         * treatmentMappingId :
         * treatmentDate : 20/2/2018
         * treatmentDescription : hello1
         */

        @SerializedName("requestType")
        private String event;
        private String treatmentMappingId;
        @SerializedName("mappingRecordKey")
        private String recordKey;
        private String treatmentDate;
        private String treatmentDescription;

        public String getEvent() {
            return event;
        }

        public void setEvent(String event) {
            this.event = event;
        }

        public String getTreatmentMappingId() {
            return treatmentMappingId;
        }

        public void setTreatmentMappingId(String treatmentMappingId) {
            this.treatmentMappingId = treatmentMappingId;
        }

        public String getRecordKey() {
            return recordKey;
        }

        public void setRecordKey(String recordKey) {
            this.recordKey = recordKey;
        }

        public String getTreatmentDate() {
            return treatmentDate;
        }

        public void setTreatmentDate(String treatmentDate) {
            this.treatmentDate = treatmentDate;
        }

        public String getTreatmentDescription() {
            return treatmentDescription;
        }

        public void setTreatmentDescription(String treatmentDescription) {
            this.treatmentDescription = treatmentDescription;
        }
    }
}
