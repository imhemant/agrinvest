package com.agrinvestapp.data.model.other;

/**
 * Created by hemant
 * Date: 12/12/18.
 */

public final class Language {
    public String localisationTitle = "";
    public String langName;
    public Boolean isSelected;
}
