package com.agrinvestapp.data.model.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hemant
 * Date: 19/11/18.
 */

@Entity(tableName = "expense_product_mapping", indices = {@Index(value = {"expense_id"})},
        foreignKeys = @ForeignKey(entity = Expense.class,
                parentColumns = "expenseId",
                childColumns = "expense_id",
                onDelete = ForeignKey.CASCADE))
public final class ExpenseProductMapping {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "expenseProductMappingId")
    private String expenseProductMappingId;

    @ColumnInfo(name = "expense_id")
    private String expense_id;

    @ColumnInfo(name = "product_id")
    private String product_id;

    @ColumnInfo(name = "quantity")
    private String quantity;

    @ColumnInfo(name = "price")
    private String price;

    @ColumnInfo(name = "status")
    private String status;

    @ColumnInfo(name = "crd")
    private String crd;

    @ColumnInfo(name = "upd")
    private String upd;

    @ColumnInfo(name = "isDataSync")
    private Boolean isDataSync;

    @SerializedName("requestType")
    @ColumnInfo(name = "event")
    private String event;

    @NonNull
    public String getExpenseProductMappingId() {
        return expenseProductMappingId;
    }

    public void setExpenseProductMappingId(@NonNull String expenseProductMappingId) {
        this.expenseProductMappingId = expenseProductMappingId;
    }

    public String getExpense_id() {
        return expense_id;
    }

    public void setExpense_id(String expense_id) {
        this.expense_id = expense_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCrd() {
        return crd;
    }

    public void setCrd(String crd) {
        this.crd = crd;
    }

    public String getUpd() {
        return upd;
    }

    public void setUpd(String upd) {
        this.upd = upd;
    }

    public Boolean getDataSync() {
        return isDataSync;
    }

    public void setDataSync(Boolean dataSync) {
        isDataSync = dataSync;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }
}
