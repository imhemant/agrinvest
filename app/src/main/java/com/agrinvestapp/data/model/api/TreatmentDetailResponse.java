package com.agrinvestapp.data.model.api;

import android.os.Parcel;
import android.os.Parcelable;

import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.CalenderUtils;

/**
 * Created by hemant
 * Date: 3/10/18.
 */

public final class TreatmentDetailResponse {


    /**
     * status : success
     * message : Animal treatment added successfully
     * treatmentDetail : {"treatmentMappingId":"37","treatment_id":"6","treatmentDate":"2018-11-15","treatmentDescription":"nothing more and more","crd":"2018-10-04 12:46:07","upd":"2018-10-04 12:46:07"}
     */

    private String status;
    private String message;
    private TreatmentDetailBean treatmentDetail;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public TreatmentDetailBean getTreatmentDetail() {
        return treatmentDetail;
    }

    public void setTreatmentDetail(TreatmentDetailBean treatmentDetail) {
        this.treatmentDetail = treatmentDetail;
    }

    public static class TreatmentDetailBean implements Parcelable {
        public static final Creator<TreatmentDetailBean> CREATOR = new Creator<TreatmentDetailBean>() {
            @Override
            public TreatmentDetailBean createFromParcel(Parcel in) {
                return new TreatmentDetailBean(in);
            }

            @Override
            public TreatmentDetailBean[] newArray(int size) {
                return new TreatmentDetailBean[size];
            }
        };
        /**
         * treatmentMappingId : 37
         * treatment_id : 6
         * treatmentDate : 2018-11-15
         * treatmentDescription : nothing more and more
         * crd : 2018-10-04 12:46:07
         * upd : 2018-10-04 12:46:07
         */

        private String treatmentMappingId;
        private String treatment_id;
        private String treatmentDate;
        private String treatmentDescription;
        private String crd;
        private String upd;
        private String recordKey;


        public TreatmentDetailBean() {
        }

        protected TreatmentDetailBean(Parcel in) {
            treatmentMappingId = in.readString();
            treatment_id = in.readString();
            treatmentDate = in.readString();
            treatmentDescription = in.readString();
            crd = in.readString();
            upd = in.readString();
            recordKey = in.readString();
        }

        public String getTreatmentMappingId() {
            return treatmentMappingId;
        }

        public void setTreatmentMappingId(String treatmentMappingId) {
            this.treatmentMappingId = treatmentMappingId;
        }

        public String getTreatment_id() {
            return treatment_id;
        }

        public void setTreatment_id(String treatment_id) {
            this.treatment_id = treatment_id;
        }

        public String getTreatmentDate() {
            return treatmentDate.contains("-") ? CalenderUtils.formatDate(treatmentDate, AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT) : treatmentDate;
        }

        public void setTreatmentDate(String treatmentDate) {
            this.treatmentDate = treatmentDate;
        }

        public String getTreatmentDescription() {
            return treatmentDescription;
        }

        public void setTreatmentDescription(String treatmentDescription) {
            this.treatmentDescription = treatmentDescription;
        }

        public String getCrd() {
            return crd;
        }

        public void setCrd(String crd) {
            this.crd = crd;
        }

        public String getUpd() {
            return upd;
        }

        public void setUpd(String upd) {
            this.upd = upd;
        }

        public String getRecordKey() {
            return recordKey;
        }

        public void setRecordKey(String recordKey) {
            this.recordKey = recordKey;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(treatmentMappingId);
            dest.writeString(treatment_id);
            dest.writeString(treatmentDate);
            dest.writeString(treatmentDescription);
            dest.writeString(crd);
            dest.writeString(upd);
            dest.writeString(recordKey);
        }
    }
}
