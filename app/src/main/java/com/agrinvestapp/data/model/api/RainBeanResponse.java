package com.agrinvestapp.data.model.api;

import android.arch.persistence.room.Ignore;

import java.util.List;

public final class RainBeanResponse {
    /**
     * status : success
     * message : OK
     * rainRecordList : [{"raindataId":"1","property_id":"402","fromDate":"2018-05-13","toDate":"2018-05-13","quantity":"1","quantityUnite":"mm"},{"raindataId":"2","property_id":"402","fromDate":"2018-05-13","toDate":"2018-05-13","quantity":"2","quantityUnite":"inch"}]
     */

    private String status;
    private String message;
    private List<RainRecordListBean> rainRecordList;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<RainRecordListBean> getRainRecordList() {
        return rainRecordList;
    }

    public void setRainRecordList(List<RainRecordListBean> rainRecordList) {
        this.rainRecordList = rainRecordList;
    }

    public static class RainRecordListBean {
        /**
         * raindataId : 1
         * property_id : 402
         * fromDate : 2018-05-13
         * toDate : 2018-05-13
         * quantity : 1
         * quantityUnite : mm
         */

        private String raindataId;

        private String property_id;

        private String fromDate;

        private String toDate;

        private String quantity; // scale

        private String quantityUnite; //scaleType

        @Ignore
        public Boolean isSelected = false;

        public String getRaindataId() {
            return raindataId;
        }

        public void setRaindataId(String raindataId) {
            this.raindataId = raindataId;
        }

        public String getProperty_id() {
            return property_id;
        }

        public void setProperty_id(String property_id) {
            this.property_id = property_id;
        }

        public String getFromDate() {
            return fromDate;
        }

        public void setFromDate(String fromDate) {
            this.fromDate = fromDate;
        }

        public String getToDate() {
            return toDate;
        }

        public void setToDate(String toDate) {
            this.toDate = toDate;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getQuantityUnite() {
            return quantityUnite;
        }

        public void setQuantityUnite(String quantityUnite) {
            this.quantityUnite = quantityUnite;
        }
    }


}
