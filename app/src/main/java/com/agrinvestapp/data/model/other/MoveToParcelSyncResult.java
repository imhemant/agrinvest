package com.agrinvestapp.data.model.other;

/**
 * Created by hemant
 * Date: 9/10/18.
 */

public final class MoveToParcelSyncResult {

    private String requestType;

    private String recordKey; //Animal recordKey

    private String movementFrom;

    private String movementFromRecordKey;

    private String movementRecordKey;

    private String parcelMovementId;

    private String movementDate;

    private String movementTo;

    private String movementToRecordKey;

    private Boolean isDataSync;

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getRecordKey() {
        return recordKey;
    }

    public void setRecordKey(String recordKey) {
        this.recordKey = recordKey;
    }

    public String getMovementFrom() {
        return movementFrom;
    }

    public void setMovementFrom(String movementFrom) {
        this.movementFrom = movementFrom;
    }

    public String getMovementFromRecordKey() {
        return movementFromRecordKey;
    }

    public void setMovementFromRecordKey(String movementFromRecordKey) {
        this.movementFromRecordKey = movementFromRecordKey;
    }

    public String getMovementRecordKey() {
        return movementRecordKey;
    }

    public void setMovementRecordKey(String movementRecordKey) {
        this.movementRecordKey = movementRecordKey;
    }

    public String getParcelMovementId() {
        return parcelMovementId;
    }

    public void setParcelMovementId(String parcelMovementId) {
        this.parcelMovementId = parcelMovementId;
    }

    public String getMovementDate() {
        return movementDate;
    }

    public void setMovementDate(String movementDate) {
        this.movementDate = movementDate;
    }

    public String getMovementTo() {
        return movementTo;
    }

    public void setMovementTo(String movementTo) {
        this.movementTo = movementTo;
    }

    public String getMovementToRecordKey() {
        return movementToRecordKey;
    }

    public void setMovementToRecordKey(String movementToRecordKey) {
        this.movementToRecordKey = movementToRecordKey;
    }

    public Boolean getDataSync() {
        return isDataSync;
    }

    public void setDataSync(Boolean dataSync) {
        isDataSync = dataSync;
    }
}
