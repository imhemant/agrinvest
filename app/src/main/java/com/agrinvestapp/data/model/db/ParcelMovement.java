package com.agrinvestapp.data.model.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hemant
 * Date: 9/10/18.
 */

@Entity(tableName = "parcel_movement", indices = {@Index(value = {"animal_id"})},
        foreignKeys = @ForeignKey(entity = Animal.class,
                parentColumns = "animalId",
                childColumns = "animal_id",
                onDelete = ForeignKey.CASCADE))
public final class ParcelMovement {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "parcelMovementId")
    private String parcelMovementId = "";

    /*  @ForeignKey(entity = Parcel.class,
              parentColumns = "parcelId",
              childColumns = "parcel_id")*/
    @ColumnInfo(name = "animal_id")
    private String animal_id;

    @ColumnInfo(name = "movementFrom")
    private String movementFrom;

    @ColumnInfo(name = "movementTo")
    private String movementTo;

    @ColumnInfo(name = "movementDate")
    private String movementDate;

    @ColumnInfo(name = "recordKey")
    private String recordKey;

    @ColumnInfo(name = "crd")
    private String crd;

    @ColumnInfo(name = "upd")
    private String upd;

    @ColumnInfo(name = "isDataSync")
    private Boolean isDataSync;

    @SerializedName("requestType")
    @ColumnInfo(name = "event")
    private String event;

    @NonNull
    public String getParcelMovementId() {
        return parcelMovementId;
    }

    public void setParcelMovementId(@NonNull String parcelMovementId) {
        this.parcelMovementId = parcelMovementId;
    }

    public String getAnimal_id() {
        return animal_id;
    }

    public void setAnimal_id(String animal_id) {
        this.animal_id = animal_id;
    }

    public String getMovementFrom() {
        return movementFrom;
    }

    public void setMovementFrom(String movementFrom) {
        this.movementFrom = movementFrom;
    }

    public String getMovementTo() {
        return movementTo;
    }

    public void setMovementTo(String movementTo) {
        this.movementTo = movementTo;
    }

    public String getMovementDate() {
        return movementDate;
    }

    public void setMovementDate(String movementDate) {
        this.movementDate = movementDate;
    }

    public String getRecordKey() {
        return recordKey;
    }

    public void setRecordKey(String recordKey) {
        this.recordKey = recordKey;
    }

    public String getCrd() {
        return crd;
    }

    public void setCrd(String crd) {
        this.crd = crd;
    }

    public String getUpd() {
        return upd;
    }

    public void setUpd(String upd) {
        this.upd = upd;
    }

    public Boolean getDataSync() {
        return isDataSync;
    }

    public void setDataSync(Boolean dataSync) {
        isDataSync = dataSync;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }
}
