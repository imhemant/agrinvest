package com.agrinvestapp.data.model.other;

import android.arch.persistence.room.Ignore;

/**
 * Created by hemant
 * Date: 8/12/18.
 */

public final class IncomeOrExpenseApplyOnBean {
    private String name;
    private String recordKey;

    @Ignore
    private Boolean isSelect;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRecordKey() {
        return recordKey;
    }

    public void setRecordKey(String recordKey) {
        this.recordKey = recordKey;
    }

    public Boolean getSelect() {
        return isSelect == null ? false : isSelect;
    }

    public void setSelect(Boolean select) {
        isSelect = select;
    }
}
