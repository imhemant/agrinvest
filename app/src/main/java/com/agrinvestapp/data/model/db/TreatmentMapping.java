package com.agrinvestapp.data.model.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by hemant
 * Date: 26/9/18.
 */

@Entity(tableName = "treatment_mapping", indices = {@Index(value = {"treatment_id"})},
        foreignKeys = @ForeignKey(entity = Treatment.class,
                parentColumns = "treatmentId",
                childColumns = "treatment_id",
                onDelete = ForeignKey.CASCADE))
public final class TreatmentMapping {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "treatmentMappingId")
    private String treatmentMappingId = "";

    @ColumnInfo(name = "treatment_id")
    private String treatment_id;

    @ColumnInfo(name = "treatmentDate")
    private String treatmentDate;

    @ColumnInfo(name = "treatmentFor")
    private String treatmentFor;

    @ColumnInfo(name = "treatmentDescription")
    private String treatmentDescription;

    @ColumnInfo(name = "recordKey")
    private String recordKey;

    @ColumnInfo(name = "crd")
    private String crd;

    @ColumnInfo(name = "upd")
    private String upd;

    @ColumnInfo(name = "isDataSync")
    private Boolean isDataSync;

    @ColumnInfo(name = "event")
    private String event;

    @NonNull
    public String getTreatmentMappingId() {
        return treatmentMappingId;
    }

    public void setTreatmentMappingId(@NonNull String treatmentMappingId) {
        this.treatmentMappingId = treatmentMappingId;
    }

    public String getTreatment_id() {
        return treatment_id;
    }

    public void setTreatment_id(String treatment_id) {
        this.treatment_id = treatment_id;
    }

    public String getTreatmentDate() {
        return treatmentDate;
    }

    public void setTreatmentDate(String treatmentDate) {
        this.treatmentDate = treatmentDate;
    }

    public String getTreatmentFor() {
        return treatmentFor;
    }

    public void setTreatmentFor(String treatmentFor) {
        this.treatmentFor = treatmentFor;
    }

    public String getTreatmentDescription() {
        return treatmentDescription;
    }

    public void setTreatmentDescription(String treatmentDescription) {
        this.treatmentDescription = treatmentDescription;
    }

    public String getRecordKey() {
        return recordKey;
    }

    public void setRecordKey(String recordKey) {
        this.recordKey = recordKey;
    }

    public String getCrd() {
        return crd;
    }

    public void setCrd(String crd) {
        this.crd = crd;
    }

    public String getUpd() {
        return upd;
    }

    public void setUpd(String upd) {
        this.upd = upd;
    }

    public Boolean getDataSync() {
        return isDataSync;
    }

    public void setDataSync(Boolean dataSync) {
        isDataSync = dataSync;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }
}
