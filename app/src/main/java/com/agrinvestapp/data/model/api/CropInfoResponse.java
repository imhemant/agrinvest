package com.agrinvestapp.data.model.api;

import android.arch.persistence.room.Ignore;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by hemant
 * Date: 17/10/18.
 */

public final class CropInfoResponse {

    /**
     * status : success
     * message : Success
     * croplList : [{"propertyId":"73","parcelName":"Parcelmanage","parcelRecordKey":"1415474661213","cropId":"2","parcel_id":"54","cropName":"Crop Test","lotId":"121","description":"test crop","recordKey":"","preparationDate":"0000-00-00","plantingDate":"0000-00-00","harvestDate":"0000-00-00","saleDate":"0000-00-00","salePrice":"","saleQuantity":"","saleTotalPrice":"","costFrom":"0000-00-00","costTo":"0000-00-00","costTotal":"","status":"0","crd":"2018-10-17 12:07:51","upd":"2018-10-17 12:07:51"},{"propertyId":"73","parcelName":"Parcel Test55","parcelRecordKey":"45245242","cropId":"1","parcel_id":"56","cropName":"Crop Test","lotId":"121","description":"test crop","recordKey":"","preparationDate":"0000-00-00","plantingDate":"0000-00-00","harvestDate":"0000-00-00","saleDate":"0000-00-00","salePrice":"","saleQuantity":"","saleTotalPrice":"","costFrom":"0000-00-00","costTo":"0000-00-00","costTotal":"","status":"0","crd":"2018-10-16 19:13:44","upd":"2018-10-16 19:13:44"}]
     */

    private String status;
    private String message;
    private List<CroplListBean> croplList;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<CroplListBean> getCroplList() {
        return croplList;
    }

    public void setCroplList(List<CroplListBean> croplList) {
        this.croplList = croplList;
    }

    public static class CroplListBean implements Parcelable{
        public static final Creator<CroplListBean> CREATOR = new Creator<CroplListBean>() {
            @Override
            public CroplListBean createFromParcel(Parcel in) {
                return new CroplListBean(in);
            }

            @Override
            public CroplListBean[] newArray(int size) {
                return new CroplListBean[size];
            }
        };
        /**
         * propertyId : 73
         * parcelName : Parcelmanage
         * parcelRecordKey : 1415474661213
         * cropId : 2
         * parcel_id : 54
         * cropName : Crop Test
         * lotId : 121
         * description : test crop
         * recordKey :
         * preparationDate : 0000-00-00
         * plantingDate : 0000-00-00
         * harvestDate : 0000-00-00
         * saleDate : 0000-00-00
         * salePrice :
         * saleQuantity :
         * saleTotalPrice :
         * costFrom : 0000-00-00
         * costTo : 0000-00-00
         * costTotal :
         * status : 0
         * crd : 2018-10-17 12:07:51
         * upd : 2018-10-17 12:07:51
         */
        @Ignore
        public Boolean isSelected = false;
        private String propertyId;
        private String parcelName;
        private String parcelRecordKey;
        private String cropId;
        private String parcel_id;
        private String cropName;
        private String lotId;
        private String description;
        private String recordKey;
        private String preparationDate;
        private String plantingDate;
        private String harvestDate;
        private String saleDate;
        private String salePrice;
        private String saleQuantity;
        private String saleTotalPrice;
        private String costFrom;
        private String costTo;
        private String costTotal;
        private String status;
        private String crd;
        private String upd;

        public CroplListBean(){}

        protected CroplListBean(Parcel in) {
            byte tmpIsSelected = in.readByte();
            isSelected = tmpIsSelected == 0 ? null : tmpIsSelected == 1;
            propertyId = in.readString();
            parcelName = in.readString();
            parcelRecordKey = in.readString();
            cropId = in.readString();
            parcel_id = in.readString();
            cropName = in.readString();
            lotId = in.readString();
            description = in.readString();
            recordKey = in.readString();
            preparationDate = in.readString();
            plantingDate = in.readString();
            harvestDate = in.readString();
            saleDate = in.readString();
            salePrice = in.readString();
            saleQuantity = in.readString();
            saleTotalPrice = in.readString();
            costFrom = in.readString();
            costTo = in.readString();
            costTotal = in.readString();
            status = in.readString();
            crd = in.readString();
            upd = in.readString();
        }

        public String getPropertyId() {
            return propertyId;
        }

        public void setPropertyId(String propertyId) {
            this.propertyId = propertyId;
        }

        public String getParcelName() {
            return parcelName;
        }

        public void setParcelName(String parcelName) {
            this.parcelName = parcelName;
        }

        public String getParcelRecordKey() {
            return parcelRecordKey;
        }

        public void setParcelRecordKey(String parcelRecordKey) {
            this.parcelRecordKey = parcelRecordKey;
        }

        public String getCropId() {
            return cropId;
        }

        public void setCropId(String cropId) {
            this.cropId = cropId;
        }

        public String getParcel_id() {
            return parcel_id;
        }

        public void setParcel_id(String parcel_id) {
            this.parcel_id = parcel_id;
        }

        public String getCropName() {
            return cropName;
        }

        public void setCropName(String cropName) {
            this.cropName = cropName;
        }

        public String getLotId() {
            return lotId;
        }

        public void setLotId(String lotId) {
            this.lotId = lotId;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getRecordKey() {
            return recordKey;
        }

        public void setRecordKey(String recordKey) {
            this.recordKey = recordKey;
        }

        public String getPreparationDate() {
            return preparationDate;
        }

        public void setPreparationDate(String preparationDate) {
            this.preparationDate = preparationDate;
        }

        public String getPlantingDate() {
            return plantingDate;
        }

        public void setPlantingDate(String plantingDate) {
            this.plantingDate = plantingDate;
        }

        public String getHarvestDate() {
            return harvestDate;
        }

        public void setHarvestDate(String harvestDate) {
            this.harvestDate = harvestDate;
        }

        public String getSaleDate() {
            return saleDate;
        }

        public void setSaleDate(String saleDate) {
            this.saleDate = saleDate;
        }

        public String getSalePrice() {
            return salePrice;
        }

        public void setSalePrice(String salePrice) {
            this.salePrice = salePrice;
        }

        public String getSaleQuantity() {
            return saleQuantity;
        }

        public void setSaleQuantity(String saleQuantity) {
            this.saleQuantity = saleQuantity;
        }

        public String getSaleTotalPrice() {
            return saleTotalPrice;
        }

        public void setSaleTotalPrice(String saleTotalPrice) {
            this.saleTotalPrice = saleTotalPrice;
        }

        public String getCostFrom() {
            return costFrom;
        }

        public void setCostFrom(String costFrom) {
            this.costFrom = costFrom;
        }

        public String getCostTo() {
            return costTo;
        }

        public void setCostTo(String costTo) {
            this.costTo = costTo;
        }

        public String getCostTotal() {
            return costTotal;
        }

        public void setCostTotal(String costTotal) {
            this.costTotal = costTotal;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCrd() {
            return crd;
        }

        public void setCrd(String crd) {
            this.crd = crd;
        }

        public String getUpd() {
            return upd;
        }

        public void setUpd(String upd) {
            this.upd = upd;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeByte((byte) (isSelected == null ? 0 : isSelected ? 1 : 2));
            dest.writeString(propertyId);
            dest.writeString(parcelName);
            dest.writeString(parcelRecordKey);
            dest.writeString(cropId);
            dest.writeString(parcel_id);
            dest.writeString(cropName);
            dest.writeString(lotId);
            dest.writeString(description);
            dest.writeString(recordKey);
            dest.writeString(preparationDate);
            dest.writeString(plantingDate);
            dest.writeString(harvestDate);
            dest.writeString(saleDate);
            dest.writeString(salePrice);
            dest.writeString(saleQuantity);
            dest.writeString(saleTotalPrice);
            dest.writeString(costFrom);
            dest.writeString(costTo);
            dest.writeString(costTotal);
            dest.writeString(status);
            dest.writeString(crd);
            dest.writeString(upd);
        }
    }
}
