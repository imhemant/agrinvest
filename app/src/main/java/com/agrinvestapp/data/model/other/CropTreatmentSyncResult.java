package com.agrinvestapp.data.model.other;

import java.util.List;

/**
 * Created by hemant
 * Date: 27/11/18.
 */

public final class CropTreatmentSyncResult {

    /**
     * requestType : add
     * recordKey : 49454545545577
     * treatmentName : Fever
     * treatmentRecordKey : 6048311543240159
     * treatmentListData : [{"requestType":"edit","mappingRecordKey":"4545241201212","treatmentDate":"21/12/2018","treatmentDescription":"hello 2 3 4 test 12"},{"requestType":"edit","mappingRecordKey":"5363563563","treatmentDate":"21/2/2018","treatmentDescription":"hello2 edit2"},{"requestType":"delete","mappingRecordKey":"5717521543240169","treatmentDate":"2/2/2018","treatmentDescription":"hello2"}]
     */

    private String requestType;
    private String recordKey;
    private String treatmentName;
    private String treatmentRecordKey;
    private List<TreatmentListDataBean> treatmentListData;

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getRecordKey() {
        return recordKey;
    }

    public void setRecordKey(String recordKey) {
        this.recordKey = recordKey;
    }

    public String getTreatmentName() {
        return treatmentName;
    }

    public void setTreatmentName(String treatmentName) {
        this.treatmentName = treatmentName;
    }

    public String getTreatmentRecordKey() {
        return treatmentRecordKey;
    }

    public void setTreatmentRecordKey(String treatmentRecordKey) {
        this.treatmentRecordKey = treatmentRecordKey;
    }

    public List<TreatmentListDataBean> getTreatmentListData() {
        return treatmentListData;
    }

    public void setTreatmentListData(List<TreatmentListDataBean> treatmentListData) {
        this.treatmentListData = treatmentListData;
    }

    public static class TreatmentListDataBean {
        /**
         * requestType : edit
         * mappingRecordKey : 4545241201212
         * treatmentDate : 21/12/2018
         * treatmentDescription : hello 2 3 4 test 12
         */

        private String requestType;
        private String mappingRecordKey;
        private String treatmentDate;
        private String treatmentDescription;

        public String getRequestType() {
            return requestType;
        }

        public void setRequestType(String requestType) {
            this.requestType = requestType;
        }

        public String getMappingRecordKey() {
            return mappingRecordKey;
        }

        public void setMappingRecordKey(String mappingRecordKey) {
            this.mappingRecordKey = mappingRecordKey;
        }

        public String getTreatmentDate() {
            return treatmentDate;
        }

        public void setTreatmentDate(String treatmentDate) {
            this.treatmentDate = treatmentDate;
        }

        public String getTreatmentDescription() {
            return treatmentDescription;
        }

        public void setTreatmentDescription(String treatmentDescription) {
            this.treatmentDescription = treatmentDescription;
        }
    }
}
