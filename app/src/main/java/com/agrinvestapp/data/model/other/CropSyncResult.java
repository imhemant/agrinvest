package com.agrinvestapp.data.model.other;

/**
 * Created by hemant
 * Date: 27/11/18.
 */

public final class CropSyncResult {

    private String requestType;
    private String recordKey;
    private String parcelRecordKey;
    private String lotId;
    private String cropName;
    private String preparationDate;
    private String plantingDate;
    private String harvestDate;
    private String saleDate;
    private String salePrice;
    private String saleQuantity;
    private String saleTotalPrice;
    private String costFrom;
    private String costTo;
    private String costTotal;
    private String description;

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getRecordKey() {
        return recordKey;
    }

    public void setRecordKey(String recordKey) {
        this.recordKey = recordKey;
    }

    public String getParcelRecordKey() {
        return parcelRecordKey;
    }

    public void setParcelRecordKey(String parcelRecordKey) {
        this.parcelRecordKey = parcelRecordKey;
    }

    public String getLotId() {
        return lotId;
    }

    public void setLotId(String lotId) {
        this.lotId = lotId;
    }

    public String getCropName() {
        return cropName;
    }

    public void setCropName(String cropName) {
        this.cropName = cropName;
    }

    public String getPreparationDate() {
        return preparationDate;
    }

    public void setPreparationDate(String preparationDate) {
        this.preparationDate = preparationDate;
    }

    public String getPlantingDate() {
        return plantingDate;
    }

    public void setPlantingDate(String plantingDate) {
        this.plantingDate = plantingDate;
    }

    public String getHarvestDate() {
        return harvestDate;
    }

    public void setHarvestDate(String harvestDate) {
        this.harvestDate = harvestDate;
    }

    public String getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(String saleDate) {
        this.saleDate = saleDate;
    }

    public String getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(String salePrice) {
        this.salePrice = salePrice;
    }

    public String getSaleQuantity() {
        return saleQuantity;
    }

    public void setSaleQuantity(String saleQuantity) {
        this.saleQuantity = saleQuantity;
    }

    public String getSaleTotalPrice() {
        return saleTotalPrice;
    }

    public void setSaleTotalPrice(String saleTotalPrice) {
        this.saleTotalPrice = saleTotalPrice;
    }

    public String getCostFrom() {
        return costFrom;
    }

    public void setCostFrom(String costFrom) {
        this.costFrom = costFrom;
    }

    public String getCostTo() {
        return costTo;
    }

    public void setCostTo(String costTo) {
        this.costTo = costTo;
    }

    public String getCostTotal() {
        return costTotal;
    }

    public void setCostTotal(String costTotal) {
        this.costTotal = costTotal;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
