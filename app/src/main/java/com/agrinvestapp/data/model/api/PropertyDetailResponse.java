package com.agrinvestapp.data.model.api;

import android.arch.persistence.room.Ignore;

import java.util.List;

public final class PropertyDetailResponse {

    /**
     * status : success
     * message : Property detail
     * propertyDetails : {"propertyId":"416","propertyName":"P1","location":"Indore Junction, Chhoti Gwaltoli, Indore, Madhya Pradesh, India","longitude":"75.86843709999994","latitude":"22.717019","size":"45","sizeUnite":"hectares","description":"hello thid is in  highway ","noOfAnimal":"1","rainCount":"1","parcel":[{"parcelId":"360","parcelName":"Parcel1"}]}
     */

    private String status;
    private String message;
    private PropertyDetailsBean propertyDetails;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public PropertyDetailsBean getPropertyDetails() {
        return propertyDetails;
    }

    public void setPropertyDetails(PropertyDetailsBean propertyDetails) {
        this.propertyDetails = propertyDetails;
    }

    public static class PropertyDetailsBean {
        /**
         * propertyId : 416
         * propertyName : P1
         * location : Indore Junction, Chhoti Gwaltoli, Indore, Madhya Pradesh, India
         * longitude : 75.86843709999994
         * latitude : 22.717019
         * size : 45
         * sizeUnite : hectares
         * description : hello thid is in  highway
         * noOfAnimal : 1
         * rainCount : 1
         * noOfCrop : 1
         * parcel : [{"parcelId":"360","parcelName":"Parcel1"}]
         */

        private String propertyId;
        private String propertyName;
        private String location;
        private String longitude;
        private String latitude;
        private String size;
        private String sizeUnite;
        private String description;
        @Ignore
        private String noOfAnimal;
        private String rainCount;
        private String noOfCrop;
        @Ignore
        private List<ParcelBean> parcel;

        public String getPropertyId() {
            return propertyId;
        }

        public void setPropertyId(String propertyId) {
            this.propertyId = propertyId;
        }

        public String getPropertyName() {
            return propertyName;
        }

        public void setPropertyName(String propertyName) {
            this.propertyName = propertyName;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getSize() {
            return size;
        }

        public void setSize(String size) {
            this.size = size;
        }

        public String getSizeUnite() {
            return sizeUnite;
        }

        public void setSizeUnite(String sizeUnite) {
            this.sizeUnite = sizeUnite;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getNoOfAnimal() {
            return noOfAnimal;
        }

        public void setNoOfAnimal(String noOfAnimal) {
            this.noOfAnimal = noOfAnimal;
        }

        public String getRainCount() {
            return rainCount;
        }

        public void setRainCount(String rainCount) {
            this.rainCount = rainCount;
        }

        public String getNoOfCrop() {
            return noOfCrop;
        }

        public void setNoOfCrop(String noOfCrop) {
            this.noOfCrop = noOfCrop;
        }

        public List<ParcelBean> getParcel() {
            return parcel;
        }

        public void setParcel(List<ParcelBean> parcel) {
            this.parcel = parcel;
        }

        public static class ParcelBean {
            @Ignore
            public Boolean isSelected = false;
            /**
             * parcelId : 360
             * parcelName : Parcel1
             * property_id":"1
             * recordKey":"4568282745
             * status":"0
             * crd":"2018-10-15 18:06:07
             * upd":"2018-10-16 10:42:3
             */

            private String parcelId;
            private String parcelName;
            private String property_id;
            private String recordKey;
            private String status;
            private String crd;
            private String upd;

            public String getParcelId() {
                return parcelId;
            }

            public void setParcelId(String parcelId) {
                this.parcelId = parcelId;
            }

            public String getParcelName() {
                return parcelName;
            }

            public void setParcelName(String parcelName) {
                this.parcelName = parcelName;
            }

            public String getProperty_id() {
                return property_id;
            }

            public void setProperty_id(String property_id) {
                this.property_id = property_id;
            }

            public String getRecordKey() {
                return recordKey;
            }

            public void setRecordKey(String recordKey) {
                this.recordKey = recordKey;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getCrd() {
                return crd;
            }

            public void setCrd(String crd) {
                this.crd = crd;
            }

            public String getUpd() {
                return upd;
            }

            public void setUpd(String upd) {
                this.upd = upd;
            }
        }
    }
}
