package com.agrinvestapp.data.model.api;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by hemant
 * Date: 30/10/18.
 */

public final class ProductInfoResponse {

    /**
     * status : success
     * message : Success
     * productList : [{"productId":"18","productName":"Milk","productImage":"https://dev.agrinvestonline.com//uploads/product/large/tXITYBPxNMm1Vylu.jpg","price":"4000","unit":"lt","quantity":"100","description":"cow milk","recordKey":"1234567135","user_id":"2","userType":"user","status":"1","crd":"2018-10-29 13:22:00","upd":"2018-10-29 13:24:41"}]
     */

    private String status;
    private String message;
    private List<ProductListBean> productList;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ProductListBean> getProductList() {
        return productList;
    }

    public void setProductList(List<ProductListBean> productList) {
        this.productList = productList;
    }

    public static class ProductListBean implements Parcelable {
        public static final Creator<ProductListBean> CREATOR = new Creator<ProductListBean>() {
            @Override
            public ProductListBean createFromParcel(Parcel in) {
                return new ProductListBean(in);
            }

            @Override
            public ProductListBean[] newArray(int size) {
                return new ProductListBean[size];
            }
        };
        /**
         * productId : 18
         * productName : Milk
         * productImage : https://dev.agrinvestonline.com//uploads/product/large/tXITYBPxNMm1Vylu.jpg
         * price : 4000
         * unit : lt
         * quantity : 100
         * description : cow milk
         * recordKey : 1234567135
         * user_id : 2
         * userType : user
         * status : 1
         * crd : 2018-10-29 13:22:00
         * upd : 2018-10-29 13:24:41
         */

        private String productId;
        private String productName;
        private String productImage;
        private String price;
        private String unit;
        private String quantity;
        private String description;
        private String recordKey;
        private String user_id;
        private String userType;
        private String status;
        private String crd;
        private String upd;
        private String productImageThumb;

        public ProductListBean(){}

        protected ProductListBean(Parcel in) {
            productId = in.readString();
            productName = in.readString();
            productImage = in.readString();
            price = in.readString();
            unit = in.readString();
            quantity = in.readString();
            description = in.readString();
            recordKey = in.readString();
            user_id = in.readString();
            userType = in.readString();
            status = in.readString();
            crd = in.readString();
            upd = in.readString();
            productImageThumb = in.readString();
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getProductImage() {
            return productImage;
        }

        public void setProductImage(String productImage) {
            this.productImage = productImage;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getRecordKey() {
            return recordKey;
        }

        public void setRecordKey(String recordKey) {
            this.recordKey = recordKey;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getUserType() {
            return userType;
        }

        public void setUserType(String userType) {
            this.userType = userType;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCrd() {
            return crd;
        }

        public void setCrd(String crd) {
            this.crd = crd;
        }

        public String getUpd() {
            return upd;
        }

        public void setUpd(String upd) {
            this.upd = upd;
        }

        public String getProductImageThumb() {
            return productImageThumb;
        }

        public void setProductImageThumb(String productImageThumb) {
            this.productImageThumb = productImageThumb;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(productId);
            dest.writeString(productName);
            dest.writeString(productImage);
            dest.writeString(price);
            dest.writeString(unit);
            dest.writeString(quantity);
            dest.writeString(description);
            dest.writeString(recordKey);
            dest.writeString(user_id);
            dest.writeString(userType);
            dest.writeString(status);
            dest.writeString(crd);
            dest.writeString(upd);
            dest.writeString(productImageThumb);
        }
    }
}
