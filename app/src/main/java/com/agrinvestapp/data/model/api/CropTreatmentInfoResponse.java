package com.agrinvestapp.data.model.api;

import android.arch.persistence.room.Ignore;
import android.os.Parcel;
import android.os.Parcelable;

import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.CalenderUtils;

import java.util.List;

/**
 * Created by hemant
 * Date: 23/10/18.
 */

public final class CropTreatmentInfoResponse {

    /**
     * status : success
     * message : Success
     * treatmentDetailList : [{"treatmentId":"5","crop_id":"19","treatmentName":"FEVER1","recordKey":"98653264","crd":"2018-10-23 11:18:19","upd":"2018-10-23 11:18:19","treatmentList":[{"treatmentMappingId":"2","treatment_id":"2","treatmentDate":"2017-10-15","treatmentDescription":"test date edit","recordKey":"147875236","crd":"2018-10-22 11:23:51","upd":"2018-10-22 11:23:51"},{"treatmentMappingId":"1","treatment_id":"2","treatmentDate":"2017-10-14","treatmentDescription":"test date edit","recordKey":"147875236","crd":"2018-10-22 11:23:42","upd":"2018-10-22 11:23:42"}]},{"treatmentId":"2","crop_id":"19","treatmentName":"FEVER1","recordKey":"9865326","crd":"2018-10-22 11:23:10","upd":"2018-10-22 11:23:10","treatmentList":[{"treatmentMappingId":"2","treatment_id":"2","treatmentDate":"2017-10-15","treatmentDescription":"test date edit","recordKey":"147875236","crd":"2018-10-22 11:23:51","upd":"2018-10-22 11:23:51"},{"treatmentMappingId":"1","treatment_id":"2","treatmentDate":"2017-10-14","treatmentDescription":"test date edit","recordKey":"147875236","crd":"2018-10-22 11:23:42","upd":"2018-10-22 11:23:42"}]}]
     */

    private String status;
    private String message;
    private List<TreatmentDetailListBean> treatmentDetailList;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<TreatmentDetailListBean> getTreatmentDetailList() {
        return treatmentDetailList;
    }

    public void setTreatmentDetailList(List<TreatmentDetailListBean> treatmentDetailList) {
        this.treatmentDetailList = treatmentDetailList;
    }

    public static class TreatmentDetailListBean implements Parcelable{
        public static final Creator<TreatmentDetailListBean> CREATOR = new Creator<TreatmentDetailListBean>() {
            @Override
            public TreatmentDetailListBean createFromParcel(Parcel in) {
                return new TreatmentDetailListBean(in);
            }

            @Override
            public TreatmentDetailListBean[] newArray(int size) {
                return new TreatmentDetailListBean[size];
            }
        };
        /**
         * treatmentId : 5
         * crop_id : 19
         * treatmentName : FEVER1
         * recordKey : 98653264
         * crd : 2018-10-23 11:18:19
         * upd : 2018-10-23 11:18:19
         * treatmentList : [{"treatmentMappingId":"2","treatment_id":"2","treatmentDate":"2017-10-15","treatmentDescription":"test date edit","recordKey":"147875236","crd":"2018-10-22 11:23:51","upd":"2018-10-22 11:23:51"},{"treatmentMappingId":"1","treatment_id":"2","treatmentDate":"2017-10-14","treatmentDescription":"test date edit","recordKey":"147875236","crd":"2018-10-22 11:23:42","upd":"2018-10-22 11:23:42"}]
         */

        public Boolean isSelected = false;
        private String treatmentId;
        private String crop_id;
        private String treatmentName;
        private String recordKey;
        private String crd;
        private String upd;
        @Ignore
        private List<TreatmentListBean> treatmentList;

        public TreatmentDetailListBean(){}

        protected TreatmentDetailListBean(Parcel in) {
            byte tmpIsSelected = in.readByte();
            isSelected = tmpIsSelected == 0 ? null : tmpIsSelected == 1;
            treatmentId = in.readString();
            crop_id = in.readString();
            treatmentName = in.readString();
            recordKey = in.readString();
            crd = in.readString();
            upd = in.readString();
            treatmentList = in.createTypedArrayList(TreatmentListBean.CREATOR);
        }

        public String getTreatmentId() {
            return treatmentId;
        }

        public void setTreatmentId(String treatmentId) {
            this.treatmentId = treatmentId;
        }

        public String getCrop_id() {
            return crop_id;
        }

        public void setCrop_id(String crop_id) {
            this.crop_id = crop_id;
        }

        public String getTreatmentName() {
            return treatmentName;
        }

        public void setTreatmentName(String treatmentName) {
            this.treatmentName = treatmentName;
        }

        public String getRecordKey() {
            return recordKey;
        }

        public void setRecordKey(String recordKey) {
            this.recordKey = recordKey;
        }

        public String getCrd() {
            return crd;
        }

        public void setCrd(String crd) {
            this.crd = crd;
        }

        public String getUpd() {
            return upd;
        }

        public void setUpd(String upd) {
            this.upd = upd;
        }

        public List<TreatmentListBean> getTreatmentList() {
            return treatmentList;
        }

        public void setTreatmentList(List<TreatmentListBean> treatmentList) {
            this.treatmentList = treatmentList;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeByte((byte) (isSelected == null ? 0 : isSelected ? 1 : 2));
            dest.writeString(treatmentId);
            dest.writeString(crop_id);
            dest.writeString(treatmentName);
            dest.writeString(recordKey);
            dest.writeString(crd);
            dest.writeString(upd);
            dest.writeTypedList(treatmentList);
        }

        public static class TreatmentListBean implements Parcelable{
            public static final Creator<TreatmentListBean> CREATOR = new Creator<TreatmentListBean>() {
                @Override
                public TreatmentListBean createFromParcel(Parcel in) {
                    return new TreatmentListBean(in);
                }

                @Override
                public TreatmentListBean[] newArray(int size) {
                    return new TreatmentListBean[size];
                }
            };
            /**
             * treatmentMappingId : 2
             * treatment_id : 2
             * treatmentDate : 2017-10-15
             * treatmentDescription : test date edit
             * recordKey : 147875236
             * crd : 2018-10-22 11:23:51
             * upd : 2018-10-22 11:23:51
             */

            private String treatmentMappingId;
            private String treatment_id;
            private String treatmentDate;
            private String treatmentDescription;
            private String recordKey;
            private String crd;
            private String upd;

            public TreatmentListBean(){}

            protected TreatmentListBean(Parcel in) {
                treatmentMappingId = in.readString();
                treatment_id = in.readString();
                treatmentDate = in.readString();
                treatmentDescription = in.readString();
                recordKey = in.readString();
                crd = in.readString();
                upd = in.readString();
            }

            public String getTreatmentMappingId() {
                return treatmentMappingId;
            }

            public void setTreatmentMappingId(String treatmentMappingId) {
                this.treatmentMappingId = treatmentMappingId;
            }

            public String getTreatment_id() {
                return treatment_id;
            }

            public void setTreatment_id(String treatment_id) {
                this.treatment_id = treatment_id;
            }

            public String getTreatmentDate() {
                return treatmentDate.contains("-") ? CalenderUtils.formatDate(treatmentDate, AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT) : treatmentDate;
            }

            public void setTreatmentDate(String treatmentDate) {
                this.treatmentDate = treatmentDate;
            }

            public String getTreatmentDescription() {
                return treatmentDescription;
            }

            public void setTreatmentDescription(String treatmentDescription) {
                this.treatmentDescription = treatmentDescription;
            }

            public String getRecordKey() {
                return recordKey;
            }

            public void setRecordKey(String recordKey) {
                this.recordKey = recordKey;
            }

            public String getCrd() {
                return crd;
            }

            public void setCrd(String crd) {
                this.crd = crd;
            }

            public String getUpd() {
                return upd;
            }

            public void setUpd(String upd) {
                this.upd = upd;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(treatmentMappingId);
                dest.writeString(treatment_id);
                dest.writeString(treatmentDate);
                dest.writeString(treatmentDescription);
                dest.writeString(recordKey);
                dest.writeString(crd);
                dest.writeString(upd);
            }
        }
    }
}
