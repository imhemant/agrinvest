package com.agrinvestapp.data.model.other;

import android.arch.persistence.room.Ignore;

import java.util.List;

/**
 * Created by hemant
 * Date: 6/12/18.
 */

public final class ExpenseSyncResult {
    private String expenseId;
    private String requestType;
    private String recordKey;
    private String cost;
    private String expenseFor;
    private String appliedOn;
    private String dateApplied;
    private String dateBought;
    private String description;
    @Ignore
    private List<AppliedOnDetail> appliedOnDetail;
    @Ignore
    private List<ProductDetail> productDetail;

    public String getExpenseId() {
        return expenseId;
    }

    public void setExpenseId(String expenseId) {
        this.expenseId = expenseId;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getRecordKey() {
        return recordKey;
    }

    public void setRecordKey(String recordKey) {
        this.recordKey = recordKey;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getExpenseFor() {
        return expenseFor;
    }

    public void setExpenseFor(String expenseFor) {
        this.expenseFor = expenseFor;
    }

    public String getAppliedOn() {
        return appliedOn;
    }

    public void setAppliedOn(String appliedOn) {
        this.appliedOn = appliedOn;
    }

    public String getDateApplied() {
        return dateApplied;
    }

    public void setDateApplied(String dateApplied) {
        this.dateApplied = dateApplied;
    }

    public String getDateBought() {
        return dateBought;
    }

    public void setDateBought(String dateBought) {
        this.dateBought = dateBought;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<AppliedOnDetail> getAppliedOnDetail() {
        return appliedOnDetail;
    }

    public void setAppliedOnDetail(List<AppliedOnDetail> appliedOnDetail) {
        this.appliedOnDetail = appliedOnDetail;
    }

    public List<ProductDetail> getProductDetail() {
        return productDetail;
    }

    public void setProductDetail(List<ProductDetail> productDetail) {
        this.productDetail = productDetail;
    }

    public static class AppliedOnDetail {
        private String appliedOnId;

        public String getAppliedOnId() {
            return appliedOnId;
        }

        public void setAppliedOnId(String appliedOnId) {
            this.appliedOnId = appliedOnId;
        }
    }

    public static class ProductDetail {
        private String productId;
        private String price;
        private String quantity;

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }
    }
}
