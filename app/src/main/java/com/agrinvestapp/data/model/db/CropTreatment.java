package com.agrinvestapp.data.model.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by hemant
 * Date: 26/9/18.
 */

@Entity(tableName = "crop_treatment", indices = {@Index(value = {"crop_id"})},
        foreignKeys = @ForeignKey(entity = Crop.class,
                parentColumns = "cropId",
                childColumns = "crop_id",
                onDelete = ForeignKey.CASCADE))
public final class CropTreatment {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "treatmentId")
    private String treatmentId = "";

    @ColumnInfo(name = "crop_id")
    private String crop_id;

    @ColumnInfo(name = "treatmentName")
    private String treatmentName;

    @ColumnInfo(name = "recordKey")
    private String recordKey;

    @ColumnInfo(name = "crd")
    private String crd;

    @ColumnInfo(name = "upd")
    private String upd;

    @ColumnInfo(name = "isDataSync")
    private Boolean isDataSync;

    @ColumnInfo(name = "event")
    private String event;

    @NonNull
    public String getTreatmentId() {
        return treatmentId;
    }

    public void setTreatmentId(@NonNull String treatmentId) {
        this.treatmentId = treatmentId;
    }

    public String getCrop_id() {
        return crop_id;
    }

    public void setCrop_id(String crop_id) {
        this.crop_id = crop_id;
    }

    public String getTreatmentName() {
        return treatmentName;
    }

    public void setTreatmentName(String treatmentName) {
        this.treatmentName = treatmentName;
    }

    public String getRecordKey() {
        return recordKey;
    }

    public void setRecordKey(String recordKey) {
        this.recordKey = recordKey;
    }

    public String getCrd() {
        return crd;
    }

    public void setCrd(String crd) {
        this.crd = crd;
    }

    public String getUpd() {
        return upd;
    }

    public void setUpd(String upd) {
        this.upd = upd;
    }

    public Boolean getDataSync() {
        return isDataSync;
    }

    public void setDataSync(Boolean dataSync) {
        isDataSync = dataSync;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }
}
