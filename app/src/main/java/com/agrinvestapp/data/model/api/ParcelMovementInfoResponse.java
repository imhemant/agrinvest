package com.agrinvestapp.data.model.api;

import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.CalenderUtils;

import java.util.List;

/**
 * Created by hemant
 * Date: 9/10/18.
 */

public final class ParcelMovementInfoResponse {


    /**
     * status : success
     * message : Success
     * parcelMovementList : [{"parcelMovementId":"1","animal_id":"27","movementFrom":"52","movementTo":"53","movementDate":"2018-12-12","recordKey":"369852147742","crd":"2018-10-09 12:13:45","upd":"2018-10-09 12:13:45"},{"parcelMovementId":"2","animal_id":"27","movementFrom":"53","movementTo":"52","movementDate":"2018-02-12","recordKey":"36985214774","crd":"2018-10-09 12:25:10","upd":"2018-10-09 12:30:04"}]
     */

    private String status;
    private String message;
    private List<ParcelMovementListBean> parcelMovementList;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ParcelMovementListBean> getParcelMovementList() {
        return parcelMovementList;
    }

    public void setParcelMovementList(List<ParcelMovementListBean> parcelMovementList) {
        this.parcelMovementList = parcelMovementList;
    }

    public static class ParcelMovementListBean {
        /**
         * parcelMovementId : 1
         * animal_id : 27
         * movementFrom : 52
         * movementTo : 53
         * movementDate : 2018-12-12
         * recordKey : 369852147742
         * crd : 2018-10-09 12:13:45
         * upd : 2018-10-09 12:13:45
         */

        private String fromName;
        private String fromRecordKey;
        private String toName;
        private String toRecordKey;
        private String parcelMovementId;
        private String animal_id;
        private String movementFrom;
        private String movementTo;
        private String movementDate;
        private String recordKey;
        private String crd;
        private String upd;

        public String getFromName() {
            return fromName;
        }

        public void setFromName(String fromName) {
            this.fromName = fromName;
        }

        public String getToName() {
            return toName;
        }

        public void setToName(String toName) {
            this.toName = toName;
        }

        public String getParcelMovementId() {
            return parcelMovementId;
        }

        public void setParcelMovementId(String parcelMovementId) {
            this.parcelMovementId = parcelMovementId;
        }

        public String getAnimal_id() {
            return animal_id;
        }

        public void setAnimal_id(String animal_id) {
            this.animal_id = animal_id;
        }

        public String getMovementFrom() {
            return movementFrom;
        }

        public void setMovementFrom(String movementFrom) {
            this.movementFrom = movementFrom;
        }

        public String getMovementTo() {
            return movementTo;
        }

        public void setMovementTo(String movementTo) {
            this.movementTo = movementTo;
        }

        public String getMovementDate() {
            return movementDate.contains("-") ? CalenderUtils.formatDate(movementDate, AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT) : movementDate;
        }

        public void setMovementDate(String movementDate) {
            this.movementDate = movementDate;
        }

        public String getRecordKey() {
            return recordKey;
        }

        public void setRecordKey(String recordKey) {
            this.recordKey = recordKey;
        }

        public String getCrd() {
            return crd;
        }

        public void setCrd(String crd) {
            this.crd = crd;
        }

        public String getUpd() {
            return upd;
        }

        public void setUpd(String upd) {
            this.upd = upd;
        }

        public String getFromRecordKey() {
            return fromRecordKey;
        }

        public void setFromRecordKey(String fromRecordKey) {
            this.fromRecordKey = fromRecordKey;
        }

        public String getToRecordKey() {
            return toRecordKey;
        }

        public void setToRecordKey(String toRecordKey) {
            this.toRecordKey = toRecordKey;
        }

    }
}
