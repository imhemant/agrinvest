package com.agrinvestapp.data.model.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hemant
 * Date: 19/11/18.
 */

@Entity(tableName = "expense_mapping", indices = {@Index(value = {"expense_id"})},
        foreignKeys = @ForeignKey(entity = Expense.class,
                parentColumns = "expenseId",
                childColumns = "expense_id",
                onDelete = ForeignKey.CASCADE))
public final class ExpenseMapping {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "expenseMappingId")
    private String expenseMappingId = "";

    @ColumnInfo(name = "expense_id")
    private String expense_id;

    @ColumnInfo(name = "appliedOn")
    private String appliedOn;

    @ColumnInfo(name = "appliedOnId")
    private String appliedOnId;

    @ColumnInfo(name = "status")
    private String status;

    @ColumnInfo(name = "crd")
    private String crd;

    @ColumnInfo(name = "upd")
    private String upd;

    @ColumnInfo(name = "isDataSync")
    private Boolean isDataSync;

    @SerializedName("requestType")
    @ColumnInfo(name = "event")
    private String event;

    @NonNull
    public String getExpenseMappingId() {
        return expenseMappingId;
    }

    public void setExpenseMappingId(@NonNull String expenseMappingId) {
        this.expenseMappingId = expenseMappingId;
    }

    public String getExpense_id() {
        return expense_id;
    }

    public void setExpense_id(String expense_id) {
        this.expense_id = expense_id;
    }

    public String getAppliedOn() {
        return appliedOn;
    }

    public void setAppliedOn(String appliedOn) {
        this.appliedOn = appliedOn;
    }

    public String getAppliedOnId() {
        return appliedOnId;
    }

    public void setAppliedOnId(String appliedOnId) {
        this.appliedOnId = appliedOnId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCrd() {
        return crd;
    }

    public void setCrd(String crd) {
        this.crd = crd;
    }

    public String getUpd() {
        return upd;
    }

    public void setUpd(String upd) {
        this.upd = upd;
    }

    public Boolean getDataSync() {
        return isDataSync;
    }

    public void setDataSync(Boolean dataSync) {
        isDataSync = dataSync;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }
}
