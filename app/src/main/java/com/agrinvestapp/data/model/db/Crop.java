package com.agrinvestapp.data.model.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import com.agrinvestapp.utils.DateConverter;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

@Entity(tableName = "crop", indices = {@Index(value = {"parcel_id"})},
        foreignKeys = @ForeignKey(entity = Parcel.class,
                parentColumns = "parcelId",
                childColumns = "parcel_id",
                onDelete = ForeignKey.CASCADE))
public final class Crop {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "cropId")
    private String cropId = "";

    @ColumnInfo(name = "parcel_id")
    private String parcel_id;

    @ColumnInfo(name = "cropName")
    private String cropName;

    @ColumnInfo(name = "lotId")
    private String lotId;

    @ColumnInfo(name = "description")
    private String description;

    @ColumnInfo(name = "preparationDate")
    private String preparationDate;

    @ColumnInfo(name = "plantingDate")
    private String plantingDate;

    @ColumnInfo(name = "harvestDate")
    private String harvestDate;

    @ColumnInfo(name = "saleDate")
    @TypeConverters({DateConverter.class})
    private Date saleDate;

    @ColumnInfo(name = "salePrice")
    private String salePrice;

    @ColumnInfo(name = "saleQuantity")
    private String saleQuantity;

    @ColumnInfo(name = "saleTotalPrice")
    private String saleTotalPrice;

    @ColumnInfo(name = "costFrom")
    private String costFrom;

    @ColumnInfo(name = "costTo")
    private String costTo;

    @ColumnInfo(name = "costTotal")
    private String costTotal;

    @ColumnInfo(name = "status")
    private String status;

    @ColumnInfo(name = "recordKey")
    private String recordKey;

    @ColumnInfo(name = "crd")
    private String crd;

    @ColumnInfo(name = "upd")
    private String upd;

    @ColumnInfo(name = "isDataSync")
    private Boolean isDataSync;

    @SerializedName("requestType")
    @ColumnInfo(name = "event")
    private String event;

    @NonNull
    public String getCropId() {
        return cropId;
    }

    public void setCropId(@NonNull String cropId) {
        this.cropId = cropId;
    }

    public String getParcel_id() {
        return parcel_id;
    }

    public void setParcel_id(String parcel_id) {
        this.parcel_id = parcel_id;
    }

    public String getCropName() {
        return cropName;
    }

    public void setCropName(String cropName) {
        this.cropName = cropName;
    }

    public String getLotId() {
        return lotId;
    }

    public void setLotId(String lotId) {
        this.lotId = lotId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPreparationDate() {
        return preparationDate;
    }

    public void setPreparationDate(String preparationDate) {
        this.preparationDate = preparationDate;
    }

    public String getPlantingDate() {
        return plantingDate;
    }

    public void setPlantingDate(String plantingDate) {
        this.plantingDate = plantingDate;
    }

    public String getHarvestDate() {
        return harvestDate;
    }

    public void setHarvestDate(String harvestDate) {
        this.harvestDate = harvestDate;
    }

    public Date getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(Date saleDate) {
        this.saleDate = saleDate;
    }

    public String getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(String salePrice) {
        this.salePrice = salePrice;
    }

    public String getSaleQuantity() {
        return saleQuantity;
    }

    public void setSaleQuantity(String saleQuantity) {
        this.saleQuantity = saleQuantity;
    }

    public String getSaleTotalPrice() {
        return saleTotalPrice;
    }

    public void setSaleTotalPrice(String saleTotalPrice) {
        this.saleTotalPrice = saleTotalPrice;
    }

    public String getCostFrom() {
        return costFrom;
    }

    public void setCostFrom(String costFrom) {
        this.costFrom = costFrom;
    }

    public String getCostTo() {
        return costTo;
    }

    public void setCostTo(String costTo) {
        this.costTo = costTo;
    }

    public String getCostTotal() {
        return costTotal;
    }

    public void setCostTotal(String costTotal) {
        this.costTotal = costTotal;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRecordKey() {
        return recordKey;
    }

    public void setRecordKey(String recordKey) {
        this.recordKey = recordKey;
    }

    public String getCrd() {
        return crd;
    }

    public void setCrd(String crd) {
        this.crd = crd;
    }

    public String getUpd() {
        return upd;
    }

    public void setUpd(String upd) {
        this.upd = upd;
    }

    public Boolean getDataSync() {
        return isDataSync;
    }

    public void setDataSync(Boolean dataSync) {
        isDataSync = dataSync;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }
}
