package com.agrinvestapp.data.model.api;

import android.arch.persistence.room.Ignore;
import android.os.Parcel;
import android.os.Parcelable;

import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.CalenderUtils;

import java.util.List;

/**
 * Created by hemant
 * Date: 27/9/18.
 */

public final class TreatmentInfoResponse {

    /**
     * status : success
     * message : Animal treatment added successfully
     * treatmentDetail : [{"treatmentId":"1","treatmentName":"glue glue","treatmentForId":"1","treatmentFor":"1","recordKey":"123456","crd":"2018-09-27 12:30:40","upd":"2018-09-27 12:30:40","treatmentList":[]},{"treatmentId":"2","treatmentName":"glue glue","treatmentForId":"1","treatmentFor":"1","recordKey":"12345611","crd":"2018-09-27 12:56:19","upd":"2018-09-27 12:56:19","treatmentList":[]},{"treatmentId":"3","treatmentName":"glue glue","treatmentForId":"1","treatmentFor":"1","recordKey":"123456112","crd":"2018-09-27 12:56:35","upd":"2018-09-27 12:56:35","treatmentList":[]}]
     */

    private String status;
    private String message;
    private List<TreatmentDetailBean> treatmentDetail;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<TreatmentDetailBean> getTreatmentDetail() {
        return treatmentDetail;
    }

    public void setTreatmentDetail(List<TreatmentDetailBean> treatmentDetail) {
        this.treatmentDetail = treatmentDetail;
    }

    public static class TreatmentDetailBean implements Parcelable {

        public static final Creator<TreatmentDetailBean> CREATOR = new Creator<TreatmentDetailBean>() {
            @Override
            public TreatmentDetailBean createFromParcel(Parcel in) {
                return new TreatmentDetailBean(in);
            }

            @Override
            public TreatmentDetailBean[] newArray(int size) {
                return new TreatmentDetailBean[size];
            }
        };
        @Ignore
        public Boolean isSelected = false;
        /**
         * treatmentId : 1
         * treatmentName : glue glue
         * treatmentForId : 1
         * treatmentFor : 1
         * recordKey : 123456
         * crd : 2018-09-27 12:30:40
         * upd : 2018-09-27 12:30:40
         * treatmentList : []
         */

        private String treatmentId;
        private String treatmentName;
        private String treatmentForId;
        private String treatmentFor;
        private String recordKey;
        private String crd;
        private String upd;

        @Ignore
        private List<TreatmentListBean> treatmentList;


        public TreatmentDetailBean() {
        }

        protected TreatmentDetailBean(Parcel in) {
            byte tmpIsSelected = in.readByte();
            isSelected = tmpIsSelected == 0 ? null : tmpIsSelected == 1;
            treatmentId = in.readString();
            treatmentName = in.readString();
            treatmentForId = in.readString();
            treatmentFor = in.readString();
            recordKey = in.readString();
            crd = in.readString();
            upd = in.readString();
            treatmentList = in.createTypedArrayList(TreatmentListBean.CREATOR);
        }

        public String getTreatmentId() {
            return treatmentId;
        }

        public void setTreatmentId(String treatmentId) {
            this.treatmentId = treatmentId;
        }

        public String getTreatmentName() {
            return treatmentName;
        }

        public void setTreatmentName(String treatmentName) {
            this.treatmentName = treatmentName;
        }

        public String getTreatmentForId() {
            return treatmentForId;
        }

        public void setTreatmentForId(String treatmentForId) {
            this.treatmentForId = treatmentForId;
        }

        public String getTreatmentFor() {
            return treatmentFor;
        }

        public void setTreatmentFor(String treatmentFor) {
            this.treatmentFor = treatmentFor;
        }

        public String getRecordKey() {
            return recordKey;
        }

        public void setRecordKey(String recordKey) {
            this.recordKey = recordKey;
        }

        public String getCrd() {
            return crd;
        }

        public void setCrd(String crd) {
            this.crd = crd;
        }

        public String getUpd() {
            return upd;
        }

        public void setUpd(String upd) {
            this.upd = upd;
        }

        public List<TreatmentListBean> getTreatmentList() {
            return treatmentList;
        }

        public void setTreatmentList(List<TreatmentListBean> treatmentList) {
            this.treatmentList = treatmentList;
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeByte((byte) (isSelected == null ? 0 : isSelected ? 1 : 2));
            dest.writeString(treatmentId);
            dest.writeString(treatmentName);
            dest.writeString(treatmentForId);
            dest.writeString(treatmentFor);
            dest.writeString(recordKey);
            dest.writeString(crd);
            dest.writeString(upd);
            dest.writeTypedList(treatmentList);
        }


        public static class TreatmentListBean implements Parcelable {
            public static final Creator<TreatmentListBean> CREATOR = new Creator<TreatmentListBean>() {
                @Override
                public TreatmentListBean createFromParcel(Parcel in) {
                    return new TreatmentListBean(in);
                }

                @Override
                public TreatmentListBean[] newArray(int size) {
                    return new TreatmentListBean[size];
                }
            };
            /**
             * treatmentMappingId : 10
             * treatment_id : 6
             * treatmentDate : 2018-11-14
             * treatmentDescription : nothing more and more
             * crd : 2018-10-03 05:23:01
             * upd : 2018-10-03 05:23:01
             */

            private String treatmentMappingId;
            private String treatment_id;
            private String treatmentDate;
            private String treatmentDescription;
            private String recordKey;
            private String crd;
            private String upd;

            public TreatmentListBean() {
            }


            protected TreatmentListBean(Parcel in) {
                treatmentMappingId = in.readString();
                treatment_id = in.readString();
                treatmentDate = in.readString();
                treatmentDescription = in.readString();
                recordKey = in.readString();
                crd = in.readString();
                upd = in.readString();
            }

            public String getTreatmentMappingId() {
                return treatmentMappingId;
            }

            public void setTreatmentMappingId(String treatmentMappingId) {
                this.treatmentMappingId = treatmentMappingId;
            }

            public String getTreatment_id() {
                return treatment_id;
            }

            public void setTreatment_id(String treatment_id) {
                this.treatment_id = treatment_id;
            }

            public String getTreatmentDate() {
                return treatmentDate.contains("-") ? CalenderUtils.formatDate(treatmentDate, AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT) : treatmentDate;
            }

            public void setTreatmentDate(String treatmentDate) {
                this.treatmentDate = treatmentDate;
            }

            public String getTreatmentDescription() {
                return treatmentDescription;
            }

            public void setTreatmentDescription(String treatmentDescription) {
                this.treatmentDescription = treatmentDescription;
            }

            public String getRecordKey() {
                return recordKey;
            }

            public void setRecordKey(String recordKey) {
                this.recordKey = recordKey;
            }

            public String getCrd() {
                return crd;
            }

            public void setCrd(String crd) {
                this.crd = crd;
            }

            public String getUpd() {
                return upd;
            }

            public void setUpd(String upd) {
                this.upd = upd;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(treatmentMappingId);
                dest.writeString(treatment_id);
                dest.writeString(treatmentDate);
                dest.writeString(treatmentDescription);
                dest.writeString(recordKey);
                dest.writeString(crd);
                dest.writeString(upd);
            }
        }
    }
}
