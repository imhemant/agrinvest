package com.agrinvestapp.data.model.other;

/**
 * Created by hemant
 * Date: 9/10/18.
 */

public final class PregnancyTestSyncResult {

    private String requestType;

    private String recordKey; //Animal recordKey

    private String pregnancyName;

    private String pregnancyRecordKey;

    private String pregnancyId;

    private String pregnancyDate;

    private String pregnancyDescription;

    private Boolean isDataSync;

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getRecordKey() {
        return recordKey;
    }

    public void setRecordKey(String recordKey) {
        this.recordKey = recordKey;
    }

    public String getPregnancyName() {
        return pregnancyName;
    }

    public void setPregnancyName(String pregnancyName) {
        this.pregnancyName = pregnancyName;
    }

    public String getPregnancyRecordKey() {
        return pregnancyRecordKey;
    }

    public void setPregnancyRecordKey(String pregnancyRecordKey) {
        this.pregnancyRecordKey = pregnancyRecordKey;
    }

    public String getPregnancyId() {
        return pregnancyId;
    }

    public void setPregnancyId(String pregnancyId) {
        this.pregnancyId = pregnancyId;
    }

    public String getPregnancyDate() {
        return pregnancyDate;
    }

    public void setPregnancyDate(String pregnancyDate) {
        this.pregnancyDate = pregnancyDate;
    }

    public String getPregnancyDescription() {
        return pregnancyDescription;
    }

    public void setPregnancyDescription(String pregnancyDescription) {
        this.pregnancyDescription = pregnancyDescription;
    }

    public Boolean getDataSync() {
        return isDataSync;
    }

    public void setDataSync(Boolean dataSync) {
        isDataSync = dataSync;
    }
}
