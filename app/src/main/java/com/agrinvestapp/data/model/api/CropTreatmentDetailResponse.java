package com.agrinvestapp.data.model.api;

import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.CalenderUtils;

/**
 * Created by hemant
 * Date: 23/10/18.
 */

public final class CropTreatmentDetailResponse {

    /**
     * status : success
     * message : Treatment added successfully
     * treatmentDetail : {"treatmentMappingId":"5","treatment_id":"7","treatmentDate":"2018-10-20","treatmentDescription":"The Facebook platform you also get it to you and your team","recordKey":"1540303150710","crd":"2018-10-23 13:59:13","upd":"2018-10-23 13:59:13"}
     */

    private String status;
    private String message;
    private TreatmentDetailBean treatmentDetail;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public TreatmentDetailBean getTreatmentDetail() {
        return treatmentDetail;
    }

    public void setTreatmentDetail(TreatmentDetailBean treatmentDetail) {
        this.treatmentDetail = treatmentDetail;
    }

    public static class TreatmentDetailBean {
        /**
         * treatmentMappingId : 5
         * treatment_id : 7
         * treatmentDate : 2018-10-20
         * treatmentDescription : The Facebook platform you also get it to you and your team
         * recordKey : 1540303150710
         * crd : 2018-10-23 13:59:13
         * upd : 2018-10-23 13:59:13
         */

        private String treatmentMappingId;
        private String treatment_id;
        private String treatmentDate;
        private String treatmentDescription;
        private String recordKey;
        private String crd;
        private String upd;

        public String getTreatmentMappingId() {
            return treatmentMappingId;
        }

        public void setTreatmentMappingId(String treatmentMappingId) {
            this.treatmentMappingId = treatmentMappingId;
        }

        public String getTreatment_id() {
            return treatment_id;
        }

        public void setTreatment_id(String treatment_id) {
            this.treatment_id = treatment_id;
        }

        public String getTreatmentDate() {
            return treatmentDate.contains("-") ? CalenderUtils.formatDate(treatmentDate, AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT) : treatmentDate;
        }

        public void setTreatmentDate(String treatmentDate) {
            this.treatmentDate = treatmentDate;
        }

        public String getTreatmentDescription() {
            return treatmentDescription;
        }

        public void setTreatmentDescription(String treatmentDescription) {
            this.treatmentDescription = treatmentDescription;
        }

        public String getRecordKey() {
            return recordKey;
        }

        public void setRecordKey(String recordKey) {
            this.recordKey = recordKey;
        }

        public String getCrd() {
            return crd;
        }

        public void setCrd(String crd) {
            this.crd = crd;
        }

        public String getUpd() {
            return upd;
        }

        public void setUpd(String upd) {
            this.upd = upd;
        }
    }
}
