package com.agrinvestapp.data.model.api;

import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.CalenderUtils;

import java.util.List;

/**
 * Created by hemant
 * Date: 9/10/18.
 */

public final class CalvingInfoResponse {

    /**
     * status : success
     * message : Success
     * calvingList : [{"calvingId":"2","animal_id":"94","calvingNumber":"21","calvingDate":"2012-01-12","recordKey":"453245225","crd":"2018-10-09 07:39:16","upd":"2018-10-09 07:39:16"}]
     */

    private String status;
    private String message;
    private List<CalvingListBean> calvingList;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<CalvingListBean> getCalvingList() {
        return calvingList;
    }

    public void setCalvingList(List<CalvingListBean> calvingList) {
        this.calvingList = calvingList;
    }

    public static class CalvingListBean {
        /**
         * calvingId : 2
         * animal_id : 94
         * calvingNumber : 21
         * calvingDate : 2012-01-12
         * recordKey : 453245225
         * crd : 2018-10-09 07:39:16
         * upd : 2018-10-09 07:39:16
         */

        private String calvingId;
        private String animal_id;
        private String calvingNumber;
        private String calvingDate;
        private String recordKey;
        private String crd;
        private String upd;

        public String getCalvingId() {
            return calvingId;
        }

        public void setCalvingId(String calvingId) {
            this.calvingId = calvingId;
        }

        public String getAnimal_id() {
            return animal_id;
        }

        public void setAnimal_id(String animal_id) {
            this.animal_id = animal_id;
        }

        public String getCalvingNumber() {
            return calvingNumber;
        }

        public void setCalvingNumber(String calvingNumber) {
            this.calvingNumber = calvingNumber;
        }

        public String getCalvingDate() {
            return calvingDate.contains("-") ? CalenderUtils.formatDate(calvingDate, AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT) : calvingDate;
        }

        public void setCalvingDate(String calvingDate) {
            this.calvingDate = calvingDate;
        }

        public String getRecordKey() {
            return recordKey;
        }

        public void setRecordKey(String recordKey) {
            this.recordKey = recordKey;
        }

        public String getCrd() {
            return crd;
        }

        public void setCrd(String crd) {
            this.crd = crd;
        }

        public String getUpd() {
            return upd;
        }

        public void setUpd(String upd) {
            this.upd = upd;
        }
    }
}
