package com.agrinvestapp.data.model.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by hemant.
 * Date: 4/9/18
 * Time: 2:14 PM
 */

@Entity(tableName = "users")
public final class UserInfoBean {
    /**
     * userId : 98
     * name : Hemant
     * email : hemant@gmail.com
     * password : $2y$10$wYSHaS5poWvqq3ypOnr86eCT27YZXpVk8Ee3mHNQNQ.XejKplcQ0q
     * contactNumber : 75666612345
     * location : Indore, Madhya Pradesh, India
     * longitude : 75.8577258
     * latitude : 22.719568699999996
     * deviceType : 0
     * deviceToken :
     * socialId :
     * socialType :
     * authToken : 3ef4b812d8255ef2ea81a98c1c7b64fdcb5b9abd
     * status : 1
     * crd : 2018-08-29 10:26:41
     * upd : 0000-00-00 00:00:00
     * profileImage : http://agrinvestonline.com/./uploads/profile/default.png
     */

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "userId")
    private String userId = "";

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "email")
    private String email;

    @ColumnInfo(name = "password")
    private String password;

    @ColumnInfo(name = "profileImage")
    private String profileImage;

    @ColumnInfo(name = "contactNumber")
    private String contactNumber;

    @ColumnInfo(name = "location")
    private String location;

    @ColumnInfo(name = "longitude")
    private String longitude;

    @ColumnInfo(name = "latitude")
    private String latitude;

    @ColumnInfo(name = "language")
    private String language;

    @ColumnInfo(name = "deviceType")
    private String deviceType;

    @ColumnInfo(name = "deviceToken")
    private String deviceToken;

    @ColumnInfo(name = "socialId")
    private String socialId;

    @ColumnInfo(name = "socialType")
    private String socialType;

    @ColumnInfo(name = "authToken")
    private String authToken;

    @ColumnInfo(name = "status")
    private String status;

    @ColumnInfo(name = "userType")
    private String userType;

    @ColumnInfo(name = "crd")
    private String crd;

    @ColumnInfo(name = "upd")
    private String upd;

    @Ignore
    private String subscriptionPlan;

    @Ignore
    private String subscriptionSaleId;

    @Ignore
    private Boolean isLocalDataExistForSync;

    @Ignore
    private Boolean isAppOnline;

    @Ignore
    private String syncTimer;

    @NonNull
    public String getUserId() {
        return userId;
    }

    public void setUserId(@NonNull String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getSocialId() {
        return socialId;
    }

    public void setSocialId(String socialId) {
        this.socialId = socialId;
    }

    public String getSocialType() {
        return socialType;
    }

    public void setSocialType(String socialType) {
        this.socialType = socialType;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCrd() {
        return crd;
    }

    public void setCrd(String crd) {
        this.crd = crd;
    }

    public String getUpd() {
        return upd;
    }

    public void setUpd(String upd) {
        this.upd = upd;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Boolean getLocalDataExistForSync() {
        return isLocalDataExistForSync;
    }

    public void setLocalDataExistForSync(Boolean localDataExistForSync) {
        isLocalDataExistForSync = localDataExistForSync;
    }

    public Boolean getAppOnline() {
        return isAppOnline;
    }

    public void setAppOnline(Boolean appOnline) {
        isAppOnline = appOnline;
    }

    public String getSyncTimer() {
        return syncTimer;
    }

    public void setSyncTimer(String syncTimer) {
        this.syncTimer = syncTimer;
    }

    public String getSubscriptionPlan() {
        return subscriptionPlan;
    }

    public void setSubscriptionPlan(String subscriptionPlan) {
        this.subscriptionPlan = subscriptionPlan;
    }

    public String getSubscriptionSaleId() {
        return subscriptionSaleId;
    }

    public void setSubscriptionSaleId(String subscriptionSaleId) {
        this.subscriptionSaleId = subscriptionSaleId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }
}
