package com.agrinvestapp.data.model.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;


@Entity(tableName = "parcel", indices = {@Index(value = {"property_id"})},
        foreignKeys = @ForeignKey(entity = Property.class,
                parentColumns = "propertyId",
                childColumns = "property_id",
                onDelete = ForeignKey.CASCADE))
public final class Parcel {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "parcelId")
    private String parcelId;

    @ColumnInfo(name = "parcelName")
    private String parcelName;

    /*@ForeignKey(entity = Property.class,
            parentColumns = "propertyId",
            childColumns = "property_id")*/
    @ColumnInfo(name = "property_id")
    private String property_id;

    @ColumnInfo(name = "recordKey")
    private String recordKey;

    @ColumnInfo(name = "status")
    private String status;

    @ColumnInfo(name = "crd")
    private String crd;

    @ColumnInfo(name = "upd")
    private String upd;

    @ColumnInfo(name = "isDataSync")
    private Boolean isDataSync;

    //SerializedName used for creating JSONArray
    @SerializedName("requestType")
    @ColumnInfo(name = "event")
    private String event;

    @NonNull
    public String getParcelId() {
        return parcelId;
    }

    public void setParcelId(@NonNull String parcelId) {
        this.parcelId = parcelId;
    }

    public String getParcelName() {
        return parcelName;
    }

    public void setParcelName(String parcelName) {
        this.parcelName = parcelName;
    }

    public String getProperty_id() {
        return property_id;
    }

    public void setProperty_id(String property_id) {
        this.property_id = property_id;
    }

    public String getRecordKey() {
        return recordKey;
    }

    public void setRecordKey(String recordKey) {
        this.recordKey = recordKey == null ? "" : recordKey;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCrd() {
        return crd;
    }

    public void setCrd(String crd) {
        this.crd = crd;
    }

    public String getUpd() {
        return upd;
    }

    public void setUpd(String upd) {
        this.upd = upd;
    }

    public Boolean getDataSync() {
        return isDataSync;
    }

    public void setDataSync(Boolean dataSync) {
        isDataSync = dataSync;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }
}
