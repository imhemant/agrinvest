package com.agrinvestapp.data.model.api;

import java.util.List;

/**
 * Created by hemant
 * Date: 26/9/18.
 */

public final class MotherResponse {

    /**
     * status : success
     * message : OK
     * animalMotherList : [{"tagNumber":"#1235"},{"tagNumber":"#1231"}]
     */

    private String status;
    private String message;
    private List<AnimalMotherListBean> animalMotherList;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<AnimalMotherListBean> getAnimalMotherList() {
        return animalMotherList;
    }

    public void setAnimalMotherList(List<AnimalMotherListBean> animalMotherList) {
        this.animalMotherList = animalMotherList;
    }

    public static class AnimalMotherListBean {
        /**
         * tagNumber : #1235
         */

        private String tagNumber;

        public String getTagNumber() {
            return tagNumber;
        }

        public void setTagNumber(String tagNumber) {
            this.tagNumber = tagNumber;
        }
    }
}
