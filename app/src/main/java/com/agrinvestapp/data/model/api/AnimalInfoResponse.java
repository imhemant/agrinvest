package com.agrinvestapp.data.model.api;

import android.arch.persistence.room.Ignore;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by hemant
 * Date: 26/9/18.
 */

public final class AnimalInfoResponse {

    /**
     * status : success
     * message : OK
     * animalList : [{"parcelName":"Parcel test01","animalId":"22","parcel_id":"48","lotId":"#12","tagNumber":"#1231","bornOrBuyDate":"2017-01-10","mother":"","boughtFrom":"Bought from indore","description":"this is my firs animal info","dismant":"0000-00-00","markingOrTagging":"0000-00-00","saleDate":"0000-00-00","salePrice":"0","saleQuantity":"0","saleTotalPrice":"0","status":"0","recordKey":"13133546779844","crd":"2018-09-25 18:07:14","upd":"2018-09-25 18:07:14"}]
     */

    private String status;
    private String message;
    private List<AnimalListBean> animalList;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<AnimalListBean> getAnimalList() {
        return animalList;
    }

    public void setAnimalList(List<AnimalListBean> animalList) {
        this.animalList = animalList;
    }

    public static class AnimalListBean implements Parcelable {


        public static final Creator<AnimalListBean> CREATOR = new Creator<AnimalListBean>() {
            @Override
            public AnimalListBean createFromParcel(Parcel in) {
                return new AnimalListBean(in);
            }

            @Override
            public AnimalListBean[] newArray(int size) {
                return new AnimalListBean[size];
            }
        };
        @Ignore
        public Boolean isSelected = false;
        /**
         * parcelName : Parcel test01
         * animalId : 22
         * parcel_id : 48
         * lotId : #12
         * tagNumber : #1231
         * bornOrBuyDate : 2017-01-10
         * mother :
         * boughtFrom : Bought from indore
         * description : this is my firs animal info
         * dismant : 0000-00-00
         * markingOrTagging : 0000-00-00
         * saleDate : 0000-00-00
         * salePrice : 0
         * saleQuantity : 0
         * saleTotalPrice : 0
         * status : 0
         * recordKey : 13133546779844
         * crd : 2018-09-25 18:07:14
         * upd : 2018-09-25 18:07:14
         */

        private String propertyId;
        private String parcelName;
        private String parcelRecordKey;
        private String animalId;
        private String parcel_id;
        private String lotId;
        private String tagNumber;
        private String bornOrBuyDate;
        private String mother;
        private String boughtFrom;
        private String description;
        private String dismant;
        private String markingOrTagging;
        private String saleDate;
        private String salePrice;
        private String saleQuantity;
        private String saleTotalPrice;
        private String costFrom;
        private String costTo;
        private String costTotal;
        private String status;
        private String recordKey;
        private String crd;
        private String upd;


        public AnimalListBean() {
        }

        protected AnimalListBean(Parcel in) {
            byte tmpIsSelected = in.readByte();
            isSelected = tmpIsSelected == 0 ? null : tmpIsSelected == 1;
            propertyId = in.readString();
            parcelName = in.readString();
            parcelRecordKey = in.readString();
            animalId = in.readString();
            parcel_id = in.readString();
            lotId = in.readString();
            tagNumber = in.readString();
            bornOrBuyDate = in.readString();
            mother = in.readString();
            boughtFrom = in.readString();
            description = in.readString();
            dismant = in.readString();
            markingOrTagging = in.readString();
            saleDate = in.readString();
            salePrice = in.readString();
            saleQuantity = in.readString();
            saleTotalPrice = in.readString();
            costFrom = in.readString();
            costTo = in.readString();
            costTotal = in.readString();
            status = in.readString();
            recordKey = in.readString();
            crd = in.readString();
            upd = in.readString();
        }

        public String getPropertyId() {
            return propertyId;
        }

        public void setPropertyId(String propertyId) {
            this.propertyId = propertyId;
        }

        public String getParcelName() {
            return parcelName;
        }

        public void setParcelName(String parcelName) {
            this.parcelName = parcelName;
        }

        public String getParcelRecordKey() {
            return parcelRecordKey;
        }

        public void setParcelRecordKey(String parcelRecordKey) {
            this.parcelRecordKey = parcelRecordKey;
        }

        public String getAnimalId() {
            return animalId;
        }

        public void setAnimalId(String animalId) {
            this.animalId = animalId;
        }

        public String getParcel_id() {
            return parcel_id;
        }

        public void setParcel_id(String parcel_id) {
            this.parcel_id = parcel_id;
        }

        public String getLotId() {
            return lotId;
        }

        public void setLotId(String lotId) {
            this.lotId = lotId;
        }

        public String getTagNumber() {
            return tagNumber;
        }

        public void setTagNumber(String tagNumber) {
            this.tagNumber = tagNumber;
        }

        public String getBornOrBuyDate() {
            return bornOrBuyDate;
        }

        public void setBornOrBuyDate(String bornOrBuyDate) {
            this.bornOrBuyDate = bornOrBuyDate;
        }

        public String getMother() {
            return mother;
        }

        public void setMother(String mother) {
            this.mother = mother;
        }

        public String getBoughtFrom() {
            return boughtFrom;
        }

        public void setBoughtFrom(String boughtFrom) {
            this.boughtFrom = boughtFrom;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getDismant() {
            return dismant;
        }

        public void setDismant(String dismant) {
            this.dismant = dismant;
        }

        public String getMarkingOrTagging() {
            return markingOrTagging;
        }

        public void setMarkingOrTagging(String markingOrTagging) {
            this.markingOrTagging = markingOrTagging;
        }

        public String getSaleDate() {
            return saleDate;
        }

        public void setSaleDate(String saleDate) {
            this.saleDate = saleDate;
        }

        public String getSalePrice() {
            return salePrice;
        }

        public void setSalePrice(String salePrice) {
            this.salePrice = salePrice;
        }

        public String getSaleQuantity() {
            return saleQuantity;
        }

        public void setSaleQuantity(String saleQuantity) {
            this.saleQuantity = saleQuantity;
        }

        public String getSaleTotalPrice() {
            return saleTotalPrice;
        }

        public void setSaleTotalPrice(String saleTotalPrice) {
            this.saleTotalPrice = saleTotalPrice;
        }

        public String getCostFrom() {
            return costFrom;
        }

        public void setCostFrom(String costFrom) {
            this.costFrom = costFrom;
        }

        public String getCostTo() {
            return costTo;
        }

        public void setCostTo(String costTo) {
            this.costTo = costTo;
        }

        public String getCostTotal() {
            return costTotal;
        }

        public void setCostTotal(String costTotal) {
            this.costTotal = costTotal;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getRecordKey() {
            return recordKey;
        }

        public void setRecordKey(String recordKey) {
            this.recordKey = recordKey;
        }

        public String getCrd() {
            return crd;
        }

        public void setCrd(String crd) {
            this.crd = crd;
        }

        public String getUpd() {
            return upd;
        }

        public void setUpd(String upd) {
            this.upd = upd;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeByte((byte) (isSelected == null ? 0 : isSelected ? 1 : 2));
            dest.writeString(propertyId);
            dest.writeString(parcelName);
            dest.writeString(parcelRecordKey);
            dest.writeString(animalId);
            dest.writeString(parcel_id);
            dest.writeString(lotId);
            dest.writeString(tagNumber);
            dest.writeString(bornOrBuyDate);
            dest.writeString(mother);
            dest.writeString(boughtFrom);
            dest.writeString(description);
            dest.writeString(dismant);
            dest.writeString(markingOrTagging);
            dest.writeString(saleDate);
            dest.writeString(salePrice);
            dest.writeString(saleQuantity);
            dest.writeString(saleTotalPrice);
            dest.writeString(costFrom);
            dest.writeString(costTo);
            dest.writeString(costTotal);
            dest.writeString(status);
            dest.writeString(recordKey);
            dest.writeString(crd);
            dest.writeString(upd);
        }
    }
}
