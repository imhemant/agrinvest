package com.agrinvestapp.data.model.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "property"/*, indices = {@Index(value = {"user_id"})},
        foreignKeys = @ForeignKey(entity = UserInfoBean.class,
                parentColumns = "userId",
                childColumns = "user_id",
                onDelete = ForeignKey.CASCADE)*/)
public final class Property {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "propertyId")
    private String propertyId;

    @ColumnInfo(name = "propertyName")
    private String propertyName;

    @ForeignKey(entity = UserInfoBean.class,
            parentColumns = "userId",
            childColumns = "user_id")
    @ColumnInfo(name = "user_id")
    private String user_id;

    @ColumnInfo(name = "location")
    private String location;

    @ColumnInfo(name = "longitude")
    private String longitude;

    @ColumnInfo(name = "latitude")
    private String latitude;


    @ColumnInfo(name = "size")
    private String size;

    @ColumnInfo(name = "sizeUnite")
    private String sizeUnite;

    @ColumnInfo(name = "description")
    private String description;

    @ColumnInfo(name = "status")
    private String status;

    @ColumnInfo(name = "crd")
    private String crd;

    @ColumnInfo(name = "upd")
    private String upd;

    @ColumnInfo(name = "isDataSync")
    private Boolean isDataSync;

    @ColumnInfo(name = "event")
    private String event;

    @NonNull
    public String getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(@NonNull String propertyId) {
        this.propertyId = propertyId;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getSizeUnite() {
        return sizeUnite;
    }

    public void setSizeUnite(String sizeUnite) {
        this.sizeUnite = sizeUnite;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCrd() {
        return crd;
    }

    public void setCrd(String crd) {
        this.crd = crd;
    }

    public String getUpd() {
        return upd;
    }

    public void setUpd(String upd) {
        this.upd = upd;
    }

    public Boolean getDataSync() {
        return isDataSync;
    }

    public void setDataSync(Boolean dataSync) {
        isDataSync = dataSync;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }
}
