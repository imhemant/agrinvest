package com.agrinvestapp.data.model.api;

/**
 * Created by hemant
 * Date: 10/4/18.
 */

public final class LoginResponse {

    /**
     * status : success
     * message : Login successfully done!
     * userDetail : {"userId":"98","name":"Hemant","email":"hemant@gmail.com","password":"$2y$10$wYSHaS5poWvqq3ypOnr86eCT27YZXpVk8Ee3mHNQNQ.XejKplcQ0q","contactNumber":"75666612345","location":"Indore, Madhya Pradesh, India","longitude":"75.8577258","latitude":"22.719568699999996","deviceType":"0","deviceToken":"","socialId":"","socialType":"","authToken":"3ef4b812d8255ef2ea81a98c1c7b64fdcb5b9abd","status":"1","crd":"2018-08-29 10:26:41","upd":"0000-00-00 00:00:00","profileImage":"http://agrinvestonline.com/./uploads/profile/default.png"}
     */

    private String status;
    private String message;
    private UserDetailBean userDetail;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserDetailBean getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(UserDetailBean userDetail) {
        this.userDetail = userDetail;
    }

    public static class UserDetailBean {
        /**
         * userId : 98
         * name : Hemant
         * email : hemant@gmail.com
         * password : $2y$10$wYSHaS5poWvqq3ypOnr86eCT27YZXpVk8Ee3mHNQNQ.XejKplcQ0q
         * contactNumber : 75666612345
         * location : Indore, Madhya Pradesh, India
         * longitude : 75.8577258
         * latitude : 22.719568699999996
         * language": "english",
         * subscriptionPlan": "free",
         * subscriptionSaleId": "",
         * deviceType : 0
         * deviceToken :
         * socialId :
         * socialType :
         * authToken : 3ef4b812d8255ef2ea81a98c1c7b64fdcb5b9abd
         * status : 1
         * crd : 2018-08-29 10:26:41
         * upd : 0000-00-00 00:00:00
         * profileImage : http://agrinvestonline.com/./uploads/profile/default.png
         */

        private String userId;
        private String name;
        private String email;
        private String password;
        private String contactNumber;
        private String location;
        private String longitude;
        private String latitude;
        private String language;
        private String deviceType;
        private String deviceToken;
        private String socialId;
        private String socialType;
        private String authToken;
        private String status;
        private String userType;
        private String crd;
        private String upd;
        private String profileImage;
        private String subscriptionPlan;
        private String subscriptionSaleId;

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getContactNumber() {
            return contactNumber;
        }

        public void setContactNumber(String contactNumber) {
            this.contactNumber = contactNumber;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public String getDeviceToken() {
            return deviceToken;
        }

        public void setDeviceToken(String deviceToken) {
            this.deviceToken = deviceToken;
        }

        public String getSocialId() {
            return socialId;
        }

        public void setSocialId(String socialId) {
            this.socialId = socialId;
        }

        public String getSocialType() {
            return socialType;
        }

        public void setSocialType(String socialType) {
            this.socialType = socialType;
        }

        public String getAuthToken() {
            return authToken;
        }

        public void setAuthToken(String authToken) {
            this.authToken = authToken;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getUserType() {
            return userType;
        }

        public void setUserType(String userType) {
            this.userType = userType;
        }

        public String getCrd() {
            return crd;
        }

        public void setCrd(String crd) {
            this.crd = crd;
        }

        public String getUpd() {
            return upd;
        }

        public void setUpd(String upd) {
            this.upd = upd;
        }

        public String getProfileImage() {
            return profileImage;
        }

        public void setProfileImage(String profileImage) {
            this.profileImage = profileImage;
        }

        public String getSubscriptionPlan() {
            return subscriptionPlan;
        }

        public void setSubscriptionPlan(String subscriptionPlan) {
            this.subscriptionPlan = subscriptionPlan;
        }

        public String getSubscriptionSaleId() {
            return subscriptionSaleId;
        }

        public void setSubscriptionSaleId(String subscriptionSaleId) {
            this.subscriptionSaleId = subscriptionSaleId;
        }
    }
}
