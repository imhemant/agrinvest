package com.agrinvestapp.data.model.other;

/**
 * Created by hemant
 * Date: 9/10/18.
 */

public final class CalvingSyncResult {

    private String requestType;

    private String recordKey; //Animal recordKey

    private String calvingNumber;

    private String calvingRecordKey;

    private String calvingId;

    private String calvingDate;

    private Boolean isDataSync;

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getRecordKey() {
        return recordKey;
    }

    public void setRecordKey(String recordKey) {
        this.recordKey = recordKey;
    }

    public String getCalvingNumber() {
        return calvingNumber;
    }

    public void setCalvingNumber(String calvingNumber) {
        this.calvingNumber = calvingNumber;
    }

    public String getCalvingRecordKey() {
        return calvingRecordKey;
    }

    public void setCalvingRecordKey(String calvingRecordKey) {
        this.calvingRecordKey = calvingRecordKey;
    }

    public String getCalvingId() {
        return calvingId;
    }

    public void setCalvingId(String calvingId) {
        this.calvingId = calvingId;
    }

    public String getCalvingDate() {
        return calvingDate;
    }

    public void setCalvingDate(String calvingDate) {
        this.calvingDate = calvingDate;
    }

    public Boolean getDataSync() {
        return isDataSync;
    }

    public void setDataSync(Boolean dataSync) {
        isDataSync = dataSync;
    }
}
