package com.agrinvestapp.data.model.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "raindata", indices = {@Index(value = {"property_id"})},
        foreignKeys = @ForeignKey(entity = Property.class,
                parentColumns = "propertyId",
                childColumns = "property_id",
                onDelete = ForeignKey.CASCADE))
public final class RainData {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "raindataId")
    private String raindataId;

    /*@ForeignKey(entity = Property.class,
            parentColumns = "propertyId",
            childColumns = "property_id")*/
    @ColumnInfo(name = "property_id")
    private String property_id;

    @ColumnInfo(name = "fromDate")
    private String fromDate;

    @ColumnInfo(name = "toDate")
    private String toDate;

    @ColumnInfo(name = "quantity")
    private String quantity;

    @ColumnInfo(name = "quantityUnite")
    private String quantityUnite;

    @ColumnInfo(name = "status")
    private String status;

    @ColumnInfo(name = "crd")
    private String crd;

    @ColumnInfo(name = "upd")
    private String upd;

    @ColumnInfo(name = "isDataSync")
    private Boolean isDataSync;

    @ColumnInfo(name = "event")
    private String event;

    @NonNull
    public String getRaindataId() {
        return raindataId;
    }

    public void setRaindataId(@NonNull String raindataId) {
        this.raindataId = raindataId;
    }

    public String getProperty_id() {
        return property_id;
    }

    public void setProperty_id(String property_id) {
        this.property_id = property_id;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getQuantityUnite() {
        return quantityUnite;
    }

    public void setQuantityUnite(String quantityUnite) {
        this.quantityUnite = quantityUnite;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCrd() {
        return crd;
    }

    public void setCrd(String crd) {
        this.crd = crd;
    }

    public String getUpd() {
        return upd;
    }

    public void setUpd(String upd) {
        this.upd = upd;
    }

    public Boolean getDataSync() {
        return isDataSync;
    }

    public void setDataSync(Boolean dataSync) {
        isDataSync = dataSync;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }
}
