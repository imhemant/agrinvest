package com.agrinvestapp.data.model.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import com.agrinvestapp.utils.DateConverter;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by hemant
 * Date: 19/11/18.
 */

@Entity(tableName = "expense")
public final class Expense {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "expenseId")
    private String expenseId = "";

    @ColumnInfo(name = "user_id")
    private String user_id;

    @ColumnInfo(name = "dateBought")
    private String dateBought;

    @ColumnInfo(name = "cost")
    private String cost;

    @ColumnInfo(name = "description")
    private String description;

    @ColumnInfo(name = "dateApplied")
    @TypeConverters({DateConverter.class})
    private Date dateApplied;

    @ColumnInfo(name = "expenseFor")
    private String expenseFor;

    @ColumnInfo(name = "recordKey")
    private String recordKey;

    @ColumnInfo(name = "status")
    private String status;

    @ColumnInfo(name = "crd")
    private String crd;

    @ColumnInfo(name = "upd")
    private String upd;

    @ColumnInfo(name = "isDataSync")
    private Boolean isDataSync;

    @SerializedName("requestType")
    @ColumnInfo(name = "event")
    private String event;

    @NonNull
    public String getExpenseId() {
        return expenseId;
    }

    public void setExpenseId(@NonNull String expenseId) {
        this.expenseId = expenseId;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getDateBought() {
        return dateBought;
    }

    public void setDateBought(String dateBought) {
        this.dateBought = dateBought;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateApplied() {
        return dateApplied;
    }

    public void setDateApplied(Date dateApplied) {
        this.dateApplied = dateApplied;
    }

    public String getExpenseFor() {
        return expenseFor;
    }

    public void setExpenseFor(String expenseFor) {
        this.expenseFor = expenseFor;
    }

    public String getRecordKey() {
        return recordKey;
    }

    public void setRecordKey(String recordKey) {
        this.recordKey = recordKey;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCrd() {
        return crd;
    }

    public void setCrd(String crd) {
        this.crd = crd;
    }

    public String getUpd() {
        return upd;
    }

    public void setUpd(String upd) {
        this.upd = upd;
    }

    public Boolean getDataSync() {
        return isDataSync;
    }

    public void setDataSync(Boolean dataSync) {
        isDataSync = dataSync;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }
}
