package com.agrinvestapp.data.model.api;

import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.utils.CalenderUtils;

import java.util.List;

/**
 * Created by hemant
 * Date: 9/10/18.
 */

public final class InseminationInfoResponse {

    /**
     * status : success
     * message : Success
     * inseminationList : [{"inseminationId":"2","animal_id":"94","inseminationName":"In1edit","inseminationDate":"2018-05-14","inseminationDescription":"first record edit","recordKey":"9194191","crd":"2018-10-03 07:42:04","upd":"2018-10-03 07:42:04"},{"inseminationId":"3","animal_id":"94","inseminationName":"In1edityyu","inseminationDate":"2018-05-14","inseminationDescription":"first record edit","recordKey":"91941917","crd":"2018-10-03 09:01:34","upd":"2018-10-03 09:01:34"},{"inseminationId":"4","animal_id":"94","inseminationName":"In1edit","inseminationDate":"2018-05-14","inseminationDescription":"first record edit","recordKey":"91941914","crd":"2018-10-09 05:28:59","upd":"2018-10-09 05:28:59"},{"inseminationId":"5","animal_id":"94","inseminationName":"In1edit","inseminationDate":"2018-05-14","inseminationDescription":"first record edit","recordKey":"919419144","crd":"2018-10-09 07:31:23","upd":"2018-10-09 07:31:23"}]
     */

    private String status;
    private String message;
    private List<InseminationListBean> inseminationList;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<InseminationListBean> getInseminationList() {
        return inseminationList;
    }

    public void setInseminationList(List<InseminationListBean> inseminationList) {
        this.inseminationList = inseminationList;
    }

    public static class InseminationListBean {
        /**
         * inseminationId : 2
         * animal_id : 94
         * inseminationName : In1edit
         * inseminationDate : 2018-05-14
         * inseminationDescription : first record edit
         * recordKey : 9194191
         * crd : 2018-10-03 07:42:04
         * upd : 2018-10-03 07:42:04
         */

        private String inseminationId;
        private String animal_id;
        private String inseminationName;
        private String inseminationDate;
        private String inseminationDescription;
        private String recordKey;
        private String crd;
        private String upd;

        public String getInseminationId() {
            return inseminationId;
        }

        public void setInseminationId(String inseminationId) {
            this.inseminationId = inseminationId;
        }

        public String getAnimal_id() {
            return animal_id;
        }

        public void setAnimal_id(String animal_id) {
            this.animal_id = animal_id;
        }

        public String getInseminationName() {
            return inseminationName;
        }

        public void setInseminationName(String inseminationName) {
            this.inseminationName = inseminationName;
        }

        public String getInseminationDate() {
            return inseminationDate.contains("-") ? CalenderUtils.formatDate(inseminationDate, AppConstants.SERVER_TIMESTAMP_FORMAT, AppConstants.TIMESTAMP_FORMAT) : inseminationDate;
        }

        public void setInseminationDate(String inseminationDate) {
            this.inseminationDate = inseminationDate;
        }

        public String getInseminationDescription() {
            return inseminationDescription;
        }

        public void setInseminationDescription(String inseminationDescription) {
            this.inseminationDescription = inseminationDescription;
        }

        public String getRecordKey() {
            return recordKey;
        }

        public void setRecordKey(String recordKey) {
            this.recordKey = recordKey;
        }

        public String getCrd() {
            return crd;
        }

        public void setCrd(String crd) {
            this.crd = crd;
        }

        public String getUpd() {
            return upd;
        }

        public void setUpd(String upd) {
            this.upd = upd;
        }
    }
}
