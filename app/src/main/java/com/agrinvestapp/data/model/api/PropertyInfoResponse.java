package com.agrinvestapp.data.model.api;

import android.arch.persistence.room.Ignore;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public final class PropertyInfoResponse {

    /**
     * status : success
     * message : OK
     * propertyInfo : [{"propertyId":"5","user_id":"2","propertyName":"Sun Flowers Farm1","location":"malviya nagar near barphani dham","longitude":"3.0111","latitude":"3.02121","size":"","sizeUnite":"","description":"","status":"0","crd":"2018-06-12 19:13:53","upd":"0000-00-00 00:00:00","rainCount":"0","parcelCount":"1","animalCount":"2","parcelList":[{"parcelId":"10","parcelName":"test1","property_id":"5","status":"0","crd":"2018-09-06 19:47:28","upd":"0000-00-00 00:00:00"}],"raindataList":[{"raindataId":"20","property_id":"47","fromDate":"2018-09-19","toDate":"2018-09-28","quantity":"12","quantityUnite":"mm","status":"1","crd":"2018-09-06 14:51:06","upd":"0000-00-00 00:00:00"},{"raindataId":"21","property_id":"47","fromDate":"2018-09-20","toDate":"0000-00-00","quantity":"4","quantityUnite":"in","status":"1","crd":"2018-09-06 14:51:06","upd":"0000-00-00 00:00:00"}],"animalList":[{"animalId":"14","parcel_id":"10","lotId":"#1233","tagNumber":"#12344","bornOrBuyDate":"10/10/2017","mother":"Bought from3","boughtFrom":"indore","description":"test","dismant":"","marketingOrTagging":"","treatment":"","insemination":"","pregnancyTest":"","moveToDifferentParcel":"","calving":"","costToDate":"","sale":"","salePrice":"0","saleQuantity":"0","saleTotalPrice":"0","treatmentDescription":"","status":"0","crd":"2018-07-09 19:17:28","upd":"2018-06-29 12:59:31"},{"animalId":"16","parcel_id":"10","lotId":"#12334","tagNumber":"#12344d","bornOrBuyDate":"10/10/2017","mother":"Bought from4","boughtFrom":"indore","description":"this is my firs animal info","dismant":"","marketingOrTagging":"","treatment":"","insemination":"","pregnancyTest":"","moveToDifferentParcel":"","calving":"","costToDate":"","sale":"","salePrice":"0","saleQuantity":"0","saleTotalPrice":"0","treatmentDescription":"","status":"0","crd":"2018-07-09 19:17:32","upd":"0000-00-00 00:00:00"}]}]
     */

    private String status;
    private String message;
    private List<PropertyInfoBean> propertyInfo;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<PropertyInfoBean> getPropertyInfo() {
        return propertyInfo;
    }

    public void setPropertyInfo(List<PropertyInfoBean> propertyInfo) {
        this.propertyInfo = propertyInfo;
    }

    public static class PropertyInfoBean implements Parcelable {
        public static final Creator<PropertyInfoBean> CREATOR = new Creator<PropertyInfoBean>() {
            @Override
            public PropertyInfoBean createFromParcel(Parcel in) {
                return new PropertyInfoBean(in);
            }

            @Override
            public PropertyInfoBean[] newArray(int size) {
                return new PropertyInfoBean[size];
            }
        };
        @Ignore
        public Boolean isSelected = false;
        /**
         * propertyId : 5
         * user_id : 2
         * propertyName : Sun Flowers Farm1
         * location : malviya nagar near barphani dham
         * longitude : 3.0111
         * latitude : 3.02121
         * size :
         * sizeUnite :
         * description :
         * status : 0
         * crd : 2018-06-12 19:13:53
         * upd : 0000-00-00 00:00:00
         * rainCount : 0
         * parcelCount : 1
         * animalCount : 2
         * parcelList : [{"parcelId":"10","parcelName":"test1","property_id":"5","status":"0","crd":"2018-09-06 19:47:28","upd":"0000-00-00 00:00:00"}]
         * raindataList : [{"raindataId":"20","property_id":"47","fromDate":"2018-09-19","toDate":"2018-09-28","quantity":"12","quantityUnite":"mm","status":"1","crd":"2018-09-06 14:51:06","upd":"0000-00-00 00:00:00"},{"raindataId":"21","property_id":"47","fromDate":"2018-09-20","toDate":"0000-00-00","quantity":"4","quantityUnite":"in","status":"1","crd":"2018-09-06 14:51:06","upd":"0000-00-00 00:00:00"}]
         * animalList : [{"animalId":"14","parcel_id":"10","lotId":"#1233","tagNumber":"#12344","bornOrBuyDate":"10/10/2017","mother":"Bought from3","boughtFrom":"indore","description":"test","dismant":"","marketingOrTagging":"","treatment":"","insemination":"","pregnancyTest":"","moveToDifferentParcel":"","calving":"","costToDate":"","sale":"","salePrice":"0","saleQuantity":"0","saleTotalPrice":"0","treatmentDescription":"","status":"0","crd":"2018-07-09 19:17:28","upd":"2018-06-29 12:59:31"},{"animalId":"16","parcel_id":"10","lotId":"#12334","tagNumber":"#12344d","bornOrBuyDate":"10/10/2017","mother":"Bought from4","boughtFrom":"indore","description":"this is my firs animal info","dismant":"","marketingOrTagging":"","treatment":"","insemination":"","pregnancyTest":"","moveToDifferentParcel":"","calving":"","costToDate":"","sale":"","salePrice":"0","saleQuantity":"0","saleTotalPrice":"0","treatmentDescription":"","status":"0","crd":"2018-07-09 19:17:32","upd":"0000-00-00 00:00:00"}]
         */

        private String propertyId;
        private String user_id;
        private String propertyName;
        private String location;
        private String longitude;
        private String latitude;
        private String size;
        private String sizeUnite;
        private String description;
        private String status;
        private String crd;
        private String upd;
        private String rainCount;
        private String parcelCount;
        private String animalCount;
        @Ignore
        private List<ParcelListBean> parcelList;
        @Ignore
        private List<RaindataListBean> raindataList;
        @Ignore
        private List<AnimalListBean> animalList;

        public PropertyInfoBean(){}

        protected PropertyInfoBean(Parcel in) {
            propertyId = in.readString();
            user_id = in.readString();
            propertyName = in.readString();
            location = in.readString();
            longitude = in.readString();
            latitude = in.readString();
            size = in.readString();
            sizeUnite = in.readString();
            description = in.readString();
            status = in.readString();
            crd = in.readString();
            upd = in.readString();
            rainCount = in.readString();
            parcelCount = in.readString();
            animalCount = in.readString();
            parcelList = in.createTypedArrayList(ParcelListBean.CREATOR);
            raindataList = in.createTypedArrayList(RaindataListBean.CREATOR);
            animalList = in.createTypedArrayList(AnimalListBean.CREATOR);
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(propertyId);
            dest.writeString(user_id);
            dest.writeString(propertyName);
            dest.writeString(location);
            dest.writeString(longitude);
            dest.writeString(latitude);
            dest.writeString(size);
            dest.writeString(sizeUnite);
            dest.writeString(description);
            dest.writeString(status);
            dest.writeString(crd);
            dest.writeString(upd);
            dest.writeString(rainCount);
            dest.writeString(parcelCount);
            dest.writeString(animalCount);
            dest.writeTypedList(parcelList);
            dest.writeTypedList(raindataList);
            dest.writeTypedList(animalList);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public String getPropertyId() {
            return propertyId;
        }

        public void setPropertyId(String propertyId) {
            this.propertyId = propertyId;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getPropertyName() {
            return propertyName;
        }

        public void setPropertyName(String propertyName) {
            this.propertyName = propertyName;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getSize() {
            return size;
        }

        public void setSize(String size) {
            this.size = size;
        }

        public String getSizeUnite() {
            return sizeUnite;
        }

        public void setSizeUnite(String sizeUnite) {
            this.sizeUnite = sizeUnite;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCrd() {
            return crd;
        }

        public void setCrd(String crd) {
            this.crd = crd;
        }

        public String getUpd() {
            return upd;
        }

        public void setUpd(String upd) {
            this.upd = upd;
        }

        public String getRainCount() {
            return rainCount;
        }

        public void setRainCount(String rainCount) {
            this.rainCount = rainCount;
        }

        public String getParcelCount() {
            return parcelCount;
        }

        public void setParcelCount(String parcelCount) {
            this.parcelCount = parcelCount;
        }

        public String getAnimalCount() {
            return animalCount;
        }

        public void setAnimalCount(String animalCount) {
            this.animalCount = animalCount;
        }

        public List<ParcelListBean> getParcelList() {
            return parcelList;
        }

        public void setParcelList(List<ParcelListBean> parcelList) {
            this.parcelList = parcelList;
        }

        public List<RaindataListBean> getRaindataList() {
            return raindataList;
        }

        public void setRaindataList(List<RaindataListBean> raindataList) {
            this.raindataList = raindataList;
        }

        public List<AnimalListBean> getAnimalList() {
            return animalList;
        }

        public void setAnimalList(List<AnimalListBean> animalList) {
            this.animalList = animalList;
        }

        public static class ParcelListBean implements Parcelable{
            public static final Creator<ParcelListBean> CREATOR = new Creator<ParcelListBean>() {
                @Override
                public ParcelListBean createFromParcel(Parcel in) {
                    return new ParcelListBean(in);
                }

                @Override
                public ParcelListBean[] newArray(int size) {
                    return new ParcelListBean[size];
                }
            };
            /**
             * parcelId : 10
             * parcelName : test1
             * property_id : 5
             * status : 0
             * crd : 2018-09-06 19:47:28
             * upd : 0000-00-00 00:00:00
             */

            private String parcelId;
            private String parcelName;
            private String property_id;
            @SerializedName("recordKey")
            private String parcelRecordKey;
            private String status;
            private String crd;
            private String upd;

            public ParcelListBean() {
            }

            ParcelListBean(Parcel in) {
                this.parcelId = in.readString();
                this.parcelName = in.readString();
                this.property_id = in.readString();
                this.parcelRecordKey = in.readString();
                this.status = in.readString();
                this.crd = in.readString();
                this.upd = in.readString();

            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {

                dest.writeString(this.parcelId);
                dest.writeString(this.parcelName);
                dest.writeString(this.property_id);
                dest.writeString(this.parcelRecordKey);
                dest.writeString(this.status);
                dest.writeString(this.crd);
                dest.writeString(this.upd);
            }


            public String getParcelId() {
                return parcelId;
            }

            public void setParcelId(String parcelId) {
                this.parcelId = parcelId;
            }

            public String getParcelName() {
                return parcelName;
            }

            public void setParcelName(String parcelName) {
                this.parcelName = parcelName;
            }

            public String getProperty_id() {
                return property_id;
            }

            public void setProperty_id(String property_id) {
                this.property_id = property_id;
            }

            public String getParcelRecordKey() {
                return parcelRecordKey;
            }

            public void setParcelRecordKey(String parcelRecordKey) {
                this.parcelRecordKey = parcelRecordKey;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getCrd() {
                return crd;
            }

            public void setCrd(String crd) {
                this.crd = crd;
            }

            public String getUpd() {
                return upd;
            }

            public void setUpd(String upd) {
                this.upd = upd;
            }
        }

        public static class RaindataListBean implements Parcelable{
            public static final Creator<RaindataListBean> CREATOR = new Creator<RaindataListBean>() {
                @Override
                public RaindataListBean createFromParcel(Parcel in) {
                    return new RaindataListBean(in);
                }

                @Override
                public RaindataListBean[] newArray(int size) {
                    return new RaindataListBean[size];
                }
            };
            /**
             * raindataId : 20
             * property_id : 47
             * fromDate : 2018-09-19
             * toDate : 2018-09-28
             * quantity : 12
             * quantityUnite : mm
             * status : 1
             * crd : 2018-09-06 14:51:06
             * upd : 0000-00-00 00:00:00
             */

            private String raindataId;
            private String property_id;
            private String fromDate;
            private String toDate;
            private String quantity;
            private String quantityUnite;
            private String status;
            private String crd;
            private String upd;

            public RaindataListBean() {
            }


            RaindataListBean(Parcel in) {
                this.raindataId = in.readString();
                this.property_id = in.readString();
                this.fromDate = in.readString();
                this.toDate = in.readString();
                this.quantity = in.readString();
                this.quantityUnite = in.readString();
                this.status = in.readString();
                this.crd = in.readString();
                this.upd = in.readString();

            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.raindataId);
                dest.writeString(this.property_id);
                dest.writeString(this.fromDate);
                dest.writeString(this.toDate);
                dest.writeString(this.quantity);
                dest.writeString(this.quantityUnite);
                dest.writeString(this.status);
                dest.writeString(this.crd);
                dest.writeString(this.upd);
            }


            public String getRaindataId() {
                return raindataId;
            }

            public void setRaindataId(String raindataId) {
                this.raindataId = raindataId;
            }

            public String getProperty_id() {
                return property_id;
            }

            public void setProperty_id(String property_id) {
                this.property_id = property_id;
            }

            public String getFromDate() {
                return fromDate;
            }

            public void setFromDate(String fromDate) {
                this.fromDate = fromDate;
            }

            public String getToDate() {
                return toDate;
            }

            public void setToDate(String toDate) {
                this.toDate = toDate;
            }

            public String getQuantity() {
                return quantity;
            }

            public void setQuantity(String quantity) {
                this.quantity = quantity;
            }

            public String getQuantityUnite() {
                return quantityUnite;
            }

            public void setQuantityUnite(String quantityUnite) {
                this.quantityUnite = quantityUnite;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getCrd() {
                return crd;
            }

            public void setCrd(String crd) {
                this.crd = crd;
            }

            public String getUpd() {
                return upd;
            }

            public void setUpd(String upd) {
                this.upd = upd;
            }
        }

        public static class AnimalListBean implements Parcelable{
            public static final Creator<AnimalListBean> CREATOR = new Creator<AnimalListBean>() {
                @Override
                public AnimalListBean createFromParcel(Parcel in) {
                    return new AnimalListBean(in);
                }

                @Override
                public AnimalListBean[] newArray(int size) {
                    return new AnimalListBean[size];
                }
            };
            /**
             * animalId : 14
             * parcel_id : 10
             * lotId : #1233
             * tagNumber : #12344
             * bornOrBuyDate : 10/10/2017
             * mother : Bought from3
             * boughtFrom : indore
             * description : test
             * dismant :
             * markingOrTagging :
             * saleDate :
             * salePrice : 0
             * saleQuantity : 0
             * saleTotalPrice : 0
             * treatmentDescription :
             * status : 0
             * crd : 2018-07-09 19:17:28
             * upd : 2018-06-29 12:59:31
             */

            private String animalId;
            private String parcel_id;
            private String lotId;
            private String tagNumber;
            private String bornOrBuyDate;
            private String mother;
            private String boughtFrom;
            private String description;
            private String dismant;
            private String markingOrTagging;
            private String saleDate;
            private String salePrice;
            private String saleQuantity;
            private String saleTotalPrice;
            private String costFrom;
            private String costTo;
            private String costTotal;
            private String status;
            private String recordKey;
            private String crd;
            private String upd;

            public AnimalListBean(){}

            AnimalListBean(Parcel in) {
                animalId = in.readString();
                parcel_id = in.readString();
                lotId = in.readString();
                tagNumber = in.readString();
                bornOrBuyDate = in.readString();
                mother = in.readString();
                boughtFrom = in.readString();
                description = in.readString();
                dismant = in.readString();
                markingOrTagging = in.readString();
                saleDate = in.readString();
                salePrice = in.readString();
                saleQuantity = in.readString();
                saleTotalPrice = in.readString();
                costFrom = in.readString();
                costTo = in.readString();
                costTotal = in.readString();
                status = in.readString();
                recordKey = in.readString();
                crd = in.readString();
                upd = in.readString();
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(animalId);
                dest.writeString(parcel_id);
                dest.writeString(lotId);
                dest.writeString(tagNumber);
                dest.writeString(bornOrBuyDate);
                dest.writeString(mother);
                dest.writeString(boughtFrom);
                dest.writeString(description);
                dest.writeString(dismant);
                dest.writeString(markingOrTagging);
                dest.writeString(saleDate);
                dest.writeString(salePrice);
                dest.writeString(saleQuantity);
                dest.writeString(saleTotalPrice);
                dest.writeString(costFrom);
                dest.writeString(costTo);
                dest.writeString(costTotal);
                dest.writeString(status);
                dest.writeString(recordKey);
                dest.writeString(crd);
                dest.writeString(upd);
            }

            @Override
            public int describeContents() {
                return 0;
            }

            public String getAnimalId() {
                return animalId;
            }

            public void setAnimalId(String animalId) {
                this.animalId = animalId;
            }

            public String getParcel_id() {
                return parcel_id;
            }

            public void setParcel_id(String parcel_id) {
                this.parcel_id = parcel_id;
            }

            public String getLotId() {
                return lotId;
            }

            public void setLotId(String lotId) {
                this.lotId = lotId;
            }

            public String getTagNumber() {
                return tagNumber;
            }

            public void setTagNumber(String tagNumber) {
                this.tagNumber = tagNumber;
            }

            public String getBornOrBuyDate() {
                return bornOrBuyDate;
            }

            public void setBornOrBuyDate(String bornOrBuyDate) {
                this.bornOrBuyDate = bornOrBuyDate;
            }

            public String getMother() {
                return mother;
            }

            public void setMother(String mother) {
                this.mother = mother;
            }

            public String getBoughtFrom() {
                return boughtFrom;
            }

            public void setBoughtFrom(String boughtFrom) {
                this.boughtFrom = boughtFrom;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getDismant() {
                return dismant;
            }

            public void setDismant(String dismant) {
                this.dismant = dismant;
            }

            public String getMarkingOrTagging() {
                return markingOrTagging;
            }

            public void setMarkingOrTagging(String markingOrTagging) {
                this.markingOrTagging = markingOrTagging;
            }

            public String getSaleDate() {
                return saleDate;
            }

            public void setSaleDate(String saleDate) {
                this.saleDate = saleDate;
            }

            public String getSalePrice() {
                return salePrice;
            }

            public void setSalePrice(String salePrice) {
                this.salePrice = salePrice;
            }

            public String getSaleQuantity() {
                return saleQuantity;
            }

            public void setSaleQuantity(String saleQuantity) {
                this.saleQuantity = saleQuantity;
            }

            public String getSaleTotalPrice() {
                return saleTotalPrice;
            }

            public void setSaleTotalPrice(String saleTotalPrice) {
                this.saleTotalPrice = saleTotalPrice;
            }

            public String getCostFrom() {
                return costFrom;
            }

            public void setCostFrom(String costFrom) {
                this.costFrom = costFrom;
            }

            public String getCostTo() {
                return costTo;
            }

            public void setCostTo(String costTo) {
                this.costTo = costTo;
            }

            public String getCostTotal() {
                return costTotal;
            }

            public void setCostTotal(String costTotal) {
                this.costTotal = costTotal;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getRecordKey() {
                return recordKey;
            }

            public void setRecordKey(String recordKey) {
                this.recordKey = recordKey;
            }

            public String getCrd() {
                return crd;
            }

            public void setCrd(String crd) {
                this.crd = crd;
            }

            public String getUpd() {
                return upd;
            }

            public void setUpd(String upd) {
                this.upd = upd;
            }
        }
    }
}
