package com.agrinvestapp.data.model.other;

/**
 * Created by hemant
 * Date: 11/12/18.
 */

public final class CommonIdBean {
    public String id;
    public String recordKey;
}
