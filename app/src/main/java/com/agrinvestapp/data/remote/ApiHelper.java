package com.agrinvestapp.data.remote;


import com.agrinvestapp.volley.VolleyMultipartRequest;
import com.agrinvestapp.volley.VolleyRequest;

import java.util.HashMap;


/**
 * Created by hemant
 * Date: 10/4/18.
 */

public interface ApiHelper {

    //Master Sync Get
    VolleyRequest doServerGetMasterSyncApiCall(HashMap<String, String> mHeaderMap);

    //Auth API's
    VolleyRequest doServerLoginApiCall(HashMap<String, String> mParameterMap);

    VolleyRequest doServerRegistrationApiCall(HashMap<String, String> mParameterMap);

    VolleyRequest doServerForgotPwdApiCall(HashMap<String, String> mParameterMap);

    //Property module API's
    VolleyRequest doServerAddPropertyApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerPropertyInfoApiCall(HashMap<String, String> mHeaderMap);

    VolleyRequest doServerPropertyDetailApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerEditPropertyDetailApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerUpdatePropertyDetailApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerDeletePropertyInfoApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerRainRecordApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerDeleteRainRecordApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerAddParcelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerUpdateParcelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerDeleteParcelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerSyncParcelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    //Animal module API's
    VolleyRequest doServerAddAnimalApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerAnimalInfoApiCall(HashMap<String, String> mHeaderMap);

    VolleyRequest doServerUpdateAnimalDetailApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerUpdateAnimalHistoryApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerDeleteAnimalInfoApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerGetParcelListApiCall(HashMap<String, String> mHeaderMap);

    VolleyRequest doServerGetAnimalListApiCall(HashMap<String, String> mHeaderMap);

    VolleyRequest doServerTreatmentInfoListApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerTreatmentAddEditDelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerTreatmentDetailAddEditDelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerInseminationAddDelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerPregnancyTestAddDelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerParcelMovementAddDelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerCalvingAddDelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerWeightAddDelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerSyncAnimalApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerSyncTreatmentApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerSyncInseminationApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerSyncPregnancyTestApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerSyncMToParcelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerSyncCalvingApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerSyncWeightApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    //Crop module API's
    VolleyRequest doServerAddCropApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerCropInfoApiCall(HashMap<String, String> mHeaderMap);

    VolleyRequest doServerUpdateCropApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerUpdateCropHistoryApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerDeleteCropInfoApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerCropTreatmentInfoApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerCropTreatmentAddEditDelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerCropTreatmentDetailAddEditDelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerSyncCropApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerSyncCropTreatmentApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);


    //Product module API's
    VolleyRequest doServerProductInfoListApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerAddProductApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap, HashMap<String, VolleyMultipartRequest.DataPart> mDataParameterMap);

    VolleyRequest doServerUpdateProductApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap, HashMap<String, VolleyMultipartRequest.DataPart> mDataParameterMap);

    VolleyRequest doServerDeleteProductApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerSyncProductApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    //Expense module API's
    VolleyRequest doServerExpenseInfoApiCall(Boolean isAnimalExpense, HashMap<String, String> mHeaderMap);

    VolleyRequest doServerAddExpenseApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerUpdateExpenseApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerDeleteExpenseApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerSyncExpenseApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    //Profile module API's
    VolleyRequest doServerProfileUpdateApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap, HashMap<String, VolleyMultipartRequest.DataPart> mDataParameterMap);

    VolleyRequest doServerChangePwdApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    VolleyRequest doServerLogoutApiCall(HashMap<String, String> mHeaderMap);

    //Setting module API's
    VolleyRequest doServerSettingContentApiCall(HashMap<String, String> mHeaderMap);

    VolleyRequest doServerChangeLangApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap);

    //Payment module API's
    VolleyRequest doServerGetPlanApiCall(HashMap<String, String> mHeaderMap);

    VolleyRequest doServerCheckPlanApiCall(HashMap<String, String> mHeaderMap);

    String getPaymentUrl(String authToken);

    VolleyRequest doServerCancelSubscriptionApiCall(HashMap<String, String> mHeaderMap);
}
