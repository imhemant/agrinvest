package com.agrinvestapp.data.remote;

import com.agrinvestapp.BuildConfig;

/**
 * Created by hemant.
 * Date: 30/8/18
 * Time: 2:30 PM
 */

final class Webservices {

    //Auth module api's
    static final String WEB_LOGIN = BuildConfig.BASE_URL + "service/userLogin";
    static final String WEB_SIGNUP = BuildConfig.BASE_URL + "service/userRegistration";
    static final String WEB_FORGOT_PWD = BuildConfig.BASE_URL + "service/forgotPassword";

    //Master get api
    static final String WEB_GET_SERVER_SYNC = BuildConfig.BASE_URL + "property/getMasterSynchronization";

    //Property module api's
    static final String WEB_ADD_PROPERTY = BuildConfig.BASE_URL + "property/addProperty";
    static final String WEB_PROPERTY_INFO = BuildConfig.BASE_URL + "property/getPropertyInfo"; //GET
    static final String WEB_PROPERTY_DETAIL = BuildConfig.BASE_URL + "property/getPropertyDetail";
    static final String WEB_EDIT_PROPERTY_DETAIL = BuildConfig.BASE_URL + "property/getEditPropertyDetail";
    static final String WEB_UPDATE_PROPERTY_DETAIL = BuildConfig.BASE_URL + "property/updateProperty";
    static final String WEB_DELETE_PROPERTY_INFO = BuildConfig.BASE_URL + "property/deleteProperty";
    static final String WEB_RAIN_RECORD_LIST = BuildConfig.BASE_URL + "property/propertyRainRecordList";
    static final String WEB_DELETE_RAIN_RECORD = BuildConfig.BASE_URL + "property/deletePropertyRainRecord";
    static final String WEB_ADD_PARCEL = BuildConfig.BASE_URL + "parcel/addParcel";
    static final String WEB_UPDATE_PARCEL = BuildConfig.BASE_URL + "parcel/updateParcel";
    static final String WEB_DELETE_PARCEL = BuildConfig.BASE_URL + "parcel/deleteParcel";

    static final String WEB_SYNC_LOCAL_DB_PARCEL = BuildConfig.BASE_URL + "parcel/manageParcelData";

    //Animal module api's
    static final String WEB_ADD_ANIMAL = BuildConfig.BASE_URL + "animal/addAnimal";
    static final String WEB_ANIMAL_INFO = BuildConfig.BASE_URL + "animal/getAnimalList"; //GET or //?limit=&start
    static final String WEB_UPDATE_ANIMAL_DETAIL = BuildConfig.BASE_URL + "animal/updateAnimal";
    static final String WEB_UPDATE_ANIMAL_HISTORY = BuildConfig.BASE_URL + "animal/updateAnimalHistory";
    static final String WEB_DELETE_ANIMAL_INFO = BuildConfig.BASE_URL + "animal/deleteAnimal";
    static final String WEB_ANIMAL_LIST = BuildConfig.BASE_URL + "animal/animalMotherList"; //GET
    static final String WEB_PARCEL_LIST = BuildConfig.BASE_URL + "parcel/getParcelListForAddAnimal"; //GET
    static final String WEB_TREATMENT_ADD_EDIT_DEL = BuildConfig.BASE_URL + "animal/treatmentAddEditDelete";
    static final String WEB_ANIMAL_OTHER_INFO = BuildConfig.BASE_URL + "animal/getAnimalOtherDetailList";
    static final String WEB_TREATMENT_DETAIL_ADD_EDIT_DEL = BuildConfig.BASE_URL + "animal/AnimalTreatmentDetailAddEditDelete";
    static final String WEB_INSEMINATION_ADD_EDIT_DEL = BuildConfig.BASE_URL + "animal/addEditDeleteInsemination";
    static final String WEB_PREGNANCY_TEST_ADD_EDIT_DEL = BuildConfig.BASE_URL + "animal/addEditDeletePregnancy";
    static final String WEB_MOVE_PARCEL_ADD_EDIT_DEL = BuildConfig.BASE_URL + "animal/addEditAnimalParcelMovement";
    static final String WEB_CALVING_ADD_EDIT_DEL = BuildConfig.BASE_URL + "animal/addEditDeleteCalving";
    static final String WEB_WEIGHT_ADD_EDIT_DEL = BuildConfig.BASE_URL + "animal/addEditDeleteWeight";

    static final String WEB_SYNC_LOCAL_DB_ANIMAL = BuildConfig.BASE_URL + "animal/manageAnimalData";
    static final String WEB_SYNC_LOCAL_DB_TREATMENT = BuildConfig.BASE_URL + "animal/manageTreatmentData";
    static final String WEB_SYNC_LOCAL_DB_INSEMINATION = BuildConfig.BASE_URL + "animal/manageInsemination";
    static final String WEB_SYNC_LOCAL_DB_PREGNANCY_TEST = BuildConfig.BASE_URL + "animal/managePregnancyTest";
    static final String WEB_SYNC_LOCAL_DB_MTOPARCEL = BuildConfig.BASE_URL + "animal/manageParcelMovement";
    static final String WEB_SYNC_LOCAL_DB_CALVING = BuildConfig.BASE_URL + "animal/manageCalving";
    static final String WEB_SYNC_LOCAL_DB_WEIGHT = BuildConfig.BASE_URL + "animal/manageWeight";

    //Crop module api's
    static final String WEB_ADD_CROP = BuildConfig.BASE_URL + "crop/addCrop";
    static final String WEB_CROP_INFO = BuildConfig.BASE_URL + "crop/getCropList"; //GET
    static final String WEB_UPDATE_CROP = BuildConfig.BASE_URL + "crop/updateCrop";
    static final String WEB_UPDATE_CROP_HISTORY = BuildConfig.BASE_URL + "crop/updateCropHistory";
    static final String WEB_DELETE_CROP_INFO = BuildConfig.BASE_URL + "crop/deleteCrop";
    static final String WEB_CROP_TREATMENT_INFO = BuildConfig.BASE_URL + "crop/getCropTreatmentList";
    static final String WEB_CROP_TREATMENT_ADD_EDIT_DEL = BuildConfig.BASE_URL + "crop/cropTreatmentAddEditDelete";
    static final String WEB_CROP_TREATMENT_DETAIL_ADD_EDIT_DEL = BuildConfig.BASE_URL + "crop/CropTreatmentDetailAddEditDelete";

    static final String WEB_SYNC_LOCAL_DB_CROP = BuildConfig.BASE_URL + "crop/manageCropHistory";
    static final String WEB_SYNC_LOCAL_DB_CROP_TREATMENT = BuildConfig.BASE_URL + "crop/manageCropTreatment";

    //Product module api's
    static final String WEB_PRODUCT_INFO_LIST = BuildConfig.BASE_URL + "product/getProductList";
    static final String WEB_ADD_PRODUCT = BuildConfig.BASE_URL + "product/addProduct";
    static final String WEB_UPDATE_PRODUCT = BuildConfig.BASE_URL + "product/updateProduct";
    static final String WEB_DELETE_PRODUCT = BuildConfig.BASE_URL + "product/deleteProduct";

    static final String WEB_SYNC_LOCAL_DB_PRODUCT = BuildConfig.BASE_URL + "product/manageProduct";

    //Expense module api's
    static final String WEB_EXPENSE_INFO = BuildConfig.BASE_URL + "expense/getExpenseList?expenseFor="; //GET //crop or animal
    static final String WEB_ADD_EXPENSE = BuildConfig.BASE_URL + "expense/addExpense";
    static final String WEB_UPDATE_EXPENSE = BuildConfig.BASE_URL + "expense/updateExpenseDetails";
    static final String WEB_DELETE_EXPENSE = BuildConfig.BASE_URL + "expense/deleteExpence";

    static final String WEB_SYNC_LOCAL_DB_EXPENSE = BuildConfig.BASE_URL + "expense/manageExpenses";

    //Profile module api's
    static final String WEB_UPDATE_PROFILE = BuildConfig.BASE_URL + "user/updateUserProfile";
    static final String WEB_CHANGE_PWD = BuildConfig.BASE_URL + "user/changePassword";
    static final String WEB_LOGOUT = BuildConfig.BASE_URL + "user/userLogout";  //GET

    //Setting module api's
    static final String WEB_SETTING_CONTENT = BuildConfig.BASE_URL + "service/getContent";  //GET
    static final String WEB_CHANGE_LANGUAGE = BuildConfig.BASE_URL + "user/updateUserlanguage";

    //Payment module api's
    static final String WEB_GET_PLAN = BuildConfig.BASE_URL + "Subscription/getPlanDetail";  //GET
    static final String WEB_CHECK_PLAN = BuildConfig.BASE_URL + "Subscription/checkPlanDetail";  //GET
    static final String WEB_CANCEL_SUBSCRIPTION = BuildConfig.BASE_URL + "Subscription/stopRecurrence";

    private Webservices() {
        // This class is not publicly instantiable
    }
}
