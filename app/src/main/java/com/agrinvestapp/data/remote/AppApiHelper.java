package com.agrinvestapp.data.remote;

import com.agrinvestapp.utils.AppConstants;
import com.agrinvestapp.volley.VolleyMultipartRequest;
import com.agrinvestapp.volley.VolleyNetwork;
import com.agrinvestapp.volley.VolleyRequest;

import java.util.HashMap;


/**
 * Created by hemant
 * Date: 10/4/18.
 */

public final class AppApiHelper implements ApiHelper {

    private final static String TAG = AppApiHelper.class.getSimpleName();
    private final static int mRetryTime = 30000;

    private static AppApiHelper instance;

    public synchronized static AppApiHelper getAppApiInstance() {
        if (instance == null) {
            instance = new AppApiHelper();
        }
        return instance;
    }

    @Override
    public VolleyRequest doServerGetMasterSyncApiCall(HashMap<String, String> mHeaderMap) {
        return VolleyNetwork.get(Webservices.WEB_GET_SERVER_SYNC)
                .addHeaders(mHeaderMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    public VolleyRequest doServerLoginApiCall(HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_LOGIN)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerRegistrationApiCall(HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_SIGNUP)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerForgotPwdApiCall(HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_FORGOT_PWD)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerAddPropertyApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_ADD_PROPERTY)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerPropertyInfoApiCall(HashMap<String, String> mHeaderMap) {
        return VolleyNetwork.get(Webservices.WEB_PROPERTY_INFO)
                .addHeaders(mHeaderMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerPropertyDetailApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_PROPERTY_DETAIL)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerEditPropertyDetailApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_EDIT_PROPERTY_DETAIL)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerUpdatePropertyDetailApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_UPDATE_PROPERTY_DETAIL)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerDeletePropertyInfoApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_DELETE_PROPERTY_INFO)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerRainRecordApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_RAIN_RECORD_LIST)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerDeleteRainRecordApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_DELETE_RAIN_RECORD)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerAddParcelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_ADD_PARCEL)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerUpdateParcelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_UPDATE_PARCEL)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerDeleteParcelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_DELETE_PARCEL)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerSyncParcelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_SYNC_LOCAL_DB_PARCEL)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerAddAnimalApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_ADD_ANIMAL)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerAnimalInfoApiCall(HashMap<String, String> mHeaderMap) {
        return VolleyNetwork.get(Webservices.WEB_ANIMAL_INFO)
                .addHeaders(mHeaderMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerUpdateAnimalDetailApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_UPDATE_ANIMAL_DETAIL)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerUpdateAnimalHistoryApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_UPDATE_ANIMAL_HISTORY)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerDeleteAnimalInfoApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_DELETE_ANIMAL_INFO)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerGetParcelListApiCall(HashMap<String, String> mHeaderMap) {
        return VolleyNetwork.get(Webservices.WEB_PARCEL_LIST)
                .addHeaders(mHeaderMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerGetAnimalListApiCall(HashMap<String, String> mHeaderMap) {
        return VolleyNetwork.get(Webservices.WEB_ANIMAL_LIST)
                .addHeaders(mHeaderMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerTreatmentInfoListApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_ANIMAL_OTHER_INFO)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerTreatmentAddEditDelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_TREATMENT_ADD_EDIT_DEL)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerTreatmentDetailAddEditDelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_TREATMENT_DETAIL_ADD_EDIT_DEL)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerInseminationAddDelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_INSEMINATION_ADD_EDIT_DEL)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerPregnancyTestAddDelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_PREGNANCY_TEST_ADD_EDIT_DEL)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerParcelMovementAddDelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_MOVE_PARCEL_ADD_EDIT_DEL)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerCalvingAddDelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_CALVING_ADD_EDIT_DEL)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerWeightAddDelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_WEIGHT_ADD_EDIT_DEL)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerSyncAnimalApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_SYNC_LOCAL_DB_ANIMAL)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerSyncTreatmentApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_SYNC_LOCAL_DB_TREATMENT)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerSyncInseminationApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_SYNC_LOCAL_DB_INSEMINATION)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerSyncPregnancyTestApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_SYNC_LOCAL_DB_PREGNANCY_TEST)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerSyncMToParcelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_SYNC_LOCAL_DB_MTOPARCEL)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerSyncCalvingApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_SYNC_LOCAL_DB_CALVING)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerSyncWeightApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_SYNC_LOCAL_DB_WEIGHT)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerAddCropApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_ADD_CROP)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerCropInfoApiCall(HashMap<String, String> mHeaderMap) {
        return VolleyNetwork.get(Webservices.WEB_CROP_INFO)
                .addHeaders(mHeaderMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerUpdateCropApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
         return VolleyNetwork.post(Webservices.WEB_UPDATE_CROP)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerUpdateCropHistoryApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
         return VolleyNetwork.post(Webservices.WEB_UPDATE_CROP_HISTORY)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerDeleteCropInfoApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
         return VolleyNetwork.post(Webservices.WEB_DELETE_CROP_INFO)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerCropTreatmentInfoApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_CROP_TREATMENT_INFO)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerCropTreatmentAddEditDelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_CROP_TREATMENT_ADD_EDIT_DEL)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerCropTreatmentDetailAddEditDelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_CROP_TREATMENT_DETAIL_ADD_EDIT_DEL)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerSyncCropApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_SYNC_LOCAL_DB_CROP)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerSyncCropTreatmentApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_SYNC_LOCAL_DB_CROP_TREATMENT)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerProductInfoListApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_PRODUCT_INFO_LIST)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerAddProductApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap, HashMap<String, VolleyMultipartRequest.DataPart> mDataParameterMap) {
        return VolleyNetwork.postMultipart(Webservices.WEB_ADD_PRODUCT)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .addDataParameter(mDataParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerUpdateProductApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap, HashMap<String, VolleyMultipartRequest.DataPart> mDataParameterMap) {
        return VolleyNetwork.postMultipart(Webservices.WEB_UPDATE_PRODUCT)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .addDataParameter(mDataParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerDeleteProductApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_DELETE_PRODUCT)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerSyncProductApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_SYNC_LOCAL_DB_PRODUCT)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerExpenseInfoApiCall(Boolean isAnimalExpense, HashMap<String, String> mHeaderMap) {
        return VolleyNetwork.get(isAnimalExpense?Webservices.WEB_EXPENSE_INFO+"animal":Webservices.WEB_EXPENSE_INFO+"crop")
                .addHeaders(mHeaderMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerAddExpenseApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.postMultipart(Webservices.WEB_ADD_EXPENSE)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerUpdateExpenseApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
          return VolleyNetwork.post(Webservices.WEB_UPDATE_EXPENSE)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerDeleteExpenseApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
          return VolleyNetwork.post(Webservices.WEB_DELETE_EXPENSE)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerSyncExpenseApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_SYNC_LOCAL_DB_EXPENSE)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerProfileUpdateApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap, HashMap<String, VolleyMultipartRequest.DataPart> mDataParameterMap) {
        return VolleyNetwork.postMultipart(Webservices.WEB_UPDATE_PROFILE)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .addDataParameter(mDataParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerChangePwdApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_CHANGE_PWD)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerLogoutApiCall(HashMap<String, String> mHeaderMap) {
        return VolleyNetwork.get(Webservices.WEB_LOGOUT)
                .addHeaders(mHeaderMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerSettingContentApiCall(HashMap<String, String> mHeaderMap) {
        return VolleyNetwork.get(Webservices.WEB_SETTING_CONTENT)
                .addHeaders(mHeaderMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerChangeLangApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return VolleyNetwork.post(Webservices.WEB_CHANGE_LANGUAGE)
                .addHeaders(mHeaderMap)
                .addParameter(mParameterMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerGetPlanApiCall(HashMap<String, String> mHeaderMap) {
        return VolleyNetwork.get(Webservices.WEB_GET_PLAN)
                .addHeaders(mHeaderMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public VolleyRequest doServerCheckPlanApiCall(HashMap<String, String> mHeaderMap) {
        return VolleyNetwork.get(Webservices.WEB_CHECK_PLAN)
                .addHeaders(mHeaderMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

    @Override
    public String getPaymentUrl(String authToken) {
        return AppConstants.WEB_PAYMENT.concat("/").concat(authToken);
    }

    @Override
    public VolleyRequest doServerCancelSubscriptionApiCall(HashMap<String, String> mHeaderMap) {
        return VolleyNetwork.get(Webservices.WEB_CANCEL_SUBSCRIPTION)
                .addHeaders(mHeaderMap)
                .setTag(TAG)
                .addRetryTime(mRetryTime)
                .build();
    }

}
