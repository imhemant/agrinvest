package com.agrinvestapp.data;

import android.app.Activity;
import android.content.Context;

import com.agrinvestapp.data.local.db.AppDbHelper;
import com.agrinvestapp.data.local.db.DbHelper;
import com.agrinvestapp.data.local.prefs.AppPreferencesHelper;
import com.agrinvestapp.data.local.prefs.PreferencesHelper;
import com.agrinvestapp.data.model.api.AnimalInfoResponse;
import com.agrinvestapp.data.model.api.CalvingInfoResponse;
import com.agrinvestapp.data.model.api.CropInfoResponse;
import com.agrinvestapp.data.model.api.CropTreatmentInfoResponse;
import com.agrinvestapp.data.model.api.ExpenseInfoResponse;
import com.agrinvestapp.data.model.api.InseminationInfoResponse;
import com.agrinvestapp.data.model.api.MotherResponse;
import com.agrinvestapp.data.model.api.ParcelMovementInfoResponse;
import com.agrinvestapp.data.model.api.PregnancyTestInfoResponse;
import com.agrinvestapp.data.model.api.ProductInfoResponse;
import com.agrinvestapp.data.model.api.PropertyDetailResponse;
import com.agrinvestapp.data.model.api.PropertyInfoResponse;
import com.agrinvestapp.data.model.api.RainBeanResponse;
import com.agrinvestapp.data.model.api.TreatmentInfoResponse;
import com.agrinvestapp.data.model.api.WeightInfoResponse;
import com.agrinvestapp.data.model.db.Animal;
import com.agrinvestapp.data.model.db.Calving;
import com.agrinvestapp.data.model.db.Crop;
import com.agrinvestapp.data.model.db.CropTreatment;
import com.agrinvestapp.data.model.db.CropTreatmentMapping;
import com.agrinvestapp.data.model.db.Expense;
import com.agrinvestapp.data.model.db.ExpenseMapping;
import com.agrinvestapp.data.model.db.ExpenseProductMapping;
import com.agrinvestapp.data.model.db.Insemination;
import com.agrinvestapp.data.model.db.Parcel;
import com.agrinvestapp.data.model.db.ParcelMovement;
import com.agrinvestapp.data.model.db.PregnancyTest;
import com.agrinvestapp.data.model.db.Product;
import com.agrinvestapp.data.model.db.Property;
import com.agrinvestapp.data.model.db.RainData;
import com.agrinvestapp.data.model.db.Treatment;
import com.agrinvestapp.data.model.db.TreatmentMapping;
import com.agrinvestapp.data.model.db.UserInfoBean;
import com.agrinvestapp.data.model.db.Weight;
import com.agrinvestapp.data.model.other.AnimalSyncResult;
import com.agrinvestapp.data.model.other.CalvingSyncResult;
import com.agrinvestapp.data.model.other.CommonIdBean;
import com.agrinvestapp.data.model.other.CropSyncResult;
import com.agrinvestapp.data.model.other.CropTreatmentSyncResult;
import com.agrinvestapp.data.model.other.ExpenseProductBean;
import com.agrinvestapp.data.model.other.ExpenseSyncResult;
import com.agrinvestapp.data.model.other.IncomeBean;
import com.agrinvestapp.data.model.other.IncomeOrExpenseApplyOnBean;
import com.agrinvestapp.data.model.other.InseminationSyncResult;
import com.agrinvestapp.data.model.other.MoveToParcelSyncResult;
import com.agrinvestapp.data.model.other.PregnancyTestSyncResult;
import com.agrinvestapp.data.model.other.ProductSyncResult;
import com.agrinvestapp.data.model.other.TreatmentSyncResult;
import com.agrinvestapp.data.model.other.WeightSyncResult;
import com.agrinvestapp.data.remote.ApiHelper;
import com.agrinvestapp.data.remote.AppApiHelper;
import com.agrinvestapp.volley.VolleyMultipartRequest;
import com.agrinvestapp.volley.VolleyRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Date;
import java.util.HashMap;
import java.util.List;


/**
 * Created by hemant
 * Date: 10/4/18.
 */


public final class AppDataManager implements DataManager {

    private static AppDataManager instance;
    public final Gson mGson;
    private final ApiHelper mApiHelper;
    private final DbHelper mDbHelper;
    private final PreferencesHelper mPreferencesHelper;

    private AppDataManager(Context context) {
        mDbHelper = AppDbHelper.getDbInstance(context);
        mPreferencesHelper = new AppPreferencesHelper(context);
        mApiHelper = AppApiHelper.getAppApiInstance();
        mGson = new GsonBuilder().create();
    }

    public synchronized static AppDataManager getInstance(Context context) {
        if (instance == null) {
            instance = new AppDataManager(context);
        }
        return instance;
    }

    /* App Api's Info */

    @Override
    public VolleyRequest doServerGetMasterSyncApiCall(HashMap<String, String> mHeaderMap) {
        return mApiHelper.doServerGetMasterSyncApiCall(mHeaderMap);
    }

    @Override
    public VolleyRequest doServerLoginApiCall(HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerLoginApiCall(mParameterMap);
    }

    @Override
    public VolleyRequest doServerRegistrationApiCall(HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerRegistrationApiCall(mParameterMap);
    }

    @Override
    public VolleyRequest doServerForgotPwdApiCall(HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerForgotPwdApiCall(mParameterMap);
    }

    @Override
    public VolleyRequest doServerAddPropertyApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerAddPropertyApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerPropertyInfoApiCall(HashMap<String, String> mHeaderMap) {
        return mApiHelper.doServerPropertyInfoApiCall(mHeaderMap);
    }

    @Override
    public VolleyRequest doServerPropertyDetailApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerPropertyDetailApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerEditPropertyDetailApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerEditPropertyDetailApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerUpdatePropertyDetailApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerUpdatePropertyDetailApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerDeletePropertyInfoApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerDeletePropertyInfoApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerRainRecordApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerRainRecordApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerDeleteRainRecordApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerDeleteRainRecordApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerAddParcelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerAddParcelApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerUpdateParcelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerUpdateParcelApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerDeleteParcelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerDeleteParcelApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerSyncParcelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerSyncParcelApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerAddAnimalApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerAddAnimalApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerAnimalInfoApiCall(HashMap<String, String> mHeaderMap) {
        return mApiHelper.doServerAnimalInfoApiCall(mHeaderMap);
    }

    @Override
    public VolleyRequest doServerUpdateAnimalDetailApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerUpdateAnimalDetailApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerUpdateAnimalHistoryApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerUpdateAnimalHistoryApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerDeleteAnimalInfoApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerDeleteAnimalInfoApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerGetParcelListApiCall(HashMap<String, String> mHeaderMap) {
        return mApiHelper.doServerGetParcelListApiCall(mHeaderMap);
    }

    @Override
    public VolleyRequest doServerGetAnimalListApiCall(HashMap<String, String> mHeaderMap) {
        return mApiHelper.doServerGetAnimalListApiCall(mHeaderMap);
    }

    @Override
    public VolleyRequest doServerTreatmentInfoListApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerTreatmentInfoListApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerTreatmentAddEditDelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerTreatmentAddEditDelApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerTreatmentDetailAddEditDelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerTreatmentDetailAddEditDelApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerInseminationAddDelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerInseminationAddDelApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerPregnancyTestAddDelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerPregnancyTestAddDelApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerParcelMovementAddDelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerParcelMovementAddDelApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerCalvingAddDelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerCalvingAddDelApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerWeightAddDelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerWeightAddDelApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerSyncAnimalApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerSyncAnimalApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerSyncTreatmentApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerSyncTreatmentApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerSyncInseminationApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerSyncInseminationApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerSyncPregnancyTestApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerSyncPregnancyTestApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerSyncMToParcelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerSyncMToParcelApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerSyncCalvingApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerSyncCalvingApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerSyncWeightApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerSyncWeightApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerAddCropApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerAddCropApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerCropInfoApiCall(HashMap<String, String> mHeaderMap) {
        return mApiHelper.doServerCropInfoApiCall(mHeaderMap);
    }

    @Override
    public VolleyRequest doServerUpdateCropApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerUpdateCropApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerUpdateCropHistoryApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerUpdateCropHistoryApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerDeleteCropInfoApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerDeleteCropInfoApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerCropTreatmentInfoApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerCropTreatmentInfoApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerCropTreatmentAddEditDelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerCropTreatmentAddEditDelApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerCropTreatmentDetailAddEditDelApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerCropTreatmentDetailAddEditDelApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerSyncCropApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerSyncCropApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerSyncCropTreatmentApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerSyncCropTreatmentApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerProductInfoListApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerProductInfoListApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerAddProductApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap, HashMap<String, VolleyMultipartRequest.DataPart> mDataParameterMap) {
        return mApiHelper.doServerAddProductApiCall(mHeaderMap, mParameterMap, mDataParameterMap);
    }

    @Override
    public VolleyRequest doServerUpdateProductApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap, HashMap<String, VolleyMultipartRequest.DataPart> mDataParameterMap) {
        return mApiHelper.doServerUpdateProductApiCall(mHeaderMap, mParameterMap, mDataParameterMap);
    }

    @Override
    public VolleyRequest doServerDeleteProductApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerDeleteProductApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerSyncProductApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerSyncProductApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerExpenseInfoApiCall(Boolean isAnimalExpense, HashMap<String, String> mHeaderMap) {
        return mApiHelper.doServerExpenseInfoApiCall(isAnimalExpense, mHeaderMap);
    }

    @Override
    public VolleyRequest doServerAddExpenseApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerAddExpenseApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerUpdateExpenseApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerUpdateExpenseApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerDeleteExpenseApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerDeleteExpenseApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerSyncExpenseApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerSyncExpenseApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerProfileUpdateApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap, HashMap<String, VolleyMultipartRequest.DataPart> mDataParameterMap) {
        return mApiHelper.doServerProfileUpdateApiCall(mHeaderMap, mParameterMap, mDataParameterMap);
    }

    @Override
    public VolleyRequest doServerChangePwdApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerChangePwdApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerLogoutApiCall(HashMap<String, String> mHeaderMap) {
        return mApiHelper.doServerLogoutApiCall(mHeaderMap);
    }

    @Override
    public VolleyRequest doServerSettingContentApiCall(HashMap<String, String> mHeaderMap) {
        return mApiHelper.doServerSettingContentApiCall(mHeaderMap);
    }

    @Override
    public VolleyRequest doServerChangeLangApiCall(HashMap<String, String> mHeaderMap, HashMap<String, String> mParameterMap) {
        return mApiHelper.doServerChangeLangApiCall(mHeaderMap, mParameterMap);
    }

    @Override
    public VolleyRequest doServerGetPlanApiCall(HashMap<String, String> mHeaderMap) {
        return mApiHelper.doServerGetPlanApiCall(mHeaderMap);
    }

    @Override
    public VolleyRequest doServerCheckPlanApiCall(HashMap<String, String> mHeaderMap) {
        return mApiHelper.doServerCheckPlanApiCall(mHeaderMap);
    }

    @Override
    public String getPaymentUrl(String authToken) {
        return mApiHelper.getPaymentUrl(authToken);
    }

    @Override
    public VolleyRequest doServerCancelSubscriptionApiCall(HashMap<String, String> mHeaderMap) {
        return mApiHelper.doServerCancelSubscriptionApiCall(mHeaderMap);
    }

    /* Local DB Info */

    @Override
    public List<UserInfoBean> getAllUsers() {
        return mDbHelper.getAllUsers();
    }

    @Override
    public String getExistingUserIdFromDB() {
        return mDbHelper.getExistingUserIdFromDB();
    }

    @Override
    public Boolean insertUser(UserInfoBean user) {
        return mDbHelper.insertUser(user);
    }

    @Override
    public Boolean deleteUser(UserInfoBean user) {
        return mDbHelper.deleteUser(user);
    }

    @Override
    public void deleteAllUser() {
        mDbHelper.deleteAllUser();
    }

    /*Property data start here*/
    @Override
    public void insertProperty(Property property) {
        mDbHelper.insertProperty(property);
    }

    @Override
    public void insertAllProperty(List<Property> properties) {
        mDbHelper.insertAllProperty(properties);
    }

    @Override
    public void updateProperty(Property property) {
        mDbHelper.updateProperty(property);
    }

    @Override
    public void deleteProperty(String propertyId) {
        mDbHelper.deleteProperty(propertyId);
    }

    @Override
    public String getPropertyCount() {
        return mDbHelper.getPropertyCount();
    }

    @Override
    public void deleteAllProperty() {
        mDbHelper.deleteAllProperty();
    }

    @Override
    public void insertRainData(RainData rainData) {
        mDbHelper.insertRainData(rainData);
    }

    @Override
    public void insertAllRainData(List<RainData> rainData) {
        mDbHelper.insertAllRainData(rainData);
    }

    @Override
    public void deleteRainData(String rainDataId) {
        mDbHelper.deleteRainData(rainDataId);
    }

    @Override
    public void deleteRainData(RainData rainData) {
        mDbHelper.deleteRainData(rainData);
    }

    @Override
    public void deleteRainDataUsingPropertyId(String propertyId) {
        mDbHelper.deleteRainDataUsingPropertyId(propertyId);
    }

    @Override
    public void deleteAllRainData() {
        mDbHelper.deleteAllRainData();
    }

    @Override
    public void insertParcel(Parcel parcel) {
        mDbHelper.insertParcel(parcel);
    }

    @Override
    public void insertAllParcel(List<Parcel> parcels) {
        mDbHelper.insertAllParcel(parcels);
    }

    @Override
    public List<Parcel> getLocalParcelListForSync() {
        return mDbHelper.getLocalParcelListForSync();
    }

    @Override
    public List<Parcel> loadAllParcel() {
        return mDbHelper.loadAllParcel();
    }

    @Override
    public List<Parcel> loadAllParcelById(String parcelId) {
        return mDbHelper.loadAllParcelById(parcelId);
    }

    @Override
    public void parcelSyncUpdate() {
        mDbHelper.parcelSyncUpdate();
    }

    @Override
    public void updateParcel(Parcel parcel) {
        mDbHelper.updateParcel(parcel);
    }

    @Override
    public Parcel findByParcelName(String userId, String parcelName) {
        return mDbHelper.findByParcelName(userId, parcelName);
    }

    @Override
    public Parcel getParcel(String parcelRk) {
        return mDbHelper.getParcel(parcelRk);
    }

    @Override
    public List<CommonIdBean> getPropertyParcel(String propertyId) {
        return mDbHelper.getPropertyParcel(propertyId);
    }

    @Override
    public void deleteUsingParcelId(String parcelId) {
        mDbHelper.deleteUsingParcelId(parcelId);
    }

    @Override
    public void deleteParcelUsingPropertyId(String propertyId) {
        mDbHelper.deleteParcelUsingPropertyId(propertyId);
    }

    @Override
    public void deleteAllParcel() {
        mDbHelper.deleteAllParcel();
    }

    @Override
    public List<PropertyInfoResponse.PropertyInfoBean> getPropertyList(String userId) {
        return mDbHelper.getPropertyList(userId);
    }

    @Override
    public PropertyDetailResponse.PropertyDetailsBean getPropertyDetail(String propertyId) {
        return mDbHelper.getPropertyDetail(propertyId);
    }

    @Override
    public List<PropertyDetailResponse.PropertyDetailsBean.ParcelBean> getParcelList(String propertyId) {
        return mDbHelper.getParcelList(propertyId);
    }

    @Override
    public String getAnimalCount(String propertyId) {
        return mDbHelper.getAnimalCount(propertyId);
    }

    @Override
    public String getCropCount(String propertyId) {
        return mDbHelper.getCropCount(propertyId);
    }

    @Override
    public List<RainBeanResponse.RainRecordListBean> getRainDataList(String propertyId) {
        return mDbHelper.getRainDataList(propertyId);
    }

    @Override
    public List<RainBeanResponse.RainRecordListBean> getFilterRainDataList(String propertyId, String fromDate) {
        return mDbHelper.getFilterRainDataList(propertyId, fromDate);
    }

    @Override
    public List<RainBeanResponse.RainRecordListBean> getFilterRainDataList(String propertyId, String fromDate, String toDate) {
        return mDbHelper.getFilterRainDataList(propertyId, fromDate, toDate);
    }
/*Property data end here*/

    /*Animal data start here*/
    @Override
    public void insertAnimal(Animal animal) {
        mDbHelper.insertAnimal(animal);
    }

    @Override
    public void insertAllAnimal(List<Animal> animals) {
        mDbHelper.insertAllAnimal(animals);
    }

    @Override
    public void updateAnimal(Animal animal) {
        mDbHelper.updateAnimal(animal);
    }

    @Override
    public void deleteUsingAnimalId(String animalId) {
        mDbHelper.deleteUsingAnimalId(animalId);
    }

    @Override
    public void deleteAnimalUsingParcelId(String parcel_id) {
        mDbHelper.deleteAnimalUsingParcelId(parcel_id);
    }

    @Override
    public void deleteCropUsingParcelId(String parcel_id) {
        mDbHelper.deleteCropUsingParcelId(parcel_id);
    }

    @Override
    public void deleteAllAnimal() {
        mDbHelper.deleteAllAnimal();
    }

    @Override
    public List<AnimalInfoResponse.AnimalListBean> getAnimalList(String userId) {
        return mDbHelper.getAnimalList(userId);
    }

    @Override
    public List<AnimalInfoResponse.AnimalListBean> getPropertyAnimalList(String propertyId) {
        return mDbHelper.getPropertyAnimalList(propertyId);
    }

    @Override
    public List<AnimalInfoResponse.AnimalListBean> getParcelAnimalList(String parcelId) {
        return mDbHelper.getParcelAnimalList(parcelId);
    }

    @Override
    public List<MotherResponse.AnimalMotherListBean> getAnimalMotherList(String userId, String animalId) {
        return mDbHelper.getAnimalMotherList(userId, animalId);
    }

    @Override
    public String getTagNumber(String userId, String tagNumber) {
        return mDbHelper.getTagNumber(userId, tagNumber);
    }

    @Override
    public List<AnimalSyncResult> getLocalAnimalListForSync() {
        return mDbHelper.getLocalAnimalListForSync();
    }

    @Override
    public void animalSyncUpdate() {
        mDbHelper.animalSyncUpdate();
    }

    @Override
    public List<CommonIdBean> getAnimals(String parcelId) {
        return mDbHelper.getAnimals(parcelId);
    }

    @Override
    public List<CommonIdBean> getAnimals(String parcelId, String recordKey) {
        return mDbHelper.getAnimals(parcelId, recordKey);
    }

    @Override
    public String getAnimalCount() {
        return mDbHelper.getAnimalCount();
    }

    @Override
    public void insertTreatment(Treatment treatment) {
        mDbHelper.insertTreatment(treatment);
    }

    @Override
    public void insertAllTreatment(List<Treatment> treatments) {
        mDbHelper.insertAllTreatment(treatments);
    }

    @Override
    public void updateTreatment(Treatment treatment) {
        mDbHelper.updateTreatment(treatment);
    }

    @Override
    public void deleteUsingTreatmentId(String treatmentId) {
        mDbHelper.deleteUsingTreatmentId(treatmentId);
    }

    @Override
    public void deleteAllTreatment() {
        mDbHelper.deleteAllTreatment();
    }

    @Override
    public void insertTreatmentDetail(TreatmentMapping treatment_mapping) {
        mDbHelper.insertTreatmentDetail(treatment_mapping);
    }

    @Override
    public void insertAllTreatmentDetail(List<TreatmentMapping> treatment_mappings) {
        mDbHelper.insertAllTreatmentDetail(treatment_mappings);
    }

    @Override
    public void updateTreatmentDetail(TreatmentMapping treatment_mapping) {
        mDbHelper.updateTreatmentDetail(treatment_mapping);
    }

    @Override
    public void deleteUsingTreatmentDetailMappingId(String treatmentMappingId) {
        mDbHelper.deleteUsingTreatmentDetailMappingId(treatmentMappingId);
    }

    @Override
    public void deleteAllTreatmentDetail() {
        mDbHelper.deleteAllTreatmentDetail();
    }

    @Override
    public List<TreatmentInfoResponse.TreatmentDetailBean> getTreatmentList(String animalId) {
        return mDbHelper.getTreatmentList(animalId);
    }

    @Override
    public List<TreatmentInfoResponse.TreatmentDetailBean.TreatmentListBean> getTreatmentDetailList(String treatment_id) {
        return mDbHelper.getTreatmentDetailList(treatment_id);
    }

    @Override
    public Animal getAnimalUsingAnimalId(String animalId) {
        return mDbHelper.getAnimalUsingAnimalId(animalId);
    }

    @Override
    public List<Animal> getAnimalUsingAnimalLotId(String lotId) {
        return mDbHelper.getAnimalUsingAnimalLotId(lotId);
    }

    @Override
    public List<TreatmentSyncResult> getManageTreatmentList() {
        return mDbHelper.getManageTreatmentList();
    }

    @Override
    public List<TreatmentSyncResult> getManageAllTreatmentList() {
        return mDbHelper.getManageAllTreatmentList();
    }

    @Override
    public List<TreatmentSyncResult.TreatmentListDataBean> getManageTreatmentDetailList() {
        return mDbHelper.getManageTreatmentDetailList();
    }

    @Override
    public List<TreatmentSyncResult.TreatmentListDataBean> getManageTreatmentDetailList(String treatment_id) {
        return mDbHelper.getManageTreatmentDetailList(treatment_id);
    }

    @Override
    public void treatmentSyncUpdate() {
        mDbHelper.treatmentSyncUpdate();
    }

    @Override
    public Treatment getTreatmentInfo(String treatmentId) {
        return mDbHelper.getTreatmentInfo(treatmentId);
    }

    @Override
    public void treatmentMappingSyncUpdate() {
        mDbHelper.treatmentMappingSyncUpdate();
    }

    @Override
    public void insertInsemination(Insemination insemination) {
        mDbHelper.insertInsemination(insemination);
    }

    @Override
    public void insertAllInsemination(List<Insemination> inseminations) {
        mDbHelper.insertAllInsemination(inseminations);
    }

    @Override
    public void updateInsemination(Insemination insemination) {
        mDbHelper.updateInsemination(insemination);
    }

    @Override
    public List<InseminationInfoResponse.InseminationListBean> getInseminationList(String animalId) {
        return mDbHelper.getInseminationList(animalId);
    }

    @Override
    public List<InseminationSyncResult> getInseminationList() {
        return mDbHelper.getInseminationList();
    }

    @Override
    public void inseminationSyncUpdate() {
        mDbHelper.inseminationSyncUpdate();
    }

    @Override
    public void deleteUsingInseminationId(String inseminationId) {
        mDbHelper.deleteUsingInseminationId(inseminationId);
    }

    @Override
    public void deleteAllInsemination() {
        mDbHelper.deleteAllInsemination();
    }

    @Override
    public void insertPregnancyTest(PregnancyTest pregnancyTest) {
        mDbHelper.insertPregnancyTest(pregnancyTest);
    }

    @Override
    public void insertAllPregnancyTest(List<PregnancyTest> pregnancyTests) {
        mDbHelper.insertAllPregnancyTest(pregnancyTests);
    }

    @Override
    public void updatePregnancyTest(PregnancyTest pregnancyTest) {
        mDbHelper.updatePregnancyTest(pregnancyTest);
    }

    @Override
    public List<PregnancyTestInfoResponse.PregnancyListBean> getPregnancyTestList(String animalId) {
        return mDbHelper.getPregnancyTestList(animalId);
    }

    @Override
    public List<PregnancyTestSyncResult> getPregnancyTestList() {
        return mDbHelper.getPregnancyTestList();
    }

    @Override
    public void pregnancyTestSyncUpdate() {
        mDbHelper.pregnancyTestSyncUpdate();
    }

    @Override
    public void deleteUsingPregnancyTestId(String pregnancyTestId) {
        mDbHelper.deleteUsingPregnancyTestId(pregnancyTestId);
    }

    @Override
    public void deleteAllPregnancyTest() {
        mDbHelper.deleteAllPregnancyTest();
    }

    @Override
    public void insertParcelMovement(ParcelMovement parcelMovement) {
        mDbHelper.insertParcelMovement(parcelMovement);
    }

    @Override
    public void insertAllParcelMovement(List<ParcelMovement> parcelMovements) {
        mDbHelper.insertAllParcelMovement(parcelMovements);
    }

    @Override
    public void updateParcelMovement(ParcelMovement parcelMovement) {
        mDbHelper.updateParcelMovement(parcelMovement);
    }

    @Override
    public List<ParcelMovementInfoResponse.ParcelMovementListBean> getParcelMovementList(String animalId) {
        return mDbHelper.getParcelMovementList(animalId);
    }

    @Override
    public List<MoveToParcelSyncResult> getMToParcelList() {
        return mDbHelper.getMToParcelList();
    }

    @Override
    public void parcelMoveSyncUpdate() {
        mDbHelper.parcelMoveSyncUpdate();
    }

    @Override
    public void deleteUsingParcelMovementId(String parcelMovementId) {
        mDbHelper.deleteUsingParcelMovementId(parcelMovementId);
    }

    @Override
    public void deleteAllParcelMovement() {
        mDbHelper.deleteAllParcelMovement();
    }

    @Override
    public void insertCalving(Calving calving) {
        mDbHelper.insertCalving(calving);
    }

    @Override
    public void insertAllCalving(List<Calving> calvings) {
        mDbHelper.insertAllCalving(calvings);
    }

    @Override
    public void updateCalving(Calving calving) {
        mDbHelper.updateCalving(calving);
    }

    @Override
    public List<CalvingInfoResponse.CalvingListBean> getCalvingList(String animalId) {
        return mDbHelper.getCalvingList(animalId);
    }

    @Override
    public List<CalvingSyncResult> getCalvingList() {
        return mDbHelper.getCalvingList();
    }

    @Override
    public void calvingSyncUpdate() {
        mDbHelper.calvingSyncUpdate();
    }

    @Override
    public void deleteUsingCalvingId(String calvingId) {
        mDbHelper.deleteUsingCalvingId(calvingId);
    }

    @Override
    public void deleteAllCalving() {
        mDbHelper.deleteAllCalving();
    }

    @Override
    public void insertWeight(Weight weight) {
        mDbHelper.insertWeight(weight);
    }

    @Override
    public void insertAllWeight(List<Weight> weights) {
        mDbHelper.insertAllWeight(weights);
    }

    @Override
    public void updateWeight(Weight weight) {
        mDbHelper.updateWeight(weight);
    }

    @Override
    public List<WeightInfoResponse.WeightListBean> getWeightList(String animalId) {
        return mDbHelper.getWeightList(animalId);
    }

    @Override
    public List<WeightSyncResult> getWeightList() {
        return mDbHelper.getWeightList();
    }

    @Override
    public void weightSyncUpdate() {
        mDbHelper.weightSyncUpdate();
    }

    @Override
    public void deleteUsingWeightId(String weightId) {
        mDbHelper.deleteUsingWeightId(weightId);
    }

    @Override
    public void deleteAllWeight() {
        mDbHelper.deleteAllWeight();
    }
    /*Animal data end here*/

    /*Crop data start here*/
    @Override
    public void insertCrop(Crop crop) {
        mDbHelper.insertCrop(crop);
    }

    @Override
    public void insertAllCrop(List<Crop> crops) {
        mDbHelper.insertAllCrop(crops);
    }

    @Override
    public void updateCrop(Crop crop) {
        mDbHelper.updateCrop(crop);
    }

    @Override
    public void deleteUsingCropId(String cropId) {
        mDbHelper.deleteUsingCropId(cropId);
    }

    @Override
    public void deleteAllCrop() {
        mDbHelper.deleteAllCrop();
    }

    @Override
    public void cropSyncUpdate() {
        mDbHelper.cropSyncUpdate();
    }

    @Override
    public List<CommonIdBean> getCrops(String parcelId) {
        return mDbHelper.getCrops(parcelId);
    }

    @Override
    public List<CommonIdBean> getCrops(String parcelId, String recordKey) {
        return mDbHelper.getCrops(parcelId, recordKey);
    }

    @Override
    public void insertCropTreatment(CropTreatment treatment) {
        mDbHelper.insertCropTreatment(treatment);
    }

    @Override
    public void insertAllCropTreatment(List<CropTreatment> treatments) {
        mDbHelper.insertAllCropTreatment(treatments);
    }

    @Override
    public void updateCropTreatment(CropTreatment treatment) {
        mDbHelper.updateCropTreatment(treatment);
    }

    @Override
    public void deleteUsingCropTreatmentId(String treatmentId) {
        mDbHelper.deleteUsingCropTreatmentId(treatmentId);
    }

    @Override
    public void deleteAllCropTreatment() {
        mDbHelper.deleteAllCropTreatment();
    }

    @Override
    public void insertCropTreatmentDetail(CropTreatmentMapping treatment_mapping) {
        mDbHelper.insertCropTreatmentDetail(treatment_mapping);
    }

    @Override
    public void insertAllCropTreatmentDetail(List<CropTreatmentMapping> treatment_mappings) {
        mDbHelper.insertAllCropTreatmentDetail(treatment_mappings);
    }

    @Override
    public void updateCropTreatmentDetail(CropTreatmentMapping treatment_mapping) {
        mDbHelper.updateCropTreatmentDetail(treatment_mapping);
    }

    @Override
    public void deleteUsingCropTreatmentDetailMappingId(String treatmentMappingId) {
        mDbHelper.deleteUsingCropTreatmentDetailMappingId(treatmentMappingId);
    }

    @Override
    public void deleteAllCropTreatmentDetail() {
        mDbHelper.deleteAllCropTreatmentDetail();
    }

    @Override
    public List<CropTreatmentInfoResponse.TreatmentDetailListBean> getCropTreatmentList(String cropId) {
        return mDbHelper.getCropTreatmentList(cropId);
    }

    @Override
    public List<CropTreatmentInfoResponse.TreatmentDetailListBean.TreatmentListBean> getCropTreatmentDetailList(String treatment_id) {
        return mDbHelper.getCropTreatmentDetailList(treatment_id);
    }

    @Override
    public String getCropRK(String cropId) {
        return mDbHelper.getCropRK(cropId);
    }

    @Override
    public Crop getCrop(String id) {
        return mDbHelper.getCrop(id);
    }

    @Override
    public List<Crop> getCropUsingCropLotId(String lotId) {
        return mDbHelper.getCropUsingCropLotId(lotId);
    }

    @Override
    public List<CropSyncResult> getCropList(String userId) {
        return mDbHelper.getCropList(userId);
    }

    @Override
    public CropTreatment getCropTreatmentInfo(String treatmentId) {
        return mDbHelper.getCropTreatmentInfo(treatmentId);
    }

    @Override
    public List<CropTreatment> getManageCropTreatmentList() {
        return mDbHelper.getManageCropTreatmentList();
    }

    @Override
    public List<CropTreatment> getManageAllCropTreatmentList() {
        return mDbHelper.getManageAllCropTreatmentList();
    }

    @Override
    public List<CropTreatmentSyncResult.TreatmentListDataBean> getManageCropTreatmentDetailList(String treatment_id) {
        return mDbHelper.getManageCropTreatmentDetailList(treatment_id);
    }

    @Override
    public void cropTreatmentSyncUpdate() {
        mDbHelper.cropTreatmentSyncUpdate();
    }

    @Override
    public void cropTreatmentMappingSyncUpdate() {
        mDbHelper.cropTreatmentMappingSyncUpdate();
    }

    @Override
    public List<CropInfoResponse.CroplListBean> getPropertyCropList() {
        return mDbHelper.getPropertyCropList();
    }

    @Override
    public List<CropInfoResponse.CroplListBean> getPropertyCropList(String propertyId) {
        return mDbHelper.getPropertyCropList(propertyId);
    }

    @Override
    public List<CropInfoResponse.CroplListBean> getParcelCropList(String parcelId) {
        return mDbHelper.getParcelCropList(parcelId);
    }

    @Override
    public List<ExpenseProductBean> getProductList() {
        return mDbHelper.getProductList();
    }

    /*Crop data end here*/

    /*Product data start here*/
    @Override
    public void deleteUsingProductId(String productId) {
        mDbHelper.deleteUsingProductId(productId);
    }

    @Override
    public void deleteAllProduct() {
        mDbHelper.deleteAllProduct();
    }

    @Override
    public void updateProduct(Product product) {
        mDbHelper.updateProduct(product);
    }

    @Override
    public void insertProduct(Product product) {
        mDbHelper.insertProduct(product);
    }

    @Override
    public void insertAllProduct(List<Product> products) {
        mDbHelper.insertAllProduct(products);
    }

    @Override
    public List<ProductInfoResponse.ProductListBean> getAllProductList() {
        return mDbHelper.getAllProductList();
    }

    @Override
    public List<ProductInfoResponse.ProductListBean> getMyProductList(String userType) {
        return mDbHelper.getMyProductList(userType);
    }

    @Override
    public void productSyncUpdate() {
        mDbHelper.productSyncUpdate();
    }

    @Override
    public List<ProductInfoResponse.ProductListBean> productFilterAll(String prodName, String minPrice, String maxPrice, String qty) {
        return mDbHelper.productFilterAll(prodName, minPrice, maxPrice, qty);
    }

    @Override
    public List<ProductInfoResponse.ProductListBean> productFilterAllName(String prodName) {
        return mDbHelper.productFilterAllName(prodName);
    }

    @Override
    public List<ProductInfoResponse.ProductListBean> productFilterAllPrice(String minPrice, String maxPrice) {
        return mDbHelper.productFilterAllPrice(minPrice, maxPrice);
    }

    @Override
    public List<ProductInfoResponse.ProductListBean> productFilterAllQty(String qty) {
        return mDbHelper.productFilterAllQty(qty);
    }

    @Override
    public List<ProductInfoResponse.ProductListBean> productFilterAllNameAndPrice(String prodName, String minPrice, String maxPrice) {
        return mDbHelper.productFilterAllNameAndPrice(prodName, minPrice, maxPrice);
    }

    @Override
    public List<ProductInfoResponse.ProductListBean> productFilterAllNameAndQty(String prodName, String qty) {
        return mDbHelper.productFilterAllNameAndQty(prodName, qty);
    }

    @Override
    public List<ProductInfoResponse.ProductListBean> productFilterAllQtyAndPrice(String qty, String minPrice, String maxPrice) {
        return mDbHelper.productFilterAllQtyAndPrice(qty, minPrice, maxPrice);
    }

    @Override
    public List<ProductInfoResponse.ProductListBean> productMyFilter(String userId, String prodName, String minPrice, String maxPrice, String qty) {
        return mDbHelper.productMyFilter(userId, prodName, minPrice, maxPrice, qty);
    }

    @Override
    public List<ProductInfoResponse.ProductListBean> productMyFilterName(String userId, String prodName) {
        return mDbHelper.productMyFilterName(userId, prodName);
    }

    @Override
    public List<ProductInfoResponse.ProductListBean> productMyFilterPrice(String userId, String minPrice, String maxPrice) {
        return mDbHelper.productMyFilterPrice(userId, minPrice, maxPrice);
    }

    @Override
    public List<ProductInfoResponse.ProductListBean> productMyFilterQty(String userId, String qty) {
        return mDbHelper.productMyFilterQty(userId, qty);
    }

    @Override
    public List<ProductInfoResponse.ProductListBean> productMyFilterNameAndPrice(String userId, String prodName, String minPrice, String maxPrice) {
        return mDbHelper.productMyFilterNameAndPrice(userId, prodName, minPrice, maxPrice);
    }

    @Override
    public List<ProductInfoResponse.ProductListBean> productMyFilterNameAndQty(String userId, String prodName, String qty) {
        return mDbHelper.productMyFilterNameAndQty(userId, prodName, qty);
    }

    @Override
    public List<ProductInfoResponse.ProductListBean> productMyFilterQtyAndPrice(String userId, String qty, String minPrice, String maxPrice) {
        return mDbHelper.productMyFilterQtyAndPrice(userId, qty, minPrice, maxPrice);
    }

    @Override
    public List<ProductSyncResult> getManageProductList() {
        return mDbHelper.getManageProductList();
    }

    @Override
    public void insertExpense(Expense expense) {
        mDbHelper.insertExpense(expense);
    }

    @Override
    public void insertAllExpense(List<Expense> expenses) {
        mDbHelper.insertAllExpense(expenses);
    }

    @Override
    public void updateExpense(Expense expense) {
        mDbHelper.updateExpense(expense);
    }

    @Override
    public void deleteAllExpense() {
        mDbHelper.deleteAllExpense();
    }

    @Override
    public void deleteUsingExpenseId(String expenseId) {
        mDbHelper.deleteUsingExpenseId(expenseId);
    }

    @Override
    public void expenseSyncUpdate() {
        mDbHelper.expenseSyncUpdate();
    }

    @Override
    public void insertExpenseMapping(ExpenseMapping expenseMapping) {
        mDbHelper.insertExpenseMapping(expenseMapping);
    }

    @Override
    public void insertAllExpenseMapping(List<ExpenseMapping> expenseMappings) {
        mDbHelper.insertAllExpenseMapping(expenseMappings);
    }

    @Override
    public void updateAllExpenseMapping(List<ExpenseMapping> expenseMappings) {
        mDbHelper.updateAllExpenseMapping(expenseMappings);
    }

    @Override
    public void updateExpenseMapping(ExpenseMapping expenseMapping) {
        mDbHelper.updateExpenseMapping(expenseMapping);
    }

    @Override
    public void deleteAllExpenseMapping() {
        mDbHelper.deleteAllExpenseMapping();
    }

    @Override
    public void deleteUsingExpenseMappingId(String expenseMappingId) {
        mDbHelper.deleteUsingExpenseMappingId(expenseMappingId);
    }

    @Override
    public void deleteExpenseMappingUsingExpenseId(String expenseId) {
        mDbHelper.deleteExpenseMappingUsingExpenseId(expenseId);
    }

    @Override
    public void expenseMappingSyncUpdate() {
        mDbHelper.expenseMappingSyncUpdate();
    }

    @Override
    public void insertExpenseProdMapping(ExpenseProductMapping expenseProductMapping) {
        mDbHelper.insertExpenseProdMapping(expenseProductMapping);
    }

    @Override
    public void insertAllExpenseProdMapping(List<ExpenseProductMapping> expenseProductMappings) {
        mDbHelper.insertAllExpenseProdMapping(expenseProductMappings);
    }

    @Override
    public void updateAllExpenseProdMapping(List<ExpenseProductMapping> expenseProductMapping) {
        mDbHelper.updateAllExpenseProdMapping(expenseProductMapping);
    }

    @Override
    public void updateExpenseProdMapping(ExpenseProductMapping expenseProductMapping) {
        mDbHelper.updateExpenseProdMapping(expenseProductMapping);
    }

    @Override
    public void deleteAllExpenseProdMapping() {
        mDbHelper.deleteAllExpenseProdMapping();
    }

    @Override
    public void deleteUsingExpenseProdMappingId(String expenseProductMappingId) {
        mDbHelper.deleteUsingExpenseProdMappingId(expenseProductMappingId);
    }

    @Override
    public void deleteExpenseProdMappingUsingExpenseId(String expenseId) {
        mDbHelper.deleteExpenseProdMappingUsingExpenseId(expenseId);
    }

    @Override
    public void expenseProductMappingSyncUpdate() {
        mDbHelper.expenseProductMappingSyncUpdate();
    }

    @Override
    public List<IncomeOrExpenseApplyOnBean> getAnimalApplyOnInfo() {
        return mDbHelper.getAnimalApplyOnInfo();
    }

    @Override
    public List<IncomeOrExpenseApplyOnBean> getCropApplyOnInfo() {
        return mDbHelper.getCropApplyOnInfo();
    }

    @Override
    public List<IncomeOrExpenseApplyOnBean> getAnimalLotApplyOnInfo() {
        return mDbHelper.getAnimalLotApplyOnInfo();
    }

    @Override
    public List<IncomeOrExpenseApplyOnBean> getCropLotApplyOnInfo() {
        return mDbHelper.getCropLotApplyOnInfo();
    }

    @Override
    public List<IncomeOrExpenseApplyOnBean> getParcelApplyOnInfo() {
        return mDbHelper.getParcelApplyOnInfo();
    }

    @Override
    public List<IncomeOrExpenseApplyOnBean> getPropertyApplyOnInfo() {
        return mDbHelper.getPropertyApplyOnInfo();
    }

    @Override
    public List<ExpenseInfoResponse.ExpenseListBean> getExpenseList(String expenseFor) {
        return mDbHelper.getExpenseList(expenseFor);
    }

    @Override
    public List<ExpenseInfoResponse.ExpenseListBean.AppliedOnBean> getAnimalExpenseAppliedOnList(String expenseId) {
        return mDbHelper.getAnimalExpenseAppliedOnList(expenseId);
    }

    @Override
    public List<ExpenseInfoResponse.ExpenseListBean.AppliedOnBean> getParcelExpenseAppliedOnList(String expenseId) {
        return mDbHelper.getParcelExpenseAppliedOnList(expenseId);
    }

    @Override
    public List<ExpenseInfoResponse.ExpenseListBean.AppliedOnBean> getPropertyExpenseAppliedOnList(String expenseId) {
        return mDbHelper.getPropertyExpenseAppliedOnList(expenseId);
    }

    @Override
    public List<ExpenseInfoResponse.ExpenseListBean.AppliedOnBean> getCropExpenseAppliedOnList(String expenseId) {
        return mDbHelper.getCropExpenseAppliedOnList(expenseId);
    }

    @Override
    public List<ExpenseInfoResponse.ExpenseListBean.AppliedOnBean> getCropLotExpenseAppliedOnList(String expenseId) {
        return mDbHelper.getCropLotExpenseAppliedOnList(expenseId);
    }

    @Override
    public List<ExpenseInfoResponse.ExpenseListBean.AppliedOnBean> getAnimalLotExpenseAppliedOnList(String expenseId) {
        return mDbHelper.getAnimalLotExpenseAppliedOnList(expenseId);
    }

    @Override
    public List<ExpenseInfoResponse.ExpenseListBean.ProductDetailsBean> getExpenseProductList(String expenseId) {
        return mDbHelper.getExpenseProductList(expenseId);
    }

    @Override
    public ExpenseProductMapping getExpenseProductDetail(String expense_id, String product_id) {
        return mDbHelper.getExpenseProductDetail(expense_id, product_id);
    }

    @Override
    public String getExpenseApplyStatus(String id) {
        return mDbHelper.getExpenseApplyStatus(id);
    }

    @Override
    public String getExpenseApplyStatus(String id, String recordKey) {
        return mDbHelper.getExpenseApplyStatus(id, recordKey);
    }

    @Override
    public String getProductExpenseApplyStatus(String id, String recordKey) {
        return mDbHelper.getProductExpenseApplyStatus(id, recordKey);
    }

    @Override
    public List<ExpenseSyncResult> getManageExpenseList() {
        return mDbHelper.getManageExpenseList();
    }

    @Override
    public List<ExpenseSyncResult.AppliedOnDetail> getManageAnimalExpenseAppliedOnList(String expenseId) {
        return mDbHelper.getManageAnimalExpenseAppliedOnList(expenseId);
    }

    @Override
    public List<ExpenseSyncResult.AppliedOnDetail> getManageParcelExpenseAppliedOnList(String expenseId) {
        return mDbHelper.getManageParcelExpenseAppliedOnList(expenseId);
    }

    @Override
    public List<ExpenseSyncResult.AppliedOnDetail> getManagePropertyExpenseAppliedOnList(String expenseId) {
        return mDbHelper.getManagePropertyExpenseAppliedOnList(expenseId);
    }

    @Override
    public List<ExpenseSyncResult.AppliedOnDetail> getManageCropExpenseAppliedOnList(String expenseId) {
        return mDbHelper.getManageCropExpenseAppliedOnList(expenseId);
    }

    @Override
    public List<ExpenseSyncResult.AppliedOnDetail> getManageCropLotExpenseAppliedOnList(String expenseId) {
        return mDbHelper.getManageCropLotExpenseAppliedOnList(expenseId);
    }

    @Override
    public List<ExpenseSyncResult.AppliedOnDetail> getManageAnimalLotExpenseAppliedOnList(String expenseId) {
        return mDbHelper.getManageAnimalLotExpenseAppliedOnList(expenseId);
    }

    @Override
    public List<ExpenseSyncResult.ProductDetail> getManageExpenseProductList(String expenseId) {
        return mDbHelper.getManageExpenseProductList(expenseId);
    }

    @Override
    public List<String> getExpenseIdFromExpenseMapping(String appliedId) {
        return mDbHelper.getExpenseIdFromExpenseMapping(appliedId);
    }

    @Override
    public List<String> getExpenseIdFromExpenseMapping(String appliedId, String recordKey) {
        return mDbHelper.getExpenseIdFromExpenseMapping(appliedId, recordKey);
    }

    @Override
    public String getExpenseCostFromExpense(String expenseId, String expenseFor) {
        return mDbHelper.getExpenseCostFromExpense(expenseId, expenseFor);
    }

    @Override
    public String getExpenseCostFromExpense(String expenseId, String expenseFor, Date fromDate) {
        return mDbHelper.getExpenseCostFromExpense(expenseId, expenseFor, fromDate);
    }

    @Override
    public String getExpenseCostFromExpense(String expenseId, String expenseFor, Date fromDate, Date toDate) {
        return mDbHelper.getExpenseCostFromExpense(expenseId, expenseFor, fromDate, toDate);
    }

    @Override
    public String getExpenseCountFromExpenseMapping(String expenseId) {
        return mDbHelper.getExpenseCountFromExpenseMapping(expenseId);
    }

    @Override
    public List<IncomeBean> getPropertyExpenseFromParcel(String propertyId) {
        return mDbHelper.getPropertyExpenseFromParcel(propertyId);
    }

    @Override
    public String getParcelCountUsingProperty(String propertyId) {
        return mDbHelper.getParcelCountUsingProperty(propertyId);
    }

    @Override
    public IncomeBean getParcelIdFromAnimal(String animalId) {
        return mDbHelper.getParcelIdFromAnimal(animalId);
    }

    @Override
    public List<IncomeBean> getAnimalIdUsingParcel(String parcelId, String recordKey) {
        return mDbHelper.getAnimalIdUsingParcel(parcelId, recordKey);
    }

    @Override
    public String getTtlSaleFromAnimal(String animalId) {
        return mDbHelper.getTtlSaleFromAnimal(animalId);
    }

    @Override
    public String getTtlSaleFromAnimal(String animalId, Date fromDate) {
        return mDbHelper.getTtlSaleFromAnimal(animalId, fromDate);
    }

    @Override
    public String getTtlSaleFromAnimal(String animalId, Date fromDate, Date toDate) {
        return mDbHelper.getTtlSaleFromAnimal(animalId, fromDate, toDate);
    }

    @Override
    public String getAnimalLotIdUsingAnimal(String animalId) {
        return mDbHelper.getAnimalLotIdUsingAnimal(animalId);
    }

    @Override
    public String getAnimalCountUsingAnimalLotId(String lotId) {
        return mDbHelper.getAnimalCountUsingAnimalLotId(lotId);
    }

    @Override
    public String getAnimalCountUsingParcel(String parcelId) {
        return mDbHelper.getAnimalCountUsingParcel(parcelId);
    }

    @Override
    public List<IncomeBean> getCropIdUsingParcel(String parcelId, String recordKey) {
        return mDbHelper.getCropIdUsingParcel(parcelId, recordKey);
    }

    @Override
    public String getTtlSaleFromCrop(String cropId) {
        return mDbHelper.getTtlSaleFromCrop(cropId);
    }

    @Override
    public String getTtlSaleFromCrop(String cropId, Date fromDate) {
        return mDbHelper.getTtlSaleFromCrop(cropId, fromDate);
    }

    @Override
    public String getTtlSaleFromCrop(String cropId, Date fromDate, Date toDate) {
        return mDbHelper.getTtlSaleFromCrop(cropId, fromDate, toDate);
    }

    @Override
    public String getCropLotIdUsingCrop(String cropId) {
        return mDbHelper.getCropLotIdUsingCrop(cropId);
    }

    @Override
    public String getCropCountUsingCropLotId(String lotId) {
        return mDbHelper.getCropCountUsingCropLotId(lotId);
    }

    @Override
    public String getCropCountUsingParcel(String parcelId) {
        return mDbHelper.getCropCountUsingParcel(parcelId);
    }
    /*Product data end here*/



    @Override
    public void clearAllTable() {
        mDbHelper.clearAllTable();
    }

    /* Session Info */
    @Override
    public Boolean isLoggedIn() {
        return mPreferencesHelper.isLoggedIn();
    }

    @Override
    public UserInfoBean getUserInfo() {
        return mPreferencesHelper.getUserInfo();
    }

    @Override
    public void setUserInfo(UserInfoBean userInfo) {
        mPreferencesHelper.setUserInfo(userInfo);
    }

    @Override
    public void setRememberMe(String email, String pwd) {
        mPreferencesHelper.setRememberMe(email, pwd);
    }

    @Override
    public String[] getRememberMe() {
        return mPreferencesHelper.getRememberMe();
    }

    public HashMap<String, String> getHeader() {
        return mPreferencesHelper.getHeader();
    }

    @Override
    public Boolean getLocalDataExistForSync() {
        return mPreferencesHelper.getLocalDataExistForSync();
    }

    @Override
    public void setLocalDataExistForSync(Boolean status) {
        mPreferencesHelper.setLocalDataExistForSync(status);
    }

    @Override
    public Boolean getAppOnline() {
        return mPreferencesHelper.getAppOnline();
    }

    @Override
    public void setAppOnline(Boolean isAppOnline) {
        mPreferencesHelper.setAppOnline(isAppOnline);
    }

    @Override
    public String getSyncTimer() {
        return mPreferencesHelper.getSyncTimer();
    }

    @Override
    public void setSyncTimer(String timer) {
        mPreferencesHelper.setSyncTimer(timer);
    }

    @Override
    public void logout(Activity activity) {
        mPreferencesHelper.logout(activity);
    }
}
