package com.agrinvestapp.data.local.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.agrinvestapp.data.model.api.ParcelMovementInfoResponse;
import com.agrinvestapp.data.model.db.ParcelMovement;
import com.agrinvestapp.data.model.other.MoveToParcelSyncResult;

import java.util.List;

/**
 * Created by hemant
 * Date: 10/4/18.
 */

@Dao
public interface ParcelMovementDao {

    @Query("DELETE FROM parcel_movement WHERE parcelMovementId = :parcelMovementId")
    void deleteUsingParcelMovementId(String parcelMovementId);

    @Query("DELETE FROM parcel_movement")
    void deleteAll();

    @Update
    void update(ParcelMovement parcelMovement);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(ParcelMovement parcelMovement);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<ParcelMovement> parcelMovements);

    //@Query("SELECT * FROM parcel_movement WHERE animal_id=:animal_id AND event != 'delete' ORDER BY crd DESC")
    @Query("SELECT pm.*, parcel1.parcelName as fromName, parcel2.parcelName as toName FROM parcel_movement as pm JOIN parcel as parcel1 ON parcel1.parcelId = pm.movementFrom JOIN parcel as parcel2 ON parcel2.parcelId = pm.movementTo WHERE animal_id=:animal_id AND pm.event != 'delete' ORDER BY crd DESC")
    List<ParcelMovementInfoResponse.ParcelMovementListBean> getParcelMovementList(String animal_id);

    @Query("SELECT * FROM parcel_movement WHERE parcelMovementId IN (:parcelMovementIds)")
    List<ParcelMovement> loadAllByIds(List<Integer> parcelMovementIds);

    @Query("SELECT animal.recordKey, parcelMovement.event as requestType , parcelMovement.movementFrom, " +
            "parcelFrom.recordKey as movementFromRecordKey, parcelTo.recordKey as movementToRecordKey, parcelMovement.recordKey as movementRecordKey," +
            " parcelMovement.parcelMovementId, parcelMovement.movementDate,parcelMovement.movementTo, " +
            "parcelMovement.isDataSync FROM animal as animal " +
            "JOIN parcel_movement as parcelMovement ON parcelMovement.animal_id=animal.animalId " +
            "JOIN parcel as parcelFrom ON parcelMovement.movementFrom = parcelFrom.parcelId " +
            "JOIN parcel as parcelTo ON parcelMovement.movementTo = parcelTo.parcelId WHERE parcelMovement.isDataSync != 1") /*GROUP BY animal.animalId*/
    List<MoveToParcelSyncResult> getMToParcelList();

    @Query("UPDATE parcel_movement SET isDataSync = 1")
    void parcelMoveSyncUpdate();
}
