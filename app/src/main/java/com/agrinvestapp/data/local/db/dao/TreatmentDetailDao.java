package com.agrinvestapp.data.local.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.agrinvestapp.data.model.db.TreatmentMapping;

import java.util.List;

/**
 * Created by hemant
 * Date: 10/4/18.
 */

@Dao
public interface TreatmentDetailDao {

    @Query("DELETE FROM treatment_mapping WHERE treatmentMappingId = :treatmentMappingId")
    void deleteUsingTreatmentMappingId(String treatmentMappingId);

    @Query("DELETE FROM treatment_mapping")
    void deleteAll();

    @Update
    void update(TreatmentMapping treatment_mapping);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(TreatmentMapping treatment_mapping);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<TreatmentMapping> treatment_mappings);

    @Query("SELECT * FROM treatment_mapping WHERE event != 'delete'")
    List<TreatmentMapping> loadAll();

    @Query("SELECT * FROM treatment_mapping WHERE event != 'delete' AND treatmentMappingId IN (:treatmentMappingId)")
    List<TreatmentMapping> loadAllByIds(List<Integer> treatmentMappingId);

    @Query("UPDATE treatment_mapping SET isDataSync = 1")
    void treatmentMappingSyncUpdate();
}
