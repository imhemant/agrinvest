package com.agrinvestapp.data.local.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.agrinvestapp.data.model.db.ExpenseProductMapping;

import java.util.List;

/**
 * Created by hemant
 * Date: 10/4/18.
 */

@Dao
public interface ExpenseProductMappingDao {

    @Query("DELETE FROM expense_product_mapping WHERE expenseProductMappingId = :expenseProductMappingId")
    void deleteUsingExpenseProdMappingId(String expenseProductMappingId);

    @Query("DELETE FROM expense_product_mapping WHERE expense_id = :expenseId")
    void deleteExpenseProdMappingUsingExpenseId(String expenseId);

    @Query("DELETE FROM expense_product_mapping")
    void deleteAll();

    @Update
    void update(ExpenseProductMapping expenseProductMapping);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(ExpenseProductMapping expenseProductMapping);

    @Update
    void updateAll(List<ExpenseProductMapping> expenseProductMapping);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<ExpenseProductMapping> expenseProductMapping);

    @Query("SELECT * FROM expense_product_mapping Where event != 'delete'")
    List<ExpenseProductMapping> loadAll();

    @Query("SELECT * FROM expense_product_mapping WHERE event != 'delete' AND expenseProductMappingId IN (:expenseProductMappingIds)")
    List<ExpenseProductMapping> loadAllByIds(List<Integer> expenseProductMappingIds);

    @Query("UPDATE expense_product_mapping SET isDataSync = 1")
    void expenseProductMappingSyncUpdate();

}
