package com.agrinvestapp.data.local.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.agrinvestapp.data.model.api.CropInfoResponse;
import com.agrinvestapp.data.model.api.CropTreatmentInfoResponse;
import com.agrinvestapp.data.model.db.Crop;
import com.agrinvestapp.data.model.db.CropTreatment;
import com.agrinvestapp.data.model.other.CommonIdBean;
import com.agrinvestapp.data.model.other.CropSyncResult;
import com.agrinvestapp.data.model.other.CropTreatmentSyncResult;

import java.util.List;

/**
 * Created by hemant
 * Date: 10/4/18.
 */

@Dao
public interface CropDao {

    @Query("DELETE FROM crop WHERE cropId = :cropId")
    void deleteUsingCropId(String cropId);

    @Query("DELETE FROM crop WHERE parcel_id = :parcel_id")
    void deleteCropUsingParcelId(String parcel_id);

    @Query("DELETE FROM crop")
    void deleteAll();

    @Update
    void update(Crop crop);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Crop crop);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Crop> crops);

    @Query("UPDATE crop SET isDataSync = 1")
    void cropSyncUpdate();

    @Query("SELECT cropId as id, recordKey FROM crop Where parcel_id =:parcelId AND event != 'delete'")
    List<CommonIdBean> getCrops(String parcelId);

    @Query("SELECT cropId as id, recordKey FROM crop Where (parcel_id =:parcelId OR parcel_id =:recordKey) AND event != 'delete'")
    List<CommonIdBean> getCrops(String parcelId, String recordKey);

    @Query("SELECT * FROM crop_treatment WHERE crop_id = :cropId AND event != 'delete' ORDER BY crd DESC")
        //ORDER BY treatmentId DESC
    List<CropTreatmentInfoResponse.TreatmentDetailListBean> getCropTreatmentList(String cropId);

    @Query("SELECT * FROM crop_treatment_mapping WHERE treatment_id = :treatment_id AND event != 'delete' ORDER BY crd DESC")
    List<CropTreatmentInfoResponse.TreatmentDetailListBean.TreatmentListBean> getCropTreatmentDetailList(String treatment_id);

    @Query("SELECT parcel.parcelName,parcel.recordKey as parcelRecordKey, crop.* FROM property as property JOIN parcel as parcel ON property.propertyId = parcel.property_id JOIN crop as crop ON crop.parcel_id = parcel.parcelId WHERE crop.event != 'delete' ORDER BY crop.crd DESC")
    List<CropInfoResponse.CroplListBean> getPropertyCropList();

    @Query("SELECT parcel.parcelName,parcel.recordKey as parcelRecordKey, crop.* FROM property as property JOIN parcel as parcel ON property.propertyId = parcel.property_id JOIN crop as crop ON crop.parcel_id = parcel.parcelId WHERE property.propertyId = :propertyId AND crop.event != 'delete' ORDER BY crop.crd DESC")
    List<CropInfoResponse.CroplListBean> getPropertyCropList(String propertyId);

    @Query("SELECT parcel.parcelName,parcel.recordKey as parcelRecordKey, crop.* FROM parcel as parcel JOIN crop as crop ON crop.parcel_id = parcel.parcelId WHERE parcel.parcelId = :parcelId AND crop.event != 'delete' ORDER BY crop.crd DESC")
    List<CropInfoResponse.CroplListBean> getParcelCropList(String parcelId);

    @Query("SELECT recordKey from crop WHERE cropId = :cropId AND event != 'delete'")
    String getCropRK(String cropId);

    @Query("SELECT * from crop WHERE (cropId =:id OR recordKey =:id) AND event != 'delete'")
    Crop getCrop(String id);

    @Query("SELECT * FROM crop WHERE lotId =:lotId AND event != 'delete'")
    List<Crop> getCropUsingCropLotId(String lotId);

    /*property.propertyId, parcel.parcelName,*/
    @Query("SELECT  parcel.recordKey as parcelRecordKey, " +
            "crop.event as requestType, crop.recordKey, crop.lotId, crop.cropName, crop.preparationDate, crop.plantingDate," +
            "crop.harvestDate, crop.saleDate, crop.salePrice, crop.saleQuantity, crop.saleTotalPrice, crop.costFrom, crop.costTo," +
            "crop.costTotal, crop.description\n" +
            "FROM users as users\n" +
            "JOIN property as property ON users.userId = property.user_id\n" +
            "JOIN parcel as parcel ON property.propertyId = parcel.property_id\n" +
            "JOIN crop as crop ON crop.parcel_id = parcel.parcelId\n" +
            "WHERE users.userId = :userId AND crop.isDataSync != 1\n" +
            "ORDER BY crop.cropId DESC")
    List<CropSyncResult> getCropList(String userId);

    @Query("SELECT *  FROM crop_treatment WHERE isDataSync != 1 ORDER BY event DESC")
    List<CropTreatment> getManageCropTreatmentList();

    @Query("SELECT * FROM crop_treatment ORDER BY event DESC")
    List<CropTreatment> getManageAllCropTreatmentList();

    @Query("SELECT cropMapping.event as requestType, cropMapping.recordKey as mappingRecordKey," +
            "cropMapping.treatmentDate, cropMapping.treatmentDescription FROM crop_treatment_mapping as cropMapping WHERE treatment_id = :treatment_id AND isDataSync != 1 ORDER BY event DESC")
    List<CropTreatmentSyncResult.TreatmentListDataBean> getManageCropTreatmentDetailList(String treatment_id);
}
