package com.agrinvestapp.data.local.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.agrinvestapp.data.model.api.AnimalInfoResponse;
import com.agrinvestapp.data.model.api.MotherResponse;
import com.agrinvestapp.data.model.api.TreatmentInfoResponse;
import com.agrinvestapp.data.model.db.Animal;
import com.agrinvestapp.data.model.other.AnimalSyncResult;
import com.agrinvestapp.data.model.other.TreatmentSyncResult;

import java.util.List;

/**
 * Created by hemant
 * Date: 10/4/18.
 */

@Dao
public interface CustomAnimalDao {

    @Query("SELECT parcel.parcelName,parcel.recordKey as parcelRecordKey, animal.* FROM users as users JOIN property as property ON users.userId = property.user_id JOIN parcel as parcel ON property.propertyId = parcel.property_id JOIN animal as animal ON animal.parcel_id = parcel.parcelId WHERE users.userId = :userId AND animal.event != 'delete' ORDER BY animal.crd DESC")
    List<AnimalInfoResponse.AnimalListBean> getAnimalList(String userId);

    @Query("SELECT parcel.parcelName,parcel.recordKey as parcelRecordKey, animal.* FROM property as property JOIN parcel as parcel ON property.propertyId = parcel.property_id JOIN animal as animal ON animal.parcel_id = parcel.parcelId WHERE property.propertyId = :propertyId AND animal.event != 'delete' ORDER BY animal.crd DESC")
    List<AnimalInfoResponse.AnimalListBean> getPropertyAnimalList(String propertyId);

    @Query("SELECT parcel.parcelName,parcel.recordKey as parcelRecordKey, animal.* FROM parcel as parcel JOIN animal as animal ON animal.parcel_id = parcel.parcelId WHERE parcel.parcelId = :parcelId AND animal.event != 'delete' ORDER BY animal.crd DESC")
    List<AnimalInfoResponse.AnimalListBean> getParcelAnimalList(String parcelId);

    @Query("SELECT animal.tagNumber FROM users as users JOIN property as property ON users.userId = property.user_id JOIN parcel as parcel ON property.propertyId = parcel.property_id JOIN animal as animal ON animal.parcel_id = parcel.parcelId WHERE users.userId = :userId AND animal.animalId != :animalId AND animal.event != 'delete' ORDER BY animal.tagNumber ASC")
    List<MotherResponse.AnimalMotherListBean> getAnimalMotherList(String userId, String animalId);

    @Query("SELECT animal.event as requestType, animal.recordKey, parcel.recordKey as parcelRecordKey," +
            "animal.lotId, animal.tagNumber, animal.bornOrBuyDate, animal.markingOrTagging, animal.mother, animal.boughtFrom," +
            "animal.description, animal.dismant, animal.saleDate, animal.salePrice, animal.saleQuantity, animal.saleTotalPrice," +
            "animal.costFrom, animal.costTo, animal.costTotal, animal.isDataSync FROM animal JOIN parcel as parcel ON animal.parcel_id=parcel.parcelId WHERE animal.isDataSync != 1 ORDER BY animal.crd ASC")
    List<AnimalSyncResult> getLocalAnimalListForSync();

    @Query("SELECT a.tagNumber FROM users as u JOIN property as p ON p.user_id = u.userId JOIN parcel as pcl ON pcl.property_id = p.propertyId JOIN animal as a ON a.parcel_id = pcl.parcelId WHERE a.tagNumber LIKE :tagNumber AND u.userId = :userId")
    String getTagNumber(String userId, String tagNumber);

    @Query("SELECT * FROM treatment WHERE treatmentForId = :animalId AND treatmentFor = '1' AND event != 'delete' ORDER BY crd DESC")
        //ORDER BY treatmentId DESC
    List<TreatmentInfoResponse.TreatmentDetailBean> getTreatmentList(String animalId); //treatmentForId is animal id AND treatmentFor ==1==animal

    @Query("SELECT * FROM treatment_mapping WHERE treatment_id = :treatment_id AND event != 'delete' ORDER BY crd DESC")
    List<TreatmentInfoResponse.TreatmentDetailBean.TreatmentListBean> getTreatmentDetailList(String treatment_id);

    @Query("SELECT * FROM animal WHERE (animalId =:animalId OR recordKey =:animalId) AND event != 'delete'")
    Animal getAnimalUsingAnimalId(String animalId);

    @Query("SELECT * FROM animal WHERE lotId =:lotId AND event != 'delete'")
    List<Animal> getAnimalUsingAnimalLotId(String lotId);

    @Query("SELECT * FROM treatment WHERE isDataSync != 1 ORDER BY event DESC")
    List<TreatmentSyncResult> getManageTreatmentList();

    @Query("SELECT * FROM treatment ORDER BY event DESC")
    List<TreatmentSyncResult> getManageAllTreatmentList();

    @Query("SELECT * FROM treatment_mapping WHERE treatment_id = :treatment_id AND isDataSync != 1 ORDER BY event DESC")
    List<TreatmentSyncResult.TreatmentListDataBean> getManageTreatmentDetailList(String treatment_id);

    @Query("SELECT * FROM treatment_mapping WHERE isDataSync != 1 ORDER BY event DESC")
    List<TreatmentSyncResult.TreatmentListDataBean> getManageTreatmentDetailList();
}
