package com.agrinvestapp.data.local.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.agrinvestapp.data.model.db.Parcel;
import com.agrinvestapp.data.model.other.CommonIdBean;

import java.util.List;

/**
 * Created by hemant
 * Date: 10/4/18.
 */

@Dao
public interface ParcelDao {

    @Query("DELETE FROM parcel WHERE property_id = :propertyId")
    void deleteUsingPropertyId(String propertyId);

    @Query("DELETE FROM parcel WHERE parcelId = :parcelId")
    void deleteUsingParcelId(String parcelId);

    @Query("DELETE FROM parcel")
    void deleteAll();

    // @Query("SELECT * FROM parcel WHERE parcelName LIKE :parcelName AND event != 'delete'  LIMIT 1")
    @Query("SELECT parcel.* FROM (parcel as parcel, property as property) JOIN users as user ON user.userId = property.user_id WHERE parcel.parcelName LIKE :parcelName AND parcel.event != 'delete' AND user.userId = :userId")
    Parcel findByParcelName(String userId, String parcelName);

    @Query("SELECT * FROM parcel WHERE (parcelId =:parcelRk OR recordKey =:parcelRk) AND event != 'delete'")
    Parcel getParcel(String parcelRk);

    @Query("SELECT parcelId as id, recordKey FROM parcel WHERE property_id =:propertyId AND event != 'delete'")
    List<CommonIdBean> getPropertyParcel(String propertyId);

    @Update
    void update(Parcel parcel);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Parcel parcel);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Parcel> parcels);

    @Query("SELECT * FROM parcel WHERE event != 'delete' order by parcelName ASC")
    List<Parcel> loadAll();

    @Query("SELECT * FROM parcel WHERE parcelId !=:parcelId AND event != 'delete'")
    List<Parcel> loadAllById(String parcelId);

    @Query("SELECT * FROM parcel WHERE parcelId IN (:parcelId) AND event != 'delete'")
    List<Parcel> loadAllByIds(List<Integer> parcelId);

    @Query("UPDATE parcel SET isDataSync = 1")
    void parcelSyncUpdate();
}
