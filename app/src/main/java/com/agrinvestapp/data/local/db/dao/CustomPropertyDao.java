package com.agrinvestapp.data.local.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.agrinvestapp.data.model.api.PropertyDetailResponse;
import com.agrinvestapp.data.model.api.PropertyInfoResponse;
import com.agrinvestapp.data.model.api.RainBeanResponse;
import com.agrinvestapp.data.model.db.Parcel;

import java.util.List;

/**
 * Created by hemant
 * Date: 10/4/18.
 */

@Dao
public interface CustomPropertyDao {

    @Query("SELECT p.propertyId, p.user_id, p.propertyName, p.location, p.longitude, p.latitude, count(par.property_id) as parcelCount FROM property as p LEFT JOIN parcel as par ON p.propertyId = par.property_id AND par.event!='delete' WHERE p.user_id = :userId GROUP BY p.propertyId ORDER BY p.crd DESC")
    List<PropertyInfoResponse.PropertyInfoBean> getPropertyList(String userId);

    @Query("SELECT property.propertyId, property.propertyName, property.location, property.longitude, property.latitude, property.size, property.sizeUnite, property.description, count(raindata.property_id) as rainCount FROM property as property LEFT JOIN raindata as raindata ON raindata.property_id= property.propertyId WHERE propertyId = :propertyId")
    PropertyDetailResponse.PropertyDetailsBean getPropertyDetail(String propertyId);

    @Query("SELECT parcelId, parcelName,property_id,recordKey,crd,upd  FROM parcel WHERE property_id = :propertyId AND event != 'delete' ORDER BY crd DESC")
        //ORDER BY parcelName ASC
    List<PropertyDetailResponse.PropertyDetailsBean.ParcelBean> getParcelList(String propertyId);

    @Query("SELECT * FROM parcel WHERE isDataSync != 1 ORDER BY crd ASC")
    List<Parcel> getLocalParcelListForSync();

    @Query("SELECT count(a.animalId) as animalCount FROM property as pro JOIN parcel as p ON p.property_id = pro.propertyId JOIN animal as a ON a.parcel_id = p.parcelId WHERE p.property_id = :propertyId")
    String getAnimalCount(String propertyId);

    @Query("SELECT count(crop.cropId) as cropCount FROM property as pro JOIN parcel as p ON p.property_id = pro.propertyId JOIN crop as crop ON crop.parcel_id = p.parcelId WHERE p.property_id = :propertyId")
    String getCropCount(String propertyId);

    @Query("SELECT raindataId, property_id, fromDate, toDate, quantity, quantityUnite FROM raindata WHERE property_id = :propertyId ")
    List<RainBeanResponse.RainRecordListBean> getRainDataList(String propertyId);

    @Query("SELECT raindataId, property_id, fromDate, toDate, quantity, quantityUnite FROM raindata WHERE property_id = :propertyId AND fromDate = :fromDate")
        // LIMIT :offset,10
    List<RainBeanResponse.RainRecordListBean> getFilterRainDataList(String propertyId, String fromDate);

    @Query("SELECT raindataId, property_id, fromDate, toDate, quantity, quantityUnite FROM raindata WHERE property_id = :propertyId AND fromDate >= :fromDate AND toDate <= :toDate AND fromDate <=:toDate ")
    List<RainBeanResponse.RainRecordListBean> getFilterRainDataList(String propertyId, String fromDate, String toDate);
}
