package com.agrinvestapp.data.local.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.agrinvestapp.data.model.db.RainData;

import java.util.List;

/**
 * Created by hemant
 * Date: 10/4/18.
 */

@Dao
public interface RainDataDao {

    @Query("DELETE FROM rainData WHERE raindataId = :rainDataId")
    void delete(String rainDataId);

    @Query("DELETE FROM rainData WHERE property_id = :propertyId")
    void deleteUsingPropertyId(String propertyId);

    @Query("DELETE FROM rainData")
    void deleteAll();

    @Delete
    void delete(RainData rainData);

    @Query("SELECT * FROM raindata WHERE raindataId LIKE :id LIMIT 1")
    RainData findByRainId(String id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(RainData rainData);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<RainData> rainData);

    @Query("SELECT * FROM raindata WHERE event != 'delete'")
    List<RainData> loadAll();

    @Query("SELECT * FROM raindata WHERE event != 'delete' AND raindataId IN (:rainIds)")
    List<RainData> loadAllByIds(List<Integer> rainIds);
}
