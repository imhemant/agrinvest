package com.agrinvestapp.data.local.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;

import com.agrinvestapp.data.local.db.dao.AnimalDao;
import com.agrinvestapp.data.local.db.dao.CalvingDao;
import com.agrinvestapp.data.local.db.dao.CropDao;
import com.agrinvestapp.data.local.db.dao.CropTreatmentDao;
import com.agrinvestapp.data.local.db.dao.CropTreatmentMappingDao;
import com.agrinvestapp.data.local.db.dao.CustomAnimalDao;
import com.agrinvestapp.data.local.db.dao.CustomPropertyDao;
import com.agrinvestapp.data.local.db.dao.ExpenseDao;
import com.agrinvestapp.data.local.db.dao.ExpenseMappingDao;
import com.agrinvestapp.data.local.db.dao.ExpenseProductMappingDao;
import com.agrinvestapp.data.local.db.dao.IncomeStatementDao;
import com.agrinvestapp.data.local.db.dao.InseminationDao;
import com.agrinvestapp.data.local.db.dao.ParcelDao;
import com.agrinvestapp.data.local.db.dao.ParcelMovementDao;
import com.agrinvestapp.data.local.db.dao.PregnancyTestDao;
import com.agrinvestapp.data.local.db.dao.ProductDao;
import com.agrinvestapp.data.local.db.dao.PropertyDao;
import com.agrinvestapp.data.local.db.dao.RainDataDao;
import com.agrinvestapp.data.local.db.dao.TreatmentDao;
import com.agrinvestapp.data.local.db.dao.TreatmentDetailDao;
import com.agrinvestapp.data.local.db.dao.UserDao;
import com.agrinvestapp.data.local.db.dao.WeightDao;
import com.agrinvestapp.data.model.db.Animal;
import com.agrinvestapp.data.model.db.Calving;
import com.agrinvestapp.data.model.db.Crop;
import com.agrinvestapp.data.model.db.CropTreatment;
import com.agrinvestapp.data.model.db.CropTreatmentMapping;
import com.agrinvestapp.data.model.db.Expense;
import com.agrinvestapp.data.model.db.ExpenseMapping;
import com.agrinvestapp.data.model.db.ExpenseProductMapping;
import com.agrinvestapp.data.model.db.Insemination;
import com.agrinvestapp.data.model.db.Parcel;
import com.agrinvestapp.data.model.db.ParcelMovement;
import com.agrinvestapp.data.model.db.PregnancyTest;
import com.agrinvestapp.data.model.db.Product;
import com.agrinvestapp.data.model.db.Property;
import com.agrinvestapp.data.model.db.RainData;
import com.agrinvestapp.data.model.db.Treatment;
import com.agrinvestapp.data.model.db.TreatmentMapping;
import com.agrinvestapp.data.model.db.UserInfoBean;
import com.agrinvestapp.data.model.db.Weight;
import com.agrinvestapp.utils.DateConverter;

/**
 * Created by hemant
 * Date: 10/4/18.
 */

@Database(entities = {UserInfoBean.class, Property.class, RainData.class, Parcel.class, Animal.class,
        Treatment.class, TreatmentMapping.class, Insemination.class, PregnancyTest.class, ParcelMovement.class,
        Calving.class, Weight.class, Crop.class, CropTreatment.class, CropTreatmentMapping.class, Product.class,
        Expense.class, ExpenseMapping.class, ExpenseProductMapping.class},
        version = 1, exportSchema = false)
@TypeConverters({DateConverter.class})
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase mAppDatabase;

    synchronized static AppDatabase getDatabaseInstance(Context context) {
        if (mAppDatabase == null) {
            mAppDatabase = Room.databaseBuilder(context, AppDatabase.class, "AgrinvestDatabase")
                    .fallbackToDestructiveMigration()
                    .build();
        }

        return mAppDatabase;
    }

    public abstract UserDao userDao();

    public abstract PropertyDao propertyDao();

    public abstract RainDataDao rainDataDao();

    public abstract ParcelDao parcelDao();

    public abstract AnimalDao animalDao();

    public abstract CustomPropertyDao customPropertyDao();

    public abstract CustomAnimalDao customAnimalDao();

    public abstract TreatmentDao treatmentDao();

    public abstract TreatmentDetailDao treatmentDetailDao();

    public abstract InseminationDao inseminationDao();

    public abstract PregnancyTestDao pregnancyTestDao();

    public abstract ParcelMovementDao parcelMovementDao();

    public abstract CalvingDao calvingDao();

    public abstract WeightDao weightDao();

    public abstract CropDao cropDao();

    public abstract CropTreatmentDao cropTreatmentDao();

    public abstract CropTreatmentMappingDao cropTreatmentMappingDao();

    public abstract ProductDao productDao();

    public abstract ExpenseDao expenseDao();

    public abstract ExpenseMappingDao expenseMappingDao();

    public abstract ExpenseProductMappingDao expenseProductMappingDao();

    public abstract IncomeStatementDao incomeStatementDao();

}
