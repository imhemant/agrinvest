package com.agrinvestapp.data.local.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.agrinvestapp.data.model.api.CalvingInfoResponse;
import com.agrinvestapp.data.model.db.Calving;
import com.agrinvestapp.data.model.other.CalvingSyncResult;

import java.util.List;

/**
 * Created by hemant
 * Date: 10/4/18.
 */

@Dao
public interface CalvingDao {

    @Query("DELETE FROM calving WHERE calvingId = :calvingId")
    void deleteUsingCalvingId(String calvingId);

    @Query("DELETE FROM calving")
    void deleteAll();

    @Update
    void update(Calving calving);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Calving calving);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Calving> calvings);

    @Query("SELECT * FROM calving WHERE animal_id=:animal_id AND event != 'delete' ORDER BY crd DESC")
    List<CalvingInfoResponse.CalvingListBean> getCalvingList(String animal_id);

    @Query("SELECT * FROM calving WHERE event != 'delete' AND calvingId IN (:calvingIds)")
    List<Calving> loadAllByIds(List<Integer> calvingIds);

    @Query("SELECT animal.recordKey, calving.event as requestType , calving.calvingNumber,  calving.recordKey as calvingRecordKey," +
            " calving.calvingId, calving.calvingDate, calving.isDataSync FROM animal as animal JOIN calving as calving ON calving.animal_id=animal.animalId WHERE calving.isDataSync != 1 GROUP BY animal.animalId")
    List<CalvingSyncResult> getCalvingList();

    @Query("UPDATE calving SET isDataSync = 1")
    void calvingSyncUpdate();
}
