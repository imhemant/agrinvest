package com.agrinvestapp.data.local.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.agrinvestapp.data.model.db.UserInfoBean;

import java.util.List;

/**
 * Created by hemant
 * Date: 10/4/18.
 */

@Dao
public interface UserDao {

    @Delete
    void delete(UserInfoBean user);

    @Query("DELETE FROM users")
    void deleteAll();

    @Query("SELECT * FROM users WHERE name LIKE :name LIMIT 1")
    UserInfoBean findByName(String name);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(UserInfoBean user);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<UserInfoBean> users);

    @Query("SELECT * FROM users")
    List<UserInfoBean> loadAll();

    @Query("SELECT userId FROM users")
    String getExistingUserIdFromDB();

    @Query("SELECT * FROM users WHERE userId IN (:userIds)")
    List<UserInfoBean> loadAllByIds(List<Integer> userIds);
}
