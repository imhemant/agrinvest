package com.agrinvestapp.data.local.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.agrinvestapp.data.model.db.CropTreatmentMapping;

import java.util.List;

/**
 * Created by hemant
 * Date: 10/4/18.
 */

@Dao
public interface CropTreatmentMappingDao {

    @Query("DELETE FROM crop_treatment_mapping WHERE treatmentMappingId = :treatmentMappingId")
    void deleteUsingCropTreatmentMappingId(String treatmentMappingId);

    @Query("DELETE FROM crop_treatment_mapping")
    void deleteAll();

    @Update
    void update(CropTreatmentMapping cropTreatmentMapping);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(CropTreatmentMapping cropTreatmentMapping);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<CropTreatmentMapping> cropTreatmentMappings);

    @Query("UPDATE crop_treatment_mapping SET isDataSync = 1")
    void cropTreatmentMappingSyncUpdate();
}
