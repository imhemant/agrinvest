package com.agrinvestapp.data.local.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.agrinvestapp.data.model.db.CropTreatment;

import java.util.List;

/**
 * Created by hemant
 * Date: 10/4/18.
 */

@Dao
public interface CropTreatmentDao {

    @Query("DELETE FROM crop_treatment WHERE treatmentId = :ctId")
    void deleteUsingCropTreatmentId(String ctId);

    @Query("DELETE FROM crop_treatment")
    void deleteAll();

    @Update
    void update(CropTreatment cropTreatment);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(CropTreatment cropTreatment);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<CropTreatment> cropTreatments);

    @Query("SELECT * from crop_treatment WHERE treatmentId = :treatmentId")
    CropTreatment getCropTreatmentInfo(String treatmentId);

    @Query("UPDATE crop_treatment SET isDataSync = 1")
    void cropTreatmentSyncUpdate();
}
