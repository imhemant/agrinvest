package com.agrinvestapp.data.local.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.agrinvestapp.data.model.db.Property;

import java.util.List;

/**
 * Created by hemant
 * Date: 10/4/18.
 */

@Dao
public interface PropertyDao {

    @Query("DELETE FROM property WHERE propertyId = :propertyId")
    void delete(String propertyId);

    @Query("DELETE FROM property")
    void deleteAll();

    @Query("SELECT * FROM property WHERE propertyId LIKE :id LIMIT 1")
    Property findByPropertyId(String id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Property property);

    @Update
    void update(Property property);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Property> properties);

    @Query("SELECT * FROM property WHERE event != 'delete'")
    List<Property> loadAll();

    @Query("SELECT count(propertyId) FROM property WHERE event != 'delete'")
    String getPropertyCount();

    @Query("SELECT * FROM property WHERE event != 'delete' AND propertyId IN (:propertyIds)")
    List<Property> loadAllByIds(List<Integer> propertyIds);
}
