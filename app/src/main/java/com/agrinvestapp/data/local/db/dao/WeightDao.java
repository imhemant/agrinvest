package com.agrinvestapp.data.local.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.agrinvestapp.data.model.api.WeightInfoResponse;
import com.agrinvestapp.data.model.db.Weight;
import com.agrinvestapp.data.model.other.WeightSyncResult;

import java.util.List;

/**
 * Created by hemant
 * Date: 10/4/18.
 */

@Dao
public interface WeightDao {

    @Query("DELETE FROM weight WHERE weightId = :weightId")
    void deleteUsingWeightId(String weightId);

    @Query("DELETE FROM weight")
    void deleteAll();

    @Update
    void update(Weight weight);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Weight weight);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Weight> weights);

    @Query("SELECT * FROM weight WHERE animal_id=:animal_id AND event != 'delete' ORDER BY crd DESC")
    List<WeightInfoResponse.WeightListBean> getWeightList(String animal_id);

    @Query("SELECT * FROM weight WHERE event != 'delete' AND weightId IN (:weightIds)")
    List<Weight> loadAllByIds(List<Integer> weightIds);

    @Query("SELECT animal.recordKey, weight.event as requestType , weight.weight,  weight.recordKey as weightRecordKey," +
            " weight.weightId, weight.weightDate,weight.weightUnit, weight.isDataSync FROM animal as animal JOIN weight as weight ON weight.animal_id=animal.animalId WHERE weight.isDataSync != 1 GROUP BY animal.animalId")
    List<WeightSyncResult> getWeightList();

    @Query("UPDATE weight SET isDataSync = 1")
    void weightSyncUpdate();
}
