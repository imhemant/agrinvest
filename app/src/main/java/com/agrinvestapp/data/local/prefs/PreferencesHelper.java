package com.agrinvestapp.data.local.prefs;


import android.app.Activity;

import com.agrinvestapp.data.model.db.UserInfoBean;

import java.util.HashMap;

/**
 * Created by hemant
 * Date: 10/4/18.
 */

public interface PreferencesHelper {

    Boolean isLoggedIn();

    UserInfoBean getUserInfo();

    void setUserInfo(UserInfoBean userInfo);

    void setRememberMe(String email, String pwd);

    String[] getRememberMe();

    HashMap<String, String> getHeader();

    Boolean getLocalDataExistForSync();

    void setLocalDataExistForSync(Boolean status);

    Boolean getAppOnline();

    void setAppOnline(Boolean isAppOnline);

    String getSyncTimer();

    void setSyncTimer(String timer);

    void logout(Activity activity);
}
