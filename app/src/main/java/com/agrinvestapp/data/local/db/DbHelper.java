package com.agrinvestapp.data.local.db;


import com.agrinvestapp.data.model.api.AnimalInfoResponse;
import com.agrinvestapp.data.model.api.CalvingInfoResponse;
import com.agrinvestapp.data.model.api.CropInfoResponse;
import com.agrinvestapp.data.model.api.CropTreatmentInfoResponse;
import com.agrinvestapp.data.model.api.ExpenseInfoResponse;
import com.agrinvestapp.data.model.api.InseminationInfoResponse;
import com.agrinvestapp.data.model.api.MotherResponse;
import com.agrinvestapp.data.model.api.ParcelMovementInfoResponse;
import com.agrinvestapp.data.model.api.PregnancyTestInfoResponse;
import com.agrinvestapp.data.model.api.ProductInfoResponse;
import com.agrinvestapp.data.model.api.PropertyDetailResponse;
import com.agrinvestapp.data.model.api.PropertyInfoResponse;
import com.agrinvestapp.data.model.api.RainBeanResponse;
import com.agrinvestapp.data.model.api.TreatmentInfoResponse;
import com.agrinvestapp.data.model.api.WeightInfoResponse;
import com.agrinvestapp.data.model.db.Animal;
import com.agrinvestapp.data.model.db.Calving;
import com.agrinvestapp.data.model.db.Crop;
import com.agrinvestapp.data.model.db.CropTreatment;
import com.agrinvestapp.data.model.db.CropTreatmentMapping;
import com.agrinvestapp.data.model.db.Expense;
import com.agrinvestapp.data.model.db.ExpenseMapping;
import com.agrinvestapp.data.model.db.ExpenseProductMapping;
import com.agrinvestapp.data.model.db.Insemination;
import com.agrinvestapp.data.model.db.Parcel;
import com.agrinvestapp.data.model.db.ParcelMovement;
import com.agrinvestapp.data.model.db.PregnancyTest;
import com.agrinvestapp.data.model.db.Product;
import com.agrinvestapp.data.model.db.Property;
import com.agrinvestapp.data.model.db.RainData;
import com.agrinvestapp.data.model.db.Treatment;
import com.agrinvestapp.data.model.db.TreatmentMapping;
import com.agrinvestapp.data.model.db.UserInfoBean;
import com.agrinvestapp.data.model.db.Weight;
import com.agrinvestapp.data.model.other.AnimalSyncResult;
import com.agrinvestapp.data.model.other.CalvingSyncResult;
import com.agrinvestapp.data.model.other.CommonIdBean;
import com.agrinvestapp.data.model.other.CropSyncResult;
import com.agrinvestapp.data.model.other.CropTreatmentSyncResult;
import com.agrinvestapp.data.model.other.ExpenseProductBean;
import com.agrinvestapp.data.model.other.ExpenseSyncResult;
import com.agrinvestapp.data.model.other.IncomeBean;
import com.agrinvestapp.data.model.other.IncomeOrExpenseApplyOnBean;
import com.agrinvestapp.data.model.other.InseminationSyncResult;
import com.agrinvestapp.data.model.other.MoveToParcelSyncResult;
import com.agrinvestapp.data.model.other.PregnancyTestSyncResult;
import com.agrinvestapp.data.model.other.ProductSyncResult;
import com.agrinvestapp.data.model.other.TreatmentSyncResult;
import com.agrinvestapp.data.model.other.WeightSyncResult;

import java.util.Date;
import java.util.List;


/**
 * Created by hemant
 * Date: 10/4/18.
 */

public interface DbHelper {

    List<UserInfoBean> getAllUsers();

    String getExistingUserIdFromDB();

    Boolean insertUser(UserInfoBean user);

    Boolean deleteUser(UserInfoBean user);

    void deleteAllUser();

    /*Property data start here*/
    void insertProperty(Property property);

    void insertAllProperty(List<Property> properties);

    void updateProperty(Property property);

    void deleteProperty(String propertyId);

    String getPropertyCount();

    void deleteAllProperty();

    void insertRainData(RainData rainData);

    void insertAllRainData(List<RainData> rainData);

    void deleteRainData(String rainDataID);

    void deleteRainData(RainData rainData);

    void deleteRainDataUsingPropertyId(String propertyId);

    void deleteAllRainData();

    void insertParcel(Parcel parcel);

    void insertAllParcel(List<Parcel> parcels);

    List<Parcel> getLocalParcelListForSync();

    List<Parcel> loadAllParcel();

    List<Parcel> loadAllParcelById(String parcelId);

    void parcelSyncUpdate();

    void updateParcel(Parcel parcel);

    Parcel findByParcelName(String userId, String parcelName);

    Parcel getParcel(String parcelRk);

    List<CommonIdBean> getPropertyParcel(String propertyId);

    void deleteUsingParcelId(String parcelId);

    void deleteParcelUsingPropertyId(String propertyId);

    void deleteAllParcel();

    List<PropertyInfoResponse.PropertyInfoBean> getPropertyList(String userId);

    PropertyDetailResponse.PropertyDetailsBean getPropertyDetail(String propertyId);

    List<PropertyDetailResponse.PropertyDetailsBean.ParcelBean> getParcelList(String propertyId);

    String getAnimalCount(String propertyId);

    String getCropCount(String propertyId);

    List<RainBeanResponse.RainRecordListBean> getRainDataList(String propertyId);

    List<RainBeanResponse.RainRecordListBean> getFilterRainDataList(String propertyId, String fromDate);

    List<RainBeanResponse.RainRecordListBean> getFilterRainDataList(String propertyId, String fromDate, String toDate);
    /*Property data end here*/

    /*Animal data start here*/
    void insertAnimal(Animal animal);

    void insertAllAnimal(List<Animal> animals);

    void updateAnimal(Animal animal);

    void deleteUsingAnimalId(String animalId);

    void deleteAnimalUsingParcelId(String parcel_id);

    void deleteCropUsingParcelId(String parcel_id);

    void deleteAllAnimal();

    List<AnimalInfoResponse.AnimalListBean> getAnimalList(String userId);

    List<AnimalInfoResponse.AnimalListBean> getPropertyAnimalList(String propertyId);

    List<AnimalInfoResponse.AnimalListBean> getParcelAnimalList(String parcelId);

    List<MotherResponse.AnimalMotherListBean> getAnimalMotherList(String userId, String animalId);

    String getTagNumber(String userId, String tagNumber);

    List<AnimalSyncResult> getLocalAnimalListForSync();

    void animalSyncUpdate();

    List<CommonIdBean> getAnimals(String parcelId);

    List<CommonIdBean> getAnimals(String parcelId, String recordKey);

    String getAnimalCount();

    /*Treatment data start here*/
    void insertTreatment(Treatment treatment);

    void insertAllTreatment(List<Treatment> treatments);

    void updateTreatment(Treatment treatment);

    void deleteUsingTreatmentId(String treatmentId);

    void deleteAllTreatment();

    void insertTreatmentDetail(TreatmentMapping treatment_mapping);

    void insertAllTreatmentDetail(List<TreatmentMapping> treatment_mappings);

    void updateTreatmentDetail(TreatmentMapping treatment_mapping);

    void deleteUsingTreatmentDetailMappingId(String treatmentMappingId);

    void deleteAllTreatmentDetail();

    List<TreatmentInfoResponse.TreatmentDetailBean> getTreatmentList(String animalId);

    List<TreatmentInfoResponse.TreatmentDetailBean.TreatmentListBean> getTreatmentDetailList(String treatment_id);

    Animal getAnimalUsingAnimalId(String animalId);

    List<Animal> getAnimalUsingAnimalLotId(String lotId);

    List<TreatmentSyncResult> getManageTreatmentList();

    List<TreatmentSyncResult> getManageAllTreatmentList();

    List<TreatmentSyncResult.TreatmentListDataBean> getManageTreatmentDetailList();

    List<TreatmentSyncResult.TreatmentListDataBean> getManageTreatmentDetailList(String treatment_id);

    void treatmentSyncUpdate();

    Treatment getTreatmentInfo(String treatmentId);

    void treatmentMappingSyncUpdate();
    /*Treatment data end here*/

    /*Insemination data start here*/

    void insertInsemination(Insemination insemination);

    void insertAllInsemination(List<Insemination> inseminations);

    void updateInsemination(Insemination insemination);

    List<InseminationInfoResponse.InseminationListBean> getInseminationList(String animalId);

    List<InseminationSyncResult> getInseminationList();

    void inseminationSyncUpdate();

    void deleteUsingInseminationId(String inseminationId);

    void deleteAllInsemination();

    /*Insemination data end here*/

    /*PregnancyTest data start here*/

    void insertPregnancyTest(PregnancyTest pregnancyTest);

    void insertAllPregnancyTest(List<PregnancyTest> pregnancyTests);

    void updatePregnancyTest(PregnancyTest pregnancyTest);

    List<PregnancyTestInfoResponse.PregnancyListBean> getPregnancyTestList(String animalId);

    List<PregnancyTestSyncResult> getPregnancyTestList();

    void pregnancyTestSyncUpdate();

    void deleteUsingPregnancyTestId(String pregnancyTestId);

    void deleteAllPregnancyTest();

    /*PregnancyTest data end here*/

    /*ParcelMovement data start here*/

    void insertParcelMovement(ParcelMovement parcelMovement);

    void insertAllParcelMovement(List<ParcelMovement> parcelMovements);

    void updateParcelMovement(ParcelMovement parcelMovement);

    List<ParcelMovementInfoResponse.ParcelMovementListBean> getParcelMovementList(String animalId);

    List<MoveToParcelSyncResult> getMToParcelList();

    void parcelMoveSyncUpdate();

    void deleteUsingParcelMovementId(String parcelMovementId);

    void deleteAllParcelMovement();

    /*ParcelMovement data end here*/

    /*Calving data start here*/

    void insertCalving(Calving calving);

    void insertAllCalving(List<Calving> calvings);

    void updateCalving(Calving calving);

    List<CalvingInfoResponse.CalvingListBean> getCalvingList(String animalId);

    List<CalvingSyncResult> getCalvingList();

    void calvingSyncUpdate();

    void deleteUsingCalvingId(String calvingId);

    void deleteAllCalving();

    /*Calving data end here*/

    /*Weight data start here*/

    void insertWeight(Weight weight);

    void insertAllWeight(List<Weight> weights);

    void updateWeight(Weight weight);

    List<WeightInfoResponse.WeightListBean> getWeightList(String animalId);

    List<WeightSyncResult> getWeightList();

    void weightSyncUpdate();

    void deleteUsingWeightId(String weightId);

    void deleteAllWeight();
    /*Weight data end here*/

    /*Animal data end here*/

    /*Crop data start here*/

    void insertCrop(Crop crop);

    void insertAllCrop(List<Crop> crops);

    void updateCrop(Crop crop);

    void deleteUsingCropId(String cropId);

    void deleteAllCrop();

    void cropSyncUpdate();

    List<CommonIdBean> getCrops(String parcelId);

    List<CommonIdBean> getCrops(String parcelId, String recordKey);

    /*Crop Treatment data start here*/
    void insertCropTreatment(CropTreatment treatment);

    void insertAllCropTreatment(List<CropTreatment> treatments);

    void updateCropTreatment(CropTreatment treatment);

    void deleteUsingCropTreatmentId(String treatmentId);

    void deleteAllCropTreatment();

    void insertCropTreatmentDetail(CropTreatmentMapping treatment_mapping);

    void insertAllCropTreatmentDetail(List<CropTreatmentMapping> treatment_mappings);

    void updateCropTreatmentDetail(CropTreatmentMapping treatment_mapping);

    void deleteUsingCropTreatmentDetailMappingId(String treatmentMappingId);

    void deleteAllCropTreatmentDetail();

    List<CropTreatmentInfoResponse.TreatmentDetailListBean> getCropTreatmentList(String cropId);

    List<CropTreatmentInfoResponse.TreatmentDetailListBean.TreatmentListBean> getCropTreatmentDetailList(String treatment_id);

    String getCropRK(String cropId);

    Crop getCrop(String id);

    List<Crop> getCropUsingCropLotId(String lotId);

    List<CropSyncResult> getCropList(String userId);

    CropTreatment getCropTreatmentInfo(String treatmentId);

    List<CropTreatment> getManageCropTreatmentList();

    List<CropTreatment> getManageAllCropTreatmentList();

    List<CropTreatmentSyncResult.TreatmentListDataBean> getManageCropTreatmentDetailList(String treatment_id);

    void cropTreatmentSyncUpdate();

    void cropTreatmentMappingSyncUpdate();
    /*Crop Treatment data end here*/

    List<CropInfoResponse.CroplListBean> getPropertyCropList();

    List<CropInfoResponse.CroplListBean> getPropertyCropList(String propertyId);

    List<CropInfoResponse.CroplListBean> getParcelCropList(String parcelId);

    /*Crop data end here*/

    /*Product data start here*/
    List<ExpenseProductBean> getProductList();

    void deleteUsingProductId(String productId);

    void deleteAllProduct();

    void updateProduct(Product product);

    void insertProduct(Product product);

    void insertAllProduct(List<Product> products);

    List<ProductInfoResponse.ProductListBean> getAllProductList();

    List<ProductInfoResponse.ProductListBean> getMyProductList(String userType);

    void productSyncUpdate();

    /*Product Filter start here*/
    //All product filter

    List<ProductInfoResponse.ProductListBean> productFilterAll(String prodName, String minPrice, String maxPrice, String qty);

    List<ProductInfoResponse.ProductListBean> productFilterAllName(String prodName);

    List<ProductInfoResponse.ProductListBean> productFilterAllPrice(String minPrice, String maxPrice);

    List<ProductInfoResponse.ProductListBean> productFilterAllQty(String qty);

    List<ProductInfoResponse.ProductListBean> productFilterAllNameAndPrice(String prodName, String minPrice, String maxPrice);

    List<ProductInfoResponse.ProductListBean> productFilterAllNameAndQty(String prodName, String qty);

    List<ProductInfoResponse.ProductListBean> productFilterAllQtyAndPrice(String qty, String minPrice, String maxPrice);

    //My product filter

    List<ProductInfoResponse.ProductListBean> productMyFilter(String userId, String prodName, String minPrice, String maxPrice, String qty);

    List<ProductInfoResponse.ProductListBean> productMyFilterName(String userId, String prodName);

    List<ProductInfoResponse.ProductListBean> productMyFilterPrice(String userId, String minPrice, String maxPrice);

    List<ProductInfoResponse.ProductListBean> productMyFilterQty(String userId, String qty);

    List<ProductInfoResponse.ProductListBean> productMyFilterNameAndPrice(String userId, String prodName, String minPrice, String maxPrice);

    List<ProductInfoResponse.ProductListBean> productMyFilterNameAndQty(String userId, String prodName, String qty);

    List<ProductInfoResponse.ProductListBean> productMyFilterQtyAndPrice(String userId, String qty, String minPrice, String maxPrice);
    /*Product Filter end here*/

    List<ProductSyncResult> getManageProductList();
    /*Product data end here*/

    /*Expense data start here*/
    void insertExpense(Expense expense);

    void insertAllExpense(List<Expense> expenses);

    void updateExpense(Expense expense);

    void deleteAllExpense();

    void deleteUsingExpenseId(String expenseId);

    void expenseSyncUpdate();

    /*ExpenseMapping data start here*/
    void insertExpenseMapping(ExpenseMapping expenseMapping);

    void insertAllExpenseMapping(List<ExpenseMapping> expenseMappings);

    void updateAllExpenseMapping(List<ExpenseMapping> expenseMapping);

    void updateExpenseMapping(ExpenseMapping expenseMapping);

    void deleteAllExpenseMapping();

    void deleteUsingExpenseMappingId(String expenseMappingId);

    void deleteExpenseMappingUsingExpenseId(String expenseId);

    void expenseMappingSyncUpdate();
    /*ExpenseMapping data end here*/

    /*ExpenseProductMapping data start here*/
    void insertExpenseProdMapping(ExpenseProductMapping expenseProductMapping);

    void insertAllExpenseProdMapping(List<ExpenseProductMapping> expenseProductMappings);

    void updateAllExpenseProdMapping(List<ExpenseProductMapping> expenseProductMapping);

    void updateExpenseProdMapping(ExpenseProductMapping expenseProductMapping);

    void deleteAllExpenseProdMapping();

    void deleteUsingExpenseProdMappingId(String expenseProductMappingId);

    void deleteExpenseProdMappingUsingExpenseId(String expenseId);

    void expenseProductMappingSyncUpdate();
    /*ExpenseProductMapping data end here*/

    List<IncomeOrExpenseApplyOnBean> getAnimalApplyOnInfo();

    List<IncomeOrExpenseApplyOnBean> getCropApplyOnInfo();

    List<IncomeOrExpenseApplyOnBean> getAnimalLotApplyOnInfo();

    List<IncomeOrExpenseApplyOnBean> getCropLotApplyOnInfo();

    List<IncomeOrExpenseApplyOnBean> getParcelApplyOnInfo();

    List<IncomeOrExpenseApplyOnBean> getPropertyApplyOnInfo();

    List<ExpenseInfoResponse.ExpenseListBean> getExpenseList(String expenseFor);

    List<ExpenseInfoResponse.ExpenseListBean.AppliedOnBean> getAnimalExpenseAppliedOnList(String expenseId);

    List<ExpenseInfoResponse.ExpenseListBean.AppliedOnBean> getParcelExpenseAppliedOnList(String expenseId);

    List<ExpenseInfoResponse.ExpenseListBean.AppliedOnBean> getPropertyExpenseAppliedOnList(String expenseId);

    List<ExpenseInfoResponse.ExpenseListBean.AppliedOnBean> getCropExpenseAppliedOnList(String expenseId);

    List<ExpenseInfoResponse.ExpenseListBean.AppliedOnBean> getCropLotExpenseAppliedOnList(String expenseId);

    List<ExpenseInfoResponse.ExpenseListBean.AppliedOnBean> getAnimalLotExpenseAppliedOnList(String expenseId);

    List<ExpenseInfoResponse.ExpenseListBean.ProductDetailsBean> getExpenseProductList(String expenseId);

    ExpenseProductMapping getExpenseProductDetail(String expense_id, String product_id);

    String getExpenseApplyStatus(String id);

    String getExpenseApplyStatus(String id, String recordKey);

    String getProductExpenseApplyStatus(String id, String recordKey);

    //Expense Sync
    List<ExpenseSyncResult> getManageExpenseList();

    List<ExpenseSyncResult.AppliedOnDetail> getManageAnimalExpenseAppliedOnList(String expenseId);

    List<ExpenseSyncResult.AppliedOnDetail> getManageParcelExpenseAppliedOnList(String expenseId);

    List<ExpenseSyncResult.AppliedOnDetail> getManagePropertyExpenseAppliedOnList(String expenseId);

    List<ExpenseSyncResult.AppliedOnDetail> getManageCropExpenseAppliedOnList(String expenseId);

    List<ExpenseSyncResult.AppliedOnDetail> getManageCropLotExpenseAppliedOnList(String expenseId);

    List<ExpenseSyncResult.AppliedOnDetail> getManageAnimalLotExpenseAppliedOnList(String expenseId);

    List<ExpenseSyncResult.ProductDetail> getManageExpenseProductList(String expenseId);
    /*Expense data end here*/

    /*Income Statement Start here*/
    /*Property Expense Start Here*/
    List<String> getExpenseIdFromExpenseMapping(String appliedId);

    List<String> getExpenseIdFromExpenseMapping(String appliedId, String recordKey);

    String getExpenseCostFromExpense(String expenseId, String expenseFor);

    String getExpenseCostFromExpense(String expenseId, String expenseFor, Date fromDate);

    String getExpenseCostFromExpense(String expenseId, String expenseFor, Date fromDate, Date toDate);

    String getExpenseCountFromExpenseMapping(String expenseId);
    /*Property Expense End Here*/

    /*Parcel Expense Start here*/
    List<IncomeBean> getPropertyExpenseFromParcel(String propertyId);

    String getParcelCountUsingProperty(String propertyId);

    IncomeBean getParcelIdFromAnimal(String animalId);
    /*Parcel Expense End here*/

    /*Animal Expense Start here*/
    List<IncomeBean> getAnimalIdUsingParcel(String parcelId, String recordKey);

    String getTtlSaleFromAnimal(String animalId);

    String getTtlSaleFromAnimal(String animalId, Date fromDate);

    String getTtlSaleFromAnimal(String animalId, Date fromDate, Date toDate);
    /*Animal Expense End here*/

    /*Animal Lot Expense Start here*/
    String getAnimalLotIdUsingAnimal(String animalId);

    String getAnimalCountUsingAnimalLotId(String lotId);

    String getAnimalCountUsingParcel(String parcelId);
    /*Animal Lot Expense End here*/

    /*Crop Expense Start here*/
    List<IncomeBean> getCropIdUsingParcel(String parcelId, String recordKey);

    String getTtlSaleFromCrop(String cropId);

    String getTtlSaleFromCrop(String cropId, Date fromDate);


    String getTtlSaleFromCrop(String cropId, Date fromDate, Date toDate);
    /*Crop Expense End here*/

    /*Crop Lot Expense Start here*/
    String getCropLotIdUsingCrop(String cropId);

    String getCropCountUsingCropLotId(String lotId);

    String getCropCountUsingParcel(String parcelId);
    /*Crop Lot Expense End here*/

    /*Income Statement End here*/

    void clearAllTable();
}
