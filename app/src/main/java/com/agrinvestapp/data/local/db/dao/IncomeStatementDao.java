package com.agrinvestapp.data.local.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.agrinvestapp.data.model.other.IncomeBean;

import java.util.Date;
import java.util.List;

/**
 * Created by hemant
 * Date: 10/4/18.
 */

@Dao
public interface IncomeStatementDao {

    /*Property Expense Start Here*/
    @Query("SELECT expense_id FROM expense_mapping Where (appliedOnId =:appliedId OR appliedOnId =:recordKey) AND event != 'delete'")
    List<String> getExpenseIdFromExpenseMapping(String appliedId, String recordKey);

    @Query("SELECT expense_id FROM expense_mapping Where appliedOnId =:appliedId AND event != 'delete'")
    List<String> getExpenseIdFromExpenseMapping(String appliedId);

    @Query("SELECT cost FROM expense Where (expenseId =:expenseId OR recordKey =:expenseId) AND expenseFor =:expenseFor AND event != 'delete'")
    String getExpenseCostFromExpense(String expenseId, String expenseFor);

    @Query("SELECT cost FROM expense Where (expenseId =:expenseId OR recordKey =:expenseId) AND expenseFor =:expenseFor AND dateApplied <=:fromDate AND event != 'delete'")
    String getExpenseCostFromExpense(String expenseId, String expenseFor, Date fromDate);

    @Query("SELECT cost FROM expense Where (expenseId =:expenseId OR recordKey =:expenseId) AND expenseFor =:expenseFor AND (dateApplied BETWEEN :fromDate AND :toDate) AND event != 'delete'")
    String getExpenseCostFromExpense(String expenseId, String expenseFor, Date fromDate, Date toDate);

    @Query("SELECT count(expense_id) FROM expense_mapping Where expense_id =:expenseId AND event != 'delete'")
    String getExpenseCountFromExpenseMapping(String expenseId);
    /*Property Expense End Here*/

    /*Parcel Expense Start here*/
    @Query("SELECT parcelId as id, recordKey FROM parcel WHERE property_id =:propertyId AND event != 'delete'")
    List<IncomeBean> getPropertyExpenseFromParcel(String propertyId);

    @Query("SELECT count(property_id) FROM parcel WHERE property_id =:propertyId AND event != 'delete'")
    String getParcelCountUsingProperty(String propertyId);

    @Query("SELECT parcel_id as id, recordKey FROM animal WHERE animalId =:animalId AND event != 'delete'")
    IncomeBean getParcelIdFromAnimal(String animalId);
    /*Parcel Expense End here*/

    /*Animal Expense Start here*/
    @Query("SELECT animalId as id, recordKey FROM animal WHERE (parcel_id =:parcelId OR parcel_id =:recordKey) AND event != 'delete'")
    List<IncomeBean> getAnimalIdUsingParcel(String parcelId, String recordKey);
    /*Animal Expense End here*/

    /*Animal Lot Expense Start here*/
    @Query("SELECT lotId FROM animal WHERE (animalId =:animalId OR recordKey =:animalId) AND event != 'delete'")
    String getAnimalLotIdUsingAnimal(String animalId);

    @Query("SELECT count(lotId) FROM animal WHERE lotId =:lotId AND event != 'delete'")
    String getAnimalCountUsingAnimalLotId(String lotId);

    @Query("SELECT count(parcel_id) FROM animal WHERE parcel_id =:parcelId AND event != 'delete'")
    String getAnimalCountUsingParcel(String parcelId);

    /*Animal Lot Expense End here*/

    /*Crop Expense Start here*/
    @Query("SELECT cropId as id, recordKey FROM crop WHERE (parcel_id =:parcelId OR parcel_id =:recordKey) AND event != 'delete'")
    List<IncomeBean> getCropIdUsingParcel(String parcelId, String recordKey);
    /*Crop Expense End here*/

    /*Crop Lot Expense Start here*/
    @Query("SELECT lotId FROM crop WHERE (cropId =:cropId OR recordKey =:cropId) AND event != 'delete'")
    String getCropLotIdUsingCrop(String cropId);

    @Query("SELECT count(lotId) FROM crop WHERE lotId =:lotId AND event != 'delete'")
    String getCropCountUsingCropLotId(String lotId);

    @Query("SELECT count(parcel_id) FROM crop WHERE parcel_id =:parcelId AND event != 'delete'")
    String getCropCountUsingParcel(String parcelId);
    /*Crop Lot Expense End here*/

    /*Sale Price for Animal Start here*/
    @Query("SELECT saleTotalPrice from animal where (animalId =:animalId OR recordKey =:animalId) AND event != 'delete'")
    String getTtlSaleFromAnimal(String animalId);

    @Query("SELECT saleTotalPrice FROM animal Where (animalId =:animalId OR recordKey =:animalId) AND saleDate <=:fromDate AND event != 'delete'")
    String getTtlSaleFromAnimal(String animalId, Date fromDate);

    @Query("SELECT saleTotalPrice FROM animal Where (animalId =:animalId OR recordKey =:animalId) AND (saleDate BETWEEN :fromDate AND :toDate) AND event != 'delete'")
    String getTtlSaleFromAnimal(String animalId, Date fromDate, Date toDate);
    /*Sale Price for Animal End here*/

    /*Sale Price for Crop Start here*/
    @Query("SELECT saleTotalPrice from crop where (cropId =:cropId OR recordKey =:cropId) AND event != 'delete'")
    String getTtlSaleFromCrop(String cropId);

    @Query("SELECT saleTotalPrice FROM crop Where (cropId =:cropId OR recordKey =:cropId) AND saleDate <=:fromDate AND event != 'delete'")
    String getTtlSaleFromCrop(String cropId, Date fromDate);

    @Query("SELECT saleTotalPrice FROM crop Where (cropId =:cropId OR recordKey =:cropId) AND (saleDate BETWEEN :fromDate AND :toDate) AND event != 'delete'")
    String getTtlSaleFromCrop(String cropId, Date fromDate, Date toDate);
    /*Sale Price for Crop End here*/
}
