package com.agrinvestapp.data.local.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.agrinvestapp.data.model.db.Treatment;

import java.util.List;

/**
 * Created by hemant
 * Date: 10/4/18.
 */

@Dao
public interface TreatmentDao {

    @Query("DELETE FROM treatment WHERE treatmentId = :treatmentId")
    void deleteUsingTreatmentId(String treatmentId);

    @Query("DELETE FROM treatment")
    void deleteAll();

    @Update
    void update(Treatment treatment);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Treatment treatment);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Treatment> treatments);

    @Query("SELECT * FROM treatment WHERE event != 'delete'")
    List<Treatment> loadAll();

    @Query("SELECT * FROM treatment WHERE event != 'delete' AND treatmentId IN (:treatmentIds)")
    List<Treatment> loadAllByIds(List<Integer> treatmentIds);

    @Query("UPDATE treatment SET isDataSync = 1")
    void treatmentSyncUpdate();

    @Query("SELECT * from treatment WHERE treatmentId = :treatmentId")
    Treatment getTreatmentInfo(String treatmentId);
}
