package com.agrinvestapp.data.local.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.agrinvestapp.data.model.db.Animal;
import com.agrinvestapp.data.model.other.CommonIdBean;

import java.util.List;

/**
 * Created by hemant
 * Date: 10/4/18.
 */

@Dao
public interface AnimalDao {

    @Query("DELETE FROM animal WHERE animalId = :animalId")
    void deleteUsingAnimalId(String animalId);

    @Query("DELETE FROM animal WHERE parcel_id = :parcel_id")
    void deleteAnimalUsingParcelId(String parcel_id);

    @Query("DELETE FROM animal")
    void deleteAll();

    @Update
    void update(Animal animal);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Animal animal);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Animal> animals);

    @Query("SELECT * FROM animal Where event != 'delete'")
    List<Animal> loadAll();

    @Query("SELECT * FROM animal WHERE event != 'delete' AND animalId IN (:animalIds)")
    List<Animal> loadAllByIds(List<Integer> animalIds);

    @Query("SELECT count(animalId) FROM animal WHERE event != 'delete'")
    String getAnimalCount();

    @Query("UPDATE animal SET isDataSync = 1")
    void animalSyncUpdate();

    @Query("SELECT animalId as id, recordKey FROM animal Where parcel_id =:parcelId AND event != 'delete'")
    List<CommonIdBean> getAnimals(String parcelId);

    @Query("SELECT animalId as id, recordKey FROM animal Where (parcel_id =:parcelId OR parcel_id =:recordKey) AND event != 'delete'")
    List<CommonIdBean> getAnimals(String parcelId, String recordKey);
}
