package com.agrinvestapp.data.local.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.agrinvestapp.data.model.api.ExpenseInfoResponse;
import com.agrinvestapp.data.model.db.Expense;
import com.agrinvestapp.data.model.db.ExpenseProductMapping;
import com.agrinvestapp.data.model.other.ExpenseSyncResult;
import com.agrinvestapp.data.model.other.IncomeOrExpenseApplyOnBean;

import java.util.List;

/**
 * Created by hemant
 * Date: 10/4/18.
 */

@Dao
public interface ExpenseDao {

    @Query("DELETE FROM expense WHERE expenseId = :expenseId")
    void deleteUsingExpenseId(String expenseId);

    @Query("DELETE FROM expense")
    void deleteAll();

    @Update
    void update(Expense expense);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Expense expense);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Expense> expense);

    @Query("SELECT * FROM expense Where event != 'delete'")
    List<Expense> loadAll();

    @Query("SELECT * FROM expense WHERE event != 'delete' AND expenseId IN (:expenseId)")
    List<Expense> loadAllByIds(List<Integer> expenseId);

    @Query("UPDATE expense SET isDataSync = 1")
    void expenseSyncUpdate();

    @Query("SELECT tagNumber as name, recordKey FROM animal ORDER BY tagNumber")
    List<IncomeOrExpenseApplyOnBean> getAnimalApplyOnInfo();

    @Query("SELECT cropName as name, recordKey FROM crop ORDER BY cropName")
    List<IncomeOrExpenseApplyOnBean> getCropApplyOnInfo();

    @Query("SELECT Distinct(lotId) as name, lotId as recordKey FROM animal WHERE event != 'delete' ORDER BY lotId ")
    List<IncomeOrExpenseApplyOnBean> getAnimalLotApplyOnInfo();

    @Query("SELECT Distinct(lotId) as name, lotId as recordKey FROM crop WHERE event != 'delete' ORDER BY lotId")
    List<IncomeOrExpenseApplyOnBean> getCropLotApplyOnInfo();

    @Query("SELECT parcelName as name,recordKey FROM parcel ORDER BY parcelName")
    List<IncomeOrExpenseApplyOnBean> getParcelApplyOnInfo();

    @Query("SELECT propertyName as name,propertyId as recordKey FROM property ORDER BY propertyName")
    List<IncomeOrExpenseApplyOnBean> getPropertyApplyOnInfo();

    @Query("SELECT expense.expenseId, expense.user_id, expense.dateBought, expense.cost, expense.description, expense.dateApplied," +
            "expense.expenseFor, expense.recordKey, expense.status, expense.crd, expense.upd, count(expenseMappingProduct.expense_id) as productCount, product.productName, expenseMapping.appliedOn as applyOnName\n" +
            "FROM expense as expense\n" +
            "LEFT JOIN expense_mapping as expenseMapping ON expenseMapping.expense_id= expense.expenseId\n" +
            "LEFT JOIN expense_product_mapping as expenseMappingProduct ON expenseMappingProduct.expense_id= expense.expenseId\n" +
            "LEFT JOIN product as product ON product.productId = expenseMappingProduct.product_id\n" +
            "WHERE expense.expenseFor = :expenseFor AND expense.event != 'delete' GROUP BY expense.expenseId\n" +
            "ORDER BY expense.crd DESC")
    List<ExpenseInfoResponse.ExpenseListBean> getExpenseList(String expenseFor);

    @Query("SELECT animal.tagNumber as appliedName, animal.recordKey as appliedOnRecordKey, expenseMapping.*" +
            "FROM expense_mapping as expenseMapping\n" +
            "JOIN animal as animal ON animal.animalId = expenseMapping.appliedOnId OR animal.recordKey = expenseMapping.appliedOnId\n" +
            "WHERE expenseMapping.expense_id = :expenseId AND expenseMapping.event != 'delete'")
    List<ExpenseInfoResponse.ExpenseListBean.AppliedOnBean> getAnimalExpenseAppliedOnList(String expenseId);

    @Query("SELECT parcel.parcelName as appliedName, parcel.recordKey as appliedOnRecordKey, expenseMapping.*\n" +
            "FROM expense_mapping as expenseMapping\n" +
            "JOIN parcel as parcel ON parcel.parcelId = expenseMapping.appliedOnId OR parcel.recordKey = expenseMapping.appliedOnId\n" +
            "WHERE expenseMapping.expense_id = :expenseId AND expenseMapping.event != 'delete'")
    List<ExpenseInfoResponse.ExpenseListBean.AppliedOnBean> getParcelExpenseAppliedOnList(String expenseId);

    @Query("SELECT property.propertyName as appliedName, property.propertyId as appliedOnRecordKey, expenseMapping.*\n" +
            "FROM expense_mapping as expenseMapping\n" +
            "JOIN property as property ON property.propertyId = expenseMapping.appliedOnId\n" +
            "WHERE expenseMapping.expense_id = :expenseId AND expenseMapping.event != 'delete'")
    List<ExpenseInfoResponse.ExpenseListBean.AppliedOnBean> getPropertyExpenseAppliedOnList(String expenseId);

    @Query("SELECT crop.cropName as appliedName, crop.recordKey as appliedOnRecordKey, expenseMapping.*\n" +
            "FROM expense_mapping as expenseMapping\n" +
            "JOIN crop as crop ON crop.cropId = expenseMapping.appliedOnId OR crop.recordKey = expenseMapping.appliedOnId\n" +
            "WHERE expenseMapping.expense_id = :expenseId AND expenseMapping.event != 'delete'")
    List<ExpenseInfoResponse.ExpenseListBean.AppliedOnBean> getCropExpenseAppliedOnList(String expenseId);

    @Query("SELECT Distinct(crop.lotId) as appliedName, crop.lotId as appliedOnRecordKey, expenseMapping.*\n" +
            "FROM expense_mapping as expenseMapping\n" +
            "JOIN crop as crop ON crop.lotId = expenseMapping.appliedOnId\n" +
            "WHERE expenseMapping.expense_id = :expenseId AND expenseMapping.event != 'delete'")
    List<ExpenseInfoResponse.ExpenseListBean.AppliedOnBean> getCropLotExpenseAppliedOnList(String expenseId);

    @Query("SELECT Distinct(animal.lotId) as appliedName, animal.lotId as appliedOnRecordKey, expenseMapping.*\n" +
            "FROM expense_mapping as expenseMapping\n" +
            "JOIN animal as animal ON animal.lotId = expenseMapping.appliedOnId\n" +
            "WHERE expenseMapping.expense_id = :expenseId AND expenseMapping.event != 'delete'")
    List<ExpenseInfoResponse.ExpenseListBean.AppliedOnBean> getAnimalLotExpenseAppliedOnList(String expenseId);

    @Query("SELECT product.productName, product.recordKey as productRecordKey," +
            "expenseMappingProduct.* FROM expense_product_mapping as expenseMappingProduct\n" +
            "JOIN product as product ON product.productId = expenseMappingProduct.product_id OR product.recordKey = expenseMappingProduct.product_id\n" +
            "WHERE expenseMappingProduct.expense_id = :expenseId AND expenseMappingProduct.event != 'delete'")
    List<ExpenseInfoResponse.ExpenseListBean.ProductDetailsBean> getExpenseProductList(String expenseId);

    @Query("SELECT * FROM expense_product_mapping WHERE expense_id =:expense_id AND product_id =:product_id AND event != 'delete'")
    ExpenseProductMapping getExpenseProductDetail(String expense_id, String product_id);

    //is expense apply on property, animal, crop...
    @Query("SELECT appliedOnId FROM expense_mapping WHERE appliedOnId =:id AND event != 'delete'")
    String getExpenseApplyStatus(String id);

    @Query("SELECT appliedOnId FROM expense_mapping WHERE (appliedOnId =:id OR appliedOnId =:recordKey) AND event != 'delete'")
    String getExpenseApplyStatus(String id, String recordKey);

    //is expense apply on product...
    @Query("SELECT product_id FROM expense_product_mapping WHERE (product_id =:id OR product_id =:recordKey) AND event != 'delete'")
    String getProductExpenseApplyStatus(String id, String recordKey);

    //Sync Query
    @Query("SELECT expense.expenseId, expense.event as requestType, expense.dateBought, expense.cost, expense.description, expense.dateApplied," +
            "expense.expenseFor, expense.recordKey, expenseMapping.appliedOn  FROM expense as expense\n" +
            "LEFT JOIN expense_mapping as expenseMapping ON expenseMapping.expense_id= expense.expenseId\n" +
            "LEFT JOIN expense_product_mapping as expenseMappingProduct ON expenseMappingProduct.expense_id= expense.expenseId\n" +
            "LEFT JOIN product as product ON product.productId = expenseMappingProduct.product_id\n" +
            "WHERE expense.isDataSync != 1 GROUP BY expense.expenseId\n" +
            "ORDER BY expense.expenseId DESC")
    List<ExpenseSyncResult> getManageExpenseList();

    @Query("SELECT expenseMapping.appliedOnId FROM expense_mapping as expenseMapping\n" +
            "JOIN animal as animal ON animal.animalId = expenseMapping.appliedOnId OR animal.recordKey = expenseMapping.appliedOnId\n" +
            "WHERE expenseMapping.expense_id = :expenseId AND expenseMapping.isDataSync != 1")
    List<ExpenseSyncResult.AppliedOnDetail> getManageAnimalExpenseAppliedOnList(String expenseId);

    @Query("SELECT expenseMapping.appliedOnId FROM expense_mapping as expenseMapping\n" +
            "JOIN parcel as parcel ON parcel.parcelId = expenseMapping.appliedOnId OR parcel.recordKey = expenseMapping.appliedOnId\n" +
            "WHERE expenseMapping.expense_id = :expenseId AND expenseMapping.isDataSync != 1")
    List<ExpenseSyncResult.AppliedOnDetail> getManageParcelExpenseAppliedOnList(String expenseId);

    @Query("SELECT expenseMapping.appliedOnId FROM expense_mapping as expenseMapping\n" +
            "JOIN property as property ON property.propertyId = expenseMapping.appliedOnId\n" +
            "WHERE expenseMapping.expense_id = :expenseId AND expenseMapping.isDataSync != 1")
    List<ExpenseSyncResult.AppliedOnDetail> getManagePropertyExpenseAppliedOnList(String expenseId);

    @Query("SELECT expenseMapping.appliedOnId FROM expense_mapping as expenseMapping\n" +
            "JOIN crop as crop ON crop.cropId = expenseMapping.appliedOnId OR crop.recordKey = expenseMapping.appliedOnId\n" +
            "WHERE expenseMapping.expense_id = :expenseId AND expenseMapping.isDataSync != 1")
    List<ExpenseSyncResult.AppliedOnDetail> getManageCropExpenseAppliedOnList(String expenseId);

    @Query("SELECT expenseMapping.appliedOnId FROM expense_mapping as expenseMapping\n" +
            "JOIN crop as crop ON crop.lotId = expenseMapping.appliedOnId\n" +
            "WHERE expenseMapping.expense_id = :expenseId AND expenseMapping.isDataSync != 1")
    List<ExpenseSyncResult.AppliedOnDetail> getManageCropLotExpenseAppliedOnList(String expenseId);

    @Query("SELECT expenseMapping.appliedOnId FROM expense_mapping as expenseMapping\n" +
            "JOIN animal as animal ON animal.lotId = expenseMapping.appliedOnId\n" +
            "WHERE expenseMapping.expense_id = :expenseId AND expenseMapping.isDataSync != 1")
    List<ExpenseSyncResult.AppliedOnDetail> getManageAnimalLotExpenseAppliedOnList(String expenseId);

    @Query("SELECT expenseMappingProduct.product_id as productId, expenseMappingProduct.price, " +
            "expenseMappingProduct.quantity FROM expense_product_mapping as expenseMappingProduct\n" +
            "JOIN product as product ON product.productId = expenseMappingProduct.product_id OR product.recordKey = expenseMappingProduct.product_id\n" +
            "WHERE expenseMappingProduct.expense_id = :expenseId AND expenseMappingProduct.isDataSync != 1")
    List<ExpenseSyncResult.ProductDetail> getManageExpenseProductList(String expenseId);

}
