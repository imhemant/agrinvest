package com.agrinvestapp.data.local.db;

import android.content.Context;

import com.agrinvestapp.data.model.api.AnimalInfoResponse;
import com.agrinvestapp.data.model.api.CalvingInfoResponse;
import com.agrinvestapp.data.model.api.CropInfoResponse;
import com.agrinvestapp.data.model.api.CropTreatmentInfoResponse;
import com.agrinvestapp.data.model.api.ExpenseInfoResponse;
import com.agrinvestapp.data.model.api.InseminationInfoResponse;
import com.agrinvestapp.data.model.api.MotherResponse;
import com.agrinvestapp.data.model.api.ParcelMovementInfoResponse;
import com.agrinvestapp.data.model.api.PregnancyTestInfoResponse;
import com.agrinvestapp.data.model.api.ProductInfoResponse;
import com.agrinvestapp.data.model.api.PropertyDetailResponse;
import com.agrinvestapp.data.model.api.PropertyInfoResponse;
import com.agrinvestapp.data.model.api.RainBeanResponse;
import com.agrinvestapp.data.model.api.TreatmentInfoResponse;
import com.agrinvestapp.data.model.api.WeightInfoResponse;
import com.agrinvestapp.data.model.db.Animal;
import com.agrinvestapp.data.model.db.Calving;
import com.agrinvestapp.data.model.db.Crop;
import com.agrinvestapp.data.model.db.CropTreatment;
import com.agrinvestapp.data.model.db.CropTreatmentMapping;
import com.agrinvestapp.data.model.db.Expense;
import com.agrinvestapp.data.model.db.ExpenseMapping;
import com.agrinvestapp.data.model.db.ExpenseProductMapping;
import com.agrinvestapp.data.model.db.Insemination;
import com.agrinvestapp.data.model.db.Parcel;
import com.agrinvestapp.data.model.db.ParcelMovement;
import com.agrinvestapp.data.model.db.PregnancyTest;
import com.agrinvestapp.data.model.db.Product;
import com.agrinvestapp.data.model.db.Property;
import com.agrinvestapp.data.model.db.RainData;
import com.agrinvestapp.data.model.db.Treatment;
import com.agrinvestapp.data.model.db.TreatmentMapping;
import com.agrinvestapp.data.model.db.UserInfoBean;
import com.agrinvestapp.data.model.db.Weight;
import com.agrinvestapp.data.model.other.AnimalSyncResult;
import com.agrinvestapp.data.model.other.CalvingSyncResult;
import com.agrinvestapp.data.model.other.CommonIdBean;
import com.agrinvestapp.data.model.other.CropSyncResult;
import com.agrinvestapp.data.model.other.CropTreatmentSyncResult;
import com.agrinvestapp.data.model.other.ExpenseProductBean;
import com.agrinvestapp.data.model.other.ExpenseSyncResult;
import com.agrinvestapp.data.model.other.IncomeBean;
import com.agrinvestapp.data.model.other.IncomeOrExpenseApplyOnBean;
import com.agrinvestapp.data.model.other.InseminationSyncResult;
import com.agrinvestapp.data.model.other.MoveToParcelSyncResult;
import com.agrinvestapp.data.model.other.PregnancyTestSyncResult;
import com.agrinvestapp.data.model.other.ProductSyncResult;
import com.agrinvestapp.data.model.other.TreatmentSyncResult;
import com.agrinvestapp.data.model.other.WeightSyncResult;

import java.util.Date;
import java.util.List;


/**
 * Created by hemant
 * Date: 10/4/18.
 */

public final class AppDbHelper implements DbHelper {

    private static AppDbHelper instance;

    private final AppDatabase mAppDatabase;

    private AppDbHelper(Context context) {
        this.mAppDatabase = AppDatabase.getDatabaseInstance(context);
    }

    public synchronized static DbHelper getDbInstance(Context context) {
        if (instance == null) {
            instance = new AppDbHelper(context);
        }
        return instance;
    }

    @Override
    public List<UserInfoBean> getAllUsers() {
        return mAppDatabase.userDao().loadAll();
    }

    @Override
    public String getExistingUserIdFromDB() {
        return mAppDatabase.userDao().getExistingUserIdFromDB();
    }

    @Override
    public Boolean insertUser(final UserInfoBean user) {
        mAppDatabase.userDao().insert(user);
        return true;
    }

    @Override
    public Boolean deleteUser(UserInfoBean user) {
        mAppDatabase.userDao().delete(user);
        return true;
    }

    @Override
    public void deleteAllUser() {
        mAppDatabase.userDao().deleteAll();
    }

    /*Property data start here*/
    @Override
    public void insertProperty(Property property) {
        mAppDatabase.propertyDao().insert(property);
    }

    @Override
    public void insertAllProperty(List<Property> properties) {
        mAppDatabase.propertyDao().insertAll(properties);
    }

    @Override
    public void updateProperty(Property property) {
        mAppDatabase.propertyDao().update(property);
    }

    @Override
    public void deleteProperty(String propertyId) {
        mAppDatabase.propertyDao().delete(propertyId);
    }

    @Override
    public String getPropertyCount() {
        return mAppDatabase.propertyDao().getPropertyCount();
    }

    @Override
    public void deleteAllProperty() {
        mAppDatabase.propertyDao().deleteAll();
    }

    @Override
    public void insertRainData(RainData rainData) {
        mAppDatabase.rainDataDao().insert(rainData);
    }

    @Override
    public void insertAllRainData(List<RainData> rainData) {
        mAppDatabase.rainDataDao().insertAll(rainData);
    }

    @Override
    public void deleteRainData(String rainDataId) {
        mAppDatabase.rainDataDao().delete(rainDataId);
    }

    @Override
    public void deleteRainData(RainData rainData) {
        mAppDatabase.rainDataDao().delete(rainData);
    }

    @Override
    public void deleteRainDataUsingPropertyId(String propertyId) {
        mAppDatabase.rainDataDao().deleteUsingPropertyId(propertyId);
    }

    @Override
    public void deleteAllRainData() {
        mAppDatabase.rainDataDao().deleteAll();
    }

    @Override
    public void insertParcel(Parcel parcel) {
        mAppDatabase.parcelDao().insert(parcel);
    }

    @Override
    public void insertAllParcel(List<Parcel> parcels) {
        mAppDatabase.parcelDao().insertAll(parcels);
    }

    @Override
    public List<Parcel> getLocalParcelListForSync() {
        return mAppDatabase.customPropertyDao().getLocalParcelListForSync();
    }

    @Override
    public List<Parcel> loadAllParcel() {
        return mAppDatabase.parcelDao().loadAll();
    }

    @Override
    public List<Parcel> loadAllParcelById(String parcelId) {
        return mAppDatabase.parcelDao().loadAllById(parcelId);
    }

    @Override
    public void parcelSyncUpdate() {
        mAppDatabase.parcelDao().parcelSyncUpdate();
    }

    @Override
    public void updateParcel(Parcel parcel) {
        mAppDatabase.parcelDao().update(parcel);
    }

    @Override
    public Parcel findByParcelName(String userId, String parcelName) {
        return mAppDatabase.parcelDao().findByParcelName(userId, parcelName);
    }

    @Override
    public Parcel getParcel(String parcelRk) {
        return mAppDatabase.parcelDao().getParcel(parcelRk);
    }

    @Override
    public List<CommonIdBean> getPropertyParcel(String propertyId) {
        return mAppDatabase.parcelDao().getPropertyParcel(propertyId);
    }


    @Override
    public void deleteUsingParcelId(String parcelId) {
        mAppDatabase.parcelDao().deleteUsingParcelId(parcelId);
    }

    @Override
    public void deleteParcelUsingPropertyId(String propertyId) {
        mAppDatabase.parcelDao().deleteUsingPropertyId(propertyId);
    }

    @Override
    public void deleteAllParcel() {
        mAppDatabase.parcelDao().deleteAll();
    }

    @Override
    public List<PropertyInfoResponse.PropertyInfoBean> getPropertyList(String userId) {
        return mAppDatabase.customPropertyDao().getPropertyList(userId);
    }

    @Override
    public PropertyDetailResponse.PropertyDetailsBean getPropertyDetail(String propertyId) {
        return mAppDatabase.customPropertyDao().getPropertyDetail(propertyId);
    }

    @Override
    public List<PropertyDetailResponse.PropertyDetailsBean.ParcelBean> getParcelList(String propertyId) {
        return mAppDatabase.customPropertyDao().getParcelList(propertyId);
    }

    @Override
    public String getAnimalCount(String propertyId) {
        return mAppDatabase.customPropertyDao().getAnimalCount(propertyId);
    }

    @Override
    public String getCropCount(String propertyId) {
        return mAppDatabase.customPropertyDao().getCropCount(propertyId);
    }

    @Override
    public List<RainBeanResponse.RainRecordListBean> getRainDataList(String propertyId) {
        return mAppDatabase.customPropertyDao().getRainDataList(propertyId);
    }

    @Override
    public List<RainBeanResponse.RainRecordListBean> getFilterRainDataList(String propertyId, String fromDate) {
        return mAppDatabase.customPropertyDao().getFilterRainDataList(propertyId, fromDate);
    }

    @Override
    public List<RainBeanResponse.RainRecordListBean> getFilterRainDataList(String propertyId, String fromDate, String toDate) {
        return mAppDatabase.customPropertyDao().getFilterRainDataList(propertyId, fromDate, toDate);
    }
    /*Property data end here*/

    /*Animal data start here*/
    @Override
    public void insertAnimal(Animal animal) {
        mAppDatabase.animalDao().insert(animal);
    }

    @Override
    public void insertAllAnimal(List<Animal> animals) {
        mAppDatabase.animalDao().insertAll(animals);
    }

    @Override
    public void updateAnimal(Animal animal) {
        mAppDatabase.animalDao().update(animal);
    }

    @Override
    public void deleteUsingAnimalId(String animalId) {
        mAppDatabase.animalDao().deleteUsingAnimalId(animalId);
    }

    @Override
    public void deleteAnimalUsingParcelId(String parcel_id) {
        mAppDatabase.animalDao().deleteAnimalUsingParcelId(parcel_id);
    }

    @Override
    public void deleteCropUsingParcelId(String parcel_id) {
        mAppDatabase.cropDao().deleteCropUsingParcelId(parcel_id);
    }

    @Override
    public void deleteAllAnimal() {
        mAppDatabase.animalDao().deleteAll();
    }

    @Override
    public List<AnimalInfoResponse.AnimalListBean> getAnimalList(String userId) {
        return mAppDatabase.customAnimalDao().getAnimalList(userId);
    }

    @Override
    public List<AnimalInfoResponse.AnimalListBean> getPropertyAnimalList(String propertyId) {
        return mAppDatabase.customAnimalDao().getPropertyAnimalList(propertyId);
    }

    @Override
    public List<AnimalInfoResponse.AnimalListBean> getParcelAnimalList(String parcelId) {
        return mAppDatabase.customAnimalDao().getParcelAnimalList(parcelId);
    }

    @Override
    public List<MotherResponse.AnimalMotherListBean> getAnimalMotherList(String userId, String animalId) {
        return mAppDatabase.customAnimalDao().getAnimalMotherList(userId, animalId);
    }

    @Override
    public String getTagNumber(String userId, String tagNumber) {
        return mAppDatabase.customAnimalDao().getTagNumber(userId, tagNumber);
    }

    @Override
    public List<AnimalSyncResult> getLocalAnimalListForSync() {
        return mAppDatabase.customAnimalDao().getLocalAnimalListForSync();
    }

    @Override
    public void animalSyncUpdate() {
        mAppDatabase.animalDao().animalSyncUpdate();
    }

    @Override
    public List<CommonIdBean> getAnimals(String parcelId) {
        return mAppDatabase.animalDao().getAnimals(parcelId);
    }

    @Override
    public List<CommonIdBean> getAnimals(String parcelId, String recordKey) {
        return mAppDatabase.animalDao().getAnimals(parcelId, recordKey);
    }

    @Override
    public String getAnimalCount() {
        return mAppDatabase.animalDao().getAnimalCount();
    }

    @Override
    public void insertTreatment(Treatment treatment) {
        mAppDatabase.treatmentDao().insert(treatment);
    }

    @Override
    public void insertAllTreatment(List<Treatment> treatments) {
        mAppDatabase.treatmentDao().insertAll(treatments);
    }

    @Override
    public void updateTreatment(Treatment treatment) {
        mAppDatabase.treatmentDao().update(treatment);
    }

    @Override
    public void deleteUsingTreatmentId(String treatmentId) {
        mAppDatabase.treatmentDao().deleteUsingTreatmentId(treatmentId);
    }

    @Override
    public void deleteAllTreatment() {
        mAppDatabase.treatmentDao().deleteAll();
    }

    @Override
    public void insertTreatmentDetail(TreatmentMapping treatment_mapping) {
        mAppDatabase.treatmentDetailDao().insert(treatment_mapping);
    }

    @Override
    public void insertAllTreatmentDetail(List<TreatmentMapping> treatment_mappings) {
        mAppDatabase.treatmentDetailDao().insertAll(treatment_mappings);
    }

    @Override
    public void updateTreatmentDetail(TreatmentMapping treatment_mapping) {
        mAppDatabase.treatmentDetailDao().update(treatment_mapping);
    }

    @Override
    public void deleteUsingTreatmentDetailMappingId(String treatmentMappingId) {
        mAppDatabase.treatmentDetailDao().deleteUsingTreatmentMappingId(treatmentMappingId);
    }

    @Override
    public void deleteAllTreatmentDetail() {
        mAppDatabase.treatmentDetailDao().deleteAll();
    }

    @Override
    public List<TreatmentInfoResponse.TreatmentDetailBean> getTreatmentList(String animalId) {
        return mAppDatabase.customAnimalDao().getTreatmentList(animalId);
    }

    @Override
    public List<TreatmentInfoResponse.TreatmentDetailBean.TreatmentListBean> getTreatmentDetailList(String treatment_id) {
        return mAppDatabase.customAnimalDao().getTreatmentDetailList(treatment_id);
    }

    @Override
    public Animal getAnimalUsingAnimalId(String animalId) {
        return mAppDatabase.customAnimalDao().getAnimalUsingAnimalId(animalId);
    }

    @Override
    public List<Animal> getAnimalUsingAnimalLotId(String lotId) {
        return mAppDatabase.customAnimalDao().getAnimalUsingAnimalLotId(lotId);
    }

    @Override
    public List<TreatmentSyncResult> getManageTreatmentList() {
        return mAppDatabase.customAnimalDao().getManageTreatmentList();
    }

    @Override
    public List<TreatmentSyncResult> getManageAllTreatmentList() {
        return mAppDatabase.customAnimalDao().getManageAllTreatmentList();
    }

    @Override
    public List<TreatmentSyncResult.TreatmentListDataBean> getManageTreatmentDetailList() {
        return mAppDatabase.customAnimalDao().getManageTreatmentDetailList();
    }

    @Override
    public List<TreatmentSyncResult.TreatmentListDataBean> getManageTreatmentDetailList(String treatment_id) {
        return mAppDatabase.customAnimalDao().getManageTreatmentDetailList(treatment_id);
    }

    @Override
    public void treatmentSyncUpdate() {
        mAppDatabase.treatmentDao().treatmentSyncUpdate();
    }

    @Override
    public Treatment getTreatmentInfo(String treatmentId) {
        return mAppDatabase.treatmentDao().getTreatmentInfo(treatmentId);
    }

    @Override
    public void treatmentMappingSyncUpdate() {
        mAppDatabase.treatmentDetailDao().treatmentMappingSyncUpdate();
    }

    @Override
    public void insertInsemination(Insemination insemination) {
        mAppDatabase.inseminationDao().insert(insemination);
    }

    @Override
    public void insertAllInsemination(List<Insemination> inseminations) {
        mAppDatabase.inseminationDao().insertAll(inseminations);
    }

    @Override
    public void updateInsemination(Insemination insemination) {
        mAppDatabase.inseminationDao().update(insemination);
    }

    @Override
    public List<InseminationInfoResponse.InseminationListBean> getInseminationList(String animalId) {
        return mAppDatabase.inseminationDao().getInseminationList(animalId);
    }

    @Override
    public List<InseminationSyncResult> getInseminationList() {
        return mAppDatabase.inseminationDao().getInseminationList();
    }

    @Override
    public void inseminationSyncUpdate() {
        mAppDatabase.inseminationDao().inseminationSyncUpdate();
    }

    @Override
    public void deleteUsingInseminationId(String inseminationId) {
        mAppDatabase.inseminationDao().deleteUsingInseminationId(inseminationId);
    }

    @Override
    public void deleteAllInsemination() {
        mAppDatabase.inseminationDao().deleteAll();
    }

    @Override
    public void insertPregnancyTest(PregnancyTest pregnancyTest) {
        mAppDatabase.pregnancyTestDao().insert(pregnancyTest);
    }

    @Override
    public void insertAllPregnancyTest(List<PregnancyTest> pregnancyTests) {
        mAppDatabase.pregnancyTestDao().insertAll(pregnancyTests);
    }

    @Override
    public void updatePregnancyTest(PregnancyTest pregnancyTest) {
        mAppDatabase.pregnancyTestDao().update(pregnancyTest);
    }

    @Override
    public List<PregnancyTestInfoResponse.PregnancyListBean> getPregnancyTestList(String animalId) {
        return mAppDatabase.pregnancyTestDao().getPregnancyTestList(animalId);
    }

    @Override
    public List<PregnancyTestSyncResult> getPregnancyTestList() {
        return mAppDatabase.pregnancyTestDao().getPregnancyTestList();
    }

    @Override
    public void pregnancyTestSyncUpdate() {
        mAppDatabase.pregnancyTestDao().pregnancyTestSyncUpdate();
    }

    @Override
    public void deleteUsingPregnancyTestId(String pregnancyTestId) {
        mAppDatabase.pregnancyTestDao().deleteUsingPregnancyTestId(pregnancyTestId);
    }

    @Override
    public void deleteAllPregnancyTest() {
        mAppDatabase.pregnancyTestDao().deleteAll();
    }

    @Override
    public void insertParcelMovement(ParcelMovement parcelMovement) {
        mAppDatabase.parcelMovementDao().insert(parcelMovement);
    }

    @Override
    public void insertAllParcelMovement(List<ParcelMovement> parcelMovements) {
        mAppDatabase.parcelMovementDao().insertAll(parcelMovements);
    }

    @Override
    public void updateParcelMovement(ParcelMovement parcelMovement) {
        mAppDatabase.parcelMovementDao().update(parcelMovement);
    }

    @Override
    public List<ParcelMovementInfoResponse.ParcelMovementListBean> getParcelMovementList(String animalId) {
        return mAppDatabase.parcelMovementDao().getParcelMovementList(animalId);
    }

    @Override
    public List<MoveToParcelSyncResult> getMToParcelList() {
        return mAppDatabase.parcelMovementDao().getMToParcelList();
    }

    @Override
    public void parcelMoveSyncUpdate() {
        mAppDatabase.parcelMovementDao().parcelMoveSyncUpdate();
    }

    @Override
    public void deleteUsingParcelMovementId(String parcelMovementId) {
        mAppDatabase.parcelMovementDao().deleteUsingParcelMovementId(parcelMovementId);
    }

    @Override
    public void deleteAllParcelMovement() {
        mAppDatabase.parcelMovementDao().deleteAll();
    }

    @Override
    public void insertCalving(Calving calving) {
        mAppDatabase.calvingDao().insert(calving);
    }

    @Override
    public void insertAllCalving(List<Calving> calvings) {
        mAppDatabase.calvingDao().insertAll(calvings);
    }

    @Override
    public void updateCalving(Calving calving) {
        mAppDatabase.calvingDao().update(calving);
    }

    @Override
    public List<CalvingInfoResponse.CalvingListBean> getCalvingList(String animalId) {
        return mAppDatabase.calvingDao().getCalvingList(animalId);
    }

    @Override
    public List<CalvingSyncResult> getCalvingList() {
        return mAppDatabase.calvingDao().getCalvingList();
    }

    @Override
    public void calvingSyncUpdate() {
        mAppDatabase.calvingDao().calvingSyncUpdate();
    }

    @Override
    public void deleteUsingCalvingId(String calvingId) {
        mAppDatabase.calvingDao().deleteUsingCalvingId(calvingId);
    }

    @Override
    public void deleteAllCalving() {
        mAppDatabase.calvingDao().deleteAll();
    }

    @Override
    public void insertWeight(Weight weight) {
        mAppDatabase.weightDao().insert(weight);
    }

    @Override
    public void insertAllWeight(List<Weight> weights) {
        mAppDatabase.weightDao().insertAll(weights);
    }

    @Override
    public void updateWeight(Weight weight) {
        mAppDatabase.weightDao().update(weight);
    }

    @Override
    public List<WeightInfoResponse.WeightListBean> getWeightList(String animalId) {
        return mAppDatabase.weightDao().getWeightList(animalId);
    }

    @Override
    public List<WeightSyncResult> getWeightList() {
        return mAppDatabase.weightDao().getWeightList();
    }

    @Override
    public void weightSyncUpdate() {
        mAppDatabase.weightDao().weightSyncUpdate();
    }

    @Override
    public void deleteUsingWeightId(String weightId) {
        mAppDatabase.weightDao().deleteUsingWeightId(weightId);
    }

    @Override
    public void deleteAllWeight() {
        mAppDatabase.weightDao().deleteAll();
    }

    @Override
    public void insertCrop(Crop crop) {
        mAppDatabase.cropDao().insert(crop);
    }

    @Override
    public void insertAllCrop(List<Crop> crops) {
        mAppDatabase.cropDao().insertAll(crops);
    }

    @Override
    public void updateCrop(Crop crop) {
        mAppDatabase.cropDao().update(crop);
    }

    @Override
    public void deleteUsingCropId(String cropId) {
        mAppDatabase.cropDao().deleteUsingCropId(cropId);
    }

    @Override
    public void deleteAllCrop() {
        mAppDatabase.cropDao().deleteAll();
    }

    @Override
    public void cropSyncUpdate() {
        mAppDatabase.cropDao().cropSyncUpdate();
    }

    @Override
    public List<CommonIdBean> getCrops(String parcelId) {
        return mAppDatabase.cropDao().getCrops(parcelId);
    }

    @Override
    public List<CommonIdBean> getCrops(String parcelId, String recordKey) {
        return mAppDatabase.cropDao().getCrops(parcelId, recordKey);
    }

    @Override
    public void insertCropTreatment(CropTreatment treatment) {
        mAppDatabase.cropTreatmentDao().insert(treatment);
    }

    @Override
    public void insertAllCropTreatment(List<CropTreatment> treatments) {
        mAppDatabase.cropTreatmentDao().insertAll(treatments);
    }

    @Override
    public void updateCropTreatment(CropTreatment treatment) {
        mAppDatabase.cropTreatmentDao().update(treatment);
    }

    @Override
    public void deleteUsingCropTreatmentId(String treatmentId) {
        mAppDatabase.cropTreatmentDao().deleteUsingCropTreatmentId(treatmentId);
    }

    @Override
    public void deleteAllCropTreatment() {
        mAppDatabase.cropTreatmentDao().deleteAll();
    }

    @Override
    public void insertCropTreatmentDetail(CropTreatmentMapping treatment_mapping) {
        mAppDatabase.cropTreatmentMappingDao().insert(treatment_mapping);
    }

    @Override
    public void insertAllCropTreatmentDetail(List<CropTreatmentMapping> treatment_mappings) {
        mAppDatabase.cropTreatmentMappingDao().insertAll(treatment_mappings);
    }

    @Override
    public void updateCropTreatmentDetail(CropTreatmentMapping treatment_mapping) {
        mAppDatabase.cropTreatmentMappingDao().update(treatment_mapping);
    }

    @Override
    public void deleteUsingCropTreatmentDetailMappingId(String treatmentMappingId) {
        mAppDatabase.cropTreatmentMappingDao().deleteUsingCropTreatmentMappingId(treatmentMappingId);
    }

    @Override
    public void deleteAllCropTreatmentDetail() {
        mAppDatabase.cropTreatmentMappingDao().deleteAll();
    }

    @Override
    public List<CropTreatmentInfoResponse.TreatmentDetailListBean> getCropTreatmentList(String cropId) {
        return mAppDatabase.cropDao().getCropTreatmentList(cropId);
    }

    @Override
    public List<CropTreatmentInfoResponse.TreatmentDetailListBean.TreatmentListBean> getCropTreatmentDetailList(String treatment_id) {
        return mAppDatabase.cropDao().getCropTreatmentDetailList(treatment_id);
    }

    @Override
    public String getCropRK(String cropId) {
        return mAppDatabase.cropDao().getCropRK(cropId);
    }

    @Override
    public Crop getCrop(String id) {
        return mAppDatabase.cropDao().getCrop(id);
    }

    @Override
    public List<Crop> getCropUsingCropLotId(String lotId) {
        return mAppDatabase.cropDao().getCropUsingCropLotId(lotId);
    }

    @Override
    public List<CropSyncResult> getCropList(String userId) {
        return mAppDatabase.cropDao().getCropList(userId);
    }

    @Override
    public CropTreatment getCropTreatmentInfo(String treatmentId) {
        return mAppDatabase.cropTreatmentDao().getCropTreatmentInfo(treatmentId);
    }

    @Override
    public List<CropTreatment> getManageCropTreatmentList() {
        return mAppDatabase.cropDao().getManageCropTreatmentList();
    }

    @Override
    public List<CropTreatment> getManageAllCropTreatmentList() {
        return mAppDatabase.cropDao().getManageAllCropTreatmentList();
    }

    @Override
    public List<CropTreatmentSyncResult.TreatmentListDataBean> getManageCropTreatmentDetailList(String treatment_id) {
        return mAppDatabase.cropDao().getManageCropTreatmentDetailList(treatment_id);
    }

    @Override
    public void cropTreatmentSyncUpdate() {
        mAppDatabase.cropTreatmentDao().cropTreatmentSyncUpdate();
    }

    @Override
    public void cropTreatmentMappingSyncUpdate() {
        mAppDatabase.cropTreatmentMappingDao().cropTreatmentMappingSyncUpdate();
    }

    @Override
    public List<CropInfoResponse.CroplListBean> getPropertyCropList() {
        return mAppDatabase.cropDao().getPropertyCropList();
    }

    @Override
    public List<CropInfoResponse.CroplListBean> getPropertyCropList(String propertyId) {
        return mAppDatabase.cropDao().getPropertyCropList(propertyId);
    }

    @Override
    public List<CropInfoResponse.CroplListBean> getParcelCropList(String parcelId) {
        return mAppDatabase.cropDao().getParcelCropList(parcelId);
    }

    @Override
    public List<ExpenseProductBean> getProductList() {
        return mAppDatabase.productDao().getProductList();
    }

    @Override
    public void deleteUsingProductId(String productId) {
        mAppDatabase.productDao().deleteUsingProductId(productId);
    }

    @Override
    public void deleteAllProduct() {
        mAppDatabase.productDao().deleteAll();
    }

    @Override
    public void updateProduct(Product product) {
        mAppDatabase.productDao().update(product);
    }

    @Override
    public void insertProduct(Product product) {
        mAppDatabase.productDao().insert(product);
    }

    @Override
    public void insertAllProduct(List<Product> products) {
        mAppDatabase.productDao().insertAll(products);
    }

    @Override
    public List<ProductInfoResponse.ProductListBean> getAllProductList() {
        return mAppDatabase.productDao().getAllProductList();
    }

    @Override
    public List<ProductInfoResponse.ProductListBean> getMyProductList(String userType) {
        return mAppDatabase.productDao().getMyProductList(userType);
    }

    @Override
    public void productSyncUpdate() {
        mAppDatabase.productDao().productSyncUpdate();
    }

    @Override
    public List<ProductInfoResponse.ProductListBean> productFilterAll(String prodName, String minPrice, String maxPrice, String qty) {
         return mAppDatabase.productDao().productFilterAll(prodName, minPrice, maxPrice, qty);
    }

    @Override
    public List<ProductInfoResponse.ProductListBean> productFilterAllName(String prodName) {
        return mAppDatabase.productDao().productFilterAllName(prodName);
    }

    @Override
    public List<ProductInfoResponse.ProductListBean> productFilterAllPrice(String minPrice, String maxPrice) {
         return mAppDatabase.productDao().productFilterAllPrice(minPrice, maxPrice);
    }

    @Override
    public List<ProductInfoResponse.ProductListBean> productFilterAllQty(String qty) {
         return mAppDatabase.productDao().productFilterAllQty(qty);
    }

    @Override
    public List<ProductInfoResponse.ProductListBean> productFilterAllNameAndPrice(String prodName, String minPrice, String maxPrice) {
         return mAppDatabase.productDao().productFilterAllNameAndPrice(prodName, minPrice, maxPrice);
    }

    @Override
    public List<ProductInfoResponse.ProductListBean> productFilterAllNameAndQty(String prodName, String qty) {
         return mAppDatabase.productDao().productFilterAllNameAndQty(prodName, qty);
    }

    @Override
    public List<ProductInfoResponse.ProductListBean> productFilterAllQtyAndPrice(String qty, String minPrice, String maxPrice) {
         return mAppDatabase.productDao().productFilterAllQtyAndPrice(qty, minPrice, maxPrice);
    }

    @Override
    public List<ProductInfoResponse.ProductListBean> productMyFilter(String userId, String prodName, String minPrice, String maxPrice, String qty) {
         return mAppDatabase.productDao().productMyFilter(userId, prodName, minPrice, maxPrice, qty);
    }

    @Override
    public List<ProductInfoResponse.ProductListBean> productMyFilterName(String userId, String prodName) {
         return mAppDatabase.productDao().productMyFilterName(userId, prodName);
    }

    @Override
    public List<ProductInfoResponse.ProductListBean> productMyFilterPrice(String userId, String minPrice, String maxPrice) {
         return mAppDatabase.productDao().productMyFilterPrice(userId, minPrice, maxPrice);
    }

    @Override
    public List<ProductInfoResponse.ProductListBean> productMyFilterQty(String userId, String qty) {
         return mAppDatabase.productDao().productMyFilterQty(userId, qty);
    }

    @Override
    public List<ProductInfoResponse.ProductListBean> productMyFilterNameAndPrice(String userId, String prodName, String minPrice, String maxPrice) {
         return mAppDatabase.productDao().productMyFilterNameAndPrice(userId, prodName, minPrice, maxPrice);
    }

    @Override
    public List<ProductInfoResponse.ProductListBean> productMyFilterNameAndQty(String userId, String prodName, String qty) {
         return mAppDatabase.productDao().productMyFilterNameAndQty(userId, prodName, qty);
    }

    @Override
    public List<ProductInfoResponse.ProductListBean> productMyFilterQtyAndPrice(String userId, String qty, String minPrice, String maxPrice) {
         return mAppDatabase.productDao().productMyFilterQtyAndPrice(userId, qty, minPrice, maxPrice);
    }

    @Override
    public List<ProductSyncResult> getManageProductList() {
        return mAppDatabase.productDao().getManageProductList();
    }

    @Override
    public void insertExpense(Expense expense) {
        mAppDatabase.expenseDao().insert(expense);
    }

    @Override
    public void insertAllExpense(List<Expense> expenses) {
        mAppDatabase.expenseDao().insertAll(expenses);
    }

    @Override
    public void updateExpense(Expense expense) {
        mAppDatabase.expenseDao().update(expense);
    }

    @Override
    public void deleteAllExpense() {
        mAppDatabase.expenseDao().deleteAll();
    }

    @Override
    public void deleteUsingExpenseId(String expenseId) {
        mAppDatabase.expenseDao().deleteUsingExpenseId(expenseId);
    }

    @Override
    public void expenseSyncUpdate() {
        mAppDatabase.expenseDao().expenseSyncUpdate();
    }

    @Override
    public void insertExpenseMapping(ExpenseMapping expenseMapping) {
        mAppDatabase.expenseMappingDao().insert(expenseMapping);
    }

    @Override
    public void insertAllExpenseMapping(List<ExpenseMapping> expenseMappings) {
        mAppDatabase.expenseMappingDao().insertAll(expenseMappings);
    }

    @Override
    public void updateAllExpenseMapping(List<ExpenseMapping> expenseMapping) {
        mAppDatabase.expenseMappingDao().updateAll(expenseMapping);
    }

    @Override
    public void updateExpenseMapping(ExpenseMapping expenseMapping) {
        mAppDatabase.expenseMappingDao().update(expenseMapping);
    }

    @Override
    public void deleteAllExpenseMapping() {
        mAppDatabase.expenseMappingDao().deleteAll();
    }

    @Override
    public void deleteUsingExpenseMappingId(String expenseMappingId) {
        mAppDatabase.expenseMappingDao().deleteUsingExpenseMappingId(expenseMappingId);
    }

    @Override
    public void deleteExpenseMappingUsingExpenseId(String expenseId) {
        mAppDatabase.expenseMappingDao().deleteExpenseMappingUsingExpenseId(expenseId);
    }

    @Override
    public void expenseMappingSyncUpdate() {
        mAppDatabase.expenseMappingDao().expenseMappingSyncUpdate();
    }

    @Override
    public void insertExpenseProdMapping(ExpenseProductMapping expenseProductMapping) {
        mAppDatabase.expenseProductMappingDao().insert(expenseProductMapping);
    }

    @Override
    public void insertAllExpenseProdMapping(List<ExpenseProductMapping> expenseProductMappings) {
        mAppDatabase.expenseProductMappingDao().insertAll(expenseProductMappings);
    }

    @Override
    public void updateAllExpenseProdMapping(List<ExpenseProductMapping> expenseProductMapping) {
        mAppDatabase.expenseProductMappingDao().updateAll(expenseProductMapping);
    }

    @Override
    public void updateExpenseProdMapping(ExpenseProductMapping expenseProductMapping) {
        mAppDatabase.expenseProductMappingDao().update(expenseProductMapping);
    }

    @Override
    public void deleteAllExpenseProdMapping() {
        mAppDatabase.expenseProductMappingDao().deleteAll();
    }

    @Override
    public void deleteUsingExpenseProdMappingId(String expenseProductMappingId) {
        mAppDatabase.expenseProductMappingDao().deleteUsingExpenseProdMappingId(expenseProductMappingId);
    }

    @Override
    public void deleteExpenseProdMappingUsingExpenseId(String expenseId) {
        mAppDatabase.expenseProductMappingDao().deleteExpenseProdMappingUsingExpenseId(expenseId);
    }

    @Override
    public void expenseProductMappingSyncUpdate() {
        mAppDatabase.expenseProductMappingDao().expenseProductMappingSyncUpdate();
    }

    @Override
    public List<IncomeOrExpenseApplyOnBean> getAnimalApplyOnInfo() {
        return mAppDatabase.expenseDao().getAnimalApplyOnInfo();
    }

    @Override
    public List<IncomeOrExpenseApplyOnBean> getCropApplyOnInfo() {
        return mAppDatabase.expenseDao().getCropApplyOnInfo();
    }

    @Override
    public List<IncomeOrExpenseApplyOnBean> getAnimalLotApplyOnInfo() {
        return mAppDatabase.expenseDao().getAnimalLotApplyOnInfo();
    }

    @Override
    public List<IncomeOrExpenseApplyOnBean> getCropLotApplyOnInfo() {
        return mAppDatabase.expenseDao().getCropLotApplyOnInfo();
    }

    @Override
    public List<IncomeOrExpenseApplyOnBean> getParcelApplyOnInfo() {
        return mAppDatabase.expenseDao().getParcelApplyOnInfo();
    }

    @Override
    public List<IncomeOrExpenseApplyOnBean> getPropertyApplyOnInfo() {
        return mAppDatabase.expenseDao().getPropertyApplyOnInfo();
    }

    @Override
    public List<ExpenseInfoResponse.ExpenseListBean> getExpenseList(String expenseFor) {
        return mAppDatabase.expenseDao().getExpenseList(expenseFor);
    }

    @Override
    public List<ExpenseInfoResponse.ExpenseListBean.AppliedOnBean> getAnimalExpenseAppliedOnList(String expenseId) {
        return mAppDatabase.expenseDao().getAnimalExpenseAppliedOnList(expenseId);
    }

    @Override
    public List<ExpenseInfoResponse.ExpenseListBean.AppliedOnBean> getParcelExpenseAppliedOnList(String expenseId) {
        return mAppDatabase.expenseDao().getParcelExpenseAppliedOnList(expenseId);
    }

    @Override
    public List<ExpenseInfoResponse.ExpenseListBean.AppliedOnBean> getPropertyExpenseAppliedOnList(String expenseId) {
        return mAppDatabase.expenseDao().getPropertyExpenseAppliedOnList(expenseId);
    }

    @Override
    public List<ExpenseInfoResponse.ExpenseListBean.AppliedOnBean> getCropExpenseAppliedOnList(String expenseId) {
        return mAppDatabase.expenseDao().getCropExpenseAppliedOnList(expenseId);
    }

    @Override
    public List<ExpenseInfoResponse.ExpenseListBean.AppliedOnBean> getCropLotExpenseAppliedOnList(String expenseId) {
        return mAppDatabase.expenseDao().getCropLotExpenseAppliedOnList(expenseId);
    }

    @Override
    public List<ExpenseInfoResponse.ExpenseListBean.AppliedOnBean> getAnimalLotExpenseAppliedOnList(String expenseId) {
        return mAppDatabase.expenseDao().getAnimalLotExpenseAppliedOnList(expenseId);
    }

    @Override
    public List<ExpenseInfoResponse.ExpenseListBean.ProductDetailsBean> getExpenseProductList(String expenseId) {
        return mAppDatabase.expenseDao().getExpenseProductList(expenseId);
    }

    @Override
    public ExpenseProductMapping getExpenseProductDetail(String expense_id, String product_id) {
        return mAppDatabase.expenseDao().getExpenseProductDetail(expense_id, product_id);
    }

    @Override
    public String getExpenseApplyStatus(String id) {
        return mAppDatabase.expenseDao().getExpenseApplyStatus(id);
    }

    @Override
    public String getExpenseApplyStatus(String id, String recordKey) {
        return mAppDatabase.expenseDao().getExpenseApplyStatus(id, recordKey);
    }

    @Override
    public String getProductExpenseApplyStatus(String id, String recordKey) {
        return mAppDatabase.expenseDao().getProductExpenseApplyStatus(id, recordKey);
    }

    @Override
    public List<ExpenseSyncResult> getManageExpenseList() {
        return mAppDatabase.expenseDao().getManageExpenseList();
    }

    @Override
    public List<ExpenseSyncResult.AppliedOnDetail> getManageAnimalExpenseAppliedOnList(String expenseId) {
        return mAppDatabase.expenseDao().getManageAnimalExpenseAppliedOnList(expenseId);
    }

    @Override
    public List<ExpenseSyncResult.AppliedOnDetail> getManageParcelExpenseAppliedOnList(String expenseId) {
        return mAppDatabase.expenseDao().getManageParcelExpenseAppliedOnList(expenseId);
    }

    @Override
    public List<ExpenseSyncResult.AppliedOnDetail> getManagePropertyExpenseAppliedOnList(String expenseId) {
        return mAppDatabase.expenseDao().getManagePropertyExpenseAppliedOnList(expenseId);
    }

    @Override
    public List<ExpenseSyncResult.AppliedOnDetail> getManageCropExpenseAppliedOnList(String expenseId) {
        return mAppDatabase.expenseDao().getManageCropExpenseAppliedOnList(expenseId);
    }

    @Override
    public List<ExpenseSyncResult.AppliedOnDetail> getManageCropLotExpenseAppliedOnList(String expenseId) {
        return mAppDatabase.expenseDao().getManageCropLotExpenseAppliedOnList(expenseId);
    }

    @Override
    public List<ExpenseSyncResult.AppliedOnDetail> getManageAnimalLotExpenseAppliedOnList(String expenseId) {
        return mAppDatabase.expenseDao().getManageAnimalLotExpenseAppliedOnList(expenseId);
    }

    @Override
    public List<ExpenseSyncResult.ProductDetail> getManageExpenseProductList(String expenseId) {
        return mAppDatabase.expenseDao().getManageExpenseProductList(expenseId);
    }

    @Override
    public List<String> getExpenseIdFromExpenseMapping(String appliedId) {
        return mAppDatabase.incomeStatementDao().getExpenseIdFromExpenseMapping(appliedId);
    }

    @Override
    public List<String> getExpenseIdFromExpenseMapping(String appliedId, String recordKey) {
        return mAppDatabase.incomeStatementDao().getExpenseIdFromExpenseMapping(appliedId, recordKey);
    }

    @Override
    public String getExpenseCostFromExpense(String expenseId, String expenseFor) {
        return mAppDatabase.incomeStatementDao().getExpenseCostFromExpense(expenseId, expenseFor);
    }

    @Override
    public String getExpenseCostFromExpense(String expenseId, String expenseFor, Date fromDate) {
        return mAppDatabase.incomeStatementDao().getExpenseCostFromExpense(expenseId, expenseFor, fromDate);
    }

    @Override
    public String getExpenseCostFromExpense(String expenseId, String expenseFor, Date fromDate, Date toDate) {
        return mAppDatabase.incomeStatementDao().getExpenseCostFromExpense(expenseId, expenseFor, fromDate, toDate);
    }

    @Override
    public String getExpenseCountFromExpenseMapping(String expenseId) {
        return mAppDatabase.incomeStatementDao().getExpenseCountFromExpenseMapping(expenseId);
    }

    @Override
    public List<IncomeBean> getPropertyExpenseFromParcel(String propertyId) {
        return mAppDatabase.incomeStatementDao().getPropertyExpenseFromParcel(propertyId);
    }

    @Override
    public String getParcelCountUsingProperty(String propertyId) {
        return mAppDatabase.incomeStatementDao().getParcelCountUsingProperty(propertyId);
    }

    @Override
    public IncomeBean getParcelIdFromAnimal(String animalId) {
        return mAppDatabase.incomeStatementDao().getParcelIdFromAnimal(animalId);
    }

    @Override
    public List<IncomeBean> getAnimalIdUsingParcel(String parcelId, String recordKey) {
        return mAppDatabase.incomeStatementDao().getAnimalIdUsingParcel(parcelId, recordKey);
    }

    @Override
    public String getTtlSaleFromAnimal(String animalId) {
        return mAppDatabase.incomeStatementDao().getTtlSaleFromAnimal(animalId);
    }

    @Override
    public String getTtlSaleFromAnimal(String animalId, Date fromDate) {
        return mAppDatabase.incomeStatementDao().getTtlSaleFromAnimal(animalId, fromDate);
    }

    @Override
    public String getTtlSaleFromAnimal(String animalId, Date fromDate, Date toDate) {
        return mAppDatabase.incomeStatementDao().getTtlSaleFromAnimal(animalId, fromDate, toDate);
    }

    @Override
    public String getAnimalLotIdUsingAnimal(String animalId) {
        return mAppDatabase.incomeStatementDao().getAnimalLotIdUsingAnimal(animalId);
    }

    @Override
    public String getAnimalCountUsingAnimalLotId(String lotId) {
        return mAppDatabase.incomeStatementDao().getAnimalCountUsingAnimalLotId(lotId);
    }

    @Override
    public String getAnimalCountUsingParcel(String parcelId) {
        return mAppDatabase.incomeStatementDao().getAnimalCountUsingParcel(parcelId);
    }

    @Override
    public List<IncomeBean> getCropIdUsingParcel(String parcelId, String recordKey) {
        return mAppDatabase.incomeStatementDao().getCropIdUsingParcel(parcelId, recordKey);
    }

    @Override
    public String getTtlSaleFromCrop(String cropId) {
        return mAppDatabase.incomeStatementDao().getTtlSaleFromCrop(cropId);
    }

    @Override
    public String getTtlSaleFromCrop(String cropId, Date fromDate) {
        return mAppDatabase.incomeStatementDao().getTtlSaleFromCrop(cropId, fromDate);
    }

    @Override
    public String getTtlSaleFromCrop(String cropId, Date fromDate, Date toDate) {
        return mAppDatabase.incomeStatementDao().getTtlSaleFromCrop(cropId, fromDate, toDate);
    }

    @Override
    public String getCropLotIdUsingCrop(String cropId) {
        return mAppDatabase.incomeStatementDao().getCropLotIdUsingCrop(cropId);
    }

    @Override
    public String getCropCountUsingCropLotId(String lotId) {
        return mAppDatabase.incomeStatementDao().getCropCountUsingCropLotId(lotId);
    }

    @Override
    public String getCropCountUsingParcel(String parcelId) {
        return mAppDatabase.incomeStatementDao().getCropCountUsingParcel(parcelId);
    }
    /*Animal data end here*/

    @Override
    public void clearAllTable() {
        mAppDatabase.clearAllTables();
    }

}
