package com.agrinvestapp.data.local.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.agrinvestapp.data.model.api.ProductInfoResponse;
import com.agrinvestapp.data.model.db.Product;
import com.agrinvestapp.data.model.other.ExpenseProductBean;
import com.agrinvestapp.data.model.other.ProductSyncResult;

import java.util.List;

/**
 * Created by hemant
 * Date: 10/4/18.
 */

@Dao
public interface ProductDao {

    @Query("DELETE FROM product WHERE productId = :productId")
    void deleteUsingProductId(String productId);

    @Query("DELETE FROM product")
    void deleteAll();

    @Update
    void update(Product product);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Product product);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Product> products);

    @Query("UPDATE product SET isDataSync = 1")
    void productSyncUpdate();

    @Query("SELECT recordKey as productId, productName, price, quantity FROM product Where event != 'delete' Order By productName")
    List<ExpenseProductBean> getProductList();

    @Query("SELECT productId, productName, productImage, price, unit, quantity, description, recordKey, user_id, userType, status, crd, upd, productImageThumb FROM product Where event != 'delete' ORDER BY crd DESC")
    List<ProductInfoResponse.ProductListBean> getAllProductList();

    @Query("SELECT productId, productName, productImage, price, unit, quantity, description, recordKey, user_id, userType, status, crd, upd, productImageThumb FROM product Where userType =:userType AND event != 'delete' ORDER BY crd DESC")
    List<ProductInfoResponse.ProductListBean> getMyProductList(String userType);

    //for all product list
    @Query("SELECT productId, productName, productImage, price, unit, quantity, description, recordKey, user_id, userType, status, crd, upd, productImageThumb FROM product WHERE event != 'delete' AND productName LIKE '%' || :prodName || '%' AND price >= :minPrice AND price <= :maxPrice AND quantity = :qty ORDER BY productId DESC")
    List<ProductInfoResponse.ProductListBean> productFilterAll(String prodName, String minPrice, String maxPrice, String qty);

    @Query("SELECT productId, productName, productImage, price, unit, quantity, description, recordKey, user_id, userType, status, crd, upd, productImageThumb FROM product WHERE event != 'delete' AND productName LIKE '%' || :prodName || '%' ORDER BY productId DESC")
    List<ProductInfoResponse.ProductListBean> productFilterAllName(String prodName);

    @Query("SELECT productId, productName, productImage, price, unit, quantity, description, recordKey, user_id, userType, status, crd, upd, productImageThumb FROM product WHERE event != 'delete' AND price >= :minPrice AND price <= :maxPrice ORDER BY productId DESC")
    List<ProductInfoResponse.ProductListBean> productFilterAllPrice(String minPrice, String maxPrice);

    @Query("SELECT productId, productName, productImage, price, unit, quantity, description, recordKey, user_id, userType, status, crd, upd, productImageThumb FROM product WHERE event != 'delete' AND quantity = :qty ORDER BY productId DESC")
    List<ProductInfoResponse.ProductListBean> productFilterAllQty(String qty);

    @Query("SELECT productId, productName, productImage, price, unit, quantity, description, recordKey, user_id, userType, status, crd, upd, productImageThumb FROM product WHERE event != 'delete' AND productName LIKE '%' || :prodName || '%' AND price >= :minPrice AND price <= :maxPrice ORDER BY productId DESC")
    List<ProductInfoResponse.ProductListBean> productFilterAllNameAndPrice(String prodName, String minPrice, String maxPrice);

    @Query("SELECT productId, productName, productImage, price, unit, quantity, description, recordKey, user_id, userType, status, crd, upd, productImageThumb FROM product WHERE event != 'delete' AND productName LIKE '%' || :prodName || '%' AND quantity = :qty ORDER BY productId DESC")
    List<ProductInfoResponse.ProductListBean> productFilterAllNameAndQty(String prodName, String qty);

    @Query("SELECT productId, productName, productImage, price, unit, quantity, description, recordKey, user_id, userType, status, crd, upd, productImageThumb FROM product WHERE event != 'delete' AND price >= :minPrice AND price <= :maxPrice AND quantity = :qty ORDER BY productId DESC")
    List<ProductInfoResponse.ProductListBean> productFilterAllQtyAndPrice(String qty, String minPrice, String maxPrice);

    //for user product
    @Query("SELECT productId, productName, productImage, price, unit, quantity, description, recordKey, user_id, userType, status, crd, upd, productImageThumb FROM product WHERE event != 'delete' AND (user_id = :userId) " +
            "AND productName LIKE '%' || :prodName || '%' AND (price >= :minPrice AND price <= :maxPrice) AND quantity = :qty ORDER BY productId DESC")
    List<ProductInfoResponse.ProductListBean> productMyFilter(String userId, String prodName, String minPrice, String maxPrice, String qty);

    @Query("SELECT productId, productName, productImage, price, unit, quantity, description, recordKey, user_id, userType, status, crd, upd, productImageThumb FROM product WHERE event != 'delete' AND (user_id = :userId) " +
            "AND productName LIKE '%' || :prodName || '%' ORDER BY productId DESC")
    List<ProductInfoResponse.ProductListBean> productMyFilterName(String userId, String prodName);

    @Query("SELECT productId, productName, productImage, price, unit, quantity, description, recordKey, user_id, userType, status, crd, upd, productImageThumb FROM product WHERE event != 'delete' AND (user_id = :userId) " +
            "AND (price >= :minPrice AND price <= :maxPrice) ORDER BY productId DESC")
    List<ProductInfoResponse.ProductListBean> productMyFilterPrice(String userId, String minPrice, String maxPrice);

    @Query("SELECT productId, productName, productImage, price, unit, quantity, description, recordKey, user_id, userType, status, crd, upd, productImageThumb FROM product WHERE event != 'delete' AND (user_id = :userId) " +
            "AND quantity = :qty ORDER BY productId DESC")
    List<ProductInfoResponse.ProductListBean> productMyFilterQty(String userId, String qty);

    @Query("SELECT productId, productName, productImage, price, unit, quantity, description, recordKey, user_id, userType, status, crd, upd, productImageThumb FROM product WHERE event != 'delete' AND (user_id = :userId) " +
            "AND productName LIKE '%' || :prodName || '%' AND ( price >= :minPrice AND price <= :maxPrice ) ORDER BY productId DESC")
    List<ProductInfoResponse.ProductListBean> productMyFilterNameAndPrice(String userId, String prodName, String minPrice, String maxPrice);

    @Query("SELECT productId, productName, productImage, price, unit, quantity, description, recordKey, user_id, userType, status, crd, upd, productImageThumb FROM product WHERE event != 'delete' AND (user_id = :userId) " +
            "AND productName LIKE '%' || :prodName || '%' AND quantity = :qty ORDER BY productId DESC")
    List<ProductInfoResponse.ProductListBean> productMyFilterNameAndQty(String userId, String prodName, String qty);

    @Query("SELECT productId, productName, productImage, price, unit, quantity, description, recordKey, user_id, userType, status, crd, upd, productImageThumb FROM product WHERE event != 'delete' AND (user_id = :userId) " +
            "AND (price >= :minPrice AND price <= :maxPrice) AND quantity = :qty ORDER BY productId DESC")
    List<ProductInfoResponse.ProductListBean> productMyFilterQtyAndPrice(String userId, String qty, String minPrice, String maxPrice);

    @Query("SELECT event as requestType, recordKey, productName, quantity, unit, price, description, productImage FROM product WHERE isDataSync != 1")
    List<ProductSyncResult> getManageProductList();
}
