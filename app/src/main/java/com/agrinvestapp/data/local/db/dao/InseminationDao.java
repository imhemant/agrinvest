package com.agrinvestapp.data.local.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.agrinvestapp.data.model.api.InseminationInfoResponse;
import com.agrinvestapp.data.model.db.Insemination;
import com.agrinvestapp.data.model.other.InseminationSyncResult;

import java.util.List;

/**
 * Created by hemant
 * Date: 10/4/18.
 */

@Dao
public interface InseminationDao {

    @Query("DELETE FROM insemination WHERE inseminationId = :inseminationId")
    void deleteUsingInseminationId(String inseminationId);

    @Query("DELETE FROM insemination")
    void deleteAll();

    @Update
    void update(Insemination insemination);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Insemination insemination);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Insemination> inseminations);

    @Query("SELECT * FROM insemination WHERE animal_id=:animal_id AND event != 'delete' ORDER BY crd DESC")
    List<InseminationInfoResponse.InseminationListBean> getInseminationList(String animal_id);

    @Query("SELECT * FROM insemination WHERE event != 'delete' AND inseminationId IN (:inseminationIds)")
    List<Insemination> loadAllByIds(List<Integer> inseminationIds);

    @Query("SELECT animal.recordKey, insemination.event as requestType , insemination.inseminationName,  insemination.recordKey as inseminationRecordKey," +
            " insemination.inseminationId, insemination.inseminationDate, insemination.inseminationDescription, insemination.isDataSync FROM animal as animal JOIN insemination as insemination ON insemination.animal_id=animal.animalId WHERE insemination.isDataSync != 1 GROUP BY animal.animalId")
    List<InseminationSyncResult> getInseminationList();

    @Query("UPDATE insemination SET isDataSync = 1")
    void inseminationSyncUpdate();
}
