package com.agrinvestapp.data.local.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.agrinvestapp.data.model.api.PregnancyTestInfoResponse;
import com.agrinvestapp.data.model.db.PregnancyTest;
import com.agrinvestapp.data.model.other.PregnancyTestSyncResult;

import java.util.List;

/**
 * Created by hemant
 * Date: 10/4/18.
 */

@Dao
public interface PregnancyTestDao {

    @Query("DELETE FROM pregnancy_test WHERE pregnancyId = :pregnancyTestId")
    void deleteUsingPregnancyTestId(String pregnancyTestId);

    @Query("DELETE FROM pregnancy_test")
    void deleteAll();

    @Update
    void update(PregnancyTest pregnancyTest);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(PregnancyTest pregnancyTest);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<PregnancyTest> pregnancyTests);

    @Query("SELECT * FROM pregnancy_test WHERE animal_id=:animal_id AND event != 'delete' ORDER BY crd DESC")
    List<PregnancyTestInfoResponse.PregnancyListBean> getPregnancyTestList(String animal_id);

    @Query("SELECT * FROM pregnancy_test WHERE pregnancyId IN (:pregnancyIds)")
    List<PregnancyTest> loadAllByIds(List<Integer> pregnancyIds);

    @Query("SELECT animal.recordKey, pregnancyTest.event as requestType , pregnancyTest.pregnancyName,  pregnancyTest.recordKey as pregnancyRecordKey," +
            " pregnancyTest.pregnancyId, pregnancyTest.pregnancyDate, pregnancyTest.pregnancyDescription, pregnancyTest.isDataSync FROM animal as animal JOIN pregnancy_test as pregnancyTest ON pregnancyTest.animal_id=animal.animalId WHERE pregnancyTest.isDataSync != 1 GROUP BY animal.animalId")
    List<PregnancyTestSyncResult> getPregnancyTestList();

    @Query("UPDATE pregnancy_test SET isDataSync = 1")
    void pregnancyTestSyncUpdate();
}
