package com.agrinvestapp.data.local.prefs;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.agrinvestapp.data.model.db.UserInfoBean;
import com.agrinvestapp.ui.auth.AuthActivity;
import com.agrinvestapp.utils.AppLogger;
import com.agrinvestapp.utils.AppUtils;

import java.util.HashMap;

/**
 * Created by hemant
 * Date: 10/4/18.
 */

public final class AppPreferencesHelper implements PreferencesHelper {

    private static final String APP_PREFERENCE = "AppPreference";
    private static final String APP_REM_PREFERENCE = "AppRemPreference";

    private static final String PREF_KEY_USER_LOGGED_IN_MODE = "PREF_KEY_USER_LOGGED_IN_MODE";
    private static final String PREF_KEY_APP_LANGUAGE = "PREF_KEY_APP_LANGUAGE";

    //Remember me
    private static final String PREF_KEY_APP_REM_EMAIL = "PREF_KEY_APP_REM_EMAIL";
    private static final String PREF_KEY_APP_REM_PWD = "PREF_KEY_APP_REM_PWD";

    //User info
    private static final String PREF_KEY_APP_USER_ID = "PREF_KEY_APP_USER_ID";
    private static final String PREF_KEY_APP_NAME = "PREF_KEY_APP_NAME";
    private static final String PREF_KEY_APP_EMAIL = "PREF_KEY_APP_EMAIL";
    private static final String PREF_KEY_APP_CONT_NUM = "PREF_KEY_APP_CONT_NUM";
    private static final String PREF_KEY_APP_LOCATION = "PREF_KEY_APP_LOCATION";
    private static final String PREF_KEY_APP_LAT = "PREF_KEY_APP_LAT";
    private static final String PREF_KEY_APP_LNG = "PREF_KEY_APP_LNG";
    private static final String PREF_KEY_APP_DEVICE_TYPE = "PREF_KEY_APP_DEVICE_TYPE";
    private static final String PREF_KEY_APP_DEVICE_TOKEN = "PREF_KEY_APP_DEVICE_TOKEN";
    private static final String PREF_KEY_APP_SOCIAL_ID = "PREF_KEY_APP_SOCIAL_ID";
    private static final String PREF_KEY_APP_SOCIAL_TYPE = "PREF_KEY_APP_SOCIAL_TYPE";
    private static final String PREF_KEY_APP_AUTH_TOKEN = "PREF_KEY_APP_AUTH_TOKEN";
    private static final String PREF_KEY_APP_STATUS = "PREF_KEY_APP_STATUS";
    private static final String PREF_KEY_APP_PROFILE_IMAGE = "PREF_KEY_APP_PROFILE_IMAGE";
    private static final String PREF_KEY_APP_SUBSCRI_ID = "PREF_KEY_APP_SUBSCRI_ID";
    private static final String PREF_KEY_APP_SUBSCRI_PLAN = "PREF_KEY_APP_SUBSCRI_PLAN";
    private static final String PREF_KEY_APP_USER_TYPE = "PREF_KEY_APP_USER_TYPE";

    //Local Data Flag
    private static final String PREF_KEY_APP_LOCAL_DATA = "PREF_KEY_APP_LOCAL_DATA";

    //Sync status for online and offline
    private static final String PREF_KEY_APP_ONLINE_OFFLINE = "PREF_KEY_APP_ONLINE_OFFLINE";

    //Sync Timer of half an hour flag
    private static final String PREF_KEY_APP_SYNC_TIMER_FLAG = "PREF_KEY_APP_SYNC_TIMER_FLAG";

    private final SharedPreferences mPrefs;
    private final SharedPreferences mRemPrefs;

    public AppPreferencesHelper(Context context) {
        mPrefs = context.getSharedPreferences(APP_PREFERENCE, Context.MODE_PRIVATE);
        mRemPrefs = context.getSharedPreferences(APP_REM_PREFERENCE, Context.MODE_PRIVATE);
    }

    @Override
    public Boolean isLoggedIn() {
        return mPrefs.getBoolean(PREF_KEY_USER_LOGGED_IN_MODE, false);
    }

    @Override
    public UserInfoBean getUserInfo() {
        UserInfoBean userInfo = new UserInfoBean();

        userInfo.setUserId(mPrefs.getString(PREF_KEY_APP_USER_ID, ""));
        userInfo.setName(mPrefs.getString(PREF_KEY_APP_NAME, ""));
        userInfo.setEmail(mPrefs.getString(PREF_KEY_APP_EMAIL, ""));
        userInfo.setLanguage(mPrefs.getString(PREF_KEY_APP_LANGUAGE, "english"));
        userInfo.setContactNumber(mPrefs.getString(PREF_KEY_APP_CONT_NUM, ""));
        userInfo.setLocation(mPrefs.getString(PREF_KEY_APP_LOCATION, ""));
        userInfo.setLatitude(mPrefs.getString(PREF_KEY_APP_LAT, ""));
        userInfo.setLongitude(mPrefs.getString(PREF_KEY_APP_LNG, ""));
        userInfo.setDeviceType(mPrefs.getString(PREF_KEY_APP_DEVICE_TYPE, ""));
        userInfo.setDeviceToken(mPrefs.getString(PREF_KEY_APP_DEVICE_TOKEN, ""));
        userInfo.setSocialId(mPrefs.getString(PREF_KEY_APP_SOCIAL_ID, ""));
        userInfo.setSocialType(mPrefs.getString(PREF_KEY_APP_SOCIAL_TYPE, ""));
        userInfo.setAuthToken(mPrefs.getString(PREF_KEY_APP_AUTH_TOKEN, ""));
        userInfo.setStatus(mPrefs.getString(PREF_KEY_APP_STATUS, ""));
        userInfo.setProfileImage(mPrefs.getString(PREF_KEY_APP_PROFILE_IMAGE, ""));
        userInfo.setSubscriptionSaleId(mPrefs.getString(PREF_KEY_APP_SUBSCRI_ID, ""));
        userInfo.setSubscriptionPlan(mPrefs.getString(PREF_KEY_APP_SUBSCRI_PLAN, "free"));
        userInfo.setLocalDataExistForSync(mPrefs.getBoolean(PREF_KEY_APP_LOCAL_DATA, false));
        userInfo.setAppOnline(mPrefs.getBoolean(PREF_KEY_APP_ONLINE_OFFLINE, true));
        userInfo.setSyncTimer(mPrefs.getString(PREF_KEY_APP_SYNC_TIMER_FLAG, ""));
        userInfo.setUserType(mPrefs.getString(PREF_KEY_APP_USER_TYPE, ""));

        return userInfo;
    }

    @Override
    public void setUserInfo(UserInfoBean userInfo) {
        mPrefs.edit().putString(PREF_KEY_APP_USER_ID, userInfo.getUserId()).apply();
        mPrefs.edit().putString(PREF_KEY_APP_NAME, userInfo.getName()).apply();
        mPrefs.edit().putString(PREF_KEY_APP_EMAIL, userInfo.getEmail()).apply();
        mPrefs.edit().putString(PREF_KEY_APP_LANGUAGE, userInfo.getLanguage()).apply();
        mPrefs.edit().putString(PREF_KEY_APP_CONT_NUM, userInfo.getContactNumber()).apply();
        mPrefs.edit().putString(PREF_KEY_APP_LOCATION, userInfo.getLocation()).apply();
        mPrefs.edit().putString(PREF_KEY_APP_LAT, userInfo.getLatitude()).apply();
        mPrefs.edit().putString(PREF_KEY_APP_LNG, userInfo.getLongitude()).apply();
        mPrefs.edit().putString(PREF_KEY_APP_DEVICE_TYPE, userInfo.getDeviceType()).apply();
        mPrefs.edit().putString(PREF_KEY_APP_DEVICE_TOKEN, userInfo.getDeviceToken()).apply();
        mPrefs.edit().putString(PREF_KEY_APP_SOCIAL_ID, userInfo.getSocialId()).apply();
        mPrefs.edit().putString(PREF_KEY_APP_SOCIAL_TYPE, userInfo.getSocialType()).apply();
        mPrefs.edit().putString(PREF_KEY_APP_AUTH_TOKEN, userInfo.getAuthToken()).apply();
        mPrefs.edit().putString(PREF_KEY_APP_STATUS, userInfo.getStatus()).apply();
        mPrefs.edit().putString(PREF_KEY_APP_PROFILE_IMAGE, userInfo.getProfileImage()).apply();
        mPrefs.edit().putString(PREF_KEY_APP_SUBSCRI_ID, userInfo.getSubscriptionSaleId()).apply();
        mPrefs.edit().putString(PREF_KEY_APP_SUBSCRI_PLAN, userInfo.getSubscriptionPlan()).apply();
        mPrefs.edit().putString(PREF_KEY_APP_SYNC_TIMER_FLAG, userInfo.getSyncTimer()).apply();
        mPrefs.edit().putString(PREF_KEY_APP_USER_TYPE, userInfo.getUserType()).apply();

        mPrefs.edit().putBoolean(PREF_KEY_APP_ONLINE_OFFLINE, userInfo.getAppOnline()).apply();
        mPrefs.edit().putBoolean(PREF_KEY_APP_LOCAL_DATA, userInfo.getLocalDataExistForSync()).apply();
        mPrefs.edit().putBoolean(PREF_KEY_USER_LOGGED_IN_MODE, true).apply();
    }

    @Override
    public void setRememberMe(String email, String pwd) {
        mRemPrefs.edit().putString(PREF_KEY_APP_REM_EMAIL, email).apply();
        mRemPrefs.edit().putString(PREF_KEY_APP_REM_PWD, pwd).apply();
    }

    @Override
    public String[] getRememberMe() {
        String[] cre = new String[2];
        cre[0] = mRemPrefs.getString(PREF_KEY_APP_REM_EMAIL, "");
        cre[1] = mRemPrefs.getString(PREF_KEY_APP_REM_PWD, "");
        return cre;
    }

    @Override
    public HashMap<String, String> getHeader() {
        HashMap<String, String> mHeaderMap = new HashMap<>();
        mHeaderMap.put("authToken", mPrefs.getString(PREF_KEY_APP_AUTH_TOKEN, ""));
        mHeaderMap.put("Language", mPrefs.getString(PREF_KEY_APP_LANGUAGE, "english"));
        AppLogger.w("authToken & Language ", mHeaderMap.toString());
        return mHeaderMap;
    }

    @Override
    public Boolean getLocalDataExistForSync() {
        return mPrefs.getBoolean(PREF_KEY_APP_LOCAL_DATA, false);
    }

    @Override
    public void setLocalDataExistForSync(Boolean status) {
        mPrefs.edit().putBoolean(PREF_KEY_APP_LOCAL_DATA, status).apply();
    }

    @Override
    public Boolean getAppOnline() {
        return mPrefs.getBoolean(PREF_KEY_APP_ONLINE_OFFLINE, true);
    }

    @Override
    public void setAppOnline(Boolean isAppOnline) {
        mPrefs.edit().putBoolean(PREF_KEY_APP_ONLINE_OFFLINE, isAppOnline).apply();
    }

    @Override
    public String getSyncTimer() {
        return mPrefs.getString(PREF_KEY_APP_SYNC_TIMER_FLAG, "");
    }

    @Override
    public void setSyncTimer(String timer) {
        mPrefs.edit().putString(PREF_KEY_APP_SYNC_TIMER_FLAG, timer).apply();
    }

    @Override
    public void logout(Activity activity) {
        AppUtils.setLanguage(activity, "en");
        mPrefs.edit().putBoolean(PREF_KEY_USER_LOGGED_IN_MODE, false).apply();
        mPrefs.edit().clear().apply();

        Intent intent = new Intent(activity, AuthActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(intent);
        activity.finish();
    }

}
