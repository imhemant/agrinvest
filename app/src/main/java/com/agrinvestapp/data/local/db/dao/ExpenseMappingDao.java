package com.agrinvestapp.data.local.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.agrinvestapp.data.model.db.ExpenseMapping;

import java.util.List;

/**
 * Created by hemant
 * Date: 10/4/18.
 */

@Dao
public interface ExpenseMappingDao {

    @Query("DELETE FROM expense_mapping WHERE expenseMappingId = :expenseMappingId")
    void deleteUsingExpenseMappingId(String expenseMappingId);

    @Query("DELETE FROM expense_mapping WHERE expense_id = :expenseId")
    void deleteExpenseMappingUsingExpenseId(String expenseId);

    @Query("DELETE FROM expense_mapping")
    void deleteAll();

    @Update
    void update(ExpenseMapping expenseMapping);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(ExpenseMapping expenseMapping);

    @Update
    void updateAll(List<ExpenseMapping> expenseMapping);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<ExpenseMapping> expenseMapping);

    @Query("SELECT * FROM expense_mapping Where event != 'delete'")
    List<ExpenseMapping> loadAll();

    @Query("SELECT * FROM expense_mapping WHERE event != 'delete' AND expenseMappingId IN (:expenseMappingIds)")
    List<ExpenseMapping> loadAllByIds(List<Integer> expenseMappingIds);

    @Query("UPDATE expense_mapping SET isDataSync = 1")
    void expenseMappingSyncUpdate();

}
