package com.agrinvestapp.data;


import android.app.Activity;

import com.agrinvestapp.data.local.db.DbHelper;
import com.agrinvestapp.data.local.prefs.PreferencesHelper;
import com.agrinvestapp.data.model.db.UserInfoBean;
import com.agrinvestapp.data.remote.ApiHelper;

import java.util.HashMap;
import java.util.List;


/**
 * Created by hemant
 * Date: 10/4/18.
 */

public interface DataManager extends DbHelper, PreferencesHelper, ApiHelper {

    @Override
    UserInfoBean getUserInfo();

    @Override
    void setUserInfo(UserInfoBean userInfo);

    @Override
    HashMap<String, String> getHeader();

    @Override
    List<UserInfoBean> getAllUsers();

    @Override
    void logout(Activity activity);

    @Override
    Boolean isLoggedIn();
}
